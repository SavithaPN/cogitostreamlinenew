USE [BalajiTesting]
GO
/****** Object:  View [dbo].[Vw_MaterialParts]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_MaterialParts]
AS
SELECT     Pk_MaterialCategory, Name AS PName
FROM         dbo.Inv_MaterialCategory
WHERE     (Name IN ('Cap', 'Plate', 'LengthPartition', 'WidthPartition', 'Board', 'Top', 'Bottom', 'Sleeve'))
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[42] 4[20] 2[7] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Inv_MaterialCategory"
            Begin Extent = 
               Top = 0
               Left = 68
               Bottom = 255
               Right = 257
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 29
         Width = 284
         Width = 1500
         Width = 2250
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_MaterialParts'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_MaterialParts'
GO
/****** Object:  View [dbo].[Vw_JobProcess]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_JobProcess]
AS
SELECT     dbo.wfWorkflowRoles.RoleName, dbo.ideUser.Pk_Users, dbo.wfWorkflowRoles.Pk_WfRole, dbo.ideUser.UserName, dbo.ideActivity.ActivityName
FROM         dbo.ideActivityPermission INNER JOIN
                      dbo.idePermission ON dbo.ideActivityPermission.Fk_Permissions = dbo.idePermission.Pk_Permissions INNER JOIN
                      dbo.ideActivity ON dbo.ideActivityPermission.Fk_Activities = dbo.ideActivity.Pk_Activities INNER JOIN
                      dbo.wfWorkflowRoles INNER JOIN
                      dbo.wfWorkflowRoleUser ON dbo.wfWorkflowRoles.Pk_WfRole = dbo.wfWorkflowRoleUser.Fk_Role INNER JOIN
                      dbo.ideUser ON dbo.wfWorkflowRoleUser.Fk_User = dbo.ideUser.Pk_Users ON dbo.ideActivityPermission.Fk_Roles = dbo.wfWorkflowRoleUser.Fk_Role
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[47] 4[14] 2[6] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "wfWorkflowRoles"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 96
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "wfWorkflowRoleUser"
            Begin Extent = 
               Top = 11
               Left = 476
               Bottom = 143
               Right = 691
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ideUser"
            Begin Extent = 
               Top = 0
               Left = 968
               Bottom = 221
               Right = 1128
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "idePermission"
            Begin Extent = 
               Top = 147
               Left = 0
               Bottom = 271
               Right = 166
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ideActivity"
            Begin Extent = 
               Top = 120
               Left = 331
               Bottom = 307
               Right = 517
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ideActivityPermission"
            Begin Extent = 
               Top = 138
               Left = 741
               Bottom = 269
               Right = 938
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Wi' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_JobProcess'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'dth = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_JobProcess'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_JobProcess'
GO
/****** Object:  View [dbo].[vw_VendorDetails]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_VendorDetails]
AS
SELECT        dbo.gen_Vendor.Pk_Vendor, dbo.gen_Vendor.VendorName, dbo.gen_ContactPerson.Pk_ContactPersonId, dbo.gen_ContactPerson.ContactPersonName, dbo.gen_VendorContacts.Pk_VendorContact, 
                         dbo.gen_City.CityName, dbo.gen_State.StateName, dbo.gen_Country.CountryName
FROM            dbo.gen_ContactPerson INNER JOIN
                         dbo.gen_VendorContacts ON dbo.gen_ContactPerson.Pk_ContactPersonId = dbo.gen_VendorContacts.Fk_ContactPerson INNER JOIN
                         dbo.gen_Vendor ON dbo.gen_VendorContacts.Fk_Vendor = dbo.gen_Vendor.Pk_Vendor INNER JOIN
                         dbo.gen_City ON dbo.gen_ContactPerson.Address_City = dbo.gen_City.Pk_CityID AND dbo.gen_Vendor.Fk_City = dbo.gen_City.Pk_CityID INNER JOIN
                         dbo.gen_State ON dbo.gen_ContactPerson.Address_State = dbo.gen_State.Pk_StateID AND dbo.gen_Vendor.Fk_State = dbo.gen_State.Pk_StateID INNER JOIN
                         dbo.gen_Country ON dbo.gen_Vendor.Fk_Country = dbo.gen_Country.Pk_Country
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "gen_ContactPerson"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 268
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_VendorContacts"
            Begin Extent = 
               Top = 6
               Left = 306
               Bottom = 119
               Right = 493
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Vendor"
            Begin Extent = 
               Top = 6
               Left = 531
               Bottom = 136
               Right = 738
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_City"
            Begin Extent = 
               Top = 6
               Left = 776
               Bottom = 119
               Right = 946
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_State"
            Begin Extent = 
               Top = 6
               Left = 984
               Bottom = 119
               Right = 1154
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Country"
            Begin Extent = 
               Top = 120
               Left = 306
               Bottom = 216
               Right = 476
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Co' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_VendorDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'lumn = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_VendorDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_VendorDetails'
GO
/****** Object:  View [dbo].[VW_MillDuplicate]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VW_MillDuplicate]
AS
SELECT     dbo.gen_ContactPerson.ContactPersonName, dbo.gen_MillContacts.Fk_Mill, dbo.gen_MillContacts.Pk_MillContacts
FROM         dbo.gen_ContactPerson INNER JOIN
                      dbo.gen_MillContacts ON dbo.gen_ContactPerson.Pk_ContactPersonId = dbo.gen_MillContacts.Fk_ContactPerson INNER JOIN
                      dbo.gen_Mill ON dbo.gen_MillContacts.Fk_Mill = dbo.gen_Mill.Pk_Mill
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "gen_ContactPerson"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 252
               Right = 223
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_MillContacts"
            Begin Extent = 
               Top = 13
               Left = 726
               Bottom = 257
               Right = 1060
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Mill"
            Begin Extent = 
               Top = 92
               Left = 337
               Bottom = 269
               Right = 544
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VW_MillDuplicate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VW_MillDuplicate'
GO
/****** Object:  View [dbo].[Vw_Mill_Paper]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_Mill_Paper]
AS
SELECT     dbo.Inv_Material.Pk_Material, dbo.Inv_Material.Name, dbo.gen_Mill.MillName, dbo.Inv_Material.GSM, dbo.Inv_Material.BF
FROM         dbo.Inv_Material INNER JOIN
                      dbo.gen_Mill ON dbo.Inv_Material.Fk_Mill = dbo.gen_Mill.Pk_Mill
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[42] 4[23] 2[4] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "gen_Mill"
            Begin Extent = 
               Top = 1
               Left = 597
               Bottom = 121
               Right = 763
            End
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 227
               Right = 227
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Mill_Paper'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Mill_Paper'
GO
/****** Object:  View [dbo].[Vw_PaperStock]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_PaperStock]
AS
SELECT     dbo.Inv_Material.Pk_Material, dbo.Inv_Material.Name, dbo.gen_Color.ColorName, dbo.gen_Mill.Pk_Mill, dbo.gen_Mill.MillName, dbo.Inv_Material.GSM, dbo.Inv_Material.Deckle, 
                      dbo.Inv_Material.BF
FROM         dbo.Inv_Material INNER JOIN
                      dbo.gen_Color ON dbo.Inv_Material.Fk_Color = dbo.gen_Color.Pk_Color INNER JOIN
                      dbo.gen_Mill ON dbo.Inv_Material.Fk_Mill = dbo.gen_Mill.Pk_Mill
GROUP BY dbo.Inv_Material.Pk_Material, dbo.Inv_Material.Name, dbo.gen_Color.ColorName, dbo.gen_Mill.Pk_Mill, dbo.gen_Mill.MillName, dbo.Inv_Material.GSM, dbo.Inv_Material.Deckle, 
                      dbo.Inv_Material.BF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[48] 4[16] 2[11] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 316
               Right = 232
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Color"
            Begin Extent = 
               Top = 166
               Left = 329
               Bottom = 434
               Right = 489
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Mill"
            Begin Extent = 
               Top = 6
               Left = 484
               Bottom = 125
               Right = 666
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PaperStock'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PaperStock'
GO
/****** Object:  View [dbo].[Vw_ItemMaster]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_ItemMaster]
AS
SELECT     dbo.Inv_MaterialCategory.Name AS Cat_Name, dbo.Inv_Material.Pk_Material, dbo.Inv_Material.Name, dbo.Inv_Material.Min_Value, dbo.Inv_Material.Max_Value, dbo.gen_Unit.UnitName, 
                      dbo.Inv_MaterialCategory.Pk_MaterialCategory
FROM         dbo.Inv_Material INNER JOIN
                      dbo.Inv_MaterialCategory ON dbo.Inv_Material.Fk_MaterialCategory = dbo.Inv_MaterialCategory.Pk_MaterialCategory INNER JOIN
                      dbo.gen_Unit ON dbo.Inv_Material.Fk_UnitId = dbo.gen_Unit.Pk_Unit
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 6
               Left = 28
               Bottom = 199
               Right = 227
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_MaterialCategory"
            Begin Extent = 
               Top = 7
               Left = 445
               Bottom = 192
               Right = 634
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Unit"
            Begin Extent = 
               Top = 6
               Left = 265
               Bottom = 126
               Right = 425
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_ItemMaster'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_ItemMaster'
GO
/****** Object:  View [dbo].[VW_BoardSearch]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VW_BoardSearch]
AS
SELECT     dbo.gen_Mill.MillName, dbo.Inv_Material.Name, dbo.Inv_Material.GSM, dbo.Inv_Material.Pk_Material, dbo.Inv_Material.Fk_MaterialCategory, dbo.Inv_Material.Fk_Mill, 
                      dbo.Inv_Material.Fk_PaperType, dbo.Inv_Material.Fk_Color, dbo.Inv_Material.Deckle, dbo.Inv_Material.BF, dbo.Inv_Material.Length, dbo.Inv_Material.Width, dbo.Inv_Material.Height, 
                      dbo.Inv_Material.GSM2, dbo.Inv_Material.PPly, dbo.Inv_Material.FPly, dbo.Inv_Material.Weight, dbo.Inv_Material.Size, dbo.Inv_Material.VendorIdNumber, dbo.Inv_Material.Company, 
                      dbo.Inv_Material.Brand, dbo.Inv_Material.MaterialType, dbo.Inv_Material.Description, dbo.Inv_Material.Fk_UnitId, dbo.gen_Color.ColorName, dbo.gen_Unit.UnitName, 
                      dbo.Inv_MaterialCategory.Name AS Mat_Cat_Name
FROM         dbo.Inv_Material INNER JOIN
                      dbo.gen_Mill ON dbo.Inv_Material.Fk_Mill = dbo.gen_Mill.Pk_Mill INNER JOIN
                      dbo.gen_Color ON dbo.Inv_Material.Fk_Color = dbo.gen_Color.Pk_Color INNER JOIN
                      dbo.gen_Unit ON dbo.Inv_Material.Fk_UnitId = dbo.gen_Unit.Pk_Unit INNER JOIN
                      dbo.Inv_MaterialCategory ON dbo.Inv_Material.Fk_MaterialCategory = dbo.Inv_MaterialCategory.Pk_MaterialCategory
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[21] 2[14] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 269
               Right = 246
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Mill"
            Begin Extent = 
               Top = 6
               Left = 255
               Bottom = 126
               Right = 421
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Color"
            Begin Extent = 
               Top = 6
               Left = 459
               Bottom = 96
               Right = 619
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Unit"
            Begin Extent = 
               Top = 6
               Left = 657
               Bottom = 126
               Right = 817
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_MaterialCategory"
            Begin Extent = 
               Top = 150
               Left = 500
               Bottom = 255
               Right = 689
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 28
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 15' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VW_BoardSearch'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'00
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VW_BoardSearch'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VW_BoardSearch'
GO
/****** Object:  View [dbo].[Vw_CustShip]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_CustShip]
AS
SELECT     dbo.gen_Customer.Pk_Customer, dbo.gen_CustomerShippingDetails.Pk_CustomerShippingId, dbo.gen_Customer.CustomerName, dbo.gen_CustomerShippingDetails.ShippingCustomerName, 
                      dbo.gen_CustomerShippingDetails.ShippingAddress, dbo.gen_CustomerShippingDetails.ShippingCountry, dbo.gen_CustomerShippingDetails.ShippingState, 
                      dbo.gen_CustomerShippingDetails.ShippingPincode, dbo.gen_CustomerShippingDetails.ShippingCity, dbo.gen_CustomerShippingDetails.ShippingMobile, 
                      dbo.gen_CustomerShippingDetails.ShippingLandLine, dbo.gen_CustomerShippingDetails.ShippingEmailId, dbo.gen_CustomerShippingDetails.Fk_CustomerId
FROM         dbo.gen_Customer INNER JOIN
                      dbo.gen_CustomerShippingDetails ON dbo.gen_Customer.Pk_Customer = dbo.gen_CustomerShippingDetails.Fk_CustomerId
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 126
               Right = 212
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_CustomerShippingDetails"
            Begin Extent = 
               Top = 6
               Left = 250
               Bottom = 126
               Right = 452
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_CustShip'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_CustShip'
GO
/****** Object:  View [dbo].[vw_CustomerDetails]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_CustomerDetails]
AS
SELECT        dbo.gen_Customer.Pk_Customer, dbo.gen_Customer.CustomerName, dbo.gen_ContactPerson.Pk_ContactPersonId, dbo.gen_ContactPerson.ContactPersonName, 
                         dbo.gen_CustomerContacts.Pk_CustomerContacts, dbo.gen_City.CityName, dbo.gen_State.StateName, dbo.gen_Country.CountryName
FROM            dbo.gen_ContactPerson INNER JOIN
                         dbo.gen_CustomerContacts ON dbo.gen_ContactPerson.Pk_ContactPersonId = dbo.gen_CustomerContacts.Fk_ContactPerson INNER JOIN
                         dbo.gen_Customer ON dbo.gen_CustomerContacts.Fk_Customer = dbo.gen_Customer.Pk_Customer INNER JOIN
                         dbo.gen_City ON dbo.gen_ContactPerson.Address_City = dbo.gen_City.Pk_CityID AND dbo.gen_Customer.Fk_City = dbo.gen_City.Pk_CityID INNER JOIN
                         dbo.gen_State ON dbo.gen_ContactPerson.Address_State = dbo.gen_State.Pk_StateID AND dbo.gen_Customer.Fk_State = dbo.gen_State.Pk_StateID INNER JOIN
                         dbo.gen_Country ON dbo.gen_Customer.Fk_Country = dbo.gen_Country.Pk_Country
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "gen_ContactPerson"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 268
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 6
               Left = 306
               Bottom = 206
               Right = 489
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_CustomerContacts"
            Begin Extent = 
               Top = 6
               Left = 527
               Bottom = 244
               Right = 733
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_City"
            Begin Extent = 
               Top = 6
               Left = 771
               Bottom = 119
               Right = 941
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_State"
            Begin Extent = 
               Top = 120
               Left = 771
               Bottom = 216
               Right = 941
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Country"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 234
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
       ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_CustomerDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'  Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_CustomerDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_CustomerDetails'
GO
/****** Object:  View [dbo].[Vw_EnqReport]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_EnqReport]
AS
SELECT     dbo.eq_Enquiry.Pk_Enquiry, dbo.eq_Enquiry.Date, dbo.gen_Customer.CustomerName, dbo.BoxMaster.Name, dbo.eq_EnquiryChild.Quantity, dbo.BoxMaster.Pk_BoxID, 
                      dbo.eq_EnquiryChild.Ddate
FROM         dbo.eq_Enquiry INNER JOIN
                      dbo.eq_EnquiryChild ON dbo.eq_Enquiry.Pk_Enquiry = dbo.eq_EnquiryChild.Fk_Enquiry INNER JOIN
                      dbo.gen_Customer ON dbo.eq_Enquiry.Customer = dbo.gen_Customer.Pk_Customer INNER JOIN
                      dbo.BoxMaster ON dbo.eq_EnquiryChild.Fk_BoxID = dbo.BoxMaster.Pk_BoxID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "eq_Enquiry"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 208
               Right = 226
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "eq_EnquiryChild"
            Begin Extent = 
               Top = 0
               Left = 468
               Bottom = 216
               Right = 670
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 98
               Left = 267
               Bottom = 218
               Right = 441
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 6
               Left = 716
               Bottom = 126
               Right = 876
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 135' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_EnqReport'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'0
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_EnqReport'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_EnqReport'
GO
/****** Object:  View [dbo].[Vw_BoardDesc]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_BoardDesc]
AS
SELECT     dbo.Inv_Material.Pk_Material, dbo.BoardDesc.GSM, dbo.BoardDesc.BF, dbo.BoardDesc.Color, dbo.Inv_Material.Name, dbo.Inv_Material.Length, dbo.Inv_Material.Width, 
                      dbo.Inv_Material.Height
FROM         dbo.BoardDesc INNER JOIN
                      dbo.Inv_Material ON dbo.BoardDesc.Fk_BoardID = dbo.Inv_Material.Pk_Material
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 126
               Right = 425
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoardDesc"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 126
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_BoardDesc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_BoardDesc'
GO
/****** Object:  View [dbo].[Vw_Inwd]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_Inwd]
AS
SELECT     dbo.QualityCheck.Pk_QualityCheck, CONVERT(varchar, dbo.QualityCheck.CheckDate, 103) AS CheckDate, dbo.QualityCheck.Fk_PONo, dbo.QualityCheck.CheckDate AS Expr1, 
                      dbo.PurchaseOrderM.Fk_Indent
FROM         dbo.QualityCheck INNER JOIN
                      dbo.PurchaseOrderM ON dbo.QualityCheck.Fk_PONo = dbo.PurchaseOrderM.Pk_PONo
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[55] 4[5] 2[22] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "QualityCheck"
            Begin Extent = 
               Top = 2
               Left = 393
               Bottom = 239
               Right = 585
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PurchaseOrderM"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 258
               Right = 197
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 14
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1890
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Inwd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Inwd'
GO
/****** Object:  View [dbo].[Vw_BoxPaperStock]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_BoxPaperStock]
AS
SELECT     dbo.Inv_Material.Pk_Material, dbo.Inv_Material.Name, dbo.BoxChild.Fk_BoxID, dbo.BoxChild.Pk_BoxCID, ISNULL(dbo.PaperStock.Quantity, 0) AS Expr1, dbo.PaperStock.Quantity, 
                      dbo.PaperStock.RollNo
FROM         dbo.Inv_Material INNER JOIN
                      dbo.BoxChild ON dbo.Inv_Material.Pk_Material = dbo.BoxChild.Fk_Material LEFT OUTER JOIN
                      dbo.PaperStock ON dbo.Inv_Material.Pk_Material = dbo.PaperStock.Fk_Material
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[9] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 213
               Right = 212
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxChild"
            Begin Extent = 
               Top = 102
               Left = 537
               Bottom = 207
               Right = 697
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PaperStock"
            Begin Extent = 
               Top = 0
               Left = 918
               Bottom = 227
               Right = 1078
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_BoxPaperStock'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_BoxPaperStock'
GO
/****** Object:  View [dbo].[Vw_SumPaperStock]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_SumPaperStock]
AS
SELECT     SUM(Quantity) AS Qty, Fk_Material
FROM         dbo.PaperStock
GROUP BY Fk_Material
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[7] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "PaperStock"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 269
               Right = 235
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_SumPaperStock'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_SumPaperStock'
GO
/****** Object:  View [dbo].[Vw_StockSum]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_StockSum]
AS
SELECT     dbo.Inv_MaterialCategory.Pk_MaterialCategory, dbo.Inv_MaterialCategory.Name AS Cat_Name, dbo.Inv_Material.Pk_Material, dbo.Inv_Material.Name AS Mat_Name, dbo.Stocks.Quantity
FROM         dbo.Inv_MaterialCategory INNER JOIN
                      dbo.Inv_Material ON dbo.Inv_MaterialCategory.Pk_MaterialCategory = dbo.Inv_Material.Fk_MaterialCategory INNER JOIN
                      dbo.Stocks ON dbo.Inv_Material.Pk_Material = dbo.Stocks.Fk_Material
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[8] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Inv_MaterialCategory"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 243
               Right = 224
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 1
               Left = 373
               Bottom = 238
               Right = 560
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Stocks"
            Begin Extent = 
               Top = 0
               Left = 727
               Bottom = 243
               Right = 881
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_StockSum'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_StockSum'
GO
/****** Object:  View [dbo].[vw_StockMaterial]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_StockMaterial]
AS
SELECT        dbo.Inv_Material.Pk_Material, dbo.Inv_Material.Name, dbo.Stocks.Quantity, dbo.Inv_Material.Min_Value, dbo.Stocks.Pk_Stock, dbo.Stocks.Fk_Tanent
FROM            dbo.Inv_Material INNER JOIN
                         dbo.Stocks ON dbo.Inv_Material.Pk_Material = dbo.Stocks.Fk_Material
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[9] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 234
               Right = 235
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Stocks"
            Begin Extent = 
               Top = 6
               Left = 273
               Bottom = 188
               Right = 443
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_StockMaterial'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_StockMaterial'
GO
/****** Object:  View [dbo].[Vw_TotPaperStockList]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_TotPaperStockList]
AS
SELECT     SUM(dbo.PaperStock.Quantity) AS TQty, dbo.Inv_Material.Pk_Material, dbo.Inv_Material.Name, dbo.Inv_Material.Min_Value, dbo.gen_Mill.MillName, dbo.gen_Color.ColorName, 
                      dbo.PaperStock.Fk_Material, dbo.Inv_Material.GSM, dbo.Inv_Material.BF
FROM         dbo.PaperStock INNER JOIN
                      dbo.Inv_Material ON dbo.PaperStock.Fk_Material = dbo.Inv_Material.Pk_Material INNER JOIN
                      dbo.gen_Mill ON dbo.Inv_Material.Fk_Mill = dbo.gen_Mill.Pk_Mill INNER JOIN
                      dbo.gen_Color ON dbo.Inv_Material.Fk_Color = dbo.gen_Color.Pk_Color
GROUP BY dbo.Inv_Material.Pk_Material, dbo.Inv_Material.Name, dbo.Inv_Material.Min_Value, dbo.gen_Mill.MillName, dbo.gen_Color.ColorName, dbo.PaperStock.Fk_Material, dbo.Inv_Material.GSM, 
                      dbo.Inv_Material.BF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[44] 4[15] 2[4] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "PaperStock"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 126
               Right = 214
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 14
               Left = 313
               Bottom = 227
               Right = 518
            End
            DisplayFlags = 280
            TopColumn = 6
         End
         Begin Table = "gen_Mill"
            Begin Extent = 
               Top = 6
               Left = 556
               Bottom = 126
               Right = 738
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Color"
            Begin Extent = 
               Top = 6
               Left = 776
               Bottom = 96
               Right = 952
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 12
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 3015
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 13' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_TotPaperStockList'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'50
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_TotPaperStockList'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_TotPaperStockList'
GO
/****** Object:  View [dbo].[vw_PurDisplay]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_PurDisplay]
AS
SELECT     dbo.PurchaseOrderM.*, dbo.Inv_Material.Name
FROM         dbo.PurchaseOrderM CROSS JOIN
                      dbo.Inv_Material
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "PurchaseOrderM"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 216
               Right = 396
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 6
               Left = 434
               Bottom = 126
               Right = 623
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_PurDisplay'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_PurDisplay'
GO
/****** Object:  View [dbo].[Vw_QualityChild]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_QualityChild]
AS
SELECT     dbo.QualityChild.AccQty, dbo.Inv_Material.Pk_Material, dbo.Inv_Material.Name, dbo.QualityChild.FkQualityCheck
FROM         dbo.Inv_Material INNER JOIN
                      dbo.QualityChild ON dbo.Inv_Material.Pk_Material = dbo.QualityChild.Fk_Material
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[5] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 226
               Right = 230
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "QualityChild"
            Begin Extent = 
               Top = 6
               Left = 265
               Bottom = 226
               Right = 429
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_QualityChild'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_QualityChild'
GO
/****** Object:  View [dbo].[Vw_Quality]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_Quality]
AS
SELECT     dbo.QualityCheck.Pk_QualityCheck, dbo.QualityCheck.CheckDate, dbo.QualityCheck.Fk_PONo, dbo.Inv_Material.Name, dbo.QualityChild.POQty, dbo.QualityChild.AccQty, 
                      dbo.QualityCheck.InvoiceNumber, dbo.gen_Color.ColorName, dbo.gen_Mill.MillName
FROM         dbo.QualityCheck INNER JOIN
                      dbo.QualityChild ON dbo.QualityCheck.Pk_QualityCheck = dbo.QualityChild.FkQualityCheck INNER JOIN
                      dbo.Inv_Material ON dbo.QualityChild.Fk_Material = dbo.Inv_Material.Pk_Material INNER JOIN
                      dbo.gen_Color ON dbo.Inv_Material.Fk_Color = dbo.gen_Color.Pk_Color INNER JOIN
                      dbo.gen_Mill ON dbo.Inv_Material.Fk_Mill = dbo.gen_Mill.Pk_Mill
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "QualityCheck"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 212
               Right = 209
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "QualityChild"
            Begin Extent = 
               Top = 6
               Left = 245
               Bottom = 205
               Right = 410
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 6
               Left = 448
               Bottom = 126
               Right = 637
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Color"
            Begin Extent = 
               Top = 6
               Left = 675
               Bottom = 96
               Right = 835
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Mill"
            Begin Extent = 
               Top = 6
               Left = 873
               Bottom = 126
               Right = 1039
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column =' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Quality'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N' 1440
         Alias = 900
         Table = 1605
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Quality'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Quality'
GO
/****** Object:  View [dbo].[Vw_Quote]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_Quote]
AS
SELECT DISTINCT 
                      dbo.QuotationMaster.Pk_Quotation, dbo.gen_Customer.CustomerName, dbo.QuotationMaster.QuotationDate, dbo.eq_Enquiry.Pk_Enquiry, dbo.eq_Enquiry.Date, dbo.eq_EnquiryChild.Quantity, 
                      dbo.BoxMaster.Pk_BoxID, dbo.BoxMaster.Name, dbo.QuotationMaster.Fk_Enquiry
FROM         dbo.eq_EnquiryChild RIGHT OUTER JOIN
                      dbo.BoxMaster ON dbo.eq_EnquiryChild.Fk_BoxID = dbo.BoxMaster.Pk_BoxID RIGHT OUTER JOIN
                      dbo.gen_Customer INNER JOIN
                      dbo.eq_Enquiry ON dbo.gen_Customer.Pk_Customer = dbo.eq_Enquiry.Customer INNER JOIN
                      dbo.QuotationMaster ON dbo.eq_Enquiry.Pk_Enquiry = dbo.QuotationMaster.Fk_Enquiry ON dbo.eq_EnquiryChild.Fk_Enquiry = dbo.eq_Enquiry.Pk_Enquiry
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[44] 4[4] 2[8] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "eq_Enquiry"
            Begin Extent = 
               Top = 171
               Left = 1137
               Bottom = 367
               Right = 1325
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "QuotationMaster"
            Begin Extent = 
               Top = 6
               Left = 276
               Bottom = 214
               Right = 527
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 246
               Left = 547
               Bottom = 420
               Right = 721
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 0
               Left = 648
               Bottom = 120
               Right = 803
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "eq_EnquiryChild"
            Begin Extent = 
               Top = 0
               Left = 949
               Bottom = 168
               Right = 1151
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 17
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         W' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Quote'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'idth = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Quote'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Quote'
GO
/****** Object:  View [dbo].[Vw_Popup_MaterialIssue]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_Popup_MaterialIssue]
AS
SELECT     dbo.Inv_MaterialCategory.Name, dbo.Inv_MaterialCategory.Pk_MaterialCategory, dbo.Inv_Material.Name AS MaterialName, dbo.Inv_Material.Pk_Material, dbo.Stocks.Quantity, dbo.Stocks.Pk_Stock, 
                      dbo.Stocks.Fk_Tanent, dbo.gen_Unit.UnitName, dbo.Stocks.Fk_BranchID, dbo.gen_Color.ColorName, dbo.gen_Mill.MillName, dbo.Inv_Material.Length, dbo.Inv_Material.Width, 
                      dbo.Inv_Material.Height, dbo.Inv_Material.PPly, dbo.Inv_Material.Weight, dbo.Inv_Material.Brand
FROM         dbo.Inv_Material INNER JOIN
                      dbo.Inv_MaterialCategory ON dbo.Inv_Material.Fk_MaterialCategory = dbo.Inv_MaterialCategory.Pk_MaterialCategory INNER JOIN
                      dbo.Stocks ON dbo.Inv_Material.Pk_Material = dbo.Stocks.Fk_Material LEFT OUTER JOIN
                      dbo.gen_Unit ON dbo.Inv_Material.Fk_UnitId = dbo.gen_Unit.Pk_Unit LEFT OUTER JOIN
                      dbo.gen_Mill ON dbo.Inv_Material.Fk_Mill = dbo.gen_Mill.Pk_Mill LEFT OUTER JOIN
                      dbo.gen_Color ON dbo.Inv_Material.Fk_Color = dbo.gen_Color.Pk_Color
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[29] 4[18] 2[11] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "gen_Mill"
            Begin Extent = 
               Top = 147
               Left = 586
               Bottom = 267
               Right = 752
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Unit"
            Begin Extent = 
               Top = 88
               Left = 812
               Bottom = 208
               Right = 972
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Color"
            Begin Extent = 
               Top = 94
               Left = 336
               Bottom = 184
               Right = 496
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_MaterialCategory"
            Begin Extent = 
               Top = 4
               Left = 607
               Bottom = 124
               Right = 791
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 11
               Left = 52
               Bottom = 214
               Right = 241
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Stocks"
            Begin Extent = 
               Top = 0
               Left = 853
               Bottom = 139
               Right = 1013
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 18
         Width = 284
         Width = 1500
       ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Popup_MaterialIssue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'  Width = 2700
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Popup_MaterialIssue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Popup_MaterialIssue'
GO
/****** Object:  View [dbo].[Vw_PartJobs]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_PartJobs]
AS
SELECT     dbo.Inv_Material.Pk_Material, dbo.Inv_Material.Name, dbo.Inv_Material.GSM, dbo.Inv_Material.BF, dbo.Inv_Material.Deckle, dbo.JobWorkChild.Length, dbo.JobWorkChild.Width, 
                      dbo.JobWorkChild.Height, dbo.JobWorkChild.Ply, dbo.JobWorkChild.AppNos, dbo.JobWorkChild.LayerWt, dbo.PartJobs.Pk_ID, dbo.PartJobs.RecdDate, dbo.PartJobs.Description, 
                      dbo.gen_Customer.CustomerName, dbo.JobWorkChild.RecdWeight, dbo.JobWorkChild.ReelNo
FROM         dbo.JobWorkChild INNER JOIN
                      dbo.Inv_Material ON dbo.JobWorkChild.Fk_Material = dbo.Inv_Material.Pk_Material INNER JOIN
                      dbo.PartJobs ON dbo.JobWorkChild.Fk_JobID = dbo.PartJobs.Pk_ID INNER JOIN
                      dbo.gen_Customer ON dbo.PartJobs.Fk_Customer = dbo.gen_Customer.Pk_Customer INNER JOIN
                      dbo.wfStates ON dbo.PartJobs.Fk_Status = dbo.wfStates.Pk_State
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 6
               Left = 665
               Bottom = 269
               Right = 854
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "JobWorkChild"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 269
               Right = 190
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 6
               Left = 892
               Bottom = 269
               Right = 1066
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "wfStates"
            Begin Extent = 
               Top = 168
               Left = 978
               Bottom = 258
               Right = 1138
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PartJobs"
            Begin Extent = 
               Top = 101
               Left = 275
               Bottom = 340
               Right = 433
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 18
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 15' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PartJobs'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'00
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PartJobs'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PartJobs'
GO
/****** Object:  View [dbo].[Vw_OrderList]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_OrderList]
AS
SELECT     dbo.gen_Order.Pk_Order, dbo.gen_Order.OrderDate, dbo.gen_Order.Product, dbo.gen_Order.Quantity, dbo.gen_Order.Price, dbo.gen_Customer.CustomerName, 
                      dbo.gen_Customer.Pk_Customer
FROM         dbo.gen_Order INNER JOIN
                      dbo.gen_Customer ON dbo.gen_Order.Fk_Customer = dbo.gen_Customer.Pk_Customer
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "gen_Order"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 243
               Right = 196
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 6
               Left = 257
               Bottom = 243
               Right = 465
            End
            DisplayFlags = 280
            TopColumn = 3
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_OrderList'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_OrderList'
GO
/****** Object:  View [dbo].[Vw_OSTasks]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_OSTasks]
AS
SELECT     dbo.Inv_Material.Pk_Material, dbo.Inv_Material.Name, dbo.gen_Vendor.VendorName, dbo.Inv_Material.GSM, dbo.Inv_Material.BF, dbo.gen_Vendor.Pk_Vendor, dbo.Outsourcing.Pk_SrcID, 
                      dbo.Outsourcing.Description, dbo.OutsourcingChild.IssueWeight, dbo.OutsourcingChild.ReelNo, dbo.Inv_Material.Deckle, dbo.OutsourcingChild.Length, dbo.OutsourcingChild.Width, 
                      dbo.OutsourcingChild.LayerWt, dbo.OutsourcingChild.AppNos, dbo.Outsourcing.IssueDate, dbo.OutsourcingChild.Ply, dbo.OutsourcingChild.Pk_SrcDetID
FROM         dbo.Outsourcing INNER JOIN
                      dbo.gen_Vendor ON dbo.Outsourcing.Fk_Vendor = dbo.gen_Vendor.Pk_Vendor INNER JOIN
                      dbo.OutsourcingChild ON dbo.Outsourcing.Pk_SrcID = dbo.OutsourcingChild.Fk_PkSrcID LEFT OUTER JOIN
                      dbo.Inv_Material ON dbo.OutsourcingChild.Fk_Material = dbo.Inv_Material.Pk_Material
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[43] 4[7] 2[3] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Outsourcing"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 255
               Right = 190
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Vendor"
            Begin Extent = 
               Top = 122
               Left = 782
               Bottom = 366
               Right = 945
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 3
               Left = 1001
               Bottom = 267
               Right = 1202
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "OutsourcingChild"
            Begin Extent = 
               Top = 100
               Left = 287
               Bottom = 367
               Right = 451
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 19
         Width = 284
         Width = 1500
         Width = 1500
         Width = 2640
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_OSTasks'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'040
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_OSTasks'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_OSTasks'
GO
/****** Object:  View [dbo].[Vw_JobWorkStock]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_JobWorkStock]
AS
SELECT     dbo.gen_Customer.CustomerName, dbo.JobWorkRMStock.StockValue, dbo.JobWorkRMStock.RollNo, dbo.Inv_Material.Pk_Material, dbo.gen_Mill.MillName, dbo.gen_Color.ColorName, 
                      dbo.Inv_Material.Name, dbo.Inv_Material.GSM, dbo.Inv_Material.BF, dbo.Inv_Material.Deckle
FROM         dbo.Inv_Material INNER JOIN
                      dbo.JobWorkRMStock ON dbo.Inv_Material.Pk_Material = dbo.JobWorkRMStock.Fk_Material INNER JOIN
                      dbo.gen_Customer ON dbo.JobWorkRMStock.Fk_Customer = dbo.gen_Customer.Pk_Customer INNER JOIN
                      dbo.gen_Mill ON dbo.Inv_Material.Fk_Mill = dbo.gen_Mill.Pk_Mill INNER JOIN
                      dbo.gen_Color ON dbo.Inv_Material.Fk_Color = dbo.gen_Color.Pk_Color
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[16] 4[45] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 6
               Left = 18
               Bottom = 269
               Right = 227
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "JobWorkRMStock"
            Begin Extent = 
               Top = 24
               Left = 928
               Bottom = 255
               Right = 1088
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 144
               Left = 515
               Bottom = 264
               Right = 689
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Mill"
            Begin Extent = 
               Top = 6
               Left = 490
               Bottom = 126
               Right = 656
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Color"
            Begin Extent = 
               Top = 10
               Left = 326
               Bottom = 100
               Right = 486
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         A' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_JobWorkStock'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'lias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_JobWorkStock'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_JobWorkStock'
GO
/****** Object:  View [dbo].[VW_Min_Level]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VW_Min_Level]
AS
SELECT     dbo.Inv_Material.Pk_Material, dbo.Inv_Material.Name, dbo.Inv_Material.Min_Value, dbo.Inv_Material.Max_Value, dbo.gen_Unit.UnitName, dbo.Stocks.Quantity, 
                      dbo.Inv_MaterialCategory.Name AS Cat_Name, dbo.Inv_MaterialCategory.Pk_MaterialCategory
FROM         dbo.Inv_Material INNER JOIN
                      dbo.gen_Unit ON dbo.Inv_Material.Fk_UnitId = dbo.gen_Unit.Pk_Unit INNER JOIN
                      dbo.Stocks ON dbo.Inv_Material.Pk_Material = dbo.Stocks.Fk_Material AND dbo.Stocks.Quantity <= dbo.Inv_Material.Min_Value INNER JOIN
                      dbo.Inv_MaterialCategory ON dbo.Inv_Material.Fk_MaterialCategory = dbo.Inv_MaterialCategory.Pk_MaterialCategory
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[27] 2[14] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 243
               Right = 227
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Unit"
            Begin Extent = 
               Top = 25
               Left = 422
               Bottom = 145
               Right = 582
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Stocks"
            Begin Extent = 
               Top = 0
               Left = 812
               Bottom = 236
               Right = 953
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_MaterialCategory"
            Begin Extent = 
               Top = 6
               Left = 620
               Bottom = 111
               Right = 809
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 135' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VW_Min_Level'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'0
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VW_Min_Level'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VW_Min_Level'
GO
/****** Object:  View [dbo].[Vw_WireCertificate]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_WireCertificate]
AS
SELECT     dbo.Inv_Material.Name, dbo.WireCertificate.Pk_Id, dbo.WireCertificateDetails.Fk_Params, dbo.WireCertificateDetails.Observed, dbo.WireCertificateDetails.Remarks, dbo.WireCertificate.Invno, 
                      dbo.WireCertificate.InvDate, dbo.WireCertificate.Quantity, dbo.WireCertificate.Tested, dbo.WireCertificate.Approved, dbo.WireChar.Name AS Expr1, dbo.WireChar.StdVal, 
                      dbo.gen_Vendor.VendorName, dbo.WireCertificate.Fk_Material, dbo.WireCertificateDetails.Pk_IdDet
FROM         dbo.Inv_Material INNER JOIN
                      dbo.WireCertificate ON dbo.Inv_Material.Pk_Material = dbo.WireCertificate.Fk_Material INNER JOIN
                      dbo.WireCertificateDetails ON dbo.WireCertificate.Pk_Id = dbo.WireCertificateDetails.Fk_ID INNER JOIN
                      dbo.WireChar ON dbo.WireCertificateDetails.Fk_Params = dbo.WireChar.Pk_CharID INNER JOIN
                      dbo.gen_Vendor ON dbo.WireCertificate.Fk_Vendor = dbo.gen_Vendor.Pk_Vendor
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[36] 4[27] 2[10] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 6
               Left = 36
               Bottom = 146
               Right = 227
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "WireCertificate"
            Begin Extent = 
               Top = 0
               Left = 464
               Bottom = 260
               Right = 624
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "WireCertificateDetails"
            Begin Extent = 
               Top = 0
               Left = 777
               Bottom = 183
               Right = 937
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "WireChar"
            Begin Extent = 
               Top = 6
               Left = 1031
               Bottom = 154
               Right = 1191
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Vendor"
            Begin Extent = 
               Top = 194
               Left = 1043
               Bottom = 314
               Right = 1236
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 16
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
    ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_WireCertificate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'     Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 2430
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_WireCertificate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_WireCertificate'
GO
/****** Object:  View [dbo].[Vw_MIndent]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_MIndent]
AS
SELECT     dbo.Inv_MaterialIndentMaster.Pk_MaterialOrderMasterId, dbo.Inv_MaterialIndentMaster.MaterialIndentDate, dbo.Inv_MaterialIndentDetails.RequiredDate, dbo.Inv_MaterialIndentDetails.Quantity, 
                      dbo.Inv_Material.Name, dbo.Inv_MaterialIndentDetails.Fk_Material, dbo.Inv_MaterialIndentDetails.Pk_MaterialOrderDetailsId, dbo.Inv_Material.Pk_Material, dbo.gen_Mill.MillName, 
                      dbo.Inv_MaterialIndentDetails.Fk_CustomerOrder, dbo.gen_Customer.CustomerName, dbo.gen_Color.ColorName, dbo.gen_Order.Cust_PO, dbo.gen_Mill.Pk_Mill, dbo.Inv_Material.GSM, 
                      dbo.Inv_Material.Deckle, dbo.Inv_Material.BF
FROM         dbo.gen_Customer INNER JOIN
                      dbo.gen_Order ON dbo.gen_Customer.Pk_Customer = dbo.gen_Order.Fk_Customer RIGHT OUTER JOIN
                      dbo.gen_Color INNER JOIN
                      dbo.Inv_MaterialIndentDetails INNER JOIN
                      dbo.Inv_MaterialIndentMaster ON dbo.Inv_MaterialIndentDetails.Fk_MaterialOrderMasterId = dbo.Inv_MaterialIndentMaster.Pk_MaterialOrderMasterId INNER JOIN
                      dbo.Inv_Material ON dbo.Inv_MaterialIndentDetails.Fk_Material = dbo.Inv_Material.Pk_Material ON dbo.gen_Color.Pk_Color = dbo.Inv_Material.Fk_Color ON 
                      dbo.gen_Order.Pk_Order = dbo.Inv_MaterialIndentDetails.Fk_CustomerOrder LEFT OUTER JOIN
                      dbo.gen_Mill ON dbo.Inv_Material.Fk_Mill = dbo.gen_Mill.Pk_Mill
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[53] 4[2] 2[12] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 270
               Left = 547
               Bottom = 477
               Right = 732
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Order"
            Begin Extent = 
               Top = 198
               Left = 292
               Bottom = 603
               Right = 465
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Color"
            Begin Extent = 
               Top = 167
               Left = 752
               Bottom = 283
               Right = 912
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_MaterialIndentDetails"
            Begin Extent = 
               Top = 0
               Left = 23
               Bottom = 223
               Right = 254
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_MaterialIndentMaster"
            Begin Extent = 
               Top = 7
               Left = 509
               Bottom = 177
               Right = 714
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 0
               Left = 952
               Bottom = 299
               Right = 1131
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Mill"
            Begin Extent = 
               Top = 36
               Left = 1216
               Bottom = 192
               Right = 139' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_MIndent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'2
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 18
         Width = 284
         Width = 2130
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 795
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1995
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_MIndent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_MIndent'
GO
/****** Object:  View [dbo].[vw_MonthlySalesStatus]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_MonthlySalesStatus]
AS
SELECT        Pk_Invoice, MONTH(InvDate) AS Months, YEAR(InvDate) AS Years, GrandTotal
FROM            dbo.Inv_Billing
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Inv_Billing"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 227
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_MonthlySalesStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_MonthlySalesStatus'
GO
/****** Object:  View [dbo].[Vw_Material]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_Material]
AS
SELECT     dbo.Inv_MaterialIndentDetails.Fk_Material, dbo.gen_Vendor.VendorName, dbo.Inv_Material.Name, dbo.Inv_MaterialIndentDetails.Quantity, dbo.Inv_MaterialIndentMaster.Pk_MaterialOrderMasterId, 
                      dbo.gen_Vendor.Pk_Vendor, dbo.Inv_MaterialIndentDetails.Pk_MaterialOrderDetailsId, dbo.PendingTrack.Quantity - dbo.PendingTrack.QC_Qty AS Pending, dbo.PendingTrack.Pk_IndentTrack, 
                      dbo.PendingTrack.QC_Qty
FROM         dbo.gen_Vendor INNER JOIN
                      dbo.Inv_MaterialIndentMaster ON dbo.gen_Vendor.Pk_Vendor = dbo.Inv_MaterialIndentMaster.Fk_VendorId INNER JOIN
                      dbo.Inv_MaterialIndentDetails ON dbo.Inv_MaterialIndentMaster.Pk_MaterialOrderMasterId = dbo.Inv_MaterialIndentDetails.Fk_MaterialOrderMasterId INNER JOIN
                      dbo.Inv_Material ON dbo.Inv_MaterialIndentDetails.Fk_Material = dbo.Inv_Material.Pk_Material LEFT OUTER JOIN
                      dbo.PendingTrack ON dbo.Inv_MaterialIndentMaster.Pk_MaterialOrderMasterId = dbo.PendingTrack.Fk_Indent AND dbo.Inv_Material.Pk_Material = dbo.PendingTrack.Fk_Material
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[44] 4[22] 2[21] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "gen_Vendor"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 239
               Right = 231
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_MaterialIndentMaster"
            Begin Extent = 
               Top = 6
               Left = 269
               Bottom = 218
               Right = 484
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_MaterialIndentDetails"
            Begin Extent = 
               Top = 20
               Left = 1137
               Bottom = 239
               Right = 1352
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 21
               Left = 556
               Bottom = 215
               Right = 739
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PendingTrack"
            Begin Extent = 
               Top = 17
               Left = 862
               Bottom = 204
               Right = 1034
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 11
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin Criter' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Material'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'iaPane = 
      Begin ColumnWidths = 11
         Column = 4155
         Alias = 900
         Table = 3420
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Material'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Material'
GO
/****** Object:  View [dbo].[Vw_OrdQty]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_OrdQty]
AS
SELECT     dbo.BoxMaster.Name, dbo.BoxType.Name AS BType, dbo.eq_EnquiryChild.Pk_EnquiryChild, dbo.eq_EnquiryChild.Fk_Enquiry, dbo.eq_EnquiryChild.Fk_BoxID, dbo.eq_EnquiryChild.Quantity, 
                      dbo.ItemPartProperty.PName, dbo.ItemPartProperty.Quantity AS PartQty, dbo.ItemPartProperty.Pk_PartPropertyID
FROM         dbo.BoxMaster INNER JOIN
                      dbo.BoxType ON dbo.BoxMaster.Fk_BoxType = dbo.BoxType.Pk_BoxTypeID INNER JOIN
                      dbo.eq_EnquiryChild ON dbo.BoxMaster.Pk_BoxID = dbo.eq_EnquiryChild.Fk_BoxID INNER JOIN
                      dbo.BoxSpecs ON dbo.BoxMaster.Pk_BoxID = dbo.BoxSpecs.Fk_BoxID INNER JOIN
                      dbo.ItemPartProperty ON dbo.BoxSpecs.Pk_BoxSpecID = dbo.ItemPartProperty.Fk_BoxSpecsID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[42] 4[20] 2[13] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 126
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 3
         End
         Begin Table = "BoxType"
            Begin Extent = 
               Top = 187
               Left = 262
               Bottom = 277
               Right = 422
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "eq_EnquiryChild"
            Begin Extent = 
               Top = 145
               Left = 693
               Bottom = 347
               Right = 895
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxSpecs"
            Begin Extent = 
               Top = 0
               Left = 378
               Bottom = 120
               Right = 561
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ItemPartProperty"
            Begin Extent = 
               Top = 1
               Left = 955
               Bottom = 343
               Right = 1164
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         A' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_OrdQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'lias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_OrdQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_OrdQty'
GO
/****** Object:  View [dbo].[Vw_PO]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_PO]
AS
SELECT     dbo.Inv_Material.Pk_Material, dbo.Inv_Material.Name, dbo.PurchaseOrderD.Quantity, dbo.PurchaseOrderM.Pk_PONo, dbo.PurchaseOrderD.Rate, dbo.PurchaseOrderD.Amount, 
                      dbo.PurchaseOrderM.Fk_Status, dbo.PurchaseOrderD.InwdQty, dbo.gen_Vendor.VendorName, dbo.Inv_Material.GSM, dbo.Inv_Material.BF, dbo.gen_Vendor.Pk_Vendor, 
                      dbo.PurchaseOrderM.PODate, dbo.gen_Color.ColorName, dbo.gen_Mill.MillName, dbo.PurchaseOrderD.Fk_IndentVal, dbo.gen_Mill.Pk_Mill, dbo.Inv_Material.Deckle
FROM         dbo.PurchaseOrderM INNER JOIN
                      dbo.PurchaseOrderD ON dbo.PurchaseOrderM.Pk_PONo = dbo.PurchaseOrderD.Fk_PONo INNER JOIN
                      dbo.Inv_Material ON dbo.PurchaseOrderD.Fk_Material = dbo.Inv_Material.Pk_Material INNER JOIN
                      dbo.gen_Vendor ON dbo.PurchaseOrderM.Fk_Vendor = dbo.gen_Vendor.Pk_Vendor INNER JOIN
                      dbo.gen_Color ON dbo.Inv_Material.Fk_Color = dbo.gen_Color.Pk_Color INNER JOIN
                      dbo.gen_Mill ON dbo.Inv_Material.Fk_Mill = dbo.gen_Mill.Pk_Mill
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[42] 4[13] 2[6] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "PurchaseOrderM"
            Begin Extent = 
               Top = 23
               Left = 60
               Bottom = 277
               Right = 271
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PurchaseOrderD"
            Begin Extent = 
               Top = 141
               Left = 393
               Bottom = 451
               Right = 609
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 7
               Left = 771
               Bottom = 249
               Right = 972
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Vendor"
            Begin Extent = 
               Top = 16
               Left = 1040
               Bottom = 136
               Right = 1233
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Color"
            Begin Extent = 
               Top = 6
               Left = 309
               Bottom = 96
               Right = 469
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Mill"
            Begin Extent = 
               Top = 6
               Left = 507
               Bottom = 126
               Right = 673
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 18
         Width = 284
         Width = 1500
     ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'    Width = 5535
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 4560
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 3675
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PO'
GO
/****** Object:  View [dbo].[Vw_PendingPO]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_PendingPO]
AS
SELECT     dbo.PurchaseOrderM.Pk_PONo, dbo.Inv_MaterialIndentDetails.Fk_Material, dbo.Inv_MaterialIndentMaster.Pk_MaterialOrderMasterId, dbo.Inv_MaterialIndentDetails.Pk_MaterialOrderDetailsId, 
                      dbo.PurchaseOrderD.Quantity
FROM         dbo.PurchaseOrderM INNER JOIN
                      dbo.PurchaseOrderD ON dbo.PurchaseOrderM.Pk_PONo = dbo.PurchaseOrderD.Fk_PONo INNER JOIN
                      dbo.Inv_MaterialIndentMaster ON dbo.PurchaseOrderM.Fk_Indent = dbo.Inv_MaterialIndentMaster.Pk_MaterialOrderMasterId RIGHT OUTER JOIN
                      dbo.Inv_MaterialIndentDetails ON dbo.Inv_MaterialIndentMaster.Pk_MaterialOrderMasterId = dbo.Inv_MaterialIndentDetails.Fk_MaterialOrderMasterId AND 
                      dbo.PurchaseOrderD.Fk_Material = dbo.Inv_MaterialIndentDetails.Fk_Material
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[48] 4[13] 2[12] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "PurchaseOrderM"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 293
               Right = 264
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PurchaseOrderD"
            Begin Extent = 
               Top = 0
               Left = 510
               Bottom = 176
               Right = 761
            End
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "Inv_MaterialIndentMaster"
            Begin Extent = 
               Top = 177
               Left = 397
               Bottom = 451
               Right = 625
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "Inv_MaterialIndentDetails"
            Begin Extent = 
               Top = 6
               Left = 985
               Bottom = 222
               Right = 1272
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 2895
         Width = 1995
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
    ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PendingPO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'     Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PendingPO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PendingPO'
GO
/****** Object:  View [dbo].[Vw_POReport]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_POReport]
AS
SELECT     dbo.Inv_Material.Pk_Material, dbo.Inv_Material.Name, dbo.PurchaseOrderD.Quantity, dbo.PurchaseOrderM.Pk_PONo, dbo.PurchaseOrderM.PODate, dbo.PurchaseOrderD.Rate, 
                      dbo.PurchaseOrderD.Amount, dbo.gen_Vendor.VendorName, dbo.PurchaseOrderM.GrandTotal, dbo.PurchaseOrderM.NetValue, dbo.PurchaseOrderM.TaxType, dbo.PurchaseOrderM.Fk_Indent, 
                      dbo.gen_Mill.MillName, dbo.gen_Color.ColorName, dbo.PurchaseOrderD.Fk_IndentVal, dbo.gen_Vendor.Email, dbo.gen_Vendor.Address_No_Street, dbo.gen_Vendor.PinCode, 
                      dbo.gen_Vendor.LandLine, dbo.gen_Vendor.MobileNumber
FROM         dbo.PurchaseOrderM INNER JOIN
                      dbo.PurchaseOrderD ON dbo.PurchaseOrderM.Pk_PONo = dbo.PurchaseOrderD.Fk_PONo INNER JOIN
                      dbo.Inv_Material ON dbo.PurchaseOrderD.Fk_Material = dbo.Inv_Material.Pk_Material INNER JOIN
                      dbo.gen_Vendor ON dbo.PurchaseOrderM.Fk_Vendor = dbo.gen_Vendor.Pk_Vendor INNER JOIN
                      dbo.gen_Color ON dbo.Inv_Material.Fk_Color = dbo.gen_Color.Pk_Color INNER JOIN
                      dbo.gen_Mill ON dbo.Inv_Material.Fk_Mill = dbo.gen_Mill.Pk_Mill
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "PurchaseOrderM"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 269
               Right = 189
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "PurchaseOrderD"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 196
               Right = 387
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 0
               Left = 478
               Bottom = 252
               Right = 667
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Vendor"
            Begin Extent = 
               Top = 22
               Left = 709
               Bottom = 247
               Right = 902
            End
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "gen_Color"
            Begin Extent = 
               Top = 6
               Left = 940
               Bottom = 96
               Right = 1100
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Mill"
            Begin Extent = 
               Top = 96
               Left = 940
               Bottom = 216
               Right = 1106
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 13
         Width = 284
         Width = 1500
      ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_POReport'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'   Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_POReport'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_POReport'
GO
/****** Object:  View [dbo].[Vw_PODet]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_PODet]
AS
SELECT     dbo.PurchaseOrderM.PODate, dbo.PurchaseOrderM.Fk_Indent, dbo.PurchaseOrderM.Pk_PONo, dbo.gen_Vendor.VendorName, dbo.PaperCertificate.Pk_Id, dbo.Inv_Material.Name
FROM         dbo.PurchaseOrderM INNER JOIN
                      dbo.gen_Vendor ON dbo.PurchaseOrderM.Fk_Vendor = dbo.gen_Vendor.Pk_Vendor INNER JOIN
                      dbo.PaperCertificate ON dbo.PurchaseOrderM.Pk_PONo = dbo.PaperCertificate.Fk_PoNo AND dbo.gen_Vendor.Pk_Vendor = dbo.PaperCertificate.Fk_Vendor LEFT OUTER JOIN
                      dbo.Inv_Material ON dbo.PaperCertificate.Fk_Material = dbo.Inv_Material.Pk_Material
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[8] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "PurchaseOrderM"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 126
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Vendor"
            Begin Extent = 
               Top = 0
               Left = 808
               Bottom = 228
               Right = 1001
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PaperCertificate"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 269
               Right = 187
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 125
               Left = 472
               Bottom = 308
               Right = 661
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PODet'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PODet'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PODet'
GO
/****** Object:  View [dbo].[Vw_Quotation]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_Quotation]
AS
SELECT     dbo.BoxMaster.Name, dbo.gen_Customer.CustomerName, dbo.QuotationDetails.Quantity, dbo.QuotationMaster.Pk_Quotation, dbo.QuotationMaster.QuotationDate, 
                      dbo.gen_Customer.CustomerType, dbo.gen_Customer.CustomerAddress, dbo.gen_Customer.LandLine, dbo.gen_Customer.Email, dbo.gen_State.StateName, dbo.gen_Country.CountryName, 
                      dbo.QuotationDetails.BoxID, dbo.QuotationDetails.BName, dbo.QuotationDetails.Dimension, dbo.QuotationDetails.Fk_EstimationID, dbo.QuotationMaster.Fk_Enquiry, 
                      dbo.gen_Customer.ContactPerson, dbo.QuotationDetails.CostPerBox, dbo.ItemPartProperty.BoardBS, dbo.BoxMaster.Description, dbo.QuotationMaster.Terms, dbo.ItemPartProperty.PName
FROM         dbo.gen_State INNER JOIN
                      dbo.gen_Customer ON dbo.gen_State.Pk_StateID = dbo.gen_Customer.Fk_State INNER JOIN
                      dbo.gen_Country ON dbo.gen_Customer.Fk_Country = dbo.gen_Country.Pk_Country AND dbo.gen_State.Country = dbo.gen_Country.Pk_Country INNER JOIN
                      dbo.eq_Enquiry ON dbo.gen_Customer.Pk_Customer = dbo.eq_Enquiry.Customer INNER JOIN
                      dbo.BoxMaster INNER JOIN
                      dbo.QuotationDetails ON dbo.BoxMaster.Pk_BoxID = dbo.QuotationDetails.BoxID INNER JOIN
                      dbo.QuotationMaster ON dbo.QuotationDetails.Fk_QuotationID = dbo.QuotationMaster.Pk_Quotation ON dbo.eq_Enquiry.Pk_Enquiry = dbo.QuotationMaster.Fk_Enquiry INNER JOIN
                      dbo.eq_EnquiryChild ON dbo.eq_Enquiry.Pk_Enquiry = dbo.eq_EnquiryChild.Fk_Enquiry AND dbo.BoxMaster.Pk_BoxID = dbo.eq_EnquiryChild.Fk_BoxID INNER JOIN
                      dbo.BoxSpecs ON dbo.BoxMaster.Pk_BoxID = dbo.BoxSpecs.Fk_BoxID INNER JOIN
                      dbo.ItemPartProperty ON dbo.BoxSpecs.Pk_BoxSpecID = dbo.ItemPartProperty.Fk_BoxSpecsID
WHERE     (dbo.ItemPartProperty.PName = N'Outer Shell')
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[25] 4[38] 2[8] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -288
         Left = 0
      End
      Begin Tables = 
         Begin Table = "gen_State"
            Begin Extent = 
               Top = 0
               Left = 1033
               Bottom = 105
               Right = 1193
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 9
               Left = 781
               Bottom = 228
               Right = 955
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Country"
            Begin Extent = 
               Top = 135
               Left = 1141
               Bottom = 225
               Right = 1301
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "eq_Enquiry"
            Begin Extent = 
               Top = 207
               Left = 161
               Bottom = 327
               Right = 349
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 28
               Left = 0
               Bottom = 225
               Right = 160
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "QuotationDetails"
            Begin Extent = 
               Top = 0
               Left = 308
               Bottom = 208
               Right = 487
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "QuotationMaster"
            Begin Extent = 
               Top = 0
               Left = 540
               Bottom = 207
               Right = 700
            E' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Quotation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'nd
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "eq_EnquiryChild"
            Begin Extent = 
               Top = 249
               Left = 897
               Bottom = 369
               Right = 1062
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxSpecs"
            Begin Extent = 
               Top = 210
               Left = 387
               Bottom = 330
               Right = 570
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ItemPartProperty"
            Begin Extent = 
               Top = 228
               Left = 608
               Bottom = 383
               Right = 787
            End
            DisplayFlags = 280
            TopColumn = 1
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 23
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Quotation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Quotation'
GO
/****** Object:  View [dbo].[Vw_EstDet1]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_EstDet1]
AS
SELECT     dbo.eq_EnquiryChild.Fk_BoxID, dbo.est_BoxEstimationChild.Fk_EnquiryChild, dbo.est_BoxEstimationChild.Pk_BoxEstimationChild, dbo.est_BoxEstimationChild.Fk_BoxEstimation, 
                      dbo.BoxMaster.Name
FROM         dbo.eq_EnquiryChild INNER JOIN
                      dbo.est_BoxEstimationChild ON dbo.eq_EnquiryChild.Pk_EnquiryChild = dbo.est_BoxEstimationChild.Fk_EnquiryChild INNER JOIN
                      dbo.BoxMaster ON dbo.eq_EnquiryChild.Fk_BoxID = dbo.BoxMaster.Pk_BoxID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[31] 4[4] 2[54] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "eq_EnquiryChild"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 212
               Right = 240
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "est_BoxEstimationChild"
            Begin Extent = 
               Top = 6
               Left = 278
               Bottom = 165
               Right = 480
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 6
               Left = 518
               Bottom = 126
               Right = 678
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_EstDet1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_EstDet1'
GO
/****** Object:  View [dbo].[Vw_Est_Det]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_Est_Det]
AS
SELECT DISTINCT 
                      dbo.est_BoxEstimation.Pk_BoxEstimation, dbo.gen_Customer.CustomerName, dbo.est_BoxEstimation.Date AS EstDATE, dbo.est_BoxEstimation.GrandTotal, dbo.eq_EnquiryChild.Fk_Enquiry, 
                      dbo.eq_EnquiryChild.Quantity, dbo.BoxMaster.Pk_BoxID, dbo.BoxMaster.Name, dbo.ItemPartProperty.Length, dbo.ItemPartProperty.Width, dbo.ItemPartProperty.Height, 
                      dbo.ItemPartProperty.BoardBS, dbo.ItemPartProperty.PName
FROM         dbo.eq_EnquiryChild INNER JOIN
                      dbo.est_BoxEstimationChild ON dbo.eq_EnquiryChild.Pk_EnquiryChild = dbo.est_BoxEstimationChild.Fk_EnquiryChild INNER JOIN
                      dbo.gen_Customer INNER JOIN
                      dbo.eq_Enquiry ON dbo.gen_Customer.Pk_Customer = dbo.eq_Enquiry.Customer ON dbo.eq_EnquiryChild.Fk_Enquiry = dbo.eq_Enquiry.Pk_Enquiry INNER JOIN
                      dbo.BoxMaster ON dbo.eq_EnquiryChild.Fk_BoxID = dbo.BoxMaster.Pk_BoxID INNER JOIN
                      dbo.BoxSpecs ON dbo.BoxMaster.Pk_BoxID = dbo.BoxSpecs.Fk_BoxID INNER JOIN
                      dbo.ItemPartProperty ON dbo.BoxSpecs.Pk_BoxSpecID = dbo.ItemPartProperty.Fk_BoxSpecsID RIGHT OUTER JOIN
                      dbo.est_BoxEstimation ON dbo.est_BoxEstimationChild.Fk_BoxEstimation = dbo.est_BoxEstimation.Pk_BoxEstimation
WHERE     (dbo.ItemPartProperty.PName = N'Outer Shell')
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[35] 4[27] 2[4] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -288
         Left = 0
      End
      Begin Tables = 
         Begin Table = "eq_EnquiryChild"
            Begin Extent = 
               Top = 14
               Left = 1027
               Bottom = 284
               Right = 1233
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "est_BoxEstimationChild"
            Begin Extent = 
               Top = 124
               Left = 644
               Bottom = 574
               Right = 949
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 354
               Right = 200
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "eq_Enquiry"
            Begin Extent = 
               Top = 0
               Left = 1307
               Bottom = 348
               Right = 1512
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 8
               Left = 648
               Bottom = 128
               Right = 808
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxSpecs"
            Begin Extent = 
               Top = 5
               Left = 362
               Bottom = 125
               Right = 545
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ItemPartProperty"
            Begin Extent = 
               Top = 295
               Left = 1015
               Bottom = 633
               Right = 1194
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Est_Det'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "est_BoxEstimation"
            Begin Extent = 
               Top = 152
               Left = 233
               Bottom = 354
               Right = 443
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 14
         Width = 284
         Width = 1500
         Width = 1500
         Width = 2835
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Est_Det'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Est_Det'
GO
/****** Object:  View [dbo].[Vw_InkCertificate]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_InkCertificate]
AS
SELECT     dbo.InkCertificateDetails.Pk_IdDet, dbo.InkCertificateDetails.Fk_Params, dbo.InkCertificateDetails.Spec_Target, dbo.InkCertificateDetails.Spec_Tolerance, dbo.InkCertificateDetails.Results, 
                      dbo.InkChar.Name, dbo.Inv_Material.Name AS Expr1, dbo.InkCertificate.Fk_Material, dbo.InkCertificate.Pk_Id, dbo.InkCertificate.BatchNo, dbo.InkCertificate.Description, dbo.InkCertificate.Quantity, 
                      dbo.InkCertificate.Tested, dbo.InkCertificate.Approved
FROM         dbo.InkCertificate INNER JOIN
                      dbo.InkCertificateDetails ON dbo.InkCertificate.Pk_Id = dbo.InkCertificateDetails.Fk_ID INNER JOIN
                      dbo.InkChar ON dbo.InkCertificateDetails.Fk_Params = dbo.InkChar.Pk_CharID INNER JOIN
                      dbo.Inv_Material ON dbo.InkCertificate.Fk_Material = dbo.Inv_Material.Pk_Material
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[9] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "InkCertificate"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 247
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "InkCertificateDetails"
            Begin Extent = 
               Top = 49
               Left = 300
               Bottom = 260
               Right = 528
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "InkChar"
            Begin Extent = 
               Top = 116
               Left = 617
               Bottom = 257
               Right = 776
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 0
               Left = 931
               Bottom = 120
               Right = 1120
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
       ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_InkCertificate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'  Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_InkCertificate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_InkCertificate'
GO
/****** Object:  View [dbo].[Vw_BoxSpec]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_BoxSpec]
AS
SELECT     dbo.BoxMaster.Name, dbo.ItemPartProperty.Length, dbo.ItemPartProperty.Width, dbo.ItemPartProperty.Height, dbo.ItemPartProperty.Weight, dbo.ItemPartProperty.Pk_PartPropertyID, 
                      dbo.BoxMaster.Pk_BoxID, dbo.BoxType.Name AS BType, dbo.ItemPartProperty.PName, dbo.BoxSpecs.Pk_BoxSpecID, dbo.gen_Customer.CustomerName, dbo.ItemPartProperty.Quantity
FROM         dbo.BoxMaster INNER JOIN
                      dbo.BoxSpecs ON dbo.BoxMaster.Pk_BoxID = dbo.BoxSpecs.Fk_BoxID INNER JOIN
                      dbo.ItemPartProperty ON dbo.BoxSpecs.Pk_BoxSpecID = dbo.ItemPartProperty.Fk_BoxSpecsID INNER JOIN
                      dbo.BoxType ON dbo.BoxMaster.Fk_BoxType = dbo.BoxType.Pk_BoxTypeID INNER JOIN
                      dbo.gen_Customer ON dbo.BoxMaster.Customer = dbo.gen_Customer.Pk_Customer
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 187
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxSpecs"
            Begin Extent = 
               Top = 5
               Left = 357
               Bottom = 269
               Right = 540
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ItemPartProperty"
            Begin Extent = 
               Top = 0
               Left = 737
               Bottom = 263
               Right = 916
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxType"
            Begin Extent = 
               Top = 6
               Left = 1084
               Bottom = 149
               Right = 1244
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 150
               Left = 954
               Bottom = 270
               Right = 1128
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 24
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 150' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_BoxSpec'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'0
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_BoxSpec'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_BoxSpec'
GO
/****** Object:  View [dbo].[Vw_IndentCertificate]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_IndentCertificate]
AS
SELECT     dbo.Inv_MaterialIndentMaster.Pk_MaterialOrderMasterId, dbo.Inv_MaterialIndentMaster.MaterialIndentDate, dbo.Inv_MaterialIndentDetails.RequiredDate, dbo.Inv_MaterialIndentDetails.Quantity, 
                      dbo.Inv_Material.Name, dbo.Inv_MaterialIndentDetails.Fk_Material, dbo.Inv_MaterialIndentDetails.Pk_MaterialOrderDetailsId, dbo.Inv_Material.Pk_Material, dbo.gen_Mill.MillName, 
                      dbo.Inv_MaterialIndentDetails.Fk_CustomerOrder, dbo.gen_Customer.CustomerName, dbo.gen_Color.ColorName, dbo.gen_Order.Cust_PO, dbo.gen_Mill.Pk_Mill
FROM         dbo.PaperCertificate INNER JOIN
                      dbo.gen_Color INNER JOIN
                      dbo.Inv_MaterialIndentDetails INNER JOIN
                      dbo.Inv_MaterialIndentMaster ON dbo.Inv_MaterialIndentDetails.Fk_MaterialOrderMasterId = dbo.Inv_MaterialIndentMaster.Pk_MaterialOrderMasterId INNER JOIN
                      dbo.Inv_Material ON dbo.Inv_MaterialIndentDetails.Fk_Material = dbo.Inv_Material.Pk_Material ON dbo.gen_Color.Pk_Color = dbo.Inv_Material.Fk_Color ON 
                      dbo.PaperCertificate.Fk_Material = dbo.Inv_Material.Pk_Material LEFT OUTER JOIN
                      dbo.gen_Customer INNER JOIN
                      dbo.gen_Order ON dbo.gen_Customer.Pk_Customer = dbo.gen_Order.Fk_Customer ON dbo.Inv_MaterialIndentDetails.Fk_CustomerOrder = dbo.gen_Order.Pk_Order LEFT OUTER JOIN
                      dbo.gen_Mill ON dbo.Inv_Material.Fk_Mill = dbo.gen_Mill.Pk_Mill
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[31] 4[16] 2[16] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 126
               Right = 212
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Order"
            Begin Extent = 
               Top = 6
               Left = 250
               Bottom = 126
               Right = 431
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Color"
            Begin Extent = 
               Top = 6
               Left = 469
               Bottom = 96
               Right = 629
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_MaterialIndentDetails"
            Begin Extent = 
               Top = 6
               Left = 667
               Bottom = 126
               Right = 882
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_MaterialIndentMaster"
            Begin Extent = 
               Top = 6
               Left = 920
               Bottom = 126
               Right = 1135
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 165
               Left = 416
               Bottom = 285
               Right = 605
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Mill"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 246
               Right = 204
   ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_IndentCertificate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'         End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PaperCertificate"
            Begin Extent = 
               Top = 82
               Left = 1160
               Bottom = 298
               Right = 1320
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 15
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_IndentCertificate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_IndentCertificate'
GO
/****** Object:  View [dbo].[Vw_Indent]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_Indent]
AS
SELECT     dbo.Inv_Material.Name, dbo.gen_Unit.UnitName, dbo.Inv_MaterialIndentMaster.MaterialIndentDate, dbo.gen_Vendor.VendorName, dbo.gen_Vendor.Pk_Vendor, 
                      dbo.Inv_MaterialIndentMaster.Pk_MaterialOrderMasterId, dbo.Inv_Material.Pk_Material, dbo.Inv_MaterialIndentDetails.Quantity, dbo.Inv_MaterialIndentDetails.Fk_CustomerOrder, 
                      dbo.Inv_MaterialIndentDetails.Pk_MaterialOrderDetailsId, dbo.Inv_MaterialIndentMaster.Fk_Tanent
FROM         dbo.Inv_MaterialIndentMaster INNER JOIN
                      dbo.gen_Vendor ON dbo.Inv_MaterialIndentMaster.Fk_VendorId = dbo.gen_Vendor.Pk_Vendor INNER JOIN
                      dbo.Inv_MaterialIndentDetails ON dbo.Inv_MaterialIndentMaster.Pk_MaterialOrderMasterId = dbo.Inv_MaterialIndentDetails.Fk_MaterialOrderMasterId INNER JOIN
                      dbo.Inv_Material ON dbo.Inv_MaterialIndentDetails.Fk_Material = dbo.Inv_Material.Pk_Material INNER JOIN
                      dbo.gen_Unit ON dbo.Inv_Material.Fk_UnitId = dbo.gen_Unit.Pk_Unit
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[53] 4[8] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Inv_MaterialIndentMaster"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 232
               Right = 305
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Vendor"
            Begin Extent = 
               Top = 205
               Left = 405
               Bottom = 442
               Right = 611
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_MaterialIndentDetails"
            Begin Extent = 
               Top = 317
               Left = 654
               Bottom = 515
               Right = 893
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 0
               Left = 1011
               Bottom = 305
               Right = 1200
            End
            DisplayFlags = 280
            TopColumn = 10
         End
         Begin Table = "gen_Unit"
            Begin Extent = 
               Top = 6
               Left = 522
               Bottom = 158
               Right = 673
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 1500
         Width = 1500
         Width = 3030
         Width = 1500
         Width = 1500
         Width = 2550
         Width = 1500
         Width = 75
         Width = 75
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWi' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Indent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'dths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Indent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Indent'
GO
/****** Object:  View [dbo].[Vw_GlueCertificate]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_GlueCertificate]
AS
SELECT     dbo.GlueCertificateDetails.Pk_IdDet, dbo.GlueCertificateDetails.PCode, dbo.GlueCertificateDetails.MixingRatio, dbo.GlueCertificateDetails.GelTemp, dbo.GlueCertificateDetails.Specification1, 
                      dbo.GlueCertificateDetails.Specification2, dbo.GlueCertificateDetails.Results1, dbo.GlueCertificateDetails.Results2, dbo.Inv_Material.Name, dbo.gen_Vendor.VendorName, dbo.GlueCertificate.Invno, 
                      dbo.GlueCertificate.Fk_Material, dbo.GlueCertificate.InvDate, dbo.GlueCertificate.Quantity, dbo.GlueCertificate.Tested, dbo.GlueCertificate.Approved, dbo.GlueCertificate.Fk_Vendor, 
                      dbo.GlueCertificate.Pk_Id
FROM         dbo.Inv_Material INNER JOIN
                      dbo.GlueCertificate ON dbo.Inv_Material.Pk_Material = dbo.GlueCertificate.Fk_Material INNER JOIN
                      dbo.gen_Vendor ON dbo.GlueCertificate.Fk_Vendor = dbo.gen_Vendor.Pk_Vendor INNER JOIN
                      dbo.GlueCertificateDetails ON dbo.GlueCertificate.Pk_Id = dbo.GlueCertificateDetails.Fk_PkID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[33] 2[9] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 126
               Right = 227
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Vendor"
            Begin Extent = 
               Top = 120
               Left = 299
               Bottom = 240
               Right = 492
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "GlueCertificate"
            Begin Extent = 
               Top = 5
               Left = 605
               Bottom = 250
               Right = 791
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "GlueCertificateDetails"
            Begin Extent = 
               Top = 7
               Left = 949
               Bottom = 269
               Right = 1183
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 19
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         T' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_GlueCertificate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'able = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_GlueCertificate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_GlueCertificate'
GO
/****** Object:  View [dbo].[Vw_GetPaperWtSum]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_GetPaperWtSum]
AS
SELECT     SUM(dbo.Items_Layers.Weight) AS SWt, dbo.Inv_Material.Pk_Material, dbo.BoxMaster.Pk_BoxID, dbo.Inv_Material.Name, dbo.gen_Mill.MillName, dbo.ItemPartProperty.PName
FROM         dbo.BoxMaster INNER JOIN
                      dbo.BoxSpecs ON dbo.BoxMaster.Pk_BoxID = dbo.BoxSpecs.Fk_BoxID INNER JOIN
                      dbo.ItemPartProperty ON dbo.BoxSpecs.Pk_BoxSpecID = dbo.ItemPartProperty.Fk_BoxSpecsID INNER JOIN
                      dbo.Items_Layers ON dbo.ItemPartProperty.Pk_PartPropertyID = dbo.Items_Layers.Fk_PartId INNER JOIN
                      dbo.Inv_Material ON dbo.Items_Layers.Fk_Material = dbo.Inv_Material.Pk_Material INNER JOIN
                      dbo.gen_Mill ON dbo.Inv_Material.Fk_Mill = dbo.gen_Mill.Pk_Mill
GROUP BY dbo.Inv_Material.Pk_Material, dbo.BoxMaster.Pk_BoxID, dbo.Inv_Material.Name, dbo.gen_Mill.MillName, dbo.ItemPartProperty.PName
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[10] 2[17] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 126
               Right = 214
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxSpecs"
            Begin Extent = 
               Top = 6
               Left = 252
               Bottom = 126
               Right = 451
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ItemPartProperty"
            Begin Extent = 
               Top = 6
               Left = 490
               Bottom = 168
               Right = 684
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Items_Layers"
            Begin Extent = 
               Top = 6
               Left = 722
               Bottom = 126
               Right = 898
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 6
               Left = 936
               Bottom = 126
               Right = 1141
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Mill"
            Begin Extent = 
               Top = 177
               Left = 767
               Bottom = 297
               Right = 949
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 11
         Width = 284
         Width = 1500
        ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_GetPaperWtSum'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N' Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_GetPaperWtSum'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_GetPaperWtSum'
GO
/****** Object:  View [dbo].[Vw_DeliveryScheduleReport]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_DeliveryScheduleReport]
AS
SELECT DISTINCT 
                      TOP (100) PERCENT dbo.gen_DeliverySchedule.Pk_DeliverySechedule, dbo.gen_DeliverySchedule.DeliveryDate, dbo.gen_Order.Pk_Order, dbo.gen_Order.OrderDate, dbo.BoxMaster.Name, 
                      dbo.gen_Customer.CustomerName, dbo.gen_DeliverySchedule.Quantity, dbo.gen_DeliverySchedule.OrdQty, dbo.BoxMaster.PartNo, dbo.BoxType.Name AS Expr1, 
                      dbo.gen_DeliverySchedule.Fk_BoxID, dbo.gen_DeliverySchedule.Fk_PartID, dbo.ItemPartProperty.PName
FROM         dbo.BoxType INNER JOIN
                      dbo.gen_Customer INNER JOIN
                      dbo.BoxMaster ON dbo.gen_Customer.Pk_Customer = dbo.BoxMaster.Customer ON dbo.BoxType.Pk_BoxTypeID = dbo.BoxMaster.Fk_BoxType INNER JOIN
                      dbo.gen_DeliverySchedule INNER JOIN
                      dbo.gen_Order ON dbo.gen_DeliverySchedule.Fk_Order = dbo.gen_Order.Pk_Order ON dbo.BoxMaster.Pk_BoxID = dbo.gen_DeliverySchedule.Fk_BoxID INNER JOIN
                      dbo.ItemPartProperty ON dbo.gen_DeliverySchedule.Fk_PartID = dbo.ItemPartProperty.Pk_PartPropertyID
ORDER BY dbo.gen_DeliverySchedule.DeliveryDate
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[38] 4[17] 2[11] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "BoxType"
            Begin Extent = 
               Top = 0
               Left = 1026
               Bottom = 90
               Right = 1186
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 110
               Left = 1007
               Bottom = 230
               Right = 1181
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 0
               Left = 360
               Bottom = 194
               Right = 525
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_DeliverySchedule"
            Begin Extent = 
               Top = 4
               Left = 684
               Bottom = 232
               Right = 869
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Order"
            Begin Extent = 
               Top = 118
               Left = 109
               Bottom = 347
               Right = 272
            End
            DisplayFlags = 280
            TopColumn = 4
         End
         Begin Table = "ItemPartProperty"
            Begin Extent = 
               Top = 0
               Left = 43
               Bottom = 120
               Right = 222
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 14
         Width = 284
         Width = 15' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_DeliveryScheduleReport'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'00
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 3165
         Width = 1500
         Width = 1500
         Width = 2730
         Width = 75
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_DeliveryScheduleReport'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_DeliveryScheduleReport'
GO
/****** Object:  View [dbo].[Vw_BoxStock]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_BoxStock]
AS
SELECT     dbo.BoxMaster.Pk_BoxID, dbo.BoxMaster.Name, dbo.BoxStock.Pk_StockID, dbo.BoxStock.Quantity, dbo.BoxMaster.PartNo, dbo.BoxType.Name AS Expr1, dbo.gen_Customer.CustomerName, 
                      dbo.ItemPartProperty.PName, dbo.ItemPartProperty.Pk_PartPropertyID
FROM         dbo.BoxStock INNER JOIN
                      dbo.BoxMaster ON dbo.BoxStock.Fk_BoxID = dbo.BoxMaster.Pk_BoxID INNER JOIN
                      dbo.BoxType ON dbo.BoxMaster.Fk_BoxType = dbo.BoxType.Pk_BoxTypeID INNER JOIN
                      dbo.gen_Customer ON dbo.BoxMaster.Customer = dbo.gen_Customer.Pk_Customer INNER JOIN
                      dbo.ItemPartProperty ON dbo.BoxStock.Fk_PartID = dbo.ItemPartProperty.Pk_PartPropertyID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[9] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "BoxStock"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 206
               Right = 162
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 5
               Left = 273
               Bottom = 222
               Right = 402
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxType"
            Begin Extent = 
               Top = 0
               Left = 745
               Bottom = 210
               Right = 895
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 80
               Left = 496
               Bottom = 200
               Right = 670
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ItemPartProperty"
            Begin Extent = 
               Top = 6
               Left = 940
               Bottom = 238
               Right = 1119
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1230
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column =' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_BoxStock'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N' 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_BoxStock'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_BoxStock'
GO
/****** Object:  View [dbo].[Vw_BoxScheduleDet]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_BoxScheduleDet]
AS
SELECT     dbo.gen_DeliverySchedule.Pk_DeliverySechedule, dbo.BoxMaster.Name, dbo.ItemPartProperty.PName, dbo.ItemPartProperty.Deckle, dbo.ItemPartProperty.CuttingSize, dbo.Items_Layers.GSM, 
                      dbo.Items_Layers.BF, dbo.Items_Layers.Weight, dbo.ItemPartProperty.Pk_PartPropertyID, dbo.ItemPartProperty.PlyVal, dbo.Items_Layers.Pk_LayerID, dbo.ItemPartProperty.Length, 
                      dbo.ItemPartProperty.Width, dbo.ItemPartProperty.Height
FROM         dbo.gen_DeliverySchedule INNER JOIN
                      dbo.BoxMaster ON dbo.gen_DeliverySchedule.Fk_BoxID = dbo.BoxMaster.Pk_BoxID INNER JOIN
                      dbo.BoxSpecs ON dbo.BoxMaster.Pk_BoxID = dbo.BoxSpecs.Fk_BoxID INNER JOIN
                      dbo.ItemPartProperty ON dbo.gen_DeliverySchedule.Fk_PartID = dbo.ItemPartProperty.Pk_PartPropertyID AND dbo.BoxSpecs.Pk_BoxSpecID = dbo.ItemPartProperty.Fk_BoxSpecsID INNER JOIN
                      dbo.Items_Layers ON dbo.ItemPartProperty.Pk_PartPropertyID = dbo.Items_Layers.Fk_PartId
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[16] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "gen_DeliverySchedule"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 269
               Right = 232
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 128
               Left = 547
               Bottom = 248
               Right = 707
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxSpecs"
            Begin Extent = 
               Top = 0
               Left = 318
               Bottom = 269
               Right = 501
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ItemPartProperty"
            Begin Extent = 
               Top = 6
               Left = 689
               Bottom = 269
               Right = 868
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Items_Layers"
            Begin Extent = 
               Top = 6
               Left = 906
               Bottom = 259
               Right = 1066
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 12
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin Crit' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_BoxScheduleDet'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'eriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_BoxScheduleDet'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_BoxScheduleDet'
GO
/****** Object:  View [dbo].[Vw_InwardRep]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_InwardRep]
AS
SELECT     dbo.MaterialInwardM.Pk_Inward, dbo.Inv_Material.Pk_Material, dbo.Inv_Material.Name, dbo.MaterialInwardD.Quantity, dbo.MaterialInwardM.Inward_Date, dbo.MaterialInwardM.Fk_QC, 
                      dbo.MaterialInwardM.Fk_Indent, dbo.MaterialInwardM.PONo, dbo.MaterialInwardD.RollNo, dbo.MaterialInwardD.Pk_InwardDet, dbo.gen_Color.ColorName, dbo.gen_Mill.MillName, 
                      dbo.gen_Mill.Pk_Mill
FROM         dbo.MaterialInwardM INNER JOIN
                      dbo.MaterialInwardD ON dbo.MaterialInwardM.Pk_Inward = dbo.MaterialInwardD.Fk_Inward INNER JOIN
                      dbo.Inv_Material ON dbo.MaterialInwardD.Fk_Material = dbo.Inv_Material.Pk_Material LEFT OUTER JOIN
                      dbo.gen_Color ON dbo.Inv_Material.Fk_Color = dbo.gen_Color.Pk_Color LEFT OUTER JOIN
                      dbo.gen_Mill ON dbo.MaterialInwardD.Fk_Mill = dbo.gen_Mill.Pk_Mill AND dbo.Inv_Material.Fk_Mill = dbo.gen_Mill.Pk_Mill
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[43] 4[17] 2[6] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -96
         Left = 0
      End
      Begin Tables = 
         Begin Table = "MaterialInwardM"
            Begin Extent = 
               Top = 0
               Left = 38
               Bottom = 216
               Right = 254
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MaterialInwardD"
            Begin Extent = 
               Top = 5
               Left = 376
               Bottom = 215
               Right = 567
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 192
               Left = 677
               Bottom = 398
               Right = 848
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Color"
            Begin Extent = 
               Top = 54
               Left = 1370
               Bottom = 144
               Right = 1530
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Mill"
            Begin Extent = 
               Top = 239
               Left = 1158
               Bottom = 472
               Right = 1324
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 14
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
        ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_InwardRep'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N' Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_InwardRep'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_InwardRep'
GO
/****** Object:  View [dbo].[Vw_InvRep]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_InvRep]
AS
SELECT     TOP (100) PERCENT dbo.gen_Customer.CustomerName, dbo.Inv_Billing.Invno, dbo.Inv_Billing.InvDate, dbo.Inv_Billing.Fk_OrderNo, dbo.Inv_BillingDetails.Quantity, dbo.Inv_BillingDetails.Price, 
                      dbo.Inv_BillingDetails.Discount, dbo.Inv_BillingDetails.NetAmount, dbo.Inv_BillingDetails.Description, dbo.Inv_Billing.Pk_Invoice, dbo.Inv_BillingDetails.Pk_Inv_Details, 
                      dbo.gen_Customer.Pk_Customer, dbo.gen_Customer.PinCode, dbo.gen_Customer.LandLine, dbo.gen_Customer.CustomerAddress, dbo.Inv_Billing.COA, dbo.Inv_Billing.ESUGAM, 
                      dbo.gen_Customer.CreditLimit, dbo.gen_Customer.VAT, dbo.gen_Customer.PAN, dbo.gen_Customer.Regn, dbo.Inv_Billing.GrandTotal, dbo.Inv_Billing.NETVALUE, dbo.Inv_Billing.FORM_CT3, 
                      dbo.Inv_Billing.FORM_H, dbo.Inv_Billing.TaxType, dbo.Inv_Billing.Buyers_OrderNo, dbo.Inv_Billing.BOrderDated, dbo.Inv_Billing.DNTimeofInv, dbo.Inv_Billing.MVehicleNo, 
                      dbo.Inv_Billing.DNTimeofRemoval, dbo.Inv_Billing.DeliveryName, dbo.Inv_Billing.DeliveryAdd1, dbo.Inv_Billing.DeliveryAdd2, dbo.Inv_BillingDetails.HsnCode, dbo.Inv_Billing.StateCode, 
                      dbo.gen_State.StateName, dbo.gen_City.CityName, dbo.gen_Country.CountryName
FROM         dbo.gen_Customer INNER JOIN
                      dbo.Inv_Billing ON dbo.gen_Customer.Pk_Customer = dbo.Inv_Billing.Fk_Customer INNER JOIN
                      dbo.Inv_BillingDetails ON dbo.Inv_Billing.Pk_Invoice = dbo.Inv_BillingDetails.Fk_Invoice INNER JOIN
                      dbo.gen_City ON dbo.gen_Customer.Fk_City = dbo.gen_City.Pk_CityID INNER JOIN
                      dbo.gen_State ON dbo.gen_Customer.Fk_State = dbo.gen_State.Pk_StateID INNER JOIN
                      dbo.gen_Country ON dbo.gen_Customer.Fk_Country = dbo.gen_Country.Pk_Country
WHERE     (dbo.Inv_Billing.Invno IS NULL)
ORDER BY dbo.Inv_Billing.Pk_Invoice
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 126
               Right = 212
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Billing"
            Begin Extent = 
               Top = 6
               Left = 250
               Bottom = 126
               Right = 426
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_BillingDetails"
            Begin Extent = 
               Top = 6
               Left = 464
               Bottom = 126
               Right = 624
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_City"
            Begin Extent = 
               Top = 6
               Left = 662
               Bottom = 126
               Right = 822
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_State"
            Begin Extent = 
               Top = 6
               Left = 860
               Bottom = 111
               Right = 1020
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Country"
            Begin Extent = 
               Top = 6
               Left = 1058
               Bottom = 96
               Right = 1218
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 1500
     ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_InvRep'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'    Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_InvRep'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_InvRep'
GO
/****** Object:  View [dbo].[Vw_InvOrdQty]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_InvOrdQty]
AS
SELECT     dbo.Inv_Billing.Fk_OrderNo, dbo.Inv_BillingDetails.Fk_PartID, dbo.Inv_Billing.Pk_Invoice, dbo.Inv_BillingDetails.Quantity, dbo.Inv_BillingDetails.Fk_BoxID
FROM         dbo.Inv_Billing INNER JOIN
                      dbo.Inv_BillingDetails ON dbo.Inv_Billing.Pk_Invoice = dbo.Inv_BillingDetails.Fk_Invoice
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[7] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Inv_Billing"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 243
               Right = 214
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_BillingDetails"
            Begin Extent = 
               Top = 6
               Left = 252
               Bottom = 251
               Right = 439
            End
            DisplayFlags = 280
            TopColumn = 2
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_InvOrdQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_InvOrdQty'
GO
/****** Object:  View [dbo].[Vw_IndentPOSearch]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_IndentPOSearch]
AS
SELECT     dbo.Inv_Material.Name, dbo.gen_Unit.UnitName, dbo.Inv_MaterialIndentMaster.MaterialIndentDate, dbo.gen_Vendor.VendorName, dbo.gen_Vendor.Pk_Vendor, 
                      dbo.Inv_MaterialIndentMaster.Pk_MaterialOrderMasterId, dbo.Inv_Material.Pk_Material, dbo.Inv_MaterialIndentDetails.Quantity, dbo.Inv_MaterialIndentDetails.Fk_CustomerOrder, 
                      dbo.Inv_MaterialIndentDetails.Pk_MaterialOrderDetailsId, dbo.Inv_MaterialIndentMaster.Fk_Tanent, dbo.gen_Color.ColorName, dbo.gen_Mill.MillName, dbo.gen_Customer.CustomerName
FROM         dbo.Inv_MaterialIndentMaster INNER JOIN
                      dbo.gen_Vendor ON dbo.Inv_MaterialIndentMaster.Fk_VendorId = dbo.gen_Vendor.Pk_Vendor INNER JOIN
                      dbo.Inv_MaterialIndentDetails ON dbo.Inv_MaterialIndentMaster.Pk_MaterialOrderMasterId = dbo.Inv_MaterialIndentDetails.Fk_MaterialOrderMasterId INNER JOIN
                      dbo.Inv_Material ON dbo.Inv_MaterialIndentDetails.Fk_Material = dbo.Inv_Material.Pk_Material INNER JOIN
                      dbo.gen_Unit ON dbo.Inv_Material.Fk_UnitId = dbo.gen_Unit.Pk_Unit INNER JOIN
                      dbo.gen_Color ON dbo.Inv_Material.Fk_Color = dbo.gen_Color.Pk_Color INNER JOIN
                      dbo.gen_Mill ON dbo.Inv_Material.Fk_Mill = dbo.gen_Mill.Pk_Mill LEFT OUTER JOIN
                      dbo.gen_Customer INNER JOIN
                      dbo.gen_Order ON dbo.gen_Customer.Pk_Customer = dbo.gen_Order.Fk_Customer ON dbo.Inv_MaterialIndentDetails.Fk_CustomerOrder = dbo.gen_Order.Pk_Order
WHERE     (dbo.Inv_Material.Pk_Material IN
                          (SELECT     Fk_Material
                            FROM          dbo.Vw_PendingPO
                            WHERE      (Pk_PONo IS NULL) AND (Quantity IS NULL)))
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[34] 4[12] 2[22] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = -192
      End
      Begin Tables = 
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 17
               Left = 529
               Bottom = 137
               Right = 703
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Order"
            Begin Extent = 
               Top = 175
               Left = 914
               Bottom = 295
               Right = 1095
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_MaterialIndentMaster"
            Begin Extent = 
               Top = 0
               Left = 7
               Bottom = 206
               Right = 253
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Vendor"
            Begin Extent = 
               Top = 6
               Left = 291
               Bottom = 126
               Right = 484
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_MaterialIndentDetails"
            Begin Extent = 
               Top = 165
               Left = 319
               Bottom = 348
               Right = 534
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 6
               Left = 740
               Bottom = 162
               Right = 929
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Unit"
            Begin Extent = 
               Top = 12
               Left = 1150
               Bottom = 132
               Right = ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_IndentPOSearch'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'1310
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Color"
            Begin Extent = 
               Top = 14
               Left = 970
               Bottom = 104
               Right = 1130
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Mill"
            Begin Extent = 
               Top = 252
               Left = 615
               Bottom = 372
               Right = 781
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 17
         Width = 284
         Width = 2235
         Width = 1500
         Width = 1770
         Width = 1500
         Width = 1500
         Width = 2160
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 840
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 2550
         Alias = 900
         Table = 3240
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_IndentPOSearch'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_IndentPOSearch'
GO
/****** Object:  View [dbo].[Vw_BoxPaper]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_BoxPaper]
AS
SELECT DISTINCT dbo.Inv_Material.Pk_Material, dbo.BoxMaster.Pk_BoxID, dbo.Inv_Material.Name, dbo.Items_Layers.Weight, dbo.PaperStock.RollNo, dbo.PaperStock.Quantity
FROM         dbo.BoxMaster INNER JOIN
                      dbo.BoxSpecs ON dbo.BoxMaster.Pk_BoxID = dbo.BoxSpecs.Fk_BoxID INNER JOIN
                      dbo.ItemPartProperty ON dbo.BoxSpecs.Pk_BoxSpecID = dbo.ItemPartProperty.Fk_BoxSpecsID INNER JOIN
                      dbo.Items_Layers ON dbo.ItemPartProperty.Pk_PartPropertyID = dbo.Items_Layers.Fk_PartId INNER JOIN
                      dbo.Inv_Material ON dbo.Items_Layers.Fk_Material = dbo.Inv_Material.Pk_Material INNER JOIN
                      dbo.PaperStock ON dbo.Inv_Material.Pk_Material = dbo.PaperStock.Fk_Material
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[50] 4[11] 2[14] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 366
               Right = 206
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxSpecs"
            Begin Extent = 
               Top = 24
               Left = 305
               Bottom = 230
               Right = 484
            End
            DisplayFlags = 280
            TopColumn = 4
         End
         Begin Table = "ItemPartProperty"
            Begin Extent = 
               Top = 8
               Left = 542
               Bottom = 128
               Right = 721
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Items_Layers"
            Begin Extent = 
               Top = 12
               Left = 893
               Bottom = 132
               Right = 1053
            End
            DisplayFlags = 280
            TopColumn = 4
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 160
               Left = 510
               Bottom = 333
               Right = 700
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PaperStock"
            Begin Extent = 
               Top = 162
               Left = 866
               Bottom = 282
               Right = 1026
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1635
  ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_BoxPaper'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'       Width = 1500
         Width = 2430
         Width = 2550
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_BoxPaper'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_BoxPaper'
GO
/****** Object:  View [dbo].[Vw_BoxOrder]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_BoxOrder]
AS
SELECT     SUM(dbo.gen_DeliverySchedule.Quantity) AS SchQty, dbo.gen_Order.Pk_Order, dbo.BoxMaster.Pk_BoxID, dbo.BoxMaster.Name, dbo.gen_Order.Fk_Enquiry, dbo.Gen_OrderChild.Pk_OrderChild, 
                      dbo.BoxMaster.PartNo, dbo.Gen_OrderChild.OrdQty, dbo.Gen_OrderChild.EnqQty, dbo.ItemPartProperty.PName, dbo.ItemPartProperty.Quantity, dbo.ItemPartProperty.Pk_PartPropertyID, 
                      dbo.Gen_OrderChild.Fk_Status, dbo.gen_Order.OrderDate, dbo.gen_Customer.CustomerName, dbo.gen_Order.Fk_Customer
FROM         dbo.eq_Enquiry INNER JOIN
                      dbo.gen_Order ON dbo.eq_Enquiry.Pk_Enquiry = dbo.gen_Order.Fk_Enquiry INNER JOIN
                      dbo.Gen_OrderChild ON dbo.gen_Order.Pk_Order = dbo.Gen_OrderChild.Fk_OrderID INNER JOIN
                      dbo.BoxMaster ON dbo.Gen_OrderChild.Fk_BoxID = dbo.BoxMaster.Pk_BoxID INNER JOIN
                      dbo.BoxSpecs ON dbo.BoxMaster.Pk_BoxID = dbo.BoxSpecs.Fk_BoxID INNER JOIN
                      dbo.ItemPartProperty ON dbo.BoxSpecs.Pk_BoxSpecID = dbo.ItemPartProperty.Fk_BoxSpecsID AND dbo.Gen_OrderChild.Fk_PartID = dbo.ItemPartProperty.Pk_PartPropertyID INNER JOIN
                      dbo.gen_Customer ON dbo.gen_Order.Fk_Customer = dbo.gen_Customer.Pk_Customer AND dbo.BoxMaster.Customer = dbo.gen_Customer.Pk_Customer LEFT OUTER JOIN
                      dbo.gen_DeliverySchedule ON dbo.gen_Order.Pk_Order = dbo.gen_DeliverySchedule.Fk_Order AND dbo.BoxMaster.Pk_BoxID = dbo.gen_DeliverySchedule.Fk_BoxID AND 
                      dbo.ItemPartProperty.Pk_PartPropertyID = dbo.gen_DeliverySchedule.Fk_PartID
GROUP BY dbo.gen_Order.Pk_Order, dbo.BoxMaster.Pk_BoxID, dbo.BoxMaster.Name, dbo.gen_Order.Fk_Enquiry, dbo.Gen_OrderChild.Pk_OrderChild, dbo.BoxMaster.PartNo, dbo.Gen_OrderChild.OrdQty, 
                      dbo.Gen_OrderChild.EnqQty, dbo.ItemPartProperty.PName, dbo.ItemPartProperty.Quantity, dbo.ItemPartProperty.Pk_PartPropertyID, dbo.Gen_OrderChild.Fk_Status, dbo.gen_Order.OrderDate, 
                      dbo.gen_Customer.CustomerName, dbo.gen_Order.Fk_Customer
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[49] 4[5] 2[6] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "eq_Enquiry"
            Begin Extent = 
               Top = 250
               Left = 267
               Bottom = 631
               Right = 428
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Order"
            Begin Extent = 
               Top = 0
               Left = 999
               Bottom = 120
               Right = 1180
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "Gen_OrderChild"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 356
               Right = 217
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 256
               Left = 543
               Bottom = 376
               Right = 703
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxSpecs"
            Begin Extent = 
               Top = 169
               Left = 1215
               Bottom = 348
               Right = 1398
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_DeliverySchedule"
            Begin Extent = 
               Top = 6
               Left = 255
               Bottom = 125
               Right = 449
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 24
               Left = 1317
               Bottom = 144
               Right = 1507
            E' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_BoxOrder'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'nd
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ItemPartProperty"
            Begin Extent = 
               Top = 114
               Left = 777
               Bottom = 319
               Right = 956
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 17
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 840
         Width = 1005
         Width = 900
         Width = 1230
         Width = 960
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_BoxOrder'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_BoxOrder'
GO
/****** Object:  View [dbo].[Vw_BoxDet]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_BoxDet]
AS
SELECT     dbo.BoxMaster.Name, dbo.ItemPartProperty.Length, dbo.ItemPartProperty.Width, dbo.ItemPartProperty.Height, dbo.ItemPartProperty.Weight, dbo.Items_Layers.GSM, dbo.Items_Layers.BF, 
                      dbo.ItemPartProperty.Pk_PartPropertyID, dbo.Items_Layers.Pk_LayerID, dbo.BoxMaster.Pk_BoxID, dbo.gen_Customer.CustomerName, dbo.BoxSpecs.OuterShellYes, dbo.BoxSpecs.CapYes, 
                      dbo.BoxSpecs.LenghtPartationYes, dbo.BoxSpecs.WidthPartationYes, dbo.BoxSpecs.PlateYes, dbo.BoxSpecs.Pk_BoxSpecID, dbo.Inv_Material.Name AS MaterialName, 
                      dbo.BoxType.Name AS BType, dbo.ItemPartProperty.BoardArea, dbo.ItemPartProperty.Deckle, dbo.ItemPartProperty.CuttingSize, dbo.ItemPartProperty.PName, dbo.ItemPartProperty.BoardBS, 
                      dbo.ItemPartProperty.BoardGSM, dbo.ItemPartProperty.TakeUpFactor, dbo.FluteType.FluteName, dbo.FluteType.TKFactor AS FluteTKF, dbo.Items_Layers.Tk_Factor AS LayerTKF, 
                      dbo.gen_Color.ColorName, dbo.ItemPartProperty.Quantity, dbo.ItemPartProperty.PlyVal, dbo.Items_Layers.Weight AS LayerWt, dbo.Inv_Material.Pk_Material
FROM         dbo.BoxMaster INNER JOIN
                      dbo.BoxSpecs ON dbo.BoxMaster.Pk_BoxID = dbo.BoxSpecs.Fk_BoxID INNER JOIN
                      dbo.ItemPartProperty ON dbo.BoxSpecs.Pk_BoxSpecID = dbo.ItemPartProperty.Fk_BoxSpecsID INNER JOIN
                      dbo.Items_Layers ON dbo.ItemPartProperty.Pk_PartPropertyID = dbo.Items_Layers.Fk_PartId INNER JOIN
                      dbo.gen_Customer ON dbo.BoxMaster.Customer = dbo.gen_Customer.Pk_Customer INNER JOIN
                      dbo.Inv_Material ON dbo.Items_Layers.Fk_Material = dbo.Inv_Material.Pk_Material INNER JOIN
                      dbo.BoxType ON dbo.BoxMaster.Fk_BoxType = dbo.BoxType.Pk_BoxTypeID INNER JOIN
                      dbo.FluteType ON dbo.BoxMaster.Fk_FluteType = dbo.FluteType.Pk_FluteID INNER JOIN
                      dbo.gen_Color ON dbo.Inv_Material.Fk_Color = dbo.gen_Color.Pk_Color
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[44] 4[11] 2[23] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 226
               Right = 207
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxSpecs"
            Begin Extent = 
               Top = 0
               Left = 352
               Bottom = 220
               Right = 538
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ItemPartProperty"
            Begin Extent = 
               Top = 13
               Left = 622
               Bottom = 233
               Right = 785
            End
            DisplayFlags = 280
            TopColumn = 6
         End
         Begin Table = "Items_Layers"
            Begin Extent = 
               Top = 0
               Left = 906
               Bottom = 220
               Right = 1071
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 228
               Left = 38
               Bottom = 348
               Right = 212
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 84
               Left = 247
               Bottom = 235
               Right = 432
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxType"
            Begin Extent = 
               Top = 228
               Left = 477
               Bottom = 318
               Right = 637
            End
      ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_BoxDet'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'      DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "FluteType"
            Begin Extent = 
               Top = 67
               Left = 1134
               Bottom = 228
               Right = 1293
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Color"
            Begin Extent = 
               Top = 222
               Left = 675
               Bottom = 312
               Right = 835
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 35
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1515
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_BoxDet'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_BoxDet'
GO
/****** Object:  View [dbo].[Vw_Agreed_ActualDelivery]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_Agreed_ActualDelivery]
AS
SELECT     dbo.gen_Customer.CustomerName, dbo.gen_Order.Pk_Order, dbo.gen_DeliverySchedule.DeliveryDate, dbo.gen_DeliverySchedule.Quantity, dbo.gen_DeliverySchedule.DeliveredDate, 
                      dbo.gen_Order.Product
FROM         dbo.gen_DeliverySchedule INNER JOIN
                      dbo.gen_Order ON dbo.gen_DeliverySchedule.Fk_Order = dbo.gen_Order.Pk_Order INNER JOIN
                      dbo.gen_Customer ON dbo.gen_Order.Fk_Customer = dbo.gen_Customer.Pk_Customer
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "gen_DeliverySchedule"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 243
               Right = 232
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 32
               Left = 470
               Bottom = 152
               Right = 644
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Order"
            Begin Extent = 
               Top = 0
               Left = 903
               Bottom = 243
               Right = 1084
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Agreed_ActualDelivery'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Agreed_ActualDelivery'
GO
/****** Object:  View [dbo].[Vw_Age]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_Age]
AS
SELECT     TOP (100) PERCENT dbo.PaperStock.RollNo, dbo.PaperStock.Pk_PaperStock, dbo.Inv_Material.Name, dbo.PaperStock.Quantity, dbo.Inv_Material.Pk_Material, DATEDIFF(day, 
                      dbo.MaterialInwardM.Inward_Date, GETDATE()) AS Age, dbo.Inv_Material.GSM, dbo.Inv_Material.BF, dbo.Inv_Material.Deckle, dbo.gen_Mill.MillName, dbo.gen_Color.ColorName
FROM         dbo.MaterialInwardM INNER JOIN
                      dbo.MaterialInwardD ON dbo.MaterialInwardM.Pk_Inward = dbo.MaterialInwardD.Fk_Inward RIGHT OUTER JOIN
                      dbo.gen_Color INNER JOIN
                      dbo.Inv_Material INNER JOIN
                      dbo.PaperStock ON dbo.Inv_Material.Pk_Material = dbo.PaperStock.Fk_Material ON dbo.gen_Color.Pk_Color = dbo.Inv_Material.Fk_Color LEFT OUTER JOIN
                      dbo.gen_Mill ON dbo.Inv_Material.Fk_Mill = dbo.gen_Mill.Pk_Mill ON dbo.MaterialInwardD.Fk_Material = dbo.Inv_Material.Pk_Material AND 
                      dbo.MaterialInwardD.RollNo = dbo.PaperStock.RollNo
WHERE     (dbo.PaperStock.Quantity > 0)
ORDER BY age DESC
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[46] 4[4] 2[16] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "MaterialInwardM"
            Begin Extent = 
               Top = 0
               Left = 1089
               Bottom = 120
               Right = 1249
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MaterialInwardD"
            Begin Extent = 
               Top = 43
               Left = 710
               Bottom = 278
               Right = 983
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 287
               Right = 227
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PaperStock"
            Begin Extent = 
               Top = 141
               Left = 369
               Bottom = 303
               Right = 517
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Mill"
            Begin Extent = 
               Top = 189
               Left = 1203
               Bottom = 309
               Right = 1369
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Color"
            Begin Extent = 
               Top = 12
               Left = 479
               Bottom = 102
               Right = 639
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 13
         Width = 284
         Width = 1500' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Age'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'
         Width = 1500
         Width = 1500
         Width = 2100
         Width = 1500
         Width = 2460
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Age'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Age'
GO
/****** Object:  View [dbo].[Vw_BalanceOrdQty]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_BalanceOrdQty]
AS
SELECT     SUM(dbo.Inv_BillingDetails.Quantity) AS DelvQty, dbo.gen_Order.Pk_Order, dbo.gen_Order.OrderDate, dbo.gen_Customer.CustomerName, dbo.Gen_OrderChild.OrdQty, 
                      dbo.ItemPartProperty.PName AS PartName, dbo.BoxMaster.Name AS BoxName, ISNULL(dbo.BoxStock.Quantity, 0) AS Ex_Stock, dbo.Inv_Billing.Pk_Invoice, dbo.Inv_Billing.InvDate, 
                      dbo.Inv_BillingDetails.Quantity, dbo.Inv_BillingDetails.Fk_BoxID, dbo.Inv_BillingDetails.Fk_PartID
FROM         dbo.BoxMaster INNER JOIN
                      dbo.gen_Order INNER JOIN
                      dbo.Gen_OrderChild ON dbo.gen_Order.Pk_Order = dbo.Gen_OrderChild.Fk_OrderID INNER JOIN
                      dbo.gen_Customer ON dbo.gen_Order.Fk_Customer = dbo.gen_Customer.Pk_Customer ON dbo.BoxMaster.Pk_BoxID = dbo.Gen_OrderChild.Fk_BoxID AND 
                      dbo.BoxMaster.Customer = dbo.gen_Customer.Pk_Customer LEFT OUTER JOIN
                      dbo.Inv_BillingDetails INNER JOIN
                      dbo.Inv_Billing ON dbo.Inv_BillingDetails.Fk_Invoice = dbo.Inv_Billing.Pk_Invoice ON dbo.BoxMaster.Pk_BoxID = dbo.Inv_BillingDetails.Fk_BoxID AND 
                      dbo.gen_Order.Pk_Order = dbo.Inv_Billing.Fk_OrderNo AND dbo.gen_Customer.Pk_Customer = dbo.Inv_Billing.Fk_Customer INNER JOIN
                      dbo.ItemPartProperty ON dbo.Gen_OrderChild.Fk_PartID = dbo.ItemPartProperty.Pk_PartPropertyID AND dbo.Inv_BillingDetails.Fk_PartID = dbo.ItemPartProperty.Pk_PartPropertyID LEFT OUTER JOIN
                      dbo.BoxStock ON dbo.BoxMaster.Pk_BoxID = dbo.BoxStock.Fk_BoxID AND dbo.ItemPartProperty.Pk_PartPropertyID = dbo.BoxStock.Fk_PartID
GROUP BY dbo.gen_Order.Pk_Order, dbo.gen_Order.OrderDate, dbo.gen_Customer.CustomerName, dbo.ItemPartProperty.PName, dbo.BoxMaster.Name, dbo.Gen_OrderChild.OrdQty, dbo.BoxStock.Quantity, 
                      dbo.Inv_Billing.Pk_Invoice, dbo.Inv_Billing.InvDate, dbo.Inv_BillingDetails.Quantity, dbo.Inv_BillingDetails.Fk_BoxID, dbo.Inv_BillingDetails.Fk_PartID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[55] 4[5] 2[7] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "gen_Order"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 235
               Right = 218
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Gen_OrderChild"
            Begin Extent = 
               Top = 14
               Left = 254
               Bottom = 219
               Right = 414
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 0
               Left = 442
               Bottom = 120
               Right = 616
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Billing"
            Begin Extent = 
               Top = 135
               Left = 774
               Bottom = 389
               Right = 950
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_BillingDetails"
            Begin Extent = 
               Top = 78
               Left = 1100
               Bottom = 423
               Right = 1321
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 226
               Left = 473
               Bottom = 346
               Right = 633
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ItemPartProperty"
            Begin Extent = 
               Top = 0
               Left = 784
               Bottom = 120
               Right = 963
          ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_BalanceOrdQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'  End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxStock"
            Begin Extent = 
               Top = 230
               Left = 79
               Bottom = 350
               Right = 255
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 14
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 2505
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_BalanceOrdQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_BalanceOrdQty'
GO
/****** Object:  View [dbo].[Vw_Qc]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_Qc]
AS
SELECT     dbo.QualityChild.AccQty - ISNULL(SUM(dbo.MaterialInwardD.Quantity), 0) AS Pending, dbo.QualityChild.Fk_Material, dbo.Inv_Material.Name, dbo.QualityChild.AccQty, 
                      dbo.QualityChild.FkQualityCheck, ISNULL(dbo.MaterialInwardD.Quantity, 0) AS InwdQty, dbo.QualityChild.Pk_QualityChild
FROM         dbo.Inv_Material INNER JOIN
                      dbo.QualityChild ON dbo.Inv_Material.Pk_Material = dbo.QualityChild.Fk_Material LEFT OUTER JOIN
                      dbo.MaterialInwardD ON dbo.Inv_Material.Pk_Material = dbo.MaterialInwardD.Fk_Material AND dbo.QualityChild.Fk_Material = dbo.MaterialInwardD.Fk_Material
GROUP BY dbo.QualityChild.Fk_Material, dbo.Inv_Material.Name, dbo.QualityChild.AccQty, dbo.QualityChild.FkQualityCheck, dbo.MaterialInwardD.Quantity, dbo.QualityChild.Pk_QualityChild
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[34] 4[10] 2[4] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 306
               Right = 215
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "QualityChild"
            Begin Extent = 
               Top = 43
               Left = 442
               Bottom = 304
               Right = 659
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MaterialInwardD"
            Begin Extent = 
               Top = 68
               Left = 961
               Bottom = 288
               Right = 1137
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Qc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Qc'
GO
/****** Object:  View [dbo].[Vw_PoBom]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_PoBom]
AS
SELECT     dbo.Inv_Material.Name, dbo.PurchaseOrderM.Pk_PONo, dbo.PurchaseOrderM.PODate, dbo.gen_Vendor.VendorName, dbo.PurchaseOrderM.Fk_Status, dbo.PurchaseOrderD.Quantity AS POQty, 
                      dbo.Inv_Material.Pk_Material, dbo.MaterialInwardD.Quantity AS AccQty, dbo.PurchaseOrderD.Quantity - dbo.MaterialInwardD.Quantity AS PendQty, dbo.PurchaseOrderM.Fk_Indent, 
                      dbo.MaterialInwardM.Inward_Date, dbo.MaterialInwardD.Pk_InwardDet
FROM         dbo.Inv_Material INNER JOIN
                      dbo.PurchaseOrderD ON dbo.Inv_Material.Pk_Material = dbo.PurchaseOrderD.Fk_Material INNER JOIN
                      dbo.PurchaseOrderM ON dbo.PurchaseOrderD.Fk_PONo = dbo.PurchaseOrderM.Pk_PONo INNER JOIN
                      dbo.gen_Vendor ON dbo.PurchaseOrderM.Fk_Vendor = dbo.gen_Vendor.Pk_Vendor INNER JOIN
                      dbo.MaterialInwardD ON dbo.Inv_Material.Pk_Material = dbo.MaterialInwardD.Fk_Material INNER JOIN
                      dbo.MaterialInwardM ON dbo.MaterialInwardD.Fk_Inward = dbo.MaterialInwardM.Pk_Inward AND dbo.PurchaseOrderM.Pk_PONo = dbo.MaterialInwardM.PONo
WHERE     (dbo.PurchaseOrderM.Fk_Status = 1)
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[44] 4[6] 2[25] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -192
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 6
               Left = 0
               Bottom = 233
               Right = 210
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PurchaseOrderD"
            Begin Extent = 
               Top = 0
               Left = 1026
               Bottom = 233
               Right = 1212
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PurchaseOrderM"
            Begin Extent = 
               Top = 227
               Left = 0
               Bottom = 491
               Right = 264
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Vendor"
            Begin Extent = 
               Top = 0
               Left = 347
               Bottom = 272
               Right = 512
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MaterialInwardD"
            Begin Extent = 
               Top = 264
               Left = 881
               Bottom = 447
               Right = 1025
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MaterialInwardM"
            Begin Extent = 
               Top = 296
               Left = 471
               Bottom = 496
               Right = 674
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 11
         Width = 284
         Wi' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PoBom'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'dth = 2445
         Width = 1500
         Width = 2370
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1935
         Alias = 900
         Table = 2070
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PoBom'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PoBom'
GO
/****** Object:  View [dbo].[VwOrderRep]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VwOrderRep]
AS
SELECT     dbo.gen_Order.Pk_Order, dbo.gen_Order.OrderDate, dbo.gen_Customer.CustomerName, dbo.BoxMaster.Name, dbo.Gen_OrderChild.Ddate, dbo.Gen_OrderChild.OrdQty, dbo.Gen_OrderChild.EnqQty,
                       dbo.BoxMaster.Pk_BoxID, dbo.ItemPartProperty.PName, dbo.gen_Order.Cust_PO, dbo.Gen_OrderChild.Pk_OrderChild
FROM         dbo.gen_Order INNER JOIN
                      dbo.eq_Enquiry ON dbo.gen_Order.Fk_Enquiry = dbo.eq_Enquiry.Pk_Enquiry INNER JOIN
                      dbo.gen_Customer ON dbo.eq_Enquiry.Customer = dbo.gen_Customer.Pk_Customer INNER JOIN
                      dbo.Gen_OrderChild ON dbo.gen_Order.Pk_Order = dbo.Gen_OrderChild.Fk_OrderID INNER JOIN
                      dbo.BoxMaster ON dbo.Gen_OrderChild.Fk_BoxID = dbo.BoxMaster.Pk_BoxID INNER JOIN
                      dbo.BoxSpecs ON dbo.BoxMaster.Pk_BoxID = dbo.BoxSpecs.Fk_BoxID INNER JOIN
                      dbo.ItemPartProperty ON dbo.Gen_OrderChild.Fk_PartID = dbo.ItemPartProperty.Pk_PartPropertyID AND dbo.BoxSpecs.Pk_BoxSpecID = dbo.ItemPartProperty.Fk_BoxSpecsID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[54] 4[7] 2[10] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "gen_Order"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 126
               Right = 219
            End
            DisplayFlags = 280
            TopColumn = 11
         End
         Begin Table = "eq_Enquiry"
            Begin Extent = 
               Top = 6
               Left = 489
               Bottom = 126
               Right = 677
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 6
               Left = 715
               Bottom = 126
               Right = 889
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Gen_OrderChild"
            Begin Extent = 
               Top = 184
               Left = 461
               Bottom = 401
               Right = 621
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 27
               Left = 1064
               Bottom = 147
               Right = 1224
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxSpecs"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 246
               Right = 221
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ItemPartProperty"
            Begin Extent = 
               Top = 191
               Left = 997
               Bottom = 311
               Right = 1176
            End
 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VwOrderRep'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'           DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VwOrderRep'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VwOrderRep'
GO
/****** Object:  View [dbo].[VwInvoice]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VwInvoice]
AS
SELECT     TOP (100) PERCENT dbo.gen_Customer.CustomerName, dbo.Inv_Billing.Invno, dbo.Inv_Billing.InvDate, dbo.Inv_Billing.Fk_OrderNo, dbo.Inv_BillingDetails.Quantity, dbo.Inv_BillingDetails.Price, 
                      dbo.Inv_BillingDetails.Discount, dbo.Inv_BillingDetails.NetAmount, dbo.Inv_BillingDetails.Description, dbo.Inv_Billing.Pk_Invoice, dbo.Inv_BillingDetails.Pk_Inv_Details, 
                      dbo.gen_Customer.Pk_Customer, dbo.gen_Customer.PinCode, dbo.gen_Customer.LandLine, dbo.gen_Customer.CustomerAddress, dbo.Inv_Billing.COA, dbo.Inv_Billing.ESUGAM, 
                      dbo.gen_Customer.CreditLimit, dbo.gen_Customer.VAT, dbo.gen_Customer.PAN, dbo.gen_Customer.Regn, dbo.Inv_Billing.GrandTotal, dbo.Inv_Billing.NETVALUE, dbo.Inv_Billing.FORM_CT3, 
                      dbo.Inv_Billing.FORM_H, dbo.Inv_Billing.TaxType, dbo.Inv_Billing.Buyers_OrderNo, dbo.Inv_Billing.BOrderDated, dbo.Inv_Billing.DNTimeofInv, dbo.Inv_Billing.MVehicleNo, 
                      dbo.Inv_Billing.DNTimeofRemoval, dbo.Inv_Billing.DeliveryName, dbo.Inv_Billing.DeliveryAdd1, dbo.Inv_Billing.DeliveryAdd2, dbo.Inv_BillingDetails.HsnCode, dbo.Inv_Billing.StateCode, 
                      dbo.gen_State.StateName, dbo.gen_City.CityName, dbo.gen_Country.CountryName, dbo.BoxMaster.Name, dbo.Tax.TaxName, dbo.Tax.TaxValue AS Expr1, dbo.Inv_BillingDetails.Fk_PartID, 
                      dbo.Inv_BillingDetails.TaxValue, dbo.ItemPartProperty.PName, dbo.ItemPartProperty.Length, dbo.ItemPartProperty.Width, dbo.ItemPartProperty.Height, dbo.gen_Customer.CreditAmt, 
                      dbo.ItemPartProperty.Pk_PartPropertyID, dbo.Inv_BillingDetails.UnitVal
FROM         dbo.gen_Customer INNER JOIN
                      dbo.Inv_Billing ON dbo.gen_Customer.Pk_Customer = dbo.Inv_Billing.Fk_Customer INNER JOIN
                      dbo.Inv_BillingDetails ON dbo.Inv_Billing.Pk_Invoice = dbo.Inv_BillingDetails.Fk_Invoice INNER JOIN
                      dbo.gen_City ON dbo.gen_Customer.Fk_City = dbo.gen_City.Pk_CityID INNER JOIN
                      dbo.gen_State ON dbo.gen_Customer.Fk_State = dbo.gen_State.Pk_StateID INNER JOIN
                      dbo.gen_Country ON dbo.gen_Customer.Fk_Country = dbo.gen_Country.Pk_Country INNER JOIN
                      dbo.BoxMaster ON dbo.gen_Customer.Pk_Customer = dbo.BoxMaster.Customer AND dbo.Inv_BillingDetails.Fk_BoxID = dbo.BoxMaster.Pk_BoxID INNER JOIN
                      dbo.Tax ON dbo.Inv_BillingDetails.Fk_TaxID = dbo.Tax.PkTax INNER JOIN
                      dbo.BoxSpecs ON dbo.BoxMaster.Pk_BoxID = dbo.BoxSpecs.Fk_BoxID INNER JOIN
                      dbo.ItemPartProperty ON dbo.BoxSpecs.Pk_BoxSpecID = dbo.ItemPartProperty.Fk_BoxSpecsID
WHERE     (dbo.Inv_Billing.Invno IS NULL)
ORDER BY dbo.Inv_Billing.Pk_Invoice
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[31] 4[34] 2[8] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 0
               Left = 42
               Bottom = 263
               Right = 174
            End
            DisplayFlags = 280
            TopColumn = 7
         End
         Begin Table = "Inv_Billing"
            Begin Extent = 
               Top = 6
               Left = 478
               Bottom = 264
               Right = 651
            End
            DisplayFlags = 280
            TopColumn = 4
         End
         Begin Table = "Inv_BillingDetails"
            Begin Extent = 
               Top = 0
               Left = 974
               Bottom = 320
               Right = 1160
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_City"
            Begin Extent = 
               Top = 122
               Left = 241
               Bottom = 272
               Right = 401
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_State"
            Begin Extent = 
               Top = 229
               Left = 654
               Bottom = 440
               Right = 814
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Country"
            Begin Extent = 
               Top = 6
               Left = 250
               Bottom = 123
               Right = 410
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 55
               Left = 706
               Bottom = 175
               Right = 866
            End
     ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VwInvoice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'       DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Tax"
            Begin Extent = 
               Top = 23
               Left = 1214
               Bottom = 128
               Right = 1374
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxSpecs"
            Begin Extent = 
               Top = 157
               Left = 106
               Bottom = 277
               Right = 289
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ItemPartProperty"
            Begin Extent = 
               Top = 264
               Left = 439
               Bottom = 546
               Right = 618
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 52
         Width = 284
         Width = 1275
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 2655
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VwInvoice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VwInvoice'
GO
/****** Object:  View [dbo].[Vw_TotInwdQty]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_TotInwdQty]
AS
SELECT     SUM(dbo.MaterialInwardD.Quantity) AS Qty, dbo.MaterialInwardM.Pk_Inward, dbo.Inv_Material.Pk_Material, dbo.Inv_Material.Name, dbo.MaterialInwardM.Inward_Date
FROM         dbo.MaterialInwardM INNER JOIN
                      dbo.MaterialInwardD ON dbo.MaterialInwardM.Pk_Inward = dbo.MaterialInwardD.Fk_Inward INNER JOIN
                      dbo.Inv_Material ON dbo.MaterialInwardD.Fk_Material = dbo.Inv_Material.Pk_Material
GROUP BY dbo.MaterialInwardM.Pk_Inward, dbo.Inv_Material.Pk_Material, dbo.Inv_Material.Name, dbo.MaterialInwardM.Inward_Date
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[17] 2[10] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "MaterialInwardM"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 255
               Right = 185
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MaterialInwardD"
            Begin Extent = 
               Top = 6
               Left = 321
               Bottom = 267
               Right = 486
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 11
               Left = 788
               Bottom = 178
               Right = 977
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 11
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_TotInwdQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_TotInwdQty'
GO
/****** Object:  View [dbo].[Vw_SampleBoxDet]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_SampleBoxDet]
AS
SELECT     dbo.SampleBoxMaster.Name, dbo.SampleItemPartProperty.Length, dbo.SampleItemPartProperty.Width, dbo.SampleItemPartProperty.Height, dbo.SampleItemPartProperty.Weight, 
                      dbo.SampleItems_Layers.GSM, dbo.SampleItems_Layers.BF, dbo.SampleItemPartProperty.Pk_PartPropertyID, dbo.SampleItems_Layers.Pk_LayerID, dbo.SampleBoxMaster.Pk_BoxID, 
                      dbo.gen_Customer.CustomerName, dbo.SampleBoxSpecs.OuterShellYes, dbo.SampleBoxSpecs.CapYes, dbo.SampleBoxSpecs.LenghtPartationYes, dbo.SampleBoxSpecs.WidthPartationYes, 
                      dbo.SampleBoxSpecs.PlateYes, dbo.SampleBoxSpecs.Pk_BoxSpecID, dbo.Inv_Material.Name AS MaterialName, dbo.BoxType.Name AS BType, dbo.SampleItemPartProperty.BoardArea, 
                      dbo.SampleItemPartProperty.Deckle, dbo.SampleItemPartProperty.CuttingSize, dbo.SampleItemPartProperty.PName, dbo.SampleItemPartProperty.BoardBS, dbo.SampleItemPartProperty.BoardGSM, 
                      dbo.SampleItemPartProperty.TakeUpFactor, dbo.FluteType.FluteName, dbo.FluteType.TKFactor AS FluteTKF, dbo.SampleItems_Layers.Tk_Factor AS LayerTKF, dbo.gen_Color.ColorName, 
                      dbo.SampleItemPartProperty.Quantity, dbo.SampleItemPartProperty.PlyVal, dbo.SampleBoxMaster.AsPer
FROM         dbo.SampleBoxMaster INNER JOIN
                      dbo.SampleBoxSpecs ON dbo.SampleBoxMaster.Pk_BoxID = dbo.SampleBoxSpecs.Fk_BoxID INNER JOIN
                      dbo.SampleItemPartProperty ON dbo.SampleBoxSpecs.Pk_BoxSpecID = dbo.SampleItemPartProperty.Fk_BoxSpecsID INNER JOIN
                      dbo.SampleItems_Layers ON dbo.SampleItemPartProperty.Pk_PartPropertyID = dbo.SampleItems_Layers.Fk_PartId INNER JOIN
                      dbo.gen_Customer ON dbo.SampleBoxMaster.Customer = dbo.gen_Customer.Pk_Customer INNER JOIN
                      dbo.Inv_Material ON dbo.SampleItems_Layers.Fk_Material = dbo.Inv_Material.Pk_Material INNER JOIN
                      dbo.BoxType ON dbo.SampleBoxMaster.Fk_BoxType = dbo.BoxType.Pk_BoxTypeID INNER JOIN
                      dbo.FluteType ON dbo.SampleBoxMaster.Fk_FluteType = dbo.FluteType.Pk_FluteID INNER JOIN
                      dbo.gen_Color ON dbo.Inv_Material.Fk_Color = dbo.gen_Color.Pk_Color
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[7] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "SampleBoxMaster"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 126
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 4
         End
         Begin Table = "SampleBoxSpecs"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 126
               Right = 419
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SampleItemPartProperty"
            Begin Extent = 
               Top = 6
               Left = 457
               Bottom = 126
               Right = 636
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SampleItems_Layers"
            Begin Extent = 
               Top = 6
               Left = 674
               Bottom = 126
               Right = 834
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 6
               Left = 872
               Bottom = 126
               Right = 1046
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 6
               Left = 1084
               Bottom = 126
               Right = 1273
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxType"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 216
               Right = 198
  ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_SampleBoxDet'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'          End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "FluteType"
            Begin Extent = 
               Top = 126
               Left = 236
               Bottom = 246
               Right = 396
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Color"
            Begin Extent = 
               Top = 126
               Left = 434
               Bottom = 216
               Right = 594
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_SampleBoxDet'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_SampleBoxDet'
GO
/****** Object:  View [dbo].[Vw_PartsSchedules]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_PartsSchedules]
AS
SELECT     dbo.gen_DeliverySchedule.DeliveryDate, dbo.gen_DeliverySchedule.Quantity AS SchQty, dbo.BoxMaster.Pk_BoxID, dbo.BoxMaster.Name, dbo.Gen_OrderChild.OrdQty, dbo.gen_Order.Pk_Order, 
                      dbo.gen_Order.OrderDate, dbo.gen_Customer.CustomerName, dbo.gen_DeliverySchedule.Pk_DeliverySechedule, dbo.gen_Order.Cust_PO, dbo.gen_Order.Fk_Enquiry, dbo.ItemPartProperty.PName, 
                      dbo.ItemPartProperty.Pk_PartPropertyID, dbo.gen_DeliverySchedule.Fk_BoxID
FROM         dbo.BoxMaster INNER JOIN
                      dbo.Gen_OrderChild ON dbo.BoxMaster.Pk_BoxID = dbo.Gen_OrderChild.Fk_BoxID INNER JOIN
                      dbo.gen_Order ON dbo.Gen_OrderChild.Fk_OrderID = dbo.gen_Order.Pk_Order INNER JOIN
                      dbo.gen_DeliverySchedule ON dbo.BoxMaster.Pk_BoxID = dbo.gen_DeliverySchedule.Fk_BoxID AND dbo.gen_Order.Pk_Order = dbo.gen_DeliverySchedule.Fk_Order INNER JOIN
                      dbo.gen_Customer ON dbo.gen_Order.Fk_Customer = dbo.gen_Customer.Pk_Customer INNER JOIN
                      dbo.BoxSpecs ON dbo.BoxMaster.Pk_BoxID = dbo.BoxSpecs.Fk_BoxID INNER JOIN
                      dbo.ItemPartProperty ON dbo.BoxSpecs.Pk_BoxSpecID = dbo.ItemPartProperty.Fk_BoxSpecsID AND dbo.gen_DeliverySchedule.Fk_PartID = dbo.ItemPartProperty.Pk_PartPropertyID AND 
                      dbo.Gen_OrderChild.Fk_PartID = dbo.ItemPartProperty.Pk_PartPropertyID
WHERE     (dbo.ItemPartProperty.PName <> 'Outer Shell')
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 126
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Gen_OrderChild"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 126
               Right = 396
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Order"
            Begin Extent = 
               Top = 6
               Left = 434
               Bottom = 126
               Right = 615
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_DeliverySchedule"
            Begin Extent = 
               Top = 6
               Left = 653
               Bottom = 126
               Right = 847
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 6
               Left = 885
               Bottom = 126
               Right = 1059
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxSpecs"
            Begin Extent = 
               Top = 6
               Left = 1097
               Bottom = 126
               Right = 1280
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ItemPartProperty"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 246
               Right = 217
            E' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PartsSchedules'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'nd
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PartsSchedules'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PartsSchedules'
GO
/****** Object:  View [dbo].[Vw_PartPaperLoad]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_PartPaperLoad]
AS
SELECT DISTINCT 
                      TOP (100) PERCENT dbo.PaperStock.RollNo, dbo.BoxMaster.Pk_BoxID, dbo.BoxMaster.Name AS BName, dbo.ItemPartProperty.PName, dbo.ItemPartProperty.Pk_PartPropertyID, 
                      dbo.Inv_Material.Pk_Material, dbo.PaperStock.Quantity, dbo.PaperStock.Pk_PaperStock, dbo.Inv_Material.Name AS MaterialName, dbo.gen_Mill.MillName, dbo.gen_Color.ColorName, 
                      dbo.Inv_Material.GSM, dbo.Inv_Material.BF, dbo.Inv_Material.Deckle
FROM         dbo.BoxMaster INNER JOIN
                      dbo.BoxSpecs ON dbo.BoxMaster.Pk_BoxID = dbo.BoxSpecs.Fk_BoxID INNER JOIN
                      dbo.ItemPartProperty ON dbo.BoxSpecs.Pk_BoxSpecID = dbo.ItemPartProperty.Fk_BoxSpecsID INNER JOIN
                      dbo.Items_Layers ON dbo.ItemPartProperty.Pk_PartPropertyID = dbo.Items_Layers.Fk_PartId INNER JOIN
                      dbo.Inv_Material ON dbo.Items_Layers.GSM = dbo.Inv_Material.GSM INNER JOIN
                      dbo.PaperStock ON dbo.Inv_Material.Pk_Material = dbo.PaperStock.Fk_Material INNER JOIN
                      dbo.gen_Color ON dbo.Inv_Material.Fk_Color = dbo.gen_Color.Pk_Color INNER JOIN
                      dbo.gen_Mill ON dbo.Inv_Material.Fk_Mill = dbo.gen_Mill.Pk_Mill
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[28] 4[31] 2[16] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 129
               Right = 200
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxSpecs"
            Begin Extent = 
               Top = 161
               Left = 305
               Bottom = 418
               Right = 491
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ItemPartProperty"
            Begin Extent = 
               Top = 126
               Left = 37
               Bottom = 309
               Right = 213
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Items_Layers"
            Begin Extent = 
               Top = 0
               Left = 328
               Bottom = 196
               Right = 488
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 0
               Left = 603
               Bottom = 308
               Right = 794
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PaperStock"
            Begin Extent = 
               Top = 137
               Left = 892
               Bottom = 418
               Right = 1052
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Color"
            Begin Extent = 
               Top = 6
               Left = 1091
               Bottom = 96
               Right = 1251
            End
     ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PartPaperLoad'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'       DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Mill"
            Begin Extent = 
               Top = 96
               Left = 1091
               Bottom = 216
               Right = 1257
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 26
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 2145
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 3060
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PartPaperLoad'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PartPaperLoad'
GO
/****** Object:  View [dbo].[Vw_PapReqIndent]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_PapReqIndent]
AS
SELECT     SUM(dbo.Items_Layers.Weight) AS Tot_Layer_Wt, SUM(dbo.Items_Layers.Weight) * dbo.Gen_OrderChild.OrdQty / 1000 AS PaperReq, dbo.Items_Layers.Fk_Material, dbo.BoxMaster.Pk_BoxID, 
                      dbo.Gen_OrderChild.Fk_OrderID, dbo.Gen_OrderChild.OrdQty, dbo.Inv_Material.Name, dbo.BoxMaster.Name AS Box_Name, dbo.Vw_SumPaperStock.Qty, dbo.Inv_Material.GSM, 
                      dbo.Inv_Material.BF, dbo.Inv_Material.Deckle
FROM         dbo.BoxMaster INNER JOIN
                      dbo.BoxSpecs ON dbo.BoxMaster.Pk_BoxID = dbo.BoxSpecs.Fk_BoxID INNER JOIN
                      dbo.ItemPartProperty ON dbo.BoxSpecs.Pk_BoxSpecID = dbo.ItemPartProperty.Fk_BoxSpecsID INNER JOIN
                      dbo.Items_Layers ON dbo.ItemPartProperty.Pk_PartPropertyID = dbo.Items_Layers.Fk_PartId INNER JOIN
                      dbo.Gen_OrderChild ON dbo.BoxMaster.Pk_BoxID = dbo.Gen_OrderChild.Fk_BoxID INNER JOIN
                      dbo.gen_Order ON dbo.Gen_OrderChild.Fk_OrderID = dbo.gen_Order.Pk_Order INNER JOIN
                      dbo.Inv_Material ON dbo.Items_Layers.Fk_Material = dbo.Inv_Material.Pk_Material INNER JOIN
                      dbo.Vw_SumPaperStock ON dbo.Inv_Material.Pk_Material = dbo.Vw_SumPaperStock.Fk_Material
GROUP BY dbo.Items_Layers.Fk_Material, dbo.BoxMaster.Pk_BoxID, dbo.Gen_OrderChild.Fk_OrderID, dbo.Gen_OrderChild.OrdQty, dbo.Inv_Material.Name, dbo.BoxMaster.Name, 
                      dbo.Vw_SumPaperStock.Qty, dbo.Inv_Material.GSM, dbo.Inv_Material.BF, dbo.Inv_Material.Deckle
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[52] 4[10] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = -192
      End
      Begin Tables = 
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 126
               Right = 214
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxSpecs"
            Begin Extent = 
               Top = 6
               Left = 252
               Bottom = 126
               Right = 451
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ItemPartProperty"
            Begin Extent = 
               Top = 6
               Left = 489
               Bottom = 126
               Right = 684
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Items_Layers"
            Begin Extent = 
               Top = 6
               Left = 722
               Bottom = 126
               Right = 898
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Gen_OrderChild"
            Begin Extent = 
               Top = 6
               Left = 936
               Bottom = 126
               Right = 1112
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Order"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 246
               Right = 235
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 126
               Left = 273
               Bottom = 246
               Right = 478
            End
 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PapReqIndent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'           DisplayFlags = 280
            TopColumn = 3
         End
         Begin Table = "Vw_SumPaperStock"
            Begin Extent = 
               Top = 126
               Left = 516
               Bottom = 231
               Right = 692
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PapReqIndent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PapReqIndent'
GO
/****** Object:  View [dbo].[Vw_PaperWtBox]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_PaperWtBox]
AS
SELECT     SUM(dbo.Items_Layers.Weight) AS Expr1, dbo.Inv_Material.Pk_Material, dbo.BoxMaster.Pk_BoxID, dbo.Inv_Material.Name, dbo.Vw_TotPaperStockList.TQty AS stkqty, dbo.ItemPartProperty.PName, 
                      dbo.ItemPartProperty.Quantity, dbo.ItemPartProperty.Pk_PartPropertyID, dbo.Items_Layers.GSM, dbo.gen_Mill.MillName, dbo.gen_Color.ColorName, dbo.ItemPartProperty.PlyVal, 
                      dbo.Items_Layers.Pk_LayerID, dbo.Items_Layers.BF
FROM         dbo.BoxMaster INNER JOIN
                      dbo.BoxSpecs ON dbo.BoxMaster.Pk_BoxID = dbo.BoxSpecs.Fk_BoxID INNER JOIN
                      dbo.ItemPartProperty ON dbo.BoxSpecs.Pk_BoxSpecID = dbo.ItemPartProperty.Fk_BoxSpecsID INNER JOIN
                      dbo.Items_Layers ON dbo.ItemPartProperty.Pk_PartPropertyID = dbo.Items_Layers.Fk_PartId INNER JOIN
                      dbo.Inv_Material ON dbo.Items_Layers.Fk_Material = dbo.Inv_Material.Pk_Material INNER JOIN
                      dbo.gen_Mill ON dbo.Inv_Material.Fk_Mill = dbo.gen_Mill.Pk_Mill INNER JOIN
                      dbo.gen_Color ON dbo.Inv_Material.Fk_Color = dbo.gen_Color.Pk_Color LEFT OUTER JOIN
                      dbo.Vw_TotPaperStockList ON dbo.Inv_Material.Pk_Material = dbo.Vw_TotPaperStockList.Pk_Material
GROUP BY dbo.Inv_Material.Pk_Material, dbo.BoxMaster.Pk_BoxID, dbo.Inv_Material.Name, dbo.Vw_TotPaperStockList.TQty, dbo.ItemPartProperty.PName, dbo.ItemPartProperty.Quantity, 
                      dbo.ItemPartProperty.Pk_PartPropertyID, dbo.Items_Layers.GSM, dbo.gen_Mill.MillName, dbo.gen_Color.ColorName, dbo.ItemPartProperty.PlyVal, dbo.Items_Layers.Pk_LayerID, 
                      dbo.Items_Layers.BF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[49] 4[9] 2[15] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 126
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxSpecs"
            Begin Extent = 
               Top = 0
               Left = 248
               Bottom = 120
               Right = 431
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ItemPartProperty"
            Begin Extent = 
               Top = 2
               Left = 678
               Bottom = 259
               Right = 857
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Items_Layers"
            Begin Extent = 
               Top = 4
               Left = 1038
               Bottom = 274
               Right = 1233
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 131
               Left = 55
               Bottom = 297
               Right = 235
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Mill"
            Begin Extent = 
               Top = 284
               Left = 922
               Bottom = 404
               Right = 1104
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Color"
            Begin Extent = 
               Top = 6
               Left = 490
               Bottom = 96
               Right = 666
            End
          ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PaperWtBox'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'  DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Vw_TotPaperStockList"
            Begin Extent = 
               Top = 135
               Left = 339
               Bottom = 352
               Right = 609
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 15
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 2700
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PaperWtBox'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PaperWtBox'
GO
/****** Object:  View [dbo].[Vw_PaperReqDetIndent]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_PaperReqDetIndent]
AS
SELECT     SUM(dbo.Items_Layers.Weight) AS PaperWeight, SUM(dbo.Items_Layers.Weight) * dbo.Gen_OrderChild.OrdQty AS PaperReq, dbo.Inv_Material.Name, dbo.BoxMaster.Pk_BoxID, 
                      dbo.Gen_OrderChild.Fk_OrderID, dbo.Inv_Material.Pk_Material, dbo.Gen_OrderChild.OrdQty, dbo.BoxMaster.Name AS BName, dbo.gen_Color.ColorName, dbo.gen_Mill.MillName, 
                      dbo.gen_Order.Pk_Order, dbo.gen_Order.OrderDate, dbo.gen_Customer.CustomerName, dbo.gen_Order.Cust_PO, dbo.Gen_OrderChild.Ddate, dbo.gen_Mill.Pk_Mill, dbo.Inv_Material.Deckle, 
                      dbo.Inv_Material.GSM, dbo.Inv_Material.BF
FROM         dbo.BoxMaster INNER JOIN
                      dbo.BoxSpecs ON dbo.BoxMaster.Pk_BoxID = dbo.BoxSpecs.Fk_BoxID INNER JOIN
                      dbo.ItemPartProperty ON dbo.BoxSpecs.Pk_BoxSpecID = dbo.ItemPartProperty.Fk_BoxSpecsID INNER JOIN
                      dbo.Items_Layers ON dbo.ItemPartProperty.Pk_PartPropertyID = dbo.Items_Layers.Fk_PartId INNER JOIN
                      dbo.Gen_OrderChild ON dbo.BoxMaster.Pk_BoxID = dbo.Gen_OrderChild.Fk_BoxID INNER JOIN
                      dbo.gen_Order ON dbo.Gen_OrderChild.Fk_OrderID = dbo.gen_Order.Pk_Order INNER JOIN
                      dbo.Inv_Material ON dbo.Items_Layers.Fk_Material = dbo.Inv_Material.Pk_Material INNER JOIN
                      dbo.gen_Color ON dbo.Inv_Material.Fk_Color = dbo.gen_Color.Pk_Color INNER JOIN
                      dbo.gen_Mill ON dbo.Inv_Material.Fk_Mill = dbo.gen_Mill.Pk_Mill INNER JOIN
                      dbo.gen_Customer ON dbo.BoxMaster.Customer = dbo.gen_Customer.Pk_Customer AND dbo.gen_Order.Fk_Customer = dbo.gen_Customer.Pk_Customer
GROUP BY dbo.Inv_Material.Name, dbo.BoxMaster.Pk_BoxID, dbo.Gen_OrderChild.Fk_OrderID, dbo.Inv_Material.Pk_Material, dbo.Gen_OrderChild.OrdQty, dbo.BoxMaster.Name, dbo.gen_Color.ColorName, 
                      dbo.gen_Mill.MillName, dbo.gen_Order.Pk_Order, dbo.gen_Order.OrderDate, dbo.gen_Customer.CustomerName, dbo.gen_Order.Cust_PO, dbo.Gen_OrderChild.Ddate, dbo.gen_Mill.Pk_Mill, 
                      dbo.Inv_Material.Deckle, dbo.Inv_Material.GSM, dbo.Inv_Material.BF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[53] 4[5] 2[8] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -192
         Left = 0
      End
      Begin Tables = 
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 213
               Right = 214
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxSpecs"
            Begin Extent = 
               Top = 11
               Left = 296
               Bottom = 131
               Right = 495
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ItemPartProperty"
            Begin Extent = 
               Top = 423
               Left = 488
               Bottom = 545
               Right = 672
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Items_Layers"
            Begin Extent = 
               Top = 0
               Left = 819
               Bottom = 244
               Right = 995
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Gen_OrderChild"
            Begin Extent = 
               Top = 4
               Left = 561
               Bottom = 246
               Right = 745
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Order"
            Begin Extent = 
               Top = 6
               Left = 1057
               Bottom = 324
               Right = 1245
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 350
               Left = 947
               Bottom = 543
               Right = 1152
            End' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PaperReqDetIndent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "gen_Color"
            Begin Extent = 
               Top = 371
               Left = 35
               Bottom = 461
               Right = 211
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Mill"
            Begin Extent = 
               Top = 223
               Left = 25
               Bottom = 343
               Right = 207
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 167
               Left = 271
               Bottom = 287
               Right = 461
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 17
         Width = 284
         Width = 1680
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 2550
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PaperReqDetIndent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PaperReqDetIndent'
GO
/****** Object:  View [dbo].[Vw_PaperCertificate]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_PaperCertificate]
AS
SELECT     dbo.PaperCertificate.Pk_Id, dbo.PaperCertificate.Invno, dbo.PaperCertificate.InvDate, dbo.Inv_Material.Name, dbo.Inv_Material.Fk_Mill, dbo.PaperCertificateDetails.Fk_Characteristics, 
                      dbo.PaperCertificateDetails.Target, dbo.PaperCertificateDetails.Acceptance, dbo.PaperCertificateDetails.Result1, dbo.PaperCertificateDetails.Result2, dbo.PaperCertificateDetails.Result3, 
                      dbo.PaperCertificateDetails.Result4, dbo.PaperCertificateDetails.Result5, dbo.PaperCertificateDetails.Result6, dbo.PaperCertificateDetails.MinVal, dbo.PaperCertificateDetails.MaxVal, 
                      dbo.PaperCertificateDetails.AvgVal, dbo.PaperCertificateDetails.Remarks, dbo.PaperCertificate.Quantity, dbo.PaperCertificate.Tested, dbo.PaperCertificate.Approved, dbo.gen_Vendor.VendorName, 
                      dbo.PaperChar.Name AS CName, dbo.PaperChar.UOM, dbo.PaperChar.Pk_CharID, dbo.PaperCertificateDetails.Pk_IdDet, dbo.PaperCertificate.Fk_Material, dbo.PaperChar.TestMethod, 
                      dbo.PaperCertificateDetails.TQty, dbo.PaperCertificateDetails.ReelNo
FROM         dbo.Inv_Material INNER JOIN
                      dbo.PaperCertificate ON dbo.Inv_Material.Pk_Material = dbo.PaperCertificate.Fk_Material INNER JOIN
                      dbo.PaperCertificateDetails ON dbo.PaperCertificate.Pk_Id = dbo.PaperCertificateDetails.Fk_PkID INNER JOIN
                      dbo.PaperChar ON dbo.PaperCertificateDetails.Fk_Characteristics = dbo.PaperChar.Pk_CharID INNER JOIN
                      dbo.gen_Vendor ON dbo.PaperCertificate.Fk_Vendor = dbo.gen_Vendor.Pk_Vendor
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[6] 2[11] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 6
               Left = 14
               Bottom = 269
               Right = 227
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PaperCertificate"
            Begin Extent = 
               Top = 36
               Left = 614
               Bottom = 273
               Right = 861
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PaperChar"
            Begin Extent = 
               Top = 12
               Left = 1081
               Bottom = 227
               Right = 1241
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Vendor"
            Begin Extent = 
               Top = 127
               Left = 916
               Bottom = 334
               Right = 1109
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PaperCertificateDetails"
            Begin Extent = 
               Top = 94
               Left = 291
               Bottom = 257
               Right = 534
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 31
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PaperCertificate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 2640
         Output = 585
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PaperCertificate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PaperCertificate'
GO
/****** Object:  View [dbo].[vw_OrderDeliveryDetails]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_OrderDeliveryDetails]
AS
SELECT        dbo.gen_DeliverySchedule.Pk_DeliverySechedule, dbo.gen_Order.Pk_Order, dbo.gen_DeliverySchedule.DeliveryDate, dbo.gen_DeliverySchedule.Quantity, dbo.gen_Customer.CustomerName, 
                         dbo.gen_Order.Fk_Tanent
FROM            dbo.gen_Order INNER JOIN
                         dbo.gen_DeliverySchedule ON dbo.gen_Order.Pk_Order = dbo.gen_DeliverySchedule.Fk_Order INNER JOIN
                         dbo.gen_Customer ON dbo.gen_Order.Fk_Customer = dbo.gen_Customer.Pk_Customer
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "gen_Order"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 263
               Right = 231
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "gen_DeliverySchedule"
            Begin Extent = 
               Top = 6
               Left = 269
               Bottom = 222
               Right = 472
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 6
               Left = 510
               Bottom = 136
               Right = 693
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 3375
         Width = 1500
         Width = 3030
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_OrderDeliveryDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_OrderDeliveryDetails'
GO
/****** Object:  View [dbo].[Vw_TSchQty]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_TSchQty]
AS
SELECT     SUM(dbo.gen_DeliverySchedule.Quantity) AS TQty, dbo.BoxMaster.Pk_BoxID, dbo.BoxMaster.Name, dbo.gen_Order.Pk_Order, dbo.gen_DeliverySchedule.Quantity, 
                      dbo.gen_DeliverySchedule.Fk_BoxID, dbo.gen_DeliverySchedule.Fk_PartID
FROM         dbo.BoxMaster INNER JOIN
                      dbo.gen_DeliverySchedule ON dbo.BoxMaster.Pk_BoxID = dbo.gen_DeliverySchedule.Fk_BoxID INNER JOIN
                      dbo.gen_Order ON dbo.gen_DeliverySchedule.Fk_Order = dbo.gen_Order.Pk_Order
GROUP BY dbo.BoxMaster.Pk_BoxID, dbo.BoxMaster.Name, dbo.gen_Order.Pk_Order, dbo.gen_DeliverySchedule.Quantity, dbo.gen_DeliverySchedule.Fk_BoxID, 
                      dbo.gen_DeliverySchedule.Fk_PartID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[48] 4[15] 2[5] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 126
               Right = 214
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_DeliverySchedule"
            Begin Extent = 
               Top = 42
               Left = 966
               Bottom = 319
               Right = 1176
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Order"
            Begin Extent = 
               Top = 0
               Left = 658
               Bottom = 319
               Right = 860
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_TSchQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_TSchQty'
GO
/****** Object:  View [dbo].[Vw_Schedules]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_Schedules]
AS
SELECT DISTINCT 
                      dbo.gen_DeliverySchedule.DeliveryDate, dbo.gen_DeliverySchedule.Quantity AS SchQty, dbo.BoxMaster.Pk_BoxID, dbo.BoxMaster.Name, dbo.Gen_OrderChild.OrdQty, dbo.gen_Order.Pk_Order, 
                      dbo.gen_Order.OrderDate, dbo.gen_Customer.CustomerName, dbo.gen_DeliverySchedule.Pk_DeliverySechedule, dbo.gen_Order.Cust_PO, dbo.gen_Order.Fk_Enquiry, dbo.ItemPartProperty.PName, 
                      dbo.ItemPartProperty.Pk_PartPropertyID, dbo.gen_DeliverySchedule.Fk_BoxID
FROM         dbo.BoxMaster INNER JOIN
                      dbo.Gen_OrderChild ON dbo.BoxMaster.Pk_BoxID = dbo.Gen_OrderChild.Fk_BoxID INNER JOIN
                      dbo.gen_Order ON dbo.Gen_OrderChild.Fk_OrderID = dbo.gen_Order.Pk_Order INNER JOIN
                      dbo.gen_DeliverySchedule ON dbo.BoxMaster.Pk_BoxID = dbo.gen_DeliverySchedule.Fk_BoxID AND dbo.gen_Order.Pk_Order = dbo.gen_DeliverySchedule.Fk_Order INNER JOIN
                      dbo.gen_Customer ON dbo.gen_Order.Fk_Customer = dbo.gen_Customer.Pk_Customer INNER JOIN
                      dbo.BoxSpecs ON dbo.BoxMaster.Pk_BoxID = dbo.BoxSpecs.Fk_BoxID INNER JOIN
                      dbo.ItemPartProperty ON dbo.BoxSpecs.Pk_BoxSpecID = dbo.ItemPartProperty.Fk_BoxSpecsID AND dbo.gen_DeliverySchedule.Fk_PartID = dbo.ItemPartProperty.Pk_PartPropertyID AND 
                      dbo.Gen_OrderChild.Fk_PartID = dbo.ItemPartProperty.Pk_PartPropertyID
WHERE     (dbo.gen_DeliverySchedule.Pk_DeliverySechedule NOT IN
                          (SELECT     Fk_Schedule
                            FROM          dbo.JobCardMaster
                            WHERE      (Fk_Status <> 11)))
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[23] 4[23] 2[27] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 5
               Left = 0
               Bottom = 125
               Right = 160
            End
            DisplayFlags = 280
            TopColumn = 3
         End
         Begin Table = "Gen_OrderChild"
            Begin Extent = 
               Top = 8
               Left = 1154
               Bottom = 277
               Right = 1341
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Order"
            Begin Extent = 
               Top = 0
               Left = 813
               Bottom = 218
               Right = 994
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_DeliverySchedule"
            Begin Extent = 
               Top = 222
               Left = 882
               Bottom = 424
               Right = 1120
            End
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 136
               Left = 0
               Bottom = 321
               Right = 174
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxSpecs"
            Begin Extent = 
               Top = 22
               Left = 237
               Bottom = 274
               Right = 420
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ItemPartProperty"
            Begin Extent = 
               Top = 0
               Left = 516
               Bottom = 343
               Right = 695
            ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Schedules'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 16
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 6330
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Schedules'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Schedules'
GO
/****** Object:  View [dbo].[Vw_NetPapReqd]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_NetPapReqd]
AS
SELECT     SUM(dbo.Vw_PaperWtBox.Expr1 * dbo.Vw_PaperWtBox.Quantity * dbo.gen_DeliverySchedule.Quantity) AS PaperReq, dbo.Vw_PaperWtBox.Pk_Material, dbo.Vw_PaperWtBox.Name, 
                      dbo.Vw_PaperWtBox.MillName, dbo.Vw_PaperWtBox.ColorName, dbo.Vw_PaperWtBox.stkqty, dbo.gen_DeliverySchedule.DeliveryDate, dbo.gen_Customer.CustomerName, 
                      dbo.BoxMaster.Name AS Box_Name, dbo.Vw_PaperWtBox.PName, dbo.ItemPartProperty.Length, dbo.ItemPartProperty.Width, dbo.ItemPartProperty.Height, dbo.gen_DeliverySchedule.Quantity, 
                      dbo.Vw_PaperWtBox.Expr1 AS LayerWt, dbo.BoxMaster.Pk_BoxID, dbo.Vw_PaperWtBox.PlyVal, dbo.ItemPartProperty.Deckle, dbo.Inv_Material.GSM, dbo.Inv_Material.BF, 
                      CEILING(132 / dbo.ItemPartProperty.Deckle) AS Ups, (dbo.ItemPartProperty.Length + dbo.ItemPartProperty.Width) * CEILING(132 / dbo.ItemPartProperty.Deckle) + 40 / 10 AS CDeck, 
                      (2 * dbo.ItemPartProperty.Length + 2 * dbo.ItemPartProperty.Width) + 50 / 10 AS CL
FROM         dbo.Inv_Material INNER JOIN
                      dbo.gen_DeliverySchedule INNER JOIN
                      dbo.Vw_PaperWtBox ON dbo.gen_DeliverySchedule.Fk_PartID = dbo.Vw_PaperWtBox.Pk_PartPropertyID INNER JOIN
                      dbo.BoxMaster ON dbo.gen_DeliverySchedule.Fk_BoxID = dbo.BoxMaster.Pk_BoxID INNER JOIN
                      dbo.gen_Customer ON dbo.BoxMaster.Customer = dbo.gen_Customer.Pk_Customer ON dbo.Inv_Material.Pk_Material = dbo.Vw_PaperWtBox.Pk_Material RIGHT OUTER JOIN
                      dbo.ItemPartProperty ON dbo.gen_DeliverySchedule.Fk_PartID = dbo.ItemPartProperty.Pk_PartPropertyID
WHERE     (dbo.ItemPartProperty.Deckle > 0)
GROUP BY dbo.Vw_PaperWtBox.Pk_Material, dbo.Vw_PaperWtBox.Name, dbo.Vw_PaperWtBox.MillName, dbo.Vw_PaperWtBox.ColorName, dbo.Vw_PaperWtBox.stkqty, 
                      dbo.gen_DeliverySchedule.DeliveryDate, dbo.gen_Customer.CustomerName, dbo.BoxMaster.Name, dbo.Vw_PaperWtBox.PName, dbo.ItemPartProperty.Length, dbo.ItemPartProperty.Width, 
                      dbo.ItemPartProperty.Height, dbo.gen_DeliverySchedule.Quantity, dbo.Vw_PaperWtBox.Expr1, dbo.BoxMaster.Pk_BoxID, dbo.Vw_PaperWtBox.PlyVal, dbo.ItemPartProperty.Deckle, 
                      dbo.Inv_Material.GSM, dbo.Inv_Material.BF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[38] 4[5] 2[23] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "gen_DeliverySchedule"
            Begin Extent = 
               Top = 0
               Left = 1008
               Bottom = 263
               Right = 1202
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Vw_PaperWtBox"
            Begin Extent = 
               Top = 0
               Left = 26
               Bottom = 263
               Right = 220
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 4
               Left = 278
               Bottom = 193
               Right = 438
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 35
               Left = 503
               Bottom = 155
               Right = 693
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ItemPartProperty"
            Begin Extent = 
               Top = 0
               Left = 758
               Bottom = 227
               Right = 953
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 55
               Left = 343
               Bottom = 254
               Right = 552
            End
            DisplayFlags = 280
            TopColumn = 1
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 21
         Width = 284
         Width' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_NetPapReqd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N' = 1500
         Width = 1500
         Width = 2415
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1380
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_NetPapReqd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_NetPapReqd'
GO
/****** Object:  View [dbo].[Vw_SalesOrderTracker]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_SalesOrderTracker]
AS
SELECT     dbo.Gen_OrderChild.OrdQty, dbo.gen_Order.Pk_Order, dbo.gen_Order.OrderDate, dbo.Vw_BoxDet.Name, dbo.Vw_BoxDet.PName, dbo.Inv_Billing.Pk_Invoice, dbo.Inv_Billing.InvDate, 
                      dbo.Inv_BillingDetails.Quantity, dbo.Vw_BoxDet.Pk_BoxID, dbo.gen_Order.Cust_PO
FROM         dbo.Gen_OrderChild INNER JOIN
                      dbo.gen_Order ON dbo.Gen_OrderChild.Fk_OrderID = dbo.gen_Order.Pk_Order INNER JOIN
                      dbo.Inv_Billing ON dbo.gen_Order.Pk_Order = dbo.Inv_Billing.Fk_OrderNo INNER JOIN
                      dbo.Vw_BoxDet ON dbo.Gen_OrderChild.Fk_PartID = dbo.Vw_BoxDet.Pk_PartPropertyID INNER JOIN
                      dbo.Inv_BillingDetails ON dbo.Inv_Billing.Pk_Invoice = dbo.Inv_BillingDetails.Fk_Invoice AND dbo.Gen_OrderChild.Fk_PartID = dbo.Inv_BillingDetails.Fk_PartID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[49] 4[4] 2[5] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Gen_OrderChild"
            Begin Extent = 
               Top = 15
               Left = 493
               Bottom = 235
               Right = 703
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Order"
            Begin Extent = 
               Top = 43
               Left = 289
               Bottom = 367
               Right = 472
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Billing"
            Begin Extent = 
               Top = 143
               Left = 28
               Bottom = 306
               Right = 204
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Vw_BoxDet"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 139
               Right = 221
            End
            DisplayFlags = 280
            TopColumn = 7
         End
         Begin Table = "Inv_BillingDetails"
            Begin Extent = 
               Top = 2
               Left = 786
               Bottom = 307
               Right = 946
            End
            DisplayFlags = 280
            TopColumn = 6
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 11
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin Colu' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_SalesOrderTracker'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'mnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_SalesOrderTracker'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_SalesOrderTracker'
GO
/****** Object:  View [dbo].[Vw_PPC]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_PPC]
AS
SELECT DISTINCT 
                      dbo.Vw_PaperWtBox.Pk_LayerID, dbo.Vw_PaperWtBox.Pk_Material, dbo.Vw_PaperWtBox.Name, dbo.Vw_PaperWtBox.MillName, dbo.Vw_PaperWtBox.ColorName, dbo.Vw_PaperWtBox.stkqty, 
                      dbo.gen_DeliverySchedule.DeliveryDate, dbo.gen_Customer.CustomerName, dbo.BoxMaster.Name AS Box_Name, dbo.Vw_PaperWtBox.PName, dbo.ItemPartProperty.Length, 
                      dbo.ItemPartProperty.Width, dbo.ItemPartProperty.Height, dbo.gen_DeliverySchedule.Quantity, dbo.Vw_PaperWtBox.Expr1, dbo.BoxMaster.Pk_BoxID, dbo.Vw_PaperWtBox.PlyVal, 
                      dbo.ItemPartProperty.Deckle, FLOOR(132 / dbo.ItemPartProperty.Deckle) AS Ups, (dbo.ItemPartProperty.Length + dbo.ItemPartProperty.Width) * FLOOR(132 / dbo.ItemPartProperty.Deckle) 
                      + 40 / 10 AS CDeck, (2 * dbo.ItemPartProperty.Length + 2 * dbo.ItemPartProperty.Width) + 50 / 10 AS CL, dbo.gen_DeliverySchedule.Pk_DeliverySechedule, dbo.ItemPartProperty.Pk_PartPropertyID, 
                      dbo.Vw_PaperWtBox.Expr1 AS LayerWt, dbo.FluteType.FluteName, dbo.ItemPartProperty.Weight, dbo.Vw_PaperWtBox.GSM, dbo.Vw_PaperWtBox.BF
FROM         dbo.ItemPartProperty LEFT OUTER JOIN
                      dbo.gen_DeliverySchedule INNER JOIN
                      dbo.Vw_PaperWtBox ON dbo.gen_DeliverySchedule.Fk_PartID = dbo.Vw_PaperWtBox.Pk_PartPropertyID INNER JOIN
                      dbo.BoxMaster ON dbo.gen_DeliverySchedule.Fk_BoxID = dbo.BoxMaster.Pk_BoxID INNER JOIN
                      dbo.gen_Customer ON dbo.BoxMaster.Customer = dbo.gen_Customer.Pk_Customer INNER JOIN
                      dbo.FluteType ON dbo.BoxMaster.Fk_FluteType = dbo.FluteType.Pk_FluteID ON dbo.ItemPartProperty.Pk_PartPropertyID = dbo.gen_DeliverySchedule.Fk_PartID
WHERE     (dbo.ItemPartProperty.Deckle > 0)
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[25] 4[25] 3[6] 2) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ItemPartProperty"
            Begin Extent = 
               Top = 6
               Left = 1124
               Bottom = 276
               Right = 1303
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_DeliverySchedule"
            Begin Extent = 
               Top = 14
               Left = 23
               Bottom = 287
               Right = 217
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Vw_PaperWtBox"
            Begin Extent = 
               Top = 11
               Left = 297
               Bottom = 284
               Right = 476
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 0
               Left = 519
               Bottom = 270
               Right = 679
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 6
               Left = 912
               Bottom = 249
               Right = 1086
            End
            DisplayFlags = 280
            TopColumn = 8
         End
         Begin Table = "FluteType"
            Begin Extent = 
               Top = 32
               Left = 697
               Bottom = 152
               Right = 873
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 32
         Width = 284
         Width ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PPC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'= 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 1710
         Table = 1950
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PPC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PPC'
GO
/****** Object:  View [dbo].[Vw_IssueScheduleList]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_IssueScheduleList]
AS
SELECT DISTINCT 
                      dbo.gen_DeliverySchedule.DeliveryDate, dbo.gen_DeliverySchedule.Quantity AS SchQty, dbo.BoxMaster.Pk_BoxID, dbo.BoxMaster.Name, dbo.Gen_OrderChild.OrdQty, dbo.gen_Order.Pk_Order, 
                      dbo.gen_Order.OrderDate, dbo.gen_Customer.CustomerName, dbo.gen_DeliverySchedule.Pk_DeliverySechedule, dbo.gen_Order.Cust_PO, dbo.gen_Order.Fk_Enquiry, dbo.ItemPartProperty.PName, 
                      dbo.ItemPartProperty.Pk_PartPropertyID, dbo.gen_DeliverySchedule.Fk_BoxID, dbo.JobCardMaster.Pk_JobCardID
FROM         dbo.BoxMaster INNER JOIN
                      dbo.Gen_OrderChild ON dbo.BoxMaster.Pk_BoxID = dbo.Gen_OrderChild.Fk_BoxID INNER JOIN
                      dbo.gen_Order ON dbo.Gen_OrderChild.Fk_OrderID = dbo.gen_Order.Pk_Order INNER JOIN
                      dbo.gen_DeliverySchedule ON dbo.BoxMaster.Pk_BoxID = dbo.gen_DeliverySchedule.Fk_BoxID AND dbo.gen_Order.Pk_Order = dbo.gen_DeliverySchedule.Fk_Order INNER JOIN
                      dbo.gen_Customer ON dbo.gen_Order.Fk_Customer = dbo.gen_Customer.Pk_Customer INNER JOIN
                      dbo.BoxSpecs ON dbo.BoxMaster.Pk_BoxID = dbo.BoxSpecs.Fk_BoxID INNER JOIN
                      dbo.ItemPartProperty ON dbo.BoxSpecs.Pk_BoxSpecID = dbo.ItemPartProperty.Fk_BoxSpecsID AND dbo.gen_DeliverySchedule.Fk_PartID = dbo.ItemPartProperty.Pk_PartPropertyID AND 
                      dbo.Gen_OrderChild.Fk_PartID = dbo.ItemPartProperty.Pk_PartPropertyID INNER JOIN
                      dbo.JobCardMaster ON dbo.BoxMaster.Pk_BoxID = dbo.JobCardMaster.Fk_BoxID AND dbo.gen_Order.Pk_Order = dbo.JobCardMaster.Fk_Order AND 
                      dbo.gen_DeliverySchedule.Pk_DeliverySechedule = dbo.JobCardMaster.Fk_Schedule
WHERE     (dbo.gen_DeliverySchedule.Pk_DeliverySechedule IN
                          (SELECT     Fk_Schedule
                            FROM          dbo.JobCardMaster AS JobCardMaster_1))
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[55] 4[6] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 126
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Gen_OrderChild"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 269
               Right = 403
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Order"
            Begin Extent = 
               Top = 285
               Left = 451
               Bottom = 548
               Right = 632
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_DeliverySchedule"
            Begin Extent = 
               Top = 134
               Left = 753
               Bottom = 397
               Right = 947
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 334
               Left = 834
               Bottom = 454
               Right = 1008
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxSpecs"
            Begin Extent = 
               Top = 6
               Left = 1097
               Bottom = 126
               Right = 1280
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ItemPartProperty"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 246
               Right = 217
        ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_IssueScheduleList'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'    End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "JobCardMaster"
            Begin Extent = 
               Top = 137
               Left = 1104
               Bottom = 366
               Right = 1273
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_IssueScheduleList'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_IssueScheduleList'
GO
/****** Object:  View [dbo].[Vw_JCReport]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_JCReport]
AS
SELECT     dbo.BoxMaster.Pk_BoxID, dbo.BoxMaster.Name AS BName, dbo.BoxMaster.Description, dbo.gen_Order.Pk_Order, dbo.gen_Order.OrderDate, dbo.gen_Order.Product, 
                      dbo.gen_Order.Quantity AS OrdQty, dbo.gen_Order.Fk_BoxID, dbo.gen_Order.Cust_PO, dbo.gen_Customer.CustomerName, dbo.ItemPartProperty.CuttingSize, 
                      dbo.ItemPartProperty.TakeUpFactor AS PartTakeup, dbo.ItemPartProperty.PName, dbo.ItemPartProperty.Length, dbo.ItemPartProperty.Width, dbo.ItemPartProperty.Height, 
                      dbo.ItemPartProperty.Quantity AS TotalQty, dbo.ItemPartProperty.Deckle, dbo.ItemPartProperty.BoardGSM, dbo.ItemPartProperty.BoardBS, dbo.ItemPartProperty.PlyVal, 
                      dbo.FluteType.Height AS FluteHt, dbo.FluteType.FluteName, dbo.Items_Layers.GSM, dbo.Items_Layers.BF, dbo.Items_Layers.Color AS ColorName, dbo.ItemPartProperty.Weight, 
                      dbo.gen_DeliverySchedule.Quantity AS SchQty, dbo.gen_DeliverySchedule.DeliveryDate, dbo.Items_Layers.Tk_Factor, dbo.ItemPartProperty.BoardArea, dbo.JobCardMaster.Pk_JobCardID, 
                      dbo.JobCardMaster.JDate, dbo.JobCardMaster.Printing, dbo.JobCardMaster.Calico, dbo.JobCardMaster.Others, dbo.JobCardMaster.Invno, dbo.JobCardMaster.Corrugation, 
                      dbo.JobCardMaster.TopPaperQty, dbo.JobCardMaster.TwoPlyQty, dbo.JobCardMaster.TwoPlyWt, dbo.JobCardMaster.PastingQty, dbo.JobCardMaster.RotaryQty, dbo.JobCardMaster.PunchingQty, 
                      dbo.JobCardMaster.SlotingQty, dbo.JobCardMaster.PinningQty, dbo.JobCardMaster.FinishingQty, dbo.JobCardMaster.TotalQty AS ProdQty, dbo.JobCardMaster.Machine, 
                      dbo.JobCardMaster.ChkQuality, dbo.JobCardMaster.PColor, dbo.JobCardMaster.PDetails, dbo.JobCardMaster.MFG, dbo.JobCardMaster.DPRSc, dbo.JobCardMaster.ECCQty, 
                      dbo.JobCardMaster.GummingQty, dbo.JobCardMaster.UpsVal, dbo.JobCardMaster.CutLength, dbo.JobCardMaster.Stitching, dbo.gen_Mill.MillName, dbo.Items_Layers.Pk_LayerID, 
                      dbo.ItemPartProperty.Rate, dbo.JobCardMaster.Corr, dbo.JobCardMaster.TopP, dbo.JobCardMaster.Rotary, dbo.JobCardMaster.Pasting, dbo.JobCardMaster.PrintingP, dbo.JobCardMaster.Punching, 
                      dbo.JobCardMaster.Slotting, dbo.JobCardMaster.Finishing, dbo.JobCardMaster.Pinning, dbo.JobCardMaster.Gumming, dbo.JobCardMaster.Bundling
FROM         dbo.Inv_Material INNER JOIN
                      dbo.gen_Mill ON dbo.Inv_Material.Fk_Mill = dbo.gen_Mill.Pk_Mill INNER JOIN
                      dbo.gen_DeliverySchedule INNER JOIN
                      dbo.gen_Customer INNER JOIN
                      dbo.gen_Order ON dbo.gen_Customer.Pk_Customer = dbo.gen_Order.Fk_Customer ON dbo.gen_DeliverySchedule.Fk_Order = dbo.gen_Order.Pk_Order INNER JOIN
                      dbo.FluteType INNER JOIN
                      dbo.BoxSpecs INNER JOIN
                      dbo.BoxMaster ON dbo.BoxSpecs.Fk_BoxID = dbo.BoxMaster.Pk_BoxID INNER JOIN
                      dbo.ItemPartProperty ON dbo.BoxSpecs.Pk_BoxSpecID = dbo.ItemPartProperty.Fk_BoxSpecsID INNER JOIN
                      dbo.Items_Layers ON dbo.ItemPartProperty.Pk_PartPropertyID = dbo.Items_Layers.Fk_PartId ON dbo.FluteType.Pk_FluteID = dbo.BoxMaster.Fk_FluteType ON 
                      dbo.gen_DeliverySchedule.Fk_BoxID = dbo.BoxMaster.Pk_BoxID AND dbo.gen_DeliverySchedule.Fk_PartID = dbo.ItemPartProperty.Pk_PartPropertyID INNER JOIN
                      dbo.JobCardMaster ON dbo.gen_DeliverySchedule.Pk_DeliverySechedule = dbo.JobCardMaster.Fk_Schedule AND dbo.gen_Order.Pk_Order = dbo.JobCardMaster.Fk_Order AND 
                      dbo.BoxMaster.Pk_BoxID = dbo.JobCardMaster.Fk_BoxID ON dbo.Inv_Material.Pk_Material = dbo.Items_Layers.Fk_Material
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[59] 4[6] 2[7] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -96
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 334
               Left = 608
               Bottom = 516
               Right = 797
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Mill"
            Begin Extent = 
               Top = 0
               Left = 770
               Bottom = 119
               Right = 936
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_DeliverySchedule"
            Begin Extent = 
               Top = 87
               Left = 1534
               Bottom = 360
               Right = 1728
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 0
               Left = 1050
               Bottom = 186
               Right = 1224
            End
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "gen_Order"
            Begin Extent = 
               Top = 204
               Left = 1260
               Bottom = 511
               Right = 1441
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "FluteType"
            Begin Extent = 
               Top = 384
               Left = 38
               Bottom = 504
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxSpecs"
            Begin Extent = 
               Top = 252
               Left = 882
               Bottom = 429
               Right = 1065
           ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_JCReport'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N' End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 0
               Left = 546
               Bottom = 120
               Right = 706
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ItemPartProperty"
            Begin Extent = 
               Top = 0
               Left = 258
               Bottom = 297
               Right = 437
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Items_Layers"
            Begin Extent = 
               Top = 0
               Left = 1333
               Bottom = 287
               Right = 1493
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "JobCardMaster"
            Begin Extent = 
               Top = 6
               Left = 12
               Bottom = 350
               Right = 207
            End
            DisplayFlags = 280
            TopColumn = 36
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 86
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 2340
         Width = 1500
         Width = 2280
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 2490
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_JCReport'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_JCReport'
GO
/****** Object:  View [dbo].[Vw_JCpartsRep]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_JCpartsRep]
AS
SELECT     dbo.BoxMaster.Pk_BoxID, dbo.BoxMaster.Name AS BName, dbo.BoxMaster.Description, dbo.gen_Order.Pk_Order, dbo.gen_Order.OrderDate, dbo.gen_Order.Fk_Enquiry, 
                      dbo.gen_Order.Fk_Customer, dbo.gen_Order.Fk_Branch, dbo.gen_Order.Product, dbo.gen_Order.ShippingAddress, dbo.gen_Order.ShippingInstruction, dbo.gen_Order.SpecialInstructions, 
                      dbo.gen_Order.Quantity, dbo.gen_Order.Price, dbo.gen_Order.Fk_Tanent, dbo.gen_Order.Fk_ShippingId, dbo.gen_Order.Fk_BoxID, dbo.gen_Order.Cust_PO, dbo.gen_Customer.CustomerName, 
                      dbo.Inv_Material.Pk_Material, dbo.ItemPartProperty.CuttingSize, dbo.ItemPartProperty.Rate AS PartRate, dbo.ItemPartProperty.TakeUpFactor AS PartTakeup, dbo.ItemPartProperty.PName, 
                      dbo.ItemPartProperty.Length, dbo.ItemPartProperty.Width, dbo.ItemPartProperty.Height, dbo.ItemPartProperty.Quantity AS TotalQty, dbo.ItemPartProperty.Deckle, dbo.ItemPartProperty.BoardArea, 
                      dbo.ItemPartProperty.BoardGSM, dbo.ItemPartProperty.BoardBS, dbo.ItemPartProperty.PlyVal, dbo.FluteType.Height AS FluteHt, dbo.FluteType.FluteName, dbo.Items_Layers.GSM, 
                      dbo.Items_Layers.BF, dbo.Items_Layers.Color, dbo.Items_Layers.Rate, dbo.Items_Layers.Fk_Material, dbo.Items_Layers.Tk_Factor, dbo.ItemPartProperty.Weight, dbo.gen_Mill.MillName, 
                      dbo.gen_Color.ColorName, dbo.Items_Layers.Pk_LayerID, dbo.gen_DeliverySchedule.Quantity AS SchQty, dbo.JobCardPartsMaster.Fk_PartID, dbo.JobCardPartsMaster.Fk_Order, 
                      dbo.JobCardPartsMaster.JDate, dbo.JobCardPartsMaster.Pk_JobCardPartsID, dbo.JobCardPartsMaster.Printing, dbo.JobCardPartsMaster.Calico, dbo.JobCardPartsMaster.Others, 
                      dbo.gen_DeliverySchedule.DeliveryDate, dbo.JobCardPartsMaster.Invno
FROM         dbo.gen_DeliverySchedule INNER JOIN
                      dbo.gen_Customer INNER JOIN
                      dbo.gen_Order ON dbo.gen_Customer.Pk_Customer = dbo.gen_Order.Fk_Customer ON dbo.gen_DeliverySchedule.Fk_Order = dbo.gen_Order.Pk_Order INNER JOIN
                      dbo.FluteType INNER JOIN
                      dbo.BoxSpecs INNER JOIN
                      dbo.BoxMaster ON dbo.BoxSpecs.Fk_BoxID = dbo.BoxMaster.Pk_BoxID INNER JOIN
                      dbo.ItemPartProperty ON dbo.BoxSpecs.Pk_BoxSpecID = dbo.ItemPartProperty.Fk_BoxSpecsID INNER JOIN
                      dbo.Items_Layers ON dbo.ItemPartProperty.Pk_PartPropertyID = dbo.Items_Layers.Fk_PartId INNER JOIN
                      dbo.Inv_Material ON dbo.Items_Layers.Fk_Material = dbo.Inv_Material.Pk_Material ON dbo.FluteType.Pk_FluteID = dbo.BoxMaster.Fk_FluteType INNER JOIN
                      dbo.gen_Mill ON dbo.Inv_Material.Fk_Mill = dbo.gen_Mill.Pk_Mill INNER JOIN
                      dbo.gen_Color ON dbo.Inv_Material.Fk_Color = dbo.gen_Color.Pk_Color ON dbo.gen_DeliverySchedule.Fk_BoxID = dbo.BoxMaster.Pk_BoxID AND 
                      dbo.gen_DeliverySchedule.Fk_PartID = dbo.ItemPartProperty.Pk_PartPropertyID INNER JOIN
                      dbo.JobCardPartsMaster ON dbo.gen_Order.Pk_Order = dbo.JobCardPartsMaster.Fk_Order AND dbo.gen_DeliverySchedule.Pk_DeliverySechedule = dbo.JobCardPartsMaster.Fk_Schedule
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[58] 4[4] 2[13] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "gen_Order"
            Begin Extent = 
               Top = 6
               Left = 245
               Bottom = 126
               Right = 426
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 6
               Left = 662
               Bottom = 126
               Right = 822
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxSpecs"
            Begin Extent = 
               Top = 6
               Left = 860
               Bottom = 126
               Right = 1043
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ItemPartProperty"
            Begin Extent = 
               Top = 6
               Left = 1081
               Bottom = 126
               Right = 1260
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Items_Layers"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 246
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 126
               Left = 236
               Bottom = 246
               Right = 425
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 126
               Left = 463
               Bottom = 246
               Right = 637
            End
  ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_JCpartsRep'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'          DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "FluteType"
            Begin Extent = 
               Top = 126
               Left = 675
               Bottom = 246
               Right = 835
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Mill"
            Begin Extent = 
               Top = 126
               Left = 873
               Bottom = 246
               Right = 1039
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Color"
            Begin Extent = 
               Top = 126
               Left = 1077
               Bottom = 216
               Right = 1237
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_DeliverySchedule"
            Begin Extent = 
               Top = 246
               Left = 38
               Bottom = 366
               Right = 232
            End
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "JobCardPartsMaster"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 269
               Right = 206
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 63
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_JCpartsRep'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_JCpartsRep'
GO
/****** Object:  View [dbo].[Vw_BoxPQty]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_BoxPQty]
AS
SELECT     SUM(dbo.Vw_BoxPaper.Quantity) AS sPaper, dbo.Vw_BoxPaper.Pk_Material, dbo.Vw_BoxOrder.Pk_Order, dbo.Vw_BoxOrder.Name AS BName, dbo.Vw_BoxPaper.Name AS MName, 
                      dbo.Vw_BoxPaper.Quantity AS PaperStkQty, dbo.Vw_BoxOrder.Pk_BoxID, dbo.Vw_BoxPaper.Weight AS LayerPaperWt, dbo.Vw_BoxOrder.OrdQty
FROM         dbo.Vw_BoxOrder INNER JOIN
                      dbo.Vw_BoxPaper ON dbo.Vw_BoxOrder.Pk_BoxID = dbo.Vw_BoxPaper.Pk_BoxID
GROUP BY dbo.Vw_BoxPaper.Pk_Material, dbo.Vw_BoxOrder.Pk_Order, dbo.Vw_BoxOrder.Name, dbo.Vw_BoxPaper.Name, dbo.Vw_BoxPaper.Quantity, dbo.Vw_BoxOrder.Pk_BoxID, 
                      dbo.Vw_BoxPaper.Weight, dbo.Vw_BoxOrder.OrdQty
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Vw_BoxOrder"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 214
               Right = 219
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Vw_BoxPaper"
            Begin Extent = 
               Top = 16
               Left = 326
               Bottom = 216
               Right = 502
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_BoxPQty'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_BoxPQty'
GO
/****** Object:  View [dbo].[Vw_BillingBoxStocks]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_BillingBoxStocks]
AS
SELECT     dbo.Vw_BoxOrder.Name, dbo.Vw_BoxOrder.Pk_BoxID, dbo.Vw_BoxOrder.Pk_Order, dbo.Vw_BoxStock.CustomerName, dbo.Vw_BoxStock.Quantity, dbo.Vw_BoxOrder.PName, 
                      dbo.Vw_BoxStock.Pk_StockID, dbo.Vw_BoxOrder.Pk_PartPropertyID
FROM         dbo.Vw_BoxOrder INNER JOIN
                      dbo.Vw_BoxStock ON dbo.Vw_BoxOrder.Pk_BoxID = dbo.Vw_BoxStock.Pk_BoxID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[2] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Vw_BoxOrder"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 252
               Right = 217
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Vw_BoxStock"
            Begin Extent = 
               Top = 3
               Left = 344
               Bottom = 224
               Right = 523
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_BillingBoxStocks'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_BillingBoxStocks'
GO
/****** Object:  View [dbo].[Vw_COARep]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_COARep]
AS
SELECT     dbo.COA.Pk_ID, dbo.COA.RDate, dbo.COA.Fk_Invno, dbo.Vw_BoxDet.Name, dbo.Vw_BoxDet.Length, dbo.Vw_BoxDet.Width, dbo.Vw_BoxDet.Height, dbo.Vw_BoxDet.PlyVal, 
                      dbo.Vw_BoxDet.Pk_BoxID, dbo.COA_Parameters.ParaName, dbo.gen_Customer.CustomerName, dbo.Inv_BillingDetails.Quantity, dbo.COADetails.Fk_ParaID, dbo.COADetails.UOM, 
                      dbo.COADetails.Specification, dbo.COADetails.Result, dbo.COADetails.Remarks, dbo.COADetails.Pk_IDDet
FROM         dbo.Vw_BoxDet RIGHT OUTER JOIN
                      dbo.gen_Order RIGHT OUTER JOIN
                      dbo.Inv_Billing INNER JOIN
                      dbo.Gen_OrderChild ON dbo.Inv_Billing.Fk_OrderNo = dbo.Gen_OrderChild.Fk_OrderID INNER JOIN
                      dbo.gen_Customer ON dbo.Inv_Billing.Fk_Customer = dbo.gen_Customer.Pk_Customer INNER JOIN
                      dbo.Inv_BillingDetails ON dbo.Inv_Billing.Pk_Invoice = dbo.Inv_BillingDetails.Fk_Invoice ON dbo.gen_Order.Fk_Customer = dbo.gen_Customer.Pk_Customer AND 
                      dbo.gen_Order.Pk_Order = dbo.Gen_OrderChild.Fk_OrderID AND dbo.gen_Order.Pk_Order = dbo.Inv_Billing.Fk_OrderNo ON dbo.Vw_BoxDet.Pk_PartPropertyID = dbo.Gen_OrderChild.Fk_PartID AND 
                      dbo.Vw_BoxDet.Pk_BoxID = dbo.Gen_OrderChild.Fk_BoxID RIGHT OUTER JOIN
                      dbo.COA INNER JOIN
                      dbo.COA_Parameters INNER JOIN
                      dbo.COADetails ON dbo.COA_Parameters.Pk_ParaID = dbo.COADetails.Fk_ParaID ON dbo.COA.Pk_ID = dbo.COADetails.Fk_MastID ON dbo.Inv_Billing.Pk_Invoice = dbo.COA.Fk_Invno
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[39] 4[21] 2[11] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "COA"
            Begin Extent = 
               Top = 5
               Left = 45
               Bottom = 118
               Right = 205
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Billing"
            Begin Extent = 
               Top = 0
               Left = 447
               Bottom = 287
               Right = 604
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "Gen_OrderChild"
            Begin Extent = 
               Top = 184
               Left = 846
               Bottom = 489
               Right = 1006
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Order"
            Begin Extent = 
               Top = 0
               Left = 720
               Bottom = 120
               Right = 901
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Vw_BoxDet"
            Begin Extent = 
               Top = 121
               Left = 0
               Bottom = 373
               Right = 162
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "COA_Parameters"
            Begin Extent = 
               Top = 307
               Left = 553
               Bottom = 397
               Right = 713
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 6
               Left = 243
               Bottom = 126
               Right = 417
            End
           ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_COARep'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N' DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "COADetails"
            Begin Extent = 
               Top = 38
               Left = 1137
               Bottom = 247
               Right = 1297
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_BillingDetails"
            Begin Extent = 
               Top = 264
               Left = 1126
               Bottom = 384
               Right = 1286
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 18
         Width = 284
         Width = 1020
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_COARep'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_COARep'
GO
/****** Object:  View [dbo].[Vw_FinishingQty_Wt]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_FinishingQty_Wt]
AS
SELECT     dbo.JobProcess.Pk_ID, dbo.JobProcess.Fk_ProcessID, dbo.JobProcess.WastageQty, dbo.JobProcess.Quantity, dbo.JobProcess.ProcessDate, dbo.JobProcess.PQuantity, 
                      dbo.JobProcess.PStartTime, dbo.JobProcess.PEndTime, dbo.JobProcess.ReasonWastage, dbo.JobProcess.RemainingQty, dbo.JobProcess.WQty, dbo.JobCardMaster.Pk_JobCardID, 
                      dbo.ProcessMaster.ProcessName, dbo.Vw_BoxDet.Weight, dbo.Vw_BoxDet.PName, dbo.Vw_BoxDet.Weight * dbo.JobProcess.PQuantity AS PrdnWt
FROM         dbo.JobCardMaster INNER JOIN
                      dbo.JobProcess ON dbo.JobCardMaster.Pk_JobCardID = dbo.JobProcess.Fk_JobCardID INNER JOIN
                      dbo.ProcessMaster ON dbo.JobProcess.Fk_ProcessID = dbo.ProcessMaster.Pk_Process INNER JOIN
                      dbo.Vw_BoxDet ON dbo.JobCardMaster.Fk_BoxID = dbo.Vw_BoxDet.Pk_BoxID INNER JOIN
                      dbo.gen_DeliverySchedule ON dbo.JobCardMaster.Fk_Schedule = dbo.gen_DeliverySchedule.Pk_DeliverySechedule AND 
                      dbo.Vw_BoxDet.Pk_PartPropertyID = dbo.gen_DeliverySchedule.Fk_PartID
WHERE     (dbo.ProcessMaster.ProcessName = 'Finishing')
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[22] 4[4] 2[24] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "JobCardMaster"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 269
               Right = 222
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "JobProcess"
            Begin Extent = 
               Top = 6
               Left = 245
               Bottom = 126
               Right = 413
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ProcessMaster"
            Begin Extent = 
               Top = 3
               Left = 691
               Bottom = 93
               Right = 851
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Vw_BoxDet"
            Begin Extent = 
               Top = 4
               Left = 894
               Bottom = 442
               Right = 1074
            End
            DisplayFlags = 280
            TopColumn = 3
         End
         Begin Table = "gen_DeliverySchedule"
            Begin Extent = 
               Top = 203
               Left = 443
               Bottom = 442
               Right = 637
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 17
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Wi' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_FinishingQty_Wt'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'dth = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_FinishingQty_Wt'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_FinishingQty_Wt'
GO
/****** Object:  View [dbo].[Vw_BoxPReq]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_BoxPReq]
AS
SELECT DISTINCT BName, Pk_Order, MName, Pk_Material, sPaper, OrdQty, OrdQty * LayerPaperWt AS PReq, Pk_BoxID, LayerPaperWt
FROM         dbo.Vw_BoxPQty
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[42] 4[15] 2[13] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Vw_BoxPQty"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 126
               Right = 214
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_BoxPReq'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_BoxPReq'
GO
/****** Object:  View [dbo].[Vw_JCPartDet]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_JCPartDet]
AS
SELECT     dbo.JobCardPartsMaster.Pk_JobCardPartsID, dbo.JobCardPartsMaster.JDate, dbo.JobCardPartsMaster.Fk_Order, dbo.JobCardPartsMaster.Fk_PartID, dbo.Inv_Material.Pk_Material, 
                      dbo.Inv_Material.Name, dbo.BoxMaster.Name AS BoxName, dbo.ItemPartProperty.PName, dbo.ItemPartProperty.Pk_PartPropertyID, dbo.PaperStock.Pk_PaperStock, dbo.PaperStock.RollNo, 
                      dbo.gen_Mill.MillName, dbo.JobCardPartsDetails.RM_Consumed
FROM         dbo.Gen_OrderChild INNER JOIN
                      dbo.gen_Order ON dbo.Gen_OrderChild.Fk_OrderID = dbo.gen_Order.Pk_Order INNER JOIN
                      dbo.ItemPartProperty ON dbo.Gen_OrderChild.Fk_PartID = dbo.ItemPartProperty.Pk_PartPropertyID INNER JOIN
                      dbo.BoxMaster ON dbo.Gen_OrderChild.Fk_BoxID = dbo.BoxMaster.Pk_BoxID INNER JOIN
                      dbo.JobCardPartsMaster ON dbo.gen_Order.Pk_Order = dbo.JobCardPartsMaster.Fk_Order AND dbo.ItemPartProperty.Pk_PartPropertyID = dbo.JobCardPartsMaster.Fk_PartID INNER JOIN
                      dbo.JobCardPartsDetails ON dbo.JobCardPartsMaster.Pk_JobCardPartsID = dbo.JobCardPartsDetails.Fk_JobCardPartsID INNER JOIN
                      dbo.Inv_Material ON dbo.JobCardPartsDetails.Fk_Material = dbo.Inv_Material.Pk_Material INNER JOIN
                      dbo.PaperStock ON dbo.JobCardPartsDetails.Fk_PaperStock = dbo.PaperStock.Pk_PaperStock AND dbo.Inv_Material.Pk_Material = dbo.PaperStock.Fk_Material INNER JOIN
                      dbo.gen_Mill ON dbo.Inv_Material.Fk_Mill = dbo.gen_Mill.Pk_Mill
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[57] 4[17] 2[6] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Gen_OrderChild"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 126
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Order"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 126
               Right = 417
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 26
               Left = 487
               Bottom = 169
               Right = 676
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ItemPartProperty"
            Begin Extent = 
               Top = 129
               Left = 781
               Bottom = 392
               Right = 960
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 6
               Left = 899
               Bottom = 126
               Right = 1059
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "JobCardPartsMaster"
            Begin Extent = 
               Top = 82
               Left = 1132
               Bottom = 253
               Right = 1355
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "JobCardPartsDetails"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 278
               Right = 223
 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_JCPartDet'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'           End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PaperStock"
            Begin Extent = 
               Top = 248
               Left = 419
               Bottom = 380
               Right = 575
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Mill"
            Begin Extent = 
               Top = 279
               Left = 6
               Bottom = 399
               Right = 172
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 15
         Width = 284
         Width = 1500
         Width = 2040
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_JCPartDet'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_JCPartDet'
GO
/****** Object:  View [dbo].[Vw_ProcessWastage]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_ProcessWastage]
AS
SELECT     TOP (100) PERCENT dbo.JobProcess.Pk_ID, dbo.JobProcess.Fk_ProcessID, dbo.JobProcess.WastageQty, dbo.JobProcess.Quantity, dbo.JobProcess.ProcessDate, dbo.JobProcess.PQuantity, 
                      dbo.JobProcess.PStartTime, dbo.JobProcess.PEndTime, dbo.JobProcess.ReasonWastage, dbo.JobProcess.RemainingQty, dbo.JobProcess.WQty, dbo.JobCardMaster.Pk_JobCardID, 
                      dbo.ProcessMaster.ProcessName, dbo.ProcessMaster.Pk_Process
FROM         dbo.JobCardMaster INNER JOIN
                      dbo.JobProcess ON dbo.JobCardMaster.Pk_JobCardID = dbo.JobProcess.Fk_JobCardID INNER JOIN
                      dbo.ProcessMaster ON dbo.JobProcess.Fk_ProcessID = dbo.ProcessMaster.Pk_Process
ORDER BY dbo.JobProcess.Pk_ID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[32] 4[26] 2[14] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = -96
      End
      Begin Tables = 
         Begin Table = "JobCardMaster"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 126
               Right = 207
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "JobProcess"
            Begin Extent = 
               Top = 12
               Left = 601
               Bottom = 234
               Right = 769
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ProcessMaster"
            Begin Extent = 
               Top = 120
               Left = 211
               Bottom = 210
               Right = 371
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 16
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_ProcessWastage'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_ProcessWastage'
GO
/****** Object:  View [dbo].[Vw_ProcessJC]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_ProcessJC]
AS
SELECT     dbo.ProcessMaster.Pk_Process, dbo.ProcessMaster.ProcessName, dbo.JProcess.Fk_JobCardID, dbo.JProcess.Pk_JProcessID
FROM         dbo.JProcess INNER JOIN
                      dbo.ProcessMaster ON dbo.JProcess.Fk_ProcessID = dbo.ProcessMaster.Pk_Process
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[6] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "JProcess"
            Begin Extent = 
               Top = 9
               Left = 179
               Bottom = 234
               Right = 341
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ProcessMaster"
            Begin Extent = 
               Top = 5
               Left = 457
               Bottom = 171
               Right = 617
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_ProcessJC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_ProcessJC'
GO
/****** Object:  View [dbo].[Vw_JobProcessSteps]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_JobProcessSteps]
AS
SELECT     dbo.JobCardMaster.Pk_JobCardID, dbo.JobProcess.Pk_ID, dbo.ProcessMaster.ProcessName, dbo.JobProcess.Quantity, dbo.JobProcess.WastageQty, dbo.JobProcess.ProcessDate, 
                      dbo.JobProcess.PQuantity, dbo.JobProcess.PStartTime, dbo.JobProcess.PEndTime, dbo.JobProcess.WQty, dbo.JobCardMaster.JDate
FROM         dbo.ProcessMaster INNER JOIN
                      dbo.JobProcess ON dbo.ProcessMaster.Pk_Process = dbo.JobProcess.Fk_ProcessID INNER JOIN
                      dbo.JobCardMaster ON dbo.JobProcess.Fk_JobCardID = dbo.JobCardMaster.Pk_JobCardID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[4] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ProcessMaster"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 157
               Right = 199
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "JobProcess"
            Begin Extent = 
               Top = 8
               Left = 357
               Bottom = 180
               Right = 517
            End
            DisplayFlags = 280
            TopColumn = 5
         End
         Begin Table = "JobCardMaster"
            Begin Extent = 
               Top = 0
               Left = 787
               Bottom = 199
               Right = 956
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_JobProcessSteps'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_JobProcessSteps'
GO
/****** Object:  View [dbo].[Vw_JCard]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_JCard]
AS
SELECT     dbo.JobCardMaster.Pk_JobCardID, dbo.JobCardMaster.JDate, dbo.JobCardMaster.Fk_Order, dbo.gen_Customer.CustomerName, dbo.Inv_Material.Name, dbo.JobCardMaster.Invno, 
                      dbo.JobCardMaster.Corrugation, dbo.JobCardMaster.TopPaperQty, dbo.JobCardMaster.TwoPlyQty, dbo.JobCardMaster.TwoPlyWt, dbo.JobCardMaster.PastingQty, 
                      dbo.JobCardMaster.PastingWstQty, dbo.JobCardMaster.RotaryQty, dbo.JobCardMaster.RotaryWstQty, dbo.JobCardMaster.PunchingQty, dbo.JobCardMaster.PunchingWstQty, 
                      dbo.JobCardMaster.SlotingQty, dbo.JobCardMaster.SlotingWstQty, dbo.JobCardMaster.PinningQty, dbo.JobCardMaster.PinningWstQty, dbo.JobCardMaster.FinishingQty, 
                      dbo.JobCardMaster.FinishingWstQty, dbo.JobCardMaster.TotalQty, dbo.JobCardMaster.TotalWstQty, dbo.BoxMaster.Name AS BName, dbo.JobCardMaster.Fk_Status, 
                      dbo.gen_DeliverySchedule.Fk_PartID, dbo.ItemPartProperty.PName, dbo.ItemPartProperty.Length, dbo.ItemPartProperty.Width, dbo.ItemPartProperty.Height, 
                      dbo.ItemPartProperty.Pk_PartPropertyID
FROM         dbo.BoxMaster INNER JOIN
                      dbo.JobCardMaster INNER JOIN
                      dbo.JobCardDetails ON dbo.JobCardMaster.Pk_JobCardID = dbo.JobCardDetails.Fk_JobCardID INNER JOIN
                      dbo.Inv_Material ON dbo.JobCardDetails.Fk_Material = dbo.Inv_Material.Pk_Material ON dbo.BoxMaster.Pk_BoxID = dbo.JobCardMaster.Fk_BoxID INNER JOIN
                      dbo.gen_DeliverySchedule ON dbo.JobCardMaster.Fk_Schedule = dbo.gen_DeliverySchedule.Pk_DeliverySechedule AND 
                      dbo.BoxMaster.Pk_BoxID = dbo.gen_DeliverySchedule.Fk_BoxID RIGHT OUTER JOIN
                      dbo.gen_Customer INNER JOIN
                      dbo.gen_Order ON dbo.gen_Customer.Pk_Customer = dbo.gen_Order.Fk_Customer ON dbo.gen_DeliverySchedule.Fk_Order = dbo.gen_Order.Pk_Order AND 
                      dbo.JobCardMaster.Fk_Order = dbo.gen_Order.Pk_Order INNER JOIN
                      dbo.BoxSpecs ON dbo.BoxMaster.Pk_BoxID = dbo.BoxSpecs.Fk_BoxID INNER JOIN
                      dbo.ItemPartProperty ON dbo.gen_DeliverySchedule.Fk_PartID = dbo.ItemPartProperty.Pk_PartPropertyID AND dbo.BoxSpecs.Pk_BoxSpecID = dbo.ItemPartProperty.Fk_BoxSpecsID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[68] 4[4] 2[3] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "JobCardMaster"
            Begin Extent = 
               Top = 2
               Left = 73
               Bottom = 347
               Right = 263
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "JobCardDetails"
            Begin Extent = 
               Top = 12
               Left = 512
               Bottom = 172
               Right = 675
            End
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 1
               Left = 678
               Bottom = 121
               Right = 867
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Order"
            Begin Extent = 
               Top = 144
               Left = 396
               Bottom = 288
               Right = 577
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 364
               Left = 96
               Bottom = 484
               Right = 270
            End
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 348
               Left = 782
               Bottom = 563
               Right = 942
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_DeliverySchedule"
            Begin Extent = 
               Top = 14
               Left = 1064
               Bottom = 340
               Right = 1258
         ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_JCard'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'   End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxSpecs"
            Begin Extent = 
               Top = 291
               Left = 1025
               Bottom = 461
               Right = 1208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ItemPartProperty"
            Begin Extent = 
               Top = 279
               Left = 460
               Bottom = 457
               Right = 639
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 33
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_JCard'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_JCard'
GO
/****** Object:  View [dbo].[Vw_AssignedStock]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_AssignedStock]
AS
SELECT     dbo.PaperStock.Pk_PaperStock, dbo.PaperStock.RollNo, dbo.Inv_Material.Name, dbo.JobCardMaster.Pk_JobCardID, dbo.Inv_Material.Pk_Material, dbo.PaperStock.Quantity, dbo.gen_Mill.MillName, 
                      dbo.gen_Color.ColorName, dbo.gen_Mill.Pk_Mill, dbo.JobCardMaster.Fk_Status, dbo.gen_Customer.CustomerName, dbo.BoxMaster.Name AS BoxName
FROM         dbo.JobCardMaster INNER JOIN
                      dbo.JobCardDetails ON dbo.JobCardMaster.Pk_JobCardID = dbo.JobCardDetails.Fk_JobCardID INNER JOIN
                      dbo.Inv_Material ON dbo.JobCardDetails.Fk_Material = dbo.Inv_Material.Pk_Material INNER JOIN
                      dbo.PaperStock ON dbo.JobCardDetails.Fk_PaperStock = dbo.PaperStock.Pk_PaperStock AND dbo.Inv_Material.Pk_Material = dbo.PaperStock.Fk_Material INNER JOIN
                      dbo.gen_Mill ON dbo.Inv_Material.Fk_Mill = dbo.gen_Mill.Pk_Mill INNER JOIN
                      dbo.gen_Color ON dbo.Inv_Material.Fk_Color = dbo.gen_Color.Pk_Color INNER JOIN
                      dbo.BoxMaster ON dbo.JobCardMaster.Fk_BoxID = dbo.BoxMaster.Pk_BoxID INNER JOIN
                      dbo.gen_Customer ON dbo.BoxMaster.Customer = dbo.gen_Customer.Pk_Customer
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[4] 4[57] 2[11] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "JobCardMaster"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 274
               Right = 207
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "JobCardDetails"
            Begin Extent = 
               Top = 13
               Left = 252
               Bottom = 360
               Right = 475
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 13
               Left = 722
               Bottom = 237
               Right = 911
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PaperStock"
            Begin Extent = 
               Top = 115
               Left = 1059
               Bottom = 235
               Right = 1219
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Mill"
            Begin Extent = 
               Top = 12
               Left = 515
               Bottom = 230
               Right = 681
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Color"
            Begin Extent = 
               Top = 6
               Left = 1017
               Bottom = 96
               Right = 1177
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 299
               Left = 10
               Bottom = 419
               Right = 184
            End
  ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_AssignedStock'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'          DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 240
               Left = 586
               Bottom = 360
               Right = 746
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 12
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_AssignedStock'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_AssignedStock'
GO
/****** Object:  View [dbo].[Vw_JobIssue]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_JobIssue]
AS
SELECT     TOP (100) PERCENT dbo.JobCardDetails.Pk_JobCardDet, dbo.PaperStock.RollNo, dbo.JobCardDetails.RM_Consumed, dbo.Inv_Material.Name, dbo.Inv_Material.Pk_Material, 
                      dbo.JobCardMaster.Fk_BoxID, dbo.JobCardMaster.Fk_Status, dbo.JobCardMaster.Pk_JobCardID, dbo.BoxMaster.Name AS Expr1, dbo.JobCardMaster.JDate, dbo.PaperStock.Quantity AS Expr2, 
                      dbo.JobCardMaster.TotalQty, dbo.gen_Customer.CustomerName, dbo.PaperStock.Pk_PaperStock, dbo.gen_Mill.SName, dbo.gen_Mill.Pk_Mill, dbo.Stocks.Quantity, dbo.gen_Color.ColorName
FROM         dbo.JobCardDetails INNER JOIN
                      dbo.Inv_Material ON dbo.JobCardDetails.Fk_Material = dbo.Inv_Material.Pk_Material INNER JOIN
                      dbo.JobCardMaster ON dbo.JobCardDetails.Fk_JobCardID = dbo.JobCardMaster.Pk_JobCardID INNER JOIN
                      dbo.BoxMaster ON dbo.JobCardMaster.Fk_BoxID = dbo.BoxMaster.Pk_BoxID INNER JOIN
                      dbo.gen_Customer ON dbo.BoxMaster.Customer = dbo.gen_Customer.Pk_Customer INNER JOIN
                      dbo.gen_Color ON dbo.Inv_Material.Fk_Color = dbo.gen_Color.Pk_Color LEFT OUTER JOIN
                      dbo.Stocks ON dbo.Inv_Material.Pk_Material = dbo.Stocks.Fk_Material LEFT OUTER JOIN
                      dbo.gen_Mill ON dbo.Inv_Material.Fk_Mill = dbo.gen_Mill.Pk_Mill LEFT OUTER JOIN
                      dbo.PaperStock ON dbo.Inv_Material.Pk_Material = dbo.PaperStock.Fk_Material AND dbo.JobCardDetails.Fk_PaperStock = dbo.PaperStock.Pk_PaperStock
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[31] 4[24] 2[5] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "JobCardDetails"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 238
               Right = 201
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 0
               Left = 830
               Bottom = 120
               Right = 1019
            End
            DisplayFlags = 280
            TopColumn = 25
         End
         Begin Table = "JobCardMaster"
            Begin Extent = 
               Top = 13
               Left = 275
               Bottom = 256
               Right = 512
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 146
               Left = 741
               Bottom = 326
               Right = 901
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 203
               Left = 235
               Bottom = 323
               Right = 409
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Color"
            Begin Extent = 
               Top = 75
               Left = 586
               Bottom = 165
               Right = 746
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Stocks"
            Begin Extent = 
               Top = 197
               Left = 841
               Bottom = 581
               Right = 1037
            End
   ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_JobIssue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'         DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Mill"
            Begin Extent = 
               Top = 55
               Left = 1101
               Bottom = 316
               Right = 1281
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PaperStock"
            Begin Extent = 
               Top = 229
               Left = 584
               Bottom = 380
               Right = 744
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 19
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 2505
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 2205
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_JobIssue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_JobIssue'
GO
/****** Object:  View [dbo].[vw_JProcessList]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_JProcessList]
AS
SELECT     dbo.ProcessMaster.ProcessName, dbo.JobCardMaster.Pk_JobCardID, dbo.ProcessMaster.Pk_Process
FROM         dbo.ProcessMaster INNER JOIN
                      dbo.JProcess ON dbo.ProcessMaster.Pk_Process = dbo.JProcess.Fk_ProcessID LEFT OUTER JOIN
                      dbo.JobCardMaster ON dbo.JProcess.Fk_JobCardID = dbo.JobCardMaster.Pk_JobCardID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ProcessMaster"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 96
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "JProcess"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 111
               Right = 396
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "JobCardMaster"
            Begin Extent = 
               Top = 6
               Left = 434
               Bottom = 126
               Right = 603
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_JProcessList'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_JProcessList'
GO
/****** Object:  View [dbo].[vw_PaperRollStock]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_PaperRollStock]
AS
SELECT     dbo.PaperStock.RollNo, dbo.PaperStock.Quantity, dbo.Inv_Material.Name, dbo.Inv_Material.Pk_Material, dbo.PaperStock.Pk_PaperStock, dbo.Inv_Material.GSM, dbo.Inv_Material.BF, 
                      dbo.gen_Color.ColorName, dbo.Inv_Material.Min_Value, dbo.Inv_Material.Max_Value, dbo.gen_Mill.SName, dbo.Inv_Material.Deckle, dbo.gen_Mill.MillName, 
                      ISNULL(dbo.JobCardDetails.Fk_JobCardID, 0) AS Fk_JobCardID
FROM         dbo.PaperStock LEFT OUTER JOIN
                      dbo.Inv_Material ON dbo.PaperStock.Fk_Material = dbo.Inv_Material.Pk_Material LEFT OUTER JOIN
                      dbo.JobCardDetails ON dbo.PaperStock.Pk_PaperStock = dbo.JobCardDetails.Fk_PaperStock AND dbo.Inv_Material.Pk_Material = dbo.JobCardDetails.Fk_Material LEFT OUTER JOIN
                      dbo.gen_Mill ON dbo.Inv_Material.Fk_Mill = dbo.gen_Mill.Pk_Mill LEFT OUTER JOIN
                      dbo.gen_Color ON dbo.Inv_Material.Fk_Color = dbo.gen_Color.Pk_Color
WHERE     (dbo.PaperStock.Quantity > 0)
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[42] 4[11] 2[16] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "PaperStock"
            Begin Extent = 
               Top = 186
               Left = 329
               Bottom = 519
               Right = 486
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 0
               Left = 30
               Bottom = 355
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Mill"
            Begin Extent = 
               Top = 0
               Left = 1033
               Bottom = 355
               Right = 1194
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Color"
            Begin Extent = 
               Top = 130
               Left = 747
               Bottom = 370
               Right = 907
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "JobCardDetails"
            Begin Extent = 
               Top = 99
               Left = 644
               Bottom = 352
               Right = 807
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 15
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_PaperRollStock'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_PaperRollStock'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_PaperRollStock'
GO
/****** Object:  View [dbo].[JProcessList]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[JProcessList]
AS
SELECT     dbo.ProcessMaster.ProcessName, dbo.JobCardMaster.Pk_JobCardID, dbo.ProcessMaster.Pk_Process
FROM         dbo.ProcessMaster INNER JOIN
                      dbo.JProcess ON dbo.ProcessMaster.Pk_Process = dbo.JProcess.Fk_ProcessID LEFT OUTER JOIN
                      dbo.JobCardMaster ON dbo.JProcess.Fk_JobCardID = dbo.JobCardMaster.Pk_JobCardID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[7] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "JobCardMaster"
            Begin Extent = 
               Top = 119
               Left = 926
               Bottom = 316
               Right = 1095
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "JProcess"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 146
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ProcessMaster"
            Begin Extent = 
               Top = 6
               Left = 443
               Bottom = 96
               Right = 603
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'JProcessList'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'JProcessList'
GO
/****** Object:  View [dbo].[Vw_WIP_Stock]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_WIP_Stock]
AS
SELECT DISTINCT 
                      dbo.Semi_FinishedGoodsStock.Pk_StkID, dbo.Semi_FinishedGoodsStock.Stock, dbo.Vw_BoxDet.Name, dbo.Semi_FinishedGoodsStock.Description, dbo.Vw_BoxDet.PName AS BoxPartName, 
                      dbo.JobCardMaster.Pk_JobCardID, dbo.Inv_MaterialCategory.Name AS CatName
FROM         dbo.Vw_ProcessWastage INNER JOIN
                      dbo.Semi_FinishedGoodsStock ON dbo.Vw_ProcessWastage.Pk_JobCardID = dbo.Semi_FinishedGoodsStock.Fk_JobCardID INNER JOIN
                      dbo.JobCardMaster ON dbo.Semi_FinishedGoodsStock.Fk_JobCardID = dbo.JobCardMaster.Pk_JobCardID INNER JOIN
                      dbo.gen_DeliverySchedule ON dbo.JobCardMaster.Fk_Schedule = dbo.gen_DeliverySchedule.Pk_DeliverySechedule INNER JOIN
                      dbo.Vw_BoxDet ON dbo.gen_DeliverySchedule.Fk_PartID = dbo.Vw_BoxDet.Pk_PartPropertyID INNER JOIN
                      dbo.Inv_MaterialCategory ON dbo.Semi_FinishedGoodsStock.Fk_MatCatID = dbo.Inv_MaterialCategory.Pk_MaterialCategory
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[28] 4[34] 2[14] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Vw_ProcessWastage"
            Begin Extent = 
               Top = 6
               Left = 1065
               Bottom = 269
               Right = 1233
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Semi_FinishedGoodsStock"
            Begin Extent = 
               Top = 18
               Left = 273
               Bottom = 281
               Right = 447
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "JobCardMaster"
            Begin Extent = 
               Top = 0
               Left = 800
               Bottom = 269
               Right = 969
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_DeliverySchedule"
            Begin Extent = 
               Top = 0
               Left = 510
               Bottom = 263
               Right = 704
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Vw_BoxDet"
            Begin Extent = 
               Top = 8
               Left = 25
               Bottom = 270
               Right = 211
            End
            DisplayFlags = 280
            TopColumn = 11
         End
         Begin Table = "Inv_MaterialCategory"
            Begin Extent = 
               Top = 300
               Left = 706
               Bottom = 467
               Right = 895
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_WIP_Stock'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N' = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 3270
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_WIP_Stock'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_WIP_Stock'
GO
/****** Object:  View [dbo].[Vw_PapDistIssueList]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_PapDistIssueList]
AS
SELECT     SUM(dbo.MaterialIssueDetails.Quantity) AS qty, dbo.MaterialIssue.Pk_MaterialIssueID, dbo.MaterialIssue.IssueDate, dbo.Inv_Material.Pk_Material, dbo.Inv_Material.Name, 
                      dbo.JobCardMaster.Pk_JobCardID, dbo.JobCardMaster.JDate, dbo.JobCardMaster.Fk_Order, dbo.gen_Customer.CustomerName, dbo.MaterialIssueDetails.Quantity, dbo.MaterialIssueDetails.RollNo, 
                      dbo.gen_Mill.MillName, dbo.gen_Color.ColorName
FROM         dbo.MaterialIssue INNER JOIN
                      dbo.MaterialIssueDetails ON dbo.MaterialIssue.Pk_MaterialIssueID = dbo.MaterialIssueDetails.Fk_IssueID INNER JOIN
                      dbo.Inv_Material ON dbo.MaterialIssueDetails.Fk_Material = dbo.Inv_Material.Pk_Material INNER JOIN
                      dbo.JobCardMaster ON dbo.MaterialIssue.Fk_JobCardID = dbo.JobCardMaster.Pk_JobCardID INNER JOIN
                      dbo.BoxMaster ON dbo.JobCardMaster.Fk_BoxID = dbo.BoxMaster.Pk_BoxID INNER JOIN
                      dbo.gen_Customer ON dbo.BoxMaster.Customer = dbo.gen_Customer.Pk_Customer INNER JOIN
                      dbo.gen_Color ON dbo.Inv_Material.Fk_Color = dbo.gen_Color.Pk_Color LEFT OUTER JOIN
                      dbo.gen_Mill ON dbo.MaterialIssueDetails.Fk_Mill = dbo.gen_Mill.Pk_Mill AND dbo.Inv_Material.Fk_Mill = dbo.gen_Mill.Pk_Mill
GROUP BY dbo.MaterialIssue.Pk_MaterialIssueID, dbo.MaterialIssue.IssueDate, dbo.Inv_Material.Pk_Material, dbo.Inv_Material.Name, dbo.JobCardMaster.Pk_JobCardID, dbo.JobCardMaster.JDate, 
                      dbo.JobCardMaster.Fk_Order, dbo.gen_Customer.CustomerName, dbo.MaterialIssueDetails.Quantity, dbo.MaterialIssueDetails.RollNo, dbo.gen_Mill.MillName, dbo.gen_Color.ColorName
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[66] 4[4] 2[5] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = -480
      End
      Begin Tables = 
         Begin Table = "MaterialIssue"
            Begin Extent = 
               Top = 17
               Left = 48
               Bottom = 221
               Right = 229
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MaterialIssueDetails"
            Begin Extent = 
               Top = 162
               Left = 418
               Bottom = 376
               Right = 633
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 0
               Left = 726
               Bottom = 281
               Right = 915
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "JobCardMaster"
            Begin Extent = 
               Top = 329
               Left = 1272
               Bottom = 669
               Right = 1441
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 22
               Left = 499
               Bottom = 142
               Right = 659
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 6
               Left = 267
               Bottom = 126
               Right = 441
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Color"
            Begin Extent = 
               Top = 6
               Left = 1380
               Bottom = 96
               Right = 1556
       ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PapDistIssueList'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'     End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Mill"
            Begin Extent = 
               Top = 172
               Left = 1411
               Bottom = 292
               Right = 1593
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 14
         Width = 284
         Width = 1500
         Width = 3360
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PapDistIssueList'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PapDistIssueList'
GO
/****** Object:  View [dbo].[Vw_PStock]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_PStock]
AS
SELECT     SUM(dbo.PaperStock.Quantity) AS TQty, dbo.PaperStock.Fk_Material, dbo.Inv_Material.Pk_Material, dbo.Vw_JobIssue.Name, dbo.Vw_JobIssue.JDate, dbo.Vw_JobIssue.Expr1 AS BoxName, 
                      dbo.Vw_JobIssue.Expr2 AS Assigned, dbo.Vw_JobIssue.Fk_Status, dbo.Vw_JobIssue.RM_Consumed, dbo.Vw_JobIssue.Pk_JobCardID, dbo.Vw_JobIssue.CustomerName
FROM         dbo.PaperStock INNER JOIN
                      dbo.Inv_Material ON dbo.PaperStock.Fk_Material = dbo.Inv_Material.Pk_Material INNER JOIN
                      dbo.Vw_JobIssue ON dbo.PaperStock.Fk_Material = dbo.Vw_JobIssue.Pk_Material
GROUP BY dbo.PaperStock.Fk_Material, dbo.Inv_Material.Pk_Material, dbo.Vw_JobIssue.Name, dbo.Vw_JobIssue.JDate, dbo.Vw_JobIssue.Expr1, dbo.Vw_JobIssue.Expr2, dbo.Vw_JobIssue.Fk_Status, 
                      dbo.Vw_JobIssue.RM_Consumed, dbo.Vw_JobIssue.Pk_JobCardID, dbo.Vw_JobIssue.CustomerName
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[30] 4[20] 2[13] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "PaperStock"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 167
               Right = 199
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 119
               Left = 406
               Bottom = 239
               Right = 595
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Vw_JobIssue"
            Begin Extent = 
               Top = 0
               Left = 844
               Bottom = 337
               Right = 1023
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 11
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1920
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PStock'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PStock'
GO
/****** Object:  View [dbo].[Vw_JcIssues]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_JcIssues]
AS
SELECT     TOP (100) PERCENT dbo.Inv_Material.Name, dbo.Inv_Material.Pk_Material, dbo.JobCardMaster.Fk_BoxID, dbo.JobCardMaster.Fk_Status, dbo.JobCardMaster.Pk_JobCardID, 
                      dbo.BoxMaster.Name AS Expr1, dbo.JobCardMaster.JDate, dbo.JobCardMaster.TotalQty, dbo.gen_Customer.CustomerName, dbo.gen_Mill.SName, dbo.gen_Mill.Pk_Mill, 
                      dbo.MaterialIssue.Pk_MaterialIssueID, dbo.MaterialIssue.IssueDate, dbo.MaterialIssueDetails.Quantity AS IssueQty, dbo.gen_Color.ColorName, dbo.gen_Mill.MillName, 
                      dbo.MaterialIssueDetails.RollNo, dbo.MaterialIssueDetails.Pk_MaterialIssueDetailsID
FROM         dbo.MaterialIssueDetails INNER JOIN
                      dbo.Inv_Material ON dbo.MaterialIssueDetails.Fk_Material = dbo.Inv_Material.Pk_Material LEFT OUTER JOIN
                      dbo.gen_Color ON dbo.Inv_Material.Fk_Color = dbo.gen_Color.Pk_Color LEFT OUTER JOIN
                      dbo.MaterialIssue ON dbo.MaterialIssueDetails.Fk_IssueID = dbo.MaterialIssue.Pk_MaterialIssueID RIGHT OUTER JOIN
                      dbo.BoxMaster INNER JOIN
                      dbo.JobCardMaster ON dbo.BoxMaster.Pk_BoxID = dbo.JobCardMaster.Fk_BoxID INNER JOIN
                      dbo.gen_Customer ON dbo.BoxMaster.Customer = dbo.gen_Customer.Pk_Customer ON dbo.MaterialIssue.Fk_JobCardID = dbo.JobCardMaster.Pk_JobCardID LEFT OUTER JOIN
                      dbo.gen_Mill ON dbo.Inv_Material.Fk_Mill = dbo.gen_Mill.Pk_Mill
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[47] 4[3] 2[14] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "MaterialIssueDetails"
            Begin Extent = 
               Top = 173
               Left = 1185
               Bottom = 388
               Right = 1398
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 0
               Left = 362
               Bottom = 120
               Right = 551
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Color"
            Begin Extent = 
               Top = 134
               Left = 22
               Bottom = 224
               Right = 182
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MaterialIssue"
            Begin Extent = 
               Top = 193
               Left = 693
               Bottom = 313
               Right = 874
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 0
               Left = 787
               Bottom = 120
               Right = 947
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "JobCardMaster"
            Begin Extent = 
               Top = 152
               Left = 307
               Bottom = 272
               Right = 476
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 0
               Left = 1016
               Bottom = 120
               Right = 1190
      ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_JcIssues'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'      End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Mill"
            Begin Extent = 
               Top = 0
               Left = 81
               Bottom = 120
               Right = 247
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 22
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_JcIssues'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_JcIssues'
GO
/****** Object:  View [dbo].[Vw_JCIssReport]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_JCIssReport]
AS
SELECT     dbo.gen_Color.ColorName, dbo.gen_Mill.MillName, dbo.MaterialIssueDetails.Quantity AS IssQty, dbo.MaterialIssueDetails.RollNo, dbo.MaterialIssueDetails.Pk_MaterialIssueDetailsID, 
                      dbo.MaterialIssue.Pk_MaterialIssueID, dbo.MaterialIssue.IssueDate, dbo.JobCardMaster.Pk_JobCardID, dbo.Inv_Material.GSM, dbo.Inv_Material.BF, dbo.Inv_Material.Name, 
                      dbo.Inv_Material.Deckle, dbo.MaterialIssueDetails.Weight, dbo.gen_Order.Pk_Order, dbo.BoxMaster.Name AS BName, dbo.JobCardMaster.TotalQty, dbo.JobCardMaster.Fk_BoxID, 
                      dbo.JobCardMaster.Fk_Schedule, dbo.Inv_Material.Pk_Material
FROM         dbo.BoxMaster INNER JOIN
                      dbo.JobCardMaster ON dbo.BoxMaster.Pk_BoxID = dbo.JobCardMaster.Fk_BoxID LEFT OUTER JOIN
                      dbo.MaterialIssueDetails INNER JOIN
                      dbo.MaterialIssue ON dbo.MaterialIssueDetails.Fk_IssueID = dbo.MaterialIssue.Pk_MaterialIssueID INNER JOIN
                      dbo.Inv_Material ON dbo.MaterialIssueDetails.Fk_Material = dbo.Inv_Material.Pk_Material INNER JOIN
                      dbo.gen_Mill ON dbo.Inv_Material.Fk_Mill = dbo.gen_Mill.Pk_Mill INNER JOIN
                      dbo.gen_Color ON dbo.Inv_Material.Fk_Color = dbo.gen_Color.Pk_Color INNER JOIN
                      dbo.gen_Order ON dbo.MaterialIssue.Fk_OrderNo = dbo.gen_Order.Pk_Order ON dbo.JobCardMaster.Fk_Order = dbo.gen_Order.Pk_Order AND 
                      dbo.JobCardMaster.Pk_JobCardID = dbo.MaterialIssue.Fk_JobCardID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[65] 4[4] 2[4] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = -192
      End
      Begin Tables = 
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 6
               Left = 1109
               Bottom = 126
               Right = 1269
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "JobCardMaster"
            Begin Extent = 
               Top = 172
               Left = 1091
               Bottom = 585
               Right = 1260
            End
            DisplayFlags = 280
            TopColumn = 32
         End
         Begin Table = "MaterialIssueDetails"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 273
               Right = 235
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MaterialIssue"
            Begin Extent = 
               Top = 3
               Left = 287
               Bottom = 273
               Right = 458
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 6
               Left = 508
               Bottom = 287
               Right = 667
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Mill"
            Begin Extent = 
               Top = 6
               Left = 735
               Bottom = 125
               Right = 901
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Color"
            Begin Extent = 
               Top = 6
               Left = 939
               Bottom = 95
               Right = 1099
            E' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_JCIssReport'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'nd
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Order"
            Begin Extent = 
               Top = 120
               Left = 890
               Bottom = 240
               Right = 1071
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 20
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 4320
         Width = 4695
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1710
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 2265
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_JCIssReport'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_JCIssReport'
GO
/****** Object:  View [dbo].[Vw_IssueDay]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_IssueDay]
AS
SELECT     dbo.JobCardMaster.Pk_JobCardID, dbo.JobCardMaster.JDate, dbo.MaterialIssueDetails.Fk_Material, dbo.MaterialIssueDetails.Quantity AS IssQty, 
                      dbo.MaterialIssueDetails.RollNo, dbo.Inv_Material.Name, dbo.MaterialIssue.Pk_MaterialIssueID, dbo.MaterialIssue.IssueDate, dbo.gen_Mill.MillName, 
                      dbo.gen_Color.ColorName, dbo.gen_Customer.CustomerName, dbo.gen_Order.Pk_Order, dbo.gen_Order.OrderDate, dbo.gen_DeliverySchedule.DeliveryDate, 
                      dbo.gen_DeliverySchedule.Quantity AS ProdQty
FROM         dbo.JobCardMaster INNER JOIN
                      dbo.JobCardDetails ON dbo.JobCardMaster.Pk_JobCardID = dbo.JobCardDetails.Fk_JobCardID INNER JOIN
                      dbo.MaterialIssue ON dbo.JobCardMaster.Pk_JobCardID = dbo.MaterialIssue.Fk_JobCardID INNER JOIN
                      dbo.MaterialIssueDetails ON dbo.MaterialIssue.Pk_MaterialIssueID = dbo.MaterialIssueDetails.Fk_IssueID INNER JOIN
                      dbo.Inv_Material ON dbo.MaterialIssueDetails.Fk_Material = dbo.Inv_Material.Pk_Material INNER JOIN
                      dbo.gen_Mill ON dbo.Inv_Material.Fk_Mill = dbo.gen_Mill.Pk_Mill INNER JOIN
                      dbo.gen_Color ON dbo.Inv_Material.Fk_Color = dbo.gen_Color.Pk_Color INNER JOIN
                      dbo.gen_Order ON dbo.JobCardMaster.Fk_Order = dbo.gen_Order.Pk_Order AND dbo.MaterialIssue.Fk_OrderNo = dbo.gen_Order.Pk_Order INNER JOIN
                      dbo.gen_Customer ON dbo.gen_Order.Fk_Customer = dbo.gen_Customer.Pk_Customer INNER JOIN
                      dbo.gen_DeliverySchedule ON dbo.JobCardMaster.Fk_Schedule = dbo.gen_DeliverySchedule.Pk_DeliverySechedule AND 
                      dbo.gen_Order.Pk_Order = dbo.gen_DeliverySchedule.Fk_Order
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[8] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "JobCardMaster"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 207
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "JobCardDetails"
            Begin Extent = 
               Top = 6
               Left = 245
               Bottom = 125
               Right = 408
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MaterialIssue"
            Begin Extent = 
               Top = 0
               Left = 454
               Bottom = 259
               Right = 635
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MaterialIssueDetails"
            Begin Extent = 
               Top = 0
               Left = 679
               Bottom = 119
               Right = 892
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 6
               Left = 916
               Bottom = 125
               Right = 1105
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Mill"
            Begin Extent = 
               Top = 6
               Left = 1143
               Bottom = 125
               Right = 1309
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Color"
            Begin Extent = 
               Top = 148
               Left = 19
               Bottom = 237
               Right = 179
            E' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_IssueDay'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'nd
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Order"
            Begin Extent = 
               Top = 243
               Left = 214
               Bottom = 362
               Right = 395
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Customer"
            Begin Extent = 
               Top = 222
               Left = 754
               Bottom = 341
               Right = 928
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_DeliverySchedule"
            Begin Extent = 
               Top = 147
               Left = 1060
               Bottom = 266
               Right = 1254
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 16
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_IssueDay'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_IssueDay'
GO
/****** Object:  View [dbo].[Vw_Issue]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_Issue]
AS
SELECT     dbo.MaterialIssue.Pk_MaterialIssueID, dbo.MaterialIssue.IssueDate, dbo.gen_Order.Pk_Order, dbo.MaterialIssueDetails.Fk_Material, dbo.Inv_Material.Name, dbo.MaterialIssueDetails.Quantity, 
                      dbo.gen_Order.Product, dbo.MaterialIssue.DCNo, dbo.MaterialIssueDetails.Pk_MaterialIssueDetailsID, dbo.gen_Unit.UnitName
FROM         dbo.gen_Order INNER JOIN
                      dbo.MaterialIssue ON dbo.gen_Order.Pk_Order = dbo.MaterialIssue.Fk_OrderNo INNER JOIN
                      dbo.MaterialIssueDetails ON dbo.MaterialIssue.Pk_MaterialIssueID = dbo.MaterialIssueDetails.Fk_IssueID INNER JOIN
                      dbo.Inv_Material ON dbo.MaterialIssueDetails.Fk_Material = dbo.Inv_Material.Pk_Material INNER JOIN
                      dbo.gen_Unit ON dbo.Inv_Material.Fk_UnitId = dbo.gen_Unit.Pk_Unit
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[45] 4[2] 2[47] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "gen_Order"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 198
               Right = 223
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MaterialIssue"
            Begin Extent = 
               Top = 6
               Left = 257
               Bottom = 210
               Right = 444
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MaterialIssueDetails"
            Begin Extent = 
               Top = 6
               Left = 482
               Bottom = 210
               Right = 696
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 6
               Left = 734
               Bottom = 210
               Right = 935
            End
            DisplayFlags = 280
            TopColumn = 17
         End
         Begin Table = "gen_Unit"
            Begin Extent = 
               Top = 6
               Left = 973
               Bottom = 126
               Right = 1133
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
       ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Issue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'  Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Issue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Issue'
GO
/****** Object:  View [dbo].[Vw_Iss_Return]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_Iss_Return]
AS
SELECT     dbo.IssueReturn.Pk_IssueReturnMasterId, dbo.IssueReturn.IssueReturnDate, dbo.IssueReturn.Fk_IssueId, dbo.IssueReturn.IssueDate, dbo.Inv_Material.Name, 
                      dbo.IssueReturnDetails.ReturnQuantity, dbo.IssueReturnDetails.RollNo, dbo.Inv_Material.Pk_Material, dbo.gen_Color.ColorName, dbo.gen_Mill.MillName
FROM         dbo.IssueReturn INNER JOIN
                      dbo.IssueReturnDetails ON dbo.IssueReturn.Pk_IssueReturnMasterId = dbo.IssueReturnDetails.Fk_IssueReturnMasterId INNER JOIN
                      dbo.Inv_Material ON dbo.IssueReturnDetails.Fk_Material = dbo.Inv_Material.Pk_Material INNER JOIN
                      dbo.gen_Color ON dbo.Inv_Material.Fk_Color = dbo.gen_Color.Pk_Color INNER JOIN
                      dbo.gen_Mill ON dbo.Inv_Material.Fk_Mill = dbo.gen_Mill.Pk_Mill
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[58] 4[5] 2[8] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "IssueReturn"
            Begin Extent = 
               Top = 23
               Left = 70
               Bottom = 211
               Right = 285
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "IssueReturnDetails"
            Begin Extent = 
               Top = 26
               Left = 425
               Bottom = 302
               Right = 633
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 0
               Left = 941
               Bottom = 329
               Right = 1110
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Color"
            Begin Extent = 
               Top = 6
               Left = 671
               Bottom = 96
               Right = 831
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Mill"
            Begin Extent = 
               Top = 96
               Left = 671
               Bottom = 216
               Right = 837
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Co' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Iss_Return'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'lumn = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Iss_Return'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Iss_Return'
GO
/****** Object:  View [dbo].[Vw_AssStock]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_AssStock]
AS
SELECT     SUM(dbo.Items_Layers.Weight) AS Expr1, dbo.Inv_Material.Pk_Material, dbo.BoxMaster.Pk_BoxID, dbo.Inv_Material.Name AS MName, dbo.JobCardMaster.Pk_JobCardID, 
                      dbo.JobCardMaster.JDate, dbo.JobCardMaster.Fk_Status, dbo.BoxMaster.Name AS bName, dbo.PaperStock.RollNo, dbo.JobCardDetails.Pk_JobCardDet, dbo.JobCardDetails.RM_Consumed
FROM         dbo.BoxMaster INNER JOIN
                      dbo.BoxSpecs ON dbo.BoxMaster.Pk_BoxID = dbo.BoxSpecs.Fk_BoxID INNER JOIN
                      dbo.ItemPartProperty ON dbo.BoxSpecs.Pk_BoxSpecID = dbo.ItemPartProperty.Fk_BoxSpecsID INNER JOIN
                      dbo.Items_Layers ON dbo.ItemPartProperty.Pk_PartPropertyID = dbo.Items_Layers.Fk_PartId INNER JOIN
                      dbo.Inv_Material ON dbo.Items_Layers.Fk_Material = dbo.Inv_Material.Pk_Material INNER JOIN
                      dbo.Vw_PStock ON dbo.Inv_Material.Pk_Material = dbo.Vw_PStock.Pk_Material INNER JOIN
                      dbo.JobCardMaster ON dbo.BoxMaster.Pk_BoxID = dbo.JobCardMaster.Fk_BoxID INNER JOIN
                      dbo.JobCardDetails ON dbo.Inv_Material.Pk_Material = dbo.JobCardDetails.Fk_Material AND dbo.JobCardMaster.Pk_JobCardID = dbo.JobCardDetails.Fk_JobCardID INNER JOIN
                      dbo.PaperStock ON dbo.Inv_Material.Pk_Material = dbo.PaperStock.Fk_Material AND dbo.JobCardDetails.Fk_PaperStock = dbo.PaperStock.Pk_PaperStock
GROUP BY dbo.Inv_Material.Pk_Material, dbo.BoxMaster.Pk_BoxID, dbo.Inv_Material.Name, dbo.JobCardMaster.Pk_JobCardID, dbo.JobCardMaster.JDate, dbo.JobCardMaster.Fk_Status, 
                      dbo.BoxMaster.Name, dbo.PaperStock.RollNo, dbo.JobCardDetails.Pk_JobCardDet, dbo.JobCardDetails.RM_Consumed
HAVING      (dbo.JobCardMaster.Fk_Status = 1)
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[57] 4[10] 2[7] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 126
               Right = 214
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxSpecs"
            Begin Extent = 
               Top = 15
               Left = 286
               Bottom = 192
               Right = 474
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ItemPartProperty"
            Begin Extent = 
               Top = 4
               Left = 535
               Bottom = 124
               Right = 730
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Items_Layers"
            Begin Extent = 
               Top = 1
               Left = 796
               Bottom = 121
               Right = 972
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 3
               Left = 1032
               Bottom = 123
               Right = 1237
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Vw_PStock"
            Begin Extent = 
               Top = 6
               Left = 1275
               Bottom = 111
               Right = 1451
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "JobCardMaster"
            Begin Extent = 
               Top = 207
               Left = 281
               Bottom = 390
               Right = 463
            End
   ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_AssStock'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'         DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "JobCardDetails"
            Begin Extent = 
               Top = 165
               Left = 626
               Bottom = 387
               Right = 817
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PaperStock"
            Begin Extent = 
               Top = 211
               Left = 1044
               Bottom = 381
               Right = 1220
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 12
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 2100
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 3120
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1815
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_AssStock'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_AssStock'
GO
/****** Object:  View [dbo].[Vw_BOMSumm]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_BOMSumm]
AS
SELECT     SUM(dbo.Items_Layers.Weight) AS PaperWeight, dbo.Inv_Material.Name, dbo.BoxMaster.Pk_BoxID, dbo.Inv_Material.Pk_Material, dbo.BoxMaster.Name AS BName, 
                      dbo.Vw_PStock.TQty AS StkQty
FROM         dbo.BoxMaster INNER JOIN
                      dbo.BoxSpecs ON dbo.BoxMaster.Pk_BoxID = dbo.BoxSpecs.Fk_BoxID INNER JOIN
                      dbo.ItemPartProperty ON dbo.BoxSpecs.Pk_BoxSpecID = dbo.ItemPartProperty.Fk_BoxSpecsID INNER JOIN
                      dbo.Items_Layers ON dbo.ItemPartProperty.Pk_PartPropertyID = dbo.Items_Layers.Fk_PartId INNER JOIN
                      dbo.Inv_Material ON dbo.Items_Layers.Fk_Material = dbo.Inv_Material.Pk_Material INNER JOIN
                      dbo.Vw_PStock ON dbo.Inv_Material.Pk_Material = dbo.Vw_PStock.Pk_Material
GROUP BY dbo.Inv_Material.Name, dbo.BoxMaster.Pk_BoxID, dbo.Inv_Material.Pk_Material, dbo.BoxMaster.Name, dbo.Vw_PStock.TQty
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[8] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 6
               Left = 0
               Bottom = 126
               Right = 176
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxSpecs"
            Begin Extent = 
               Top = 6
               Left = 252
               Bottom = 126
               Right = 451
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ItemPartProperty"
            Begin Extent = 
               Top = 6
               Left = 489
               Bottom = 126
               Right = 684
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Items_Layers"
            Begin Extent = 
               Top = 6
               Left = 722
               Bottom = 126
               Right = 898
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 24
               Left = 1098
               Bottom = 144
               Right = 1303
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Vw_PStock"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 269
               Right = 214
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_BOMSumm'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_BOMSumm'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_BOMSumm'
GO
/****** Object:  View [dbo].[vw_StockLedger]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_StockLedger]
AS
SELECT     TOP (100) PERCENT dbo.Inv_Material.Pk_Material, dbo.Inv_Material.Name, ISNULL(SUM(dbo.MaterialInwardD.Quantity), 0) AS Inwd_Qty, ISNULL(SUM(dbo.MaterialIssueDetails.Quantity), 0) 
                      AS Issue_Qty, ISNULL(SUM(dbo.IssueReturnDetails.ReturnQuantity), 0) AS RQty, dbo.Stocks.Quantity AS StockQty, dbo.Stocks.Fk_Tanent, dbo.Stocks.Pk_Stock, dbo.Stocks.Fk_BranchID, 
                      dbo.MaterialInwardD.Quantity
FROM         dbo.MaterialInwardM INNER JOIN
                      dbo.Inv_Material INNER JOIN
                      dbo.Stocks ON dbo.Stocks.Fk_Material = dbo.Inv_Material.Pk_Material INNER JOIN
                      dbo.MaterialInwardD ON dbo.Inv_Material.Pk_Material = dbo.MaterialInwardD.Fk_Material ON dbo.MaterialInwardM.Pk_Inward = dbo.MaterialInwardD.Fk_Inward LEFT OUTER JOIN
                      dbo.MaterialIssue INNER JOIN
                      dbo.MaterialIssueDetails ON dbo.MaterialIssue.Pk_MaterialIssueID = dbo.MaterialIssueDetails.Fk_IssueID ON 
                      dbo.Inv_Material.Pk_Material = dbo.MaterialIssueDetails.Fk_Material LEFT OUTER JOIN
                      dbo.IssueReturnDetails ON dbo.Inv_Material.Pk_Material = dbo.IssueReturnDetails.Fk_Material
GROUP BY dbo.Inv_Material.Pk_Material, dbo.Inv_Material.Name, dbo.Stocks.Quantity, dbo.Stocks.Fk_Tanent, dbo.Stocks.Pk_Stock, dbo.Stocks.Fk_BranchID, dbo.MaterialInwardD.Quantity
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -96
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Stocks"
            Begin Extent = 
               Top = 6
               Left = 281
               Bottom = 186
               Right = 456
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MaterialIssueDetails"
            Begin Extent = 
               Top = 6
               Left = 495
               Bottom = 258
               Right = 705
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MaterialInwardD"
            Begin Extent = 
               Top = 120
               Left = 750
               Bottom = 240
               Right = 926
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MaterialInwardM"
            Begin Extent = 
               Top = 192
               Left = 38
               Bottom = 365
               Right = 214
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "IssueReturnDetails"
            Begin Extent = 
               Top = 6
               Left = 976
               Bottom = 199
               Right = 1200
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MaterialIssue"
            Begin Extent = 
               Top = 303
               Left = 265
               Bottom = 423
               Right = 462
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 190
               Right = 246' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_StockLedger'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 11
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_StockLedger'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_StockLedger'
GO
/****** Object:  View [dbo].[Vw_PaperSummary]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_PaperSummary]
AS
SELECT     SUM(dbo.Items_Layers.Weight) AS SWt, dbo.Inv_Material.Pk_Material, dbo.BoxMaster.Pk_BoxID, dbo.Inv_Material.Name, dbo.gen_Mill.MillName, dbo.PurchaseOrderM.Fk_Status, 
                      dbo.PurchaseOrderD.Quantity, dbo.PurchaseOrderM.Pk_PONo, dbo.PurchaseOrderM.PODate, dbo.PurchaseOrderD.InwdQty
FROM         dbo.BoxMaster INNER JOIN
                      dbo.BoxSpecs ON dbo.BoxMaster.Pk_BoxID = dbo.BoxSpecs.Fk_BoxID INNER JOIN
                      dbo.ItemPartProperty ON dbo.BoxSpecs.Pk_BoxSpecID = dbo.ItemPartProperty.Fk_BoxSpecsID INNER JOIN
                      dbo.Items_Layers ON dbo.ItemPartProperty.Pk_PartPropertyID = dbo.Items_Layers.Fk_PartId INNER JOIN
                      dbo.Inv_Material ON dbo.Items_Layers.Fk_Material = dbo.Inv_Material.Pk_Material INNER JOIN
                      dbo.Vw_PStock ON dbo.Inv_Material.Pk_Material = dbo.Vw_PStock.Pk_Material INNER JOIN
                      dbo.gen_Mill ON dbo.Inv_Material.Fk_Mill = dbo.gen_Mill.Pk_Mill INNER JOIN
                      dbo.PurchaseOrderD ON dbo.Inv_Material.Pk_Material = dbo.PurchaseOrderD.Fk_Material INNER JOIN
                      dbo.PurchaseOrderM ON dbo.PurchaseOrderD.Fk_PONo = dbo.PurchaseOrderM.Pk_PONo
GROUP BY dbo.Inv_Material.Pk_Material, dbo.BoxMaster.Pk_BoxID, dbo.Inv_Material.Name, dbo.gen_Mill.MillName, dbo.PurchaseOrderM.Fk_Status, dbo.PurchaseOrderD.Quantity, 
                      dbo.PurchaseOrderM.Pk_PONo, dbo.PurchaseOrderM.PODate, dbo.PurchaseOrderD.InwdQty
HAVING      (dbo.PurchaseOrderM.Fk_Status = 1)
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[11] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "BoxMaster"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 126
               Right = 214
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "BoxSpecs"
            Begin Extent = 
               Top = 6
               Left = 252
               Bottom = 126
               Right = 451
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ItemPartProperty"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 246
               Right = 233
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Items_Layers"
            Begin Extent = 
               Top = 126
               Left = 271
               Bottom = 246
               Right = 447
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 246
               Left = 38
               Bottom = 366
               Right = 243
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Vw_PStock"
            Begin Extent = 
               Top = 246
               Left = 281
               Bottom = 366
               Right = 459
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_Mill"
            Begin Extent = 
               Top = 366
               Left = 38
               Bottom = 486
               Right = 220
            End
       ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PaperSummary'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'     DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PurchaseOrderD"
            Begin Extent = 
               Top = 366
               Left = 258
               Bottom = 486
               Right = 434
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PurchaseOrderM"
            Begin Extent = 
               Top = 486
               Left = 38
               Bottom = 606
               Right = 214
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PaperSummary'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_PaperSummary'
GO
/****** Object:  View [dbo].[Vw_MonthlyConsumption]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_MonthlyConsumption]
AS
SELECT     dbo.Inv_MaterialCategory.Name AS Cat_Name, dbo.Inv_Material.Name AS MaterialName, dbo.MaterialIssueDetails.Quantity, dbo.MaterialIssueDetails.Pk_MaterialIssueDetailsID, 
                      ISNULL(dbo.IssueReturnDetails.ReturnQuantity, 0) AS RetQty, ISNULL(dbo.MaterialIssueDetails.Quantity - dbo.IssueReturnDetails.ReturnQuantity, 0) AS Consumption, dbo.MaterialIssue.IssueDate, 
                      dbo.MaterialIssue.Pk_MaterialIssueID
FROM         dbo.Inv_Material INNER JOIN
                      dbo.MaterialIssue INNER JOIN
                      dbo.MaterialIssueDetails ON dbo.MaterialIssue.Pk_MaterialIssueID = dbo.MaterialIssueDetails.Fk_IssueID ON 
                      dbo.Inv_Material.Pk_Material = dbo.MaterialIssueDetails.Fk_Material LEFT OUTER JOIN
                      dbo.IssueReturnDetails INNER JOIN
                      dbo.IssueReturn ON dbo.IssueReturnDetails.Fk_IssueReturnMasterId = dbo.IssueReturn.Pk_IssueReturnMasterId ON dbo.Inv_Material.Pk_Material = dbo.IssueReturnDetails.Fk_Material AND 
                      dbo.MaterialIssue.Pk_MaterialIssueID = dbo.IssueReturn.Fk_IssueId INNER JOIN
                      dbo.Inv_MaterialCategory ON dbo.Inv_Material.Fk_MaterialCategory = dbo.Inv_MaterialCategory.Pk_MaterialCategory
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[20] 4[44] 2[7] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Inv_Material"
            Begin Extent = 
               Top = 0
               Left = 1046
               Bottom = 299
               Right = 1235
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MaterialIssue"
            Begin Extent = 
               Top = 13
               Left = 4
               Bottom = 312
               Right = 185
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MaterialIssueDetails"
            Begin Extent = 
               Top = 114
               Left = 237
               Bottom = 413
               Right = 450
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "IssueReturnDetails"
            Begin Extent = 
               Top = 117
               Left = 782
               Bottom = 386
               Right = 990
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "IssueReturn"
            Begin Extent = 
               Top = 37
               Left = 540
               Bottom = 336
               Right = 748
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inv_MaterialCategory"
            Begin Extent = 
               Top = 300
               Left = 1018
               Bottom = 405
               Right = 1207
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_MonthlyConsumption'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'84
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 8850
         Alias = 4710
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_MonthlyConsumption'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_MonthlyConsumption'
GO
/****** Object:  View [dbo].[Vw_JobBoxDetails]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_JobBoxDetails]
AS
SELECT DISTINCT 
                      dbo.Vw_JCIssReport.ColorName, dbo.Vw_JCIssReport.MillName, dbo.Vw_JCIssReport.IssQty, dbo.Vw_JCIssReport.RollNo, dbo.Vw_JCIssReport.Pk_MaterialIssueDetailsID, 
                      dbo.Vw_JCIssReport.Pk_MaterialIssueID, dbo.Vw_JCIssReport.IssueDate, dbo.Vw_JCIssReport.Pk_JobCardID, dbo.Vw_JCIssReport.GSM, dbo.Vw_JCIssReport.BF, dbo.Vw_JCIssReport.Name, 
                      dbo.Vw_JCIssReport.Deckle, dbo.Vw_JCIssReport.BName, dbo.Vw_JCIssReport.TotalQty, dbo.Vw_BoxDet.LayerWt, dbo.Vw_BoxDet.Quantity, dbo.Vw_JCIssReport.Pk_Material, 
                      ISNULL(dbo.Vw_Iss_Return.ReturnQuantity, 0) AS ReturnQty, dbo.Vw_BoxDet.Weight
FROM         dbo.Vw_JCIssReport INNER JOIN
                      dbo.Vw_BoxDet ON dbo.Vw_JCIssReport.Fk_BoxID = dbo.Vw_BoxDet.Pk_BoxID INNER JOIN
                      dbo.gen_DeliverySchedule ON dbo.Vw_BoxDet.Pk_BoxID = dbo.gen_DeliverySchedule.Fk_BoxID AND dbo.Vw_JCIssReport.Fk_Schedule = dbo.gen_DeliverySchedule.Pk_DeliverySechedule AND 
                      dbo.Vw_BoxDet.Pk_PartPropertyID = dbo.gen_DeliverySchedule.Fk_PartID LEFT OUTER JOIN
                      dbo.Vw_Iss_Return ON dbo.Vw_JCIssReport.Pk_MaterialIssueID = dbo.Vw_Iss_Return.Fk_IssueId
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[36] 4[14] 2[10] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Vw_BoxDet"
            Begin Extent = 
               Top = 30
               Left = 534
               Bottom = 263
               Right = 717
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "gen_DeliverySchedule"
            Begin Extent = 
               Top = 163
               Left = 922
               Bottom = 376
               Right = 1116
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "Vw_JCIssReport"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 233
               Right = 251
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Vw_Iss_Return"
            Begin Extent = 
               Top = 14
               Left = 1124
               Bottom = 230
               Right = 1332
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 20
         Width = 284
         Width = 1875
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
       ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_JobBoxDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'  Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_JobBoxDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_JobBoxDetails'
GO
/****** Object:  View [dbo].[Vw_BOM]    Script Date: 11/23/2018 12:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_BOM]
AS
SELECT     dbo.Vw_PoBom.PendQty, dbo.Vw_PaperSummary.SWt, dbo.Vw_PaperSummary.Pk_Material, dbo.Vw_PaperSummary.Pk_BoxID, dbo.Vw_PaperSummary.Name, dbo.Vw_PaperSummary.Quantity, 
                      dbo.Vw_PoBom.Pk_PONo, dbo.Vw_AssStock.Pk_JobCardID, dbo.Vw_AssStock.RM_Consumed
FROM         dbo.Vw_PaperSummary FULL OUTER JOIN
                      dbo.Vw_AssStock ON dbo.Vw_PaperSummary.Pk_BoxID = dbo.Vw_AssStock.Pk_BoxID FULL OUTER JOIN
                      dbo.Vw_PoBom ON dbo.Vw_PaperSummary.Pk_Material = dbo.Vw_PoBom.Pk_Material
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[42] 4[15] 2[17] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Vw_PaperSummary"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 233
               Right = 200
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Vw_PoBom"
            Begin Extent = 
               Top = 147
               Left = 397
               Bottom = 374
               Right = 550
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Vw_AssStock"
            Begin Extent = 
               Top = 3
               Left = 655
               Bottom = 232
               Right = 815
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 13
         Width = 284
         Width = 2220
         Width = 2100
         Width = 2160
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_BOM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_BOM'
GO
