﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;
using System.IO;


namespace CogitoStreamline.Controllers
{

    public class TransPaymtController : CommonController
    {
        TransPaymt oColor = new TransPaymt();

        public TransPaymtController()
        {
            oDoaminObject = new TransPaymt();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Transporter Payment";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            oColor.Dispose();
            base.Dispose(disposing);
        }


        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                TransPaymt oColor = oDoaminObject as TransPaymt;
                oColor.ID = decimal.Parse(pId);
                Trans_Payment oAtualObject = oColor.DAO as Trans_Payment;
                //Create a anominious object here to break the circular reference

                var oColorToDisplay = new
                {

                
                    //TransporterName = oAtualObject.TransporterName
                    Pk_ID = oAtualObject.Pk_ID,
                    Fk_Transporter = oAtualObject.Fk_Transporter,
                    AmtPaid = oAtualObject.AmtPaid,
                    InvNo = oAtualObject.InvNo,
                    ChequeNo = oAtualObject.ChequeNo,                   
                    ChequeDate = DateTime.Parse(oAtualObject.ChequeDate.ToString()).ToString("dd/MM/yyyy"),
                    Pay_Date = DateTime.Parse(oAtualObject.Pay_Date.ToString()).ToString("dd/MM/yyyy"),

                };

                return Json(new { success = true, data = oColorToDisplay });
            }
            else
            {
                var oColorToDisplay = new Trans_Payment();
                return Json(new { success = true, data = oColorToDisplay });
            }
        }



        //[HttpPost]
        //public JsonResult ColorDuplicateChecking(string data = "")
        //{
        //    try
        //    {
        //        Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        //        string sColorName = oValues["TransporterName"];

        //        List<SearchParameter> oSearchParams = new List<SearchParameter>();
        //        oSearchParams.Add(new SearchParameter("TransporterName", sColorName));

        //        SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

        //        List<EntityObject> oColorToDisplayObjects = oSearchResult.ListOfRecords;
        //        List<gen_Color> oColorObjects = oColorToDisplayObjects.Select(p => p).OfType<gen_Color>().ToList();

        //        var oColorToDisplay = oColorObjects.Select(p => new
        //        {
        //            Pk_ID = p.Pk_ID,
        //            TransporterName = p.TransporterName
        //        }).ToList().OrderBy(s => s.TransporterName);

        //        return Json(new { Success = true, Records = oColorToDisplay, TotalRecordCount = oSearchResult.RecordCount });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { Success = false, Message = ex.Message });
        //    }
        //}

        [HttpPost]
        public JsonResult TransPaymtListByFiter(string ChequeNo = "", string Pk_ID = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_ID", Pk_ID));
                oSearchParams.Add(new SearchParameter("ChequeNo", ChequeNo));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oColorToDisplayObjects = oSearchResult.ListOfRecords;
                List<Trans_Payment> oColorObjects = oColorToDisplayObjects.Select(p => p).OfType<Trans_Payment>().ToList();

                //Create a anominious object here to break the circular reference
                var oColorToDisplay = oColorObjects.Select(p => new
                {
                    Pk_ID = p.Pk_ID,
                    Fk_Transporter=p.TransporterM.TransporterName,
                    AmtPaid=p.AmtPaid,
                     InvNo=p.InvNo,
                    ChequeNo=p.ChequeNo,     
                    ChequeDate = p.ChequeDate != null ? DateTime.Parse(p.ChequeDate.ToString()).ToString("dd/MM/yyyy") : "",
                    Pay_Date = p.Pay_Date != null ? DateTime.Parse(p.Pay_Date.ToString()).ToString("dd/MM/yyyy") : "",                  

                }).ToList();

                return Json(new { Result = "OK", Records = oColorToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
    }
}
