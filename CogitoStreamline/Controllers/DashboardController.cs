﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.DomainModel;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using WebGareCore.CommonObjects.Graphs;
using CogitoStreamLineModel.DomainModel;
using Quartz.Impl;
using Quartz.Impl.Triggers;
using Quartz;
using WebGareCore.CommonObjects;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;


namespace CogitoStreamline.Controllers
{
    public class DashboardController : Controller
    {

        public ActionResult Index()
        {
            //var schedulerFactory = new StdSchedulerFactory();
            //IScheduler scheduler = schedulerFactory.GetScheduler();

            //var jobDetails = new JobDetailImpl("MessageJob", typeof(MessageJob));
            //var trigger = new CalendarIntervalTriggerImpl("myTrigger", IntervalUnit.Second, 3);

            //scheduler.ScheduleJob(jobDetails, trigger);
            //scheduler.Start();
      
            return View();
        }
        ActivitiesRepository oActives = new ActivitiesRepository();
        //[HttpPost]
        //public JsonResult SearchActivities(string data = "")
        //{
        //    try
        //    {
        //        Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        //        string sActivtyName = oValues["SearchDetails"];

        //        List<SearchParameter> oSearchParams = new List<SearchParameter>();
        //        oSearchParams.Add(new SearchParameter("ActivityName", sActivtyName));

        //        SearchResult oSearchResult = oActives.Search(oSearchParams);

        //        List<EntityObject> oActivitiesToDisplayObjects = oSearchResult.ListOfRecords;
        //        List<ideActivity> oActivitiesObjects = oActivitiesToDisplayObjects.Select(p => p).OfType<ideActivity>().ToList();

        //        var oActivityToDisplay = oActivitiesObjects.Select(p => new
        //        {
        //            Pk_Activities = p.Pk_Activities,
        //            ActivityName = p.ActivityName
        //        }).ToList();
        //        ViewBag.Activities = oActivityToDisplay;
        //        return Json(new { Success = true, Records = oActivityToDisplay, TotalRecordCount = oSearchResult.RecordCount });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { Success = false, Message = ex.Message });
        //    }
        //}

        [HttpPost]
        public JsonResult GetModulesBySearch(string data)
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            object oModeules = oActives.SearchModules(Session["UserName"].ToString(), oValues["Search"]);
            return Json(oModeules);
        }

        [HttpPost]
        public JsonResult MonthWiseEnquiryOrders()
        {
            LineGraphData oGraphData = new LineGraphData();

            oGraphData.AddGraphSeries("Enquiry");

            Enquiry oEnquiry = new Enquiry();

            Dictionary<int, double> oEnq = oEnquiry.GetMonthWiseEnquiryCount();

            foreach (KeyValuePair<int, double> oItem in oEnq)
            {
                oGraphData.AddGraphData(0, oItem.Key, oItem.Value);
            }

            oGraphData.AddGraphSeries("Order");

            Order oOrder = new Order();

            Dictionary<int, double> oOrderCount = oOrder.GetMonthWiseOrderCount();

            foreach (KeyValuePair<int, double> oItem in oOrderCount)
            {
                oGraphData.AddGraphData(1, oItem.Key, oItem.Value);
            }

            return Json(oGraphData.GetFormattedData());
        }

        //[HttpPost]
        //public JsonResult OrderStatusReport()
        //{
        //    PiGraphData oGraphData = new PiGraphData();

        //    Order oOrder = new Order();

        //    Dictionary<string, int> oResult = oOrder.GetOrderSatusCount();

        //    foreach (KeyValuePair<string, int> oItem in oResult)
        //    {
        //        oGraphData.AddPercentage(oItem.Value, oItem.Key);
        //    }

        //    return Json(oGraphData.GetFormattedData());
        //}

        [HttpPost]
        public JsonResult OrderDeliveryStatus()
        {
            PiGraphData oGraphData = new PiGraphData();

            Order oOrder = new Order();

            Dictionary<string, int> oResult = oOrder.GetDeliveryScheduleStatus();

            foreach (KeyValuePair<string, int> oItem in oResult)
            {
                oGraphData.AddPercentage(oItem.Value, oItem.Key);
            }

            return Json(oGraphData.GetFormattedData());
        }

        [HttpPost]
        public JsonResult OnTimeDeliveryPercentage()
        {

            Order oOrder = new Order();

            double oResult = oOrder.OnTimeDeliveryPercentage();

            return Json(new { data = oResult });

        }

        [HttpPost]
        public JsonResult QuaterlyCustomersYear()
        {

            Customer oCustomers = new Customer();

            List<int> oResult = oCustomers.QuaterlyCustomersYear();

            return Json(new { data = oResult });

        }

        [HttpPost]
        public JsonResult QuaterlyCustomers()
        {

            Customer oCustomers = new Customer();

            Dictionary<string, int> oResult = oCustomers.QuaterlyCustomers();

            return Json(new { data = oResult });

        }


        [HttpPost]
        public JsonResult IndentRejectedReport()
        {
            MaterialIndent oMaterialIndent = new MaterialIndent();
            Dictionary<string, int> oResult = oMaterialIndent.GetIndentRejectedCount();
            return Json(new { data = oResult, Count = oResult.Count() });
        }

        [HttpPost]
        public JsonResult MinLevelStocks()
        {
            Material oMaterial = new Material();
            Dictionary<string, string> oResult = oMaterial.GetMinLevelStocks();
            return Json(new { data = oResult, Count = oResult.Count() });
        }

        [HttpPost]
        public JsonResult OrderDeliveries()
        {
            Order oOrder = new Order();
            Dictionary<string, string> oResult = oOrder.OrderDeliveries();
            return Json(new { data = oResult, Count = oResult.Count() });
        }

        [HttpPost]
        public JsonResult IndentApproval()
        {
            MaterialIndent oMaterialIndent = new MaterialIndent();
            Dictionary<string, string> oResult = oMaterialIndent.IndentApproval();
            //List<SearchParameter> oSearchParams = new List<SearchParameter>();
            //oSearchParams.Add(new SearchParameter("Status", "1"));
            //SearchResult oSearchResult = oMaterialIndent.Search(oSearchParams);
            //List<EntityObject> oMaterialIndentsToDisplayObjects = oSearchResult.ListOfRecords;
            //List<Inv_MaterialIndentMaster> oMaterialIndentObjects = oMaterialIndentsToDisplayObjects.Select(p => p).OfType<Inv_MaterialIndentMaster>().ToList();

            //var oMaterialIndentsToDisplay = oMaterialIndentObjects.Select(p => new
            //{
            //    Pk_MaterialOrderMasterId = p.Pk_MaterialOrderMasterId,
            //    MaterialIndentDate = p.MaterialIndentDate
            //}).ToList();
            //return Json(new { Result = "OK", Records = oMaterialIndentsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            return Json(new { data = oResult, Count = oResult.Count() });
        }

        [HttpPost]
        public JsonResult EnquiryApproval()
        {
            Enquiry oEnquiry = new Enquiry();
            Dictionary<string, string> oResult = oEnquiry.EnquiryApproval();

            return Json(new { data = oResult, Count = oResult.Count() });
        }

        [HttpPost]
        public JsonResult InwardStock()
        {
            InwardMaterial oInwardStock = new InwardMaterial();
            Dictionary<string, string> oResult = oInwardStock.InwardStock();
            return Json(new { data = oResult, Count = oResult.Count() });
        }

        [HttpPost]
        public JsonResult MonthlySalesStatus()
        {
            Billing oBilling = new Billing();
            Dictionary<string, string> oResult = oBilling.MonthlySalesStatus();
            return Json(new { data = oResult, Count = oResult.Count() });
        }


    }
}
