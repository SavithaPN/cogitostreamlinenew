﻿//Name            : Employee Designation Controller Class  (one of the material )
//Description     :   Contains the page class definition, every page in the application will be instance of this class with the following
//                    1. Load Method --- Used during Editing 2. List by filter method -- used for data loading in the jtable
// 3.Designation duplicatechecking for checking the duplicate entry of the Designation name
//Author          :   CH.V.N.Ravi Kumar
//Date            :   21/10/2015
//CRH Number      :   
//Modifications   :


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;
using System.Data;
using System.IO;
using System.Drawing.Printing;



namespace CogitoStreamline.Controllers
{
    public class EmployeeDesignationController : CommonController
    {
        //
        // GET: /EmployeeDesignation/
        EmployeeDesignation oEmployeeDesignation = new EmployeeDesignation();
        public EmployeeDesignationController()
        {
            oDoaminObject = new EmployeeDesignation();
        }


        public override ActionResult Index()
        {
            ViewBag.Header = "Employee Designation";
            return base.Index();
        }

        

        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                EmployeeDesignation oEmployeeDesignation = oDoaminObject as EmployeeDesignation;
                oEmployeeDesignation.ID = decimal.Parse(pId);
                emp_EmployeeDesignation oAtualObject = oEmployeeDesignation.DAO as emp_EmployeeDesignation;
                //Create a anominious object here to break the circular reference
                var oEmployeeDesignationToDisplay = new
                {
                    Pk_EmployeeDesignationId = oAtualObject.Pk_EmployeeDesignationId,
                    DesignationName = oAtualObject.DesignationName,
                    Description = oAtualObject.Description
                };

                return Json(new { success = true, data = oEmployeeDesignationToDisplay });
            }
            else
            {
                var oEmployeeDesignationToDisplay = new emp_EmployeeDesignation();
                return Json(new { success = true, data = oEmployeeDesignationToDisplay });
            }
        }

        [HttpPost]
        public JsonResult EmployeeDesignationListByFiter(string Pk_EmployeeDesignationId = "", string EmployeeDesignationName = null, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_EmployeeDesignationId", Pk_EmployeeDesignationId));
                oSearchParams.Add(new SearchParameter("DesignationName", EmployeeDesignationName));

                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oEmployeeDesignationsToDisplayObjects = oSearchResult.ListOfRecords;
                List<emp_EmployeeDesignation> oEmployeeDesignationObjects = oEmployeeDesignationsToDisplayObjects.Select(p => p).OfType<emp_EmployeeDesignation>().ToList();

                //Create a anominious object here to break the circular reference
                var oEmployeeDesignationsToDisplay = oEmployeeDesignationObjects.Select(p => new
                {
                    Pk_EmployeeDesignation = p.Pk_EmployeeDesignationId,
                    DesignationName = p.DesignationName

                }).ToList();

                return Json(new { Result = "OK", Records = oEmployeeDesignationsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult EmployeeDesignationDuplicateChecking(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sDesignationName = oValues["DesignationName"];

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("DesignationName", sDesignationName));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oEmployeeDesignationToDisplayObjects = oSearchResult.ListOfRecords;
                List<emp_EmployeeDesignation> oEmployeeDesignationObjects = oEmployeeDesignationToDisplayObjects.Select(p => p).OfType<emp_EmployeeDesignation>().ToList();

                var oEmployeeDesignationToDisplay = oEmployeeDesignationObjects.Select(p => new
                {
                    Pk_EmployeeDesignationId = p.Pk_EmployeeDesignationId,
                    DesignationName = p.DesignationName
                }).ToList().OrderBy(s => s.DesignationName);

                return Json(new { Success = true, Records = oEmployeeDesignationToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }
     

    }
}
