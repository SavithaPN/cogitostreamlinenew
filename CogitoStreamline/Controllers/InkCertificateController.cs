﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;

using System.IO;
using Common.Logging.Configuration;
using System.Collections.Specialized;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Data;
using System.Drawing.Printing;

namespace CogitoStreamline.Controllers
{

    public class InkCertificateController : CommonController
    {
        InkParams oColor = new InkParams();

        public InkCertificateController()
        {
            oDoaminObject = new InkCert();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Ink Quality Certificate";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            oColor.Dispose();
            base.Dispose(disposing);
        }


        //[HttpPost]
        //public JsonResult getChar(string pId = "")
        //{
        //    List<InkParams> oListOfBoxType = oColor.Search(null).ListOfRecords.OfType<InkParams>().ToList();

        //    var oBoxToDisplay = oListOfBoxType.Select(p => new
        //    {
        //        Name = p.Name,
        //        Id = p.Pk_CharID

        //    });

        //    return Json(oBoxToDisplay);
        //}
        [HttpPost]
        public override JsonResult Load(string data = "")
        {

            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                InkCert oPOrders = oDoaminObject as InkCert;
                // PurchaseOrderMs oPOrders = oDoaminObject as PurchaseOrderM;
                oPOrders.ID = decimal.Parse(pId);
                InkCertificate oAtualObject = oPOrders.DAO as InkCertificate;

                int i = -1;

                var oMaterials = oAtualObject.InkCertificateDetails.Select(p => new
                {
                    slno = ++i,
                    Pk_IdDet = p.Pk_IdDet,
                    Fk_ID = p.Fk_ID,
                    Fk_Params = p.Fk_Params,
                    Spec_Target = p.Spec_Target,
                    Spec_Tolerance = p.Spec_Tolerance,
                    Fk_Characteristics = p.Fk_Params,
                    txtFMaterial = p.InkChar.Name,
                    Results=p.Results,

                });

                var oPOrderDisplay = new
                {
                    Pk_Id = oAtualObject.Pk_Id,
                    Fk_Material = oAtualObject.Fk_Material,
                    BatchNo = oAtualObject.BatchNo,
                    //InvDate = DateTime.Parse(oAtualObject.InvDate.ToString()).ToString("dd/MM/yyyy"),
                    Quantity = oAtualObject.Quantity,
                    Tested = oAtualObject.Tested,
                    Approved = oAtualObject.Approved,
                    Description = oAtualObject.Description,

                    MaterialData = Json(oMaterials).Data
                };

                return Json(new { success = true, data = oPOrderDisplay });
            }
            else
            {
                var oReturnGoodFromSiteDisplay = new PaperCertificate();
                return Json(new { success = true, data = oReturnGoodFromSiteDisplay });
            }
        }





        [HttpPost]
        public JsonResult InkCertListByFiter(string Name = "", string Pk_Id = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_Id", Pk_Id));
                oSearchParams.Add(new SearchParameter("Name", Name));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oColorToDisplayObjects = oSearchResult.ListOfRecords;
                List<InkCertificate> oColorObjects = oColorToDisplayObjects.Select(p => p).OfType<InkCertificate>().ToList();

                //Create a anominious object here to break the circular reference
                var oColorToDisplay = oColorObjects.Select(p => new
                {
                    Pk_Id = p.Pk_Id,
                    Name = p.Inv_Material.Name,
                    Description = p.Description,
                    BatchNo = p.BatchNo,
                   // InvDate = DateTime.Parse(p.InvDate.ToString()).ToString("dd/MM/yyyy"),


                }).ToList().OrderBy(s => s.Pk_Id);

                return Json(new { Result = "OK", Records = oColorToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


        public ActionResult InkCert(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["JCNO"]);

                List<Vw_InkCertificate> BillList = new List<Vw_InkCertificate>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();


                BillList = dc.Vw_InkCertificate.Where(x => x.Pk_Id == sInvno).Select(x => x).OfType<Vw_InkCertificate>().ToList();

                //            SELECT Vw_PaperCertificate.Pk_Id, Vw_PaperCertificate.Invno, Vw_PaperCertificate.InvDate, Vw_PaperCertificate.Name,
                //               Vw_PaperCertificate.TestMethod, Vw_PaperCertificate.Target, Vw_PaperCertificate.Acceptance, Vw_PaperCertificate.Result1,
                //               Vw_PaperCertificate.Result2, Vw_PaperCertificate.Result3, Vw_PaperCertificate.Result4, Vw_PaperCertificate.Result5,
                //               Vw_PaperCertificate.Result6, Vw_PaperCertificate.MinVal, Vw_PaperCertificate.MaxVal, Vw_PaperCertificate.AvgVal, 
                //               Vw_PaperCertificate.Remarks, Vw_PaperCertificate.Quantity, Vw_PaperCertificate.Tested, Vw_PaperCertificate.Approved,
                //               Vw_PaperCertificate.VendorName, Vw_PaperCertificate.UOM, Vw_PaperCertificate.Pk_CharID, Vw_PaperCertificate.Pk_IdDet,
                //               Vw_PaperCertificate.Fk_Material
                //FROM   Balaji.dbo.Vw_PaperCertificate Vw_PaperCertificate
                //ORDER BY Vw_PaperCertificate.Pk_Id, Vw_PaperCertificate.Pk_IdDet



                if (BillList.Count > 0)
                {
                    DataTable dt = new DataTable();

                    dt.Columns.Add("Pk_Id");
                    dt.Columns.Add("BatchNo");
                 //   dt.Columns.Add("InvDate");
                    dt.Columns.Add("Name");
                  //  dt.Columns.Add("Remarks");
                    dt.Columns.Add("Quantity");
                    dt.Columns.Add("Tested");
                    dt.Columns.Add("Approved");
                    dt.Columns.Add("Spec_Target");
                    dt.Columns.Add("Fk_Params");
                    dt.Columns.Add("Expr1");
                    dt.Columns.Add("Spec_Tolerance");
                    dt.Columns.Add("Results");
                    dt.Columns.Add("Fk_Material");
                    dt.Columns.Add("Pk_IdDet");

                    foreach (Vw_InkCertificate entity in BillList)
                    {
                        DataRow row = dt.NewRow();

                        row["Pk_Id"] = entity.Pk_Id;
                        row["BatchNo"] = entity.BatchNo;
                        //row["InvDate"] = entity.InvDate;
                        row["Name"] = entity.Name;
                        row["Spec_Target"] = entity.Spec_Target;
                        row["Spec_Tolerance"] = entity.Spec_Tolerance;
                        row["Results"] = entity.Results;
                        row["Quantity"] = entity.Quantity;
                        row["Tested"] = entity.Tested;
                        row["Approved"] = entity.Approved;
                        //row["VendorName"] = entity.VendorName;
                        row["Expr1"] = entity.Expr1;
                        row["Fk_Params"] = entity.Fk_Params;
                        row["Fk_Material"] = entity.Fk_Material;
                        row["Pk_IdDet"] = entity.Pk_IdDet;
                        dt.Rows.Add(row);
                    }

                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.InkCertificate orp = new CogitoStreamline.Report.InkCertificate();

                    orp.Load("@\\Report\\InkCertificate.rpt");
                    orp.SetDataSource(dt.DefaultView);


                    CrystalDecisions.CrystalReports.Engine.ReportDocument repDoc = orp;


                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "ICertificate" + sInvno + ".pdf");
                    FileInfo file = new FileInfo(pdfPath);
                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();

                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }

        [HttpPost]

        public JsonResult SaveNew([System.Web.Http.FromBody]string data)
        {
            data = data.TrimStart('=');
            Dictionary<string, object> values = JsonConvert.DeserializeObject<Dictionary<string, object>>(data);
            data = JsonConvert.SerializeObject(values);
            return base.Save(data);


        }

    }

}

