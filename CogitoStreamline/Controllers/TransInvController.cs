﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;
using System.IO;


namespace CogitoStreamline.Controllers
{

    public class TransInvController : CommonController
    {
        Transporter oTransM = new Transporter();
        Trans_Inv oColor = new Trans_Inv();

        public TransInvController()
        {
            oDoaminObject = new Trans_Inv();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Transporter Invoice";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            oColor.Dispose();
            base.Dispose(disposing);
        }


        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                Trans_Inv oColor = oDoaminObject as Trans_Inv;
                oColor.ID = decimal.Parse(pId);
                Trans_Invoice oAtualObject = oColor.DAO as Trans_Invoice;
                //Create a anominious object here to break the circular reference

                var oColorToDisplay = new
                {

                    Pk_ID = oAtualObject.Pk_ID,
                    VehicleNo = oAtualObject.VehicleNo,

                    VehicleName = oAtualObject.VehicleName,
                    DriverName = oAtualObject.DriverName,
                    MobileNo = oAtualObject.MobileNo,
                    Pickup_Point = oAtualObject.Pickup_Point,
                    Drop_Point = oAtualObject.Drop_Point,
                    Invno = oAtualObject.Invno,

                    Amount = oAtualObject.Amount,
                    CustomerName = oAtualObject.CustomerName,
                    Fk_Transporter = oAtualObject.TransporterM.TransporterName,
                    BalAmt = oAtualObject.BalAmt,

                    InvDate = DateTime.Parse(oAtualObject.InvDate.ToString()).ToString("dd/MM/yyyy"),

                };

                return Json(new { success = true, data = oColorToDisplay });
            }
            else
            {
                var oColorToDisplay = new Trans_Invoice();
                return Json(new { success = true, data = oColorToDisplay });
            }
        }


        //[HttpPost]
        //public JsonResult getTransporter(string pId = "")
        //{
        //    List<TransporterM> oListOfTanent = oTransM.Search(null).ListOfRecords.OfType<TransporterM>().ToList();
        //    var oTanentToDisplay = oListOfTanent.Select(p => new
        //    {
        //        Name = p.TransporterName,
        //        Id = p.Pk_ID
        //    });

        //    return Json(oTanentToDisplay);
        //}

        [HttpPost]
        public JsonResult TransInvListByFiter(string VehicleNo = "", string Pk_ID = "", string Fk_Transporter = "", string Invno = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_ID", Pk_ID));
                oSearchParams.Add(new SearchParameter("VehicleNo", VehicleNo));
                oSearchParams.Add(new SearchParameter("Fk_Transporter", Fk_Transporter));
                oSearchParams.Add(new SearchParameter("Invno", Invno));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oColorToDisplayObjects = oSearchResult.ListOfRecords;
                List<Trans_Invoice> oColorObjects = oColorToDisplayObjects.Select(p => p).OfType<Trans_Invoice>().ToList();

                //Create a anominious object here to break the circular reference
                var oColorToDisplay = oColorObjects.Where(p => p.BalAmt > 0).Select(p => new
                {
                    Pk_ID = p.Pk_ID,
                    VehicleNo = p.VehicleNo,
                    VehicleName = p.VehicleName,
                    DriverName = p.DriverName,
                    MobileNo = p.MobileNo,
                    Pickup_Point = p.Pickup_Point,
                    Drop_Point = p.Drop_Point,
                    Invno = p.Invno,
                    InvDate = p.InvDate != null ? DateTime.Parse(p.InvDate.ToString()).ToString("dd/MM/yyyy") : "",

                    Amount = p.Amount,
                    CustomerName = p.CustomerName,
                    Fk_Transporter = p.TransporterM.TransporterName,
                    BalAmt = p.BalAmt,
                }).ToList();

                return Json(new { Result = "OK", Records = oColorToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
    }
}
