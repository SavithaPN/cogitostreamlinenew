﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;

//namespace CogitoStreamline.Controllers
//{
//    public class OrderApprovalController : Controller
//    {
//        //
//        // GET: /OrderApproval/

//        public ActionResult Index()
//        {
//            return View();
//        }

//    }
//}


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;
using System.IO;
using System.Data;
using CrystalDecisions.ReportAppServer.ReportDefModel;
using System.Drawing.Printing;
using CrystalDecisions.Shared;
using WebGareCore.Controls;
using WebGareCore.CommonObjects.WorkFlow;
using System.Net.Http;
using System.Net;
//using System.Web.Http;
//using System.Web.Http;

namespace CogitoStreamline.Controllers
{
    public class OrderApprovalController : CommonController
    {

        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();

        //
        // GET: /Order/
        Customer oCustomer = new Customer();
        //Product oProduct = new Product();
        State oState = new State();

        Order oOB = new Order();
        readonly CogitoStreamLineEntities _oEntities = new CogitoStreamLineEntities();
        public OrderApprovalController()
        {
            oDoaminObject = new Order();
        }

        public class OrderSave
        {
            public DateTime OrderDate
            {
                get;
                set;
            }
            public int Fk_Customer
            {
                get;
                set;
            }
            public string Cust_PO
            {
                get;
                set;
            }
            public int Fk_BoxID
            {
                get;
                set;
            }
            public int OrdQty
            {
                get;
                set;
            }
            public int EnqQty
            {
                get;
                set;
            }

            public DateTime DDate
            {
                get;
                set;
            }
            public int Fk_PartID
            {
                get;
                set;
            }



        }

        public ActionResult SaveOrder(DateTime OrderDate, int Fk_Customer, string Cust_PO, Order[] order)
        {
            string result = "Order";

            gen_Order OrdM = new gen_Order();

            OrdM.OrderDate = OrderDate;
            OrdM.Fk_Customer = Fk_Customer;
            OrdM.Cust_PO = Cust_PO;



            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public override ActionResult Index()
        {
            ViewBag.Header = "Order Approval";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        [HttpPost]
        public JsonResult getCustomer(string pId = "")
        {
            List<gen_Customer> oListOfOrderRelationShips = oCustomer.Search(null).ListOfRecords.OfType<gen_Customer>().ToList();
            var oContriesToDisplay = oListOfOrderRelationShips.Select(p => new
            {
                Name = p.CustomerName,
                Id = p.Pk_Customer
            });

            return Json(oContriesToDisplay);
        }


        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                Order oOrder = oDoaminObject as Order;
                oOrder.ID = decimal.Parse(pId);
                gen_Order oAtualObject = oOrder.DAO as gen_Order;

                int i = -1;

                var oDelivery = oAtualObject.Gen_OrderChild.Select(p => new
                {
                    Pk_OrderChild = p.Pk_OrderChild,
                    slno = ++i,
                    OrdQty = p.OrdQty,
                    txtFk_Box = p.Fk_BoxID,
                    Name = p.ItemPartProperty.BoxSpecs.BoxMaster.Name,
                    EnqQty = p.EnqQty,
                    Fk_Status = p.Fk_Status,
                    Ddate = p.Ddate != null ? DateTime.Parse(p.Ddate.ToString()).ToString("dd/MM/yyyy") : "",
                });


                //Create a anominious object here to break the circular reference
                var oOrderToDisplay = new
                {
                    fk_Customer = oAtualObject.Fk_Customer,
                    Fk_Customer = oAtualObject.Fk_Customer,
                    Cust_PO = oAtualObject.Cust_PO,
                    fk_Enquiry = oAtualObject.Fk_Enquiry,
                    Pk_Order = oAtualObject.Pk_Order,
                    OrderDate = DateTime.Parse(oAtualObject.OrderDate.ToString()).ToString("dd/MM/yyyy"),
                    Product = oAtualObject.Product,
                    PONo = oAtualObject.Cust_PO,
                    //Fk_Status = oAtualObject.Fk_Status,
                    deliverySchedule = Json(oDelivery).Data,
                    //State = oAtualObject.wfState.State,


                };

                return Json(new { success = true, data = oOrderToDisplay });
            }
            else
            {
                var oOrderToDisplay = new gen_Order();
                return Json(new { success = true, data = oOrderToDisplay });
            }
        }

        [HttpPost]
        public JsonResult OrderListByFiter(string OrderDate = "", string CustomerName = "", string Fk_Enquiry = "", string FromEnquiryDate = "", string ToEnquiryDate = "", string Pk_Order = "", string OnlyPending = "", string ProductName = "", string FromOrderDate = "", string ToOrderDate = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {



                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("OnlyPending", OnlyPending));
                oSearchParams.Add(new SearchParameter("CustomerName", CustomerName));
                oSearchParams.Add(new SearchParameter("Fk_Enquiry", Fk_Enquiry));
                oSearchParams.Add(new SearchParameter("FromEnquiryDate", FromEnquiryDate));
                oSearchParams.Add(new SearchParameter("ToEnquiryDate", ToEnquiryDate));
                oSearchParams.Add(new SearchParameter("OrderDate", OrderDate));

                oSearchParams.Add(new SearchParameter("FromOrderDate", FromOrderDate));
                oSearchParams.Add(new SearchParameter("ToOrderDate", ToOrderDate));

                oSearchParams.Add(new SearchParameter("Pk_Order", Pk_Order));
                oSearchParams.Add(new SearchParameter("ProductName", ProductName.ToString()));

                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oOrdersToDisplayObjects = oSearchResult.ListOfRecords;
                List<gen_Order> oOrderObjects = oOrdersToDisplayObjects.Select(p => p).OfType<gen_Order>().ToList();

                //Create a anominious object here to break the circular reference
                var oOrdersToDisplay = oOrderObjects.Where(p=>p.Fk_StatusVal==1).Select(p => new
                {
                    Fk_Customer = p.Fk_Customer,
                    CustomerName = p.gen_Customer.CustomerName,
                    CustID = p.Fk_Customer,
                    //ProductName = p.Product,
                    Quantity = p.Quantity,
                    Price = p.Price,
                    //Status= p.wfState.State,
                    OrderDate = DateTime.Parse(p.OrderDate.ToString()).ToString("dd/MM/yyyy"),
                    Pk_Order = p.Pk_Order,
                    Fk_Enquiry = p.Fk_Enquiry,
                    EnquiryDate = DateTime.Parse(p.eq_Enquiry.Date.ToString()).ToString("dd/MM/yyyy"),
                    //Fk_BoxID=p.Fk_BoxID,
                    //BoxName=p.BoxMaster.Name,
                    PONo = p.Cust_PO,





                }).ToList();

                return Json(new { Result = "OK", Records = oOrdersToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        public ActionResult ChangeStatus(string data = "")
        {
            try
            {

                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["Pk_Order"]);
                //string rollNo = (oValues["RollNo"].ToString());
                //int StockVal = Convert.ToInt32(oValues["Stock"]);
                //int pkmat = Convert.ToInt32(oValues["pkmat"]);
                //   List<JobCardMaster> PaperList = new List<JobCardMaster>();

                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();
                if (sInvno > 0)
                {
                    gen_Order oJC = _oEntites.gen_Order.Where(x => x.Pk_Order == sInvno).Single();

                    oJC.Fk_StatusVal = 2;
                    oJC.Accepted = "Yes";
                    //oPaperList.RollNo = rollNo;
                    //oPaperList.Quantity = StockVal;
                    //oPaperList.Fk_Material = pkmat;

                    _oEntites.SaveChanges();
                }




                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }
    

        [HttpPost]
        public JsonResult OrderScheduledListByFiter(string OrderDate = "", string CustomerName = "", string Fk_Enquiry = "", string FromEnquiryDate = "", string ToEnquiryDate = "", string Pk_Order = "", string OnlyPending = "", string ProductName = "", string FromOrderDate = "", string ToOrderDate = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("OnlyPending", OnlyPending));
                oSearchParams.Add(new SearchParameter("CustomerName", CustomerName));
                oSearchParams.Add(new SearchParameter("Fk_Enquiry", Fk_Enquiry));
                oSearchParams.Add(new SearchParameter("FromEnquiryDate", FromEnquiryDate));
                oSearchParams.Add(new SearchParameter("ToEnquiryDate", ToEnquiryDate));
                oSearchParams.Add(new SearchParameter("OrderDate", OrderDate));

                oSearchParams.Add(new SearchParameter("FromOrderDate", FromOrderDate));
                oSearchParams.Add(new SearchParameter("ToOrderDate", ToOrderDate));

                oSearchParams.Add(new SearchParameter("Pk_Order", Pk_Order));
                oSearchParams.Add(new SearchParameter("ProductName", ProductName.ToString()));

                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oOB.SearchSch(oSearchParams);

                List<EntityObject> oOrdersToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_SchOrders> oOrderObjects = oOrdersToDisplayObjects.Select(p => p).OfType<Vw_SchOrders>().ToList();

                //Create a anominious object here to break the circular reference
                var oOrdersToDisplay = oOrderObjects.Select(p => new
                {
                    Fk_Customer = p.Fk_Customer,
                    CustomerName = p.CustomerName,
                    CustID = p.Fk_Customer,
                    //ProductName = p.Product,
                    //Quantity = p.Quantity,
                    //Price = p.Price,
                    //Status= p.wfState.State,
                    OrderDate = DateTime.Parse(p.OrderDate.ToString()).ToString("dd/MM/yyyy"),
                    Pk_Order = p.Pk_Order,
                    //Fk_Enquiry = p.Fk_Enquiry,
                    //EnquiryDate = DateTime.Parse(p.eq_Enquiry.Date.ToString()).ToString("dd/MM/yyyy"),
                    //Fk_BoxID=p.Fk_BoxID,
                    //BoxName=p.BoxMaster.Name,
                    PONo = p.Cust_PO


                }).ToList();

                return Json(new { Result = "OK", Records = oOrdersToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


        [HttpPost]
        public JsonResult OrderUnScheduledListByFiter(string OrderDate = "", string CustomerName = "", string Fk_Enquiry = "", string FromEnquiryDate = "", string ToEnquiryDate = "", string Pk_Order = "", string OnlyPending = "", string ProductName = "", string FromOrderDate = "", string ToOrderDate = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("OnlyPending", OnlyPending));
                oSearchParams.Add(new SearchParameter("CustomerName", CustomerName));
                oSearchParams.Add(new SearchParameter("Fk_Enquiry", Fk_Enquiry));
                oSearchParams.Add(new SearchParameter("FromEnquiryDate", FromEnquiryDate));
                oSearchParams.Add(new SearchParameter("ToEnquiryDate", ToEnquiryDate));
                oSearchParams.Add(new SearchParameter("OrderDate", OrderDate));

                oSearchParams.Add(new SearchParameter("FromOrderDate", FromOrderDate));
                oSearchParams.Add(new SearchParameter("ToOrderDate", ToOrderDate));

                oSearchParams.Add(new SearchParameter("Pk_Order", Pk_Order));
                oSearchParams.Add(new SearchParameter("ProductName", ProductName.ToString()));

                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oOB.SearchUnSch(oSearchParams);

                List<EntityObject> oOrdersToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_UnSchOrders> oOrderObjects = oOrdersToDisplayObjects.Select(p => p).OfType<Vw_UnSchOrders>().ToList();

                //Create a anominious object here to break the circular reference
                var oOrdersToDisplay = oOrderObjects.Select(p => new
                {
                    Fk_Customer = p.Fk_Customer,
                    CustomerName = p.CustomerName,
                    CustID = p.Fk_Customer,
                    //ProductName = p.Product,
                    //Quantity = p.Quantity,
                    //Price = p.Price,
                    //Status= p.wfState.State,
                    OrderDate = DateTime.Parse(p.OrderDate.ToString()).ToString("dd/MM/yyyy"),
                    Pk_Order = p.Pk_Order,
                    //Fk_Enquiry = p.Fk_Enquiry,
                    //EnquiryDate = DateTime.Parse(p.eq_Enquiry.Date.ToString()).ToString("dd/MM/yyyy"),
                    //Fk_BoxID=p.Fk_BoxID,
                    //BoxName=p.BoxMaster.Name,
                    PONo = p.Cust_PO


                }).ToList();

                return Json(new { Result = "OK", Records = oOrdersToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        //[HttpPost]
        //public JsonResult IssueOrderList(string OrderDate = "", string CustomerName = "", string Fk_Enquiry = "", string FromEnquiryDate = "", string ToEnquiryDate = "", string Pk_Order = "", string OnlyPending = "", string ProductName = "", string FromOrderDate = "", string ToOrderDate = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        //{
        //    try
        //    {
        //        List<SearchParameter> oSearchParams = new List<SearchParameter>();
        //        oSearchParams.Add(new SearchParameter("OnlyPending", OnlyPending));
        //        oSearchParams.Add(new SearchParameter("CustomerName", CustomerName));
        //        oSearchParams.Add(new SearchParameter("Fk_Enquiry", Fk_Enquiry));
        //        oSearchParams.Add(new SearchParameter("FromEnquiryDate", FromEnquiryDate));
        //        oSearchParams.Add(new SearchParameter("ToEnquiryDate", ToEnquiryDate));
        //        oSearchParams.Add(new SearchParameter("OrderDate", OrderDate));

        //        oSearchParams.Add(new SearchParameter("FromOrderDate", FromOrderDate));
        //        oSearchParams.Add(new SearchParameter("ToOrderDate", ToOrderDate));

        //        oSearchParams.Add(new SearchParameter("Pk_Order", Pk_Order));
        //        oSearchParams.Add(new SearchParameter("ProductName", ProductName.ToString()));

        //        oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
        //        oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

        //        SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

        //        List<EntityObject> oOrdersToDisplayObjects = oSearchResult.ListOfRecords;
        //        List<gen_Order> oOrderObjects = oOrdersToDisplayObjects.Select(p => p).OfType<gen_Order>().ToList();

        //        //Create a anominious object here to break the circular reference
        //        var oOrdersToDisplay = oOrderObjects.Select(p => new
        //        {
        //            Fk_Customer = p.Fk_Customer,
        //            CustomerName = p.gen_Customer.CustomerName,
        //            CustID = p.Fk_Customer,
        //            //ProductName = p.Product,
        //            Quantity = p.Quantity,
        //            Price = p.Price,
        //            //Status= p.wfState.State,
        //            OrderDate = DateTime.Parse(p.OrderDate.ToString()).ToString("dd/MM/yyyy"),
        //            Pk_Order = p.Pk_Order,
        //            Fk_Enquiry = p.Fk_Enquiry,
        //            EnquiryDate = DateTime.Parse(p.eq_Enquiry.Date.ToString()).ToString("dd/MM/yyyy"),
        //            //Fk_BoxID=p.Fk_BoxID,
        //            //BoxName=p.BoxMaster.Name,
        //            PONo = p.Cust_PO


        //        }).ToList();

        //        return Json(new { Result = "OK", Records = oOrdersToDisplay, TotalRecordCount = oSearchResult.RecordCount });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { Result = "ERROR", Message = ex.Message });
        //    }
        //}


        [HttpPost]
        public ActionResult OrdDet(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);


            string pId = oValues["Pk_Order"];
            if (pId != "")
            {
                var OrdID = int.Parse(pId);
                ModelManuplationResult oResult = new ModelManuplationResult();

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_Order", pId));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oEnquirysToDisplayObjects = oSearchResult.ListOfRecords;
                List<gen_Order> oEnquiryObjects = oEnquirysToDisplayObjects.Select(p => p).OfType<gen_Order>().ToList();

                var oEnquiryToDisplay = oEnquiryObjects.Select(p => new
                {

                    Fk_Customer = p.Fk_Customer,
                    CustomerName = p.gen_Customer.CustomerName,
                    CustID = p.Fk_Customer,
                    Fk_Enquiry = p.Fk_Enquiry,
                    PONo = p.Cust_PO,
                    //Quantity = p.Gen_OrderChild.o

                });

                return Json(new { success = true, data = oEnquiryToDisplay });

            }
            return null;
        }

        [HttpPost]
        public JsonResult OrderDeliveryDetails(string Pk_Order = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                Order oOrder = oDoaminObject as Order;
                oOrder.ID = decimal.Parse(Pk_Order);
                gen_Order oOrderObjects = oOrder.DAO as gen_Order;

                var oDeliveryScheduleToDisplay = oOrderObjects.Gen_OrderChild.Select(p => new
                {
                    Pk_OrderChild = p.Pk_OrderChild,
                    Ddate = DateTime.Parse(p.Ddate.ToString()).ToString("dd/MM/yyyy"),
                    OrdQty = p.OrdQty,
                    EnqQty = p.EnqQty,
                    //   BName=p.BoxMaster.Name,
                    BName = p.ItemPartProperty.BoxSpecs.BoxMaster.Name,
                    BId = p.ItemPartProperty.BoxSpecs.BoxMaster.Pk_BoxID,
                    // BId=p.BoxMaster.Pk_BoxID,
                    PartId = p.Fk_PartID,
                    PartName = p.ItemPartProperty.PName,

                }).ToList();

                return Json(new { Result = "OK", Records = oDeliveryScheduleToDisplay, TotalRecordCount = oDeliveryScheduleToDisplay.Count() });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult OrderIndentDetails(string CustomerName = "", string GSM = "", string BF = "", string Deckle = "", string Mill = "", string BoxName = "", string MaterialName = "", string Pk_Order = "", string Pk_BoxID = "", string ProductName = "", string FromOrderDate = "", string ToOrderDate = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();


                oSearchParams.Add(new SearchParameter("Pk_Order", Pk_Order));
                oSearchParams.Add(new SearchParameter("Pk_BoxID", Pk_BoxID));
                oSearchParams.Add(new SearchParameter("ProductName", ProductName.ToString()));
                oSearchParams.Add(new SearchParameter("CustomerName", CustomerName));
                oSearchParams.Add(new SearchParameter("MaterialName", MaterialName));
                oSearchParams.Add(new SearchParameter("BoxName", BoxName));
                oSearchParams.Add(new SearchParameter("GSM", GSM));
                oSearchParams.Add(new SearchParameter("BF", BF));
                oSearchParams.Add(new SearchParameter("Decke", Deckle));
                oSearchParams.Add(new SearchParameter("Mill", Mill));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oOB.SearchIndent(oSearchParams);

                List<EntityObject> oOrdersToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_PaperReqDetIndent> oOrderObjects = oOrdersToDisplayObjects.Select(p => p).OfType<Vw_PaperReqDetIndent>().ToList();

                //Create a anominious object here to break the circular reference
                var oOrdersToDisplay = oOrderObjects.Select(p => new
                {
                    Pk_Material = p.Pk_Material,
                    Pk_Order = p.Fk_OrderID,
                    Pk_BoxID = p.Pk_BoxID,
                    MName = p.Name,
                    BName = p.BName,
                    OrdQty = p.OrdQty,
                    PaperReq = p.PaperReq,
                    Color = p.ColorName,
                    Mill = p.MillName,
                    PONo = p.Cust_PO,
                    OrderDate = DateTime.Parse(p.OrderDate.ToString()).ToString("dd/MM/yyyy"),
                    CustomerName = p.CustomerName,
                    DelDate = DateTime.Parse(p.Ddate.ToString()).ToString("dd/MM/yyyy"),

                    MillID = p.Pk_Mill,



                }).ToList().OrderByDescending(p => p.Pk_Order);

                return Json(new { Result = "OK", Records = oOrdersToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        public ActionResult OrderRep(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["OrderID"]);

                List<VwOrderRep> BillList = new List<VwOrderRep>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                //BillList = dc.VwInvoices.ToList();
                BillList = dc.VwOrderRep.Where(x => x.Pk_Order == sInvno).Select(x => x).OfType<VwOrderRep>().ToList();


                if (BillList.Count > 0)
                {
                    DataTable dt = new DataTable();

                    dt.Columns.Add("Name");
                    dt.Columns.Add("CustomerName");
                    dt.Columns.Add("Pk_Order");
                    dt.Columns.Add("OrderDate");
                    dt.Columns.Add("OrdQty");
                    dt.Columns.Add("EnqQty");
                    dt.Columns.Add("Pk_BoxID");
                    dt.Columns.Add("Ddate");
                    dt.Columns.Add("PName");
                    dt.Columns.Add("Cust_PO");
                    dt.Columns.Add("Pk_OrderChild");

                    foreach (VwOrderRep entity in BillList)
                    {
                        DataRow row = dt.NewRow();

                        row["Name"] = entity.Name;
                        row["CustomerName"] = entity.CustomerName;
                        row["Pk_Order"] = entity.Pk_Order;
                        row["OrderDate"] = entity.OrderDate;
                        row["OrdQty"] = entity.OrdQty;
                        row["EnqQty"] = entity.EnqQty;
                        row["Pk_BoxID"] = entity.Pk_BoxID;
                        row["Ddate"] = entity.Ddate;
                        row["PName"] = entity.PName;
                        row["Cust_PO"] = entity.Cust_PO;
                        row["Pk_OrderChild"] = entity.Pk_OrderChild;
                        dt.Rows.Add(row);
                    }


                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.OrderM orp = new CogitoStreamline.Report.OrderM();

                    orp.Load("@\\Report\\OrderM.rpt");
                    orp.SetDataSource(dt.DefaultView);

                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "Order" + sInvno + ".pdf");
                    FileInfo file = new FileInfo(pdfPath);
                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();

                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }



        public ActionResult OrderEnqDetails(string data = "")
        {
            try
            {
                //_comLayer.parameters.add("BoxID", rows[0].data["BoxID"]);
                //_comLayer.parameters.add("OrderNo", OrdID);OrderNo = "", string BoxID = ""
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sInvno = oValues["OrderNo"];
                string sBId = oValues["BoxID"];

                string sPId = oValues["PartID"];

                List<SearchParameter> oSearchParams = new List<SearchParameter>();


                oSearchParams.Add(new SearchParameter("OrderNo", sInvno));
                oSearchParams.Add(new SearchParameter("BoxID", sBId));
                oSearchParams.Add(new SearchParameter("PartID", sPId));
                //oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                //oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oOB.SearchQtyDet(oSearchParams);

                List<EntityObject> oOrdersToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_TSchQty> oOrderObjects = oOrdersToDisplayObjects.Select(p => p).OfType<Vw_TSchQty>().ToList();

                //Create a anominious object here to break the circular reference
                var oOrdersToDisplay = oOrderObjects.Select(p => new
                {
                    TSchQty = p.TQty,
                    //Pk_Order = p.,
                    //MName = p.MName,
                    //BName = p.BName,
                    //SchQty = p.ScHQty,
                    //PaperStkQty = p.sPaper,
                    //PaperReq = p.PaperR,


                }).ToList();

                return Json(new { Result = "OK", data = oOrdersToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


        [HttpPost]
        public JsonResult OrderChildDetails(string Fk_OrderID = "", string BName = "", string PName = "")
        {
            try
            {
                //Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                //string Fk_OrderID = oValues["Fk_OrderID"];

                List<SearchParameter> oSearchParams = new List<SearchParameter>();


                oSearchParams.Add(new SearchParameter("Fk_OrderID", Fk_OrderID));
                oSearchParams.Add(new SearchParameter("BName", BName));
                oSearchParams.Add(new SearchParameter("PName", PName));
                SearchResult oSearchResult = oOB.SearchOrdDet(oSearchParams);
                List<EntityObject> oOrdersToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_BoxOrder> oOrderObjects = oOrdersToDisplayObjects.Select(p => p).OfType<Vw_BoxOrder>().ToList();



                var oDeliveryScheduleToDisplay = oOrderObjects.Where(p => p.SchQty < p.OrdQty || p.SchQty == null).Select(p => new
                {
                    Pk_OrderChild = p.Pk_OrderChild,
                    EnqQty = p.EnqQty,
                    OrdQty = p.OrdQty,
                    BoxID = p.Pk_BoxID,
                    PartNo = p.PartNo,
                    BName = p.Name,
                    OrdNo = p.Pk_Order,
                    Enq = p.Fk_Enquiry,
                    //EnqChild=p.Pk_EnquiryChild,
                    PName = p.PName,
                    PartID = p.Pk_PartPropertyID,
                    PQuantity = p.Quantity,
                    Status = p.Fk_Status,
                    CustomerName = p.CustomerName,
                    OrderDate = DateTime.Parse(p.OrderDate.ToString()).ToString("dd/MM/yyyy"),

                }).ToList();

                return Json(new { Result = "OK", Records = oDeliveryScheduleToDisplay, TotalRecordCount = oDeliveryScheduleToDisplay.Count() });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


        [HttpPost]
        public JsonResult OrderForBills(string Fk_OrderID = "", string BName = "", string PName = "", string CustomerName = "")
        {
            try
            {
                //Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                //string Fk_OrderID = oValues["Fk_OrderID"];

                List<SearchParameter> oSearchParams = new List<SearchParameter>();


                oSearchParams.Add(new SearchParameter("Fk_OrderID", Fk_OrderID));
                oSearchParams.Add(new SearchParameter("BName", BName));
                oSearchParams.Add(new SearchParameter("PName", PName));
                oSearchParams.Add(new SearchParameter("CustomerName", CustomerName));
                SearchResult oSearchResult = oOB.SearchOrdDet(oSearchParams);
                List<EntityObject> oOrdersToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_BoxOrder> oOrderObjects = oOrdersToDisplayObjects.Select(p => p).OfType<Vw_BoxOrder>().ToList();



                var oDeliveryScheduleToDisplay = oOrderObjects.Where(p => p.Fk_Status != 4).Select(p => new
                {
                    Pk_OrderChild = p.Pk_OrderChild,
                    EnqQty = p.EnqQty,
                    OrdQty = p.OrdQty,
                    BoxID = p.Pk_BoxID,
                    PartNo = p.PartNo,
                    BName = p.Name,
                    OrdNo = p.Pk_Order,
                    Enq = p.Fk_Enquiry,
                    //EnqChild=p.Pk_EnquiryChild,
                    PName = p.PName,
                    PartID = p.Pk_PartPropertyID,
                    PQuantity = p.Quantity,
                    Status = p.Fk_Status,
                    Fk_Customer = p.Fk_Customer,
                    CustomerName = p.CustomerName,
                    OrderDate = DateTime.Parse(p.OrderDate.ToString()).ToString("dd/MM/yyyy"),

                }).ToList();

                return Json(new { Result = "OK", Records = oDeliveryScheduleToDisplay, TotalRecordCount = oDeliveryScheduleToDisplay.Count() });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }



        [HttpPost]
        public JsonResult OrderPaperDetails(string Fk_BoxID = "", string PartId = "", string ProductName = "", string FromOrderDate = "", string ToOrderDate = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();


                oSearchParams.Add(new SearchParameter("Fk_BoxID", Fk_BoxID));
                oSearchParams.Add(new SearchParameter("PartId", PartId));

                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oOB.SearchOBox(oSearchParams);

                List<EntityObject> oOrdersToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_PaperWtBox> oOrderObjects = oOrdersToDisplayObjects.Select(p => p).OfType<Vw_PaperWtBox>().ToList();

                //Create a anominious object here to break the circular reference
                var oOrdersToDisplay = oOrderObjects.Select(p => new
                {
                    Pk_Material = p.Pk_Material,
                    //Pk_LayerId = p.Pk_LayerID,
                    MName = p.Name,
                    PaperWt = p.Expr1,        /// paper wt in a box
                    StkQty = p.stkqty,

                    PName = p.PName,
                    PQty = p.Quantity,        // no. of pieces of a part in a box
                    PWt = (p.Expr1 * p.Quantity),


                }).ToList();

                return Json(new { Result = "OK", Records = oOrdersToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }



        [HttpPost]
        public JsonResult GetPaperDetails(string Fk_BoxID = "", string PartId = "", string ProductName = "", string FromOrderDate = "", string ToOrderDate = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();


                oSearchParams.Add(new SearchParameter("Fk_BoxID", Fk_BoxID));
                oSearchParams.Add(new SearchParameter("PartId", PartId));

                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oOB.SearchOBoxWt(oSearchParams);

                List<EntityObject> oOrdersToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_GetPaperWtSum> oOrderObjects = oOrdersToDisplayObjects.Select(p => p).OfType<Vw_GetPaperWtSum>().ToList();

                //Create a anominious object here to break the circular reference
                var oOrdersToDisplay = oOrderObjects.Select(p => new
                {
                    Pk_Material = p.Pk_Material,
                    //Pk_LayerId = p.Pk_LayerID,
                    MName = p.Name,
                    PaperWt = p.SWt,        /// paper wt in a box
                    // StkQty = p.stkqty,

                    //PName = p.PName,
                    //PQty = p.Quantity,        // no. of pieces of a part in a box
                    //PWt = (p.Expr1 * p.Quantity),


                }).ToList();

                return Json(new { Result = "OK", Records = oOrdersToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


        [HttpPost]

        public JsonResult SaveNew([System.Web.Http.FromBody]string data)
        {
            var DataRecd = "";
            try
            {
                data = data.TrimStart('=');
                DataRecd = data.Length.ToString();
                Dictionary<string, object> values = JsonConvert.DeserializeObject<Dictionary<string, object>>(data);
                data = JsonConvert.SerializeObject(values);
                return base.Save(data);

            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message + "INCOMING DATA IS " + DataRecd });
            }
        }

        public ActionResult StatusUpdate(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["POno"]);

                // List<Vw_POReport> BillList = new List<Vw_POReport>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                Gen_OrderChild oGen_Child = _oEntities.Gen_OrderChild.Where(p => p.Fk_OrderID == sInvno).Single();
                //  PurchaseOrderM oPurM = _oEntities.PurchaseOrderM.Where(p => p.Pk_PONo == sInvno).Single();
                //Purchase_Order oPOrder = new Purchase_Order();

                oGen_Child.Fk_Status = 4;
                _oEntities.SaveChanges();

                return Json(new { success = true });
            }
            catch (Exception ex)
            {
                return null;

            }
        }

        public ActionResult OpenStatus(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["POno"]);

                // List<Vw_POReport> BillList = new List<Vw_POReport>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();


                Gen_OrderChild oGen_Child = _oEntities.Gen_OrderChild.Where(p => p.Fk_OrderID == sInvno).Single();
                //Purchase_Order oPOrder = new Purchase_Order();

                oGen_Child.Fk_Status = 1;
                _oEntities.SaveChanges();

                return Json(new { success = true });
            }
            catch (Exception ex)
            {
                return null;

            }
        }


        //[HttpPost]

        //public  JsonResult Save([System.Web.Http.FromBody] SaveRequest saveRequest)
        //{
        //    var DataRecd = "";
        //    var data = saveRequest.data;

        //    try
        //    {
        //        DataRecd = data.Length.ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { Result = "ERROR", Message = ex.Message + "INCOMING DATA IS " + DataRecd });
        //    }


        //    Dictionary<string, object> values = JsonConvert.DeserializeObject<Dictionary<string, object>>(data);


        //    data = JsonConvert.SerializeObject(values);
        //    //this.CurrentObject = null;
        //    return base.Save(data);


        //}


        /// <summary>
        /// saveRequest:
        /// {
        ///     "data": "blahblah"
        /// }
        /// </summary>
        public class SaveRequest
        {
            public string data { get; set; }
        }

    }
}

