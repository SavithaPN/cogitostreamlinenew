﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;

//namespace CogitoStreamline.Controllers
//{
//    public class PlateController : Controller
//    {
//        //
//        // GET: /Plate/

//        public ActionResult Index()
//        {
//            return View();
//        }

//    }
//}


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;

namespace CogitoStreamline.Controllers
{
    public class PlateController : CommonController
    {
        //
        // GET: /Mill/
        Material oMaterial = new Material();

        //Mill oMill = new Mill();
        //PaperType oPaper = new PaperType();
        Mat_Category oMCategory = new Mat_Category();
        //Color oColor = new Color();
        //Vendor oVendor = new Vendor();
        //Unit oUnit = new Unit();


        public PlateController()
        {
            oDoaminObject = new Material();
        }

        protected override void Dispose(bool disposing)
        {
            //oMill.Dispose();
            //oPaper.Dispose();
            oMCategory.Dispose();
            //oColor.Dispose();
            //oVendor.Dispose();
            //oUnit.Dispose();
            base.Dispose(disposing);
        }

        public override ActionResult Index()
        {
            ViewBag.Header = "Plate";
            return base.Index();
        }


        //[HttpPost]
        //public JsonResult getMill(string pId = "")
        //{
        //    List<gen_Mill> oListOfMill = oMill.Search(null).ListOfRecords.OfType<gen_Mill>().ToList();
        //    var oMillToDisplay = oListOfMill.Select(p => new
        //    {
        //        Name = p.MillName,
        //        Id = p.Pk_Mill
        //    });

        //    return Json(oMillToDisplay);
        //}

        //[HttpPost]
        //public JsonResult getUnit(string pId = "")
        //{
        //    List<gen_Unit> oListOfUnit = oUnit.Search(null).ListOfRecords.OfType<gen_Unit>().ToList();
        //    var oUnitToDisplay = oListOfUnit.Select(p => new
        //    {
        //        Name = p.UnitName,
        //        Id = p.Pk_Unit
        //    });

        //    return Json(oUnitToDisplay);
        //}


        //[HttpPost]
        //public JsonResult getColor(string pId = "")
        //{
        //    List<gen_Color> oListOfColor = oColor.Search(null).ListOfRecords.OfType<gen_Color>().ToList();
        //    var oColorToDisplay = oListOfColor.Select(p => new
        //    {
        //        Name = p.ColorName,
        //        Id = p.Pk_Color
        //    });

        //    return Json(oColorToDisplay);
        //}


        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                Material oMaterial = oDoaminObject as Material;
                oMaterial.ID = decimal.Parse(pId);
                Inv_Material oAtualObject = oMaterial.DAO as Inv_Material;
                //Create a anominious object here to break the circular reference

                var oMaterialToDisplay = new
                {

                    Pk_Material = oAtualObject.Pk_Material,
                    Name = oAtualObject.Name,
                    //Fk_Mill = oAtualObject.Fk_Mill,
                    Length = oAtualObject.Length,
                    Width = oAtualObject.Width,
                    //Height = oAtualObject.Height,
                    GSM = oAtualObject.GSM,
                    // GSM2 = oAtualObject.GSM2,
                    //  GSM3 = oAtualObject.GSM3,
                    BF = oAtualObject.BF,
                    PPly = oAtualObject.PPly,
                    // Fply = oAtualObject.FPly,
                    //  Fk_Color = oAtualObject.Fk_Color,
                    Weight = oAtualObject.Weight,
                    // Fk_UnitId = oAtualObject.Fk_UnitId,
                    PartNo = oAtualObject.PartNo,

                };

                return Json(new { success = true, data = oMaterialToDisplay });
            }
            else
            {
                var oMaterialToDisplay = new Inv_Material();
                return Json(new { success = true, data = oMaterialToDisplay });
            }
        }


        [HttpPost]
        public JsonResult PlateDuplicateChecking(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sName = oValues["Name"];

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Name", sName));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oBoardToDisplayObjects = oSearchResult.ListOfRecords;
                List<Inv_Material> oBoardObjects = oBoardToDisplayObjects.Select(p => p).OfType<Inv_Material>().ToList();

                var oBoardToDisplay = oBoardObjects.Select(p => new
                {
                    Pk_Material = p.Pk_Material,
                    Name = p.Name
                }).ToList().OrderBy(s => s.Name);

                return Json(new { Success = true, Records = oBoardToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }


        [HttpPost]
        public JsonResult PlateListByFiter(string Length = "", string Name = "", string Width = "", string GSM = "", string BF = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Name", Name));
                oSearchParams.Add(new SearchParameter("Length", Length));
                oSearchParams.Add(new SearchParameter("Width", Width));
                oSearchParams.Add(new SearchParameter("GSM", GSM));
                oSearchParams.Add(new SearchParameter("BF", BF));

                oSearchParams.Add(new SearchParameter("Category", "Plate"));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oMaterialToDisplayObjects = oSearchResult.ListOfRecords;
                List<Inv_Material> oMaterialObjects = oMaterialToDisplayObjects.Select(p => p).OfType<Inv_Material>().ToList();

                //Create a anominious object here to break the circular reference
                var oMaterialToDisplay = oMaterialObjects.Select(p => new
                {
                    Pk_Material = p.Pk_Material,
                    Name = p.Name,
                    Length = p.Length,
                    Width = p.Width,
                    GSM = p.GSM,
                    BF = p.BF,
                    Weight = p.Weight,
                 
                    PPly = p.PPly


                }).ToList().OrderBy(s => s.Name);

                return Json(new { Result = "OK", Records = oMaterialToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

    }
}
