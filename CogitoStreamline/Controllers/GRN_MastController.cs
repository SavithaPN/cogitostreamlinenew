﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;
using System.IO;
using System.Data;
using CrystalDecisions.ReportAppServer.ReportDefModel;
using System.Drawing.Printing;
using CrystalDecisions.Shared;

namespace CogitoStreamline.Controllers
{
    public class GRN_MastController : CommonController
    {
        //
        // GET: /MaterialIndent/
        //  Customer oCustomer = new Customer();
        WebGareCore.DomainModel.Tenants oTanent = new WebGareCore.DomainModel.Tenants();
        //   Material oMaterial = new Material();
        GRN oInwd = new GRN();
        InwardMaterial oMaterial = new InwardMaterial();
        public GRN_MastController()
        {
            oDoaminObject = new GRN();
        }

        public override ActionResult Index()
        {
            ViewBag.Header = "GRN Material";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }





        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                GRN oMaterialInward = oDoaminObject as GRN;
                oMaterialInward.ID = decimal.Parse(pId);
                GRN_Mast oAtualObject = oMaterialInward.DAO as GRN_Mast;

                int i = -1;

                var oMaterials = oAtualObject.GRN_Details.Select(p => new
                {
                    slno = ++i,
                    TotQty = p.TotQty,
                    TaxValue = p.TaxValue,
                    Fk_Material = p.Fk_Material,
                    Name = p.Inv_Material.Name,
                    Pk_GRNDetId = p.Pk_GRNDetId,
                    Rate = p.Rate,
                    TaxAmt = p.TaxAmt,
                    HSNCode = p.HSNCode,
                    TotAmt = p.TotAmt,



                });
                //Create a anominious object here to break the circular reference
                var oMaterialInwardToDisplay = new
                {
                    Pk_GRNId = oAtualObject.Pk_GRNId,
                    GRNDate = DateTime.Parse(oAtualObject.GRNDate.ToString()).ToString("dd/MM/yyyy"),
            
                    Fk_Inward = oAtualObject.Fk_Inward,
                   
                    MaterialData = Json(oMaterials).Data
                };

                return Json(new { success = true, data = oMaterialInwardToDisplay });
            }
            else
            {
                var oMaterialInwardToDisplay = new GRN_Mast();
                return Json(new { success = true, data = oMaterialInwardToDisplay });
            }
        }

        [HttpPost]
        public JsonResult InwdGetRec(string InwardNo = "", string Vendor = "", string GSM = "", string BF = "", string Dec = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {


            try
            {

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("InwardNo", InwardNo));
                oSearchParams.Add(new SearchParameter("GSM", GSM));
                oSearchParams.Add(new SearchParameter("BF", BF));
                oSearchParams.Add(new SearchParameter("Dec", Dec));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oMaterial.SearchInwardDet(oSearchParams);

                List<EntityObject> oColorToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_GRNMatData> oColorObjects = oColorToDisplayObjects.Select(p => p).OfType<Vw_GRNMatData>().ToList();

                //RollNo = p.RollNo !=null ? p.RollNo : "" ,
                var oColorToDisplay = oColorObjects.Select(p => new
                {
                    Pk_Material = p.Fk_Material,
                    MaterialName = p.Name,
                    Quantity = p.TQty,
                    //Color = p.ColorName != null ? p.ColorName : "",
                    //Mill = p.MillName != null ? p.MillName : "",
                    //Fk_IndentVal = p.Fk_IndentVal,
                    //Pk_Mill = p.Pk_Mill != null ? p.Pk_Mill : 0,
                    //CatName = p.CatName,
                    //UnitName = p.UnitName,
                    //Pk_PoDet = p.Pk_PODet,
                }).ToList();

                return Json(new { Result = "OK", Records = oColorToDisplay, TotalRecordCount = oSearchResult.RecordCount });

            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }


        }
    
        [HttpPost]
        public JsonResult MaterialInwardListByFiter(string Pk_GRNId = null, string FromDate = "", string ToDate = "", string Vendor = "", string Material = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_GRNId", Pk_GRNId));
                oSearchParams.Add(new SearchParameter("FromDate", FromDate));
                oSearchParams.Add(new SearchParameter("ToDate", ToDate));
                oSearchParams.Add(new SearchParameter("Vendor", Vendor));
                oSearchParams.Add(new SearchParameter("Material", Material));


                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oMaterialInwardsToDisplayObjects = oSearchResult.ListOfRecords;
                List<GRN_Mast> oMaterialInwardObjects = oMaterialInwardsToDisplayObjects.Select(p => p).OfType<GRN_Mast>().ToList();

                //Create a anominious object here to break the circular reference
                var oMaterialInwardsToDisplay = oMaterialInwardObjects.Select(p => new
                {

                    Pk_GRNId = p.Pk_GRNId,
                    GRNDate = p.GRNDate != null ? DateTime.Parse(p.GRNDate.ToString()).ToString("dd/MM/yyyy") : "",
                    //Fk_Indent = p.Inv_MaterialIndentMaster.Pk_MaterialOrderMasterId,
                    //VendorName = p.PurchaseOrderM.Fk_Vendor != null ? p.PurchaseOrderM.gen_Vendor.VendorName : "",
                    //PONo = p.PONo,
                    //PkDisp = p.PurchaseOrderM.PkDisp,
                    //IndentNo = p.Fk_Indent,
                    //Pur_InvDate = p.Pur_InvDate != null ? DateTime.Parse(p.Pur_InvDate.ToString()).ToString("dd/MM/yyyy") : "",
                    //Pur_InvNo = p.Pur_InvNo,

                    //Fk_InwardBy = p.ideUser.FirstName,


                }).ToList();


                return Json(new { Result = "OK", Records = oMaterialInwardsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


        public JsonResult InwardGRNdet(string Pk_Inward = "", string MaterialName = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {

            try
            {
                //  QualityChild oIssueDetails = new QualityChild();
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_Inward", Pk_Inward));
                oSearchParams.Add(new SearchParameter("MaterialName", MaterialName));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oInwd.SearchInwdGRNDet(oSearchParams);

                List<EntityObject> oIssueDetailsToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_GrnInwdDet> oIssueObjects = oIssueDetailsToDisplayObjects.Select(p => p).OfType<Vw_GrnInwdDet>().ToList();

                //Create a anominious object here to break the circular reference
                var oIssueDetailsToDisplay = oIssueObjects.Select(p => new
                {
                    Pk_GRNId = p.Pk_GRNId,
                    PkInwd = p.Pk_Inward,
                    GRNDate = p.GRNDate != null ? DateTime.Parse(p.GRNDate.ToString()).ToString("dd/MM/yyyy") : "",
                    //InwardDet = p.Pk_InwardDet,
                    //Name = p.Name,
                    //Quantity = p.Quantity,
                    //QC = p.Fk_QC,
                    //Indent = p.Fk_Indent,
                    //PONo = p.PONo,
                    //ReelNo = p.RollNo,
                    //Color = p.ColorName,
                    //Mill = p.MillName,
                    //Pk_Mill = p.Pk_Mill,
                }).ToList();

                return Json(new { Result = "OK", Records = oIssueDetailsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = "Select Quality Check No. and Proceed" });
            }
        }


        public JsonResult GRNdet(string Pk_GRNId = "", string MaterialName = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {

            try
            {
                //  QualityChild oIssueDetails = new QualityChild();
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_GRNId", Pk_GRNId));
                oSearchParams.Add(new SearchParameter("MaterialName", MaterialName));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oInwd.SearchGRNDet(oSearchParams);

                List<EntityObject> oIssueDetailsToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_GRNMaterialDet> oIssueObjects = oIssueDetailsToDisplayObjects.Select(p => p).OfType<Vw_GRNMaterialDet>().ToList();

                //Create a anominious object here to break the circular reference
                var oIssueDetailsToDisplay = oIssueObjects.Select(p => new
                {
                    Pk_GRNId = p.Pk_GRNId,
                    PkInwd = p.Pk_Inward,
                    GRNDate = p.GRNDate != null ? DateTime.Parse(p.GRNDate.ToString()).ToString("dd/MM/yyyy") : "",
                    Pk_GRNDetId = p.Pk_GRNDetId,
                    TotQty = p.TotQty,
                    TaxValue = p.TaxValue,
                    Rate = p.Rate,
                    TaxAmt = p.TaxAmt,
                    HSNCode = p.HSNCode,
                    TotAmt = p.TotAmt,
                    Name = p.Name,
                    //Mill = p.MillName,
                    //Pk_Mill = p.Pk_Mill,
                }).ToList();

                return Json(new { Result = "OK", Records = oIssueDetailsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = "Select Quality Check No. and Proceed" });
            }
        }


        //public ActionResult InwdRep(string data = "")
        //{
        //    try
        //    {
        //        Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        //        int sInvno = Convert.ToInt32(oValues["Pk_Inw"]);

        //        List<Vw_InwardRep> BillList = new List<Vw_InwardRep>();
        //        CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

        //        //BillList = dc.VwInvoices.ToList();
        //        BillList = dc.Vw_InwardRep.Where(x => x.Pk_GRNId == sInvno).Select(x => x).OfType<Vw_InwardRep>().ToList();


        //        //SELECT     dbo.GRN_Mast.Pk_GRNId, dbo.Inv_Material.Pk_Material, dbo.Inv_Material.Name, dbo.GRN_Details.TotQty,
        //        //dbo.GRN_Mast.GRNDate, dbo.GRN_Mast.Fk_QC, 
        //        //                      dbo.GRN_Mast.Fk_Indent, dbo.GRN_Mast.PONo, dbo.GRN_Details.RollNo
        //        //FROM         dbo.GRN_Mast INNER JOIN
        //        //                      dbo.GRN_Details ON dbo.GRN_Mast.Pk_GRNId = dbo.GRN_Details.Fk_Inward INNER JOIN
        //        //                      dbo.Inv_Material ON dbo.GRN_Details.Fk_Material = dbo.Inv_Material.Pk_Material


        //        if (BillList.Count > 0)
        //        {
        //            DataTable dt = new DataTable();

        //            dt.Columns.Add("Name");
        //            dt.Columns.Add("Pk_Material");
        //            dt.Columns.Add("Pk_GRNId");
        //            dt.Columns.Add("Fk_QC");
        //            dt.Columns.Add("GRNDate");
        //            dt.Columns.Add("TotQty");
        //            dt.Columns.Add("Fk_Indent");
        //            dt.Columns.Add("PONo");
        //            dt.Columns.Add("RollNo");
        //            dt.Columns.Add("ColorName");
        //            dt.Columns.Add("MillName");
        //            foreach (Vw_InwardRep entity in BillList)
        //            {
        //                DataRow row = dt.NewRow();

        //                row["Name"] = entity.Name;
        //                row["Pk_Material"] = entity.Pk_Material;
        //                row["Pk_GRNId"] = entity.Pk_GRNId;
        //                row["Fk_QC"] = entity.Fk_QC;
        //                row["GRNDate"] = entity.GRNDate;
        //                row["TotQty"] = entity.TotQty;
        //                row["Fk_Indent"] = entity.Fk_Indent;
        //                row["PONo"] = entity.PONo;
        //                row["RollNo"] = entity.RollNo;
        //                row["ColorName"] = entity.ColorName;
        //                row["MillName"] = entity.MillName;

        //                dt.Rows.Add(row);
        //            }


        //            DataSet ds = new DataSet();
        //            ds.Tables.Add(dt);
        //            CogitoStreamline.Report.Inwd_Consumables orp = new CogitoStreamline.Report.Inwd_Consumables();

        //            orp.Load("@\\Report\\Inwd_Consumables.rpt");
        //            orp.SetDataSource(dt.DefaultView);

        //            string pdfPath = Server.MapPath("~/ConvertPDF/" + "Inwd_Consumables" + sInvno + ".pdf");
        //            FileInfo file = new FileInfo(pdfPath);
        //            if (file.Exists)
        //            {
        //                file.Delete();
        //            }
        //            var pd = new PrintDocument();


        //            orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
        //            orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
        //            DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
        //            objDiskOpt.DiskFileName = pdfPath;

        //            orp.ExportOptions.DestinationOptions = objDiskOpt;
        //            orp.Export();

        //        }
        //        return null;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;

        //    }
        //}

        //public ActionResult InwdRepConsumables(string data = "")
        //{
        //    try
        //    {
        //        Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        //        int sInvno = Convert.ToInt32(oValues["Pk_Inw"]);

        //        List<Vw_InwardRep> BillList = new List<Vw_InwardRep>();
        //        CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

        //        //BillList = dc.VwInvoices.ToList();
        //        BillList = dc.Vw_InwardRep.Where(x => x.Pk_GRNId == sInvno).Select(x => x).OfType<Vw_InwardRep>().ToList();


        //        //SELECT     dbo.GRN_Mast.Pk_GRNId, dbo.Inv_Material.Pk_Material, dbo.Inv_Material.Name, dbo.GRN_Details.TotQty,
        //        //dbo.GRN_Mast.GRNDate, dbo.GRN_Mast.Fk_QC, 
        //        //                      dbo.GRN_Mast.Fk_Indent, dbo.GRN_Mast.PONo, dbo.GRN_Details.RollNo
        //        //FROM         dbo.GRN_Mast INNER JOIN
        //        //                      dbo.GRN_Details ON dbo.GRN_Mast.Pk_GRNId = dbo.GRN_Details.Fk_Inward INNER JOIN
        //        //                      dbo.Inv_Material ON dbo.GRN_Details.Fk_Material = dbo.Inv_Material.Pk_Material


        //        if (BillList.Count > 0)
        //        {
        //            DataTable dt = new DataTable();

        //            dt.Columns.Add("Name");
        //            dt.Columns.Add("Pk_Material");
        //            dt.Columns.Add("Pk_GRNId");
        //            dt.Columns.Add("Fk_QC");
        //            dt.Columns.Add("GRNDate");
        //            dt.Columns.Add("TotQty");
        //            dt.Columns.Add("Fk_Indent");
        //            dt.Columns.Add("PONo");
        //            dt.Columns.Add("RollNo");
        //            dt.Columns.Add("ColorName");
        //            dt.Columns.Add("MillName");
        //            foreach (Vw_InwardRep entity in BillList)
        //            {
        //                DataRow row = dt.NewRow();

        //                row["Name"] = entity.Name;
        //                row["Pk_Material"] = entity.Pk_Material;
        //                row["Pk_GRNId"] = entity.Pk_GRNId;
        //                row["Fk_QC"] = entity.Fk_QC;
        //                row["GRNDate"] = entity.GRNDate;
        //                row["TotQty"] = entity.TotQty;
        //                row["Fk_Indent"] = entity.Fk_Indent;
        //                row["PONo"] = entity.PONo;
        //                row["RollNo"] = entity.RollNo;
        //                row["ColorName"] = entity.ColorName;
        //                row["MillName"] = entity.MillName;

        //                dt.Rows.Add(row);
        //            }


        //            DataSet ds = new DataSet();
        //            ds.Tables.Add(dt);
        //            CogitoStreamline.Report.Inwd_Consumables orp = new CogitoStreamline.Report.Inwd_Consumables();

        //            orp.Load("@\\Report\\Inwd_Consumables.rpt");
        //            orp.SetDataSource(dt.DefaultView);

        //            string pdfPath = Server.MapPath("~/ConvertPDF/" + "Inward_Consumables" + sInvno + ".pdf");
        //            FileInfo file = new FileInfo(pdfPath);
        //            if (file.Exists)
        //            {
        //                file.Delete();
        //            }
        //            var pd = new PrintDocument();


        //            orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
        //            orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
        //            DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
        //            objDiskOpt.DiskFileName = pdfPath;

        //            orp.ExportOptions.DestinationOptions = objDiskOpt;
        //            orp.Export();

        //        }
        //        return null;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;

        //    }
        //}


        //public JsonResult InwardGetRec(string Pk_GRNId = "", string MaterialName = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        //{

        //    try
        //    {
        //        //  QualityChild oIssueDetails = new QualityChild();
        //        List<SearchParameter> oSearchParams = new List<SearchParameter>();
        //        oSearchParams.Add(new SearchParameter("Pk_GRNId", Pk_GRNId));
        //        oSearchParams.Add(new SearchParameter("MaterialName", MaterialName));
        //        oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
        //        oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

        //        SearchResult oSearchResult = oInwd.SearchInwdDet(oSearchParams);

        //        List<EntityObject> oIssueDetailsToDisplayObjects = oSearchResult.ListOfRecords;
        //        List<Vw_InwardRep> oIssueObjects = oIssueDetailsToDisplayObjects.Select(p => p).OfType<Vw_InwardRep>().ToList();

        //        //Create a anominious object here to break the circular reference
        //        var oIssueDetailsToDisplay = oIssueObjects.Select(p => new
        //        {
        //            PkMaterial = p.Pk_Material,
        //            PkInwd = p.Pk_GRNId,
        //            InwDate = p.GRNDate != null ? DateTime.Parse(p.GRNDate.ToString()).ToString("dd/MM/yyyy") : "",
        //            InwardDet = p.Pk_GRNDetId,
        //            Name = p.Name,
        //            TotQty = p.TotQty,
        //            QC = p.Fk_QC,
        //            Indent = p.Fk_Indent,
        //            PONo = p.PONo,
        //            ReelNo = p.RollNo,
        //            Color = p.ColorName,
        //            Mill = p.MillName,
        //            Pk_Mill = p.Pk_Mill,
        //        }).ToList();

        //        return Json(new { Result = "OK", Records = oIssueDetailsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { Result = "ERROR", Message = "Select Quality Check No. and Proceed" });
        //    }
        //}



        [HttpPost]

        public JsonResult SaveNew([System.Web.Http.FromBody]string data)
        {
            data = data.TrimStart('=');
            Dictionary<string, object> values = JsonConvert.DeserializeObject<Dictionary<string, object>>(data);
            data = JsonConvert.SerializeObject(values);
            return base.Save(data);


        }
    }
}
