﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;

using System.IO;

namespace CogitoStreamline.Controllers
{

    public class DelScheduleController : CommonController
    {

        Cgen_DeliverySchedule oOB = new Cgen_DeliverySchedule();
        Cgen_DeliverySchedule oGSCh = new Cgen_DeliverySchedule();

        public DelScheduleController()
        {
            oDoaminObject = new Cgen_DeliverySchedule();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Production Schedule";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            //oDel.Dispose();
            base.Dispose(disposing);
        }

      

        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                Cgen_DeliverySchedule oDel = oDoaminObject as Cgen_DeliverySchedule;
                oDel.ID = decimal.Parse(pId);
                gen_DeliverySchedule oAtualObject = oDel.DAO as gen_DeliverySchedule;
                //Create a anominious object here to break the circular reference

                var oDelivery = new
                {

                    Pk_DeliverySchedule = oAtualObject.Pk_DeliverySechedule,
                    DeliveryDate = DateTime.Parse(oAtualObject.DeliveryDate.ToString()).ToString("dd/MM/yyyy"),
                    Quantity = oAtualObject.Quantity,
                    DeliveryCompleted = oAtualObject.DeliveryCompleted,
                    DeliveredDate = oAtualObject.DeliveredDate != null ? DateTime.Parse(oAtualObject.DeliveredDate.ToString()).ToString("dd/MM/yyyy") : null,
                    Fk_Box = oAtualObject.Fk_BoxID,
                    Name = oAtualObject.BoxMaster.Name,
                    OrdQty = oAtualObject.OrdQty,
                    Fk_Enquiry = oAtualObject.gen_Order.Fk_Enquiry,
                    Fk_Order = oAtualObject.Fk_Order,
                    dtPOrderDate = DateTime.Parse(oAtualObject.gen_Order.OrderDate.ToString()).ToString("dd/MM/yyyy"),
              
           
                


                };
                return Json(new { success = true, data = oDelivery });
            }
            else
            {
                var oDelToDisplay = new gen_DeliverySchedule();
                return Json(new { success = true, data = oDelToDisplay });
            }
        }

        [HttpPost]
        public JsonResult WIPStkList(string Pk_StkID = "", string Fk_Enquiry = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_StkID", Pk_StkID));
                //oSearchParams.Add(new SearchParameter("Fk_Enquiry", Fk_Enquiry));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oOB.SearchStk(oSearchParams);

                List<EntityObject> oPapersToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_WIP_Stock> oPaperObjects = oPapersToDisplayObjects.Select(p => p).OfType<Vw_WIP_Stock>().ToList();

                var oPapersToDisplay = oPaperObjects.Select(p => new
                {
                    Pk_StkID=p.Pk_StkID,
                    Stock=p.Stock,
                    Name=p.Name,
                    Description=p.Description,
                    PName=p.BoxPartName,
                    Pk_JobCardID = p.Pk_JobCardID,
                    PartName = p.CatName
                }).ToList();

                return Json(new { Result = "OK", Records = oPapersToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }



        [HttpPost]
        public JsonResult ScheduleList(string Fk_BoxID = "", string Fk_Enquiry = "", string SchNo = "", string Pk_Order = "", string BoxName = "", string PONo = "", string CustomerName = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Fk_BoxID", Fk_BoxID));
                oSearchParams.Add(new SearchParameter("Pk_Order", Pk_Order));
                oSearchParams.Add(new SearchParameter("BoxName", BoxName));
                oSearchParams.Add(new SearchParameter("CustomerName", CustomerName));
                oSearchParams.Add(new SearchParameter("PONo", PONo));
                oSearchParams.Add(new SearchParameter("SchNo", SchNo));
                oSearchParams.Add(new SearchParameter("Fk_Enquiry", Fk_Enquiry));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oOB.SearchSch(oSearchParams);

                List<EntityObject> oPapersToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_Schedules> oPaperObjects = oPapersToDisplayObjects.Select(p => p).OfType<Vw_Schedules>().ToList();

                var oPapersToDisplay = oPaperObjects.Select(p => new
                {

                    DeliveryDate = DateTime.Parse(p.DeliveryDate.ToString()).ToString("dd/MM/yyyy"),
                    Pk_BoxID = p.Pk_BoxID,
                    BoxName=p.Name,
                    OrdQty=p.OrdQty,
                    OrderNo=p.Pk_Order,
                    OrderDate = DateTime.Parse(p.OrderDate.ToString()).ToString("dd/MM/yyyy"),          
                    CustomerName=p.CustomerName,
                    Pk_DelID=p.Pk_DeliverySechedule,
                    CustPO=p.Cust_PO,
                    EnquiryNo=p.Fk_Enquiry,
                    Quantity = p.SchQty,
                    PName=p.PName,
                    PartID=p.Pk_PartPropertyID,
                }).ToList();

                return Json(new { Result = "OK", Records = oPapersToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


        [HttpPost]
        public JsonResult IssueScheduleList(string JCNO = "", string Fk_BoxID = "", string Fk_Enquiry = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Fk_BoxID", Fk_BoxID));
                oSearchParams.Add(new SearchParameter("Fk_Enquiry", Fk_Enquiry));
                oSearchParams.Add(new SearchParameter("PK_JobCardID", JCNO));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oOB.SearchSchIssue(oSearchParams);

                List<EntityObject> oPapersToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_IssueScheduleList> oPaperObjects = oPapersToDisplayObjects.Select(p => p).OfType<Vw_IssueScheduleList>().ToList();

                var oPapersToDisplay = oPaperObjects.Select(p => new
                {

                    DeliveryDate = DateTime.Parse(p.DeliveryDate.ToString()).ToString("dd/MM/yyyy"),
                    Pk_BoxID = p.Pk_BoxID,
                    BoxName = p.Name,
                    OrdQty = p.OrdQty,
                    OrderNo = p.Pk_Order,
                    OrderDate = DateTime.Parse(p.OrderDate.ToString()).ToString("dd/MM/yyyy"),
                    CustomerName = p.CustomerName,
                    Pk_DelID = p.Pk_DeliverySechedule,
                    CustPO = p.Cust_PO,
                    EnquiryNo = p.Fk_Enquiry,
                    Quantity = p.SchQty,
                    PName = p.PName,
                    PartID = p.Pk_PartPropertyID,
                    Pk_JobCardID=p.Pk_JobCardID,
                    Product=p.Product,
                    WorderNo=p.WorkOrder_ID,
                }).ToList();

                return Json(new { Result = "OK", Records = oPapersToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult PartsScheduleList(string Fk_BoxID = "", string Fk_Enquiry = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {

              
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Fk_BoxID", Fk_BoxID));
                oSearchParams.Add(new SearchParameter("Fk_Enquiry", Fk_Enquiry));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oOB.SearchSchParts(oSearchParams);

                List<EntityObject> oPapersToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_PartsSchedules> oPaperObjects = oPapersToDisplayObjects.Select(p => p).OfType<Vw_PartsSchedules>().ToList();

                var oPapersToDisplay = oPaperObjects.Select(p => new
                {

                    DeliveryDate = DateTime.Parse(p.DeliveryDate.ToString()).ToString("dd/MM/yyyy"),
                    Pk_BoxID = p.Pk_BoxID,
                    BoxName = p.Name,
                    OrdQty = p.OrdQty,
                    OrderNo = p.Pk_Order,
                    OrderDate = DateTime.Parse(p.OrderDate.ToString()).ToString("dd/MM/yyyy"),
                    CustomerName = p.CustomerName,
                    Pk_DelID = p.Pk_DeliverySechedule,
                    CustPO = p.Cust_PO,
                    EnquiryNo = p.Fk_Enquiry,
                    Quantity = p.SchQty,
                    PName=p.PName,
                    PartID=p.Pk_PartPropertyID,

                }).ToList();

                return Json(new { Result = "OK", Records = oPapersToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }



        [HttpPost]
        public JsonResult DelListByFiter(string Pk_DeliverySechedule = "", string CustName = "", string Fk_Order = "", string BoxName = "", string FromDate = "", string ToDate = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_DeliverySechedule", Pk_DeliverySechedule));
                oSearchParams.Add(new SearchParameter("BoxName", BoxName));
                oSearchParams.Add(new SearchParameter("CustName", CustName));

                oSearchParams.Add(new SearchParameter("Fk_Order", Fk_Order));
                oSearchParams.Add(new SearchParameter("FromDate", FromDate));
                oSearchParams.Add(new SearchParameter("ToDate", ToDate));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oDelToDisplayObjects = oSearchResult.ListOfRecords;
                List<gen_DeliverySchedule> oDelObjects = oDelToDisplayObjects.Select(p => p).OfType<gen_DeliverySchedule>().ToList();

                //Create a anominious object here to break the circular reference
                var oDelToDisplay = oDelObjects.Select(p => new
                {
                    Pk_DeliverySechedule = p.Pk_DeliverySechedule,
                    DeliveryDate = DateTime.Parse(p.DeliveryDate.ToString()).ToString("dd/MM/yyyy"),
                    Fk_BoxID = p.Fk_BoxID,
                    BName =p.BoxMaster.Name,
                    Quantity=p.Quantity,
                    Fk_Order=p.Fk_Order,
                    CustName=p.gen_Order.gen_Customer.CustomerName,
                   DeliveredDate= p.DeliveredDate != null ? DateTime.Parse(p.DeliveredDate.ToString()).ToString("dd/MM/yyyy"):"",
                    DeliveryCompleted=p.DeliveryCompleted,
                    PartName=p.ItemPartProperty.PName,
                    PartID=p.Fk_PartID,
                    OrdQty=p.OrdQty,
                    Pending=p.OrdQty-p.Quantity,

                }).ToList();

                return Json(new { Result = "OK", Records = oDelToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult TotalScheduled(string Pk_Material = "", string OrderNo = "", string MaterialName = "", string GSM = "", string BF = "", string Mill = "", string Color = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_Material", Pk_Material));
                oSearchParams.Add(new SearchParameter("OrderNo", OrderNo));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oGSCh.SearchSchPendingQty(oSearchParams);

                List<EntityObject> oStockToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_Sch_Ord_Pending> oStockObjects = oStockToDisplayObjects.Select(p => p).OfType<Vw_Sch_Ord_Pending>().ToList();

                //Create a anominious object here to break the circular reference
                var oStockToDisplay = oStockObjects.Select(p => new
                {
                    //Name = p.Name,
                    //StkQty = p.TQty,
                   Pk_Order= p.Pk_Order,
                 OrdQty=   p.OrdQty,
                   TotPendingQty= p.TotPendingQty,
                 TotSchQty=   p.TotSchQty


                }).ToList();

                return Json(new { Result = "OK", Records = oStockToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


        [HttpPost]
        public JsonResult DelGetRec(string Pk_DeliverySechedule = "", string PartID = "", string FromDate = "", string ToDate = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                string[] Part = Pk_DeliverySechedule.Split(',');
                Pk_DeliverySechedule = Part[0];
                PartID = Part[1];
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_DeliverySechedule", Pk_DeliverySechedule));
                oSearchParams.Add(new SearchParameter("PartID", PartID));
                //oSearchParams.Add(new SearchParameter("FromDate", FromDate));
                //oSearchParams.Add(new SearchParameter("ToDate", ToDate));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oGSCh.SearchSchDet(oSearchParams);

                List<EntityObject> oDelToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_BoxScheduleDet> oDelObjects = oDelToDisplayObjects.Select(p => p).OfType<Vw_BoxScheduleDet>().ToList();

                //Create a anominious object here to break the circular reference
                var oDelToDisplay = oDelObjects.Select(p => new
                {
                    Pk_DeliverySechedule = p.Pk_DeliverySechedule,
                    //DeliveryDate = DateTime.Parse(p.DeliveryDate.ToString()).ToString("dd/MM/yyyy"),
                    //Fk_BoxID = p.Fk_BoxID,
                    Pk_PartPropertyID=p.Pk_PartPropertyID,
                    PName = p.PName,
                    GSM=p.GSM,
                    BF=p.BF,
                    Weight=p.Weight,
                    BoxName=p.Name,
                    Deckel=p.Deckle,
                    CuttingSize=p.CuttingSize,
                    LayerID = p.Pk_LayerID,
                    //Quantity = p.Quantity,
                    //Fk_Order = p.Fk_Order,
                    //CustName = p.gen_Order.gen_Customer.CustomerName,
                    //DeliveredDate = p.DeliveredDate != null ? DateTime.Parse(p.DeliveredDate.ToString()).ToString("dd/MM/yyyy") : "",
                    //DeliveryCompleted = p.DeliveryCompleted,
                    //PartName = p.ItemPartProperty.PName,

                }).ToList();

                return Json(new { Result = "OK", Records = oDelToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }



        public ActionResult DelDetails(string data = "")
        {
            try
            {
                //_comLayer.parameters.add("BoxID", rows[0].data["BoxID"]);
                //_comLayer.parameters.add("OrderNo", OrdID);OrderNo = "", string BoxID = ""
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sInvno = oValues["Id"];
             
                List<SearchParameter> oSearchParams = new List<SearchParameter>();


                oSearchParams.Add(new SearchParameter("Pk_DeliverySechedule", sInvno));
          

                SearchResult oSearchResult = oOB.SearchDet(oSearchParams);

                List<EntityObject> oOrdersToDisplayObjects = oSearchResult.ListOfRecords;
                List<gen_DeliverySchedule> oOrderObjects = oOrdersToDisplayObjects.Select(p => p).OfType<gen_DeliverySchedule>().ToList();

                //Create a anominious object here to break the circular reference
                var oOrdersToDisplay = oOrderObjects.Select(p => new
                {
                    Pk_DeliverySechedule = p.Pk_DeliverySechedule,
                    DeliveryDate = DateTime.Parse(p.DeliveryDate.ToString()).ToString("dd/MM/yyyy"),
                    Fk_BoxID = p.Fk_BoxID,
                    BName = p.BoxMaster.Name,
                    Quantity = p.Quantity,
                    Fk_Order = p.Fk_Order,
                    CustName = p.gen_Order.gen_Customer.CustomerName


                }).ToList();

                return Json(new { Result = "Success", data = oOrdersToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]

        public JsonResult SaveNew([System.Web.Http.FromBody]string data)
        {
            data = data.TrimStart('=');
            Dictionary<string, object> values = JsonConvert.DeserializeObject<Dictionary<string, object>>(data);
            data = JsonConvert.SerializeObject(values);
            return base.Save(data);


        }
    }
}
