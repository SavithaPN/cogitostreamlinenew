﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;
using System.Data;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.IO;
using System.Drawing.Printing;

namespace CogitoStreamline.Controllers
{
    public class CoaReportController : CommonController
    {
        //
        // GET: /Mill/
        Material oMaterial = new Material();


        COAParameters oColor = new COAParameters();

        Unit oUnit = new Unit();


        public CoaReportController()
        {
            oDoaminObject = new COARep();
        }

        protected override void Dispose(bool disposing)
        {

            oColor.Dispose();

            oUnit.Dispose();
            base.Dispose(disposing);
        }

        public override ActionResult Index()
        {
            ViewBag.Header = "COA Report";
            return base.Index();
        }



        [HttpPost]
        public JsonResult getUnit(string pId = "")
        {
            List<gen_Unit> oListOfUnit = oUnit.Search(null).ListOfRecords.OfType<gen_Unit>().ToList();
            var oUnitToDisplay = oListOfUnit.Select(p => new
            {
                Name = p.UnitName,
                Id = p.Pk_Unit
            });

            return Json(oUnitToDisplay);
        }


        [HttpPost]
        public JsonResult getParameter(string pId = "")
        {
            List<COA_Parameters> oListOfColor = oColor.Search(null).ListOfRecords.OfType<COA_Parameters>().ToList();
            var oColorToDisplay = oListOfColor.Select(p => new
            {
                Name = p.ParaName,
                Id = p.Pk_ParaID
            });

            return Json(oColorToDisplay);
        }

        //[HttpPost]
        //public JsonResult InkDuplicateChecking(string data = "")
        //{
        //    try
        //    {
        //        Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        //        string sName = oValues["Name"];

        //        List<SearchParameter> oSearchParams = new List<SearchParameter>();
        //        oSearchParams.Add(new SearchParameter("Name", sName));

        //        SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

        //        List<EntityObject> oInkToDisplayObjects = oSearchResult.ListOfRecords;
        //        List<Inv_Material> oInkObjects = oInkToDisplayObjects.Select(p => p).OfType<Inv_Material>().ToList();

        //        var oInkToDisplay = oInkObjects.Select(p => new
        //        {
        //            Pk_Material = p.Pk_Material,
        //            Name = p.Name
        //        }).ToList().OrderBy(s => s.Name);

        //        return Json(new { Success = true, Records = oInkToDisplay, TotalRecordCount = oSearchResult.RecordCount });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { Success = false, Message = ex.Message });
        //    }
        //}
      
        
        
        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                COARep oMaterial = oDoaminObject as COARep;
                oMaterial.ID = decimal.Parse(pId);
                COA oAtualObject = oMaterial.DAO as COA;
                //Create a anominious object here to break the circular reference

                var oMaterialToDisplay = new
                {

                    Pk_ID = oAtualObject.Pk_ID,
               
                      Fk_Invno = oAtualObject.Fk_Invno,

                    RDate = DateTime.Parse(oAtualObject.RDate.ToString()).ToString("dd/MM/yyyy"),

                };

                return Json(new { success = true, data = oMaterialToDisplay });
            }
            else
            {
                var oMaterialToDisplay = new Inv_Material();
                return Json(new { success = true, data = oMaterialToDisplay });
            }
        }

        [HttpPost]
        public JsonResult COAListByFiter(string Pk_ID = "", string Fk_Invno = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                //oSearchParams.Add(new SearchParameter("ParaName", ParaName));
                oSearchParams.Add(new SearchParameter("Fk_Invno", Fk_Invno));
                oSearchParams.Add(new SearchParameter("Pk_ID", Pk_ID));
             //   oSearchParams.Add(new SearchParameter("Category", "Ink"));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oMaterialToDisplayObjects = oSearchResult.ListOfRecords;
                List<COA> oMaterialObjects = oMaterialToDisplayObjects.Select(p => p).OfType<COA>().ToList();

                //Create a anominious object here to break the circular reference
                var oMaterialToDisplay = oMaterialObjects.Select(p => new
                {


                    Pk_ID = p.Pk_ID,
               
                    RDate = DateTime.Parse(p.RDate.ToString()).ToString("dd/MM/yyyy"),
               
                  Fk_Invno = p.Fk_Invno

               
                }).ToList().OrderBy(s => s.Pk_ID);

                return Json(new { Result = "OK", Records = oMaterialToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }



        public ActionResult COARep(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["JCNO"]);

                List<Vw_COARep> BillList = new List<Vw_COARep>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();


                BillList = dc.Vw_COARep.Where(x => x.Pk_ID == sInvno).Select(x => x).OfType<Vw_COARep>().ToList();

 

                if (BillList.Count > 0)
                {
                    DataTable dt = new DataTable();

                    dt.Columns.Add("Pk_ID");
                    dt.Columns.Add("RDate");                  
                    dt.Columns.Add("Fk_Invno");
                    dt.Columns.Add("Pk_IDDet");
                    dt.Columns.Add("UOM");
                    dt.Columns.Add("Name");
                    dt.Columns.Add("Length");
                    dt.Columns.Add("Width");
                    dt.Columns.Add("Height");
                    dt.Columns.Add("PlyVal");
                    dt.Columns.Add("Specification");
                    dt.Columns.Add("Result");
                    dt.Columns.Add("Remarks");
                    dt.Columns.Add("Pk_BoxID");
                    dt.Columns.Add("Fk_ParaID");
                    dt.Columns.Add("ParaName");
                    dt.Columns.Add("CustomerName");
                    dt.Columns.Add("Quantity");
                    
                    foreach (Vw_COARep entity in BillList)
                    {
                        DataRow row = dt.NewRow();

                        row["Pk_ID"] = entity.Pk_ID;
                        row["RDate"] = entity.RDate;
                        row["ParaName"] = entity.ParaName;
                        row["Fk_Invno"] = entity.Fk_Invno;
                        row["Pk_IDDet"] = entity.Pk_IDDet;
                        row["UOM"] = entity.UOM;
                        row["Name"] = entity.Name;
                        row["Length"] = entity.Length;
                        row["Width"] = entity.Width;
                        row["Height"] = entity.Height;
                        row["PlyVal"] = entity.PlyVal;
                        row["Specification"] = entity.Specification;
                        row["Result"] = entity.Result;
                        row["Pk_BoxID"] = entity.Pk_BoxID;
                        row["Remarks"] = entity.Remarks;
                        row["CustomerName"] = entity.CustomerName;
                        row["Quantity"] = entity.Quantity;
                        row["Fk_ParaID"] = entity.Fk_ParaID;
                        dt.Rows.Add(row);
                    }

                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.COARep orp = new CogitoStreamline.Report.COARep();

                    orp.Load("@\\Report\\COARep.rpt");
                    orp.SetDataSource(dt.DefaultView);


                    CrystalDecisions.CrystalReports.Engine.ReportDocument repDoc = orp;


                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "COARep" + sInvno + ".pdf");
                    FileInfo file = new FileInfo(pdfPath);
                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();

                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }


      

    }
}

