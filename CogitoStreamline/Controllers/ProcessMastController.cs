﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;

using System.IO;

namespace CogitoStreamline.Controllers
{

    public class ProcessMastController : CommonController
    {
        ProcessM oColor = new ProcessM();

        public ProcessMastController()
        {
            oDoaminObject = new ProcessM();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Process";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            oColor.Dispose();
            base.Dispose(disposing);
        }


        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                ProcessM oColor = oDoaminObject as ProcessM;
                oColor.ID = decimal.Parse(pId);
                ProcessMaster oAtualObject = oColor.DAO as ProcessMaster;
                //Create a anominious object here to break the circular reference

                var oColorToDisplay = new
                {

                    Pk_Process = oAtualObject.Pk_Process,
                    ProcessName = oAtualObject.ProcessName
                };

                return Json(new { success = true, data = oColorToDisplay });
            }
            else
            {
                var oColorToDisplay = new ProcessMaster();
                return Json(new { success = true, data = oColorToDisplay });
            }
        }



        [HttpPost]
        public JsonResult ProcessDuplicateChecking(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sColorName = oValues["ProcessName"];

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("ProcessName", sColorName));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oColorToDisplayObjects = oSearchResult.ListOfRecords;
                List<ProcessMaster> oColorObjects = oColorToDisplayObjects.Select(p => p).OfType<ProcessMaster>().ToList();

                var oColorToDisplay = oColorObjects.Select(p => new
                {
                    Pk_Process = p.Pk_Process,
                    ProcessName = p.ProcessName
                }).ToList().OrderBy(s => s.ProcessName);

                return Json(new { Success = true, Records = oColorToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult ProcessListByFiter(string ProcessName = "", string Pk_Process = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_Process", Pk_Process));
                oSearchParams.Add(new SearchParameter("ProcessName", ProcessName));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oColorToDisplayObjects = oSearchResult.ListOfRecords;
                List<ProcessMaster> oColorObjects = oColorToDisplayObjects.Select(p => p).OfType<ProcessMaster>().ToList();

                //Create a anominious object here to break the circular reference
                var oColorToDisplay = oColorObjects.Select(p => new
                {
                    Pk_Process = p.Pk_Process,
                    ProcessName = p.ProcessName,


                }).ToList().OrderBy(s => s.Pk_Process);

                return Json(new { Result = "OK", Records = oColorToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }



        [HttpPost]
        public JsonResult ProcessList(string Pk_JobCardID = "", string ProcessName = "", string Pk_Process = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_JobCardID", Pk_JobCardID));
                oSearchParams.Add(new SearchParameter("ProcessName", ProcessName));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oColor.SearchJobCard(oSearchParams);

                List<EntityObject> oColorToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_ProcessJC> oColorObjects = oColorToDisplayObjects.Select(p => p).OfType<Vw_ProcessJC>().ToList();

                //Create a anominious object here to break the circular reference
                var oColorToDisplay = oColorObjects.Select(p => new
                {
                    Pk_Process = p.Pk_Process,
                    ProcessName = p.ProcessName,
                    Pk_JProcessID=p.Pk_JProcessID

                }).ToList().OrderBy(s => s.Pk_JProcessID);

                return Json(new { Result = "OK", Records = oColorToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
  
    }
}
