﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;
using System.IO;
using System.Data;
using CrystalDecisions.ReportAppServer.ReportDefModel;
using System.Drawing.Printing;
using CrystalDecisions.Shared;
using WebGareCore.Controls;
using WebGareCore.CommonObjects.WorkFlow;
using CrystalDecisions.CrystalReports.Engine;

namespace CogitoStreamline.Controllers
{
    public class JCforSamplesController : CommonController
    {
        //
        // GET: /Order/
        Machine oMachine = new Machine();
        ////Product oProduct = new Product();
        JCSamples oPaperStock = new JCSamples();


        public JCforSamplesController()
        {
            oDoaminObject = new JCSamples();
        }

        public override ActionResult Index()
        {
            ViewBag.Header = "Job Card for Samples";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        [HttpPost]
        public JsonResult getMachine(string pId = "")
        {
            List<gen_Machine> oListOfCustomers = oMachine.Search(null).ListOfRecords.OfType<gen_Machine>().ToList();
            var oCustomersToDisplay = oListOfCustomers.Select(p => new
            {
                Name = p.Name,
                Id = p.Pk_Machine
            });

            return Json(oCustomersToDisplay);
        }
        [HttpPost]
        public JsonResult GetMax(string data = "")
        {

            CogitoStreamLineEntities dc = new CogitoStreamLineEntities();
            List<Sample_JobCardMaster> BillList = new List<Sample_JobCardMaster>();
            //BillList = dc.StockTransferMaster.Where(x => x.Pk_TransferId).Select(x => x).OfType<StockTransferMaster>().ToList();
            var BillCount =
                dc.Sample_JobCardMaster.Count();

            if (BillCount == 0)
            {
                var BillMax = 0;
                return Json(BillMax);
            }
            else
            {

                var BillMax =
                   (from Sample_JobCardMaster in dc.Sample_JobCardMaster

                    orderby Sample_JobCardMaster.Pk_JobCardID  descending

                    select Sample_JobCardMaster.Pk_JobCardID).Take(1);
                return Json(BillMax);
            }
        }

        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                JCSamples oJob = oDoaminObject as JCSamples;
                oJob.ID = decimal.Parse(pId);
                Sample_JobCardMaster oAtualObject = oJob.DAO as Sample_JobCardMaster;

                int i = -1;

                var oJobDet = oAtualObject.Sample_JobCardDetails.Select(p => new
                {

                    Pk_JobCardDet = p.Pk_JobCardDet,
                    slno = ++i,
                    Fk_JobCardID = p.Fk_JobCardID,
                    //p.CheckDate != null ? DateTime.Parse(p.CheckDate.ToString()).ToString("dd/MM/yyyy") : "",
                    //Fk_PaperStock = p.Fk_PaperStock,
                    Fk_PaperStock = p.Fk_PaperStock != null ? p.Fk_PaperStock : 0,
                    Fk_Material = p.Fk_Material,
                    MName = p.Inv_Material.Name,
                    
                    //Name = p.PaperStock != null ? p.PaperStock.Inv_Material.Name : "",
                    //RollNo = p.PaperStock != null ? p.PaperStock.RollNo : "",
                    Quantity = p.RM_Consumed,
                    QtyConsumed = p.QtyConsumed,


                });


                //Create a anominious object here to break the circular reference
                var oJobToDisplay = new
                {
                    Pk_JobCardID = oAtualObject.Pk_JobCardID,
                    Corrugation = oAtualObject.Corrugation,
                    Fk_Order = oAtualObject.Fk_Order,
                    //Fk_Enquiry = oAtualObject.gen_Order.Fk_Enquiry,
                    //txtfk_Customer = oAtualObject.gen_Customer.CustomerName,
                    Fk_Status = oAtualObject.Fk_Status,
                    ChkQuality = oAtualObject.ChkQuality,
                    Fk_BoxID = oAtualObject.Fk_BoxID,
                   
                    BoxID = oAtualObject.Fk_BoxID,
                    BoxType = oAtualObject.SampleBoxMaster.BoxType.Name,
                    Printing = oAtualObject.Printing,
                    PrintVal = oAtualObject.Printing,
                    DPRScVal = oAtualObject.DPRSc,
                    Calico = oAtualObject.Calico,
                    Others = oAtualObject.Others,
                    Invno = oAtualObject.Invno,
                    JDate = DateTime.Parse(oAtualObject.JDate.ToString()).ToString("dd/MM/yyyy"),
                    TopPaperQty = oAtualObject.TopPaperQty,
                    TwoPlyQty = oAtualObject.TwoPlyQty,
                    TwoPlyWt = oAtualObject.TwoPlyWt,
                    PastingQty = oAtualObject.PastingQty,
                    PastingWstQty = oAtualObject.PastingWstQty,
                    RotaryQty = oAtualObject.RotaryQty,
                    RotaryWstQty = oAtualObject.RotaryWstQty,
                    PunchingQty = oAtualObject.PunchingQty,
                    PunchingWstQty = oAtualObject.PunchingWstQty,
                    SlotingQty = oAtualObject.SlotingQty,
                    SlotingWstQty = oAtualObject.SlotingWstQty,
                    PinningQty = oAtualObject.PinningQty,
                    PinningWstQty = oAtualObject.PinningWstQty,
                    FinishingQty = oAtualObject.FinishingQty,
                    FinishingWstQty = oAtualObject.FinishingWstQty,
                    TotalQty = oAtualObject.TotalQty,
                    TotalWstQty = oAtualObject.TotalWstQty,
                    //MaterialData = Json(oJobDet).Data,
                    //Status = oAtualObject.wfStates.State,
                    Machine = oAtualObject.Machine,
                    StartTime = oAtualObject.StartTime,
                    EndTime = oAtualObject.EndTime,
                    PColor = oAtualObject.PColor,
                    PDetails = oAtualObject.PDetails,
                    MFG = oAtualObject.MFG,
                    DPRSc = oAtualObject.DPRSc,
                    ECCQty = oAtualObject.ECCQty,
                    EccWstQty = oAtualObject.EccWstQty,
                    GummingQty = oAtualObject.GummingQty,
                    GummingWstQty = oAtualObject.GummingWstQty,

                    Fk_Schedule = oAtualObject.Fk_Schedule,
                };

                return Json(new { success = true, data = oJobToDisplay });
            }
            else
            {
                var oJobToDisplay = new gen_Order();
                return Json(new { success = true, data = oJobToDisplay });
            }
        }
        [HttpPost]
        public JsonResult PaperStockList(string Fk_BoxID = "", string Fk_Enquiry = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Fk_BoxID", Fk_BoxID));
                oSearchParams.Add(new SearchParameter("Fk_Enquiry", Fk_Enquiry));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oPaperStock.SearchPaperStock(oSearchParams);

                List<EntityObject> oPapersToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_BoxPaperStock> oPaperObjects = oPapersToDisplayObjects.Select(p => p).OfType<Vw_BoxPaperStock>().ToList();

                var oPapersToDisplay = oPaperObjects.Select(p => new
                {
                    Pk_Material = p.Pk_Material,
                    Name = p.Name,
                    Fk_BoxID = p.Fk_BoxID,
                    //Quantity = p.Expr1,
                    //Fk_Enquiry = p.Fk_Enquiry

                }).ToList();

                return Json(new { Result = "OK", Records = oPapersToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult BoxPaperStock(string Fk_BoxID = "", string Fk_Enquiry = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Fk_BoxID", Fk_BoxID));
                oSearchParams.Add(new SearchParameter("Fk_Enquiry", Fk_Enquiry));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oPaperStock.SearchBoxPaper(oSearchParams);

                List<EntityObject> oPapersToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_BoxPaper> oPaperObjects = oPapersToDisplayObjects.Select(p => p).OfType<Vw_BoxPaper>().ToList();

                var oPapersToDisplay = oPaperObjects.Select(p => new
                {
                    Pk_Material = p.Pk_Material,
                    Name = p.Name,
                    //Fk_BoxID = p.Fk_BoxID,
                    Quantity = p.Quantity,
                    RollNo = p.RollNo

                }).ToList();

                return Json(new { Result = "OK", Records = oPapersToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult JCPaperStockList(string Pk_PaperStock = "", string Fk_Material = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_PaperStock", Pk_PaperStock));
                oSearchParams.Add(new SearchParameter("Fk_Material", Fk_Material));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oPaperStock.SearchJCPaperStock(oSearchParams);

                List<EntityObject> oPapersToDisplayObjects = oSearchResult.ListOfRecords;
                List<PaperStock> oPaperObjects = oPapersToDisplayObjects.Select(p => p).OfType<PaperStock>().ToList();

                var oPapersToDisplay = oPaperObjects.Select(p => new
                {
                    Pk_PaperStock = p.Pk_PaperStock,
                    Name = p.Inv_Material.Name,
                    Fk_Material = p.Fk_Material,
                    RollNo = p.RollNo,
                    Quantity = p.Quantity

                }).ToList();

                return Json(new { Result = "OK", Records = oPapersToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult JobListByFiter(string Customer = "", string BoxName = "", string JDate = "", string Fk_Order = "", string OnlyPending = "", string FromJDate = "", string ToJDate = "", string Pk_Order = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("OnlyPending", OnlyPending));
                oSearchParams.Add(new SearchParameter("BoxName", BoxName));
                oSearchParams.Add(new SearchParameter("Customer", Customer));
                oSearchParams.Add(new SearchParameter("JDate", JDate));
                oSearchParams.Add(new SearchParameter("Fk_Order", Fk_Order));
                oSearchParams.Add(new SearchParameter("FromJDate", FromJDate));
                oSearchParams.Add(new SearchParameter("ToJDate", ToJDate));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oJobsToDisplayObjects = oSearchResult.ListOfRecords;
                List<Sample_JobCardMaster> oJobObjects = oJobsToDisplayObjects.Select(p => p).OfType<Sample_JobCardMaster>().ToList();

                //Create a anominious object here to break the circular reference
                var oJobsToDisplay = oJobObjects.Select(p => new
                {
                    Pk_JobCardID = p.Pk_JobCardID,
                    JDate = DateTime.Parse(p.JDate.ToString()).ToString("dd/MM/yyyy"),
                    //Fk_Order = p.Fk_Order,
                    Customer = p.SampleBoxMaster.gen_Customer.CustomerName,
                    Invno = p.Invno,
                    TotalQty = p.TotalQty,
                    BoxName = p.SampleBoxMaster.Name
                }).ToList();

                return Json(new { Result = "OK", Records = oJobsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


        [HttpPost]
        public JsonResult JobAssignedStock(string Pk_Material = "", string BoxName = "", string JDate = "", string Fk_Order = "", string OnlyPending = "", string FromJDate = "", string ToJDate = "", string Pk_Order = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_Material", Pk_Material));
                //oSearchParams.Add(new SearchParameter("BoxName", BoxName));
                //oSearchParams.Add(new SearchParameter("Customer", Customer));
                //oSearchParams.Add(new SearchParameter("JDate", JDate));
                //oSearchParams.Add(new SearchParameter("Fk_Order", Fk_Order));
                //oSearchParams.Add(new SearchParameter("FromJDate", FromJDate));
                //oSearchParams.Add(new SearchParameter("ToJDate", ToJDate));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oPaperStock.SearchAssignedStock(oSearchParams);

                List<EntityObject> oJobsToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_PStock> oJobObjects = oJobsToDisplayObjects.Select(p => p).OfType<Vw_PStock>().ToList();

                //Create a anominious object here to break the circular reference
                var oJobsToDisplay = oJobObjects.Where(p => p.Fk_Status == 1 && p.RM_Consumed > 0).Select(p => new
                {
                    Pk_Material = p.Pk_Material,
                    Pk_JobCardID = p.Pk_JobCardID,
                    BoxName = p.BoxName,
                    JDate = DateTime.Parse(p.JDate.ToString()).ToString("dd/MM/yyyy"),
                    MName = p.Name,
                    Quantity = p.RM_Consumed,    ///Assigned Stock
                    StockQty = p.TQty,
                    Customer = p.CustomerName,
                }).ToList();

                return Json(new { Result = "OK", Records = oJobsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


        [HttpPost]
        public JsonResult InvDuplicateChecking(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sName = oValues["invno"];

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("InvNo", sName));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oPinToDisplayObjects = oSearchResult.ListOfRecords;
                List<Sample_JobCardMaster> oPinObjects = oPinToDisplayObjects.Select(p => p).OfType<Sample_JobCardMaster>().ToList();

                var oPinToDisplay = oPinObjects.Select(p => new
                {
                    Pk_JobCardID = p.Pk_JobCardID,
                    Invno = p.Invno

                }).ToList().OrderBy(s => s.Invno);

                return Json(new { Success = true, Records = oPinToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }

        //      public ActionResult JCRep(string data = "")
        //      {
        //          try
        //          {
        //              Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        //              int sInvno = Convert.ToInt32(oValues["JCNO"]);

        //              List<Vw_JCard> BillList = new List<Vw_JCard>();
        //              CogitoStreamLineEntities dc = new CogitoStreamLineEntities();


        //              BillList = dc.Vw_JCard.Where(x => x.Pk_JobCardID == sInvno).Select(x => x).OfType<Vw_JCard>().ToList();

        ////               SELECT "Sample_JobCardMaster"."Pk_JobCardID", "Sample_JobCardMaster"."JDate", "BoxMaster"."Name", "Inv_Material"."Name", 
        ////              "Sample_JobCardMaster"."Fk_Order", "Sample_JobCardMaster"."Invno", "Sample_JobCardMaster"."Corrugation", "Sample_JobCardMaster"."TopPaperQty", 
        ////              "Sample_JobCardMaster"."TwoPlyQty", "Sample_JobCardMaster"."TwoPlyWt", "Sample_JobCardMaster"."PastingQty", "Sample_JobCardMaster"."PastingWstQty",
        ////              "Sample_JobCardMaster"."RotaryQty", "Sample_JobCardMaster"."RotaryWstQty", "Sample_JobCardMaster"."PunchingQty",
        ////              "Sample_JobCardMaster"."PunchingWstQty", "Sample_JobCardMaster"."SlotingQty", "Sample_JobCardMaster"."SlotingWstQty",
        ////              "Sample_JobCardMaster"."PinningQty", "Sample_JobCardMaster"."PinningWstQty", "Sample_JobCardMaster"."FinishingQty", 
        ////              "Sample_JobCardMaster"."FinishingWstQty", "Sample_JobCardMaster"."TotalQty", "Sample_JobCardMaster"."TotalWstQty", 
        ////              "gen_Customer"."CustomerName", "gen_Machine"."Name"
        ////FROM   (((("ERP"."dbo"."Sample_JobCardDetails" "Sample_JobCardDetails" INNER JOIN "ERP"."dbo"."Sample_JobCardMaster" "Sample_JobCardMaster" 
        ////              ON "Sample_JobCardDetails"."Fk_JobCardID"="Sample_JobCardMaster"."Pk_JobCardID") INNER JOIN "ERP"."dbo"."Inv_Material"
        ////              "Inv_Material" ON "Sample_JobCardDetails"."Fk_Material"="Inv_Material"."Pk_Material") INNER JOIN "ERP"."dbo"."BoxMaster"
        ////              "BoxMaster" ON "Sample_JobCardMaster"."Fk_BoxID"="BoxMaster"."Pk_BoxID") INNER JOIN "ERP"."dbo"."gen_Machine" "gen_Machine"
        ////              ON "Sample_JobCardMaster"."Machine"="gen_Machine"."Pk_Machine") INNER JOIN "ERP"."dbo"."gen_Customer" "gen_Customer" ON 
        ////              "BoxMaster"."Customer"="gen_Customer"."Pk_Customer"
        ////ORDER BY "Inv_Material"."Name"






        //              if (BillList.Count > 0)
        //              {
        //                  DataTable dt = new DataTable();

        //                  dt.Columns.Add("Pk_JobCardID");
        //                  dt.Columns.Add("JDate");
        //                  dt.Columns.Add("BName");
        //                  dt.Columns.Add("Name");
        //                  dt.Columns.Add("Fk_Order");
        //                  dt.Columns.Add("Invno");

        //                  dt.Columns.Add("Corrugation");
        //                  dt.Columns.Add("TopPaperQty");
        //                  dt.Columns.Add("TwoPlyQty");
        //                  dt.Columns.Add("TwoPlyWt");
        //                  dt.Columns.Add("PastingQty");

        //                  dt.Columns.Add("PastingWstQty");
        //                  dt.Columns.Add("RotaryQty");
        //                  dt.Columns.Add("RotaryWstQty");
        //                  dt.Columns.Add("PunchingQty");
        //                  dt.Columns.Add("PunchingWstQty");

        //                  dt.Columns.Add("SlotingQty");
        //                  dt.Columns.Add("SlotingWstQty");
        //                  dt.Columns.Add("PinningQty");
        //                  dt.Columns.Add("PinningWstQty");
        //                  dt.Columns.Add("FinishingQty");

        //                  dt.Columns.Add("FinishingWstQty");
        //                  dt.Columns.Add("TotalQty");
        //                  dt.Columns.Add("TotalWstQty");
        //                  dt.Columns.Add("CustomerName");
        //                  //dt.Columns.Add("Expr1");



        //                  foreach (Vw_JCard entity in BillList)
        //                  {
        //                      DataRow row = dt.NewRow();

        //                      row["Pk_JobCardID"] = entity.Pk_JobCardID;
        //                      row["JDate"] = entity.JDate;
        //                      row["BName"] = entity.BName;
        //                      row["Name"] = entity.Name;
        //                      row["Fk_Order"] = entity.Fk_Order;
        //                      row["Invno"] = entity.Invno;


        //                      row["Corrugation"] = entity.Corrugation;
        //                      row["TopPaperQty"] = entity.TopPaperQty;
        //                      row["TwoPlyQty"] = entity.TwoPlyQty;
        //                      row["TwoPlyWt"] = entity.TwoPlyWt;
        //                      row["PastingQty"] = entity.PastingQty;
        //                      row["PastingWstQty"] = entity.PastingWstQty;

        //                      row["RotaryQty"] = entity.RotaryQty;
        //                      row["RotaryWstQty"] = entity.RotaryWstQty;
        //                      row["PunchingQty"] = entity.PunchingQty;
        //                      row["PunchingWstQty"] = entity.PunchingWstQty;
        //                      row["SlotingQty"] = entity.SlotingQty;
        //                      row["SlotingWstQty"] = entity.SlotingWstQty;

        //                      row["PinningQty"] = entity.PinningQty;
        //                      row["PinningWstQty"] = entity.PinningWstQty;
        //                      row["FinishingQty"] = entity.FinishingQty;
        //                      row["FinishingWstQty"] = entity.FinishingWstQty;
        //                      row["TotalQty"] = entity.TotalQty;
        //                      row["TotalWstQty"] = entity.TotalWstQty;
        //                      row["CustomerName"] = entity.CustomerName;
        //                      //row["Name"] = entity.Expr1;
        //                      dt.Rows.Add(row);
        //                  }


        //                  DataSet ds = new DataSet();
        //                  ds.Tables.Add(dt);
        //                  CogitoStreamline.Report.JCSamples orp = new CogitoStreamline.Report.JCSamples();

        //                  orp.Load("@\\Report\\JCSamples.rpt");
        //                  orp.SetDataSource(dt.DefaultView);

        //                  string pdfPath = Server.MapPath("~/ConvertPDF/" + "JCSamples" + sInvno + ".pdf");
        //                  FileInfo file = new FileInfo(pdfPath);
        //                  if (file.Exists)
        //                  {
        //                      file.Delete();
        //                  }
        //                  var pd = new PrintDocument();


        //                  orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
        //                  orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
        //                  DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
        //                  objDiskOpt.DiskFileName = pdfPath;

        //                  orp.ExportOptions.DestinationOptions = objDiskOpt;
        //                  orp.Export();

        //              }
        //              return null;
        //          }
        //          catch (Exception ex)
        //          {
        //              return null;

        //          }
        //      }


        public ActionResult JCRep1(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["JCNO"]);

                List<Vw_JCReport> BillList = new List<Vw_JCReport>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();


                BillList = dc.Vw_JCReport.Where(x => x.Pk_JobCardID == sInvno).Select(x => x).OfType<Vw_JCReport>().ToList();


                decimal NoOfPapers = 0;
                decimal PlySheets = 0;

                decimal TkUpFactor = 0;
                decimal OrderQty = 0;
                decimal PVal = 0;

                decimal DeckelVal;

                decimal BdSize = 0;
                decimal BoardSize = 0;

                if (BillList.Count > 0)
                {
                    DataTable dt = new DataTable();

                    dt.Columns.Add("Pk_JobCardID");
                    dt.Columns.Add("JDate");
                    dt.Columns.Add("BName");
                    dt.Columns.Add("CustomerName");
                    dt.Columns.Add("CuttingSize");
                    dt.Columns.Add("Rate");

                    dt.Columns.Add("PartTakeup");
                    dt.Columns.Add("BoardGSM");
                    dt.Columns.Add("BoardBS");
                    dt.Columns.Add("PlyVal");
                    //dt.Columns.Add("Pk_JobCardID");
                    dt.Columns.Add("Deckle");
                    dt.Columns.Add("PColor");
                    dt.Columns.Add("PDetails");
                    dt.Columns.Add("MFG");
                    dt.Columns.Add("DPRSc");
                    dt.Columns.Add("GSM");
                    dt.Columns.Add("BF");
                    dt.Columns.Add("TotalQty");
                    dt.Columns.Add("ChkQuality");

                    dt.Columns.Add("Printing");
                    dt.Columns.Add("OrdQty");
                    dt.Columns.Add("FluHt");

                    dt.Columns.Add("FluteName");
                    dt.Columns.Add("Weight");
                    dt.Columns.Add("DeliveryDate");
                    dt.Columns.Add("Tk_Factor");
                    dt.Columns.Add("Length");
                    dt.Columns.Add("Width");
                    dt.Columns.Add("Height");
                    dt.Columns.Add("ReturnQuantity");
                    dt.Columns.Add("MillName");
                    dt.Columns.Add("ColorName");
                    dt.Columns.Add("SchQty");

                    dt.Columns.Add("PastingQty");
                    dt.Columns.Add("Corrugation");
                    dt.Columns.Add("TopPaperQty");
                    dt.Columns.Add("TwoPlyQty");

                    dt.Columns.Add("RotaryQty");
                    dt.Columns.Add("PunchingQty");
                    dt.Columns.Add("SlotingQty");
                    dt.Columns.Add("PinningQty");

                    dt.Columns.Add("FinishingQty");
                    //dt.Columns.Add("TotalQty");
                    dt.Columns.Add("ECCQty");
                    dt.Columns.Add("GummingQty");




                    foreach (Vw_JCReport entity in BillList)
                    {
                        DataRow row = dt.NewRow();

                        row["Pk_JobCardID"] = entity.Pk_JobCardID;
                        row["JDate"] = entity.JDate;
                        row["BName"] = entity.BName;
                        row["CustomerName"] = entity.CustomerName;
                        row["CuttingSize"] = entity.CuttingSize;
                        //row["Rate"] = entity.PartRate;
                        row["PartTakeup"] = entity.PartTakeup;
                        row["BoardGSM"] = entity.BoardGSM;
                        row["BoardBS"] = entity.BoardBS;
                        row["PlyVal"] = entity.PlyVal;
                        row["PColor"] = entity.PColor;
                        row["PDetails"] = entity.PDetails;
                        row["MFG"] = entity.MFG;
                        row["DPRSc"] = entity.DPRSc;
                        row["GSM"] = entity.GSM;
                        row["BF"] = entity.BF;
                        row["TotalQty"] = entity.TotalQty;
                        //row["OrdQty"] = entity.OrdQty;
                        row["Printing"] = entity.Printing;
                        row["FluHt"] = entity.FluteHt;
                        row["FluteName"] = entity.FluteName;
                        row["Weight"] = entity.Weight;
                        row["DeliveryDate"] = entity.DeliveryDate;
                        row["Tk_Factor"] = entity.Tk_Factor;
                        row["Length"] = entity.Length;
                        row["Width"] = entity.Width;
                        row["Height"] = entity.Height;
                        row["Deckle"] = entity.Deckle;
                        //row["ColorName"] = entity.ColorName;
                        //row["MillName"] = entity.MillName;
                        row["SchQty"] = entity.SchQty;
                        row["ChkQuality"] = entity.ChkQuality;

                        row["PastingQty"] = entity.PastingQty;
                        row["Corrugation"] = entity.Corrugation;
                        row["TopPaperQty"] = entity.TopPaperQty;

                        row["TwoPlyQty"] = entity.TwoPlyQty;
                        row["RotaryQty"] = entity.RotaryQty;
                        row["PunchingQty"] = entity.PunchingQty;

                        row["SlotingQty"] = entity.SlotingQty;
                        row["PinningQty"] = entity.PinningQty;
                        row["FinishingQty"] = entity.FinishingQty;


                        row["TotalQty"] = entity.TotalQty;
                        row["ECCQty"] = entity.ECCQty;
                        row["GummingQty"] = entity.GummingQty;

                        //    row["ReturnQuantity"] = entity.ReturnQuantity;

                        TkUpFactor = Convert.ToDecimal(entity.ChkQuality);
                        DeckelVal = Convert.ToDecimal(entity.Deckle);
                        OrderQty = Convert.ToDecimal(entity.SchQty);                 ////earlier it was order qty, it should be schqty, after linking production schedule
                        PVal = Convert.ToDecimal(entity.PlyVal);

                        BdSize = DeckelVal - 2;
                        BoardSize = (BdSize * TkUpFactor) + 2;


                        if (TkUpFactor > 0)     ///FOR SGST
                        {
                            NoOfPapers = Convert.ToDecimal(OrderQty / TkUpFactor);

                            //PlySheets = Convert.ToDecimal(NoOfPapers * TkUpFactor);

                            if (PVal == 3)
                            {
                                PlySheets = Convert.ToDecimal(NoOfPapers * 1);
                            }
                            else
                                if (PVal == 5)
                                {
                                    PlySheets = Convert.ToDecimal(NoOfPapers * 2);
                                }

                                else if (PVal == 7)
                                {
                                    PlySheets = Convert.ToDecimal(NoOfPapers * 3);
                                }
                                else if (PVal == 9)
                                {
                                    PlySheets = Convert.ToDecimal(NoOfPapers * 4);
                                }
                        }

                        //  QtyReq = (NoOfPapers * TkUpFactor * CutLen / 100 * DCl / 100 * GSMVal / 100);
                        dt.Rows.Add(row);
                    }

                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.JCRep orp = new CogitoStreamline.Report.JCRep();

                    orp.Load("@\\Report\\JCRep.rpt");
                    orp.SetDataSource(dt.DefaultView);


                    CrystalDecisions.CrystalReports.Engine.ReportDocument repDoc = orp;

                    ParameterFieldDefinitions crParameterFieldDefinitions3;
                    ParameterFieldDefinition crParameterFieldDefinition3;
                    ParameterValues crParameterValues3 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue3 = new ParameterDiscreteValue();
                    crParameterDiscreteValue3.Value = NoOfPapers;
                    crParameterFieldDefinitions3 = orp.DataDefinition.ParameterFields;
                    crParameterFieldDefinition3 = crParameterFieldDefinitions3["NoPly"];
                    crParameterValues3 = crParameterFieldDefinition3.CurrentValues;

                    crParameterValues3.Clear();
                    crParameterValues3.Add(crParameterDiscreteValue3);
                    crParameterFieldDefinition3.ApplyCurrentValues(crParameterValues3);

                    ParameterFieldDefinitions crParameterFieldDefinitions5;
                    ParameterFieldDefinition crParameterFieldDefinition5;
                    ParameterValues crParameterValues5 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue5 = new ParameterDiscreteValue();
                    crParameterDiscreteValue5.Value = BoardSize;
                    crParameterFieldDefinitions5 = orp.DataDefinition.ParameterFields;
                    crParameterFieldDefinition5 = crParameterFieldDefinitions5["BoardSize"];
                    crParameterValues5 = crParameterFieldDefinition3.CurrentValues;

                    crParameterValues5.Clear();
                    crParameterValues5.Add(crParameterDiscreteValue5);
                    crParameterFieldDefinition5.ApplyCurrentValues(crParameterValues5);



                    ParameterFieldDefinitions crParameterFieldDefinitions4;
                    ParameterFieldDefinition crParameterFieldDefinition4;
                    ParameterValues crParameterValues4 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                    crParameterDiscreteValue4.Value = PlySheets;
                    crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                    crParameterFieldDefinition4 = crParameterFieldDefinitions4["PlySh"];
                    crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                    crParameterValues4.Clear();
                    crParameterValues4.Add(crParameterDiscreteValue4);
                    crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);


                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "JCRep" + sInvno + ".pdf");
                    FileInfo file = new FileInfo(pdfPath);
                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();

                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }

        [HttpPost]
        public override JsonResult Save(string data)
        {
            Dictionary<string, object> values = JsonConvert.DeserializeObject<Dictionary<string, object>>(data);


            //      Fk_EnquiryChild
            //ConvRate
            //ConvValue
            //GMarginPercentage
            //GMarginValue
            //TaxesPercntage
            //TaxesValue
            //TransportValue
            //WeightHValue
            //HandlingChanrgesValue
            //PackingChargesValue
            //RejectionPercentage
            //RejectionValue
            //TotalWeight
            //TotalPrice
            //Status
            //VATPercentage
            //VATValue
            //CSTPercentage
            //CSTValue
            //TransportPercentage

            data = JsonConvert.SerializeObject(values);
            //this.CurrentObject = null;
            return base.Save(data);


        }



        [HttpPost]

        public JsonResult SaveNew([System.Web.Http.FromBody]string data)
        {
            data = data.TrimStart('=');
            Dictionary<string, object> values = JsonConvert.DeserializeObject<Dictionary<string, object>>(data);
            data = JsonConvert.SerializeObject(values);
            return base.Save(data);


        }
    }
}


