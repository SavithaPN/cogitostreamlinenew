﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using WebGareCore;
using CogitoStreamline.Controllers;
//using iTextSharp;
using System.IO;
//using iTextSharp.text;
//using iTextSharp.text.pdf;
using System.Data;
using Microsoft.Reporting.WebForms;
using CogitoStreamline;
using System.Globalization;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Drawing.Printing;


namespace CogitoStreamline.Controllers
{
    public class InvBillingController : CommonController
    {
        
        Billing oBilling = new Billing();
        Taxx oTax = new Taxx();
        readonly CogitoStreamLineEntities _oEntities = new CogitoStreamLineEntities();

        public InvBillingController()
        {
            oDoaminObject = new Billing();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Billing";
            return base.Index();
        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
        //public ActionResult MonthWiseInvReport(string data = "")
        //{
        //    try
        //    {
        //        Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

        //        string[] a = oValues["FromDate1"].ToString().Split('/');
        //        DateTime FromDate = DateTime.ParseExact(a[1] + '/' + a[0] + '/' + a[2] + " 12:00", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);


        //        int dmonth = Convert.ToInt32(a[1]);
        //        int dyear = Convert.ToInt32(a[2]);

        //        string monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dmonth);
        //        //string Mname=FromDate.ToString( "MMMM");   // this one will also work to get the month name

        //        string Repfor = "Bill List for the month" + " " + "-" + monthName + " " + dyear.ToString();

        //        List<VwInvoice> BillList = new List<VwInvoice>();
        //        CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

        //        BillList = dc.VwInvoices.Where(x => x.InvDate.Value.Month == dmonth && x.InvDate.Value.Year == dyear).Select(x => x).OfType<VwInvoice>().ToList();

        //        if (BillList.Count > 0)
        //        {
        //            DataTable dt = new DataTable("InvoiceBill");

        //            dt.Columns.Add("CustomerName");
        //            dt.Columns.Add("InvDate");
        //            dt.Columns.Add("NetAmount");
        //            dt.Columns.Add("Pk_Invoice");
        //            dt.Columns.Add("GrandTotal");
        //            dt.Columns.Add("NetValue");
        //            //dt.Columns.Add("NetGrand");
        //            //dt.Columns.Add("NetVal");


        //            decimal NetValue = 0;



        //            foreach (VwInvoice entity in BillList)
        //            {
        //                DataRow row = dt.NewRow();

        //                row["CustomerName"] = entity.CustomerName;
        //                row["InvDate"] = entity.InvDate;
        //                row["NetAmount"] = entity.NetAmount;
        //                NetValue = Convert.ToDecimal(NetValue + entity.NetAmount);
        //                row["Pk_Invoice"] = entity.Pk_Invoice;
        //                row["GrandTotal"] = entity.GrandTotal;
        //                row["NetValue"] = entity.NETVALUE;

        //                dt.Rows.Add(row);
        //            }



        //            DataSet ds = new DataSet();
        //            ds.Tables.Add(dt);
        //            CogitoStreamline.Report.BillsList_MonthWise orp = new CogitoStreamline.Report.BillsList_MonthWise();

        //            orp.Load("@\\Report\\BillsList_MonthWise.rpt");
        //            orp.SetDataSource(dt.DefaultView);



        //            ReportDocument repDoc = orp;







        //            ParameterFieldDefinitions crParameterFieldDefinitions4;
        //            ParameterFieldDefinition crParameterFieldDefinition4;
        //            ParameterValues crParameterValues4 = new ParameterValues();
        //            ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
        //            crParameterDiscreteValue4.Value = Repfor;
        //            crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
        //            crParameterFieldDefinition4 = crParameterFieldDefinitions4["period"];
        //            crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

        //            crParameterValues4.Clear();
        //            crParameterValues4.Add(crParameterDiscreteValue4);
        //            crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);


        //            string pdfPath = Server.MapPath("~/ConvertPDF/" + "Desi MonthlyList" + monthName + " " + dyear + ".pdf");
        //            FileInfo file = new FileInfo(pdfPath);
        //            if (file.Exists)
        //            {
        //                file.Delete();
        //            }
        //            var pd = new PrintDocument();


        //            orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
        //            orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
        //            DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
        //            objDiskOpt.DiskFileName = pdfPath;

        //            orp.ExportOptions.DestinationOptions = objDiskOpt;
        //            orp.Export();

        //        }
        //        return null;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //        //  Response.Redirect("Index");
        //    }
        //}
        //public ActionResult CustomerWiseInvReport(string data = "")
        //{
        //    try
        //    {
        //        Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

        //        string[] a = oValues["FromDate1"].ToString().Split('/');
        //        DateTime FromDate = DateTime.ParseExact(a[1] + '/' + a[0] + '/' + a[2] + " 12:00", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);


        //        int dmonth = Convert.ToInt32(a[1]);
        //        int dyear = Convert.ToInt32(a[2]);

        //        string monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dmonth);
        //        //string Mname=FromDate.ToString( "MMMM");   // this one will also work to get the month name

        //        string Repfor = "Customerwise Bill List for the month" + " " + "-" + monthName + " " + dyear.ToString();

        //        List<vw_CustomerDesiBills> BillList = new List<vw_CustomerDesiBills>();
        //        CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

        //        BillList = dc.vw_CustomerDesiBills.Where(x => x.InvDate.Value.Month == dmonth && x.InvDate.Value.Year == dyear).Select(x => x).OfType<vw_CustomerDesiBills>().ToList();

        //        if (BillList.Count > 0)
        //        {
        //            DataTable dt = new DataTable("InvoiceBill");

        //            dt.Columns.Add("CustomerName");
        //            dt.Columns.Add("InvDate");
        //            dt.Columns.Add("NetAmount");
        //            dt.Columns.Add("Pk_Invoice");
        //            dt.Columns.Add("GrandTotal");
        //            dt.Columns.Add("NetValue");
        //            //dt.Columns.Add("SGOT");
        //            //dt.Columns.Add("SNET");
        //            dt.Columns.Add("Fk_Customer");
        //            dt.Columns.Add("DueDate");

        //            //decimal NetValue = 0;



        //            foreach (vw_CustomerDesiBills entity in BillList)
        //            {
        //                DataRow row = dt.NewRow();

        //                row["CustomerName"] = entity.CustomerName;
        //                row["InvDate"] = entity.InvDate;
        //                row["Fk_Customer"] = entity.Fk_Customer;
        //                //row["NetAmount"] = entity.NetAmount;
        //                //NetValue = Convert.ToDecimal(NetValue + entity.NetAmount);
        //                row["Pk_Invoice"] = entity.Pk_Invoice;
        //                row["GrandTotal"] = entity.GrandTotal;
        //                row["NetValue"] = entity.NETVALUE;
        //                //row["SGOT"] = entity.SGOT;
        //                //row["SNET"] = entity.SNET;
        //                row["DueDate"] = entity.DueDate;
        //                dt.Rows.Add(row);

        //            }





        //            //List<vw_CustwiseTot> BillAmount = new List<vw_CustwiseTot>();
        //            //CogitoStreamLineEntities dc1 = new CogitoStreamLineEntities();

        //            //BillAmount = dc1.vw_CustwiseTot.Where(x => x.InvDate.Value.Month == dmonth && x.InvDate.Value.Year == dyear).Select(x => x).OfType<vw_CustwiseTot>().ToList();

        //            //decimal NetGrand = 0;
        //            //decimal NetVal = 0;

        //            //foreach (vw_CustwiseTot entity in BillAmount)
        //            //{
        //            //    DataRow row = dt.NewRow();


        //            //    row["GrandTotal"] = entity.GTOT;
        //            //    NetGrand = Convert.ToDecimal(NetGrand + entity.GTOT);

        //            //    row["NetValue"] = entity.GNETVAL;
        //            //    NetVal = Convert.ToDecimal(NetVal + entity.GNETVAL);
        //            //}




        //            DataSet ds = new DataSet();
        //            ds.Tables.Add(dt);
        //            CogitoStreamline.Report.DesiCustRep orp = new CogitoStreamline.Report.DesiCustRep();

        //            orp.Load("@\\Report\\DesiCustRep.rpt");
        //            orp.SetDataSource(dt.DefaultView);



        //            ReportDocument repDoc = orp;

        //            //ParameterFieldDefinitions crParameterFieldDefinitions3;
        //            //ParameterFieldDefinition crParameterFieldDefinition3;
        //            //ParameterValues crParameterValues3 = new ParameterValues();
        //            //ParameterDiscreteValue crParameterDiscreteValue3 = new ParameterDiscreteValue();
        //            //crParameterDiscreteValue3.Value = NetGrand;
        //            //crParameterFieldDefinitions3 = orp.DataDefinition.ParameterFields;
        //            //crParameterFieldDefinition3 = crParameterFieldDefinitions3["NetGrandtot"];
        //            //crParameterValues3 = crParameterFieldDefinition3.CurrentValues;

        //            //crParameterValues3.Clear();
        //            //crParameterValues3.Add(crParameterDiscreteValue3);
        //            //crParameterFieldDefinition3.ApplyCurrentValues(crParameterValues3);


        //            //ParameterFieldDefinitions crParameterFieldDefinitions5;
        //            //ParameterFieldDefinition crParameterFieldDefinition5;
        //            //ParameterValues crParameterValues5 = new ParameterValues();
        //            //ParameterDiscreteValue crParameterDiscreteValue5 = new ParameterDiscreteValue();
        //            //crParameterDiscreteValue5.Value = NetVal;
        //            //crParameterFieldDefinitions5 = orp.DataDefinition.ParameterFields;
        //            //crParameterFieldDefinition5 = crParameterFieldDefinitions5["NetGrandValue"];
        //            //crParameterValues5 = crParameterFieldDefinition5.CurrentValues;

        //            //crParameterValues5.Clear();
        //            //crParameterValues5.Add(crParameterDiscreteValue5);
        //            //crParameterFieldDefinition5.ApplyCurrentValues(crParameterValues5);



        //            ParameterFieldDefinitions crParameterFieldDefinitions4;
        //            ParameterFieldDefinition crParameterFieldDefinition4;
        //            ParameterValues crParameterValues4 = new ParameterValues();
        //            ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
        //            crParameterDiscreteValue4.Value = Repfor;
        //            crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
        //            crParameterFieldDefinition4 = crParameterFieldDefinitions4["period"];
        //            crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

        //            crParameterValues4.Clear();
        //            crParameterValues4.Add(crParameterDiscreteValue4);
        //            crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);


        //            string pdfPath = Server.MapPath("~/ConvertPDF/" + "Desi MonthlyList CustomerWise" + "-" + monthName + " " + dyear + ".pdf");
        //            FileInfo file = new FileInfo(pdfPath);
        //            if (file.Exists)
        //            {
        //                file.Delete();
        //            }
        //            var pd = new PrintDocument();


        //            orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
        //            orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
        //            DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
        //            objDiskOpt.DiskFileName = pdfPath;

        //            orp.ExportOptions.DestinationOptions = objDiskOpt;
        //            orp.Export();

        //        }
        //        return null;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //        //  Response.Redirect("Index");D:\shantha\Billing\CogitoStreamline\CogitoStreamline\Controllers\CartonBillingController.cs
        //    }
        //}
        public ActionResult InvReport(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["Invno"]);

                List<VwInvoice> BillList = new List<VwInvoice>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();


                Inv_Billing oBill = _oEntities.Inv_Billing.Where(p => p.Pk_Invoice == sInvno).Single();

                var DelAdd = oBill.DeliveryName;
                if (DelAdd != null)
                { }
                else
                {
                    oBill.DeliveryName = oBill.gen_Customer.CustomerName;
                    oBill.DeliveryAdd1 = oBill.gen_Customer.CustomerAddress;
                    oBill.DeliveryAdd2 = oBill.gen_Customer.gen_State.StateName +","+ oBill.gen_Customer.gen_City.CityName+","+ oBill.gen_Customer.PinCode;
                    _oEntities.SaveChanges();
                }


                //BillList = dc.VwInvoices.ToList();
                BillList = dc.VwInvoice.Where(x => x.Pk_Invoice == sInvno).Select(x => x).OfType<VwInvoice>().ToList();

                if (BillList.Count > 0)
                {
                    //var CtrVal = BillList.Count;
                    DataTable dt = new DataTable("InvoiceBill");

                    dt.Columns.Add("CustomerName");
                    dt.Columns.Add("CustomerAddress");
                    dt.Columns.Add("CityName");
                    dt.Columns.Add("StateName");
                    dt.Columns.Add("PinCode");
                    dt.Columns.Add("Pk_Invoice");
                    dt.Columns.Add("InvDate");
                    dt.Columns.Add("Fk_OrderNo");
                    dt.Columns.Add("TaxValue");
                    dt.Columns.Add("Quantity");
                    dt.Columns.Add("Price");
                    dt.Columns.Add("Fk_JobCardID");
                    dt.Columns.Add("NetAmount");
                    dt.Columns.Add("Name");
                    dt.Columns.Add("Description");
                    dt.Columns.Add("Pk_Inv_Details");
                    dt.Columns.Add("Esugam");
                    dt.Columns.Add("COA");
                    dt.Columns.Add("CreditLimit");
                    dt.Columns.Add("HsnCode");
                    dt.Columns.Add("LandLine");
                    dt.Columns.Add("VAT");
                    //dt.Columns.Add("");
                    dt.Columns.Add("TaxType");

                    dt.Columns.Add("DeliveryName");
                    dt.Columns.Add("DeliveryAdd1");
                    dt.Columns.Add("DeliveryAdd2");

                    dt.Columns.Add("PName");
                    dt.Columns.Add("Length");
                    dt.Columns.Add("Width");
                    dt.Columns.Add("Height");


                    dt.Columns.Add("Buyers_OrderNo");
                    dt.Columns.Add("BOrderDated");
                    dt.Columns.Add("DNTimeofInv");
                    dt.Columns.Add("MVehicleNo");
                    dt.Columns.Add("DNTimeofRemoval");
                    dt.Columns.Add("Pk_PartPropertyID");
                    dt.Columns.Add("UnitVal");

                    dt.Columns.Add("Despatched");
                    dt.Columns.Add("Destination");
                    dt.Columns.Add("Bundles");


                    decimal NetValue = 0;
                    decimal cgval = 0;
                    decimal ExValue = 0;
                    decimal VatVal = 0;
                    string ttype = "";
                    string ttype1 = "";
                    string Ttax = "";
                    string fct3 = "";
                    //string fh = "";

                    foreach (VwInvoice entity in BillList)
                    {
                        DataRow row = dt.NewRow();

                        row["CustomerName"] = entity.CustomerName;
                        row["CustomerAddress"] = entity.CustomerAddress;
                        row["CityName"] = entity.CityName;
                        row["StateName"] = entity.StateName;
                        row["PinCode"] = entity.PinCode;
                        row["Pk_Invoice"] = entity.Pk_Invoice;
                        row["InvDate"] = entity.InvDate;
                        row["Fk_OrderNo"] = entity.Fk_OrderNo;
                        row["TaxValue"] = entity.TaxValue;
                        row["Quantity"] = entity.Quantity;
                        row["Price"] = entity.Price;
                       row["VAT"] = entity.VAT;
                        row["NetAmount"] = entity.NetAmount;
                        NetValue = Convert.ToDecimal(NetValue + entity.NetAmount);
                        row["Name"] = entity.Name;
                        row["HsnCode"] = entity.HsnCode;
                        row["Pk_Inv_Details"] = entity.Pk_Inv_Details;
                        row["Esugam"] = entity.ESUGAM;
                        row["COA"] = entity.COA;
                        row["CreditLimit"] = entity.CreditLimit;
                        row["HsnCode"] = entity.HsnCode;
                        row["Description"] = entity.Description;

                        row["DeliveryName"] = entity.DeliveryName;
                        row["DeliveryAdd1"] = entity.DeliveryAdd1;
                        row["DeliveryAdd2"] = entity.DeliveryAdd2;


                        row["PName"] = entity.PName;
                        row["Length"] = entity.Length;
                        row["Width"] = entity.Width;
                        row["Height"] = entity.Height;


                        row["Buyers_OrderNo"] = entity.Buyers_OrderNo;
                        row["BOrderDated"] = entity.BOrderDated;
                        row["DNTimeofInv"] = entity.DNTimeofInv;
                        row["MVehicleNo"] = entity.MVehicleNo;
                        row["DNTimeofRemoval"] = entity.DNTimeofRemoval;
                        row["Pk_PartPropertyID"] = entity.Pk_PartPropertyID;

                        row["UnitVal"] = entity.UnitVal;
                        row["LandLine"] = entity.LandLine;



                        row["Despatched"] = entity.Despatched;
                        row["Destination"] = entity.Destination;
                        row["Bundles"] = entity.Bundles;

                        row["TaxType"] = entity.TaxType;

                        if (entity.TaxType.TrimEnd() == "SGST")     ///FOR SGST
                        {
                            Ttax = "V";
                            ttype = "CGST @ 6%";
                            ttype1 = "SGST @ 6%";

                        }
                        else if (entity.TaxType.TrimEnd() == "IGST")   ///FOR IGST
                        {
                            Ttax = "C";
                            ttype = "IGST @ 12 % ";
                        }

                        else if (entity.TaxType.TrimEnd() == "9%")   ///FOR IGST
                        {
                            Ttax = "9";
                            ttype = "CGST @ 9%";
                            ttype1 = "SGST @ 9%";
                        }
                        else if (entity.TaxType.TrimEnd() == "2.5%")   ///FOR IGST
                        {
                            Ttax = "2.5";
                            ttype = "CGST @ 2.5%";
                            ttype1 = "SGST @ 2.5%";
                        }
                        else if (entity.TaxType.TrimEnd() == "5%")   ///FOR IGST
                        {
                            Ttax = "5";
                            ttype = "CGST @ 5%";
                            ttype1 = "SGST @ 5%";
                        }
                        dt.Rows.Add(row);
                    }
                    NetValue = Convert.ToDecimal(NetValue);
                    if (fct3.Length > 0)
                    {
                        ExValue = 0;
                    }
                    else
                    {
                        ExValue = (Convert.ToDecimal(NetValue) * Convert.ToDecimal(0.06));
                    }

                    if (Ttax == "V")
                    {
                        VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.06);
                        cgval = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.06);
                    }
                    else if (Ttax == "C")
                    {
                        VatVal = 0;
                        cgval = 0;
                        VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.12);
                    }
                    else if (Ttax == "9")
                    {
                        VatVal = 0;
                        cgval = 0;
                        VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.09);
                        cgval = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.09);
                    }
                    else if (Ttax == "2.5")
                    {
                        VatVal = 0;
                        cgval = 0;
                        VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.025);
                        cgval = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.025);
                    }
                    else if (Ttax == "5")
                    {
                        VatVal = 0;
                        cgval = 0;
                        VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.05);
                        cgval = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.05);
                    }




                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.Bill1 orp = new CogitoStreamline.Report.Bill1();

                    orp.Load("@\\Report\\Bill1.rpt");
                    orp.SetDataSource(dt.DefaultView);



                    ReportDocument repDoc = orp;

                    ParameterFieldDefinitions crParameterFieldDefinitions3;
                    ParameterFieldDefinition crParameterFieldDefinition3;
                    ParameterValues crParameterValues3 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue3 = new ParameterDiscreteValue();
                    crParameterDiscreteValue3.Value = NetValue;
                    crParameterFieldDefinitions3 = orp.DataDefinition.ParameterFields;
                    crParameterFieldDefinition3 = crParameterFieldDefinitions3["TotAmt"];
                    crParameterValues3 = crParameterFieldDefinition3.CurrentValues;

                    crParameterValues3.Clear();
                    crParameterValues3.Add(crParameterDiscreteValue3);
                    crParameterFieldDefinition3.ApplyCurrentValues(crParameterValues3);



                    ParameterFieldDefinitions crParameterFieldDefinitions4;
                    ParameterFieldDefinition crParameterFieldDefinition4;
                    ParameterValues crParameterValues4 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                    crParameterDiscreteValue4.Value = "Original for Recipient";
                    crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                    crParameterFieldDefinition4 = crParameterFieldDefinitions4["CopyName"];
                    crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                    crParameterValues4.Clear();
                    crParameterValues4.Add(crParameterDiscreteValue4);
                    crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);


                    //ParameterFieldDefinitions crParameterFieldDefinitions5;
                    //ParameterFieldDefinition crParameterFieldDefinition5;
                    //ParameterValues crParameterValues5 = new ParameterValues();
                    //ParameterDiscreteValue crParameterDiscreteValue5 = new ParameterDiscreteValue();
                    //crParameterDiscreteValue5.Value = fct3;
                    //crParameterFieldDefinitions5 = orp.DataDefinition.ParameterFields;
                    //crParameterFieldDefinition5 = crParameterFieldDefinitions4["StrValuefct3"];
                    //crParameterValues5 = crParameterFieldDefinition5.CurrentValues;

                    //crParameterValues5.Clear();
                    //crParameterValues5.Add(crParameterDiscreteValue5);
                    //crParameterFieldDefinition5.ApplyCurrentValues(crParameterValues5);




                    //ParameterFieldDefinitions crParameterFieldDefinitions6;
                    //ParameterFieldDefinition crParameterFieldDefinition6;
                    //ParameterValues crParameterValues6 = new ParameterValues();
                    //ParameterDiscreteValue crParameterDiscreteValue6 = new ParameterDiscreteValue();
                    //crParameterDiscreteValue6.Value = fh;
                    //crParameterFieldDefinitions6 = orp.DataDefinition.ParameterFields;
                    //crParameterFieldDefinition6 = crParameterFieldDefinitions6["StrValueh"];
                    //crParameterValues6 = crParameterFieldDefinition6.CurrentValues;

                    //crParameterValues6.Clear();
                    //crParameterValues6.Add(crParameterDiscreteValue6);
                    //crParameterFieldDefinition6.ApplyCurrentValues(crParameterValues6);


                    ParameterFieldDefinitions crParameterFieldDefinitions7;
                    ParameterFieldDefinition crParameterFieldDefinition7;
                    ParameterValues crParameterValues7 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue7 = new ParameterDiscreteValue();
                    crParameterDiscreteValue7.Value = VatVal;
                    crParameterFieldDefinitions7 = orp.DataDefinition.ParameterFields;
                    crParameterFieldDefinition7 = crParameterFieldDefinitions7["cgst"];
                    crParameterValues7 = crParameterFieldDefinition7.CurrentValues;

                    crParameterValues7.Clear();
                    crParameterValues7.Add(crParameterDiscreteValue7);
                    crParameterFieldDefinition7.ApplyCurrentValues(crParameterValues7);




                    ParameterFieldDefinitions crParameterFieldDefinitions1;
                    ParameterFieldDefinition crParameterFieldDefinition1;
                    ParameterValues crParameterValues1 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue1 = new ParameterDiscreteValue();
                    crParameterDiscreteValue1.Value = VatVal;
                    crParameterFieldDefinitions1 = orp.DataDefinition.ParameterFields;
                    crParameterFieldDefinition1 = crParameterFieldDefinitions1["vat5"];
                    crParameterValues1 = crParameterFieldDefinition1.CurrentValues;

                    crParameterValues1.Clear();
                    crParameterValues1.Add(crParameterDiscreteValue1);
                    crParameterFieldDefinition1.ApplyCurrentValues(crParameterValues1);

                    //ttype


                    ParameterFieldDefinitions crParameterFieldDefinitions12;
                    ParameterFieldDefinition crParameterFieldDefinition12;
                    ParameterValues crParameterValues12 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue12 = new ParameterDiscreteValue();
                    crParameterDiscreteValue12.Value = "OUTPUT " + ttype;
                    crParameterFieldDefinitions12 = orp.DataDefinition.ParameterFields;
                    crParameterFieldDefinition12 = crParameterFieldDefinitions1["TAXT"];
                    crParameterValues12 = crParameterFieldDefinition12.CurrentValues;

                    crParameterValues12.Clear();
                    crParameterValues12.Add(crParameterDiscreteValue12);
                    crParameterFieldDefinition12.ApplyCurrentValues(crParameterValues12);


                    if (ttype1.Length > 0)
                    {
                        ParameterFieldDefinitions crParameterFieldDefinitions6;
                        ParameterFieldDefinition crParameterFieldDefinition6;
                        ParameterValues crParameterValues6 = new ParameterValues();
                        ParameterDiscreteValue crParameterDiscreteValue6 = new ParameterDiscreteValue();
                        crParameterDiscreteValue6.Value = "OUTPUT " + ttype1;
                        crParameterFieldDefinitions6 = orp.DataDefinition.ParameterFields;
                        crParameterFieldDefinition6 = crParameterFieldDefinitions1["TAXT1"];
                        crParameterValues6 = crParameterFieldDefinition6.CurrentValues;

                        crParameterValues6.Clear();
                        crParameterValues6.Add(crParameterDiscreteValue6);
                        crParameterFieldDefinition6.ApplyCurrentValues(crParameterValues6);
                    }
                    else
                    {
                        ParameterFieldDefinitions crParameterFieldDefinitions6;
                        ParameterFieldDefinition crParameterFieldDefinition6;
                        ParameterValues crParameterValues6 = new ParameterValues();
                        ParameterDiscreteValue crParameterDiscreteValue6 = new ParameterDiscreteValue();
                        crParameterDiscreteValue6.Value = ttype1;
                        crParameterFieldDefinitions6 = orp.DataDefinition.ParameterFields;
                        crParameterFieldDefinition6 = crParameterFieldDefinitions1["TAXT1"];
                        crParameterValues6 = crParameterFieldDefinition6.CurrentValues;

                        crParameterValues6.Clear();
                        crParameterValues6.Add(crParameterDiscreteValue6);
                        crParameterFieldDefinition6.ApplyCurrentValues(crParameterValues6);
                    }
                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "BillOriginal-" + " " + sInvno + ".pdf");
                    FileInfo file = new FileInfo(pdfPath);
                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();

                }
                else
                {
                    List<Vw_InvRM> BillList1 = new List<Vw_InvRM>();
                    CogitoStreamLineEntities dc1 = new CogitoStreamLineEntities();
                    BillList1 = dc1.Vw_InvRM.Where(x => x.Pk_Invoice == sInvno).Select(x => x).OfType<Vw_InvRM>().ToList();


                    if (BillList1.Count > 0)
                    {
                        //var CtrVal = BillList.Count;
                        DataTable dt1 = new DataTable("InvoiceBill");

                        dt1.Columns.Add("CustomerName");
                        dt1.Columns.Add("CustomerAddress");
                        dt1.Columns.Add("CityName");
                        dt1.Columns.Add("StateName");
                        dt1.Columns.Add("PinCode");
                        dt1.Columns.Add("Pk_Invoice");
                        dt1.Columns.Add("InvDate");
                        dt1.Columns.Add("Fk_OrderNo");
                        dt1.Columns.Add("TaxValue");
                        dt1.Columns.Add("Quantity");
                        dt1.Columns.Add("Price");
                        dt1.Columns.Add("Fk_JobCardID");
                        dt1.Columns.Add("NetAmount");
                        //dt1.Columns.Add("Name");
                        dt1.Columns.Add("Description");
                        dt1.Columns.Add("Pk_Inv_Details");
                        dt1.Columns.Add("Esugam");
                        dt1.Columns.Add("COA");
                        dt1.Columns.Add("CreditLimit");
                        dt1.Columns.Add("HsnCode");
                        dt1.Columns.Add("LandLine");
                        dt1.Columns.Add("VAT");
                        //dt1.Columns.Add("");
                        dt1.Columns.Add("TaxType");

                        dt1.Columns.Add("DeliveryName");
                        dt1.Columns.Add("DeliveryAdd1");
                        dt1.Columns.Add("DeliveryAdd2");

                        dt1.Columns.Add("PName");
                        dt1.Columns.Add("Length");
                        dt1.Columns.Add("Width");
                        dt1.Columns.Add("Height");


                        dt1.Columns.Add("Buyers_OrderNo");
                        dt1.Columns.Add("BOrderDated");
                        dt1.Columns.Add("DNTimeofInv");
                        dt1.Columns.Add("MVehicleNo");
                        dt1.Columns.Add("DNTimeofRemoval");
                        dt1.Columns.Add("Name");
                        dt1.Columns.Add("UnitVal");
                        dt1.Columns.Add("Despatched");
                        dt1.Columns.Add("Destination");


                        decimal NetValue = 0;
                        decimal cgval = 0;
                        decimal ExValue = 0;
                        decimal VatVal = 0;
                        string ttype = "";
                        string ttype1 = "";
                        string Ttax = "";
                        string fct3 = "";
                        //string fh = "";

                        foreach (Vw_InvRM entity in BillList1)
                        {
                            DataRow row = dt1.NewRow();

                            row["CustomerName"] = entity.CustomerName;
                            row["CustomerAddress"] = entity.CustomerAddress;
                            row["CityName"] = entity.CityName;
                            row["StateName"] = entity.StateName;
                            row["PinCode"] = entity.PinCode;
                            row["Pk_Invoice"] = entity.Pk_Invoice;
                            row["InvDate"] = entity.InvDate;
                            row["Fk_OrderNo"] = entity.Fk_OrderNo;
                            row["TaxValue"] = entity.TaxValue;
                            row["Quantity"] = entity.Quantity;
                            row["Price"] = entity.Price;
                            //row["Fk_JobCardID"] = entity.Fk_JobCardID;
                            row["NetAmount"] = entity.NetAmount;
                            NetValue = Convert.ToDecimal(NetValue + entity.NetAmount);
                            //row["Name"] = entity.Name;
                            row["HsnCode"] = entity.HsnCode;
                            row["Pk_Inv_Details"] = entity.Pk_Inv_Details;
                            row["Esugam"] = entity.ESUGAM;
                            row["COA"] = entity.COA;
                            row["CreditLimit"] = entity.CreditLimit;
                            row["HsnCode"] = entity.HsnCode;
                            row["Description"] = entity.Description;

                            row["DeliveryName"] = entity.DeliveryName;
                            row["DeliveryAdd1"] = entity.DeliveryAdd1;
                            row["DeliveryAdd2"] = entity.DeliveryAdd2;


                            //row["PName"] = entity.PName;
                            //row["Length"] = entity.Length;
                            //row["Width"] = entity.Width;
                            //row["Height"] = entity.Height;


                            row["Buyers_OrderNo"] = entity.Buyers_OrderNo;
                            row["BOrderDated"] = entity.BOrderDated;
                            row["DNTimeofInv"] = entity.DNTimeofInv;
                            row["MVehicleNo"] = entity.MVehicleNo;
                            row["DNTimeofRemoval"] = entity.DNTimeofRemoval;
                            row["Name"] = entity.Name;

                            row["UnitVal"] = entity.UnitVal;
                            row["LandLine"] = entity.LandLine;


                            row["Despatched"] = entity.Despatched;
                            row["Destination"] = entity.Destination;
                            row["VAT"] = entity.VAT;
                            row["TaxType"] = entity.TaxType;

                            if (entity.TaxType.TrimEnd() == "SGST")     ///FOR SGST
                            {
                                Ttax = "V";
                                ttype = "CGST @ 6%";
                                ttype1 = "SGST @ 6 %";

                            }
                            else if (entity.TaxType.TrimEnd() == "IGST")   ///FOR IGST
                            {
                                Ttax = "C";
                                ttype = "IGST @ 12 % ";
                            }
                            else if (entity.TaxType.TrimEnd() == "9%")   ///FOR IGST
                        {
                            Ttax = "9";
                            ttype = "CGST @ 9%";
                            ttype1 = "SGST @ 9%";
                        }
                        else if (entity.TaxType.TrimEnd() == "2.5%")   ///FOR IGST
                        {
                            Ttax = "2.5";
                            ttype = "CGST @ 2.5%";
                            ttype1 = "SGST @ 2.5%";
                        }
                        else if (entity.TaxType.TrimEnd() == "5%")   ///FOR IGST
                        {
                            Ttax = "5";
                            ttype = "CGST @ 5%";
                            ttype1 = "SGST @ 5%";
                        }
                        dt1.Rows.Add(row);
                    }
                    NetValue = Convert.ToDecimal(NetValue);
                    if (fct3.Length > 0)
                    {
                        ExValue = 0;
                    }
                    else
                    {
                        ExValue = (Convert.ToDecimal(NetValue) * Convert.ToDecimal(0.06));
                    }

                    if (Ttax == "V")
                    {
                        VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.06);
                        cgval = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.06);
                    }
                    else if (Ttax == "C")
                    {
                        VatVal = 0;
                        cgval = 0;
                        VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.12);
                    }
                    else if (Ttax == "9")
                    {
                        VatVal = 0;
                        cgval = 0;
                        VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.09);
                        cgval = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.09);
                    }
                    else if (Ttax == "2.5")
                    {
                        VatVal = 0;
                        cgval = 0;
                        VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.025);
                        cgval = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.025);
                    }
                    else if (Ttax == "5")
                    {
                        VatVal = 0;
                        cgval = 0;
                        VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.05);
                        cgval = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.05);
                    }

                         



                        DataSet ds = new DataSet();
                        ds.Tables.Add(dt1);
                        CogitoStreamline.Report.BillRM orp = new CogitoStreamline.Report.BillRM();

                        orp.Load("@\\Report\\BillRM.rpt");
                        orp.SetDataSource(dt1.DefaultView);



                        ReportDocument repDoc = orp;

                        ParameterFieldDefinitions crParameterFieldDefinitions3;
                        ParameterFieldDefinition crParameterFieldDefinition3;
                        ParameterValues crParameterValues3 = new ParameterValues();
                        ParameterDiscreteValue crParameterDiscreteValue3 = new ParameterDiscreteValue();
                        crParameterDiscreteValue3.Value = NetValue;
                        crParameterFieldDefinitions3 = orp.DataDefinition.ParameterFields;
                        crParameterFieldDefinition3 = crParameterFieldDefinitions3["TotAmt"];
                        crParameterValues3 = crParameterFieldDefinition3.CurrentValues;

                        crParameterValues3.Clear();
                        crParameterValues3.Add(crParameterDiscreteValue3);
                        crParameterFieldDefinition3.ApplyCurrentValues(crParameterValues3);



                        ParameterFieldDefinitions crParameterFieldDefinitions4;
                        ParameterFieldDefinition crParameterFieldDefinition4;
                        ParameterValues crParameterValues4 = new ParameterValues();
                        ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                        crParameterDiscreteValue4.Value = "Original for Recipient";
                        crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                        crParameterFieldDefinition4 = crParameterFieldDefinitions4["CopyName"];
                        crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                        crParameterValues4.Clear();
                        crParameterValues4.Add(crParameterDiscreteValue4);
                        crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);


                        //ParameterFieldDefinitions crParameterFieldDefinitions5;
                        //ParameterFieldDefinition crParameterFieldDefinition5;
                        //ParameterValues crParameterValues5 = new ParameterValues();
                        //ParameterDiscreteValue crParameterDiscreteValue5 = new ParameterDiscreteValue();
                        //crParameterDiscreteValue5.Value = fct3;
                        //crParameterFieldDefinitions5 = orp.DataDefinition.ParameterFields;
                        //crParameterFieldDefinition5 = crParameterFieldDefinitions4["StrValuefct3"];
                        //crParameterValues5 = crParameterFieldDefinition5.CurrentValues;

                        //crParameterValues5.Clear();
                        //crParameterValues5.Add(crParameterDiscreteValue5);
                        //crParameterFieldDefinition5.ApplyCurrentValues(crParameterValues5);




                        //ParameterFieldDefinitions crParameterFieldDefinitions6;
                        //ParameterFieldDefinition crParameterFieldDefinition6;
                        //ParameterValues crParameterValues6 = new ParameterValues();
                        //ParameterDiscreteValue crParameterDiscreteValue6 = new ParameterDiscreteValue();
                        //crParameterDiscreteValue6.Value = fh;
                        //crParameterFieldDefinitions6 = orp.DataDefinition.ParameterFields;
                        //crParameterFieldDefinition6 = crParameterFieldDefinitions6["StrValueh"];
                        //crParameterValues6 = crParameterFieldDefinition6.CurrentValues;

                        //crParameterValues6.Clear();
                        //crParameterValues6.Add(crParameterDiscreteValue6);
                        //crParameterFieldDefinition6.ApplyCurrentValues(crParameterValues6);


                        ParameterFieldDefinitions crParameterFieldDefinitions7;
                        ParameterFieldDefinition crParameterFieldDefinition7;
                        ParameterValues crParameterValues7 = new ParameterValues();
                        ParameterDiscreteValue crParameterDiscreteValue7 = new ParameterDiscreteValue();
                        crParameterDiscreteValue7.Value = VatVal;
                        crParameterFieldDefinitions7 = orp.DataDefinition.ParameterFields;
                        crParameterFieldDefinition7 = crParameterFieldDefinitions7["cgst"];
                        crParameterValues7 = crParameterFieldDefinition7.CurrentValues;

                        crParameterValues7.Clear();
                        crParameterValues7.Add(crParameterDiscreteValue7);
                        crParameterFieldDefinition7.ApplyCurrentValues(crParameterValues7);




                        ParameterFieldDefinitions crParameterFieldDefinitions1;
                        ParameterFieldDefinition crParameterFieldDefinition1;
                        ParameterValues crParameterValues1 = new ParameterValues();
                        ParameterDiscreteValue crParameterDiscreteValue1 = new ParameterDiscreteValue();
                        crParameterDiscreteValue1.Value = VatVal;
                        crParameterFieldDefinitions1 = orp.DataDefinition.ParameterFields;
                        crParameterFieldDefinition1 = crParameterFieldDefinitions1["vat5"];
                        crParameterValues1 = crParameterFieldDefinition1.CurrentValues;

                        crParameterValues1.Clear();
                        crParameterValues1.Add(crParameterDiscreteValue1);
                        crParameterFieldDefinition1.ApplyCurrentValues(crParameterValues1);

                        //ttype


                        ParameterFieldDefinitions crParameterFieldDefinitions12;
                        ParameterFieldDefinition crParameterFieldDefinition12;
                        ParameterValues crParameterValues12 = new ParameterValues();
                        ParameterDiscreteValue crParameterDiscreteValue12 = new ParameterDiscreteValue();
                        crParameterDiscreteValue12.Value = "OUTPUT " + ttype;
                        crParameterFieldDefinitions12 = orp.DataDefinition.ParameterFields;
                        crParameterFieldDefinition12 = crParameterFieldDefinitions1["TAXT"];
                        crParameterValues12 = crParameterFieldDefinition12.CurrentValues;

                        crParameterValues12.Clear();
                        crParameterValues12.Add(crParameterDiscreteValue12);
                        crParameterFieldDefinition12.ApplyCurrentValues(crParameterValues12);


                        if (ttype1.Length > 0)
                        {
                            ParameterFieldDefinitions crParameterFieldDefinitions6;
                            ParameterFieldDefinition crParameterFieldDefinition6;
                            ParameterValues crParameterValues6 = new ParameterValues();
                            ParameterDiscreteValue crParameterDiscreteValue6 = new ParameterDiscreteValue();
                            crParameterDiscreteValue6.Value = "OUTPUT " + ttype1;
                            crParameterFieldDefinitions6 = orp.DataDefinition.ParameterFields;
                            crParameterFieldDefinition6 = crParameterFieldDefinitions1["TAXT1"];
                            crParameterValues6 = crParameterFieldDefinition6.CurrentValues;

                            crParameterValues6.Clear();
                            crParameterValues6.Add(crParameterDiscreteValue6);
                            crParameterFieldDefinition6.ApplyCurrentValues(crParameterValues6);
                        }
                        else
                        {
                            ParameterFieldDefinitions crParameterFieldDefinitions6;
                            ParameterFieldDefinition crParameterFieldDefinition6;
                            ParameterValues crParameterValues6 = new ParameterValues();
                            ParameterDiscreteValue crParameterDiscreteValue6 = new ParameterDiscreteValue();
                            crParameterDiscreteValue6.Value = ttype1;
                            crParameterFieldDefinitions6 = orp.DataDefinition.ParameterFields;
                            crParameterFieldDefinition6 = crParameterFieldDefinitions1["TAXT1"];
                            crParameterValues6 = crParameterFieldDefinition6.CurrentValues;

                            crParameterValues6.Clear();
                            crParameterValues6.Add(crParameterDiscreteValue6);
                            crParameterFieldDefinition6.ApplyCurrentValues(crParameterValues6);
                        }
                        string pdfPath = Server.MapPath("~/ConvertPDF/" + "BillOriginal-" + " " + sInvno + ".pdf");
                        FileInfo file = new FileInfo(pdfPath);
                        if (file.Exists)
                        {
                            file.Delete();
                        }
                        var pd = new PrintDocument();


                        orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                        orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                        DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                        objDiskOpt.DiskFileName = pdfPath;

                        orp.ExportOptions.DestinationOptions = objDiskOpt;
                        orp.Export();
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message, ex.Source);
                return null;
                //  Response.Redirect("Index");
            }
        }





        public ActionResult InvReportDuplicate(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["Invno"]);

                List<VwInvoice> BillList = new List<VwInvoice>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                //BillList = dc.VwInvoices.ToList();
                BillList = dc.VwInvoice.Where(x => x.Pk_Invoice == sInvno).Select(x => x).OfType<VwInvoice>().ToList();

                if (BillList.Count > 0)
                {
                    //var CtrVal = BillList.Count;
                    DataTable dt = new DataTable("InvoiceBill");

                    dt.Columns.Add("CustomerName");
                    dt.Columns.Add("CustomerAddress");
                    dt.Columns.Add("CityName");
                    dt.Columns.Add("StateName");
                    dt.Columns.Add("PinCode");
                    dt.Columns.Add("Pk_Invoice");
                    dt.Columns.Add("InvDate");
                    dt.Columns.Add("Fk_OrderNo");
                    dt.Columns.Add("TaxValue");
                    dt.Columns.Add("Quantity");
                    dt.Columns.Add("Price");
                    dt.Columns.Add("Fk_JobCardID");
                    dt.Columns.Add("NetAmount");
                    dt.Columns.Add("Name");
                    dt.Columns.Add("Description");
                    dt.Columns.Add("Pk_Inv_Details");
                    dt.Columns.Add("Esugam");
                    dt.Columns.Add("COA");
                    dt.Columns.Add("CreditLimit");
                    dt.Columns.Add("HsnCode");
                    dt.Columns.Add("LandLine");
                    dt.Columns.Add("VAT");
                    //dt.Columns.Add("");
                    dt.Columns.Add("TaxType");

                    dt.Columns.Add("DeliveryName");
                    dt.Columns.Add("DeliveryAdd1");
                    dt.Columns.Add("DeliveryAdd2");

                    dt.Columns.Add("PName");
                    dt.Columns.Add("Length");
                    dt.Columns.Add("Width");
                    dt.Columns.Add("Height");


                    dt.Columns.Add("Buyers_OrderNo");
                    dt.Columns.Add("BOrderDated");
                    dt.Columns.Add("DNTimeofInv");
                    dt.Columns.Add("MVehicleNo");
                    dt.Columns.Add("DNTimeofRemoval");
                    dt.Columns.Add("Pk_PartPropertyID");
                    dt.Columns.Add("UnitVal");

                    dt.Columns.Add("Despatched");
                    dt.Columns.Add("Destination");
                    dt.Columns.Add("Bundles");

                    decimal NetValue = 0;
                    decimal cgval = 0;
                    decimal ExValue = 0;
                    decimal VatVal = 0;
                    string ttype = "";
                    string ttype1 = "";
                    string Ttax = "";
                    string fct3 = "";
                    //string fh = "";

                    foreach (VwInvoice entity in BillList)
                    {
                        DataRow row = dt.NewRow();

                        row["CustomerName"] = entity.CustomerName;
                        row["CustomerAddress"] = entity.CustomerAddress;
                        row["CityName"] = entity.CityName;
                        row["StateName"] = entity.StateName;
                        row["PinCode"] = entity.PinCode;
                        row["Pk_Invoice"] = entity.Pk_Invoice;
                        row["InvDate"] = entity.InvDate;
                        row["Fk_OrderNo"] = entity.Fk_OrderNo;
                        row["TaxValue"] = entity.TaxValue;
                        row["Quantity"] = entity.Quantity;
                        row["Price"] = entity.Price;
                        row["VAT"] = entity.VAT;
                        row["NetAmount"] = entity.NetAmount;
                        NetValue = Convert.ToDecimal(NetValue + entity.NetAmount);
                        row["Name"] = entity.Name;
                        row["HsnCode"] = entity.HsnCode;
                        row["Pk_Inv_Details"] = entity.Pk_Inv_Details;
                        row["Esugam"] = entity.ESUGAM;
                        row["COA"] = entity.COA;
                        row["CreditLimit"] = entity.CreditLimit;
                        row["HsnCode"] = entity.HsnCode;
                        row["Description"] = entity.Description;

                        row["DeliveryName"] = entity.DeliveryName;
                        row["DeliveryAdd1"] = entity.DeliveryAdd1;
                        row["DeliveryAdd2"] = entity.DeliveryAdd2;


                        row["PName"] = entity.PName;
                        row["Length"] = entity.Length;
                        row["Width"] = entity.Width;
                        row["Height"] = entity.Height;


                        row["Buyers_OrderNo"] = entity.Buyers_OrderNo;
                        row["BOrderDated"] = entity.BOrderDated;
                        row["DNTimeofInv"] = entity.DNTimeofInv;
                        row["MVehicleNo"] = entity.MVehicleNo;
                        row["DNTimeofRemoval"] = entity.DNTimeofRemoval;
                        row["Pk_PartPropertyID"] = entity.Pk_PartPropertyID;

                        row["UnitVal"] = entity.UnitVal;
                        row["LandLine"] = entity.LandLine;
                        row["Despatched"] = entity.Despatched;
                        row["Destination"] = entity.Destination;
                        row["TaxType"] = entity.TaxType;
                        row["Bundles"] = entity.Bundles;
                        if (entity.TaxType.TrimEnd() == "SGST")     ///FOR SGST
                        {
                            Ttax = "V";
                            ttype = "CGST @ 6%";
                            ttype1 = "SGST @ 6%";

                        }
                        else if (entity.TaxType.TrimEnd() == "IGST")   ///FOR IGST
                        {
                            Ttax = "C";
                            ttype = "IGST @ 12 % ";
                        }

                        else if (entity.TaxType.TrimEnd() == "9%")   ///FOR IGST
                        {
                            Ttax = "9";
                            ttype = "CGST @ 9%";
                            ttype1 = "SGST @ 9%";
                        }
                        else if (entity.TaxType.TrimEnd() == "2.5%")   ///FOR IGST
                        {
                            Ttax = "2.5";
                            ttype = "CGST @ 2.5%";
                            ttype1 = "SGST @ 2.5%";
                        }
                        else if (entity.TaxType.TrimEnd() == "5%")   ///FOR IGST
                        {
                            Ttax = "5";
                            ttype = "CGST @ 5%";
                            ttype1 = "SGST @ 5%";
                        }
                        dt.Rows.Add(row);
                    }
                    NetValue = Convert.ToDecimal(NetValue);
                    if (fct3.Length > 0)
                    {
                        ExValue = 0;
                    }
                    else
                    {
                        ExValue = (Convert.ToDecimal(NetValue) * Convert.ToDecimal(0.06));
                    }

                    if (Ttax == "V")
                    {
                        VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.06);
                        cgval = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.06);
                    }
                    else if (Ttax == "C")
                    {
                        VatVal = 0;
                        cgval = 0;
                        VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.12);
                    }
                    else if (Ttax == "9")
                    {
                        VatVal = 0;
                        cgval = 0;
                        VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.09);
                        cgval = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.09);
                    }
                    else if (Ttax == "2.5")
                    {
                        VatVal = 0;
                        cgval = 0;
                        VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.025);
                        cgval = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.025);
                    }
                    else if (Ttax == "5")
                    {
                        VatVal = 0;
                        cgval = 0;
                        VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.05);
                        cgval = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.05);
                    }




                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.Bill1 orp = new CogitoStreamline.Report.Bill1();

                    orp.Load("@\\Report\\Bill1.rpt");
                    orp.SetDataSource(dt.DefaultView);



                    ReportDocument repDoc = orp;

                    ParameterFieldDefinitions crParameterFieldDefinitions3;
                    ParameterFieldDefinition crParameterFieldDefinition3;
                    ParameterValues crParameterValues3 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue3 = new ParameterDiscreteValue();
                    crParameterDiscreteValue3.Value = NetValue;
                    crParameterFieldDefinitions3 = orp.DataDefinition.ParameterFields;
                    crParameterFieldDefinition3 = crParameterFieldDefinitions3["TotAmt"];
                    crParameterValues3 = crParameterFieldDefinition3.CurrentValues;

                    crParameterValues3.Clear();
                    crParameterValues3.Add(crParameterDiscreteValue3);
                    crParameterFieldDefinition3.ApplyCurrentValues(crParameterValues3);



                    ParameterFieldDefinitions crParameterFieldDefinitions4;
                    ParameterFieldDefinition crParameterFieldDefinition4;
                    ParameterValues crParameterValues4 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                    crParameterDiscreteValue4.Value = "Duplicate for Transporter";
                    crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                    crParameterFieldDefinition4 = crParameterFieldDefinitions4["CopyName"];
                    crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                    crParameterValues4.Clear();
                    crParameterValues4.Add(crParameterDiscreteValue4);
                    crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);


                    //ParameterFieldDefinitions crParameterFieldDefinitions5;
                    //ParameterFieldDefinition crParameterFieldDefinition5;
                    //ParameterValues crParameterValues5 = new ParameterValues();
                    //ParameterDiscreteValue crParameterDiscreteValue5 = new ParameterDiscreteValue();
                    //crParameterDiscreteValue5.Value = fct3;
                    //crParameterFieldDefinitions5 = orp.DataDefinition.ParameterFields;
                    //crParameterFieldDefinition5 = crParameterFieldDefinitions4["StrValuefct3"];
                    //crParameterValues5 = crParameterFieldDefinition5.CurrentValues;

                    //crParameterValues5.Clear();
                    //crParameterValues5.Add(crParameterDiscreteValue5);
                    //crParameterFieldDefinition5.ApplyCurrentValues(crParameterValues5);




                    //ParameterFieldDefinitions crParameterFieldDefinitions6;
                    //ParameterFieldDefinition crParameterFieldDefinition6;
                    //ParameterValues crParameterValues6 = new ParameterValues();
                    //ParameterDiscreteValue crParameterDiscreteValue6 = new ParameterDiscreteValue();
                    //crParameterDiscreteValue6.Value = fh;
                    //crParameterFieldDefinitions6 = orp.DataDefinition.ParameterFields;
                    //crParameterFieldDefinition6 = crParameterFieldDefinitions6["StrValueh"];
                    //crParameterValues6 = crParameterFieldDefinition6.CurrentValues;

                    //crParameterValues6.Clear();
                    //crParameterValues6.Add(crParameterDiscreteValue6);
                    //crParameterFieldDefinition6.ApplyCurrentValues(crParameterValues6);


                    ParameterFieldDefinitions crParameterFieldDefinitions7;
                    ParameterFieldDefinition crParameterFieldDefinition7;
                    ParameterValues crParameterValues7 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue7 = new ParameterDiscreteValue();
                    crParameterDiscreteValue7.Value = VatVal;
                    crParameterFieldDefinitions7 = orp.DataDefinition.ParameterFields;
                    crParameterFieldDefinition7 = crParameterFieldDefinitions7["cgst"];
                    crParameterValues7 = crParameterFieldDefinition7.CurrentValues;

                    crParameterValues7.Clear();
                    crParameterValues7.Add(crParameterDiscreteValue7);
                    crParameterFieldDefinition7.ApplyCurrentValues(crParameterValues7);




                    ParameterFieldDefinitions crParameterFieldDefinitions1;
                    ParameterFieldDefinition crParameterFieldDefinition1;
                    ParameterValues crParameterValues1 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue1 = new ParameterDiscreteValue();
                    crParameterDiscreteValue1.Value = VatVal;
                    crParameterFieldDefinitions1 = orp.DataDefinition.ParameterFields;
                    crParameterFieldDefinition1 = crParameterFieldDefinitions1["vat5"];
                    crParameterValues1 = crParameterFieldDefinition1.CurrentValues;

                    crParameterValues1.Clear();
                    crParameterValues1.Add(crParameterDiscreteValue1);
                    crParameterFieldDefinition1.ApplyCurrentValues(crParameterValues1);

                    //ttype


                    ParameterFieldDefinitions crParameterFieldDefinitions12;
                    ParameterFieldDefinition crParameterFieldDefinition12;
                    ParameterValues crParameterValues12 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue12 = new ParameterDiscreteValue();
                    crParameterDiscreteValue12.Value = "OUTPUT " + ttype;
                    crParameterFieldDefinitions12 = orp.DataDefinition.ParameterFields;
                    crParameterFieldDefinition12 = crParameterFieldDefinitions1["TAXT"];
                    crParameterValues12 = crParameterFieldDefinition12.CurrentValues;

                    crParameterValues12.Clear();
                    crParameterValues12.Add(crParameterDiscreteValue12);
                    crParameterFieldDefinition12.ApplyCurrentValues(crParameterValues12);


                    if (ttype1.Length > 0)
                    {
                        ParameterFieldDefinitions crParameterFieldDefinitions6;
                        ParameterFieldDefinition crParameterFieldDefinition6;
                        ParameterValues crParameterValues6 = new ParameterValues();
                        ParameterDiscreteValue crParameterDiscreteValue6 = new ParameterDiscreteValue();
                        crParameterDiscreteValue6.Value = "OUTPUT " + ttype1;
                        crParameterFieldDefinitions6 = orp.DataDefinition.ParameterFields;
                        crParameterFieldDefinition6 = crParameterFieldDefinitions1["TAXT1"];
                        crParameterValues6 = crParameterFieldDefinition6.CurrentValues;

                        crParameterValues6.Clear();
                        crParameterValues6.Add(crParameterDiscreteValue6);
                        crParameterFieldDefinition6.ApplyCurrentValues(crParameterValues6);
                    }
                    else
                    {
                        ParameterFieldDefinitions crParameterFieldDefinitions6;
                        ParameterFieldDefinition crParameterFieldDefinition6;
                        ParameterValues crParameterValues6 = new ParameterValues();
                        ParameterDiscreteValue crParameterDiscreteValue6 = new ParameterDiscreteValue();
                        crParameterDiscreteValue6.Value = ttype1;
                        crParameterFieldDefinitions6 = orp.DataDefinition.ParameterFields;
                        crParameterFieldDefinition6 = crParameterFieldDefinitions1["TAXT1"];
                        crParameterValues6 = crParameterFieldDefinition6.CurrentValues;

                        crParameterValues6.Clear();
                        crParameterValues6.Add(crParameterDiscreteValue6);
                        crParameterFieldDefinition6.ApplyCurrentValues(crParameterValues6);
                    }
                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "DC-" + " " + sInvno + ".pdf");
                    FileInfo file = new FileInfo(pdfPath);
                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();

                }
                else
                {
                    List<Vw_InvRM> BillList1 = new List<Vw_InvRM>();
                    CogitoStreamLineEntities dc1 = new CogitoStreamLineEntities();
                    BillList1 = dc1.Vw_InvRM.Where(x => x.Pk_Invoice == sInvno).Select(x => x).OfType<Vw_InvRM>().ToList();


                    if (BillList1.Count > 0)
                    {
                        //var CtrVal = BillList.Count;
                        DataTable dt1 = new DataTable("InvoiceBill");

                        dt1.Columns.Add("CustomerName");
                        dt1.Columns.Add("CustomerAddress");
                        dt1.Columns.Add("CityName");
                        dt1.Columns.Add("StateName");
                        dt1.Columns.Add("PinCode");
                        dt1.Columns.Add("Pk_Invoice");
                        dt1.Columns.Add("InvDate");
                        dt1.Columns.Add("Fk_OrderNo");
                        dt1.Columns.Add("TaxValue");
                        dt1.Columns.Add("Quantity");
                        dt1.Columns.Add("Price");
                        dt1.Columns.Add("Fk_JobCardID");
                        dt1.Columns.Add("NetAmount");
                        //dt1.Columns.Add("Name");
                        dt1.Columns.Add("Description");
                        dt1.Columns.Add("Pk_Inv_Details");
                        dt1.Columns.Add("Esugam");
                        dt1.Columns.Add("COA");
                        dt1.Columns.Add("CreditLimit");
                        dt1.Columns.Add("HsnCode");
                        dt1.Columns.Add("LandLine");
                        dt1.Columns.Add("VAT");
                        //dt1.Columns.Add("");
                        dt1.Columns.Add("TaxType");

                        dt1.Columns.Add("DeliveryName");
                        dt1.Columns.Add("DeliveryAdd1");
                        dt1.Columns.Add("DeliveryAdd2");

                        dt1.Columns.Add("PName");
                        dt1.Columns.Add("Length");
                        dt1.Columns.Add("Width");
                        dt1.Columns.Add("Height");


                        dt1.Columns.Add("Buyers_OrderNo");
                        dt1.Columns.Add("BOrderDated");
                        dt1.Columns.Add("DNTimeofInv");
                        dt1.Columns.Add("MVehicleNo");
                        dt1.Columns.Add("DNTimeofRemoval");
                        dt1.Columns.Add("Name");
                        dt1.Columns.Add("UnitVal");
                        dt1.Columns.Add("Despatched");
                        dt1.Columns.Add("Destination");


                        decimal NetValue = 0;
                        decimal cgval = 0;
                        decimal ExValue = 0;
                        decimal VatVal = 0;
                        string ttype = "";
                        string ttype1 = "";
                        string Ttax = "";
                        string fct3 = "";
                        //string fh = "";

                        foreach (Vw_InvRM entity in BillList1)
                        {
                            DataRow row = dt1.NewRow();

                            row["CustomerName"] = entity.CustomerName;
                            row["CustomerAddress"] = entity.CustomerAddress;
                            row["CityName"] = entity.CityName;
                            row["StateName"] = entity.StateName;
                            row["PinCode"] = entity.PinCode;
                            row["Pk_Invoice"] = entity.Pk_Invoice;
                            row["InvDate"] = entity.InvDate;
                            row["Fk_OrderNo"] = entity.Fk_OrderNo;
                            row["TaxValue"] = entity.TaxValue;
                            row["Quantity"] = entity.Quantity;
                            row["Price"] = entity.Price;
                            //row["Fk_JobCardID"] = entity.Fk_JobCardID;
                            row["NetAmount"] = entity.NetAmount;
                            NetValue = Convert.ToDecimal(NetValue + entity.NetAmount);
                            //row["Name"] = entity.Name;
                            row["HsnCode"] = entity.HsnCode;
                            row["Pk_Inv_Details"] = entity.Pk_Inv_Details;
                            row["Esugam"] = entity.ESUGAM;
                            row["COA"] = entity.COA;
                            row["CreditLimit"] = entity.CreditLimit;
                            row["HsnCode"] = entity.HsnCode;
                            row["Description"] = entity.Description;

                            row["DeliveryName"] = entity.DeliveryName;
                            row["DeliveryAdd1"] = entity.DeliveryAdd1;
                            row["DeliveryAdd2"] = entity.DeliveryAdd2;


                            //row["PName"] = entity.PName;
                            //row["Length"] = entity.Length;
                            //row["Width"] = entity.Width;
                            //row["Height"] = entity.Height;


                            row["Buyers_OrderNo"] = entity.Buyers_OrderNo;
                            row["BOrderDated"] = entity.BOrderDated;
                            row["DNTimeofInv"] = entity.DNTimeofInv;
                            row["MVehicleNo"] = entity.MVehicleNo;
                            row["DNTimeofRemoval"] = entity.DNTimeofRemoval;
                            row["Name"] = entity.Name;

                            row["UnitVal"] = entity.UnitVal;
                            row["LandLine"] = entity.LandLine;
                            row["VAT"] = entity.VAT;
                            row["TaxType"] = entity.TaxType;

                            row["Despatched"] = entity.Despatched;
                            row["Destination"] = entity.Destination;
                            //row["Bundles"] = entity.Bundles;
                            if (entity.TaxType.TrimEnd() == "SGST")     ///FOR SGST
                            {
                                Ttax = "V";
                                ttype = "CGST @ 6%";
                                ttype1 = "SGST @ 6 %";

                            }
                            else if (entity.TaxType.TrimEnd() == "IGST")   ///FOR IGST
                            {
                                Ttax = "C";
                                ttype = "IGST @ 12 % ";
                            }
                            else if (entity.TaxType.TrimEnd() == "9%")   ///FOR IGST
                            {
                                Ttax = "9";
                                ttype = "CGST @ 9%";
                                ttype1 = "SGST @ 9%";
                            }
                            else if (entity.TaxType.TrimEnd() == "2.5%")   ///FOR IGST
                            {
                                Ttax = "2.5";
                                ttype = "CGST @ 2.5%";
                                ttype1 = "SGST @ 2.5%";
                            }
                            else if (entity.TaxType.TrimEnd() == "5%")   ///FOR IGST
                            {
                                Ttax = "5";
                                ttype = "CGST @ 5%";
                                ttype1 = "SGST @ 5%";
                            }
                            dt1.Rows.Add(row);
                        }
                        NetValue = Convert.ToDecimal(NetValue);
                        if (fct3.Length > 0)
                        {
                            ExValue = 0;
                        }
                        else
                        {
                            ExValue = (Convert.ToDecimal(NetValue) * Convert.ToDecimal(0.06));
                        }

                        if (Ttax == "V")
                        {
                            VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.06);
                            cgval = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.06);
                        }
                        else if (Ttax == "C")
                        {
                            VatVal = 0;
                            cgval = 0;
                            VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.12);
                        }
                        else if (Ttax == "9")
                        {
                            VatVal = 0;
                            cgval = 0;
                            VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.09);
                            cgval = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.09);
                        }
                        else if (Ttax == "2.5")
                        {
                            VatVal = 0;
                            cgval = 0;
                            VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.025);
                            cgval = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.025);
                        }
                        else if (Ttax == "5")
                        {
                            VatVal = 0;
                            cgval = 0;
                            VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.05);
                            cgval = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.05);
                        }





                        DataSet ds = new DataSet();
                        ds.Tables.Add(dt1);
                        CogitoStreamline.Report.BillRM orp = new CogitoStreamline.Report.BillRM();

                        orp.Load("@\\Report\\BillRM.rpt");
                        orp.SetDataSource(dt1.DefaultView);



                        ReportDocument repDoc = orp;

                        ParameterFieldDefinitions crParameterFieldDefinitions3;
                        ParameterFieldDefinition crParameterFieldDefinition3;
                        ParameterValues crParameterValues3 = new ParameterValues();
                        ParameterDiscreteValue crParameterDiscreteValue3 = new ParameterDiscreteValue();
                        crParameterDiscreteValue3.Value = NetValue;
                        crParameterFieldDefinitions3 = orp.DataDefinition.ParameterFields;
                        crParameterFieldDefinition3 = crParameterFieldDefinitions3["TotAmt"];
                        crParameterValues3 = crParameterFieldDefinition3.CurrentValues;

                        crParameterValues3.Clear();
                        crParameterValues3.Add(crParameterDiscreteValue3);
                        crParameterFieldDefinition3.ApplyCurrentValues(crParameterValues3);



                        ParameterFieldDefinitions crParameterFieldDefinitions4;
                        ParameterFieldDefinition crParameterFieldDefinition4;
                        ParameterValues crParameterValues4 = new ParameterValues();
                        ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                        crParameterDiscreteValue4.Value = "Duplicate for Transporter";
                        crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                        crParameterFieldDefinition4 = crParameterFieldDefinitions4["CopyName"];
                        crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                        crParameterValues4.Clear();
                        crParameterValues4.Add(crParameterDiscreteValue4);
                        crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);


                        //ParameterFieldDefinitions crParameterFieldDefinitions5;
                        //ParameterFieldDefinition crParameterFieldDefinition5;
                        //ParameterValues crParameterValues5 = new ParameterValues();
                        //ParameterDiscreteValue crParameterDiscreteValue5 = new ParameterDiscreteValue();
                        //crParameterDiscreteValue5.Value = fct3;
                        //crParameterFieldDefinitions5 = orp.DataDefinition.ParameterFields;
                        //crParameterFieldDefinition5 = crParameterFieldDefinitions4["StrValuefct3"];
                        //crParameterValues5 = crParameterFieldDefinition5.CurrentValues;

                        //crParameterValues5.Clear();
                        //crParameterValues5.Add(crParameterDiscreteValue5);
                        //crParameterFieldDefinition5.ApplyCurrentValues(crParameterValues5);




                        //ParameterFieldDefinitions crParameterFieldDefinitions6;
                        //ParameterFieldDefinition crParameterFieldDefinition6;
                        //ParameterValues crParameterValues6 = new ParameterValues();
                        //ParameterDiscreteValue crParameterDiscreteValue6 = new ParameterDiscreteValue();
                        //crParameterDiscreteValue6.Value = fh;
                        //crParameterFieldDefinitions6 = orp.DataDefinition.ParameterFields;
                        //crParameterFieldDefinition6 = crParameterFieldDefinitions6["StrValueh"];
                        //crParameterValues6 = crParameterFieldDefinition6.CurrentValues;

                        //crParameterValues6.Clear();
                        //crParameterValues6.Add(crParameterDiscreteValue6);
                        //crParameterFieldDefinition6.ApplyCurrentValues(crParameterValues6);


                        ParameterFieldDefinitions crParameterFieldDefinitions7;
                        ParameterFieldDefinition crParameterFieldDefinition7;
                        ParameterValues crParameterValues7 = new ParameterValues();
                        ParameterDiscreteValue crParameterDiscreteValue7 = new ParameterDiscreteValue();
                        crParameterDiscreteValue7.Value = VatVal;
                        crParameterFieldDefinitions7 = orp.DataDefinition.ParameterFields;
                        crParameterFieldDefinition7 = crParameterFieldDefinitions7["cgst"];
                        crParameterValues7 = crParameterFieldDefinition7.CurrentValues;

                        crParameterValues7.Clear();
                        crParameterValues7.Add(crParameterDiscreteValue7);
                        crParameterFieldDefinition7.ApplyCurrentValues(crParameterValues7);




                        ParameterFieldDefinitions crParameterFieldDefinitions1;
                        ParameterFieldDefinition crParameterFieldDefinition1;
                        ParameterValues crParameterValues1 = new ParameterValues();
                        ParameterDiscreteValue crParameterDiscreteValue1 = new ParameterDiscreteValue();
                        crParameterDiscreteValue1.Value = VatVal;
                        crParameterFieldDefinitions1 = orp.DataDefinition.ParameterFields;
                        crParameterFieldDefinition1 = crParameterFieldDefinitions1["vat5"];
                        crParameterValues1 = crParameterFieldDefinition1.CurrentValues;

                        crParameterValues1.Clear();
                        crParameterValues1.Add(crParameterDiscreteValue1);
                        crParameterFieldDefinition1.ApplyCurrentValues(crParameterValues1);

                        //ttype


                        ParameterFieldDefinitions crParameterFieldDefinitions12;
                        ParameterFieldDefinition crParameterFieldDefinition12;
                        ParameterValues crParameterValues12 = new ParameterValues();
                        ParameterDiscreteValue crParameterDiscreteValue12 = new ParameterDiscreteValue();
                        crParameterDiscreteValue12.Value = "OUTPUT " + ttype;
                        crParameterFieldDefinitions12 = orp.DataDefinition.ParameterFields;
                        crParameterFieldDefinition12 = crParameterFieldDefinitions1["TAXT"];
                        crParameterValues12 = crParameterFieldDefinition12.CurrentValues;

                        crParameterValues12.Clear();
                        crParameterValues12.Add(crParameterDiscreteValue12);
                        crParameterFieldDefinition12.ApplyCurrentValues(crParameterValues12);


                        if (ttype1.Length > 0)
                        {
                            ParameterFieldDefinitions crParameterFieldDefinitions6;
                            ParameterFieldDefinition crParameterFieldDefinition6;
                            ParameterValues crParameterValues6 = new ParameterValues();
                            ParameterDiscreteValue crParameterDiscreteValue6 = new ParameterDiscreteValue();
                            crParameterDiscreteValue6.Value = "OUTPUT " + ttype1;
                            crParameterFieldDefinitions6 = orp.DataDefinition.ParameterFields;
                            crParameterFieldDefinition6 = crParameterFieldDefinitions1["TAXT1"];
                            crParameterValues6 = crParameterFieldDefinition6.CurrentValues;

                            crParameterValues6.Clear();
                            crParameterValues6.Add(crParameterDiscreteValue6);
                            crParameterFieldDefinition6.ApplyCurrentValues(crParameterValues6);
                        }
                        else
                        {
                            ParameterFieldDefinitions crParameterFieldDefinitions6;
                            ParameterFieldDefinition crParameterFieldDefinition6;
                            ParameterValues crParameterValues6 = new ParameterValues();
                            ParameterDiscreteValue crParameterDiscreteValue6 = new ParameterDiscreteValue();
                            crParameterDiscreteValue6.Value = ttype1;
                            crParameterFieldDefinitions6 = orp.DataDefinition.ParameterFields;
                            crParameterFieldDefinition6 = crParameterFieldDefinitions1["TAXT1"];
                            crParameterValues6 = crParameterFieldDefinition6.CurrentValues;

                            crParameterValues6.Clear();
                            crParameterValues6.Add(crParameterDiscreteValue6);
                            crParameterFieldDefinition6.ApplyCurrentValues(crParameterValues6);
                        }
                        string pdfPath = Server.MapPath("~/ConvertPDF/" + "DC-" + " " + sInvno + ".pdf");
                        FileInfo file = new FileInfo(pdfPath);
                        if (file.Exists)
                        {
                            file.Delete();
                        }
                        var pd = new PrintDocument();


                        orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                        orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                        DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                        objDiskOpt.DiskFileName = pdfPath;

                        orp.ExportOptions.DestinationOptions = objDiskOpt;
                        orp.Export();
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
                //  Response.Redirect("Index");
            }
        }

        public ActionResult InvReportTriplicate(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["Invno"]);

                List<VwInvoice> BillList = new List<VwInvoice>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                //BillList = dc.VwInvoices.ToList();
                BillList = dc.VwInvoice.Where(x => x.Pk_Invoice == sInvno).Select(x => x).OfType<VwInvoice>().ToList();

                if (BillList.Count > 0)
                {
                    //var CtrVal = BillList.Count;
                    DataTable dt = new DataTable("InvoiceBill");

                    dt.Columns.Add("CustomerName");
                    dt.Columns.Add("CustomerAddress");
                    dt.Columns.Add("CityName");
                    dt.Columns.Add("StateName");
                    dt.Columns.Add("PinCode");
                    dt.Columns.Add("Pk_Invoice");
                    dt.Columns.Add("InvDate");
                    dt.Columns.Add("Fk_OrderNo");
                    dt.Columns.Add("TaxValue");
                    dt.Columns.Add("Quantity");
                    dt.Columns.Add("Price");
                    dt.Columns.Add("Fk_JobCardID");
                    dt.Columns.Add("NetAmount");
                    dt.Columns.Add("Name");
                    dt.Columns.Add("Description");
                    dt.Columns.Add("Pk_Inv_Details");
                    dt.Columns.Add("Esugam");
                    dt.Columns.Add("COA");
                    dt.Columns.Add("CreditLimit");
                    dt.Columns.Add("HsnCode");
                    dt.Columns.Add("LandLine");
                    dt.Columns.Add("VAT");
                    //dt.Columns.Add("");
                    dt.Columns.Add("TaxType");

                    dt.Columns.Add("DeliveryName");
                    dt.Columns.Add("DeliveryAdd1");
                    dt.Columns.Add("DeliveryAdd2");

                    dt.Columns.Add("PName");
                    dt.Columns.Add("Length");
                    dt.Columns.Add("Width");
                    dt.Columns.Add("Height");


                    dt.Columns.Add("Buyers_OrderNo");
                    dt.Columns.Add("BOrderDated");
                    dt.Columns.Add("DNTimeofInv");
                    dt.Columns.Add("MVehicleNo");
                    dt.Columns.Add("DNTimeofRemoval");
                    dt.Columns.Add("Pk_PartPropertyID");
                    dt.Columns.Add("UnitVal");


                    dt.Columns.Add("Despatched");
                    dt.Columns.Add("Destination");
                    dt.Columns.Add("Bundles");

                    decimal NetValue = 0;
                    decimal cgval = 0;
                    decimal ExValue = 0;
                    decimal VatVal = 0;
                    string ttype = "";
                    string ttype1 = "";
                    string Ttax = "";
                    string fct3 = "";
                    //string fh = "";

                    foreach (VwInvoice entity in BillList)
                    {
                        DataRow row = dt.NewRow();

                        row["CustomerName"] = entity.CustomerName;
                        row["CustomerAddress"] = entity.CustomerAddress;
                        row["CityName"] = entity.CityName;
                        row["StateName"] = entity.StateName;
                        row["PinCode"] = entity.PinCode;
                        row["Pk_Invoice"] = entity.Pk_Invoice;
                        row["InvDate"] = entity.InvDate;
                        row["Fk_OrderNo"] = entity.Fk_OrderNo;
                        row["TaxValue"] = entity.TaxValue;
                        row["Quantity"] = entity.Quantity;
                        row["Price"] = entity.Price;
                        row["VAT"] = entity.VAT;
                        row["NetAmount"] = entity.NetAmount;
                        NetValue = Convert.ToDecimal(NetValue + entity.NetAmount);
                        row["Name"] = entity.Name;
                        row["HsnCode"] = entity.HsnCode;
                        row["Pk_Inv_Details"] = entity.Pk_Inv_Details;
                        row["Esugam"] = entity.ESUGAM;
                        row["COA"] = entity.COA;
                        row["CreditLimit"] = entity.CreditLimit;
                        row["HsnCode"] = entity.HsnCode;
                        row["Description"] = entity.Description;

                        row["DeliveryName"] = entity.DeliveryName;
                        row["DeliveryAdd1"] = entity.DeliveryAdd1;
                        row["DeliveryAdd2"] = entity.DeliveryAdd2;


                        row["PName"] = entity.PName;
                        row["Length"] = entity.Length;
                        row["Width"] = entity.Width;
                        row["Height"] = entity.Height;


                        row["Buyers_OrderNo"] = entity.Buyers_OrderNo;
                        row["BOrderDated"] = entity.BOrderDated;
                        row["DNTimeofInv"] = entity.DNTimeofInv;
                        row["MVehicleNo"] = entity.MVehicleNo;
                        row["DNTimeofRemoval"] = entity.DNTimeofRemoval;
                        row["Pk_PartPropertyID"] = entity.Pk_PartPropertyID;

                        row["UnitVal"] = entity.UnitVal;
                        row["LandLine"] = entity.LandLine;

                        row["TaxType"] = entity.TaxType;


                        row["Despatched"] = entity.Despatched;
                        row["Destination"] = entity.Destination;
                        row["Bundles"] = entity.Bundles;

                        if (entity.TaxType.TrimEnd() == "SGST")     ///FOR SGST
                        {
                            Ttax = "V";
                            ttype = "CGST @ 6%";
                            ttype1 = "SGST @ 6%";

                        }
                        else if (entity.TaxType.TrimEnd() == "IGST")   ///FOR IGST
                        {
                            Ttax = "C";
                            ttype = "IGST @ 12 % ";
                        }

                        else if (entity.TaxType.TrimEnd() == "9%")   ///FOR IGST
                        {
                            Ttax = "9";
                            ttype = "CGST @ 9%";
                            ttype1 = "SGST @ 9%";
                        }
                        else if (entity.TaxType.TrimEnd() == "2.5%")   ///FOR IGST
                        {
                            Ttax = "2.5";
                            ttype = "CGST @ 2.5%";
                            ttype1 = "SGST @ 2.5%";
                        }
                        else if (entity.TaxType.TrimEnd() == "5%")   ///FOR IGST
                        {
                            Ttax = "5";
                            ttype = "CGST @ 5%";
                            ttype1 = "SGST @ 5%";
                        }
                        dt.Rows.Add(row);
                    }
                    NetValue = Convert.ToDecimal(NetValue);
                    if (fct3.Length > 0)
                    {
                        ExValue = 0;
                    }
                    else
                    {
                        ExValue = (Convert.ToDecimal(NetValue) * Convert.ToDecimal(0.06));
                    }

                    if (Ttax == "V")
                    {
                        VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.06);
                        cgval = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.06);
                    }
                    else if (Ttax == "C")
                    {
                        VatVal = 0;
                        cgval = 0;
                        VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.12);
                    }
                    else if (Ttax == "9")
                    {
                        VatVal = 0;
                        cgval = 0;
                        VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.09);
                        cgval = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.09);
                    }
                    else if (Ttax == "2.5")
                    {
                        VatVal = 0;
                        cgval = 0;
                        VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.025);
                        cgval = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.025);
                    }
                    else if (Ttax == "5")
                    {
                        VatVal = 0;
                        cgval = 0;
                        VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.05);
                        cgval = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.05);
                    }




                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.Bill1 orp = new CogitoStreamline.Report.Bill1();

                    orp.Load("@\\Report\\Bill1.rpt");
                    orp.SetDataSource(dt.DefaultView);



                    ReportDocument repDoc = orp;

                    ParameterFieldDefinitions crParameterFieldDefinitions3;
                    ParameterFieldDefinition crParameterFieldDefinition3;
                    ParameterValues crParameterValues3 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue3 = new ParameterDiscreteValue();
                    crParameterDiscreteValue3.Value = NetValue;
                    crParameterFieldDefinitions3 = orp.DataDefinition.ParameterFields;
                    crParameterFieldDefinition3 = crParameterFieldDefinitions3["TotAmt"];
                    crParameterValues3 = crParameterFieldDefinition3.CurrentValues;

                    crParameterValues3.Clear();
                    crParameterValues3.Add(crParameterDiscreteValue3);
                    crParameterFieldDefinition3.ApplyCurrentValues(crParameterValues3);



                    ParameterFieldDefinitions crParameterFieldDefinitions4;
                    ParameterFieldDefinition crParameterFieldDefinition4;
                    ParameterValues crParameterValues4 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                    crParameterDiscreteValue4.Value = "Triplicate for Supplier";
                    crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                    crParameterFieldDefinition4 = crParameterFieldDefinitions4["CopyName"];
                    crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                    crParameterValues4.Clear();
                    crParameterValues4.Add(crParameterDiscreteValue4);
                    crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);


                    //ParameterFieldDefinitions crParameterFieldDefinitions5;
                    //ParameterFieldDefinition crParameterFieldDefinition5;
                    //ParameterValues crParameterValues5 = new ParameterValues();
                    //ParameterDiscreteValue crParameterDiscreteValue5 = new ParameterDiscreteValue();
                    //crParameterDiscreteValue5.Value = fct3;
                    //crParameterFieldDefinitions5 = orp.DataDefinition.ParameterFields;
                    //crParameterFieldDefinition5 = crParameterFieldDefinitions4["StrValuefct3"];
                    //crParameterValues5 = crParameterFieldDefinition5.CurrentValues;

                    //crParameterValues5.Clear();
                    //crParameterValues5.Add(crParameterDiscreteValue5);
                    //crParameterFieldDefinition5.ApplyCurrentValues(crParameterValues5);




                    //ParameterFieldDefinitions crParameterFieldDefinitions6;
                    //ParameterFieldDefinition crParameterFieldDefinition6;
                    //ParameterValues crParameterValues6 = new ParameterValues();
                    //ParameterDiscreteValue crParameterDiscreteValue6 = new ParameterDiscreteValue();
                    //crParameterDiscreteValue6.Value = fh;
                    //crParameterFieldDefinitions6 = orp.DataDefinition.ParameterFields;
                    //crParameterFieldDefinition6 = crParameterFieldDefinitions6["StrValueh"];
                    //crParameterValues6 = crParameterFieldDefinition6.CurrentValues;

                    //crParameterValues6.Clear();
                    //crParameterValues6.Add(crParameterDiscreteValue6);
                    //crParameterFieldDefinition6.ApplyCurrentValues(crParameterValues6);


                    ParameterFieldDefinitions crParameterFieldDefinitions7;
                    ParameterFieldDefinition crParameterFieldDefinition7;
                    ParameterValues crParameterValues7 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue7 = new ParameterDiscreteValue();
                    crParameterDiscreteValue7.Value = VatVal;
                    crParameterFieldDefinitions7 = orp.DataDefinition.ParameterFields;
                    crParameterFieldDefinition7 = crParameterFieldDefinitions7["cgst"];
                    crParameterValues7 = crParameterFieldDefinition7.CurrentValues;

                    crParameterValues7.Clear();
                    crParameterValues7.Add(crParameterDiscreteValue7);
                    crParameterFieldDefinition7.ApplyCurrentValues(crParameterValues7);




                    ParameterFieldDefinitions crParameterFieldDefinitions1;
                    ParameterFieldDefinition crParameterFieldDefinition1;
                    ParameterValues crParameterValues1 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue1 = new ParameterDiscreteValue();
                    crParameterDiscreteValue1.Value = VatVal;
                    crParameterFieldDefinitions1 = orp.DataDefinition.ParameterFields;
                    crParameterFieldDefinition1 = crParameterFieldDefinitions1["vat5"];
                    crParameterValues1 = crParameterFieldDefinition1.CurrentValues;

                    crParameterValues1.Clear();
                    crParameterValues1.Add(crParameterDiscreteValue1);
                    crParameterFieldDefinition1.ApplyCurrentValues(crParameterValues1);

                    //ttype


                    ParameterFieldDefinitions crParameterFieldDefinitions12;
                    ParameterFieldDefinition crParameterFieldDefinition12;
                    ParameterValues crParameterValues12 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue12 = new ParameterDiscreteValue();
                    crParameterDiscreteValue12.Value = "OUTPUT " + ttype;
                    crParameterFieldDefinitions12 = orp.DataDefinition.ParameterFields;
                    crParameterFieldDefinition12 = crParameterFieldDefinitions1["TAXT"];
                    crParameterValues12 = crParameterFieldDefinition12.CurrentValues;

                    crParameterValues12.Clear();
                    crParameterValues12.Add(crParameterDiscreteValue12);
                    crParameterFieldDefinition12.ApplyCurrentValues(crParameterValues12);


                    if (ttype1.Length > 0)
                    {
                        ParameterFieldDefinitions crParameterFieldDefinitions6;
                        ParameterFieldDefinition crParameterFieldDefinition6;
                        ParameterValues crParameterValues6 = new ParameterValues();
                        ParameterDiscreteValue crParameterDiscreteValue6 = new ParameterDiscreteValue();
                        crParameterDiscreteValue6.Value = "OUTPUT " + ttype1;
                        crParameterFieldDefinitions6 = orp.DataDefinition.ParameterFields;
                        crParameterFieldDefinition6 = crParameterFieldDefinitions1["TAXT1"];
                        crParameterValues6 = crParameterFieldDefinition6.CurrentValues;

                        crParameterValues6.Clear();
                        crParameterValues6.Add(crParameterDiscreteValue6);
                        crParameterFieldDefinition6.ApplyCurrentValues(crParameterValues6);
                    }
                    else
                    {
                        ParameterFieldDefinitions crParameterFieldDefinitions6;
                        ParameterFieldDefinition crParameterFieldDefinition6;
                        ParameterValues crParameterValues6 = new ParameterValues();
                        ParameterDiscreteValue crParameterDiscreteValue6 = new ParameterDiscreteValue();
                        crParameterDiscreteValue6.Value = ttype1;
                        crParameterFieldDefinitions6 = orp.DataDefinition.ParameterFields;
                        crParameterFieldDefinition6 = crParameterFieldDefinitions1["TAXT1"];
                        crParameterValues6 = crParameterFieldDefinition6.CurrentValues;

                        crParameterValues6.Clear();
                        crParameterValues6.Add(crParameterDiscreteValue6);
                        crParameterFieldDefinition6.ApplyCurrentValues(crParameterValues6);
                    }
                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "Triplicate-" + " " + sInvno + ".pdf");
                    FileInfo file = new FileInfo(pdfPath);
                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();

                }
                else
                {
                    List<Vw_InvRM> BillList1 = new List<Vw_InvRM>();
                    CogitoStreamLineEntities dc1 = new CogitoStreamLineEntities();
                    BillList1 = dc1.Vw_InvRM.Where(x => x.Pk_Invoice == sInvno).Select(x => x).OfType<Vw_InvRM>().ToList();


                    if (BillList1.Count > 0)
                    {
                        //var CtrVal = BillList.Count;
                        DataTable dt1 = new DataTable("InvoiceBill");

                        dt1.Columns.Add("CustomerName");
                        dt1.Columns.Add("CustomerAddress");
                        dt1.Columns.Add("CityName");
                        dt1.Columns.Add("StateName");
                        dt1.Columns.Add("PinCode");
                        dt1.Columns.Add("Pk_Invoice");
                        dt1.Columns.Add("InvDate");
                        dt1.Columns.Add("Fk_OrderNo");
                        dt1.Columns.Add("TaxValue");
                        dt1.Columns.Add("Quantity");
                        dt1.Columns.Add("Price");
                        dt1.Columns.Add("Fk_JobCardID");
                        dt1.Columns.Add("NetAmount");
                        //dt1.Columns.Add("Name");
                        dt1.Columns.Add("Description");
                        dt1.Columns.Add("Pk_Inv_Details");
                        dt1.Columns.Add("Esugam");
                        dt1.Columns.Add("COA");
                        dt1.Columns.Add("CreditLimit");
                        dt1.Columns.Add("HsnCode");
                        dt1.Columns.Add("LandLine");
                        dt1.Columns.Add("VAT");
                        //dt1.Columns.Add("");
                        dt1.Columns.Add("TaxType");

                        dt1.Columns.Add("DeliveryName");
                        dt1.Columns.Add("DeliveryAdd1");
                        dt1.Columns.Add("DeliveryAdd2");

                        dt1.Columns.Add("PName");
                        dt1.Columns.Add("Length");
                        dt1.Columns.Add("Width");
                        dt1.Columns.Add("Height");


                        dt1.Columns.Add("Buyers_OrderNo");
                        dt1.Columns.Add("BOrderDated");
                        dt1.Columns.Add("DNTimeofInv");
                        dt1.Columns.Add("MVehicleNo");
                        dt1.Columns.Add("DNTimeofRemoval");
                        dt1.Columns.Add("Name");
                        dt1.Columns.Add("UnitVal");
                        dt1.Columns.Add("Despatched");
                        dt1.Columns.Add("Destination");


                        decimal NetValue = 0;
                        decimal cgval = 0;
                        decimal ExValue = 0;
                        decimal VatVal = 0;
                        string ttype = "";
                        string ttype1 = "";
                        string Ttax = "";
                        string fct3 = "";
                        //string fh = "";

                        foreach (Vw_InvRM entity in BillList1)
                        {
                            DataRow row = dt1.NewRow();

                            row["CustomerName"] = entity.CustomerName;
                            row["CustomerAddress"] = entity.CustomerAddress;
                            row["CityName"] = entity.CityName;
                            row["StateName"] = entity.StateName;
                            row["PinCode"] = entity.PinCode;
                            row["Pk_Invoice"] = entity.Pk_Invoice;
                            row["InvDate"] = entity.InvDate;
                            row["Fk_OrderNo"] = entity.Fk_OrderNo;
                            row["TaxValue"] = entity.TaxValue;
                            row["Quantity"] = entity.Quantity;
                            row["Price"] = entity.Price;
                            //row["Fk_JobCardID"] = entity.Fk_JobCardID;
                            row["NetAmount"] = entity.NetAmount;
                            NetValue = Convert.ToDecimal(NetValue + entity.NetAmount);
                            //row["Name"] = entity.Name;
                            row["HsnCode"] = entity.HsnCode;
                            row["Pk_Inv_Details"] = entity.Pk_Inv_Details;
                            row["Esugam"] = entity.ESUGAM;
                            row["COA"] = entity.COA;
                            row["CreditLimit"] = entity.CreditLimit;
                            row["HsnCode"] = entity.HsnCode;
                            row["Description"] = entity.Description;

                            row["DeliveryName"] = entity.DeliveryName;
                            row["DeliveryAdd1"] = entity.DeliveryAdd1;
                            row["DeliveryAdd2"] = entity.DeliveryAdd2;


                            //row["PName"] = entity.PName;
                            //row["Length"] = entity.Length;
                            //row["Width"] = entity.Width;
                            //row["Height"] = entity.Height;


                            row["Buyers_OrderNo"] = entity.Buyers_OrderNo;
                            row["BOrderDated"] = entity.BOrderDated;
                            row["DNTimeofInv"] = entity.DNTimeofInv;
                            row["MVehicleNo"] = entity.MVehicleNo;
                            row["DNTimeofRemoval"] = entity.DNTimeofRemoval;
                            row["Name"] = entity.Name;

                            row["UnitVal"] = entity.UnitVal;
                            row["LandLine"] = entity.LandLine;

                            row["TaxType"] = entity.TaxType;
                            row["VAT"] = entity.VAT;
                            row["Despatched"] = entity.Despatched;
                            row["Destination"] = entity.Destination;
                            //row["Bundles"] = entity.Bundles;

                            if (entity.TaxType.TrimEnd() == "SGST")     ///FOR SGST
                            {
                                Ttax = "V";
                                ttype = "CGST @ 6%";
                                ttype1 = "SGST @ 6 %";

                            }
                            else if (entity.TaxType.TrimEnd() == "IGST")   ///FOR IGST
                            {
                                Ttax = "C";
                                ttype = "IGST @ 12 % ";
                            }
                            else if (entity.TaxType.TrimEnd() == "9%")   ///FOR IGST
                            {
                                Ttax = "9";
                                ttype = "CGST @ 9%";
                                ttype1 = "SGST @ 9%";
                            }
                            else if (entity.TaxType.TrimEnd() == "2.5%")   ///FOR IGST
                            {
                                Ttax = "2.5";
                                ttype = "CGST @ 2.5%";
                                ttype1 = "SGST @ 2.5%";
                            }
                            else if (entity.TaxType.TrimEnd() == "5%")   ///FOR IGST
                            {
                                Ttax = "5";
                                ttype = "CGST @ 5%";
                                ttype1 = "SGST @ 5%";
                            }
                            dt1.Rows.Add(row);
                        }
                        NetValue = Convert.ToDecimal(NetValue);
                        if (fct3.Length > 0)
                        {
                            ExValue = 0;
                        }
                        else
                        {
                            ExValue = (Convert.ToDecimal(NetValue) * Convert.ToDecimal(0.06));
                        }

                        if (Ttax == "V")
                        {
                            VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.06);
                            cgval = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.06);
                        }
                        else if (Ttax == "C")
                        {
                            VatVal = 0;
                            cgval = 0;
                            VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.12);
                        }
                        else if (Ttax == "9")
                        {
                            VatVal = 0;
                            cgval = 0;
                            VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.09);
                            cgval = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.09);
                        }
                        else if (Ttax == "2.5")
                        {
                            VatVal = 0;
                            cgval = 0;
                            VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.025);
                            cgval = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.025);
                        }
                        else if (Ttax == "5")
                        {
                            VatVal = 0;
                            cgval = 0;
                            VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.05);
                            cgval = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.05);
                        }





                        DataSet ds = new DataSet();
                        ds.Tables.Add(dt1);
                        CogitoStreamline.Report.BillRM orp = new CogitoStreamline.Report.BillRM();

                        orp.Load("@\\Report\\BillRM.rpt");
                        orp.SetDataSource(dt1.DefaultView);



                        ReportDocument repDoc = orp;

                        ParameterFieldDefinitions crParameterFieldDefinitions3;
                        ParameterFieldDefinition crParameterFieldDefinition3;
                        ParameterValues crParameterValues3 = new ParameterValues();
                        ParameterDiscreteValue crParameterDiscreteValue3 = new ParameterDiscreteValue();
                        crParameterDiscreteValue3.Value = NetValue;
                        crParameterFieldDefinitions3 = orp.DataDefinition.ParameterFields;
                        crParameterFieldDefinition3 = crParameterFieldDefinitions3["TotAmt"];
                        crParameterValues3 = crParameterFieldDefinition3.CurrentValues;

                        crParameterValues3.Clear();
                        crParameterValues3.Add(crParameterDiscreteValue3);
                        crParameterFieldDefinition3.ApplyCurrentValues(crParameterValues3);



                        ParameterFieldDefinitions crParameterFieldDefinitions4;
                        ParameterFieldDefinition crParameterFieldDefinition4;
                        ParameterValues crParameterValues4 = new ParameterValues();
                        ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                        crParameterDiscreteValue4.Value = "Triplicate for Supplier";
                        crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                        crParameterFieldDefinition4 = crParameterFieldDefinitions4["CopyName"];
                        crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                        crParameterValues4.Clear();
                        crParameterValues4.Add(crParameterDiscreteValue4);
                        crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);


                        //ParameterFieldDefinitions crParameterFieldDefinitions5;
                        //ParameterFieldDefinition crParameterFieldDefinition5;
                        //ParameterValues crParameterValues5 = new ParameterValues();
                        //ParameterDiscreteValue crParameterDiscreteValue5 = new ParameterDiscreteValue();
                        //crParameterDiscreteValue5.Value = fct3;
                        //crParameterFieldDefinitions5 = orp.DataDefinition.ParameterFields;
                        //crParameterFieldDefinition5 = crParameterFieldDefinitions4["StrValuefct3"];
                        //crParameterValues5 = crParameterFieldDefinition5.CurrentValues;

                        //crParameterValues5.Clear();
                        //crParameterValues5.Add(crParameterDiscreteValue5);
                        //crParameterFieldDefinition5.ApplyCurrentValues(crParameterValues5);




                        //ParameterFieldDefinitions crParameterFieldDefinitions6;
                        //ParameterFieldDefinition crParameterFieldDefinition6;
                        //ParameterValues crParameterValues6 = new ParameterValues();
                        //ParameterDiscreteValue crParameterDiscreteValue6 = new ParameterDiscreteValue();
                        //crParameterDiscreteValue6.Value = fh;
                        //crParameterFieldDefinitions6 = orp.DataDefinition.ParameterFields;
                        //crParameterFieldDefinition6 = crParameterFieldDefinitions6["StrValueh"];
                        //crParameterValues6 = crParameterFieldDefinition6.CurrentValues;

                        //crParameterValues6.Clear();
                        //crParameterValues6.Add(crParameterDiscreteValue6);
                        //crParameterFieldDefinition6.ApplyCurrentValues(crParameterValues6);


                        ParameterFieldDefinitions crParameterFieldDefinitions7;
                        ParameterFieldDefinition crParameterFieldDefinition7;
                        ParameterValues crParameterValues7 = new ParameterValues();
                        ParameterDiscreteValue crParameterDiscreteValue7 = new ParameterDiscreteValue();
                        crParameterDiscreteValue7.Value = VatVal;
                        crParameterFieldDefinitions7 = orp.DataDefinition.ParameterFields;
                        crParameterFieldDefinition7 = crParameterFieldDefinitions7["cgst"];
                        crParameterValues7 = crParameterFieldDefinition7.CurrentValues;

                        crParameterValues7.Clear();
                        crParameterValues7.Add(crParameterDiscreteValue7);
                        crParameterFieldDefinition7.ApplyCurrentValues(crParameterValues7);




                        ParameterFieldDefinitions crParameterFieldDefinitions1;
                        ParameterFieldDefinition crParameterFieldDefinition1;
                        ParameterValues crParameterValues1 = new ParameterValues();
                        ParameterDiscreteValue crParameterDiscreteValue1 = new ParameterDiscreteValue();
                        crParameterDiscreteValue1.Value = VatVal;
                        crParameterFieldDefinitions1 = orp.DataDefinition.ParameterFields;
                        crParameterFieldDefinition1 = crParameterFieldDefinitions1["vat5"];
                        crParameterValues1 = crParameterFieldDefinition1.CurrentValues;

                        crParameterValues1.Clear();
                        crParameterValues1.Add(crParameterDiscreteValue1);
                        crParameterFieldDefinition1.ApplyCurrentValues(crParameterValues1);

                        //ttype


                        ParameterFieldDefinitions crParameterFieldDefinitions12;
                        ParameterFieldDefinition crParameterFieldDefinition12;
                        ParameterValues crParameterValues12 = new ParameterValues();
                        ParameterDiscreteValue crParameterDiscreteValue12 = new ParameterDiscreteValue();
                        crParameterDiscreteValue12.Value = "OUTPUT " + ttype;
                        crParameterFieldDefinitions12 = orp.DataDefinition.ParameterFields;
                        crParameterFieldDefinition12 = crParameterFieldDefinitions1["TAXT"];
                        crParameterValues12 = crParameterFieldDefinition12.CurrentValues;

                        crParameterValues12.Clear();
                        crParameterValues12.Add(crParameterDiscreteValue12);
                        crParameterFieldDefinition12.ApplyCurrentValues(crParameterValues12);


                        if (ttype1.Length > 0)
                        {
                            ParameterFieldDefinitions crParameterFieldDefinitions6;
                            ParameterFieldDefinition crParameterFieldDefinition6;
                            ParameterValues crParameterValues6 = new ParameterValues();
                            ParameterDiscreteValue crParameterDiscreteValue6 = new ParameterDiscreteValue();
                            crParameterDiscreteValue6.Value = "OUTPUT " + ttype1;
                            crParameterFieldDefinitions6 = orp.DataDefinition.ParameterFields;
                            crParameterFieldDefinition6 = crParameterFieldDefinitions1["TAXT1"];
                            crParameterValues6 = crParameterFieldDefinition6.CurrentValues;

                            crParameterValues6.Clear();
                            crParameterValues6.Add(crParameterDiscreteValue6);
                            crParameterFieldDefinition6.ApplyCurrentValues(crParameterValues6);
                        }
                        else
                        {
                            ParameterFieldDefinitions crParameterFieldDefinitions6;
                            ParameterFieldDefinition crParameterFieldDefinition6;
                            ParameterValues crParameterValues6 = new ParameterValues();
                            ParameterDiscreteValue crParameterDiscreteValue6 = new ParameterDiscreteValue();
                            crParameterDiscreteValue6.Value = ttype1;
                            crParameterFieldDefinitions6 = orp.DataDefinition.ParameterFields;
                            crParameterFieldDefinition6 = crParameterFieldDefinitions1["TAXT1"];
                            crParameterValues6 = crParameterFieldDefinition6.CurrentValues;

                            crParameterValues6.Clear();
                            crParameterValues6.Add(crParameterDiscreteValue6);
                            crParameterFieldDefinition6.ApplyCurrentValues(crParameterValues6);
                        }
                        string pdfPath = Server.MapPath("~/ConvertPDF/" + "Triplicate-" + " " + sInvno + ".pdf");
                        FileInfo file = new FileInfo(pdfPath);
                        if (file.Exists)
                        {
                            file.Delete();
                        }
                        var pd = new PrintDocument();


                        orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                        orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                        DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                        objDiskOpt.DiskFileName = pdfPath;

                        orp.ExportOptions.DestinationOptions = objDiskOpt;
                        orp.Export();
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
                //  Response.Redirect("Index");
            }
        }



        public ActionResult InvReportExtra(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["Invno"]);

                List<VwInvoice> BillList = new List<VwInvoice>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                //BillList = dc.VwInvoices.ToList();
                BillList = dc.VwInvoice.Where(x => x.Pk_Invoice == sInvno).Select(x => x).OfType<VwInvoice>().ToList();

                if (BillList.Count > 0)
                {
                    //var CtrVal = BillList.Count;
                    DataTable dt = new DataTable("InvoiceBill");

                    dt.Columns.Add("CustomerName");
                    dt.Columns.Add("CustomerAddress");
                    dt.Columns.Add("CityName");
                    dt.Columns.Add("StateName");
                    dt.Columns.Add("PinCode");
                    dt.Columns.Add("Pk_Invoice");
                    dt.Columns.Add("InvDate");
                    dt.Columns.Add("Fk_OrderNo");
                    dt.Columns.Add("TaxValue");
                    dt.Columns.Add("Quantity");
                    dt.Columns.Add("Price");
                    dt.Columns.Add("Fk_JobCardID");
                    dt.Columns.Add("NetAmount");
                    dt.Columns.Add("Name");
                    dt.Columns.Add("Description");
                    dt.Columns.Add("Pk_Inv_Details");
                    dt.Columns.Add("Esugam");
                    dt.Columns.Add("COA");
                    dt.Columns.Add("CreditLimit");
                    dt.Columns.Add("HsnCode");
                    dt.Columns.Add("LandLine");
                    dt.Columns.Add("VAT");
                    //dt.Columns.Add("");
                    dt.Columns.Add("TaxType");

                    dt.Columns.Add("DeliveryName");
                    dt.Columns.Add("DeliveryAdd1");
                    dt.Columns.Add("DeliveryAdd2");

                    dt.Columns.Add("PName");
                    dt.Columns.Add("Length");
                    dt.Columns.Add("Width");
                    dt.Columns.Add("Height");


                    dt.Columns.Add("Buyers_OrderNo");
                    dt.Columns.Add("BOrderDated");
                    dt.Columns.Add("DNTimeofInv");
                    dt.Columns.Add("MVehicleNo");
                    dt.Columns.Add("DNTimeofRemoval");
                    dt.Columns.Add("Pk_PartPropertyID");
                    dt.Columns.Add("UnitVal");
                    dt.Columns.Add("Despatched");
                    dt.Columns.Add("Destination");
                    dt.Columns.Add("Bundles");



                    decimal NetValue = 0;
                    decimal cgval = 0;
                    decimal ExValue = 0;
                    decimal VatVal = 0;
                    string ttype = "";
                    string ttype1 = "";
                    string Ttax = "";
                    string fct3 = "";
                    //string fh = "";

                    foreach (VwInvoice entity in BillList)
                    {
                        DataRow row = dt.NewRow();

                        row["CustomerName"] = entity.CustomerName;
                        row["CustomerAddress"] = entity.CustomerAddress;
                        row["CityName"] = entity.CityName;
                        row["StateName"] = entity.StateName;
                        row["PinCode"] = entity.PinCode;
                        row["Pk_Invoice"] = entity.Pk_Invoice;
                        row["InvDate"] = entity.InvDate;
                        row["Fk_OrderNo"] = entity.Fk_OrderNo;
                        row["TaxValue"] = entity.TaxValue;
                        row["Quantity"] = entity.Quantity;
                        row["Price"] = entity.Price;
                        row["VAT"] = entity.VAT;
                        row["NetAmount"] = entity.NetAmount;
                        NetValue = Convert.ToDecimal(NetValue + entity.NetAmount);
                        row["Name"] = entity.Name;
                        row["HsnCode"] = entity.HsnCode;
                        row["Pk_Inv_Details"] = entity.Pk_Inv_Details;
                        row["Esugam"] = entity.ESUGAM;
                        row["COA"] = entity.COA;
                        row["CreditLimit"] = entity.CreditLimit;
                        row["HsnCode"] = entity.HsnCode;
                        row["Description"] = entity.Description;

                        row["DeliveryName"] = entity.DeliveryName;
                        row["DeliveryAdd1"] = entity.DeliveryAdd1;
                        row["DeliveryAdd2"] = entity.DeliveryAdd2;


                        row["PName"] = entity.PName;
                        row["Length"] = entity.Length;
                        row["Width"] = entity.Width;
                        row["Height"] = entity.Height;


                        row["Buyers_OrderNo"] = entity.Buyers_OrderNo;
                        row["BOrderDated"] = entity.BOrderDated;
                        row["DNTimeofInv"] = entity.DNTimeofInv;
                        row["MVehicleNo"] = entity.MVehicleNo;
                        row["DNTimeofRemoval"] = entity.DNTimeofRemoval;
                        row["Pk_PartPropertyID"] = entity.Pk_PartPropertyID;

                        row["UnitVal"] = entity.UnitVal;
                        row["LandLine"] = entity.LandLine;

                        row["TaxType"] = entity.TaxType;

                        row["Despatched"] = entity.Despatched;
                        row["Destination"] = entity.Destination;
                        row["Bundles"] = entity.Bundles;


                        if (entity.TaxType.TrimEnd() == "SGST")     ///FOR SGST
                        {
                            Ttax = "V";
                            ttype = "CGST @ 6%";
                            ttype1 = "SGST @ 6%";

                        }
                        else if (entity.TaxType.TrimEnd() == "IGST")   ///FOR IGST
                        {
                            Ttax = "C";
                            ttype = "IGST @ 12 % ";
                        }

                        else if (entity.TaxType.TrimEnd() == "9%")   ///FOR IGST
                        {
                            Ttax = "9";
                            ttype = "CGST @ 9%";
                            ttype1 = "SGST @ 9%";
                        }
                        else if (entity.TaxType.TrimEnd() == "2.5%")   ///FOR IGST
                        {
                            Ttax = "2.5";
                            ttype = "CGST @ 2.5%";
                            ttype1 = "SGST @ 2.5%";
                        }
                        else if (entity.TaxType.TrimEnd() == "5%")   ///FOR IGST
                        {
                            Ttax = "5";
                            ttype = "CGST @ 5%";
                            ttype1 = "SGST @ 5%";
                        }
                        dt.Rows.Add(row);
                    }
                    NetValue = Convert.ToDecimal(NetValue);
                    if (fct3.Length > 0)
                    {
                        ExValue = 0;
                    }
                    else
                    {
                        ExValue = (Convert.ToDecimal(NetValue) * Convert.ToDecimal(0.06));
                    }

                    if (Ttax == "V")
                    {
                        VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.06);
                        cgval = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.06);
                    }
                    else if (Ttax == "C")
                    {
                        VatVal = 0;
                        cgval = 0;
                        VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.12);
                    }
                    else if (Ttax == "9")
                    {
                        VatVal = 0;
                        cgval = 0;
                        VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.09);
                        cgval = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.09);
                    }
                    else if (Ttax == "2.5")
                    {
                        VatVal = 0;
                        cgval = 0;
                        VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.025);
                        cgval = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.025);
                    }
                    else if (Ttax == "5")
                    {
                        VatVal = 0;
                        cgval = 0;
                        VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.05);
                        cgval = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.05);
                    }




                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.Bill1 orp = new CogitoStreamline.Report.Bill1();

                    orp.Load("@\\Report\\Bill1.rpt");
                    orp.SetDataSource(dt.DefaultView);



                    ReportDocument repDoc = orp;

                    ParameterFieldDefinitions crParameterFieldDefinitions3;
                    ParameterFieldDefinition crParameterFieldDefinition3;
                    ParameterValues crParameterValues3 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue3 = new ParameterDiscreteValue();
                    crParameterDiscreteValue3.Value = NetValue;
                    crParameterFieldDefinitions3 = orp.DataDefinition.ParameterFields;
                    crParameterFieldDefinition3 = crParameterFieldDefinitions3["TotAmt"];
                    crParameterValues3 = crParameterFieldDefinition3.CurrentValues;

                    crParameterValues3.Clear();
                    crParameterValues3.Add(crParameterDiscreteValue3);
                    crParameterFieldDefinition3.ApplyCurrentValues(crParameterValues3);



                    ParameterFieldDefinitions crParameterFieldDefinitions4;
                    ParameterFieldDefinition crParameterFieldDefinition4;
                    ParameterValues crParameterValues4 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                    crParameterDiscreteValue4.Value = "Extra Copy";
                    crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                    crParameterFieldDefinition4 = crParameterFieldDefinitions4["CopyName"];
                    crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                    crParameterValues4.Clear();
                    crParameterValues4.Add(crParameterDiscreteValue4);
                    crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);


                    //ParameterFieldDefinitions crParameterFieldDefinitions5;
                    //ParameterFieldDefinition crParameterFieldDefinition5;
                    //ParameterValues crParameterValues5 = new ParameterValues();
                    //ParameterDiscreteValue crParameterDiscreteValue5 = new ParameterDiscreteValue();
                    //crParameterDiscreteValue5.Value = fct3;
                    //crParameterFieldDefinitions5 = orp.DataDefinition.ParameterFields;
                    //crParameterFieldDefinition5 = crParameterFieldDefinitions4["StrValuefct3"];
                    //crParameterValues5 = crParameterFieldDefinition5.CurrentValues;

                    //crParameterValues5.Clear();
                    //crParameterValues5.Add(crParameterDiscreteValue5);
                    //crParameterFieldDefinition5.ApplyCurrentValues(crParameterValues5);




                    //ParameterFieldDefinitions crParameterFieldDefinitions6;
                    //ParameterFieldDefinition crParameterFieldDefinition6;
                    //ParameterValues crParameterValues6 = new ParameterValues();
                    //ParameterDiscreteValue crParameterDiscreteValue6 = new ParameterDiscreteValue();
                    //crParameterDiscreteValue6.Value = fh;
                    //crParameterFieldDefinitions6 = orp.DataDefinition.ParameterFields;
                    //crParameterFieldDefinition6 = crParameterFieldDefinitions6["StrValueh"];
                    //crParameterValues6 = crParameterFieldDefinition6.CurrentValues;

                    //crParameterValues6.Clear();
                    //crParameterValues6.Add(crParameterDiscreteValue6);
                    //crParameterFieldDefinition6.ApplyCurrentValues(crParameterValues6);


                    ParameterFieldDefinitions crParameterFieldDefinitions7;
                    ParameterFieldDefinition crParameterFieldDefinition7;
                    ParameterValues crParameterValues7 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue7 = new ParameterDiscreteValue();
                    crParameterDiscreteValue7.Value = VatVal;
                    crParameterFieldDefinitions7 = orp.DataDefinition.ParameterFields;
                    crParameterFieldDefinition7 = crParameterFieldDefinitions7["cgst"];
                    crParameterValues7 = crParameterFieldDefinition7.CurrentValues;

                    crParameterValues7.Clear();
                    crParameterValues7.Add(crParameterDiscreteValue7);
                    crParameterFieldDefinition7.ApplyCurrentValues(crParameterValues7);




                    ParameterFieldDefinitions crParameterFieldDefinitions1;
                    ParameterFieldDefinition crParameterFieldDefinition1;
                    ParameterValues crParameterValues1 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue1 = new ParameterDiscreteValue();
                    crParameterDiscreteValue1.Value = VatVal;
                    crParameterFieldDefinitions1 = orp.DataDefinition.ParameterFields;
                    crParameterFieldDefinition1 = crParameterFieldDefinitions1["vat5"];
                    crParameterValues1 = crParameterFieldDefinition1.CurrentValues;

                    crParameterValues1.Clear();
                    crParameterValues1.Add(crParameterDiscreteValue1);
                    crParameterFieldDefinition1.ApplyCurrentValues(crParameterValues1);

                    //ttype


                    ParameterFieldDefinitions crParameterFieldDefinitions12;
                    ParameterFieldDefinition crParameterFieldDefinition12;
                    ParameterValues crParameterValues12 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue12 = new ParameterDiscreteValue();
                    crParameterDiscreteValue12.Value = "OUTPUT " + ttype;
                    crParameterFieldDefinitions12 = orp.DataDefinition.ParameterFields;
                    crParameterFieldDefinition12 = crParameterFieldDefinitions1["TAXT"];
                    crParameterValues12 = crParameterFieldDefinition12.CurrentValues;

                    crParameterValues12.Clear();
                    crParameterValues12.Add(crParameterDiscreteValue12);
                    crParameterFieldDefinition12.ApplyCurrentValues(crParameterValues12);


                    if (ttype1.Length > 0)
                    {
                        ParameterFieldDefinitions crParameterFieldDefinitions6;
                        ParameterFieldDefinition crParameterFieldDefinition6;
                        ParameterValues crParameterValues6 = new ParameterValues();
                        ParameterDiscreteValue crParameterDiscreteValue6 = new ParameterDiscreteValue();
                        crParameterDiscreteValue6.Value = "OUTPUT " + ttype1;
                        crParameterFieldDefinitions6 = orp.DataDefinition.ParameterFields;
                        crParameterFieldDefinition6 = crParameterFieldDefinitions1["TAXT1"];
                        crParameterValues6 = crParameterFieldDefinition6.CurrentValues;

                        crParameterValues6.Clear();
                        crParameterValues6.Add(crParameterDiscreteValue6);
                        crParameterFieldDefinition6.ApplyCurrentValues(crParameterValues6);
                    }
                    else
                    {
                        ParameterFieldDefinitions crParameterFieldDefinitions6;
                        ParameterFieldDefinition crParameterFieldDefinition6;
                        ParameterValues crParameterValues6 = new ParameterValues();
                        ParameterDiscreteValue crParameterDiscreteValue6 = new ParameterDiscreteValue();
                        crParameterDiscreteValue6.Value = ttype1;
                        crParameterFieldDefinitions6 = orp.DataDefinition.ParameterFields;
                        crParameterFieldDefinition6 = crParameterFieldDefinitions1["TAXT1"];
                        crParameterValues6 = crParameterFieldDefinition6.CurrentValues;

                        crParameterValues6.Clear();
                        crParameterValues6.Add(crParameterDiscreteValue6);
                        crParameterFieldDefinition6.ApplyCurrentValues(crParameterValues6);
                    }
                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "BillExtra-" + " " + sInvno + ".pdf");
                    FileInfo file = new FileInfo(pdfPath);
                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();

                }
                else
                {
                    List<Vw_InvRM> BillList1 = new List<Vw_InvRM>();
                    CogitoStreamLineEntities dc1 = new CogitoStreamLineEntities();
                    BillList1 = dc1.Vw_InvRM.Where(x => x.Pk_Invoice == sInvno).Select(x => x).OfType<Vw_InvRM>().ToList();


                    if (BillList1.Count > 0)
                    {
                        //var CtrVal = BillList.Count;
                        DataTable dt1 = new DataTable("InvoiceBill");

                        dt1.Columns.Add("CustomerName");
                        dt1.Columns.Add("CustomerAddress");
                        dt1.Columns.Add("CityName");
                        dt1.Columns.Add("StateName");
                        dt1.Columns.Add("PinCode");
                        dt1.Columns.Add("Pk_Invoice");
                        dt1.Columns.Add("InvDate");
                        dt1.Columns.Add("Fk_OrderNo");
                        dt1.Columns.Add("TaxValue");
                        dt1.Columns.Add("Quantity");
                        dt1.Columns.Add("Price");
                        dt1.Columns.Add("Fk_JobCardID");
                        dt1.Columns.Add("NetAmount");
                        //dt1.Columns.Add("Name");
                        dt1.Columns.Add("Description");
                        dt1.Columns.Add("Pk_Inv_Details");
                        dt1.Columns.Add("Esugam");
                        dt1.Columns.Add("COA");
                        dt1.Columns.Add("CreditLimit");
                        dt1.Columns.Add("HsnCode");
                        dt1.Columns.Add("LandLine");
                        dt1.Columns.Add("VAT");
                        //dt1.Columns.Add("");
                        dt1.Columns.Add("TaxType");

                        dt1.Columns.Add("DeliveryName");
                        dt1.Columns.Add("DeliveryAdd1");
                        dt1.Columns.Add("DeliveryAdd2");

                        dt1.Columns.Add("PName");
                        dt1.Columns.Add("Length");
                        dt1.Columns.Add("Width");
                        dt1.Columns.Add("Height");


                        dt1.Columns.Add("Buyers_OrderNo");
                        dt1.Columns.Add("BOrderDated");
                        dt1.Columns.Add("DNTimeofInv");
                        dt1.Columns.Add("MVehicleNo");
                        dt1.Columns.Add("DNTimeofRemoval");
                        dt1.Columns.Add("Name");
                        dt1.Columns.Add("UnitVal");

                        dt1.Columns.Add("Despatched");
                        dt1.Columns.Add("Destination");


                        decimal NetValue = 0;
                        decimal cgval = 0;
                        decimal ExValue = 0;
                        decimal VatVal = 0;
                        string ttype = "";
                        string ttype1 = "";
                        string Ttax = "";
                        string fct3 = "";
                        //string fh = "";

                        foreach (Vw_InvRM entity in BillList1)
                        {
                            DataRow row = dt1.NewRow();

                            row["CustomerName"] = entity.CustomerName;
                            row["CustomerAddress"] = entity.CustomerAddress;
                            row["CityName"] = entity.CityName;
                            row["StateName"] = entity.StateName;
                            row["PinCode"] = entity.PinCode;
                            row["Pk_Invoice"] = entity.Pk_Invoice;
                            row["InvDate"] = entity.InvDate;
                            row["Fk_OrderNo"] = entity.Fk_OrderNo;
                            row["TaxValue"] = entity.TaxValue;
                            row["Quantity"] = entity.Quantity;
                            row["Price"] = entity.Price;
                            //row["Fk_JobCardID"] = entity.Fk_JobCardID;
                            row["NetAmount"] = entity.NetAmount;
                            NetValue = Convert.ToDecimal(NetValue + entity.NetAmount);
                            //row["Name"] = entity.Name;
                            row["HsnCode"] = entity.HsnCode;
                            row["Pk_Inv_Details"] = entity.Pk_Inv_Details;
                            row["Esugam"] = entity.ESUGAM;
                            row["COA"] = entity.COA;
                            row["CreditLimit"] = entity.CreditLimit;
                            row["HsnCode"] = entity.HsnCode;
                            row["Description"] = entity.Description;
                            row["VAT"] = entity.VAT;

                            row["DeliveryName"] = entity.DeliveryName;
                            row["DeliveryAdd1"] = entity.DeliveryAdd1;
                            row["DeliveryAdd2"] = entity.DeliveryAdd2;


                            //row["PName"] = entity.PName;
                            //row["Length"] = entity.Length;
                            //row["Width"] = entity.Width;
                            //row["Height"] = entity.Height;


                            row["Buyers_OrderNo"] = entity.Buyers_OrderNo;
                            row["BOrderDated"] = entity.BOrderDated;
                            row["DNTimeofInv"] = entity.DNTimeofInv;
                            row["MVehicleNo"] = entity.MVehicleNo;
                            row["DNTimeofRemoval"] = entity.DNTimeofRemoval;
                            row["Name"] = entity.Name;

                            row["UnitVal"] = entity.UnitVal;
                            row["LandLine"] = entity.LandLine;

                            row["TaxType"] = entity.TaxType;

                            row["Despatched"] = entity.Despatched;
                            row["Destination"] = entity.Destination;
                            //row["Bundles"] = entity.Bundles;
                            if (entity.TaxType.TrimEnd() == "SGST")     ///FOR SGST
                            {
                                Ttax = "V";
                                ttype = "CGST @ 6%";
                                ttype1 = "SGST @ 6 %";

                            }
                            else if (entity.TaxType.TrimEnd() == "IGST")   ///FOR IGST
                            {
                                Ttax = "C";
                                ttype = "IGST @ 12 % ";
                            }
                            else if (entity.TaxType.TrimEnd() == "9%")   ///FOR IGST
                            {
                                Ttax = "9";
                                ttype = "CGST @ 9%";
                                ttype1 = "SGST @ 9%";
                            }
                            else if (entity.TaxType.TrimEnd() == "2.5%")   ///FOR IGST
                            {
                                Ttax = "2.5";
                                ttype = "CGST @ 2.5%";
                                ttype1 = "SGST @ 2.5%";
                            }
                            else if (entity.TaxType.TrimEnd() == "5%")   ///FOR IGST
                            {
                                Ttax = "5";
                                ttype = "CGST @ 5%";
                                ttype1 = "SGST @ 5%";
                            }
                            dt1.Rows.Add(row);
                        }
                        NetValue = Convert.ToDecimal(NetValue);
                        if (fct3.Length > 0)
                        {
                            ExValue = 0;
                        }
                        else
                        {
                            ExValue = (Convert.ToDecimal(NetValue) * Convert.ToDecimal(0.06));
                        }

                        if (Ttax == "V")
                        {
                            VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.06);
                            cgval = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.06);
                        }
                        else if (Ttax == "C")
                        {
                            VatVal = 0;
                            cgval = 0;
                            VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.12);
                        }
                        else if (Ttax == "9")
                        {
                            VatVal = 0;
                            cgval = 0;
                            VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.09);
                            cgval = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.09);
                        }
                        else if (Ttax == "2.5")
                        {
                            VatVal = 0;
                            cgval = 0;
                            VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.025);
                            cgval = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.025);
                        }
                        else if (Ttax == "5")
                        {
                            VatVal = 0;
                            cgval = 0;
                            VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.05);
                            cgval = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.05);
                        }





                        DataSet ds = new DataSet();
                        ds.Tables.Add(dt1);
                        CogitoStreamline.Report.BillRM orp = new CogitoStreamline.Report.BillRM();

                        orp.Load("@\\Report\\BillRM.rpt");
                        orp.SetDataSource(dt1.DefaultView);



                        ReportDocument repDoc = orp;

                        ParameterFieldDefinitions crParameterFieldDefinitions3;
                        ParameterFieldDefinition crParameterFieldDefinition3;
                        ParameterValues crParameterValues3 = new ParameterValues();
                        ParameterDiscreteValue crParameterDiscreteValue3 = new ParameterDiscreteValue();
                        crParameterDiscreteValue3.Value = NetValue;
                        crParameterFieldDefinitions3 = orp.DataDefinition.ParameterFields;
                        crParameterFieldDefinition3 = crParameterFieldDefinitions3["TotAmt"];
                        crParameterValues3 = crParameterFieldDefinition3.CurrentValues;

                        crParameterValues3.Clear();
                        crParameterValues3.Add(crParameterDiscreteValue3);
                        crParameterFieldDefinition3.ApplyCurrentValues(crParameterValues3);



                        ParameterFieldDefinitions crParameterFieldDefinitions4;
                        ParameterFieldDefinition crParameterFieldDefinition4;
                        ParameterValues crParameterValues4 = new ParameterValues();
                        ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                        crParameterDiscreteValue4.Value = "Extra Copy";
                        crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                        crParameterFieldDefinition4 = crParameterFieldDefinitions4["CopyName"];
                        crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                        crParameterValues4.Clear();
                        crParameterValues4.Add(crParameterDiscreteValue4);
                        crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);


                        //ParameterFieldDefinitions crParameterFieldDefinitions5;
                        //ParameterFieldDefinition crParameterFieldDefinition5;
                        //ParameterValues crParameterValues5 = new ParameterValues();
                        //ParameterDiscreteValue crParameterDiscreteValue5 = new ParameterDiscreteValue();
                        //crParameterDiscreteValue5.Value = fct3;
                        //crParameterFieldDefinitions5 = orp.DataDefinition.ParameterFields;
                        //crParameterFieldDefinition5 = crParameterFieldDefinitions4["StrValuefct3"];
                        //crParameterValues5 = crParameterFieldDefinition5.CurrentValues;

                        //crParameterValues5.Clear();
                        //crParameterValues5.Add(crParameterDiscreteValue5);
                        //crParameterFieldDefinition5.ApplyCurrentValues(crParameterValues5);




                        //ParameterFieldDefinitions crParameterFieldDefinitions6;
                        //ParameterFieldDefinition crParameterFieldDefinition6;
                        //ParameterValues crParameterValues6 = new ParameterValues();
                        //ParameterDiscreteValue crParameterDiscreteValue6 = new ParameterDiscreteValue();
                        //crParameterDiscreteValue6.Value = fh;
                        //crParameterFieldDefinitions6 = orp.DataDefinition.ParameterFields;
                        //crParameterFieldDefinition6 = crParameterFieldDefinitions6["StrValueh"];
                        //crParameterValues6 = crParameterFieldDefinition6.CurrentValues;

                        //crParameterValues6.Clear();
                        //crParameterValues6.Add(crParameterDiscreteValue6);
                        //crParameterFieldDefinition6.ApplyCurrentValues(crParameterValues6);


                        ParameterFieldDefinitions crParameterFieldDefinitions7;
                        ParameterFieldDefinition crParameterFieldDefinition7;
                        ParameterValues crParameterValues7 = new ParameterValues();
                        ParameterDiscreteValue crParameterDiscreteValue7 = new ParameterDiscreteValue();
                        crParameterDiscreteValue7.Value = VatVal;
                        crParameterFieldDefinitions7 = orp.DataDefinition.ParameterFields;
                        crParameterFieldDefinition7 = crParameterFieldDefinitions7["cgst"];
                        crParameterValues7 = crParameterFieldDefinition7.CurrentValues;

                        crParameterValues7.Clear();
                        crParameterValues7.Add(crParameterDiscreteValue7);
                        crParameterFieldDefinition7.ApplyCurrentValues(crParameterValues7);




                        ParameterFieldDefinitions crParameterFieldDefinitions1;
                        ParameterFieldDefinition crParameterFieldDefinition1;
                        ParameterValues crParameterValues1 = new ParameterValues();
                        ParameterDiscreteValue crParameterDiscreteValue1 = new ParameterDiscreteValue();
                        crParameterDiscreteValue1.Value = VatVal;
                        crParameterFieldDefinitions1 = orp.DataDefinition.ParameterFields;
                        crParameterFieldDefinition1 = crParameterFieldDefinitions1["vat5"];
                        crParameterValues1 = crParameterFieldDefinition1.CurrentValues;

                        crParameterValues1.Clear();
                        crParameterValues1.Add(crParameterDiscreteValue1);
                        crParameterFieldDefinition1.ApplyCurrentValues(crParameterValues1);

                        //ttype


                        ParameterFieldDefinitions crParameterFieldDefinitions12;
                        ParameterFieldDefinition crParameterFieldDefinition12;
                        ParameterValues crParameterValues12 = new ParameterValues();
                        ParameterDiscreteValue crParameterDiscreteValue12 = new ParameterDiscreteValue();
                        crParameterDiscreteValue12.Value = "OUTPUT " + ttype;
                        crParameterFieldDefinitions12 = orp.DataDefinition.ParameterFields;
                        crParameterFieldDefinition12 = crParameterFieldDefinitions1["TAXT"];
                        crParameterValues12 = crParameterFieldDefinition12.CurrentValues;

                        crParameterValues12.Clear();
                        crParameterValues12.Add(crParameterDiscreteValue12);
                        crParameterFieldDefinition12.ApplyCurrentValues(crParameterValues12);


                        if (ttype1.Length > 0)
                        {
                            ParameterFieldDefinitions crParameterFieldDefinitions6;
                            ParameterFieldDefinition crParameterFieldDefinition6;
                            ParameterValues crParameterValues6 = new ParameterValues();
                            ParameterDiscreteValue crParameterDiscreteValue6 = new ParameterDiscreteValue();
                            crParameterDiscreteValue6.Value = "OUTPUT " + ttype1;
                            crParameterFieldDefinitions6 = orp.DataDefinition.ParameterFields;
                            crParameterFieldDefinition6 = crParameterFieldDefinitions1["TAXT1"];
                            crParameterValues6 = crParameterFieldDefinition6.CurrentValues;

                            crParameterValues6.Clear();
                            crParameterValues6.Add(crParameterDiscreteValue6);
                            crParameterFieldDefinition6.ApplyCurrentValues(crParameterValues6);
                        }
                        else
                        {
                            ParameterFieldDefinitions crParameterFieldDefinitions6;
                            ParameterFieldDefinition crParameterFieldDefinition6;
                            ParameterValues crParameterValues6 = new ParameterValues();
                            ParameterDiscreteValue crParameterDiscreteValue6 = new ParameterDiscreteValue();
                            crParameterDiscreteValue6.Value = ttype1;
                            crParameterFieldDefinitions6 = orp.DataDefinition.ParameterFields;
                            crParameterFieldDefinition6 = crParameterFieldDefinitions1["TAXT1"];
                            crParameterValues6 = crParameterFieldDefinition6.CurrentValues;

                            crParameterValues6.Clear();
                            crParameterValues6.Add(crParameterDiscreteValue6);
                            crParameterFieldDefinition6.ApplyCurrentValues(crParameterValues6);
                        }
                        string pdfPath = Server.MapPath("~/ConvertPDF/" + "BillExtra-" + " " + sInvno + ".pdf");
                        FileInfo file = new FileInfo(pdfPath);
                        if (file.Exists)
                        {
                            file.Delete();
                        }
                        var pd = new PrintDocument();


                        orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                        orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                        DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                        objDiskOpt.DiskFileName = pdfPath;

                        orp.ExportOptions.DestinationOptions = objDiskOpt;
                        orp.Export();
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
                //  Response.Redirect("Index");
            }
        }



        [HttpPost]
        public JsonResult getTax(string pId = "")
        {
            List<Tax> oListOfTax = oTax.Search(null).ListOfRecords.OfType<Tax>().ToList();
            var oTaxToDisplay = oListOfTax.Select(p => new
            {
                Name = p.TaxName,
                Id = p.PkTax
            });

            return Json(oTaxToDisplay);
        }
        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                Billing oBilling = oDoaminObject as Billing;
                oBilling.ID = decimal.Parse(pId);
                Inv_Billing oAtualObject = oBilling.DAO as Inv_Billing;
           
                int i = -1;

                var oInvBillDetails = oAtualObject.Inv_BillingDetails.Select(p => new
                {
                    Pk_Inv_Details = p.Pk_Inv_Details,
                    slno = ++i,
                    Quantity = p.Quantity,
                    Fk_BoxID = p.Fk_BoxID,
                    BoxID = p.Fk_BoxID,

                    HsnCode = p.HsnCode,
                    Name = p.BoxMaster.Name,
                    Fk_PartID=p.Fk_PartID,
                    Price = p.Price,
                    TotalAmount=p.TotalAmount,
                    NetAmount=p.NetAmount,
                Description=p.Description
           
                });


                //Create a anominious object here to break the circular reference
                var oInvBillToDisplay = new
                {
                    Pk_Invoice = oAtualObject.Pk_Invoice,
                    InvDate = DateTime.Parse(oAtualObject.InvDate.ToString()).ToString("dd/MM/yyyy"),
                    //Invno = oAtualObject.Invno,
                    Fk_Customer = oAtualObject.Fk_Customer,
                    GrandTotal=oAtualObject.GrandTotal,
                    PayMode = oAtualObject.PayMode,
         COA=oAtualObject.COA,
                    ESUGAM=oAtualObject.ESUGAM,
                    NETVALUE = oAtualObject.NETVALUE,
                              DeliveryName=oAtualObject.DeliveryName,
                            DeliveryAdd1=  oAtualObject.DeliveryAdd1,
                           DeliveryAdd2=   oAtualObject.DeliveryAdd2,
                   // FORM_CT3=oAtualObject.FORM_CT3,
                   // FORM_H=oAtualObject.FORM_H,
                    TaxType = oAtualObject.TaxType.TrimEnd(),
                   ED=oAtualObject.ED,
                  Cancel=oAtualObject.Cancel,
                    Inv_BillingDetails = Json(oInvBillDetails).Data
                    //Fk_VendorId,
                };

                return Json(new { success = true, data = oInvBillToDisplay });
            }
            else
            {
                var oInvBillToDisplay = new Inv_Billing();
                return Json(new { success = true, data = oInvBillToDisplay });
            }
        }
        [HttpPost]
        public JsonResult BillingListByFiter(string Pk_Invoice = null, string Invno = "", string FromInvDate = "", string Customer = "", string TxtFromDate = "", string TxtToDate = "", string OrderNo = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_Invoice", Pk_Invoice));
                oSearchParams.Add(new SearchParameter("Fk_Customer", Customer));
                oSearchParams.Add(new SearchParameter("FromInvDate", FromInvDate));
                oSearchParams.Add(new SearchParameter("TxtFromDate", TxtFromDate));
                oSearchParams.Add(new SearchParameter("TxtToDate", TxtToDate));
                oSearchParams.Add(new SearchParameter("OrderNo", OrderNo));
                oSearchParams.Add(new SearchParameter("Invno", Invno));

                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oInvBillToDisplayObjects = oSearchResult.ListOfRecords;
                List<Inv_Billing> oInvBillObjects = oInvBillToDisplayObjects.Select(p => p).OfType<Inv_Billing>().ToList();

                //Create a anominious object here to break the circular reference
                var oInvBillToDisplay = oInvBillObjects.Select(p => new
                {

                    Pk_Invoice = p.Pk_Invoice,
                    InvDate = p.InvDate != null ? DateTime.Parse(p.InvDate.ToString()).ToString("dd/MM/yyyy") : "",
                    Invno = p.Invno,
                    Fk_Customer = p.Fk_Customer != null ? p.gen_Customer.CustomerName : "",
                    GrandTotal=p.GrandTotal,
                    NETVALUE = p.NETVALUE,
                    Pk_Order=p.Fk_OrderNo,
                    ED = p.ED,

                }).ToList();

                return Json(new { Result = "OK", Records = oInvBillToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]

        public JsonResult SaveNew([System.Web.Http.FromBody]string data)
        {
            data = data.TrimStart('=');
            Dictionary<string, object> values = JsonConvert.DeserializeObject<Dictionary<string, object>>(data);
            data = JsonConvert.SerializeObject(values);
            return base.Save(data);


        }

    }
}
