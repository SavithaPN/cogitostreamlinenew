﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;
using System.IO;
namespace CogitoStreamLine.Controllers
{
    public class DepartmentController : CommonController
    {
        Department oCountry = new Department();
        //Module oModule = new Module();
        //Role oRole = new Role();

        public DepartmentController()
        {
            oDoaminObject = new Department();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Department";
            return base.Index();
        }
        protected override void Dispose(bool disposing)
        {
            oCountry.Dispose();
            base.Dispose(disposing);
        }
        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                Department oDept = oDoaminObject as Department;
                oDept.ID = decimal.Parse(pId);
                emp_Department oAtualObject = oDept.DAO as emp_Department;
                //Create a anominious object here to break the circular reference
                var oDeptToDisplay = new
                {
                    Pk_DepartmentId = oAtualObject.Pk_DepartmentId,
                    Name = oAtualObject.Name,
                    Description = oAtualObject.Description
                };
                return Json(new { success = true, data = oDeptToDisplay });
            }
            else
            {
                var oDeptToDisplay = new emp_Department();
                return Json(new { success = true, data = oDeptToDisplay });
            }
        }
        [HttpPost]
        public JsonResult DeptDuplicateChecking(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sName = oValues["Name"];
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Name", sName));
                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);
                List<EntityObject> oDeptToDisplayObjects = oSearchResult.ListOfRecords;
                List<emp_Department> oDeptObjects = oDeptToDisplayObjects.Select(p => p).OfType<emp_Department>().ToList();
                var oDeptToDisplay = oDeptObjects.Select(p => new
                {
                    Pk_DepartmentId = p.Pk_DepartmentId,
                    Name = p.Name
                }).ToList().OrderBy(s => s.Name);
                return Json(new { Success = true, Records = oDeptToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult DepartmentListByFiter(string Name = "", string Pk_DepartmentId = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Name", Name));
                oSearchParams.Add(new SearchParameter("Pk_DepartmentId", Pk_DepartmentId));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));
                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);
                List<EntityObject> oDeptToDisplayObjects = oSearchResult.ListOfRecords;
                List<emp_Department> oDeptObjects = oDeptToDisplayObjects.Select(p => p).OfType<emp_Department>().ToList();
                //Create a anominious object here to break the circular reference
                var oDeptToDisplay = oDeptObjects.Select(p => new
                {
                    Pk_DepartmentId = p.Pk_DepartmentId,
                    Name = p.Name,
                    Description = p.Description
                }).ToList().OrderBy(s => s.Name);
                return Json(new { Result = "OK", Records = oDeptToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
    }
}

