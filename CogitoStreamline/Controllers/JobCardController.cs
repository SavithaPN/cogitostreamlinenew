﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;
using System.IO;
using System.Data;
using CrystalDecisions.ReportAppServer.ReportDefModel;
using System.Drawing.Printing;
using CrystalDecisions.Shared;
using WebGareCore.Controls;
using WebGareCore.CommonObjects.WorkFlow;
using CrystalDecisions.CrystalReports.Engine;

namespace CogitoStreamline.Controllers
{
    public class JobCardController : CommonController
    {
        //
        // GET: /Order/

        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();


        Machine oMachine = new Machine();
        ////Product oProduct = new Product();
        JobCard oPaperStock = new JobCard();
        BoxMaster oBox = new BoxMaster();
        public WgFileUpload oFileUpload = new WgFileUpload("documents", "uploads");
        public JobCardController()
        {
            oDoaminObject = new JobCard();
        }

        public override ActionResult Index()
        {
            ViewBag.Header = "JobCard";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
        [HttpPost]
        public JsonResult FileUpload()
        {
            return Json(oFileUpload.SaveFile(Request.Files));
        }
        [HttpPost]
        public JsonResult getMachine(string pId = "")
        {
            List<gen_Machine> oListOfCustomers = oMachine.Search(null).ListOfRecords.OfType<gen_Machine>().ToList();
            var oCustomersToDisplay = oListOfCustomers.Select(p => new
            {
                Name = p.Name,
                Id = p.Pk_Machine
            });

            return Json(oCustomersToDisplay);
        }


        [HttpPost]
        public JsonResult getBox(string pId = "")
        {
            var Custval = 15;
            List<BoxMaster> oListOfUnit = new BoxM().Search(null).ListOfRecords.OfType<BoxMaster>().ToList();
            var oUnitToDisplay = oListOfUnit.Where(p => p.Customer == Custval).Select(p => new
            {
                Name = p.Name,
                Id = p.Pk_BoxID
            });

            return Json(oUnitToDisplay);
        }

        [HttpPost]
        public JsonResult getBoxdata(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            var Custval = Convert.ToInt64(oValues["CustId"]);
            List<BoxMaster> oListOfUnit = new BoxM().Search(null).ListOfRecords.OfType<BoxMaster>().ToList();
            var oUnitToDisplay = oListOfUnit.Where(p => p.Customer == Custval).Select(p => new
            {
                Name = p.Name,
                Id = p.Pk_BoxID
            });

            return Json(oUnitToDisplay);
        }
        //[HttpPost]
        public JsonResult getBoxdata1(string data = "")
        {
            List<BoxMaster> oListOfBox = new List<BoxMaster>();
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            var Custval = Convert.ToInt64(oValues["Custval"]);
            //List<BoxMaster> oListOfUnit = new BoxM().Search(null).ListOfRecords.OfType<BoxMaster>().ToList();
            var oBoxToDisplay = _oEntites.BoxMaster.Where(x => x.Customer == Custval).Select(x => x).ToList();

            if (oBoxToDisplay != null && oBoxToDisplay.Count() > 0)
            {
                foreach (var dataval in oBoxToDisplay)
                {
                    BoxMaster oBox = new BoxMaster();
                    oBox.Pk_BoxID = dataval.Pk_BoxID;
                    oBox.Name = dataval.Name;
                    oListOfBox.Add(oBox);
                }
            }
            return Json(oListOfBox);

        }
        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                JobCard oJob = oDoaminObject as JobCard;
                oJob.ID = decimal.Parse(pId);
                JobCardMaster oAtualObject = oJob.DAO as JobCardMaster;

                int i = -1;

                var oJobDet = oAtualObject.JobCardDetails.Select(p => new
                {

                    Pk_JobCardDet = p.Pk_JobCardDet,
                    slno = ++i,
                    Fk_JobCardID = p.Fk_JobCardID,
                    //p.CheckDate != null ? DateTime.Parse(p.CheckDate.ToString()).ToString("dd/MM/yyyy") : "",
                    //Fk_PaperStock = p.Fk_PaperStock,
                    Fk_PaperStock = p.Fk_PaperStock != null ? p.Fk_PaperStock : 0,
                    Fk_Material = p.Fk_Material,
                    MName = p.Inv_Material.Name,
                    Name = p.PaperStock != null ? p.PaperStock.Inv_Material.Name : "",
                    RollNo = p.PaperStock != null ? p.PaperStock.RollNo : "",
                    Quantity = p.RM_Consumed,
                    QtyConsumed = p.QtyConsumed,
                    Color = p.Inv_Material.gen_Color.ColorName,

                });

                var oJobProcess = oAtualObject.JobProcess.Select(p => new
                {

                    Pk_ID = p.Pk_ID,
                    slno = ++i,
                    Fk_JobCardID = p.Fk_JobCardID,
                    ProcessDate = p.ProcessDate != null ? DateTime.Parse(p.ProcessDate.ToString()).ToString("dd/MM/yyyy") : "",
                    //Fk_PaperStock = p.Fk_PaperStock,
                    Fk_ProcessID = p.Fk_ProcessID != null ? p.Fk_ProcessID : 0,
                    //  Fk_Material = p.Fk_Material,
                    PQuantity = p.PQuantity,
                    ProcessName = p.Fk_ProcessID != null ? p.ProcessMaster.ProcessName : "",
                    //    RollNo = p.PaperStock != null ? p.PaperStock.RollNo : "",
                    Quantity = p.Quantity,
                    WastageQty = p.WastageQty,
                    PSTime = p.PSTime != null ? p.PSTime : "",
                    PETime = p.PETime != null ? p.PETime : "",
                    //  Color = p.Inv_Material.gen_Color.ColorName,
                    ReasonWastage = p.ReasonWastage,
                    RemainingQty = p.RemainingQty,
                    WQty = p.WQty,
                    R1Wastage = p.R1Wastage,
                    R2Wastage = p.R2Wastage,
                    R3Wastage = p.R3Wastage,
                    R4Wastage = p.R4Wastage,

                });
                //Create a anominious object here to break the circular reference
                var oJobToDisplay = new
                {
                    Pk_JobCardID = oAtualObject.Pk_JobCardID,
                    Corrugation = oAtualObject.Corrugation,
                    Fk_Order = oAtualObject.Fk_Order,
                    Fk_Enquiry = oAtualObject.gen_Order.Fk_Enquiry,
                    txtfk_Customer = oAtualObject.gen_Order.gen_Customer.CustomerName,
                    Fk_Status = oAtualObject.Fk_Status,
                    ChkQuality = oAtualObject.ChkQuality,
                    Fk_BoxID = oAtualObject.Fk_BoxID,

                    BoxID = oAtualObject.Fk_BoxID,
                    Fk_PartID = oAtualObject.gen_DeliverySchedule.Fk_PartID,

                    BoxType = oAtualObject.BoxMaster.BoxType.Name,
                    Printing = oAtualObject.Printing,
                    PrintVal = oAtualObject.Printing,
                    DPRScVal = oAtualObject.DPRSc,
                    Calico = oAtualObject.Calico,
                    Stitching = oAtualObject.Stitching,
                    Others = oAtualObject.Others,
                    Invno = oAtualObject.Invno,
                    JDate = DateTime.Parse(oAtualObject.JDate.ToString()).ToString("dd/MM/yyyy"),
                    TopPaperQty = oAtualObject.TopPaperQty,
                    TwoPlyQty = oAtualObject.TwoPlyQty,
                    TwoPlyWt = oAtualObject.TwoPlyWt,
                    PastingQty = oAtualObject.PastingQty,
                    PastingWstQty = oAtualObject.PastingWstQty,
                    RotaryQty = oAtualObject.RotaryQty,
                    RotaryWstQty = oAtualObject.RotaryWstQty,
                    PunchingQty = oAtualObject.PunchingQty,
                    PunchingWstQty = oAtualObject.PunchingWstQty,
                    SlotingQty = oAtualObject.SlotingQty,
                    SlotingWstQty = oAtualObject.SlotingWstQty,
                    PinningQty = oAtualObject.PinningQty,
                    PinningWstQty = oAtualObject.PinningWstQty,
                    FinishingQty = oAtualObject.FinishingQty,
                    FinishingWstQty = oAtualObject.FinishingWstQty,
                    TotalQty = oAtualObject.TotalQty,
                    TotalWstQty = oAtualObject.TotalWstQty,
                    MaterialData = Json(oJobDet).Data,
                    ProcessData = Json(oJobProcess).Data,

                    Status = oAtualObject.wfStates.State,
                    Machine = oAtualObject.Machine,
                    StartTime = oAtualObject.StartTime,
                    EndTime = oAtualObject.EndTime,
                    PColor = oAtualObject.PColor,
                    PDetails = oAtualObject.PDetails,
                    MFG = oAtualObject.MFG,
                    DPRSc = oAtualObject.DPRSc,
                    ECCQty = oAtualObject.ECCQty,
                    EccWstQty = oAtualObject.EccWstQty,
                    GummingQty = oAtualObject.GummingQty,
                    GummingWstQty = oAtualObject.GummingWstQty,
                    CutLength = oAtualObject.CutLength,
                    UpsVal = oAtualObject.UpsVal,
                    Fk_Schedule = oAtualObject.Fk_Schedule,
                    PName = oAtualObject.gen_DeliverySchedule.ItemPartProperty.PName,

                    Corr = oAtualObject.Corr.Trim(),
                    TopP = oAtualObject.TopP.Trim(),
                    Pasting = oAtualObject.Pasting.Trim(),
                    Rotary = oAtualObject.Rotary.Trim(),
                    PrintingP = oAtualObject.PrintingP.Trim(),
                    Punching = oAtualObject.Punching.Trim(),
                    Slotting = oAtualObject.Slotting.Trim(),
                    Pinning = oAtualObject.Pinning.Trim(),
                    Gumming = oAtualObject.Gumming.Trim(),
                    Bundling = oAtualObject.Bundling.Trim(),
                    Finishing = oAtualObject.Finishing.Trim(),
                    Bundles = oAtualObject.Bundles,

                };

                return Json(new { success = true, data = oJobToDisplay });
            }
            else
            {
                var oJobToDisplay = new gen_Order();
                return Json(new { success = true, data = oJobToDisplay });
            }
        }
        [HttpPost]
        public JsonResult PaperStockList(string Fk_BoxID = "", string Fk_Enquiry = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Fk_BoxID", Fk_BoxID));
                oSearchParams.Add(new SearchParameter("Fk_Enquiry", Fk_Enquiry));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oPaperStock.SearchPaperStock(oSearchParams);

                List<EntityObject> oPapersToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_BoxPaperStock> oPaperObjects = oPapersToDisplayObjects.Select(p => p).OfType<Vw_BoxPaperStock>().ToList();

                var oPapersToDisplay = oPaperObjects.Select(p => new
                {
                    Pk_Material = p.Pk_Material,
                    Name = p.Name,
                    Fk_BoxID = p.Fk_BoxID,
                    //Quantity = p.Expr1,
                    //Fk_Enquiry = p.Fk_Enquiry

                }).ToList();

                return Json(new { Result = "OK", Records = oPapersToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult BoxPaperStock(string Fk_BoxID = "", string Fk_Enquiry = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Fk_BoxID", Fk_BoxID));
                oSearchParams.Add(new SearchParameter("Fk_Enquiry", Fk_Enquiry));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oPaperStock.SearchBoxPaper(oSearchParams);

                List<EntityObject> oPapersToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_BoxPaper> oPaperObjects = oPapersToDisplayObjects.Select(p => p).OfType<Vw_BoxPaper>().ToList();

                var oPapersToDisplay = oPaperObjects.Select(p => new
                {
                    Pk_Material = p.Pk_Material,
                    Name = p.Name,
                    //Fk_BoxID = p.Fk_BoxID,
                    Quantity = p.Quantity,
                    RollNo = p.RollNo

                }).ToList();

                return Json(new { Result = "OK", Records = oPapersToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult UpdBoxStock(string Fk_JobCardID = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Fk_JobCardID", Fk_JobCardID));
                //oSearchParams.Add(new SearchParameter("Fk_Enquiry", Fk_Enquiry));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oPaperStock.SearchStockUpd(oSearchParams);

                List<EntityObject> oPapersToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_Upd_Stock_Jobprocess> oPaperObjects = oPapersToDisplayObjects.Select(p => p).OfType<Vw_Upd_Stock_Jobprocess>().ToList();

                var oPapersToDisplay = oPaperObjects.Select(p => new
                {
                    StockUpd = p.StockUpd,
                    ProcessName = p.ProcessName,
                    //Fk_BoxID = p.Fk_BoxID,
                    Fk_JobCardID = p.Fk_JobCardID,
                    Pk_Process = p.Pk_Process

                }).ToList();

                return Json(new { Result = "OK", Records = oPapersToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


        [HttpPost]
        public JsonResult JCPaperStockList(string Pk_PaperStock = "", string Fk_Material = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_PaperStock", Pk_PaperStock));
                oSearchParams.Add(new SearchParameter("Fk_Material", Fk_Material));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oPaperStock.SearchJCPaperStock(oSearchParams);

                List<EntityObject> oPapersToDisplayObjects = oSearchResult.ListOfRecords;
                List<PaperStock> oPaperObjects = oPapersToDisplayObjects.Select(p => p).OfType<PaperStock>().ToList();

                var oPapersToDisplay = oPaperObjects.Select(p => new
                {
                    Pk_PaperStock = p.Pk_PaperStock,
                    Name = p.Inv_Material.Name,
                    Fk_Material = p.Fk_Material,
                    RollNo = p.RollNo,
                    Quantity = p.Quantity

                }).ToList();

                return Json(new { Result = "OK", Records = oPapersToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult JobListByFiter(string Customer = "", string Pk_JobcardID = "", string BoxName = "", string BoxID = "", string JDate = "", string Fk_Order = "", string OnlyPending = "", string FromJDate = "", string ToJDate = "", string Pk_Order = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("OnlyPending", OnlyPending));
                oSearchParams.Add(new SearchParameter("BoxName", BoxName));
                oSearchParams.Add(new SearchParameter("Customer", Customer));
                oSearchParams.Add(new SearchParameter("Pk_JobcardID", Pk_JobcardID));
                oSearchParams.Add(new SearchParameter("JDate", JDate));
                oSearchParams.Add(new SearchParameter("Fk_Order", Fk_Order));
                oSearchParams.Add(new SearchParameter("BoxID", BoxID));
                oSearchParams.Add(new SearchParameter("FromJDate", FromJDate));
                oSearchParams.Add(new SearchParameter("ToJDate", ToJDate));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);


                //if (OnlyPending == "")
                //{
                List<EntityObject> oJobsToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_InProcessJC> oJobObjects = oJobsToDisplayObjects.Select(p => p).OfType<Vw_InProcessJC>().ToList();
                //}
                //else { 
                //   List<EntityObject> oJobsToDisplayObjects = oSearchResult.ListOfRecords;
                //   List<JobCardMaster> oJobObjects = oJobsToDisplayObjects.Select(p => p).OfType<JobCardMaster>().ToList();
                //}

                //Create a anominious object here to break the circular reference

                //.Where(x => !states.Contains(x.state));

                //int[] ids = { 22,23 };
                var oJobsToDisplay = oJobObjects.Select(p => new
                {
                    Pk_JobCardID = p.Pk_JobCardID,
                    JDate = DateTime.Parse(p.JDate.ToString()).ToString("dd/MM/yyyy"),
                    Fk_Order = p.Fk_Order,
                    Customer = p.CustomerName,
                    Fk_Status = p.State,
                    SchNo = p.Fk_Schedule,
                    BoxName = p.Name,
                    BType = p.BType,
                    PartName = p.PName,
                }).ToList();

                return Json(new { Result = "OK", Records = oJobsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


        [HttpPost]
        public JsonResult JobAssignedStock(string Pk_Material = "", string BoxName = "", string JDate = "", string Fk_Order = "", string OnlyPending = "", string FromJDate = "", string ToJDate = "", string Pk_Order = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_Material", Pk_Material));
                //oSearchParams.Add(new SearchParameter("BoxName", BoxName));
                //oSearchParams.Add(new SearchParameter("Customer", Customer));
                //oSearchParams.Add(new SearchParameter("JDate", JDate));
                //oSearchParams.Add(new SearchParameter("Fk_Order", Fk_Order));
                //oSearchParams.Add(new SearchParameter("FromJDate", FromJDate));
                //oSearchParams.Add(new SearchParameter("ToJDate", ToJDate));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oPaperStock.SearchAssignedStock(oSearchParams);

                List<EntityObject> oJobsToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_PStock> oJobObjects = oJobsToDisplayObjects.Select(p => p).OfType<Vw_PStock>().ToList();

                //Create a anominious object here to break the circular reference
                var oJobsToDisplay = oJobObjects.Where(p => p.Fk_Status == 1 && p.RM_Consumed > 0).Select(p => new
                {
                    Pk_Material = p.Pk_Material,
                    Pk_JobCardID = p.Pk_JobCardID,
                    BoxName = p.BoxName,
                    JDate = DateTime.Parse(p.JDate.ToString()).ToString("dd/MM/yyyy"),
                    MName = p.Name,
                    Quantity = p.RM_Consumed,    ///Assigned Stock
                    StockQty = p.TQty,
                    Customer = p.CustomerName,
                }).ToList();

                return Json(new { Result = "OK", Records = oJobsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


        [HttpPost]
        public JsonResult InvDuplicateChecking(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sName = oValues["invno"];

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("InvNo", sName));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oPinToDisplayObjects = oSearchResult.ListOfRecords;
                List<JobCardMaster> oPinObjects = oPinToDisplayObjects.Select(p => p).OfType<JobCardMaster>().ToList();

                var oPinToDisplay = oPinObjects.Select(p => new
                {
                    Pk_JobCardID = p.Pk_JobCardID,
                    Invno = p.Invno

                }).ToList().OrderBy(s => s.Invno);

                return Json(new { Success = true, Records = oPinToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }

        //      public ActionResult JCRep(string data = "")
        //      {
        //          try
        //          {
        //              Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        //              int sInvno = Convert.ToInt32(oValues["JCNO"]);

        //              List<Vw_JCard> BillList = new List<Vw_JCard>();
        //              CogitoStreamLineEntities dc = new CogitoStreamLineEntities();


        //              BillList = dc.Vw_JCard.Where(x => x.Pk_JobCardID == sInvno).Select(x => x).OfType<Vw_JCard>().ToList();

        ////               SELECT "JobCardMaster"."Pk_JobCardID", "JobCardMaster"."JDate", "BoxMaster"."Name", "Inv_Material"."Name", 
        ////              "JobCardMaster"."Fk_Order", "JobCardMaster"."Invno", "JobCardMaster"."Corrugation", "JobCardMaster"."TopPaperQty", 
        ////              "JobCardMaster"."TwoPlyQty", "JobCardMaster"."TwoPlyWt", "JobCardMaster"."PastingQty", "JobCardMaster"."PastingWstQty",
        ////              "JobCardMaster"."RotaryQty", "JobCardMaster"."RotaryWstQty", "JobCardMaster"."PunchingQty",
        ////              "JobCardMaster"."PunchingWstQty", "JobCardMaster"."SlotingQty", "JobCardMaster"."SlotingWstQty",
        ////              "JobCardMaster"."PinningQty", "JobCardMaster"."PinningWstQty", "JobCardMaster"."FinishingQty", 
        ////              "JobCardMaster"."FinishingWstQty", "JobCardMaster"."TotalQty", "JobCardMaster"."TotalWstQty", 
        ////              "gen_Customer"."CustomerName", "gen_Machine"."Name"
        ////FROM   (((("ERP"."dbo"."JobCardDetails" "JobCardDetails" INNER JOIN "ERP"."dbo"."JobCardMaster" "JobCardMaster" 
        ////              ON "JobCardDetails"."Fk_JobCardID"="JobCardMaster"."Pk_JobCardID") INNER JOIN "ERP"."dbo"."Inv_Material"
        ////              "Inv_Material" ON "JobCardDetails"."Fk_Material"="Inv_Material"."Pk_Material") INNER JOIN "ERP"."dbo"."BoxMaster"
        ////              "BoxMaster" ON "JobCardMaster"."Fk_BoxID"="BoxMaster"."Pk_BoxID") INNER JOIN "ERP"."dbo"."gen_Machine" "gen_Machine"
        ////              ON "JobCardMaster"."Machine"="gen_Machine"."Pk_Machine") INNER JOIN "ERP"."dbo"."gen_Customer" "gen_Customer" ON 
        ////              "BoxMaster"."Customer"="gen_Customer"."Pk_Customer"
        ////ORDER BY "Inv_Material"."Name"






        //              if (BillList.Count > 0)
        //              {
        //                  DataTable dt = new DataTable();

        //                  dt.Columns.Add("Pk_JobCardID");
        //                  dt.Columns.Add("JDate");
        //                  dt.Columns.Add("BName");
        //                  dt.Columns.Add("Name");
        //                  dt.Columns.Add("Fk_Order");
        //                  dt.Columns.Add("Invno");

        //                  dt.Columns.Add("Corrugation");
        //                  dt.Columns.Add("TopPaperQty");
        //                  dt.Columns.Add("TwoPlyQty");
        //                  dt.Columns.Add("TwoPlyWt");
        //                  dt.Columns.Add("PastingQty");

        //                  dt.Columns.Add("PastingWstQty");
        //                  dt.Columns.Add("RotaryQty");
        //                  dt.Columns.Add("RotaryWstQty");
        //                  dt.Columns.Add("PunchingQty");
        //                  dt.Columns.Add("PunchingWstQty");

        //                  dt.Columns.Add("SlotingQty");
        //                  dt.Columns.Add("SlotingWstQty");
        //                  dt.Columns.Add("PinningQty");
        //                  dt.Columns.Add("PinningWstQty");
        //                  dt.Columns.Add("FinishingQty");

        //                  dt.Columns.Add("FinishingWstQty");
        //                  dt.Columns.Add("TotalQty");
        //                  dt.Columns.Add("TotalWstQty");
        //                  dt.Columns.Add("CustomerName");
        //                  //dt.Columns.Add("Expr1");



        //                  foreach (Vw_JCard entity in BillList)
        //                  {
        //                      DataRow row = dt.NewRow();

        //                      row["Pk_JobCardID"] = entity.Pk_JobCardID;
        //                      row["JDate"] = entity.JDate;
        //                      row["BName"] = entity.BName;
        //                      row["Name"] = entity.Name;
        //                      row["Fk_Order"] = entity.Fk_Order;
        //                      row["Invno"] = entity.Invno;


        //                      row["Corrugation"] = entity.Corrugation;
        //                      row["TopPaperQty"] = entity.TopPaperQty;
        //                      row["TwoPlyQty"] = entity.TwoPlyQty;
        //                      row["TwoPlyWt"] = entity.TwoPlyWt;
        //                      row["PastingQty"] = entity.PastingQty;
        //                      row["PastingWstQty"] = entity.PastingWstQty;

        //                      row["RotaryQty"] = entity.RotaryQty;
        //                      row["RotaryWstQty"] = entity.RotaryWstQty;
        //                      row["PunchingQty"] = entity.PunchingQty;
        //                      row["PunchingWstQty"] = entity.PunchingWstQty;
        //                      row["SlotingQty"] = entity.SlotingQty;
        //                      row["SlotingWstQty"] = entity.SlotingWstQty;

        //                      row["PinningQty"] = entity.PinningQty;
        //                      row["PinningWstQty"] = entity.PinningWstQty;
        //                      row["FinishingQty"] = entity.FinishingQty;
        //                      row["FinishingWstQty"] = entity.FinishingWstQty;
        //                      row["TotalQty"] = entity.TotalQty;
        //                      row["TotalWstQty"] = entity.TotalWstQty;
        //                      row["CustomerName"] = entity.CustomerName;
        //                      //row["Name"] = entity.Expr1;
        //                      dt.Rows.Add(row);
        //                  }


        //                  DataSet ds = new DataSet();
        //                  ds.Tables.Add(dt);
        //                  CogitoStreamline.Report.JobCard orp = new CogitoStreamline.Report.JobCard();

        //                  orp.Load("@\\Report\\JobCard.rpt");
        //                  orp.SetDataSource(dt.DefaultView);

        //                  string pdfPath = Server.MapPath("~/ConvertPDF/" + "JobCard" + sInvno + ".pdf");
        //                  FileInfo file = new FileInfo(pdfPath);
        //                  if (file.Exists)
        //                  {
        //                      file.Delete();
        //                  }
        //                  var pd = new PrintDocument();


        //                  orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
        //                  orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
        //                  DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
        //                  objDiskOpt.DiskFileName = pdfPath;

        //                  orp.ExportOptions.DestinationOptions = objDiskOpt;
        //                  orp.Export();

        //              }
        //              return null;
        //          }
        //          catch (Exception ex)
        //          {
        //              return null;

        //          }
        //      }
        [HttpPost]
        public JsonResult JCList(string Fk_BoxID = "", string Fk_Enquiry = "", string SchNo = "", string Pk_Order = "", string BoxName = "", string PONo = "", string CustomerName = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Fk_BoxID", Fk_BoxID));
                oSearchParams.Add(new SearchParameter("Pk_Order", Pk_Order));
                oSearchParams.Add(new SearchParameter("BoxName", BoxName));
                oSearchParams.Add(new SearchParameter("CustomerName", CustomerName));
                //oSearchParams.Add(new SearchParameter("PONo", PONo));
                //oSearchParams.Add(new SearchParameter("SchNo", SchNo));
                //oSearchParams.Add(new SearchParameter("Fk_Enquiry", Fk_Enquiry));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oPaperStock.SearchJCList(oSearchParams);

                List<EntityObject> oPapersToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw__OpenJcList> oPaperObjects = oPapersToDisplayObjects.Select(p => p).OfType<Vw__OpenJcList>().ToList();

                var oPapersToDisplay = oPaperObjects.Where(p => p.Fk_Status == 1).Select(p => new
                {

                    //DeliveryDate = DateTime.Parse(p.DeliveryDate.ToString()).ToString("dd/MM/yyyy"),
                    Pk_BoxID = p.Pk_BoxID,
                    Pk_JobCardID = p.Pk_JobCardID,
                    BoxName = p.Name,
                    PrdQty = p.TotalQty,
                    OrderNo = p.Pk_Order,
                    OrderDate = DateTime.Parse(p.OrderDate.ToString()).ToString("dd/MM/yyyy"),
                    CustomerName = p.CustomerName,
                    //Pk_DelID = p.Pk_DeliverySechedule,
                    //CustPO = p.Cust_PO,
                    //EnquiryNo = p.Fk_Enquiry,
                    //Quantity = p.SchQty,
                    PName = p.PName,
                    //PartID = p.Pk_PartPropertyID,
                }).ToList();

                return Json(new { Result = "OK", Records = oPapersToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }



        public ActionResult JCRep1(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["JCNO"]);
                string pList = "";
                int i = 0;
                List<Vw_JCReport> BillList = new List<Vw_JCReport>();
                List<vw_JProcessList> ProcessList = new List<vw_JProcessList>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();


                BillList = dc.Vw_JCReport.Where(x => x.Pk_JobCardID == sInvno).Select(x => x).OfType<Vw_JCReport>().ToList();
                ProcessList = dc.vw_JProcessList.Where(x => x.Pk_JobCardID == sInvno).Select(x => x).OfType<vw_JProcessList>().ToList();

                foreach (vw_JProcessList entity in ProcessList)
                {
                    if (i == 0)
                    {
                        pList = ProcessList[i].ProcessName;
                    }
                    else
                    { pList = pList + "," + ProcessList[i].ProcessName; }
                    i = i + 1;
                }

                decimal NoOfPapers = 0;
                decimal PlySheets = 0;

                decimal TkUpFactor = 0;
                decimal OrderQty = 0;
                decimal PVal = 0;
                decimal CalcDecVal = 0;
                decimal DeckelVal;

                decimal BdSize = 0;
                decimal BoardSize = 0;

                if (BillList.Count > 0)
                {
                    DataTable dt = new DataTable();

                    dt.Columns.Add("Pk_JobCardID");
                    dt.Columns.Add("JDate");
                    dt.Columns.Add("BName");
                    dt.Columns.Add("CustomerName");
                    dt.Columns.Add("CuttingSize");
                    dt.Columns.Add("Rate");

                    dt.Columns.Add("PartTakeup");
                    dt.Columns.Add("BoardGSM");
                    dt.Columns.Add("BoardBS");
                    dt.Columns.Add("PlyVal");
                    //dt.Columns.Add("Pk_JobCardID");
                    dt.Columns.Add("Deckle");
                    dt.Columns.Add("PColor");
                    dt.Columns.Add("PDetails");
                    dt.Columns.Add("MFG");
                    dt.Columns.Add("DPRSc");
                    dt.Columns.Add("GSM");
                    dt.Columns.Add("BF");
                    dt.Columns.Add("TotalQty");
                    dt.Columns.Add("ChkQuality");

                    dt.Columns.Add("Printing");
                    dt.Columns.Add("OrdQty");
                    dt.Columns.Add("FluHt");

                    dt.Columns.Add("FluteName");
                    dt.Columns.Add("Weight");
                    dt.Columns.Add("DeliveryDate");
                    dt.Columns.Add("Tk_Factor");
                    dt.Columns.Add("Length");
                    dt.Columns.Add("Width");
                    dt.Columns.Add("Height");
                    //dt.Columns.Add("ReturnQuantity");
                    dt.Columns.Add("MillName");
                    dt.Columns.Add("ColorName");
                    dt.Columns.Add("SchQty");
                    dt.Columns.Add("PName");
                    dt.Columns.Add("PastingQty");
                    dt.Columns.Add("Corrugation");
                    dt.Columns.Add("TopPaperQty");
                    dt.Columns.Add("TwoPlyQty");

                    dt.Columns.Add("RotaryQty");
                    dt.Columns.Add("PunchingQty");
                    dt.Columns.Add("SlotingQty");
                    dt.Columns.Add("PinningQty");

                    dt.Columns.Add("FinishingQty");
                    //dt.Columns.Add("IssQty");
                    //dt.Columns.Add("RollNo");
                    //dt.Columns.Add("RetrunQuantity");
                    dt.Columns.Add("ECCQty");
                    dt.Columns.Add("GummingQty");
                    dt.Columns.Add("CutLength");
                    dt.Columns.Add("UpsVal");
                    dt.Columns.Add("ProdQty");

                    //dt.Columns.Add("ProcessName");
                    //dt.Columns.Add("PQuantity");
                    //dt.Columns.Add("Quantity");
                    //dt.Columns.Add("WastageQty");
                    //dt.Columns.Add("ProdQty");





                    foreach (Vw_JCReport entity in BillList)
                    {
                        DataRow row = dt.NewRow();

                        row["Pk_JobCardID"] = entity.Pk_JobCardID;
                        row["JDate"] = entity.JDate;
                        row["BName"] = entity.BName;
                        row["CustomerName"] = entity.CustomerName;
                        row["CuttingSize"] = entity.CuttingSize;
                        //row["Rate"] = entity.PartRate;
                        row["PartTakeup"] = entity.PartTakeup;
                        row["BoardGSM"] = entity.BoardGSM;
                        row["BoardBS"] = entity.BoardBS;
                        row["PlyVal"] = entity.PlyVal;
                        row["PColor"] = entity.PColor;
                        row["PDetails"] = entity.PDetails;
                        row["MFG"] = entity.MFG;
                        row["DPRSc"] = entity.DPRSc;
                        row["GSM"] = entity.GSM;
                        row["BF"] = entity.BF;
                        row["TotalQty"] = entity.TotalQty;
                        row["PName"] = entity.PName;
                        row["Printing"] = entity.Printing;
                        row["FluHt"] = entity.FluteHt;
                        row["FluteName"] = entity.FluteName;
                        row["Weight"] = entity.Weight;
                        row["DeliveryDate"] = entity.DeliveryDate;
                        row["Tk_Factor"] = entity.Tk_Factor;
                        row["Length"] = entity.Length;
                        row["Width"] = entity.Width;
                        row["Height"] = entity.Height;
                        row["Deckle"] = entity.Deckle;
                        row["ColorName"] = entity.ColorName;
                        row["MillName"] = entity.MillName;
                        row["SchQty"] = entity.SchQty;
                        row["ChkQuality"] = entity.ChkQuality;

                        row["PastingQty"] = entity.PastingQty;
                        row["Corrugation"] = entity.Corrugation;
                        row["TopPaperQty"] = entity.TopPaperQty;

                        row["TwoPlyQty"] = entity.TwoPlyQty;
                        row["RotaryQty"] = entity.RotaryQty;
                        row["PunchingQty"] = entity.PunchingQty;

                        row["SlotingQty"] = entity.SlotingQty;
                        row["PinningQty"] = entity.PinningQty;
                        row["FinishingQty"] = entity.FinishingQty;


                        row["TotalQty"] = entity.TotalQty;
                        row["ECCQty"] = entity.ECCQty;
                        row["GummingQty"] = entity.GummingQty;

                        //row["ReturnQuantity"] = entity.ReturnQuantity;
                        //row["IssQty"] = entity.IssQty;
                        //row["RollNo"] = entity.RollNo;

                        row["CutLength"] = entity.CutLength;
                        row["UpsVal"] = entity.UpsVal;
                        row["ProdQty"] = entity.ProdQty;
                        //row["ProcessName"] = entity.ProcessName;




                        //    row["ReturnQuantity"] = entity.ReturnQuantity;

                        TkUpFactor = Convert.ToDecimal(entity.UpsVal);
                        DeckelVal = Convert.ToDecimal(entity.Deckle);
                        OrderQty = Convert.ToDecimal(entity.SchQty);                 ////earlier it was order qty, it should be schqty, after linking production schedule
                        PVal = Convert.ToDecimal(entity.PlyVal);

                        //BdSize = DeckelVal - 2;

                        //CalcDecVal = Math.Truncate(1820 / DeckelVal);

                        //BoardSize = (BdSize * CalcDecVal) + 2;

                        BoardSize = DeckelVal;


                        if (TkUpFactor > 0)     ///FOR SGST
                        {
                            NoOfPapers = Convert.ToDecimal(OrderQty / TkUpFactor);

                            //PlySheets = Convert.ToDecimal(NoOfPapers * TkUpFactor);

                            if (PVal == 3)
                            {
                                PlySheets = Convert.ToDecimal(NoOfPapers * 1);
                            }
                            else
                                if (PVal == 5)
                                {
                                    PlySheets = Convert.ToDecimal(NoOfPapers * 2);
                                }

                                else if (PVal == 7)
                                {
                                    PlySheets = Convert.ToDecimal(NoOfPapers * 3);
                                }
                                else if (PVal == 9)
                                {
                                    PlySheets = Convert.ToDecimal(NoOfPapers * 4);
                                }
                        }

                        //  QtyReq = (NoOfPapers * TkUpFactor * CutLen / 100 * DCl / 100 * GSMVal / 100);
                        dt.Rows.Add(row);
                    }

                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);


                    CogitoStreamline.Report.JCRep orp = new CogitoStreamline.Report.JCRep();

                    orp.Load("@\\Report\\JCRep.rpt");
                    orp.SetDataSource(dt.DefaultView);


                    CrystalDecisions.CrystalReports.Engine.ReportDocument repDoc = orp;

                    ParameterFieldDefinitions crParameterFieldDefinitions3;
                    ParameterFieldDefinition crParameterFieldDefinition3;
                    ParameterValues crParameterValues3 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue3 = new ParameterDiscreteValue();
                    crParameterDiscreteValue3.Value = NoOfPapers;
                    crParameterFieldDefinitions3 = orp.DataDefinition.ParameterFields;
                    crParameterFieldDefinition3 = crParameterFieldDefinitions3["NoPly"];
                    crParameterValues3 = crParameterFieldDefinition3.CurrentValues;

                    crParameterValues3.Clear();
                    crParameterValues3.Add(crParameterDiscreteValue3);
                    crParameterFieldDefinition3.ApplyCurrentValues(crParameterValues3);

                    ParameterFieldDefinitions crParameterFieldDefinitions5;
                    ParameterFieldDefinition crParameterFieldDefinition5;
                    ParameterValues crParameterValues5 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue5 = new ParameterDiscreteValue();
                    crParameterDiscreteValue5.Value = BoardSize;
                    crParameterFieldDefinitions5 = orp.DataDefinition.ParameterFields;
                    crParameterFieldDefinition5 = crParameterFieldDefinitions5["BoardSize"];
                    crParameterValues5 = crParameterFieldDefinition5.CurrentValues;

                    crParameterValues5.Clear();
                    crParameterValues5.Add(crParameterDiscreteValue5);
                    crParameterFieldDefinition5.ApplyCurrentValues(crParameterValues5);



                    ParameterFieldDefinitions crParameterFieldDefinitions4;
                    ParameterFieldDefinition crParameterFieldDefinition4;
                    ParameterValues crParameterValues4 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                    crParameterDiscreteValue4.Value = PlySheets;
                    crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                    crParameterFieldDefinition4 = crParameterFieldDefinitions4["PlySh"];
                    crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                    crParameterValues4.Clear();
                    crParameterValues4.Add(crParameterDiscreteValue4);
                    crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);




                    ParameterFieldDefinitions crParameterFieldDefinitions2;
                    ParameterFieldDefinition crParameterFieldDefinition2;
                    ParameterValues crParameterValues2 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue2 = new ParameterDiscreteValue();
                    crParameterDiscreteValue2.Value = pList;
                    crParameterFieldDefinitions2 = orp.DataDefinition.ParameterFields;
                    crParameterFieldDefinition2 = crParameterFieldDefinitions2["ProcName"];
                    crParameterValues2 = crParameterFieldDefinition2.CurrentValues;

                    crParameterValues2.Clear();
                    crParameterValues2.Add(crParameterDiscreteValue2);
                    crParameterFieldDefinition2.ApplyCurrentValues(crParameterValues2);

                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "JCRep" + sInvno + ".pdf");
                    FileInfo file = new FileInfo(pdfPath);
                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    //orp.ExportToDisk(PdfFormatOptions, "JC");



                    orp.Export();


                    ///////////////////////


                }
                return null;
            }

            catch (Exception ex)
            {
                return null;

            }
        }

        public ActionResult JCRepPlnBrd(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["JCNO"]);
                string pList = "";
                int i = 0;
                List<Vw_JCReport> BillList = new List<Vw_JCReport>();
                List<vw_JProcessList> ProcessList = new List<vw_JProcessList>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();


                BillList = dc.Vw_JCReport.Where(x => x.Pk_JobCardID == sInvno).Select(x => x).OfType<Vw_JCReport>().ToList();
                ProcessList = dc.vw_JProcessList.Where(x => x.Pk_JobCardID == sInvno).Select(x => x).OfType<vw_JProcessList>().ToList();

                foreach (vw_JProcessList entity in ProcessList)
                {
                    if (i == 0)
                    {
                        pList = ProcessList[i].ProcessName;
                    }
                    else
                    { pList = pList + "," + ProcessList[i].ProcessName; }
                    i = i + 1;
                }

                decimal NoOfPapers = 0;
                decimal PlySheets = 0;

                decimal TkUpFactor = 0;
                decimal OrderQty = 0;
                decimal PVal = 0;
                decimal CalcDecVal = 0;
                decimal DeckelVal;

                decimal BdSize = 0;
                decimal BoardSize = 0;

                if (BillList.Count > 0)
                {
                    DataTable dt = new DataTable();

                    dt.Columns.Add("Pk_JobCardID");
                    dt.Columns.Add("JDate");
                    dt.Columns.Add("BName");
                    dt.Columns.Add("CustomerName");
                    dt.Columns.Add("CuttingSize");
                    dt.Columns.Add("Rate");

                    dt.Columns.Add("PartTakeup");
                    dt.Columns.Add("BoardGSM");
                    dt.Columns.Add("BoardBS");
                    dt.Columns.Add("PlyVal");
                    //dt.Columns.Add("Pk_JobCardID");
                    dt.Columns.Add("Deckle");
                    dt.Columns.Add("PColor");
                    dt.Columns.Add("PDetails");
                    dt.Columns.Add("MFG");
                    dt.Columns.Add("DPRSc");
                    dt.Columns.Add("GSM");
                    dt.Columns.Add("BF");
                    dt.Columns.Add("TotalQty");
                    dt.Columns.Add("ChkQuality");

                    dt.Columns.Add("Printing");
                    dt.Columns.Add("OrdQty");
                    dt.Columns.Add("FluHt");

                    dt.Columns.Add("FluteName");
                    dt.Columns.Add("Weight");
                    dt.Columns.Add("DeliveryDate");
                    dt.Columns.Add("Tk_Factor");
                    dt.Columns.Add("Length");
                    dt.Columns.Add("Width");
                    dt.Columns.Add("Height");
                    //dt.Columns.Add("ReturnQuantity");
                    dt.Columns.Add("MillName");
                    dt.Columns.Add("ColorName");
                    dt.Columns.Add("SchQty");
                    dt.Columns.Add("PName");
                    dt.Columns.Add("PastingQty");
                    dt.Columns.Add("Corrugation");
                    dt.Columns.Add("TopPaperQty");
                    dt.Columns.Add("TwoPlyQty");

                    dt.Columns.Add("RotaryQty");
                    dt.Columns.Add("PunchingQty");
                    dt.Columns.Add("SlotingQty");
                    dt.Columns.Add("PinningQty");

                    dt.Columns.Add("FinishingQty");
                    //dt.Columns.Add("IssQty");
                    //dt.Columns.Add("RollNo");
                    //dt.Columns.Add("RetrunQuantity");
                    dt.Columns.Add("ECCQty");
                    dt.Columns.Add("GummingQty");
                    dt.Columns.Add("CutLength");
                    dt.Columns.Add("UpsVal");
                    dt.Columns.Add("ProdQty");

                    //dt.Columns.Add("ProcessName");
                    //dt.Columns.Add("PQuantity");
                    //dt.Columns.Add("Quantity");
                    //dt.Columns.Add("WastageQty");
                    //dt.Columns.Add("ProdQty");





                    foreach (Vw_JCReport entity in BillList)
                    {
                        DataRow row = dt.NewRow();

                        row["Pk_JobCardID"] = entity.Pk_JobCardID;
                        row["JDate"] = entity.JDate;
                        row["BName"] = entity.BName;
                        row["CustomerName"] = entity.CustomerName;
                        row["CuttingSize"] = entity.CuttingSize;
                        //row["Rate"] = entity.PartRate;
                        row["PartTakeup"] = entity.PartTakeup;
                        row["BoardGSM"] = entity.BoardGSM;
                        row["BoardBS"] = entity.BoardBS;
                        row["PlyVal"] = entity.PlyVal;
                        row["PColor"] = entity.PColor;
                        row["PDetails"] = entity.PDetails;
                        row["MFG"] = entity.MFG;
                        row["DPRSc"] = entity.DPRSc;
                        row["GSM"] = entity.GSM;
                        row["BF"] = entity.BF;
                        row["TotalQty"] = entity.TotalQty;
                        row["PName"] = entity.PName;
                        row["Printing"] = entity.Printing;
                        row["FluHt"] = entity.FluteHt;
                        row["FluteName"] = entity.FluteName;
                        row["Weight"] = entity.Weight;
                        row["DeliveryDate"] = entity.DeliveryDate;
                        row["Tk_Factor"] = entity.Tk_Factor;
                        row["Length"] = entity.Length;
                        row["Width"] = entity.Width;
                        row["Height"] = entity.Height;
                        row["Deckle"] = entity.Deckle;
                        row["ColorName"] = entity.ColorName;
                        row["MillName"] = entity.MillName;
                        row["SchQty"] = entity.SchQty;
                        row["ChkQuality"] = entity.ChkQuality;

                        row["PastingQty"] = entity.PastingQty;
                        row["Corrugation"] = entity.Corrugation;
                        row["TopPaperQty"] = entity.TopPaperQty;

                        row["TwoPlyQty"] = entity.TwoPlyQty;
                        row["RotaryQty"] = entity.RotaryQty;
                        row["PunchingQty"] = entity.PunchingQty;

                        row["SlotingQty"] = entity.SlotingQty;
                        row["PinningQty"] = entity.PinningQty;
                        row["FinishingQty"] = entity.FinishingQty;


                        row["TotalQty"] = entity.TotalQty;
                        row["ECCQty"] = entity.ECCQty;
                        row["GummingQty"] = entity.GummingQty;

                        //row["ReturnQuantity"] = entity.ReturnQuantity;
                        //row["IssQty"] = entity.IssQty;
                        //row["RollNo"] = entity.RollNo;

                        row["CutLength"] = entity.CutLength;
                        row["UpsVal"] = entity.UpsVal;
                        row["ProdQty"] = entity.ProdQty;
                        //row["ProcessName"] = entity.ProcessName;




                        //    row["ReturnQuantity"] = entity.ReturnQuantity;

                        TkUpFactor = Convert.ToDecimal(entity.UpsVal);
                        DeckelVal = Convert.ToDecimal(entity.Deckle);
                        OrderQty = Convert.ToDecimal(entity.SchQty);                 ////earlier it was order qty, it should be schqty, after linking production schedule
                        PVal = Convert.ToDecimal(entity.PlyVal);

                        BdSize = DeckelVal - 2;

                        CalcDecVal = Math.Truncate(182 / DeckelVal);

                        BoardSize = (BdSize * CalcDecVal) + 2;




                        if (TkUpFactor > 0)     ///FOR SGST
                        {
                            NoOfPapers = Convert.ToDecimal(OrderQty / TkUpFactor);

                            //PlySheets = Convert.ToDecimal(NoOfPapers * TkUpFactor);

                            if (PVal == 3)
                            {
                                PlySheets = Convert.ToDecimal(NoOfPapers * 1);
                            }
                            else
                                if (PVal == 5)
                                {
                                    PlySheets = Convert.ToDecimal(NoOfPapers * 2);
                                }

                                else if (PVal == 7)
                                {
                                    PlySheets = Convert.ToDecimal(NoOfPapers * 3);
                                }
                                else if (PVal == 9)
                                {
                                    PlySheets = Convert.ToDecimal(NoOfPapers * 4);
                                }
                        }

                        //  QtyReq = (NoOfPapers * TkUpFactor * CutLen / 100 * DCl / 100 * GSMVal / 100);
                        dt.Rows.Add(row);
                    }

                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);


                    CogitoStreamline.Report.JCRep_PlnBrd orp = new CogitoStreamline.Report.JCRep_PlnBrd();

                    orp.Load("@\\Report\\JCRep_PlnBrd.rpt");
                    orp.SetDataSource(dt.DefaultView);


                    CrystalDecisions.CrystalReports.Engine.ReportDocument repDoc = orp;

                    ParameterFieldDefinitions crParameterFieldDefinitions3;
                    ParameterFieldDefinition crParameterFieldDefinition3;
                    ParameterValues crParameterValues3 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue3 = new ParameterDiscreteValue();
                    crParameterDiscreteValue3.Value = NoOfPapers;
                    crParameterFieldDefinitions3 = orp.DataDefinition.ParameterFields;
                    crParameterFieldDefinition3 = crParameterFieldDefinitions3["NoPly"];
                    crParameterValues3 = crParameterFieldDefinition3.CurrentValues;

                    crParameterValues3.Clear();
                    crParameterValues3.Add(crParameterDiscreteValue3);
                    crParameterFieldDefinition3.ApplyCurrentValues(crParameterValues3);

                    ParameterFieldDefinitions crParameterFieldDefinitions5;
                    ParameterFieldDefinition crParameterFieldDefinition5;
                    ParameterValues crParameterValues5 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue5 = new ParameterDiscreteValue();
                    crParameterDiscreteValue5.Value = BoardSize;
                    crParameterFieldDefinitions5 = orp.DataDefinition.ParameterFields;
                    crParameterFieldDefinition5 = crParameterFieldDefinitions5["BoardSize"];
                    crParameterValues5 = crParameterFieldDefinition5.CurrentValues;

                    crParameterValues5.Clear();
                    crParameterValues5.Add(crParameterDiscreteValue5);
                    crParameterFieldDefinition5.ApplyCurrentValues(crParameterValues5);



                    ParameterFieldDefinitions crParameterFieldDefinitions4;
                    ParameterFieldDefinition crParameterFieldDefinition4;
                    ParameterValues crParameterValues4 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                    crParameterDiscreteValue4.Value = PlySheets;
                    crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                    crParameterFieldDefinition4 = crParameterFieldDefinitions4["PlySh"];
                    crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                    crParameterValues4.Clear();
                    crParameterValues4.Add(crParameterDiscreteValue4);
                    crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);




                    ParameterFieldDefinitions crParameterFieldDefinitions2;
                    ParameterFieldDefinition crParameterFieldDefinition2;
                    ParameterValues crParameterValues2 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue2 = new ParameterDiscreteValue();
                    crParameterDiscreteValue2.Value = pList;
                    crParameterFieldDefinitions2 = orp.DataDefinition.ParameterFields;
                    crParameterFieldDefinition2 = crParameterFieldDefinitions2["ProcName"];
                    crParameterValues2 = crParameterFieldDefinition2.CurrentValues;

                    crParameterValues2.Clear();
                    crParameterValues2.Add(crParameterDiscreteValue2);
                    crParameterFieldDefinition2.ApplyCurrentValues(crParameterValues2);

                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "JCRep_PlnBrd" + sInvno + ".pdf");
                    FileInfo file = new FileInfo(pdfPath);
                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    //orp.ExportToDisk(PdfFormatOptions, "JC");



                    orp.Export();


                    ///////////////////////


                }
                return null;
            }

            catch (Exception ex)
            {
                return null;

            }
        }


        public ActionResult IssueRep1(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["JCNO"]);
                int sOrdNo = Convert.ToInt32(oValues["OrdNo"]);


                List<Vw_JCIssReport> IssueList = new List<Vw_JCIssReport>();

                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();



                IssueList = dc.Vw_JCIssReport.Where(x => x.Pk_JobCardID == sInvno && x.Pk_Order == sOrdNo).Select(x => x).OfType<Vw_JCIssReport>().ToList();



                if (IssueList.Count > 0)
                {
                    DataTable dt1 = new DataTable();

                    dt1.Columns.Add("Pk_JobCardID");
                    dt1.Columns.Add("Pk_MaterialIssueID");
                    dt1.Columns.Add("IssueDate");
                    dt1.Columns.Add("IssQty");
                    dt1.Columns.Add("RollNo");
                    dt1.Columns.Add("GSM");

                    dt1.Columns.Add("BF");
                    dt1.Columns.Add("Deckle");
                    dt1.Columns.Add("Pk_Material");
                    //dt1.Columns.Add("ColorName");
                    //dt1.Columns.Add("Pk_IssueReturnMasterId");
                    //dt1.Columns.Add("IssueReturnDate");
                    //dt1.Columns.Add("ReturnQuantity");
                    //dt1.Columns.Add("RetRellNo");
                    dt1.Columns.Add("Pk_MaterialIssueDetailsID");




                    foreach (Vw_JCIssReport entity in IssueList)
                    {
                        DataRow row2 = dt1.NewRow();
                        row2["Pk_MaterialIssueDetailsID"] = entity.Pk_MaterialIssueDetailsID;
                        row2["Pk_JobCardID"] = entity.Pk_JobCardID;
                        row2["Pk_MaterialIssueID"] = entity.Pk_MaterialIssueID;
                        row2["IssueDate"] = entity.IssueDate;
                        row2["IssQty"] = entity.IssQty;
                        row2["RollNo"] = entity.RollNo;
                        row2["GSM"] = entity.GSM;
                        row2["BF"] = entity.BF;
                        row2["Deckle"] = entity.Deckle;
                        row2["Pk_Material"] = entity.Pk_Material;
                        //row2["ColorName"] = entity.ColorName;
                        //row2["Pk_IssueReturnMasterId"] = entity.Pk_IssueReturnMasterId;
                        //row2["IssueReturnDate"] = entity.IssueReturnDate;
                        //row2["ReturnQuantity"] = entity.ReturnQuantity;
                        //row2["RetRellNo"] = entity.RetRellNo;



                        //  QtyReq = (NoOfPapers * TkUpFactor * CutLen / 100 * DCl / 100 * GSMVal / 100);
                        dt1.Rows.Add(row2);
                    }

                    DataSet ds1 = new DataSet();
                    ds1.Tables.Add(dt1);
                    CogitoStreamline.Report.IssJCRep orp1 = new CogitoStreamline.Report.IssJCRep();

                    orp1.Load("@\\Report\\IssJCRep");
                    orp1.SetDataSource(dt1.DefaultView);


                    CrystalDecisions.CrystalReports.Engine.ReportDocument repDoc1 = orp1;



                    string pdfPath1 = Server.MapPath("~/ConvertPDF/" + "IssueRep" + sInvno + ".pdf");
                    FileInfo file1 = new FileInfo(pdfPath1);
                    if (file1.Exists)
                    {
                        file1.Delete();
                    }
                    var pd1 = new PrintDocument();


                    orp1.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp1.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt1 = new DiskFileDestinationOptions();
                    objDiskOpt1.DiskFileName = pdfPath1;

                    orp1.ExportOptions.DestinationOptions = objDiskOpt1;
                    //orp.ExportToDisk(PdfFormatOptions, "JC");



                    orp1.Export();
                    //////////////////////////
                }
                ///////////////////////



                return null;
            }

            catch (Exception ex)
            {
                return null;

            }
        }






        public ActionResult ProcessRep1(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["JCNO"]);
                int sOrdNo = Convert.ToInt32(oValues["OrdNo"]);
                int sJNo = Convert.ToInt32(oValues["JNo"]);


                List<Vw_JobProcessSteps> ProcessList = new List<Vw_JobProcessSteps>();

                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();



                ProcessList = dc.Vw_JobProcessSteps.Where(x => x.Pk_JobCardID == sJNo).Select(x => x).OfType<Vw_JobProcessSteps>().ToList();



                if (ProcessList.Count > 0)
                {
                    DataTable dt1 = new DataTable();

                    dt1.Columns.Add("Pk_JobCardID");
                    dt1.Columns.Add("ProcessName");
                    dt1.Columns.Add("Pk_ID");
                    dt1.Columns.Add("Quantity");
                    dt1.Columns.Add("WastageQty");
                    dt1.Columns.Add("ProcessDate");

                    dt1.Columns.Add("PQuantity");
                    //dt1.Columns.Add("PStartTime");
                    //dt1.Columns.Add("PEndTime");
                    //dt1.Columns.Add("Pk_IssueReturnMasterId");
                    //dt1.Columns.Add("IssueReturnDate");
                    //dt1.Columns.Add("ReturnQuantity");
                    //dt1.Columns.Add("RetRellNo");
                    //dt1.Columns.Add("Pk_MaterialIssueDetailsID");




                    foreach (Vw_JobProcessSteps entity in ProcessList)
                    {
                        DataRow row2 = dt1.NewRow();
                        row2["ProcessName"] = entity.ProcessName;
                        row2["Pk_JobCardID"] = entity.Pk_JobCardID;
                        row2["Pk_ID"] = entity.Pk_ID;
                        row2["Quantity"] = entity.Quantity;
                        row2["WastageQty"] = entity.WastageQty;
                        row2["ProcessDate"] = entity.ProcessDate;
                        row2["PQuantity"] = entity.PQuantity;
                        //row2["PStartTime"] = entity.PStartTime;
                        //row2["PEndTime"] = entity.PEndTime;


                        //row2["MillName"] = entity.MillName;
                        //row2["ColorName"] = entity.ColorName;
                        //row2["Pk_IssueReturnMasterId"] = entity.Pk_IssueReturnMasterId;
                        //row2["IssueReturnDate"] = entity.IssueReturnDate;
                        //row2["ReturnQuantity"] = entity.ReturnQuantity;
                        //row2["RetRellNo"] = entity.RetRellNo;



                        //  QtyReq = (NoOfPapers * TkUpFactor * CutLen / 100 * DCl / 100 * GSMVal / 100);
                        dt1.Rows.Add(row2);
                    }

                    DataSet ds1 = new DataSet();
                    ds1.Tables.Add(dt1);
                    CogitoStreamline.Report.JobProcessList orp1 = new CogitoStreamline.Report.JobProcessList();

                    orp1.Load("@\\Report\\JobProcessList");
                    orp1.SetDataSource(dt1.DefaultView);


                    CrystalDecisions.CrystalReports.Engine.ReportDocument repDoc1 = orp1;



                    string pdfPath1 = Server.MapPath("~/ConvertPDF/" + "ProcessRep" + sInvno + ".pdf");
                    FileInfo file1 = new FileInfo(pdfPath1);
                    if (file1.Exists)
                    {
                        file1.Delete();
                    }
                    var pd1 = new PrintDocument();


                    orp1.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp1.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt1 = new DiskFileDestinationOptions();
                    objDiskOpt1.DiskFileName = pdfPath1;

                    orp1.ExportOptions.DestinationOptions = objDiskOpt1;
                    //orp.ExportToDisk(PdfFormatOptions, "JC");



                    orp1.Export();
                    //////////////////////////
                }
                ///////////////////////



                return null;
            }

            catch (Exception ex)
            {
                return null;

            }
        }



        [HttpPost]
        public override JsonResult Save(string data)
        {
            Dictionary<string, object> values = JsonConvert.DeserializeObject<Dictionary<string, object>>(data);


            data = JsonConvert.SerializeObject(values);
            //this.CurrentObject = null;
            return base.Save(data);


        }




        [HttpPost]
        public JsonResult GetMax(string data = "")
        {

            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            int sInvno = Convert.ToInt32(oValues["JCID"]);
            //decimal? RQty = 0;
            CogitoStreamLineEntities dc = new CogitoStreamLineEntities();
            List<JobProcess> BillList = new List<JobProcess>();
            //BillList = dc.StockTransferMaster.Where(x => x.Pk_TransferId).Select(x => x).OfType<StockTransferMaster>().ToList();
            var BillCount =
                dc.JobProcess.Count();

            if (BillCount == 0)
            {
                var BillMax = 0;
                return Json(BillMax);
            }
            else
            {

                var BillMax =
                   (from JobProcess in dc.JobProcess
                    where JobProcess.Fk_JobCardID == sInvno
                    orderby JobProcess.Pk_ID descending

                    select JobProcess.RemainingQty).Take(1);

                //   decimal IdVal = 0;

                //   IdVal =BillMax;
                //    JobProcess oJCProcess = _oEntites.JobProcess.Where(p => p.Pk_ID==IdVal).SingleOrDefault();
                //var  RQty = oJCProcess.RemainingQty;


                return Json(BillMax);
            }
        }

        //[HttpPost]
        //public JsonResult GetMaxProcessID(string data = "")
        //{

        //    CogitoStreamLineEntities dc = new CogitoStreamLineEntities();
        //    List<JobProcess> BillList = new List<JobProcess>();
        //    //BillList = dc.StockTransferMaster.Where(x => x.Pk_TransferId).Select(x => x).OfType<StockTransferMaster>().ToList();
        //    var BillCount =
        //        dc.JobProcess.Count();

        //    if (BillCount == 0)
        //    {
        //        var BillMax = 0;
        //        return Json(BillMax);
        //    }
        //    else
        //    {

        //        var BillMax =
        //           (from JobProcess in dc.JobProcess

        //            orderby JobProcess.Pk_ID descending

        //            select JobProcess.Pk_ID).Take(1);
        //        return Json(BillMax);
        //    }
        //}


        [HttpPost]
        public JsonResult GetDirectWaste(string data = "")
        {

            decimal Actual = 0;
            decimal CalcA = 0;
            decimal CalcB = 0;
            decimal TWastage = 0;
            decimal TotalWastage = 0;

            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            int sInvno = Convert.ToInt32(oValues["JCNO"]);

            JobCardMaster oJCBox = _oEntites.JobCardMaster.Where(p => p.Pk_JobCardID == sInvno).Single();

            var BoxID = oJCBox.Fk_BoxID;

            List<Vw_BoxDet> oBoxMast = new List<Vw_BoxDet>();
            oBoxMast = _oEntites.Vw_BoxDet.Where(p => p.Pk_BoxID == BoxID).ToList();
            int j = 0;
            decimal sBoxDeckel = 0;
            for (j = 0; j < oBoxMast.Count(); j++)
            {
                sBoxDeckel = Convert.ToDecimal(oBoxMast.ElementAt(j).Deckle);
            }

            Actual = sBoxDeckel - Convert.ToDecimal(20);

            List<Vw_JCIssReport> IssueList = new List<Vw_JCIssReport>();

            CogitoStreamLineEntities dc1 = new CogitoStreamLineEntities();

            //int RCount1 = _oEntites.MaterialIssue.Where(p => p.Fk_JobCardID == sInvno).Count();
            //if (RCount1 > 0)


            IssueList = dc1.Vw_JCIssReport.Where(x => x.Pk_JobCardID == sInvno).Select(x => x).OfType<Vw_JCIssReport>().ToList();


            if (IssueList.Count > 0)
            {
                DataTable dt1 = new DataTable();

                dt1.Columns.Add("Pk_JobCardID");
                dt1.Columns.Add("Pk_MaterialIssueID");
                dt1.Columns.Add("IssueDate");
                dt1.Columns.Add("IssQty");
                dt1.Columns.Add("RollNo");
                dt1.Columns.Add("GSM");

                dt1.Columns.Add("BF");
                dt1.Columns.Add("Deckle");
                dt1.Columns.Add("Weight");

                dt1.Columns.Add("Pk_MaterialIssueDetailsID");



                foreach (Vw_JCIssReport entity in IssueList)
                {
                    DataRow row2 = dt1.NewRow();
                    row2["Pk_MaterialIssueDetailsID"] = entity.Pk_MaterialIssueDetailsID;
                    row2["Pk_JobCardID"] = entity.Pk_JobCardID;
                    row2["Pk_MaterialIssueID"] = entity.Pk_MaterialIssueID;
                    row2["IssueDate"] = entity.IssueDate;
                    row2["IssQty"] = entity.IssQty;
                    row2["RollNo"] = entity.RollNo;
                    row2["GSM"] = entity.GSM;
                    row2["BF"] = entity.BF;
                    row2["Deckle"] = entity.Deckle;


                    CalcA = Actual + 20;
                    CalcB = CalcA - Actual;

                    TWastage = Convert.ToInt32(entity.Deckle) - Convert.ToInt32((CalcA + CalcB));
                    row2["Weight"] = entity.Weight;

                    MaterialIssueDetail oIssueMat = _oEntites.MaterialIssueDetails.Where(p => p.Pk_MaterialIssueDetailsID == entity.Pk_MaterialIssueDetailsID && p.Fk_IssueID == entity.Pk_MaterialIssueID).Single();

                    oIssueMat.Weight = TWastage;
                    _oEntites.SaveChanges();


                    TotalWastage = TotalWastage + TWastage;

                    dt1.Rows.Add(row2);
                }
                //var BillCount =
                //    dc.JobProcess.Count();

                //if (BillCount == 0)
                //{
                //    var BillMax = 0;
                //    return Json(BillMax);
                //}
                //else
                //{

                //    var BillMax =
                //       (from JobProcess in dc.JobProcess

                //        orderby JobProcess.Pk_ID descending

                //        select JobProcess.Pk_ID).Take(1);
                return Json(TotalWastage);
            }
            else
            {
                return Json(TotalWastage);
            }

        }



        public ActionResult DirectWastage(string data = "")    ///made for direct wastage
        {
            try
            {

                decimal Actual = 0;
                decimal CalcA = 0;
                //decimal CalcB = 0;
                float TWastageDeckle = 0;
                float TotalWastage = 0;

                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["JCNO"]);
                //decimal sBoxDeckel = Convert.ToDecimal(oValues["BoxDeckel"]);

                //Actual = Convert.ToDecimal(sBoxDeckel) - Convert.ToDecimal(20);

                //List<Vw_JCIssReport> IssueList = new List<Vw_JCIssReport>();

                //CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                JobCardMaster oJCBox = _oEntites.JobCardMaster.Where(p => p.Pk_JobCardID == sInvno).Single();

                var BoxID = oJCBox.Fk_BoxID;

                List<Vw_BoxDet> oBoxMast = new List<Vw_BoxDet>();
                oBoxMast = _oEntites.Vw_BoxDet.Where(p => p.Pk_BoxID == BoxID).ToList();

                decimal CutLength = 0;
                decimal BoxWt = 0;
                decimal BoardArea = 0;

                int j = 0;
                decimal sBoxDeckel = 0;
                for (j = 0; j < oBoxMast.Count(); j++)
                {
                    sBoxDeckel = Convert.ToDecimal(oBoxMast.ElementAt(j).Deckle);
                    CutLength = Convert.ToDecimal(oBoxMast.ElementAt(j).CuttingSize);
                    BoxWt = Convert.ToDecimal(oBoxMast.ElementAt(j).Weight);
                    BoardArea = Convert.ToDecimal(oBoxMast.ElementAt(j).BoardArea);
                }

                Actual = sBoxDeckel;

                List<Vw_JCIssReport> IssueList = new List<Vw_JCIssReport>();

                CogitoStreamLineEntities dc1 = new CogitoStreamLineEntities();

                int RCount1 = _oEntites.MaterialIssue.Where(p => p.Fk_JobCardID == sInvno).Count();
                if (RCount1 > 0)

                    IssueList = dc1.Vw_JCIssReport.Where(x => x.Pk_JobCardID == sInvno).Select(x => x).OfType<Vw_JCIssReport>().ToList();



                if (IssueList.Count > 0)
                {
                    DataTable dt1 = new DataTable();

                    dt1.Columns.Add("Pk_JobCardID");
                    dt1.Columns.Add("Pk_MaterialIssueID");
                    dt1.Columns.Add("IssueDate");
                    dt1.Columns.Add("IssQty");
                    dt1.Columns.Add("RollNo");
                    dt1.Columns.Add("GSM");

                    dt1.Columns.Add("BF");
                    dt1.Columns.Add("Deckle");
                    dt1.Columns.Add("Weight");
                    dt1.Columns.Add("MillName");
                    dt1.Columns.Add("BName");
                    dt1.Columns.Add("ColorName");
                    dt1.Columns.Add("Pk_MaterialIssueDetailsID");




                    foreach (Vw_JCIssReport entity in IssueList)
                    {
                        DataRow row2 = dt1.NewRow();
                        row2["Pk_MaterialIssueDetailsID"] = entity.Pk_MaterialIssueDetailsID;
                        row2["Pk_JobCardID"] = entity.Pk_JobCardID;
                        row2["Pk_MaterialIssueID"] = entity.Pk_MaterialIssueID;
                        row2["IssueDate"] = entity.IssueDate;
                        row2["IssQty"] = entity.IssQty;
                        row2["RollNo"] = entity.RollNo;
                        row2["GSM"] = entity.GSM;
                        row2["BF"] = entity.BF;
                        row2["Deckle"] = entity.Deckle;
                        row2["ColorName"] = entity.ColorName;
                        row2["BName"] = entity.BName;
                        row2["MillName"] = entity.MillName;


                        CalcA = Actual;
                        //   CalcB = CalcA - Actual;
                        float DVal = (float)entity.Deckle;
                        float FCalcA = (float)CalcA;
                        TWastageDeckle = DVal - FCalcA;
                        row2["Weight"] = entity.Weight;

                        MaterialIssueDetail oIssueMat = _oEntites.MaterialIssueDetails.Where(p => p.Pk_MaterialIssueDetailsID == entity.Pk_MaterialIssueDetailsID && p.Fk_IssueID == entity.Pk_MaterialIssueID).Single();

                        oIssueMat.Weight = Convert.ToDecimal(TWastageDeckle);
                        _oEntites.SaveChanges();

                        TotalWastage = TotalWastage + TWastageDeckle;

                        dt1.Rows.Add(row2);
                    }

                    var PerSqVal = BoxWt / BoardArea;
                    var Area = CutLength * Convert.ToDecimal(CalcA);

                    var WastageWt = Math.Round(PerSqVal * Area);

                    DataSet ds1 = new DataSet();
                    ds1.Tables.Add(dt1);
                    CogitoStreamline.Report.WastageRep orp1 = new CogitoStreamline.Report.WastageRep();

                    orp1.Load("@\\Report\\WastageRep");
                    orp1.SetDataSource(dt1.DefaultView);



                    ParameterFieldDefinitions crParameterFieldDefinitions5;
                    ParameterFieldDefinition crParameterFieldDefinition5;
                    ParameterValues crParameterValues5 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue5 = new ParameterDiscreteValue();
                    crParameterDiscreteValue5.Value = WastageWt;
                    crParameterFieldDefinitions5 = orp1.DataDefinition.ParameterFields;
                    crParameterFieldDefinition5 = crParameterFieldDefinitions5["TotalWaste"];
                    crParameterValues5 = crParameterFieldDefinition5.CurrentValues;

                    crParameterValues5.Clear();
                    crParameterValues5.Add(crParameterDiscreteValue5);
                    crParameterFieldDefinition5.ApplyCurrentValues(crParameterValues5);


                    string pdfPath1 = Server.MapPath("~/ConvertPDF/" + "DirectWastage" + sInvno + ".pdf");
                    FileInfo file1 = new FileInfo(pdfPath1);
                    if (file1.Exists)
                    {
                        file1.Delete();
                    }
                    var pd1 = new PrintDocument();


                    orp1.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp1.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt1 = new DiskFileDestinationOptions();
                    objDiskOpt1.DiskFileName = pdfPath1;

                    orp1.ExportOptions.DestinationOptions = objDiskOpt1;
                    //orp.ExportToDisk(PdfFormatOptions, "JC");



                    orp1.Export();
                    //////////////////////////
                }
                ///////////////////////



                return null;
            }

            catch (Exception ex)
            {
                return null;

            }
        }







        public ActionResult WastageRep(string data = "")
        {
            try
            {

                //decimal Actual = 0;
                //decimal CalcA = 0;
                //decimal CalcB = 0;
                decimal TWastage = 0;
                decimal TotalWastage = 0;

                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["JCNO"]);


                List<Vw_ProcessWastage> WastageList = new List<Vw_ProcessWastage>();

                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                WastageList = dc.Vw_ProcessWastage.Where(x => x.Pk_JobCardID == sInvno).Select(x => x).OrderBy(x => x.Pk_ID).OfType<Vw_ProcessWastage>().ToList();

                if (WastageList.Count > 0)
                {
                    DataTable dt1 = new DataTable();

                    dt1.Columns.Add("Pk_ID");
                    dt1.Columns.Add("Fk_ProcessID");
                    dt1.Columns.Add("WastageQty");
                    dt1.Columns.Add("Quantity");
                    dt1.Columns.Add("ProcessDate");
                    dt1.Columns.Add("PQuantity");

                    //dt1.Columns.Add("Name");
                    dt1.Columns.Add("ProcessName");
                    //dt1.Columns.Add("Weight");
                    dt1.Columns.Add("WQty");

                    dt1.Columns.Add("Pk_JobCardID");




                    foreach (Vw_ProcessWastage entity in WastageList)
                    {
                        DataRow row2 = dt1.NewRow();
                        row2["Pk_ID"] = entity.Pk_ID;
                        row2["Pk_JobCardID"] = entity.Pk_JobCardID;
                        row2["Fk_ProcessID"] = entity.Fk_ProcessID;
                        row2["WastageQty"] = entity.WastageQty;
                        row2["Quantity"] = entity.Quantity;
                        row2["ProcessDate"] = entity.ProcessDate;
                        row2["PQuantity"] = entity.PQuantity;
                        //row2["Name"] = entity.Name;
                        row2["ProcessName"] = entity.ProcessName;
                        //row2["Weight"] = entity.Weight;
                        row2["WQty"] = entity.WQty;

                        //CalcA = Actual + 20;
                        //CalcB = CalcA - Actual;

                        //TWastage = Convert.ToInt32(entity.Deckle) - Convert.ToInt32((CalcA + CalcB));
                        //row2["Weight"] = entity.Weight;

                        //MaterialIssueDetail oIssueMat = _oEntites.MaterialIssueDetails.Where(p => p.Pk_MaterialIssueDetailsID == entity.Pk_MaterialIssueDetailsID && p.Fk_IssueID == entity.Pk_MaterialIssueID).Single();

                        //oIssueMat.Weight = TWastage;
                        //_oEntites.SaveChanges();


                        //TotalWastage = TotalWastage + TWastage;

                        dt1.Rows.Add(row2);
                    }



                    DataSet ds1 = new DataSet();
                    ds1.Tables.Add(dt1);
                    CogitoStreamline.Report.Wastage orp1 = new CogitoStreamline.Report.Wastage();

                    orp1.Load("@\\Report\\Wastage");
                    orp1.SetDataSource(dt1.DefaultView);


                    string pdfPath1 = Server.MapPath("~/ConvertPDF/" + "ProcessWastage-" + sInvno + ".pdf");
                    FileInfo file1 = new FileInfo(pdfPath1);
                    if (file1.Exists)
                    {
                        file1.Delete();
                    }
                    var pd1 = new PrintDocument();


                    orp1.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp1.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt1 = new DiskFileDestinationOptions();
                    objDiskOpt1.DiskFileName = pdfPath1;

                    orp1.ExportOptions.DestinationOptions = objDiskOpt1;
                    //orp.ExportToDisk(PdfFormatOptions, "JC");



                    orp1.Export();
                    //////////////////////////
                }



                return null;



                //if (IssueList.Count > 0)
                //{
                //    DataTable dt1 = new DataTable();

                //    dt1.Columns.Add("Pk_JobCardID");
                //    dt1.Columns.Add("Pk_MaterialIssueID");
                //    dt1.Columns.Add("IssueDate");
                //    dt1.Columns.Add("IssQty");
                //    dt1.Columns.Add("RollNo");
                //    dt1.Columns.Add("GSM");

                //    dt1.Columns.Add("BF");
                //    dt1.Columns.Add("Deckle");
                //    dt1.Columns.Add("Weight");
                //    //dt1.Columns.Add("ColorName");
                //    //dt1.Columns.Add("Pk_IssueReturnMasterId");
                //    //dt1.Columns.Add("IssueReturnDate");
                //    //dt1.Columns.Add("ReturnQuantity");
                //    //dt1.Columns.Add("RetRellNo");
                //    dt1.Columns.Add("Pk_MaterialIssueDetailsID");




                //    foreach (Vw_JCIssReport entity in IssueList)
                //    {
                //        DataRow row2 = dt1.NewRow();
                //        row2["Pk_MaterialIssueDetailsID"] = entity.Pk_MaterialIssueDetailsID;
                //        row2["Pk_JobCardID"] = entity.Pk_JobCardID;
                //        row2["Pk_MaterialIssueID"] = entity.Pk_MaterialIssueID;
                //        row2["IssueDate"] = entity.IssueDate;
                //        row2["IssQty"] = entity.IssQty;
                //        row2["RollNo"] = entity.RollNo;
                //        row2["GSM"] = entity.GSM;
                //        row2["BF"] = entity.BF;
                //        row2["Deckle"] = entity.Deckle;


                //        CalcA = Actual + 20;
                //        CalcB = CalcA - Actual;

                //        TWastage = Convert.ToInt32(entity.Deckle) - Convert.ToInt32((CalcA + CalcB));
                //        row2["Weight"] = entity.Weight;

                //        MaterialIssueDetail oIssueMat = _oEntites.MaterialIssueDetails.Where(p => p.Pk_MaterialIssueDetailsID == entity.Pk_MaterialIssueDetailsID && p.Fk_IssueID == entity.Pk_MaterialIssueID).Single();

                //        oIssueMat.Weight = TWastage;
                //        _oEntites.SaveChanges();


                //        TotalWastage = TotalWastage + TWastage;

                //        dt1.Rows.Add(row2);
                //    }



                //    DataSet ds1 = new DataSet();
                //    ds1.Tables.Add(dt1);
                //    CogitoStreamline.Report.WastageRep orp1 = new CogitoStreamline.Report.WastageRep();

                //    orp1.Load("@\\Report\\WastageRep");
                //    orp1.SetDataSource(dt1.DefaultView);


                //    ParameterFieldDefinitions crParameterFieldDefinitions5;
                //    ParameterFieldDefinition crParameterFieldDefinition5;
                //    ParameterValues crParameterValues5 = new ParameterValues();
                //    ParameterDiscreteValue crParameterDiscreteValue5 = new ParameterDiscreteValue();
                //    crParameterDiscreteValue5.Value = TotalWastage;
                //    crParameterFieldDefinitions5 = orp1.DataDefinition.ParameterFields;
                //    crParameterFieldDefinition5 = crParameterFieldDefinitions5["TotalWaste"];
                //    crParameterValues5 = crParameterFieldDefinition5.CurrentValues;

                //    crParameterValues5.Clear();
                //    crParameterValues5.Add(crParameterDiscreteValue5);
                //    crParameterFieldDefinition5.ApplyCurrentValues(crParameterValues5);


                //    string pdfPath1 = Server.MapPath("~/ConvertPDF/" + "WastageRep" + sInvno + ".pdf");
                //    FileInfo file1 = new FileInfo(pdfPath1);
                //    if (file1.Exists)
                //    {
                //        file1.Delete();
                //    }
                //    var pd1 = new PrintDocument();


                //    orp1.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                //    orp1.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                //    DiskFileDestinationOptions objDiskOpt1 = new DiskFileDestinationOptions();
                //    objDiskOpt1.DiskFileName = pdfPath1;

                //    orp1.ExportOptions.DestinationOptions = objDiskOpt1;
                //    //orp.ExportToDisk(PdfFormatOptions, "JC");



                //    orp1.Export();
                //    //////////////////////////
                //}
                //return null;
            }

            catch (Exception ex)
            {
                return null;

            }
        }



        // List<Vw_JobProcessSteps> ProcessList = new List<Vw_JobProcessSteps>();

        //CogitoStreamLineEntities dc = new CogitoStreamLineEntities();



        //ProcessList = dc.Vw_JobProcessSteps.Where(x => x.Pk_JobCardID == sJNo).Select(x => x).OfType<Vw_JobProcessSteps>().ToList();

        public JsonResult GetIssueCount(string data = "")
        {
            var IssueCtr = 0;
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            int sInvno = Convert.ToInt32(oValues["JCNO"]);

            int RCount1 = _oEntites.MaterialIssue.Where(p => p.Fk_JobCardID == sInvno).Count();
            if (RCount1 > 0)
            {
                IssueCtr = RCount1;
            }

            //List<gen_Machine> oListOfCustomers = oMachine.Search(null).ListOfRecords.OfType<gen_Machine>().ToList();
            //var oCustomersToDisplay = oListOfCustomers.Select(p => new
            //{
            //    Name = p.Name,
            //    Id = p.Pk_Machine
            //});

            return Json(IssueCtr);
        }



        [HttpPost]

        public JsonResult SaveNew([System.Web.Http.FromBody]string data)
        {
            data = data.TrimStart('=');
            Dictionary<string, object> values = JsonConvert.DeserializeObject<Dictionary<string, object>>(data);
            data = JsonConvert.SerializeObject(values);
            return base.Save(data);


        }


        public JsonResult getProcessVal(string data = "")
        {
            var IssueCtr = 0;
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            int sInvno = Convert.ToInt32(oValues["JCID"]);
            decimal? RQty = 0;
            int RCount1 = _oEntites.JobProcess.Where(p => p.Fk_JobCardID == sInvno).Count();
            if (RCount1 > 0)
            {

                JobProcess oJCProcess = _oEntites.JobProcess.Where(p => p.Fk_JobCardID == sInvno).LastOrDefault();
                RQty = oJCProcess.RemainingQty;


                // IssueCtr = RCount1;
            }

            //List<gen_Machine> oListOfCustomers = oMachine.Search(null).ListOfRecords.OfType<gen_Machine>().ToList();
            //var oCustomersToDisplay = oListOfCustomers.Select(p => new
            //{
            //    Name = p.Name,
            //    Id = p.Pk_Machine
            //});

            return Json(RQty);
        }



        public ActionResult UpdateStk(string data = "")
        {
            try
            {

                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);


                var sInvno = Convert.ToInt32(oValues["Pk_JobCardID"]);                
                //int BoxID = Convert.ToInt32(oValues["BoxID"]);
                int StockVal = Convert.ToInt32(oValues["StockVal"]);
                //decimal PartID = Convert.ToDecimal(oValues["PartID"]);
                    int ItemID = Convert.ToInt32(oValues["ItemID"]);
              
                //int PartId = Convert.ToInt32(oValues["PartId"]);
                    //decimal BId = Convert.ToInt32(oValues["ItemID"]);
CogitoStreamLineEntities dc = new CogitoStreamLineEntities();
                var ProcessID =
                  (from JobProcess in dc.JobProcess where JobProcess.Fk_JobCardID == sInvno orderby JobProcess.Pk_ID descending  select JobProcess.Pk_ID).Take(1);

                Vw_BoxDet OPart = _oEntites.Vw_BoxDet.Where(p => p.Pk_BoxID == ItemID).FirstOrDefault();
                var Pid = Convert.ToDecimal(OPart.Pk_PartPropertyID);

                if (ItemID > 0)
                {
                    int RCount1 = _oEntites.BoxStock.Where(p => p.Fk_BoxID == ItemID && p.Fk_PartID == Pid).Count();

                    if (RCount1 == 1)
                    {

                        BoxStock oBoxList = _oEntites.BoxStock.Where(p => p.Fk_BoxID == ItemID && p.Fk_PartID == Pid).Single();

                        var ExQty = oBoxList.Quantity;
                        var TotQty = ExQty + StockVal;
                        oBoxList.Quantity = TotQty;
                        _oEntites.SaveChanges();
                    }
                    else if (RCount1 == 0)
                    {

                        //Vw_BoxDet oBoxList = _oEntites.Vw_BoxDet.Where(x => x.Pk_BoxID == BoxID).FirstOrDefault();
                        BoxStock oBoxStock = new BoxStock();

                        oBoxStock.Fk_BoxID = ItemID;
                        oBoxStock.Quantity = StockVal;
                        oBoxStock.Fk_PartID = Pid;
                        _oEntites.AddToBoxStock(oBoxStock);
                        _oEntites.SaveChanges();
                    }

                  decimal PID = ProcessID.FirstOrDefault();

                   JobProcess oProcess = _oEntites.JobProcess.Where(x => x.Pk_ID == PID).FirstOrDefault();
                   oProcess.fk_boxid = ItemID;

                    oProcess.StockUpd = StockVal;
                    _oEntites.SaveChanges();

                }



                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }

    }
}


