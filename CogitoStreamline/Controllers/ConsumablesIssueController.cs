﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;
using System.IO;
using System.Data;
using CrystalDecisions.ReportAppServer.ReportDefModel;
using System.Drawing.Printing;
using CrystalDecisions.Shared;

namespace CogitoStreamline.Controllers
{
    public class ConsumablesIssueController : CommonController
    {
        //
        // GET: /MaterialIndent/
        //  Customer oCustomer = new Customer();
        WebGareCore.DomainModel.Tenants oTanent = new WebGareCore.DomainModel.Tenants();
    //    ConsumableIssue oIssue = new ConsumableIssue();
        ConsumableIssue oInwd = new ConsumableIssue();
        public ConsumablesIssueController()
        {
            oDoaminObject = new ConsumableIssue();
        }

        public override ActionResult Index()
        {
            ViewBag.Header = "Consumables Issues";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }





        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                ConsumableIssue oMaterialInward = oDoaminObject as ConsumableIssue;
                oMaterialInward.ID = decimal.Parse(pId);
                ConsumablesIssue oAtualObject = oMaterialInward.DAO as ConsumablesIssue;

                int i = -1;

                var oMaterials = oAtualObject.ConsumableIssueDet.Select(p => new
                {
                    slno = ++i,
                    Quantity = p.Quantity,
                 //   AccQty = p.AccQty,
                    Fk_Material = p.Fk_Material,
                    Name = p.Inv_Material.Name,
                    Pk_ID_Det = p.Pk_ID_Det,
                    //Price = p.Price,
                    //RollNo = p.RollNo,
                    //Color = p.Inv_Material.gen_Color.ColorName,
                    //Mill = p.Inv_Material.gen_Mill.MillName,



                });
                //Create a anominious object here to break the circular reference
                var oMaterialInwardToDisplay = new
                {
                    Pk_ID = oAtualObject.Pk_ID,
                    IssueDate = DateTime.Parse(oAtualObject.IssueDate.ToString()).ToString("dd/MM/yyyy"),
                    //Pur_InvDate = DateTime.Parse(oAtualObject.Pur_InvDate.ToString()).ToString("dd/MM/yyyy"),
                    //Fk_Indent = oAtualObject.Fk_Indent,
                    //Fk_QC = oAtualObject.Fk_QC,
                    //PONo = oAtualObject.PONo,
                    //Pur_InvNo = oAtualObject.Pur_InvNo,

                    //Fk_Material = oAtualObject.Fk_Material,
                    //Quantity = oAtualObject.Quantity,                    
                    ////Quality_Qty=oAtualObject.QualityCheck.Quantity,
                    //Price = oAtualObject.Price,
                    //Weight = oAtualObject.Weight,
                    ////SQuantity=oAtualObject.Inv_Material.Stocks.First().Quantity,

                    //SQuantity=oAtualObject.Inv_Material.Stocks.First().Quantity!= null ? oAtualObject.Inv_Material.Stocks.First().Quantity.ToString() : "",
                    //= p.Inv_MaterialIndentMaster.Fk_VendorId != null ? p.Inv_MaterialIndentMaster.gen_Vendor.VendorName : "",
                    //TanentName = oAtualObject.Inv_MaterialIndentMaster.wgTenant.TanentName,
                    MaterialData = Json(oMaterials).Data
                };

                return Json(new { success = true, data = oMaterialInwardToDisplay });
            }
            else
            {
                var oMaterialInwardToDisplay = new ConsumablesIssue();
                return Json(new { success = true, data = oMaterialInwardToDisplay });
            }
        }

        [HttpPost]
        public JsonResult ConsumablesStockList(string Pk_Material = "", string MatName = "")
        {
            try
            {
                //Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                //string Fk_OrderID = oValues["Fk_OrderID"];

                List<SearchParameter> oSearchParams = new List<SearchParameter>();


                oSearchParams.Add(new SearchParameter("Pk_Material", Pk_Material));
                oSearchParams.Add(new SearchParameter("MatName", MatName));
                //oSearchParams.Add(new SearchParameter("PName", PName));
                //oSearchParams.Add(new SearchParameter("CustomerName", CustomerName));
                SearchResult oSearchResult = oInwd.SearchOthersStock(oSearchParams);
                List<EntityObject> oOrdersToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_OthersStock> oOrderObjects = oOrdersToDisplayObjects.Select(p => p).OfType<Vw_OthersStock>().ToList();



                var oDeliveryScheduleToDisplay = oOrderObjects.Select(p => new
                {
                    MatName = p.MatName,
                    CatName = p.CatName,
                    Quantity = p.Quantity,
                    Fk_MaterialCategory = p.Fk_MaterialCategory,
                    Pk_Material = p.Pk_Material,
                   Pk_Stock= p.Pk_Stock,

                }).ToList();

                return Json(new { Result = "OK", Records = oDeliveryScheduleToDisplay, TotalRecordCount = oDeliveryScheduleToDisplay.Count() });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult ConsumablesListByFiter(string Pk_ID = null, string FromDate = "", string ToDate = "",  int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_ID", Pk_ID));
                oSearchParams.Add(new SearchParameter("FromDate", FromDate));
                oSearchParams.Add(new SearchParameter("ToDate", ToDate));
                //oSearchParams.Add(new SearchParameter("Vendor", Vendor));
                //oSearchParams.Add(new SearchParameter("Material", Material));


                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oMaterialInwardsToDisplayObjects = oSearchResult.ListOfRecords;
                List<ConsumablesIssue> oMaterialInwardObjects = oMaterialInwardsToDisplayObjects.Select(p => p).OfType<ConsumablesIssue>().ToList();

                //Create a anominious object here to break the circular reference
                var oMaterialInwardsToDisplay = oMaterialInwardObjects.Select(p => new
                {

                    Pk_ID = p.Pk_ID,
                    IssueDate = p.IssueDate != null ? DateTime.Parse(p.IssueDate.ToString()).ToString("dd/MM/yyyy") : "",
                    IssuedBy=p.IssuedBy,
                   IssuedTo= p.IssuedTo,
              
                    //Fk_Indent = p.Inv_MaterialIndentMaster.Pk_MaterialOrderMasterId,
                    //VendorName = p.PurchaseOrderM.Fk_Vendor != null ? p.PurchaseOrderM.gen_Vendor.VendorName : "",
                    //PONo = p.PONo,
                    //PkDisp = p.PurchaseOrderM.PkDisp,
                    //IndentNo = p.Fk_Indent,
                    //Pur_InvDate = p.Pur_InvDate != null ? DateTime.Parse(p.Pur_InvDate.ToString()).ToString("dd/MM/yyyy") : "",
                    //Pur_InvNo = p.Pur_InvNo,

                    //Fk_InwardBy = p.ideUser.FirstName,


                }).ToList();


                return Json(new { Result = "OK", Records = oMaterialInwardsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        //public ActionResult InwdRep(string data = "")
        //{
        //    try
        //    {
        //        Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        //        int sInvno = Convert.ToInt32(oValues["Pk_Inw"]);

        //        List<Vw_InwardRep> BillList = new List<Vw_InwardRep>();
        //        CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

        //        //BillList = dc.VwInvoices.ToList();
        //        BillList = dc.Vw_InwardRep.Where(x => x.Pk_ID == sInvno).Select(x => x).OfType<Vw_InwardRep>().ToList();


        //        //SELECT     dbo.ConsumablesIssue.Pk_ID, dbo.Inv_Material.Pk_Material, dbo.Inv_Material.Name, dbo.ConsumableIssueDet.Quantity,
        //        //dbo.ConsumablesIssue.IssueDate, dbo.ConsumablesIssue.Fk_QC, 
        //        //                      dbo.ConsumablesIssue.Fk_Indent, dbo.ConsumablesIssue.PONo, dbo.ConsumableIssueDet.RollNo
        //        //FROM         dbo.ConsumablesIssue INNER JOIN
        //        //                      dbo.ConsumableIssueDet ON dbo.ConsumablesIssue.Pk_ID = dbo.ConsumableIssueDet.Fk_Inward INNER JOIN
        //        //                      dbo.Inv_Material ON dbo.ConsumableIssueDet.Fk_Material = dbo.Inv_Material.Pk_Material


        //        if (BillList.Count > 0)
        //        {
        //            DataTable dt = new DataTable();

        //            dt.Columns.Add("Name");
        //            dt.Columns.Add("Pk_Material");
        //            dt.Columns.Add("Pk_ID");
        //            dt.Columns.Add("Fk_QC");
        //            dt.Columns.Add("IssueDate");
        //            dt.Columns.Add("Quantity");
        //            dt.Columns.Add("Fk_Indent");
        //            dt.Columns.Add("PONo");
        //            dt.Columns.Add("RollNo");
        //            dt.Columns.Add("ColorName");
        //            dt.Columns.Add("MillName");
        //            foreach (Vw_InwardRep entity in BillList)
        //            {
        //                DataRow row = dt.NewRow();

        //                row["Name"] = entity.Name;
        //                row["Pk_Material"] = entity.Pk_Material;
        //                row["Pk_ID"] = entity.Pk_ID;
        //                row["Fk_QC"] = entity.Fk_QC;
        //                row["IssueDate"] = entity.IssueDate;
        //                row["Quantity"] = entity.Quantity;
        //                row["Fk_Indent"] = entity.Fk_Indent;
        //                row["PONo"] = entity.PONo;
        //                row["RollNo"] = entity.RollNo;
        //                row["ColorName"] = entity.ColorName;
        //                row["MillName"] = entity.MillName;

        //                dt.Rows.Add(row);
        //            }


        //            DataSet ds = new DataSet();
        //            ds.Tables.Add(dt);
        //            CogitoStreamline.Report.MInward orp = new CogitoStreamline.Report.MInward();

        //            orp.Load("@\\Report\\MInward.rpt");
        //            orp.SetDataSource(dt.DefaultView);

        //            string pdfPath = Server.MapPath("~/ConvertPDF/" + "MInward" + sInvno + ".pdf");
        //            FileInfo file = new FileInfo(pdfPath);
        //            if (file.Exists)
        //            {
        //                file.Delete();
        //            }
        //            var pd = new PrintDocument();


        //            orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
        //            orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
        //            DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
        //            objDiskOpt.DiskFileName = pdfPath;

        //            orp.ExportOptions.DestinationOptions = objDiskOpt;
        //            orp.Export();

        //        }
        //        return null;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;

        //    }
        //}


        //public JsonResult InwardGetRec(string Pk_ID = "", string MaterialName = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        //{

        //    try
        //    {
        //        //  QualityChild oIssueDetails = new QualityChild();
        //        List<SearchParameter> oSearchParams = new List<SearchParameter>();
        //        oSearchParams.Add(new SearchParameter("Pk_ID", Pk_ID));
        //        oSearchParams.Add(new SearchParameter("MaterialName", MaterialName));
        //        oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
        //        oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

        //        SearchResult oSearchResult = oInwd.SearchInwdDet(oSearchParams);

        //        List<EntityObject> oIssueDetailsToDisplayObjects = oSearchResult.ListOfRecords;
        //        List<Vw_InwardRep> oIssueObjects = oIssueDetailsToDisplayObjects.Select(p => p).OfType<Vw_InwardRep>().ToList();

        //        //Create a anominious object here to break the circular reference
        //        var oIssueDetailsToDisplay = oIssueObjects.Select(p => new
        //        {
        //            PkMaterial = p.Pk_Material,
        //            PkInwd = p.Pk_ID,
        //            InwDate = p.IssueDate != null ? DateTime.Parse(p.IssueDate.ToString()).ToString("dd/MM/yyyy") : "",
        //            InwardDet = p.Pk_ID_Det,
        //            Name = p.Name,
        //            Quantity = p.Quantity,
        //            QC = p.Fk_QC,
        //            Indent = p.Fk_Indent,
        //            PONo = p.PONo,
        //            ReelNo = p.RollNo,
        //            Color = p.ColorName,
        //            Mill = p.MillName,
        //            Pk_Mill = p.Pk_Mill,
        //        }).ToList();

        //        return Json(new { Result = "OK", Records = oIssueDetailsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { Result = "ERROR", Message = "Select Quality Check No. and Proceed" });
        //    }
        //}

        [HttpPost]
        public JsonResult IssueGetRec(string Pk_ID = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {


            try
            {

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_ID", Pk_ID));
               // oSearchParams.Add(new SearchParameter("Pk_JobCardID", Pk_JobCardID));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oInwd.SearchIssueDetails(oSearchParams);

                List<EntityObject> oColorToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_ConsIssueList> oColorObjects = oColorToDisplayObjects.Select(p => p).OfType<Vw_ConsIssueList>().ToList();


                var oColorToDisplay = oColorObjects.Select(p => new
                {
                    Pk_ID = p.Pk_ID,
                    MaterialName = p.MatName,
                    Quantity = p.Quantity,
                    CatName = p.CatName
                     
                }).ToList();

                return Json(new { Result = "OK", Records = oColorToDisplay, TotalRecordCount = oSearchResult.RecordCount });

            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }


        }



        [HttpPost]

        public JsonResult SaveNew([System.Web.Http.FromBody]string data)
        {
            data = data.TrimStart('=');
            Dictionary<string, object> values = JsonConvert.DeserializeObject<Dictionary<string, object>>(data);
            data = JsonConvert.SerializeObject(values);
            return base.Save(data);


        }
    }
}
