﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;
using System.IO;
using System.Data;
using CrystalDecisions.ReportAppServer.ReportDefModel;
using System.Drawing.Printing;
using CrystalDecisions.Shared;
using System.Data.SqlClient;
namespace CogitoStreamLine.Controllers
{
    public class BOMController : CommonController
    {
        BoxM oBoxMaster = new BoxM();
        BoxTyp oBoxT = new BoxTyp();
        BoxM oItProp = new BoxM();
        Machine oMAC = new Machine();
        //Module oModule = new Module();
        //Role oRole = new Role();

        public BOMController()
        {
            oDoaminObject = new BoxM();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Bill of Materials";
            return base.Index();
        }
        protected override void Dispose(bool disposing)
        {
            oBoxMaster.Dispose();
            base.Dispose(disposing);
        }


        [HttpPost]
        public JsonResult getBox(string pId = "")
        {
            List<BoxMaster> oListOfBoxType = oBoxMaster.Search(null).ListOfRecords.OfType<BoxMaster>().ToList();

            var oBoxToDisplay = oListOfBoxType.Select(p => new
            {
                Name = p.Name,
                Id = p.Pk_BoxID

            });

            return Json(oBoxToDisplay);
        }
        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                BoxM oBox = oDoaminObject as BoxM;
                oBox.ID = decimal.Parse(pId);
                BoxMaster oAtualObject = oBox.DAO as BoxMaster;


                int i = -1;

                //var oBoxDetails = oAtualObject.BoxChilds.Select(p => new
                //{
                //    Pk_BoxCID = p.Pk_BoxCID,
                //    slno = ++i,
                //    Fk_Material = p.Fk_Material,
                //    txtFk_Material = p.Inv_Material.Name,
                //    Name = p.Inv_Material.Name
                //});


                //Create a anominious object here to break the circular reference
                var oHeadToDisplay = new
                {
                    Pk_BoxID = oAtualObject.Pk_BoxID,
                    Name = oAtualObject.Name,
                    Description = oAtualObject.Description,
                    //Customer = oAtualObject.gen_Customer.CustomerName,
                    PartNo = oAtualObject.PartNo,
                    Fk_BoxType = oAtualObject.Fk_BoxType,
                    //Ply = oAtualObject.Ply,
                    //BoxDetails = Json(oBoxDetails).Data
                };
                return Json(new { success = true, data = oHeadToDisplay });
            }
            else
            {
                var oHeadToDisplay = new BoxMaster();
                return Json(new { success = true, data = oHeadToDisplay });
            }
        }
        [HttpPost]
        public JsonResult NameDuplicateChecking(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sName = oValues["Name"];
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Name", sName));
                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);
                List<EntityObject> oHeadToDisplayObjects = oSearchResult.ListOfRecords;
                List<BoxMaster> oHeadObjects = oHeadToDisplayObjects.Select(p => p).OfType<BoxMaster>().ToList();
                var oHeadToDisplay = oHeadObjects.Select(p => new
                {
                    Pk_BoxId = p.Pk_BoxID,
                    Name = p.Name
                }).ToList().OrderBy(s => s.Name);
                return Json(new { Success = true, Records = oHeadToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult BoxNameListByFiter(string Name = "", string CustName = "", string Pk_BoxID = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Name", Name));
                oSearchParams.Add(new SearchParameter("CustName", CustName));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);
                List<EntityObject> oHeadToDisplayObjects = oSearchResult.ListOfRecords;

                List<BoxMaster> oHeadObjects = oHeadToDisplayObjects.Select(p => p).OfType<BoxMaster>().ToList();
                //Create a anominious object here to break the circular reference
                var oHeadToDisplay = oHeadObjects.Select(p => new
                {
                    Pk_BoxID = p.Pk_BoxID,
                    BTypeId = p.BoxType.Pk_BoxTypeID,
                    BoxType = p.BoxType.Name,
                    //Customer = p.gen_Customer.CustomerName,
                    Name = p.Name,
                    Description = p.Description,
                    PartNo = p.PartNo,
                    //Ply = p.Ply,

                }).ToList();
                return Json(new { Result = "OK", Records = oHeadToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


        [HttpPost]
        public JsonResult PODetails(string Name = "", string Pk_BoxID = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Name", Name));
                oSearchParams.Add(new SearchParameter("Pk_BoxID", Pk_BoxID));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oBoxMaster.SearchPSummary(oSearchParams);
                List<EntityObject> oHeadToDisplayObjects = oSearchResult.ListOfRecords;

                List<Vw_PaperSummary> oHeadObjects = oHeadToDisplayObjects.Select(p => p).OfType<Vw_PaperSummary>().ToList();
                //Create a anominious object here to break the circular reference
                var oHeadToDisplay = oHeadObjects.Where(p=>p.Quantity-p.InwdQty>0).Select(p => new
                {
                    Pk_BoxID = p.Pk_BoxID,
                    POQty=p.Quantity,
                    PoNo=p.Pk_PONo,
                    Name = p.Name,
                    PoDate = DateTime.Parse(p.PODate.ToString()).ToString("dd/MM/yyyy"),
                    MillName=p.MillName,                                       
                    InQty=p.InwdQty,
                    PendQty = (p.Quantity - p.InwdQty),

                }).ToList().OrderBy(s => s.Pk_BoxID);
                return Json(new { Result = "OK", Records = oHeadToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult BoxDetails(string Pk_BoxID = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                BoxM oBoxMaster = oDoaminObject as BoxM;

                oBoxMaster.ID = decimal.Parse(Pk_BoxID);

                BoxMaster oEnquiryObjects = oBoxMaster.DAO as BoxMaster;

                var oDeliveryScheduleToDisplay = oEnquiryObjects.BoxSpecs.Select(p => new
                {
                    Pk_BoxSpecID = p.Pk_BoxSpecID,
                    OYes = p.OuterShellYes,
                    CYes = p.CapYes,
                    LPYes = p.LenghtPartationYes,
                    WPYes = p.WidthPartationYes,
                    PYes = p.PlateYes


                }).ToList();

                return Json(new { Result = "OK", Records = oDeliveryScheduleToDisplay, TotalRecordCount = oDeliveryScheduleToDisplay.Count() });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


        [HttpPost]
        public JsonResult JobAssignedStock(string Pk_Material = "", string Fk_BoxID = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_Material", Pk_Material));
                oSearchParams.Add(new SearchParameter("Fk_BoxID", Fk_BoxID));
                //oSearchParams.Add(new SearchParameter("BoxName", BoxName));
                //oSearchParams.Add(new SearchParameter("Customer", Customer));
                //oSearchParams.Add(new SearchParameter("JDate", JDate));
                //oSearchParams.Add(new SearchParameter("Fk_Order", Fk_Order));
                //oSearchParams.Add(new SearchParameter("FromJDate", FromJDate));
                //oSearchParams.Add(new SearchParameter("ToJDate", ToJDate));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oBoxMaster.SearchAssignedStock(oSearchParams);

                List<EntityObject> oJobsToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_AssStock> oJobObjects = oJobsToDisplayObjects.Select(p => p).OfType<Vw_AssStock>().ToList();

                //Create a anominious object here to break the circular reference
                var oJobsToDisplay = oJobObjects.Where(p => p.Fk_Status == 1).Select(p => new
                {
                    Pk_JobCardID = p.Pk_JobCardID,
                    Pk_Material = p.Pk_Material,
                    BoxName = p.bName,
                    JDate = DateTime.Parse(p.JDate.ToString()).ToString("dd/MM/yyyy"),
                    MName = p.MName,
                    Quantity = p.RM_Consumed,
                    RollNo=p.RollNo,
                    Pk_JobCardDet = p.Pk_JobCardDet,
                }).ToList();

                return Json(new { Result = "OK", Records = oJobsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


        [HttpPost]
        public JsonResult PODet(string Pk_Material = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_Material", Pk_Material));
                //oSearchParams.Add(new SearchParameter("BoxName", BoxName));
                //oSearchParams.Add(new SearchParameter("Customer", Customer));
                //oSearchParams.Add(new SearchParameter("JDate", JDate));
                //oSearchParams.Add(new SearchParameter("Fk_Order", Fk_Order));
                //oSearchParams.Add(new SearchParameter("FromJDate", FromJDate));
                //oSearchParams.Add(new SearchParameter("ToJDate", ToJDate));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oBoxMaster.SearchPOD(oSearchParams);

                List<EntityObject> oJobsToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_PoBom> oJobObjects = oJobsToDisplayObjects.Select(p => p).OfType<Vw_PoBom>().ToList();

                //Create a anominious object here to break the circular reference
                var oJobsToDisplay = oJobObjects.Where(p => p.Fk_Status == 1).Select(p => new
                {
                    Pk_Material = p.Pk_Material,
                    Name = p.Name,
                    Pk_PONo = p.Pk_PONo,
                    PODate = DateTime.Parse(p.PODate.ToString()).ToString("dd/MM/yyyy"),
                    POQty = p.POQty,
                    AccQty = p.AccQty,
                    PendQty = p.PendQty,
                    IndNo = p.Fk_Indent
                   
                }).ToList();

                return Json(new { Result = "OK", Records = oJobsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public ActionResult SBoxDet(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);


            string pId = oValues["Pk_BoxID"];
            if (pId != "")
            {
                var BoxID = int.Parse(pId);
                ModelManuplationResult oResult = new ModelManuplationResult();

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_BoxID", pId));

                SearchResult oSearchResult = oItProp.SearchP(oSearchParams);

                List<EntityObject> oEnquirysToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_BoxDet> oEnquiryObjects = oEnquirysToDisplayObjects.Select(p => p).OfType<Vw_BoxDet>().ToList();

                var oEnquiryToDisplay = oEnquiryObjects.Select(p => new
                {

                    Length = p.Length,
                    Width = p.Width,
                    Height = p.Height,
                    BType = p.BType
                });

                return Json(new { success = true, data = oEnquiryToDisplay });

            }
            return null;
        }



        [HttpPost]
        public ActionResult SMachineDet(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);


            string pId = oValues["Pk_Machine"];
            if (pId != "")
            {
                var BoxID = int.Parse(pId);
                ModelManuplationResult oResult = new ModelManuplationResult();

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_Machine", pId));

                SearchResult oSearchResult = oBoxMaster.SearchMachine(oSearchParams);

                List<EntityObject> oEnquirysToDisplayObjects = oSearchResult.ListOfRecords;
                List<gen_Machine> oEnquiryObjects = oEnquirysToDisplayObjects.Select(p => p).OfType<gen_Machine>().ToList();

                var oEnquiryToDisplay = oEnquiryObjects.Select(p => new
                {

                    Capacity_Length = p.Capacity_Length,

                });

                return Json(new { success = true, data = oEnquiryToDisplay });

            }
            return null;
        }

 //       public ActionResult BomRep(string data = "")
 //       {
 //           try
 //           {


 //               List<Vw_PaperSummary> SummaryList = new List<Vw_PaperSummary>();
 //               List<Vw_AssStock> JCList = new List<Vw_AssStock>();

 //               List<Vw_PoBom> POList = new List<Vw_PoBom>();
 //               CogitoStreamLineEntities dc = new CogitoStreamLineEntities();


 //               Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
 //               int sInvno = Convert.ToInt32(oValues["fkbox"]);


 //               SqlConnection cn;



 //               cn = new SqlConnection("data Source=sql6005.site4now.net; Initial Catalog=DB_A3CED5_V2InnovativesTesting; Integrated Security=SSPI");

 //               SummaryList = dc.Vw_PaperSummary.Where(x => x.Pk_BoxID == sInvno).Select(x => x).OfType<Vw_PaperSummary>().ToList();

 //               cn.Open();

 //               SqlCommand scom1 = new SqlCommand("delete from BarcodeT", cn);
 //               scom1.ExecuteNonQuery();
 //               //DataTable dt = new DataTable();
 //               foreach (Vw_PaperSummary entity in SummaryList)
 //               {
 //                  // DataRow row = dt.NewRow();
 //                   SqlCommand scom = new SqlCommand("INSERT INTO BarcodeT (Fk_BoxID,Fk_Material,BName,MName,ExStk) VALUES (@BoxID,@FkMat,@BName,@MName,@ExStk)", cn);

 //                   scom.Parameters.Add("BoxID", SqlDbType.Int);
 //                   scom.Parameters["BoxID"].Value = sInvno;

 //                   scom.Parameters.Add("FkMat", SqlDbType.Int);
 //                   scom.Parameters["FkMat"].Value = entity.Pk_Material;

 //                   scom.Parameters.Add("BName", SqlDbType.NVarChar);
 //                   scom.Parameters["BName"].Value = entity.BName;

 //                   scom.Parameters.Add("MName", SqlDbType.NVarChar);
 //                   scom.Parameters["MName"].Value = entity.Name;

 //                   scom.Parameters.Add("ExStk", SqlDbType.Decimal);
 //                   scom.Parameters["ExStk"].Value = entity.Quantity;

 //                   var PkMat = entity.Pk_Material;
 //                   var MN = entity.Name;
 //                   var Stk = entity.Quantity;
 //                   var BN = entity.BName;

 //                   JCList = dc.Vw_AssStock.Where(x => x.Pk_Material == PkMat).Select(x => x).OfType<Vw_AssStock>().ToList();

 //                   foreach (Vw_AssStock entity1 in JCList)
 //                   {
 //                       // DataRow row = dt.NewRow();
 //                       SqlCommand scom2 = new SqlCommand("INSERT INTO BarcodeT (Fk_BoxID,Fk_Material,Fk_JobCardNo,JCDate,AssStkQty,MName,ExStk,BName) VALUES (@BoxID,@FkMat,@FkJC,@Dt,@SQty,@MName,@ExStk,@BName)", cn);

 //                       scom2.Parameters.Add("BoxID", SqlDbType.Int);
 //                       scom2.Parameters["BoxID"].Value = sInvno;

 //                       scom2.Parameters.Add("FkMat", SqlDbType.Int);
 //                       scom2.Parameters["FkMat"].Value = entity1.Pk_Material;

 //                       scom2.Parameters.Add("FkJC", SqlDbType.Int);
 //                       scom2.Parameters["FkJC"].Value = entity1.Pk_JobCardID;

 //                       scom2.Parameters.Add("Dt", SqlDbType.DateTime);
 //                       scom2.Parameters["Dt"].Value = entity1.JDate;

 //                       scom2.Parameters.Add("SQty", SqlDbType.Int);
 //                       scom2.Parameters["SQty"].Value = entity1.RM_Consumed;

 //                       scom2.Parameters.Add("MName", SqlDbType.NVarChar);
 //                       scom2.Parameters["MName"].Value = MN;

 //                       scom2.Parameters.Add("ExStk", SqlDbType.Decimal);
 //                       scom2.Parameters["ExStk"].Value = Stk;

 //                       scom2.Parameters.Add("BName", SqlDbType.NVarChar);
 //                       scom2.Parameters["BName"].Value = BN;
                        


 //                       scom2.ExecuteNonQuery();
 //                   }


 //                   POList = dc.Vw_PoBom.Where(x => x.Pk_Material == PkMat && x.PendQty>0).Select(x => x).OfType<Vw_PoBom>().ToList();

 //                   foreach (Vw_PoBom entity2 in POList)
 //                   {
 //                       // DataRow row = dt.NewRow();
 //                       SqlCommand scom3 = new SqlCommand("INSERT INTO BarcodeT (Fk_BoxID,Fk_Material,Fk_PoNo,PODate,InwdQty,POQty,PendingQty,MName,ExStk,BName) VALUES (@BoxID,@FkMat,@FkPO,@Dt,@InwdQty,@POQty,@PQty,@MName,@ExStk,@BName)", cn);


 //                       scom3.Parameters.Add("BoxID", SqlDbType.Int);
 //                       scom3.Parameters["BoxID"].Value = sInvno;

 //                       scom3.Parameters.Add("FkMat", SqlDbType.Int);
 //                       scom3.Parameters["FkMat"].Value = entity2.Pk_Material;

 //                       scom3.Parameters.Add("FkPO", SqlDbType.Int);
 //                       scom3.Parameters["FkPO"].Value = entity2.Pk_PONo;                      

 //                       scom3.Parameters.Add("Dt", SqlDbType.DateTime);
 //                       scom3.Parameters["Dt"].Value = entity2.PODate;

 //                       scom3.Parameters.Add("InwdQty", SqlDbType.Int);
 //                       scom3.Parameters["InwdQty"].Value = entity2.AccQty;

 //                       scom3.Parameters.Add("POQty", SqlDbType.Int);
 //                       scom3.Parameters["POQty"].Value = entity2.POQty;

 //                       scom3.Parameters.Add("PQty", SqlDbType.Int);
 //                       scom3.Parameters["PQty"].Value = entity2.PendQty;

 //                       scom3.Parameters.Add("MName", SqlDbType.NVarChar);
 //                       scom3.Parameters["MName"].Value = MN;

 //                       scom3.Parameters.Add("ExStk", SqlDbType.Decimal);
 //                       scom3.Parameters["ExStk"].Value = Stk;

 //                       scom3.Parameters.Add("BName", SqlDbType.NVarChar);
 //                       scom3.Parameters["BName"].Value = BN;

 //                       scom3.ExecuteNonQuery();
 //                   }


 //                   scom.ExecuteNonQuery();

 //               }

 //               //JCList = dc.Vw_AssStock.Where(x => x.Pk_Material == sInvno).Select(x => x).OfType<Vw_AssStock>().ToList();



 //               cn.Close();


 //               List<BarCodeT> BillList = new List<BarCodeT>();
 //               //CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

 //               //BillList = dc.VwInvoices.ToList();
 //               BillList = dc.BarCodeT.Where(x => x.Fk_Material >0 ).Select(x => x).OfType<BarCodeT>().ToList();

 ////                SELECT "BarCodeT"."Fk_BoxID", "BarCodeT"."Fk_Material", "BarCodeT"."Fk_PoNo", "BarCodeT"."Fk_JobCardNo", "BarCodeT"."JCDate", 
 ////               "BarCodeT"."AssStkQty", "BarCodeT"."PODate", "BarCodeT"."InwdQty", "BarCodeT"."POQty", "BarCodeT"."PendingQty", "Inv_Material"."Name", 
 ////               "BoxMaster"."Name"
 ////FROM   ("ERP"."dbo"."BarCodeT" "BarCodeT" INNER JOIN "ERP"."dbo"."Inv_Material" "Inv_Material" ON "BarCodeT"."Fk_Material"="Inv_Material"."Pk_Material")
 ////               INNER JOIN "ERP"."dbo"."BoxMaster" "BoxMaster" ON "BarCodeT"."Fk_BoxID"="BoxMaster"."Pk_BoxID"
 ////ORDER BY "BarCodeT"."Fk_BoxID", "BarCodeT"."Fk_Material", "BarCodeT"."Fk_JobCardNo", "BarCodeT"."Fk_PoNo"

 //             if (BillList.Count > 0)
 //               {
 //                   DataTable dt = new DataTable();

 //                   dt.Columns.Add("Fk_BoxID");
 //                   dt.Columns.Add("Fk_Material");
 //                   dt.Columns.Add("Fk_PoNo");
 //                   dt.Columns.Add("Fk_JobCardNo");
 //                   dt.Columns.Add("JCDate");
 //                   dt.Columns.Add("AssStkQty");
 //                   dt.Columns.Add("PODate");
 //                   dt.Columns.Add("InwdQty");

 //                   dt.Columns.Add("POQty");
 //                   dt.Columns.Add("PendingQty");
 //                   dt.Columns.Add("BName");
 //                   dt.Columns.Add("MName");
 //                   dt.Columns.Add("ExStk");

 //                   foreach (BarCodeT entity in BillList)
 //                   {
 //                       DataRow row = dt.NewRow();

 //                       row["Fk_BoxID"] = entity.Fk_BoxID;
 //                       row["Fk_Material"] = entity.Fk_Material;
 //                       row["Fk_PoNo"] = entity.Fk_PoNo;
 //                       row["Fk_JobCardNo"] = entity.Fk_JobCardNo;
 //                       row["JCDate"] = entity.JCDate;
 //                       row["AssStkQty"] = entity.AssStkQty;
 //                       row["PODate"] = entity.PODate;
 //                       row["InwdQty"] = entity.InwdQty;
 //                       row["POQty"] = entity.POQty;
 //                       row["PendingQty"] = entity.PendingQty;
 //                       row["BName"] = entity.BName;
 //                       row["MName"] = entity.MName;
 //                       row["ExStk"] = entity.ExStk;
 //                       //row["PlateYes"] = entity.PlateYes;
 //                       //row["MaterialName"] = entity.MaterialName;

 //                       dt.Rows.Add(row);
 //                   }


 //                   DataSet ds = new DataSet();
 //                   ds.Tables.Add(dt);
 //                   CogitoStreamline.Report.BOMRep orp = new CogitoStreamline.Report.BOMRep();

 //                   orp.Load("@\\Report\\BOMRep.rpt");
 //                   orp.SetDataSource(dt.DefaultView);

 //                   string pdfPath = Server.MapPath("~/ConvertPDF/" + "BomReport" + sInvno + ".pdf");
 //                   FileInfo file = new FileInfo(pdfPath);
 //                   if (file.Exists)
 //                   {
 //                       file.Delete();
 //                   }
 //                   var pd = new PrintDocument();


 //                   orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
 //                   orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
 //                   DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
 //                   objDiskOpt.DiskFileName = pdfPath;

 //                   orp.ExportOptions.DestinationOptions = objDiskOpt;
 //                   orp.Export();

 //               }
 //               return null;
 //           }
 //           catch (Exception ex)
 //           {
 //               return null;

 //           }
 //       }

        [HttpPost]
        public ActionResult InsertFunc(string data = "")
        {
            SqlConnection cn;
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            int sfkb = Convert.ToInt32(oValues["fkbox"]);


            cn = new SqlConnection("data Source=sql6005.site4now.net; Initial Catalog=DB_A3CED5_V2InnovativesTesting; Integrated Security=SSPI");

            SqlCommand scom = new SqlCommand("INSERT INTO TempCheck (Upd_Val) VALUES (@Address)", cn);
            scom.Parameters.Add("Address", SqlDbType.Int);
            scom.Parameters["Address"].Value = sfkb;

            cn.Open();
            scom.ExecuteNonQuery();
            //cn.Close();



            return null;
        }


    }
}

