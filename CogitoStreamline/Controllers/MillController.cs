﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;


namespace CogitoStreamline.Controllers
{
    public class MillController : CommonController
    {
        //
        // GET: /Mill/
        Mill oMill = new Mill();
        
        //StateMaster oState = new StateMaster();
        //CityMaster oCity = new CityMaster();

        public MillController()
        {
            oDoaminObject = new Mill();
        }

        protected override void Dispose(bool disposing)
        {
            //oState.Dispose();
            //oCity.Dispose();
            base.Dispose(disposing);
        }

        public override ActionResult Index()
        {
            ViewBag.Header = "Mill";
            return base.Index();
        }



        [HttpPost]
        public JsonResult MillDuplicateChecking(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sMillName = oValues["MillName"];

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("MillName", sMillName));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oMillToDisplayObjects = oSearchResult.ListOfRecords;
                List<gen_Mill> oMillObjects = oMillToDisplayObjects.Select(p => p).OfType<gen_Mill>().ToList();

                var oMillToDisplay = oMillObjects.Select(p => new
                {
                    Pk_Mill = p.Pk_Mill,
                    MillName = p.MillName
                }).ToList().OrderBy(s => s.MillName);

                return Json(new { Success = true, Records = oMillToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }


        [HttpPost]
        public JsonResult ContactDuplicateChecking(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sContactPersonName = oValues["ContactPersonName"];
                string sPk_Mill = oValues["Pk_Mill"];

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("ContactPersonName", sContactPersonName));
                oSearchParams.Add(new SearchParameter("Pk_Mill", sPk_Mill));
        
                    //SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                SearchResult oSearchResult = oMill.SearchNew(oSearchParams);


                List<EntityObject> oContactToDisplayObjects = oSearchResult.ListOfRecords;
                List<VW_MillDuplicate> oContactObjects = oContactToDisplayObjects.Select(p => p).OfType<VW_MillDuplicate>().ToList();

                var oContactToDisplay = oContactObjects.Select(p => new
                {
                    Fk_Mill = p.Fk_Mill,
                    ContactPersonName = p.ContactPersonName
                }).ToList().OrderBy(s => s.ContactPersonName);

                return Json(new { Success = true, Records = oContactToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }



        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                Mill oMill = oDoaminObject as Mill;
                oMill.ID = decimal.Parse(pId);
                gen_Mill oAtualObject = oMill.DAO as gen_Mill;

                var oContacts = oAtualObject.gen_MillContacts.Select(p => new
                {
                    id = p.gen_ContactPerson.Pk_ContactPersonId,
                    Name = p.gen_ContactPerson.ContactPersonName,
                    Details = "Number:" + p.gen_ContactPerson.ContactPersonNumber +
                              ";eMail:" + p.gen_ContactPerson.ContactPersonEmailId +
                              ";State:" + p.gen_ContactPerson.gen_State.StateName+
                              ";City:" + p.gen_ContactPerson.gen_City.CityName+
                              ";Street:" + p.gen_ContactPerson.Address_No_Street +
                              ";Pin:" + p.gen_ContactPerson.Address_PinCode
                });

                //Create a anominious object here to break the circular reference
                var oMillToDisplay = new
                {
                    Pk_Mill = oAtualObject.Pk_Mill,
                    MillName = oAtualObject.MillName,
                    Address=oAtualObject.Address,
                    ContactNumber = oAtualObject.ContactNumber,
                    EMail = oAtualObject.Email,
                    ContactPerson1 = oAtualObject.ContactPerson1,
                    LandLine = oAtualObject.LandLine,
                    Website = oAtualObject.Website,
                    SName=oAtualObject.SName,
                      Contacts = Json(oContacts).Data
                                   };

                return Json(new { success = true, data = oMillToDisplay });
            }
            else
            {
                var oMillToDisplay = new gen_Mill();
                return Json(new { success = true, data = oMillToDisplay });
            }
        }

        [HttpPost]
        public JsonResult MillListByFiter(string MillName = "",string Pk_Mill = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("MillName", MillName));
                oSearchParams.Add(new SearchParameter("Pk_Mill", Pk_Mill.ToString()));        
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oMillToDisplayObjects = oSearchResult.ListOfRecords;
                List<gen_Mill> oMillObjects = oMillToDisplayObjects.Select(p => p).OfType<gen_Mill>().ToList();

                //Create a anominious object here to break the circular reference
                var oMillToDisplay = oMillObjects.Select(p => new
                {
                    Pk_Mill = p.Pk_Mill,
                    MillName = p.MillName,
                    ContactNumber=p.ContactNumber,
                    ContactPerson1 = p.ContactPerson1,
                    LandLine = p.LandLine,
                    Email=p.Email,
                    SName = p.SName
                
                }).ToList().OrderBy(p=>p.MillName);

                return Json(new { Result = "OK", Records = oMillToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

    }
}

