﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;

using System.IO;

namespace CogitoStreamline.Controllers
{

    public class MaterialCategoryController : CommonController
    {
        Mat_Category oMatCategory = new Mat_Category();

        JobCardParts oPaperStock = new JobCardParts();

        public MaterialCategoryController()
        {
            oDoaminObject = new Mat_Category();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Material Category";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            oMatCategory.Dispose();
            base.Dispose(disposing);
        }


        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                Mat_Category oMatCategory = oDoaminObject as Mat_Category;
                oMatCategory.ID = decimal.Parse(pId);
                Inv_MaterialCategory oAtualObject = oMatCategory.DAO as Inv_MaterialCategory;
                //Create a anominious object here to break the circular reference

                var oMatCategoryToDisplay = new
                {

                    Pk_MaterialCategory = oAtualObject.Pk_MaterialCategory,
                    Name = oAtualObject.Name,
                    Description=oAtualObject.Description
                };

                return Json(new { success = true, data = oMatCategoryToDisplay });
            }
            else
            {
                var oMatCategoryToDisplay = new Inv_MaterialCategory();
                return Json(new { success = true, data = oMatCategoryToDisplay });
            }
        }
        
        [HttpPost]
        public JsonResult MCatDuplicateChecking(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sName = oValues["Name"];

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Name", sName));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oMatCategoryToDisplayObjects = oSearchResult.ListOfRecords;
                List<Inv_MaterialCategory> oMatCategoryObjects = oMatCategoryToDisplayObjects.Select(p => p).OfType<Inv_MaterialCategory>().ToList();

                var oMatCategoryToDisplay = oMatCategoryObjects.Select(p => new
                {
                    Pk_MaterialCategory = p.Pk_MaterialCategory,
                    Name = p.Name
                }).ToList().OrderBy(s => s.Name);

                return Json(new { Success = true, Records = oMatCategoryToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult MIssueListByFiter(string MaterialCategory = "",string JobCardID="", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("MaterialCategory", MaterialCategory));
                oSearchParams.Add(new SearchParameter("JobCardID", JobCardID));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oMatCategory.SearchJobCard(oSearchParams);

                List<EntityObject> oMatCategoryToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_JobIssue> oMatCategoryObjects = oMatCategoryToDisplayObjects.Select(p => p).OfType<Vw_JobIssue>().ToList();


           
                var oMatCategoryToDisplay = oMatCategoryObjects.Select(p => new
                {
                    Pk_Material = p.Pk_Material,
                    Name = p.Name,
                    Pk_PaperStock = p.Pk_PaperStock,
                  RollNo = p.RollNo !=null ? p.RollNo : "" ,
                    Quantity = p.RM_Consumed,
                    SName=p.SName,
                    Fk_Mill=p.Pk_Mill,
                    Color=p.ColorName,
                }).ToList();

                   
                    return Json(new { Result = "OK", Records = oMatCategoryToDisplay, TotalRecordCount = oSearchResult.RecordCount });
                 }
          
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        
        }




        [HttpPost]
        public JsonResult MIssueList(string MaterialCategory = "", string JobCardID = "", string Name = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("MaterialCategory", MaterialCategory));
                oSearchParams.Add(new SearchParameter("JobCardID", JobCardID));
                oSearchParams.Add(new SearchParameter("Name", Name));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oPaperStock.SearchJCPart(oSearchParams);

                List<EntityObject> oMatCategoryToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_JCPartDet> oMatCategoryObjects = oMatCategoryToDisplayObjects.Select(p => p).OfType<Vw_JCPartDet>().ToList();



                var oMatCategoryToDisplay = oMatCategoryObjects.Select(p => new
                {
                    Pk_Material = p.Pk_Material,
                    Name = p.Name,
                    Pk_PaperStock = p.Pk_PaperStock,
                    RollNo = p.RollNo != null ? p.RollNo : "",
                    Quantity = p.RM_Consumed,
                    //SName = p.SName,
                    //Fk_Mill = p.Pk_Mill,
                    MillName=p.MillName
                }).ToList();


                return Json(new { Result = "OK", Records = oMatCategoryToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }

            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }

        }
        
        [HttpPost]
        public JsonResult MaterialSearchListByFiter(string Name = "",  string MaterialCategory = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("MaterialCategory", MaterialCategory));
                oSearchParams.Add(new SearchParameter("Name", Name));
                
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));
                 if (MaterialCategory != "Paper")
                {
                SearchResult oSearchResult = oMatCategory.SearchIssue(oSearchParams);

                List<EntityObject> oMatCategoryToDisplayObjects = oSearchResult.ListOfRecords;


                List<Vw_Popup_MaterialIssue> oMatCategoryObjects = oMatCategoryToDisplayObjects.Select(p => p).OfType<Vw_Popup_MaterialIssue>().ToList();

                    var oMatCategoryToDisplay = oMatCategoryObjects.Select(p => new
                    {
                        Pk_Material = p.Pk_Material,
                        Name = p.MaterialName,
                        Category = p.Name,
                        Quantity = p.Quantity,
                    Length=p.Length,
                    Width=p.Width,
                    Weight=p.Weight,
                        Ply = p.PPly,
                        Brand=p.Brand,
                        ColorName = p.ColorName,
                    }).ToList();

                      
                    return Json(new { Result = "OK", Records = oMatCategoryToDisplay, TotalRecordCount = oSearchResult.RecordCount });
                }
                else
                {
                    SearchResult oSearchResult = oMatCategory.SearchPaperIssue(oSearchParams);

                    List<EntityObject> oMatCategoryToDisplayObjects = oSearchResult.ListOfRecords;

                    List<vw_PaperRollStock> oMatCategoryObjects = oMatCategoryToDisplayObjects.Select(p => p).OfType<vw_PaperRollStock>().ToList();

                var oMatCategoryToDisplay = oMatCategoryObjects.Where(p=>p.Quantity>0).Select(p => new
                {
                    Pk_Material=p.Pk_Material,
                    Pk_PaperStock = p.Pk_PaperStock,
                    Name = p.Name,
                    RollNo = p.RollNo,
                    Quantity = p.Quantity,
                    Category="Paper",
                    GSM=p.GSM,
                    Color = p.ColorName,
                    //UnitName = p.UnitName
                }).ToList();
                return Json(new { Result = "OK", Records = oMatCategoryToDisplay, TotalRecordCount = oSearchResult.RecordCount });
                }



                //Create a anominious object here to break the circular reference
                //var oMatCategoryToDisplay = oMatCategoryObjects.Select(p => new
                //{
                //    Pk_Material = p.Pk_Material,
                //    Name = p.MaterialName,
                //    //Category = p.Name,
                //    Quantity = p.Quantity,
                   
                //    //UnitName = p.UnitName
                //}).ToList();

                //return Json(new { Result = "OK", Records = oMatCategoryToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult MCatListByFiter(string Name = "", string Pk_MaterialCategory = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_MaterialCategory", Pk_MaterialCategory));
                oSearchParams.Add(new SearchParameter("Name", Name));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oMatCategoryToDisplayObjects = oSearchResult.ListOfRecords;
                List<Inv_MaterialCategory> oMatCategoryObjects = oMatCategoryToDisplayObjects.Select(p => p).OfType<Inv_MaterialCategory>().ToList();

                //Create a anominious object here to break the circular reference
                var oMatCategoryToDisplay = oMatCategoryObjects.Select(p => new
                {
                    Pk_MaterialCategory = p.Pk_MaterialCategory,
                    Name = p.Name

                }).ToList();

                return Json(new { Result = "OK", Records = oMatCategoryToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }



        [HttpPost]
        public JsonResult MaterialIndentSearch(string Name = "", string MaterialName = "", string GSM = "", string Deckle = "", string BF = "", string MaterialCategory = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("MaterialCategory", MaterialCategory));
                oSearchParams.Add(new SearchParameter("Name", Name));
                oSearchParams.Add(new SearchParameter("MaterialName", MaterialName));
                oSearchParams.Add(new SearchParameter("BF", BF));
                oSearchParams.Add(new SearchParameter("GSM", GSM));
                oSearchParams.Add(new SearchParameter("Deckle", Deckle));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));
                if (MaterialCategory != "Paper" || MaterialCategory == "Others")
                {
                    SearchResult oSearchResult = oMatCategory.SearchIssue(oSearchParams);

                    List<EntityObject> oMatCategoryToDisplayObjects = oSearchResult.ListOfRecords;


                    List<Vw_Popup_MaterialIssue> oMatCategoryObjects = oMatCategoryToDisplayObjects.Select(p => p).OfType<Vw_Popup_MaterialIssue>().ToList();

                    var oMatCategoryToDisplay = oMatCategoryObjects.Select(p => new
                    {
                        Pk_Material = p.Pk_Material,
                        Name = p.MaterialName,
                        Color = p.ColorName,
                        Mill=p.MillName,
                        Quantity = p.Quantity,
                        Category=p.Name,
                        Desc=p.Description,
                        UnitName = p.UnitName
                    }).ToList();
                    return Json(new { Result = "OK", Records = oMatCategoryToDisplay, TotalRecordCount = oSearchResult.RecordCount });
                }
                else
                {
                    SearchResult oSearchResult = oMatCategory.SearchPaperIndentIssue(oSearchParams);

                    List<EntityObject> oMatCategoryToDisplayObjects = oSearchResult.ListOfRecords;


                    List<Vw_PaperStock> oMatCategoryObjects = oMatCategoryToDisplayObjects.Select(p => p).OfType<Vw_PaperStock>().ToList();
                    var oMatCategoryToDisplay = oMatCategoryObjects.Select(p => new
                    {
                        Pk_Material = p.Pk_Material,
                        Name = p.Name,
                        Color = p.ColorName,
                        //Quantity = p.Quantity,
                        Mill=p.MillName,
                        Pk_mill=p.Pk_Mill,
                        //UnitName = p.UnitName
                    }).ToList();
                    return Json(new { Result = "OK", Records = oMatCategoryToDisplay, TotalRecordCount = oSearchResult.RecordCount });
                }



                //Create a anominious object here to break the circular reference
                //var oMatCategoryToDisplay = oMatCategoryObjects.Select(p => new
                //{
                //    Pk_Material = p.Pk_Material,
                //    Name = p.MaterialName,
                //    //Category = p.Name,
                //    Quantity = p.Quantity,

                //    //UnitName = p.UnitName
                //}).ToList();

                //return Json(new { Result = "OK", Records = oMatCategoryToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


    }
}
