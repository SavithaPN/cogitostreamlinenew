﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;
using System.IO;
using System.Data;
using CrystalDecisions.ReportAppServer.ReportDefModel;
using System.Drawing.Printing;
using CrystalDecisions.Shared;

namespace CogitoStreamline.Controllers
{

    public class POReturnController : CommonController
    {
        #region Variables
        POReturn oPOrder = new POReturn();
        #endregion Variables

        #region Constractor
        public POReturnController()
        {
            oDoaminObject = new POReturn();
        }
        #endregion Constractor

        #region Methods
        public override ActionResult Index()
        {
            ViewBag.Header = "Purchase Order Returns";
            return base.Index();
        }
        protected override void Dispose(bool disposing)
        {
            oPOrder.Dispose();
            base.Dispose(disposing);
        }
        [HttpPost]
        public override JsonResult Load(string data = "")
        {

            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                POReturn oPOrders = oDoaminObject as POReturn;
              
                oPOrders.ID = decimal.Parse(pId);
                POReturnMaster oAtualObject = oPOrders.DAO as POReturnMaster;

                int i = -1;

                var oMaterials = oAtualObject.POReturnDetails.Select(p => new
                {
                    slno = ++i,
                    Quantity = p.Quantity,
                    Fk_Material = p.Fk_Material,
                    Name = p.Inv_Material.Name,
                    Pk_PoRetDetID = p.Pk_PoRetDetID,
                    Rate = p.Rate,
                    Amount = p.Amount

                });

                var oPOrderDisplay = new
                {
                    Pk_PONo = oAtualObject.Pk_PoRetID,
                    //ED = oAtualObject.ED,
                    PoRetDate = DateTime.Parse(oAtualObject.PoRetDate.ToString()).ToString("dd/MM/yyyy"),
                    //GrandTotal = oAtualObject.GrandTotal,
                    //NETVALUE = oAtualObject.NetValue,
                    //Fk_Vendor = oAtualObject.Fk_Vendor,
                    //Fk_Indent = oAtualObject.Fk_Indent,
                    //TaxType = oAtualObject.TaxType,
                    Fk_PoNo = oAtualObject.Fk_PoNo,
                    MaterialData = Json(oMaterials).Data
                };

                return Json(new { success = true, data = oPOrderDisplay });
            }
            else
            {
                var oReturnGoodFromSiteDisplay = new POReturnMaster();
                return Json(new { success = true, data = oReturnGoodFromSiteDisplay });
            }
        }
        public JsonResult getMaterialDetails(string Pk_MaterialID = "", string MaterialName = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {

            try
            {
                vw_StockMaterial oIssueDetails = new vw_StockMaterial();
                List<SearchParameter> oSearchParams = new List<SearchParameter>();

                oSearchParams.Add(new SearchParameter("Pk_MaterialID", Pk_MaterialID));
                oSearchParams.Add(new SearchParameter("MaterialName", MaterialName));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oPOrder.SearchIssueDetails(oSearchParams);

                List<EntityObject> oIssueDetailsToDisplayObjects = oSearchResult.ListOfRecords;
                List<vw_StockMaterial> oIssueObjects = oIssueDetailsToDisplayObjects.Select(p => p).OfType<vw_StockMaterial>().ToList();

                //Create a anominious object here to break the circular reference
                var oIssueDetailsToDisplay = oIssueObjects.Where(p => (p.Quantity) <= 0).Select(p => new
                {
                    Pk_Material = p.Pk_Material,
                    MaterialName = p.Name,
                    Quantity = p.Quantity


                }).ToList();

                return Json(new { Result = "OK", Records = oIssueDetailsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        public JsonResult getMaterialIndentDetails(string Indent = "", string Pk_MaterialID = "", string MaterialName = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {

            try
            {
                vw_StockMaterial oIssueDetails = new vw_StockMaterial();
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("IndentNo", Indent));
                oSearchParams.Add(new SearchParameter("Pk_MaterialID", Pk_MaterialID));
                oSearchParams.Add(new SearchParameter("MaterialName", MaterialName));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oPOrder.SearchIndentDetails(oSearchParams);

                List<EntityObject> oIssueDetailsToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_MIndent> oIssueObjects = oIssueDetailsToDisplayObjects.Select(p => p).OfType<Vw_MIndent>().ToList();

                //Create a anominious object here to break the circular reference
                var oIssueDetailsToDisplay = oIssueObjects.Select(p => new
                {
                    Pk_Material = p.Pk_Material,
                    MaterialName = p.Name,
                    Quantity = p.Quantity


                }).ToList();

                return Json(new { Result = "OK", Records = oIssueDetailsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        public JsonResult PurchaseOrderReturnListByFiter(string Fk_PoNo = "", string Pk_PoRetID = "",  string TxtFromDate = "", string TxtToDate = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Fk_PoNo", Fk_PoNo));
                oSearchParams.Add(new SearchParameter("Pk_PoRetID", Pk_PoRetID));
          
                oSearchParams.Add(new SearchParameter("TxtFromDate", TxtFromDate));
                oSearchParams.Add(new SearchParameter("TxtToDate", TxtToDate));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oIssueReturnToDisplayObjects = oSearchResult.ListOfRecords;
                List<POReturnMaster> oIssueReturnObjects = oIssueReturnToDisplayObjects.Select(p => p).OfType<POReturnMaster>().ToList();

                var oIssureReturnToDisplay = oIssueReturnObjects.Select(p => new
                {
                    Pk_PoRetID = p.Pk_PoRetID,
                    PoRetDate = DateTime.Parse(p.PoRetDate.ToString()).ToString("dd/MM/yyyy"),
                    Fk_PoNo = p.Fk_PoNo

                }).ToList();

                return Json(new { Result = "OK", Records = oIssureReturnToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        #endregion Methods

        public ActionResult POrderRep(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["Pk_PONO"]);

                List<Vw_POReport> BillList = new List<Vw_POReport>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                //BillList = dc.VwInvoices.ToList();
                BillList = dc.Vw_POReport.Where(x => x.Pk_PONo == sInvno).Select(x => x).OfType<Vw_POReport>().ToList();

                //                SELECT     dbo.Inv_Material.Pk_Material, dbo.Inv_Material.Name, dbo.POReturnDetail.Quantity, dbo.POReturnMaster.Pk_PONo, dbo.POReturnMaster.PODate, dbo.POReturnDetail.Rate, 
                //                      dbo.POReturnDetail.Amount, dbo.gen_Vendor.VendorName, dbo.POReturnMaster.GrandTotal, dbo.POReturnMaster.NetValue, dbo.POReturnMaster.TaxType, dbo.POReturnMaster.Fk_Indent
                //FROM         dbo.POReturnMaster INNER JOIN
                //                      dbo.POReturnDetail ON dbo.POReturnMaster.Pk_PONo = dbo.POReturnDetail.Fk_PONo INNER JOIN
                //                      dbo.Inv_Material ON dbo.POReturnDetail.Fk_Material = dbo.Inv_Material.Pk_Material INNER JOIN
                //                      dbo.gen_Vendor ON dbo.POReturnMaster.Fk_Vendor = dbo.gen_Vendor.Pk_Vendor



                if (BillList.Count > 0)
                {
                    DataTable dt = new DataTable();

                    dt.Columns.Add("Name");
                    dt.Columns.Add("Quantity");
                    dt.Columns.Add("Pk_PONo");
                    dt.Columns.Add("PODate");
                    dt.Columns.Add("Rate");
                    dt.Columns.Add("Amount");
                    dt.Columns.Add("VendorName");
                    dt.Columns.Add("GrandTotal");
                    dt.Columns.Add("NetValue");
                    dt.Columns.Add("TaxType");
                    dt.Columns.Add("Fk_Indent");
                    dt.Columns.Add("Pk_Material");
                    dt.Columns.Add("FORM_H");
                    dt.Columns.Add("FORM_CT3");

                    foreach (Vw_POReport entity in BillList)
                    {
                        DataRow row = dt.NewRow();

                        row["Name"] = entity.Name;
                        row["Quantity"] = entity.Quantity;
                        row["Pk_PONo"] = entity.Pk_PONo;
                        row["PODate"] = entity.PODate;
                        row["Rate"] = entity.Rate;
                        row["Amount"] = entity.Amount;
                        row["VendorName"] = entity.VendorName;
                        row["GrandTotal"] = entity.GrandTotal;
                        row["NetValue"] = entity.NetValue;
                        row["TaxType"] = entity.TaxType;
                        row["Fk_Indent"] = entity.Fk_Indent;
                        row["Pk_Material"] = entity.Pk_Material;
                        //row["FORM_H"] = entity.FORM_H;
                        //row["FORM_CT3"] = entity.FORM_CT3;
                        dt.Rows.Add(row);
                    }


                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.POrder orp = new CogitoStreamline.Report.POrder();

                    orp.Load("@\\Report\\POrder.rpt");
                    orp.SetDataSource(dt.DefaultView);

                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "POrder" + sInvno + ".pdf");
                    FileInfo file = new FileInfo(pdfPath);
                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();

                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }
    }
}


