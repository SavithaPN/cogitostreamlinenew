﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;

using System.IO;
using Common.Logging.Configuration;
using System.Collections.Specialized;

namespace CogitoStreamline.Controllers
{

    public class WireParamsController : CommonController
    {
        WireParams oColor = new WireParams();

        public WireParamsController()
        {
            oDoaminObject = new WireParams();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Wire Parameters";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            oColor.Dispose();
            base.Dispose(disposing);
        }


        //[HttpPost]
        //public JsonResult getChar(string pId = "")
        //{
        //    List<PaperChar> oListOfBoxType = oColor.Search(null).ListOfRecords.OfType<PaperChar>().ToList();

        //    var oBoxToDisplay = oListOfBoxType.Select(p => new
        //    {
        //        Name = p.Name,
        //        Id = p.Pk_CharID

        //    });

        //    return Json(oBoxToDisplay);
        //}
        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                WireParams oColor = oDoaminObject as WireParams;
                oColor.ID = decimal.Parse(pId);
                WireChar oAtualObject = oColor.DAO as WireChar;
                //Create a anominious object here to break the circular reference

                var oColorToDisplay = new
                {

                    Pk_CharID = oAtualObject.Pk_CharID,
                    Name = oAtualObject.Name,
                    StdVal = oAtualObject.StdVal,
                };

                return Json(new { success = true, data = oColorToDisplay });
            }
            else
            {
                var oColorToDisplay = new PaperChar();
                return Json(new { success = true, data = oColorToDisplay });
            }
        }



        //[HttpPost]
        //public JsonResult ColorDuplicateChecking(string data = "")
        //{
        //    try
        //    {
        //        Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        //        string sColorName = oValues["Name"];

        //        List<SearchParameter> oSearchParams = new List<SearchParameter>();
        //        oSearchParams.Add(new SearchParameter("Name", sColorName));

        //        SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

        //        List<EntityObject> oColorToDisplayObjects = oSearchResult.ListOfRecords;
        //        List<PaperChar> oColorObjects = oColorToDisplayObjects.Select(p => p).OfType<PaperChar>().ToList();

        //        var oColorToDisplay = oColorObjects.Select(p => new
        //        {
        //            Pk_CharID = p.Pk_CharID,
        //            Name = p.Inv_Material.Name
        //        }).ToList().OrderBy(s => s.Name);

        //        return Json(new { Success = true, Records = oColorToDisplay, TotalRecordCount = oSearchResult.RecordCount });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { Success = false, Message = ex.Message });
        //    }
        //}

        [HttpPost]
        public JsonResult WireParamsListByFiter(string Name = "", string Pk_CharID = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_CharID", Pk_CharID));
                oSearchParams.Add(new SearchParameter("Name", Name));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oColorToDisplayObjects = oSearchResult.ListOfRecords;
                List<WireChar> oColorObjects = oColorToDisplayObjects.Select(p => p).OfType<WireChar>().ToList();

                //Create a anominious object here to break the circular reference
                var oColorToDisplay = oColorObjects.Select(p => new
                {
                    Pk_CharID = p.Pk_CharID,
                    Name = p.Name,
                    StdVal = p.StdVal,


                }).ToList().OrderBy(s => s.Pk_CharID);

                return Json(new { Result = "OK", Records = oColorToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }




    }

}
