﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;
using System.IO;
using System.Data;
using CrystalDecisions.ReportAppServer.ReportDefModel;
using System.Drawing.Printing;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using WebGareCore.Controls;


namespace CogitoStreamLine.Controllers
{
    public class BoxMasterController : CommonController
    {
        BoxM oBoxMaster = new BoxM();
        BoxTyp oBoxT = new BoxTyp();
        BoxM oItProp = new BoxM();
        Machine oMAC = new Machine();

        FType oFType = new FType();
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        public WgFileUpload oFileUpload = new WgFileUpload("documents", "uploads");

      
        public BoxMasterController()
        {
            oDoaminObject = new BoxM();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Box Master";
            return base.Index();
        }
        protected override void Dispose(bool disposing)
        {
            oBoxMaster.Dispose();
            base.Dispose(disposing);
        }
        [HttpPost]
        public JsonResult FileUpload()
        {
            return Json(oFileUpload.SaveFile(Request.Files));
        }

        [HttpPost]
        public JsonResult getBoxType(string pId = "")
        {
            List<BoxType> oListOfBoxType = oBoxT.Search(null).ListOfRecords.OfType<BoxType>().ToList();

            var oBoxToDisplay = oListOfBoxType.Select(p => new
            {
                Name = p.Name,
                Id = p.Pk_BoxTypeID
               
            });

            return Json(oBoxToDisplay);
        }


        [HttpPost]
        public JsonResult getFluteType(string pId = "")
        {
            List<FluteType> oListOfBoxType = oFType.Search(null).ListOfRecords.OfType<FluteType>().ToList();

            var oBoxToDisplay = oListOfBoxType.Select(p => new
            {
                Name = p.FluteName,
                Id = p.Pk_FluteID

            });

            return Json(oBoxToDisplay);
        }
        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                BoxM oBox = oDoaminObject as BoxM;
                oBox.ID = decimal.Parse(pId);
                BoxMaster oAtualObject = oBox.DAO as BoxMaster;


                int i = -1;

                       //Create a anominious object here to break the circular reference
                var oHeadToDisplay = new
                {
                    Pk_BoxID = oAtualObject.Pk_BoxID,
                    Name = oAtualObject.Name,
                    Description = oAtualObject.Description,
                    Customer = oAtualObject.gen_Customer.CustomerName,
                    PartNo=oAtualObject.PartNo,
                    Fk_BoxType = oAtualObject.Fk_BoxType,
                    Fk_FluteType=oAtualObject.FluteType.FluteName
                    
                };
                return Json(new { success = true, data = oHeadToDisplay });
            }
            else
            {
                var oHeadToDisplay = new BoxMaster();
                return Json(new { success = true, data = oHeadToDisplay });
            }
        }
        [HttpPost]
        public JsonResult NameDuplicateChecking(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sName = oValues["Name"];
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Name", sName));
                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);
                List<EntityObject> oHeadToDisplayObjects = oSearchResult.ListOfRecords;
                List<BoxMaster> oHeadObjects = oHeadToDisplayObjects.Select(p => p).OfType<BoxMaster>().ToList();
                var oHeadToDisplay = oHeadObjects.Select(p => new
                {
                    Pk_BoxId = p.Pk_BoxID,
                    Name = p.Name
                }).ToList().OrderBy(s => s.Name);
                return Json(new { Success = true, Records = oHeadToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult BoxNameListByFiter(string Name = "", string CustName = "", string CustID = "", string Pk_BoxID = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
             
                oSearchParams.Add(new SearchParameter("Name", Name));
                oSearchParams.Add(new SearchParameter("CustName", CustName));
                oSearchParams.Add(new SearchParameter("CustID", CustID));
                oSearchParams.Add(new SearchParameter("Pk_BoxID", Pk_BoxID));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);
                List<EntityObject> oHeadToDisplayObjects = oSearchResult.ListOfRecords;

                List<BoxMaster> oHeadObjects = oHeadToDisplayObjects.Select(p => p).OfType<BoxMaster>().ToList();
                //Create a anominious object here to break the circular reference
                var oHeadToDisplay = oHeadObjects.Select(p => new
                {
                    Pk_BoxID = p.Pk_BoxID,
                    BTypeId=p.BoxType.Pk_BoxTypeID,
                    BoxType = p.BoxType.Name,
                    Customer = p.gen_Customer.CustomerName,
                    Name = p.Name,
                    Description = p.Description,
                    PartNo=p.PartNo,
                    Fk_FluteType=p.Fk_FluteType,
              TakeUp=p.FluteType.TKFactor,
                  BType=p.BoxType.Pk_BoxTypeID,
                
                }).ToList();
                return Json(new { Result = "OK", Records = oHeadToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


        //[HttpPost]
        //public JsonResult JobBoxDetails(string Name = "", string Pk_BoxID = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        //{
        //    try
        //    {
        //        List<SearchParameter> oSearchParams = new List<SearchParameter>();
        //        oSearchParams.Add(new SearchParameter("Name", Name));
        //        oSearchParams.Add(new SearchParameter("Pk_BoxID", Pk_BoxID));
        //        oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
        //        oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

        //        SearchResult oSearchResult = oBoxMaster.SearchJobBox(oSearchParams);
        //        List<EntityObject> oHeadToDisplayObjects = oSearchResult.ListOfRecords;

        //        List<Vw_JobBoxDetails> oHeadObjects = oHeadToDisplayObjects.Select(p => p).OfType<Vw_JobBoxDetails>().ToList();
        //        //Create a anominious object here to break the circular reference
        //        var oHeadToDisplay = oHeadObjects.Select(p => new
        //        {
        //            Pk_BoxID = p.Fk_BoxID,
        //            Name = p.Name,
        //            CustomerName = p.CustomerName,
        //            Pk_JobCardID = p.Pk_JobCardID,
        //            JDate = DateTime.Parse(p.JDate.ToString()).ToString("dd/MM/yyyy"),
        //            Quantity = p.Quantity,
        //            TotalPrice = p.TotalPrice

        //        }).ToList().OrderBy(s => s.Pk_BoxID);
        //        return Json(new { Result = "OK", Records = oHeadToDisplay, TotalRecordCount = oSearchResult.RecordCount });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { Result = "ERROR", Message = ex.Message });
        //    }
        //}

        [HttpPost]
        public JsonResult BoxDetails(string Pk_BoxID = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                BoxM oBoxMaster = oDoaminObject as BoxM;

                oBoxMaster.ID = decimal.Parse(Pk_BoxID);

                BoxMaster oEnquiryObjects = oBoxMaster.DAO as BoxMaster;
                
                var oDeliveryScheduleToDisplay = oEnquiryObjects.BoxSpecs.Select(p => new
                {
                    Pk_BoxSpecID = p.Pk_BoxSpecID,
                    OYes=p.OuterShellYes,
                    CYes=p.CapYes,
                    LPYes=p.LenghtPartationYes,
                    WPYes=p.WidthPartationYes,
                    PYes=p.PlateYes,
                    TYes=p.TopYes,
                    BYes=p.BottomYes


                }).ToList();

                return Json(new { Result = "OK", Records = oDeliveryScheduleToDisplay, TotalRecordCount = oDeliveryScheduleToDisplay.Count() });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


        [HttpPost]
        public JsonResult BoxPartDetails(string Pk_BoxID = "", string CustName = "", string CustIDVal="", string PartName = "", string BoxName = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {

                var BoxID = Pk_BoxID;
                var CName = CustName;
                var BName = BoxName;
                ModelManuplationResult oResult = new ModelManuplationResult();

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_BoxID", BoxID));
                oSearchParams.Add(new SearchParameter("CustIDVal", CustIDVal));
                oSearchParams.Add(new SearchParameter("CName", CName));
                oSearchParams.Add(new SearchParameter("BName", BName));
                oSearchParams.Add(new SearchParameter("PartName", PartName));
                SearchResult oSearchResult = oItProp.SearchBoxSp(oSearchParams);

                List<EntityObject> oEnquirysToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_BoxSpec> oEnquiryObjects = oEnquirysToDisplayObjects.Select(p => p).OfType<Vw_BoxSpec>().ToList();

                               
                //oBoxMaster.ID = decimal.Parse(Pk_BoxID);              

                var oDeliveryScheduleToDisplay = oEnquiryObjects.Select(p => new
                {
                    Pk_PartPropertyID=p.Pk_PartPropertyID,
                   PName=p.PName,
                    Length = p.Length,
                    Width=p.Width,
                    Height=p.Height,
                    Weight=p.Weight,
                    Name=p.Name,
                    Quantity=p.Quantity,
                    Pk_BoxID=p.Pk_BoxID,
                    Pk_PartID=p.Pk_PartPropertyID,
                   

                    //BType=p.BType,
                    //CuttingSize=p.CuttingSize,
                    //BoardArea=p.BoardArea,
                    //Deck=p.Deckle

                }).ToList();
                return Json(new { Result = "OK", Records = oDeliveryScheduleToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


        [HttpPost]
        public JsonResult BoxLayerDetails(string Pk_BoxID = "", string CustName = "", string CustIDVal = "", string PartName = "", string BoxName = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {

                var BoxID = Pk_BoxID;
                var CName = CustName;
                var BName = BoxName;
                ModelManuplationResult oResult = new ModelManuplationResult();

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_BoxID", BoxID));
                oSearchParams.Add(new SearchParameter("CustIDVal", CustIDVal));
                oSearchParams.Add(new SearchParameter("CName", CName));
                oSearchParams.Add(new SearchParameter("BName", BName));
                oSearchParams.Add(new SearchParameter("PartName", PartName));
                SearchResult oSearchResult = oItProp.SearchP(oSearchParams);

                List<EntityObject> oEnquirysToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_BoxDet> oEnquiryObjects = oEnquirysToDisplayObjects.Select(p => p).OfType<Vw_BoxDet>().ToList();


                //oBoxMaster.ID = decimal.Parse(Pk_BoxID);              

                var oDeliveryScheduleToDisplay = oEnquiryObjects.Select(p => new
                {
                    
                    Pk_BoxID = p.Pk_BoxID,
                    Pk_Material = p.Pk_Material,
                    MaterialName = p.MaterialName,
                    GSM=p.GSM,
                    BF=p.BF,
                    Pk_LayerID=p.Pk_LayerID,
                    PName = p.PName,
                    //CuttingSize=p.CuttingSize,
                    //BoardArea=p.BoardArea,
                    //Deck=p.Deckle

                }).ToList();
                return Json(new { Result = "OK", Records = oDeliveryScheduleToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }




        [HttpPost]
        public ActionResult SBoxDet(string data = "")
        {

            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string PartID = "";

            string pId = oValues["Pk_BoxID"];

            if (oValues.ContainsKey("PartID"))
            {
                PartID = oValues["PartID"];
            }
            else
            {
                PartID = "";
            }
            if (pId != "")
            {
                var BoxID = int.Parse(pId);
                ModelManuplationResult oResult = new ModelManuplationResult();

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_BoxID", pId));
                oSearchParams.Add(new SearchParameter("PartID", PartID));
                SearchResult oSearchResult = oItProp.SearchP(oSearchParams);

                List<EntityObject> oEnquirysToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_BoxDet> oEnquiryObjects = oEnquirysToDisplayObjects.Select(p => p).OfType<Vw_BoxDet>().ToList();

                var oEnquiryToDisplay = oEnquiryObjects.Select(p => new
                {
                    Pk_PartPropertyID = p.Pk_PartPropertyID,
                    PName = p.PName,
                    Length = p.Length,
                    Width = p.Width,
                    Height = p.Height,
                    BType = p.BType,
                    CuttingSize = p.CuttingSize,
                    BoardArea = p.BoardArea,
                    Deck = p.Deckle,
                    Quantity = p.Quantity,
                    Ply = p.PlyVal,
                    LayerWt = p.LayerWt,
                    LayerID = p.Pk_LayerID,
                    TakeUp = p.TakeUpFactor,
                    FType = p.FluteName,
                    UpsVal=p.NoBoards,
                    Pk_Material = p.Pk_Material,
                    MaterialName=p.MaterialName,
                });

                return Json(new { success = true, data = oEnquiryToDisplay });

            }
            return null;
        }


        [HttpPost]
        public ActionResult BoxCopyPaste(string data = "")
        {
                CogitoStreamLineEntities _oEntities = new CogitoStreamLineEntities();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            int i = 0;
            decimal PartPropertyID = 0;
            string PartID = ""; 
            string Boxname=oValues["BName"];
            decimal pId = Convert.ToDecimal(oValues["Pk_BoxID"]);
            BoxMaster oBox = _oEntities.BoxMaster.Where(p => p.Pk_BoxID == pId).Single();

            BoxMaster oNewBoxMast = new BoxMaster();
            var Name=oBox.Name;
           var PartNo= oBox.PartNo;
           var customer = oBox.Customer;
           var Desc = oBox.Description;
           var BoxType = oBox.Fk_BoxType;
           var FType = oBox.Fk_FluteType;



           oNewBoxMast.Name = Boxname;
            oNewBoxMast.Customer = customer;
            oNewBoxMast.Description = Desc;
            oNewBoxMast.Fk_BoxType = BoxType;
            oNewBoxMast.PartNo = PartNo;
            oNewBoxMast.Fk_FluteType = FType;


            _oEntites.AddToBoxMaster(oNewBoxMast);
            _oEntites.SaveChanges();

            var BID = oNewBoxMast.Pk_BoxID;   // autogenerated number

            BoxSpec oBoxVal = _oEntities.BoxSpecs.Where(p => p.Fk_BoxID == pId).Single();

            BoxSpec oNewBoxSpec = new BoxSpec();
            //Pk_BoxSpecID
            oNewBoxSpec.OuterShellYes = oBoxVal.OuterShellYes;
            oNewBoxSpec.CapYes = oBoxVal.CapYes;
            oNewBoxSpec.LenghtPartationYes = oBoxVal.LenghtPartationYes;
            oNewBoxSpec.WidthPartationYes = oBoxVal.WidthPartationYes;
            oNewBoxSpec.PlateYes = oBoxVal.PlateYes;
            oNewBoxSpec.OuterShell = oBoxVal.OuterShell;
            oNewBoxSpec.Cap = oBoxVal.Cap;
            oNewBoxSpec.LenghtPartation = oBoxVal.LenghtPartation;
            oNewBoxSpec.WidthPartation = oBoxVal.WidthPartation;
            oNewBoxSpec.Plate = oBoxVal.Plate;
            oNewBoxSpec.TotalWeight = oBoxVal.TotalWeight;
            oNewBoxSpec.Fk_BoxID = BID;
            oNewBoxSpec.TopYes = oBoxVal.TopYes;
            oNewBoxSpec.BottomYes = oBoxVal.BottomYes;
            oNewBoxSpec.TopVal = oBoxVal.TopVal;
            oNewBoxSpec.BottVal = oBoxVal.BottVal;
            oNewBoxSpec.SleeveYes = oBoxVal.SleeveYes;
            oNewBoxSpec.Sleeve = oBoxVal.Sleeve;

            _oEntites.AddToBoxSpecs(oNewBoxSpec);
            _oEntites.SaveChanges();


            var BoxSpecID = oNewBoxSpec.Pk_BoxSpecID;   // autogenerated number

            int StkCnt = _oEntities.ItemPartProperty.Where(p => p.Fk_BoxSpecsID == oBoxVal.Pk_BoxSpecID).Count();
            if (StkCnt >= 1)
            {
                List<ItemPartProperty> oPrdList = new List<ItemPartProperty>();
                oPrdList = dc.ItemPartProperty.Where(p => p.Fk_BoxSpecsID == oBoxVal.Pk_BoxSpecID).Select(s => s).OfType<ItemPartProperty>().ToList();

                for (i = 0; i < oPrdList.Count(); i++)
                {
                    ItemPartProperty oNewItem = new ItemPartProperty();
                    var PrdId = Convert.ToDecimal(oPrdList.ElementAt(i).Pk_PartPropertyID);
                    oNewItem.Fk_BoxSpecsID = BoxSpecID;
                    oNewItem.PName = oPrdList.ElementAt(i).PName;
                    oNewItem.Length = Convert.ToInt32(oPrdList.ElementAt(i).Length);
                    oNewItem.Width = Convert.ToInt32(oPrdList.ElementAt(i).Width);
                    oNewItem.Height = Convert.ToInt32(oPrdList.ElementAt(i).Height);
                    oNewItem.Weight = Convert.ToDecimal(oPrdList.ElementAt(i).Weight);
                    oNewItem.Quantity = Convert.ToInt32(oPrdList.ElementAt(i).Quantity);
                    oNewItem.NoBoards = Convert.ToInt32(oPrdList.ElementAt(i).NoBoards);
                    oNewItem.BoardArea = Convert.ToDecimal(oPrdList.ElementAt(i).BoardArea);
                    oNewItem.Deckle = Convert.ToDecimal(oPrdList.ElementAt(i).Deckle);
                    oNewItem.CuttingSize = Convert.ToDecimal(oPrdList.ElementAt(i).CuttingSize);

                    oNewItem.Rate = Convert.ToDecimal(oPrdList.ElementAt(i).Rate);
                    oNewItem.AddBLength = Convert.ToDecimal(oPrdList.ElementAt(i).AddBLength);
                    oNewItem.TakeUpFactor = Convert.ToDecimal(oPrdList.ElementAt(i).TakeUpFactor);
                    oNewItem.PlyVal = Convert.ToInt32(oPrdList.ElementAt(i).PlyVal);

                    oNewItem.OLength1 = Convert.ToInt32(oPrdList.ElementAt(i).OLength1);
                    oNewItem.OHeight1 = Convert.ToInt32(oPrdList.ElementAt(i).OHeight1);
                    oNewItem.OWidth1 = Convert.ToInt32(oPrdList.ElementAt(i).OWidth1);

                    oNewItem.BoardGSM = Convert.ToDecimal(oPrdList.ElementAt(i).BoardGSM);
                    oNewItem.BoardBS = Convert.ToDecimal(oPrdList.ElementAt(i).BoardBS);


                    _oEntites.AddToItemPartProperty(oNewItem);
                    _oEntites.SaveChanges();

                    PartPropertyID = oNewItem.Pk_PartPropertyID;   // autogenerated number


                    var j = 0;
                    int StkCnt1 = _oEntities.Items_Layers.Where(p => p.Fk_PartId == PrdId).Count();
                    if (StkCnt1 >= 1)
                    {
                        List<Items_Layers> oLayerList = new List<Items_Layers>();
                        oLayerList = dc.Items_Layers.Where(p => p.Fk_PartId == PrdId).Select(s => s).OfType<Items_Layers>().ToList();

                        for (j = 0; j < oLayerList.Count(); j++)
                        {

                            Items_Layers oNewItemLayer = new Items_Layers();
                            //  var PrdId = Convert.ToDecimal(oPrdList.ElementAt(i).Pk_PartPropertyID);
                            oNewItemLayer.Fk_PartId = PartPropertyID;
                            oNewItemLayer.GSM = oLayerList.ElementAt(j).GSM;
                            oNewItemLayer.BF = oLayerList.ElementAt(j).BF;
                            oNewItemLayer.Color = oLayerList.ElementAt(j).Color;
                            oNewItemLayer.Rate = oLayerList.ElementAt(j).Rate;
                            oNewItemLayer.Fk_Material = oLayerList.ElementAt(j).Fk_Material;
                            oNewItemLayer.Weight = oLayerList.ElementAt(j).Weight;
                            oNewItemLayer.Tk_Factor = oLayerList.ElementAt(j).Tk_Factor;
                            //  oNewItemLayer = oLayerList.ElementAt(i).PName;



                            _oEntites.AddToItems_Layers(oNewItemLayer);
                            _oEntites.SaveChanges();


                        }
                    }
                }
            }

            
            return null;
        }

        [HttpPost]
        public ActionResult SBoxDetails(string data = "")
        {

            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string PartID = "";

            string pId = oValues["Pk_BoxID"];

            if (oValues.ContainsKey("PartID"))
            {
                PartID = oValues["PartID"];
            }
            else
            {
                PartID = "";
            }
            if (pId != "")
            {
                var BoxID = int.Parse(pId);
                ModelManuplationResult oResult = new ModelManuplationResult();

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_BoxID", pId));
                oSearchParams.Add(new SearchParameter("PartID", PartID));
                SearchResult oSearchResult = oItProp.Search(oSearchParams);

                List<EntityObject> oEnquirysToDisplayObjects = oSearchResult.ListOfRecords;
                List<BoxMaster> oEnquiryObjects = oEnquirysToDisplayObjects.Select(p => p).OfType<BoxMaster>().ToList();

                var oEnquiryToDisplay = oEnquiryObjects.Select(p => new
                {
                    Fk_FluteType = p.Fk_FluteType,
                    TakeUp = p.FluteType.TKFactor,
                    BType = p.BoxType.Pk_BoxTypeID,

                });

                return Json(new { success = true, data = oEnquiryToDisplay });

            }
            return null;
        }


        [HttpPost]
        public ActionResult SMachineDet(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);


            string pId = oValues["Pk_Machine"];
            if (pId != "")
            {
                var BoxID = int.Parse(pId);
                ModelManuplationResult oResult = new ModelManuplationResult();

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_Machine", pId));

                SearchResult oSearchResult = oBoxMaster.SearchMachine(oSearchParams);

                List<EntityObject> oEnquirysToDisplayObjects = oSearchResult.ListOfRecords;
                List<gen_Machine> oEnquiryObjects = oEnquirysToDisplayObjects.Select(p => p).OfType<gen_Machine>().ToList();

                var oEnquiryToDisplay = oEnquiryObjects.Select(p => new
                {

                    Capacity_Length = p.Capacity_Length,
                    
                });
             
                return Json(new { success = true, data = oEnquiryToDisplay });

            }
            return null;
        }

        public ActionResult BoxRep(string data = "")
        {
            var BoxTyp="";
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["fkbox"]);

                List<Vw_BoxDet> BillList = new List<Vw_BoxDet>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                //BillList = dc.VwInvoices.ToList();
                BillList = dc.Vw_BoxDet.Where(x => x.Pk_BoxID == sInvno).Select(x => x).OfType<Vw_BoxDet>().ToList();
                 if (BillList.Count > 0)
                {
                     var ctr=0;
                      BoxTyp = BillList.ElementAt(0).BType;
                 }

                if (BillList.Count > 0)
                {
                    DataTable dt = new DataTable();

                    dt.Columns.Add("Pk_BoxID");
                    dt.Columns.Add("Name");
                    dt.Columns.Add("Length");
                    dt.Columns.Add("Width");
                    dt.Columns.Add("Height");
                    dt.Columns.Add("Weight");
                    dt.Columns.Add("GSM");
                    dt.Columns.Add("BF");
                    dt.Columns.Add("Pk_PartPropertyID");
                    dt.Columns.Add("OuterShellYes");
                    dt.Columns.Add("CapYes");
                    dt.Columns.Add("LenghtPartationYes");
                    dt.Columns.Add("WidthPartationYes");
                    dt.Columns.Add("PlateYes");
                    dt.Columns.Add("MaterialName");
                    dt.Columns.Add("Customer");
                    dt.Columns.Add("PartNo");
                    dt.Columns.Add("PlyVal");
                    dt.Columns.Add("Deckle");
                    dt.Columns.Add("CuttingSize");
                    dt.Columns.Add("FluteName");
                    dt.Columns.Add("TakeUpFactor");
                    dt.Columns.Add("CustomerName");
                    dt.Columns.Add("BoardGSM");
                    dt.Columns.Add("BoardBS");
                    dt.Columns.Add("PName");
                    dt.Columns.Add("BType");
                    dt.Columns.Add("LayerTKF");
                       dt.Columns.Add("ColorName");
                       dt.Columns.Add("Quantity");
                       dt.Columns.Add("LayerWt");
                    

                    foreach (Vw_BoxDet entity in BillList)
                    {
                        DataRow row = dt.NewRow();

                        row["Pk_BoxID"] = entity.Pk_BoxID;
                        row["Name"] = entity.Name;
                        row["Length"] = entity.Length;
                        row["Width"] = entity.Width;
                        row["Height"] = entity.Height;
                        row["Weight"] = entity.Weight;
                        row["GSM"] = entity.GSM;
                        row["BF"] = entity.BF;
                        row["Pk_PartPropertyID"] = entity.Pk_PartPropertyID;
                        row["OuterShellYes"] = entity.OuterShellYes;
                        row["CapYes"] = entity.CapYes;
                        row["LenghtPartationYes"] = entity.LenghtPartationYes;
                        row["WidthPartationYes"] = entity.WidthPartationYes;
                        row["PlateYes"] = entity.PlateYes;
                        row["MaterialName"] = entity.MaterialName;
                        row["BType"] = entity.BType;
                        row["PName"] = entity.PName;
                        row["CustomerName"] = entity.CustomerName;
                        row["BoardBS"] = entity.BoardBS;
                        row["BoardGSM"] = entity.BoardGSM;
                        row["Deckle"] = entity.Deckle;
                        row["CuttingSize"] = entity.CuttingSize;
                        row["FluteName"] = entity.FluteName;
                        row["TakeUpFactor"] = entity.TakeUpFactor;
                        row["LayerTKF"] = entity.LayerTKF;
                        row["ColorName"] = entity.ColorName;
                        row["Quantity"] = entity.Quantity;
                        row["PlyVal"] = entity.PlyVal;
                        row["LayerWt"] = entity.LayerWt;


                        dt.Rows.Add(row);
                    }


                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
             

                    string pdfPath = "";
                   
                        CogitoStreamline.Report.BoxReport orp = new CogitoStreamline.Report.BoxReport();
                        orp.Load("@\\Report\\BoxReport.rpt");
                        orp.SetDataSource(dt.DefaultView);

                         pdfPath = Server.MapPath("~/ConvertPDF/" + "BoxReport" + sInvno + ".pdf");
                         FileInfo file = new FileInfo(pdfPath);
                         if (file.Exists)
                         {
                             file.Delete();
                         }
                         var pd = new PrintDocument();


                         orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                         orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                         DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                         objDiskOpt.DiskFileName = pdfPath;

                         orp.ExportOptions.DestinationOptions = objDiskOpt;
                         orp.Export();
                   
                  

                }
                ////if ends here



                return null;
            }
            catch (Exception ex)
            {
                return null;
                
            }
        }



         public JsonResult BoxRepPBoard([System.Web.Http.FromBody]string data)
        
        {
            var BoxTyp = "";
            try
            {
                data = data.TrimStart('=');
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["fkbox"]);

                List<Vw_BoxDet> BillList = new List<Vw_BoxDet>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                //BillList = dc.VwInvoices.ToList();
                BillList = dc.Vw_BoxDet.Where(x => x.Pk_BoxID == sInvno).Select(x => x).OfType<Vw_BoxDet>().ToList();
                if (BillList.Count > 0)
                {
                    var ctr = 0;
                    BoxTyp = BillList.ElementAt(0).BType;
                }

                if (BillList.Count > 0)
                {
                    DataTable dt = new DataTable();

                    dt.Columns.Add("Pk_BoxID");
                    dt.Columns.Add("Name");
                    dt.Columns.Add("Length");
                    dt.Columns.Add("Width");
                    dt.Columns.Add("Height");
                    dt.Columns.Add("Weight");
                    dt.Columns.Add("GSM");
                    dt.Columns.Add("BF");
                    dt.Columns.Add("Pk_PartPropertyID");
                    dt.Columns.Add("OuterShellYes");
                    dt.Columns.Add("CapYes");
                    dt.Columns.Add("LenghtPartationYes");
                    dt.Columns.Add("WidthPartationYes");
                    dt.Columns.Add("PlateYes");
                    dt.Columns.Add("MaterialName");
                    dt.Columns.Add("Customer");
                    dt.Columns.Add("PartNo");
                    dt.Columns.Add("PlyVal");
                    dt.Columns.Add("Deckle");
                    dt.Columns.Add("CuttingSize");
                    dt.Columns.Add("FluteName");
                    dt.Columns.Add("TakeUpFactor");
                    dt.Columns.Add("CustomerName");
                    dt.Columns.Add("BoardGSM");
                    dt.Columns.Add("BoardBS");
                    dt.Columns.Add("PName");
                    dt.Columns.Add("BType");
                    dt.Columns.Add("LayerTKF");
                    dt.Columns.Add("ColorName");
                    dt.Columns.Add("Quantity");
                    dt.Columns.Add("LayerWt");


                    foreach (Vw_BoxDet entity in BillList)
                    {
                        DataRow row = dt.NewRow();

                        row["Pk_BoxID"] = entity.Pk_BoxID;
                        row["Name"] = entity.Name;
                        row["Length"] = entity.Length;
                        row["Width"] = entity.Width;
                        row["Height"] = entity.Height;
                        row["Weight"] = entity.Weight;
                        row["GSM"] = entity.GSM;
                        row["BF"] = entity.BF;
                        row["Pk_PartPropertyID"] = entity.Pk_PartPropertyID;
                        row["OuterShellYes"] = entity.OuterShellYes;
                        row["CapYes"] = entity.CapYes;
                        row["LenghtPartationYes"] = entity.LenghtPartationYes;
                        row["WidthPartationYes"] = entity.WidthPartationYes;
                        row["PlateYes"] = entity.PlateYes;
                        row["MaterialName"] = entity.MaterialName;
                        row["BType"] = entity.BType;
                        row["PName"] = entity.PName;
                        row["CustomerName"] = entity.CustomerName;
                        row["BoardBS"] = entity.BoardBS;
                        row["BoardGSM"] = entity.BoardGSM;
                        row["Deckle"] = entity.Deckle;
                        row["CuttingSize"] = entity.CuttingSize;
                        row["FluteName"] = entity.FluteName;
                        row["TakeUpFactor"] = entity.TakeUpFactor;
                        row["LayerTKF"] = entity.LayerTKF;
                        row["ColorName"] = entity.ColorName;
                        row["Quantity"] = entity.Quantity;
                        row["PlyVal"] = entity.PlyVal;
                        row["LayerWt"] = entity.LayerWt;


                        dt.Rows.Add(row);
                    }


                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);


                    string pdfPath = "";

                    CogitoStreamline.Report.BoxReportPlainBrd orp = new CogitoStreamline.Report.BoxReportPlainBrd();
                    orp.Load("@\\Report\\BoxReportPlainBrd.rpt");
                    orp.SetDataSource(dt.DefaultView);

                    pdfPath = Server.MapPath("~/ConvertPDF/" + "BoxReportPBoard" + sInvno + ".pdf");
                    FileInfo file = new FileInfo(pdfPath);
                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();
                }
                ////if ends here



                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }

        public ActionResult BoxDist(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["fkbox"]);


                List<BoxMaster> BillList = new List<BoxMaster>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                //BillList = dc.VwInvoices.ToList();
                BillList = dc.BoxMaster.Where(x => x.Pk_BoxID == sInvno).Select(x => x).OfType<BoxMaster>().ToList();

                if (BillList.Count > 0)
                {
                    DataTable dt = new DataTable();

                    dt.Columns.Add("Pk_BoxID");
                    dt.Columns.Add("Name");
                    //dt.Columns.Add("Length");
                    //dt.Columns.Add("Width");
                    //dt.Columns.Add("Height");
                    //dt.Columns.Add("Weight");
                    //dt.Columns.Add("GSM");
                    //dt.Columns.Add("BF");
                    //dt.Columns.Add("Pk_PartPropertyID");
                    //dt.Columns.Add("OuterShellYes");
                    //dt.Columns.Add("CapYes");
                    //dt.Columns.Add("LenghtPartationYes");
                    //dt.Columns.Add("WidthPartationYes");
                    //dt.Columns.Add("PlateYes");
                    //dt.Columns.Add("MaterialName");

                    //dt.Columns.Add("Customer");
                    //dt.Columns.Add("PartNo");
                    //dt.Columns.Add("PlyVal");
                    //dt.Columns.Add("Deckle");
                    //dt.Columns.Add("CuttingSize");
                    //dt.Columns.Add("FluteName");
                    //dt.Columns.Add("TakeUpFactor");
                    //dt.Columns.Add("CustomerName");


                    foreach (BoxMaster entity in BillList)
                    {
                        DataRow row = dt.NewRow();

                        row["Pk_BoxID"] = entity.Pk_BoxID;
                        row["Name"] = entity.Name;
                        //row["Length"] = entity.Length;
                        //row["Width"] = entity.Width;
                        //row["Height"] = entity.Height;
                        //row["Weight"] = entity.Weight;
                        //row["GSM"] = entity.GSM;
                        //row["BF"] = entity.BF;
                        //row["Pk_PartPropertyID"] = entity.Pk_PartPropertyID;
                        //row["OuterShellYes"] = entity.OuterShellYes;
                        //row["CapYes"] = entity.CapYes;
                        //row["LenghtPartationYes"] = entity.LenghtPartationYes;
                        //row["WidthPartationYes"] = entity.WidthPartationYes;
                        //row["PlateYes"] = entity.PlateYes;
                        //row["MaterialName"] = entity.MaterialName;

                        //row["Customer"] = entity.Customer;
                        //row["CustomerName"] = entity.CustomerName;
                        //row["PartNo"] = entity.PartNo;
                        //row["PlyVal"] = entity.PlyVal;
                        //row["Deckle"] = entity.Deckle;
                        //row["CuttingSize"] = entity.CuttingSize;
                        //row["FluteName"] = entity.FluteName;
                        //row["TakeUpFactor"] = entity.TakeUpFactor;
                        //;


                        dt.Rows.Add(row);
                    }

                var    Pk_BoxID = sInvno;
                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.Box orp = new CogitoStreamline.Report.Box();

                    orp.Load("@\\Report\\Box.rpt");
                    orp.SetDataSource(dt.DefaultView);


                    CrystalDecisions.CrystalReports.Engine.ReportDocument repDoc = orp;

                    ParameterFieldDefinitions crParameterFieldDefinitions3;
                    ParameterFieldDefinition crParameterFieldDefinition3;
                    ParameterValues crParameterValues3 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue3 = new ParameterDiscreteValue();
                    crParameterDiscreteValue3.Value = Pk_BoxID;
                    crParameterFieldDefinitions3 = orp.DataDefinition.ParameterFields;
                    crParameterFieldDefinition3 = crParameterFieldDefinitions3["Pk_BoxID"];
                    crParameterValues3 = crParameterFieldDefinition3.CurrentValues;

                    crParameterValues3.Clear();
                    crParameterValues3.Add(crParameterDiscreteValue3);
                    crParameterFieldDefinition3.ApplyCurrentValues(crParameterValues3);

                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "Box" + sInvno + ".pdf");
                    FileInfo file = new FileInfo(pdfPath);
                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();

                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }
        [HttpPost]
        public JsonResult FindWeight(string data)
        {
            decimal? CCode = 0;
            int i = 0;
            decimal TempVal;
            decimal WtBox;


            if (data != "{}")
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

                decimal pId = Convert.ToDecimal(oValues["Boxid"]);
                TempVal=0;
                WtBox = 0;

                CogitoStreamLineEntities dc= new CogitoStreamLineEntities();
                List<Vw_BoxSpec> oPrdListChild = new List<Vw_BoxSpec>();
                oPrdListChild = dc.Vw_BoxSpec.Where(p => p.Pk_BoxID==pId).Select(p => p).OfType<Vw_BoxSpec>().ToList();


                int RCount = _oEntites.Vw_BoxSpec.Where(p => p.Pk_BoxID == pId).Count();
                if (RCount > 0)
                {

                    //Vw_BoxSpec oQC = _oEntites.Vw_BoxSpec.Where(p => p.Pk_BoxID == pId).Single();

                    for (i = 0; i < RCount; i++)
                    {
                        TempVal = Convert.ToDecimal(oPrdListChild.ElementAt(i).Weight);

                        WtBox = WtBox + TempVal;
                    }

                    CCode = WtBox;
                }

                else
                { CCode = 0; }

            }
            return Json(CCode);
        }


        public JsonResult Print([System.Web.Http.FromBody]string data)
        {
            //var DataRecd = "";
            //var data = saveRequest.data;

            try
            {
                data = data.TrimStart('=');
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["fkbox"]);

                List<Vw_BoxDet> BillList = new List<Vw_BoxDet>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                //BillList = dc.VwInvoices.ToList();
                BillList = dc.Vw_BoxDet.Where(x => x.Pk_BoxID == sInvno).Select(x => x).OfType<Vw_BoxDet>().ToList();

                if (BillList.Count > 0)
                {
                    DataTable dt = new DataTable();

                    dt.Columns.Add("Pk_BoxID");
                    dt.Columns.Add("Name");
                    dt.Columns.Add("Length");
                    dt.Columns.Add("Width");
                    dt.Columns.Add("Height");
                    dt.Columns.Add("Weight");
                    dt.Columns.Add("GSM");
                    dt.Columns.Add("BF");
                    dt.Columns.Add("Pk_PartPropertyID");
                    dt.Columns.Add("OuterShellYes");
                    dt.Columns.Add("CapYes");
                    dt.Columns.Add("LenghtPartationYes");
                    dt.Columns.Add("WidthPartationYes");
                    dt.Columns.Add("PlateYes");
                    dt.Columns.Add("MaterialName");
                    dt.Columns.Add("Customer");
                    dt.Columns.Add("PartNo");
                    dt.Columns.Add("PlyVal");
                    dt.Columns.Add("Deckle");
                    dt.Columns.Add("CuttingSize");
                    dt.Columns.Add("FluteName");
                    dt.Columns.Add("TakeUpFactor");
                    dt.Columns.Add("CustomerName");
                    dt.Columns.Add("BoardGSM");
                    dt.Columns.Add("BoardBS");
                    dt.Columns.Add("PName");
                    dt.Columns.Add("BType");
                    dt.Columns.Add("LayerTKF");
                    dt.Columns.Add("ColorName");
                    dt.Columns.Add("Quantity");
                    dt.Columns.Add("LayerWt");
                    dt.Columns.Add("NoBoards");


                    foreach (Vw_BoxDet entity in BillList)
                    {
                        DataRow row = dt.NewRow();

                        row["Pk_BoxID"] = entity.Pk_BoxID;
                        row["Name"] = entity.Name;
                        row["Length"] = entity.Length;
                        row["Width"] = entity.Width;
                        row["Height"] = entity.Height;
                        row["Weight"] = entity.Weight;
                        row["GSM"] = entity.GSM;
                        row["BF"] = entity.BF;
                        row["Pk_PartPropertyID"] = entity.Pk_PartPropertyID;
                        row["OuterShellYes"] = entity.OuterShellYes;
                        row["CapYes"] = entity.CapYes;
                        row["LenghtPartationYes"] = entity.LenghtPartationYes;
                        row["WidthPartationYes"] = entity.WidthPartationYes;
                        row["PlateYes"] = entity.PlateYes;
                        row["MaterialName"] = entity.MaterialName;
                        row["BType"] = entity.BType;
                        row["PName"] = entity.PName;
                        row["CustomerName"] = entity.CustomerName;
                        row["BoardBS"] = entity.BoardBS;
                        row["BoardGSM"] = entity.BoardGSM;
                        row["Deckle"] = entity.Deckle;
                        row["CuttingSize"] = entity.CuttingSize;
                        row["FluteName"] = entity.FluteName;
                        row["TakeUpFactor"] = entity.TakeUpFactor;
                        row["LayerTKF"] = entity.LayerTKF;
                        row["ColorName"] = entity.ColorName;
                        row["Quantity"] = entity.Quantity;
                        row["PlyVal"] = entity.PlyVal;
                        row["LayerWt"] = entity.LayerWt;
                        row["NoBoards"] = entity.NoBoards;


                        dt.Rows.Add(row);
                    }


                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.BoxReport orp = new CogitoStreamline.Report.BoxReport();

                    string RepPath = Server.MapPath("~/Report/BoxReport.rpt");
                  //  orp.Load("@\\Report\\BoxReport.rpt");
                    orp.Load(RepPath);
                    orp.SetDataSource(dt.DefaultView);

                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "BoxReport" + sInvno + ".pdf");
                    FileInfo file = new FileInfo(pdfPath);
                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();

                }
                return null;
            }
            
            catch (Exception ex)
            { 
                return Json(new { Result = "ERROR", Message = ex.Message + "INCOMING DATA IS " + data });
            }
      
        }


        public ActionResult UpdateRow(string data = "")
        {
            try
            {

                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);



                //_comLayer.parameters.add("fkbox", fkbox);
                //_comLayer.parameters.add("dec", dec);
                //_comLayer.parameters.add("CL", CL);
                //_comLayer.parameters.add("Ups", Ups);




                int BoxID = Convert.ToInt32(oValues["fkbox"]);
                int dec = Convert.ToInt32(oValues["dec"]);
                decimal CL = Convert.ToDecimal(oValues["CL"]);
                int Ups = Convert.ToInt32(oValues["Ups"]);

                //List<vw_PaperRollStock> PaperList = new List<vw_PaperRollStock>();

                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();
                if (BoxID > 0)
                {
                    //Vw_BoxDet oBoxList = _oEntites.Vw_BoxDet.Where(x => x.Pk_BoxID == BoxID).FirstOrDefault();

                   // var ItemP = oBoxList.Pk_PartPropertyID;

                    ItemPartProperty Oitem = _oEntites.ItemPartProperty.Where(x => x.Pk_PartPropertyID == BoxID).FirstOrDefault();

                    //var ExQty = oBoxList.Quantity;
                    //var TotQty = ExQty + StockVal;
                    Oitem.Deckle = dec;
                    Oitem.CuttingSize = CL;
                    Oitem.NoBoards = Ups;

                    _oEntites.SaveChanges();
                }


                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }

    }
}

