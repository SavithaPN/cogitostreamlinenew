﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using ManufacturingModel;
using ManufacturingModel.DomainModel;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using WebGareCore;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;
using System.Data.SqlClient;
using System.Data;


namespace ManufacturingERP.Controllers
{


    public class SampBoxSpecController : CommonController
    {
        //
        // GET: /FlexEst/
        public decimal PlyValue = 0;
        public string PlyV = "";

        public decimal len = 0;
        public decimal Wd = 0;
        public decimal Ht = 0;
        FType oType = new FType();

        Estimation oEst;
        //BoxM oBox;
        ODID oOd = new ODID();

        public SampBoxSpecController()
        {
            oDoaminObject = new SampBoxSpe();
            oEst = new Estimation();

        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }


        public override ActionResult Index()
        {
            return base.Index();
        }


        public override JsonResult Load(string pId = "")
        {
            return null;
        }
        //[HttpPost]
        //public ActionResult RecvFk(string data = "")
        //{
        //    SqlConnection cn;
        //    Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        //    int sfkb = Convert.ToInt32(oValues["fkbox"]);


        //    cn = new SqlConnection("data Source=Hariprasad\\SQLEXPRESS; Initial Catalog=DB_A3CED5_V2InnovativesTesting; Integrated Security=SSPI");

        //    SqlCommand scom = new SqlCommand("INSERT INTO TempCheck (Upd_Val) VALUES (@Address)", cn);
        //    scom.Parameters.Add("Address", SqlDbType.Int);
        //    scom.Parameters["Address"].Value = sfkb;

        //    cn.Open();
        //    scom.ExecuteNonQuery();
        //    //cn.Close();



        //    return null;
        //}


        //[HttpPost]

        //public JsonResult SaveNew([System.Web.Http.FromBody]string data)
        //{
        //    data = data.TrimStart('=');
        //    Dictionary<string, object> values = JsonConvert.DeserializeObject<Dictionary<string, object>>(data);
        //    data = JsonConvert.SerializeObject(values);
        //    return base.Save(data);


        //}

        [HttpPost]
        public JsonResult LoadBySpecID([System.Web.Http.FromBody]string data)
        {
            data = data.TrimStart('=');
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            //string[] parts1 = oValues.


            string eId = oValues["BoxId"];

            string myString = eId;
            string[] parts = myString.Split(',');

            string eqchild = "";
            string boxEstId = "";
            string boxEStChild = "";
            string FType = "";
            string FTypeVal = "";

            bool bEditMode = false;
            //if (oValues.Count == 3 && oValues["EnqChild"] != "")
            //{
            //    eId = parts[0];
            //    eqchild = oValues["EnqChild"];
            //    //int pos = (pStr.IndexOf("=")) + 1;

            //    boxEstId = oValues["Fk_BoxEstimation"];
            //    boxEStChild = oValues["Pk_BoxEstimationChild"];
            //    bEditMode = true;
            //}


            if (parts.Length == 3)
            {
                eId = parts[0];

                FType = parts[2];
                var ss = FType.Split('=');
                FTypeVal = ss[1];
            }
            else
                if (parts.Length == 1 && oValues.Count != 3 && oValues.Count != 1)
                {
                    eId = parts[0];

                    eqchild = oValues["EnqChild"];
                    boxEstId = oValues["BoxEstimation"];
                    boxEStChild = oValues["BoxEstimationChild"];
                    bEditMode = true;
                }
                else
                    if (parts.Length == 2)
                    {
                        eId = parts[0];
                    }
                    else
                        if (parts.Length == 7)
                        {
                            eId = parts[0];
                            eqchild = parts[2];
                            boxEstId = parts[4];
                            bEditMode = true;
                        }
                        else
                            if (parts.Length == 4)
                            {
                                eId = parts[0];
                                eqchild = parts[2];
                                boxEstId = parts[3];
                                bEditMode = true;
                            }


            if (eId != "")
            {

                //if (FTypeVal != "")
                //{
                //    var FluteID = int.Parse(FTypeVal);

                //    SampBoxSpe oBoxSp1 = oDoaminObject as SampBoxSpe;
                //    SampleBoxSpec oAtualObject1 = oBoxSp1.DAO as SampleBoxSpec;
                //    var PP = oAtualObject1.Pk_BoxSpecID;
                //    var PlyValue1 = oAtualObject1.ItemPartProperty.Where(x => x.Fk_BoxSpecsID == PP).Select(x => x).OfType<ItemPartProperty>().ToList();

                //    PlyV = (PlyValue1[0].PlyVal).ToString();
                //    ModelManuplationResult oResult = new ModelManuplationResult();

                //    List<SearchParameter> oSearchParams = new List<SearchParameter>();
                //    oSearchParams.Add(new SearchParameter("FTypeVal", FTypeVal));
                //    oSearchParams.Add(new SearchParameter("PlyV", PlyV));

                //    SearchResult oSearchResult = oOd.Search(oSearchParams);

                //    List<EntityObject> oEnquirysToDisplayObjects = oSearchResult.ListOfRecords;
                //    List<OD_ID> oEnquiryObjects = oEnquirysToDisplayObjects.Select(p => p).OfType<OD_ID>().ToList();
                //    var oHeadToDisplay = oEnquiryObjects.Select(p => new
                //    {   
                //        len = p.Length,
                //        Wd = p.Width,
                //        Ht = p.Height

                //    }).ToList();

                //}



                SampBoxSpe oBoxSp = oDoaminObject as SampBoxSpe;

                if (oBoxSp.SetSpecId(decimal.Parse(eId)))
                {
                    SampleBoxSpec oAtualObject = oBoxSp.DAO as SampleBoxSpec;

                    //var PP = oAtualObject.Pk_BoxSpecID;
                    //var PlyValue1 = oAtualObject.ItemPartProperty.Where(x => x.Fk_BoxSpecsID == PP).Select(x => x).OfType<ItemPartProperty>().ToList();

                    //PlyV = (PlyValue1[0].PlyVal).ToString();


                    //ModelManuplationResult oResult = new ModelManuplationResult();

                    //List<SearchParameter> oSearchParams = new List<SearchParameter>();
                    //oSearchParams.Add(new SearchParameter("FTypeVal", FTypeVal));
                    //oSearchParams.Add(new SearchParameter("PlyV", PlyV));

                    //SearchResult oSearchResult = oOd.Search(oSearchParams);

                    //List<EntityObject> oEnquirysToDisplayObjects = oSearchResult.ListOfRecords;
                    //List<OD_ID> oEnquiryObjects = oEnquirysToDisplayObjects.Select(y => y).OfType<OD_ID>().ToList();
                    //var oHeadToDisplay = oEnquiryObjects.Select(y => new
                    //{
                    //    len = y.Length,
                    //    Wd = y.Width,
                    //    Ht = y.Height

                    //}).ToList();

                    //let us get parts of the estimation
                    var oParts = oAtualObject.SampleItemPartProperty.Select(p => new
                    {
                        id = p.Pk_PartPropertyID,
                        layers = Json(p.SampleItems_Layers.Select(x => new
                        {
                            id = x.Pk_LayerID,
                            gsm = x.GSM,
                            bf = x.BF,
                            color = x.Color,
                            Fk_Material = x.Fk_Material,
                            rate = x.Tk_Factor,
                            Weight = x.Weight,
                            //Tk_Factor=x.Tk_Factor,

                        })).Data,

                        SID = p.Pk_PartPropertyID,
                        length = p.Length,
                        width = p.Width,
                        height = p.Height,
                        boardArea = p.BoardArea,
                        deckle = p.Deckle,
                        cuttingSize = p.CuttingSize,
                        quantity = p.Quantity,
                        noBoards = p.NoBoards,
                        weight = p.Weight,
                        Rate = p.Rate,
                        AddBLength = p.AddBLength,
                        OTakeUpFactor = p.TakeUpFactor,
                        Ply = p.PlyVal,

                        BoardGSM = p.BoardGSM,
                        BoardBS = p.BoardBS,
                        //OLength1 =p.OLength1,
                        //OWidth1 = p.OWidth1,
                        //OHeight1 = p.OHeight1

                    });

                    //////////////-------02032017

                    //if (FTypeVal != "")
                    //{
                    //    var FluteID = int.Parse(FTypeVal);

                    //    SampBoxSpe oBoxSp1 = oDoaminObject as SampBoxSpe;
                    //    SampleBoxSpec oAtualObject1 = oBoxSp1.DAO as SampleBoxSpec;
                    //    var PP = oAtualObject1.Pk_BoxSpecID;
                    //    var PlyValue1 = oAtualObject1.ItemPartProperty.Where(x => x.Fk_BoxSpecsID == PP).Select(x => x).OfType<ItemPartProperty>().ToList();

                    //    PlyV = (PlyValue1[0].PlyVal).ToString();
                    //    ModelManuplationResult oResult = new ModelManuplationResult();

                    //    List<SearchParameter> oSearchParams = new List<SearchParameter>();
                    //    oSearchParams.Add(new SearchParameter("FTypeVal", FTypeVal));
                    //    oSearchParams.Add(new SearchParameter("PlyV", PlyV));

                    //    SearchResult oSearchResult = oOd.Search(oSearchParams);

                    //    List<EntityObject> oEnquirysToDisplayObjects = oSearchResult.ListOfRecords;
                    //    List<OD_ID> oEnquiryObjects = oEnquirysToDisplayObjects.Select(p => p).OfType<OD_ID>().ToList();
                    //    var oHeadToDisplay = oEnquiryObjects.Select(p => new
                    //    {
                    //        OLength1 = p.Length,
                    //        OWidth1 = p.Width,
                    //        OHeight1 = p.Height

                    //    }).ToList();

                    //}

                    ////////
                    var oEstimateToDisplay = new object();
                    if (bEditMode)
                    {
                        oEst.ID = decimal.Parse(boxEstId);
                        est_BoxEstimation oAtualObjectBoxEst = oEst.DAO as est_BoxEstimation;

                        est_BoxEstimationChild oBoxEstimationChild = oAtualObjectBoxEst.est_BoxEstimationChild.Where(p => p.Fk_EnquiryChild == decimal.Parse(eqchild)).Single();

                        oEstimateToDisplay = new
                        {
                            ID = oAtualObject.Pk_BoxSpecID,
                            plateYes = oAtualObject.PlateYes,
                            widthPartationYes = oAtualObject.WidthPartationYes,
                            lenghtPartationYes = oAtualObject.LenghtPartationYes,
                            outerShellYes = oAtualObject.OuterShellYes,
                            capYes = oAtualObject.CapYes,
                            outerShell = oAtualObject.OuterShell,
                            sleeve = oAtualObject.Sleeve,
                            sleeveYes = oAtualObject.SleeveYes,
                            plate = oAtualObject.Plate,
                            topYes = oAtualObject.TopYes,
                            bottomYes = oAtualObject.BottomYes,
                            widthPartation = oAtualObject.WidthPartation,
                            lenghtPartation = oAtualObject.LenghtPartation,
                            cap = oAtualObject.Cap,
                            topval = oAtualObject.TopVal,
                            bottval = oAtualObject.BottVal,
                            parts = Json(oParts).Data,


                            //totalWeight = oAtualObject.TotalWeight,

                            estID = oEst.ID,
                            convRate = oBoxEstimationChild.ConvRate,
                            convValue = oBoxEstimationChild.ConvValue,
                            gMarginPercentage = oBoxEstimationChild.GMarginPercentage,
                            gMarginValue = oBoxEstimationChild.GMarginValue,
                            handlingChanrgesValue = oBoxEstimationChild.HandlingChanrgesValue,
                            packingChargesValue = oBoxEstimationChild.PackingChargesValue,
                            rejectionPercentage = oBoxEstimationChild.RejectionPercentage,
                            rejectionValue = oBoxEstimationChild.RejectionValue,
                            taxesPercntage = oBoxEstimationChild.TaxesPercntage,
                            taxesValue = oBoxEstimationChild.TaxesValue,
                            totalPrice = oBoxEstimationChild.TotalPrice,
                            transportValue = oBoxEstimationChild.TransportValue,
                            weightHValue = oBoxEstimationChild.WeightHValue,
                            VATPercentage = oBoxEstimationChild.VATPercentage,
                            VATValue = oBoxEstimationChild.VATValue,
                            CSTPercentage = oBoxEstimationChild.CSTPercentage,
                            CSTValue = oBoxEstimationChild.CSTValue,
                            TransportPercentage = oBoxEstimationChild.TransportPercentage,

                            //eqEnqChild=oBoxEstimationChild.Fk_EnquiryChild
                        };

                    }
                    else
                    {
                        //var FluteID = int.Parse(FTypeVal);

                        //SampBoxSpe oBoxSp1 = oDoaminObject as SampBoxSpe;
                        //SampleBoxSpec oAtualObject1 = oBoxSp1.DAO as SampleBoxSpec;

                        //var PP = oAtualObject1.Pk_BoxSpecID;
                        //var PlyValue1 = oAtualObject1.ItemPartProperty.Where(x => x.Fk_BoxSpecsID == PP).Select(x => x).OfType<ItemPartProperty>().ToList();
                        // PlyV = (PlyValue1[0].PlyVal).ToString();

                        //FType oFT=oDoaminObject as FType;
                        //FluteType oAtualObject2 = oFT.DAO as FluteType;

                        //var DetDisplay = oAtualObject2.OD_ID.Where(x => x.Fk_FluteID == FluteID && x.PlyValue == PlyValue1[0].PlyVal).Select(x => x).OfType<OD_ID>().ToList();
                        //Create a anominious object here to break the circular reference
                        oEstimateToDisplay = new
                        {
                            ID = oAtualObject.Pk_BoxSpecID,
                            plateYes = oAtualObject.PlateYes,
                            widthPartationYes = oAtualObject.WidthPartationYes,
                            lenghtPartationYes = oAtualObject.LenghtPartationYes,
                            outerShellYes = oAtualObject.OuterShellYes,
                            capYes = oAtualObject.CapYes,
                            topYes = oAtualObject.TopYes,
                            bottomYes = oAtualObject.BottomYes,
                            outerShell = oAtualObject.OuterShell,
                            sleeve = oAtualObject.Sleeve,
                            sleeveYes = oAtualObject.SleeveYes,
                            plate = oAtualObject.Plate,
                            widthPartation = oAtualObject.WidthPartation,
                            lenghtPartation = oAtualObject.LenghtPartation,
                            cap = oAtualObject.Cap,
                            topval = oAtualObject.TopVal,
                            bottval = oAtualObject.BottVal,
                            parts = Json(oParts).Data,


                            /////////////////////

                            /////////////////////
                            estID = (decimal?)null,


                        };
                    }

                    return Json(new { success = true, data = oEstimateToDisplay });
                    //return Json(new { success = true, data = oHeadToDisplay });
                }
                else
                {
                    return Json(new { success = false, data = "" });
                }
            }
            else
            {
                //var oEstimateToDisplay = new gen_Estimate();
                //return Json(new { success = true, data = oEstimateToDisplay });
                return null;
            }
        }



        //[HttpPost]
        //public JsonResult getFType(string pId = "")
        //{
        //    List<FType> oListOfCommunicationTypes = oType.Search(null).ListOfRecords.OfType<FType>().ToList();
        //    var oContriesToDisplay = oListOfCommunicationTypes.Select(p => new
        //    {
        //        Name =p.
        //        Id = p.
        //    });

        //    return Json(oContriesToDisplay);
        //}

        [HttpPost]
        public JsonResult BoxSpecDetails(string Pk_BoxSpecID = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                SampBoxSpe oBoxMaster = oDoaminObject as SampBoxSpe;

                oBoxMaster.ID = decimal.Parse(Pk_BoxSpecID);

                SampleBoxSpec oEnquiryObjects = oBoxMaster.DAO as SampleBoxSpec;

                var oDeliveryScheduleToDisplay = oEnquiryObjects.SampleItemPartProperty.Select(p => new
                {
                    Pk_PartPropertyID = p.Pk_PartPropertyID,
                    Length = p.Length,
                    Width = p.Width,
                    Height = p.Height,
                    Weight = p.Weight,
                    BS = p.BoardBS,
                    Deckle = p.Deckle,
                    CL = p.CuttingSize,

                }).ToList();

                return Json(new { Result = "OK", Records = oDeliveryScheduleToDisplay, TotalRecordCount = oDeliveryScheduleToDisplay.Count() });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public override JsonResult Save(string data)
        {
            //BoxMaster oGetTemp;
            Dictionary<string, object> values = JsonConvert.DeserializeObject<Dictionary<string, object>>(data);
            string sInvno = values["partsCopy"].ToString();

            data = JsonConvert.SerializeObject(values);
            //this.CurrentObject = null;
            return base.Save(data);


        }

        //[HttpPost]
        //public override JsonResult Save(string data)
        //{
        //    //BoxMaster oGetTemp;
        //    Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        //    int sInvno = Convert.ToInt32(oValues["Set"]);


        //    //if (this.CurrentObject != null)
        //    //    oGetTemp = this.CurrentObject as BoxMaster;
        //    //else
        //    //    oGetTemp = new BoxMaster();

        //  //  Dictionary<string, object> values = JsonConvert.DeserializeObject<Dictionary<string, object>>(data);

        //    //int PlyVal = Convert.ToInt16(values["Ply"]);
        //    //int PlatePlyVal = Convert.ToInt16(values["PlatePly"]);


        //    //string oGSM = values["outGsm"].ToString();
        //    //string oBF = values["outBF"].ToString();
        //    //string oShade = values["outShade"].ToString();

        //    //string oL = values["OuterL"].ToString();
        //    //string oW = values["OuterW"].ToString();
        //    //string oH = values["OuterH"].ToString();

        //    //string PGSM = values["PlateGsm"].ToString();
        //    //string PBF = values["PlateBF"].ToString();
        //    //string PShade = values["PlateShade"].ToString();


        //    //string oPL = values["PlateL"].ToString();
        //    //string oPW = values["PlateW"].ToString();
        //    //string oPH = values["PlateQty"].ToString();

        //    //var i = 0;

        //    //string[] GSMVal = oGSM.Split(',');
        //    //string[] BFVal = oBF.Split(',');
        //    //string[] ShadeVal = oShade.Split(',');

        //    //string[] LVal = oL.Split(',');
        //    //string[] WVal = oW.Split(',');
        //    //string[] HVal = oH.Split(',');


        //    //string[] PGSMVal = PGSM.Split(',');
        //    //string[] PBFVal = PBF.Split(',');
        //    //string[] PShadeVal = PShade.Split(',');

        //    //string[] PLVal = oPL.Split(',');
        //    //string[] PWVal = oPW.Split(',');
        //    //string[] PHVal = oPH.Split(',');

        //    //for(i=0;i<GSMVal.Length;i++)
        //    //{
        //    //GSMVal = GSMVal[];
        //    //}       

        //    //}

        //    ///////////////////

        //    //var BranchID = Convert.ToDecimal(values["Fk_ToBranch"].ToString());

        //    //OptBranch oQC = _oEntites.OptBranch.Where(p => p.BranchID == BranchID).Single();
        //    //var JCode = oQC.BranchTagCode;
        //    //var MVal = oQC.TagMax;
        //    //values.Add("TagNumber", JCode);
        //    //values.Add("MaxVal", MVal);

        //    //////////////////////

        //    /////////////////////////////////////////
        //    CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

        //    //var BillMax =
        //    //   (from StockTransferChild in dc.StockTransferChild

        //    //    orderby StockTransferChild.Pk_TransferDet descending

        //    //    select StockTransferChild.Pk_TransferDet).Take(1);


        //    ////////////////////////////////////////////
        //    //values.Add("TransMaxVal", BillMax);

        //    //for (int i = 0; i < oGetTemp.StockTransferChild.Count; i++)
        //    //{
        //    //    values.Add("Fk_PrdID" + i, oGetTemp.StockTransferChild.ElementAt(i).Fk_PrdID);
        //    //}


        //    data = JsonConvert.SerializeObject(oValues);
        //    //this.CurrentObject = null;
        //    return base.Save(data);


        //}


    }
}