﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;
using System.IO;


namespace CogitoStreamLine.Controllers
{
    public class EstimationController : CommonController
    {
        //
        // GET: /Estimation/
        Enquiry oEnquiry = new Enquiry();
        eq_Enquiry oEnq = new eq_Enquiry();

        Estimation oEst = new Estimation();

        public EstimationController()
        {
            oDoaminObject = new Estimation();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }


        public override ActionResult Index()
        {
            ViewBag.Header = "Estimation List";
            return base.Index();
        }

        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                Estimation oEstimate = oDoaminObject as Estimation;
                oEstimate.ID = decimal.Parse(pId);
                est_BoxEstimation oAtualObject = oEstimate.DAO as est_BoxEstimation;

                //get Documents
                var oBoxEstimationChild = oAtualObject.est_BoxEstimationChild.Select(p => new
                {
                    ConvRate = p.ConvRate,
                    ConvValue = p.ConvValue,
                    GMarginPercentage = p.GMarginPercentage,
                    GMarginValue = p.GMarginValue,
                    HandlingChanrgesValue = p.HandlingChanrgesValue,
                    PackingChargesValue = p.PackingChargesValue,
                    RejectionPercentage = p.RejectionPercentage,
                    RejectionValue = p.RejectionValue,
                    TaxesPercntage = p.TaxesPercntage,
                    TaxesValue = p.TaxesValue,
                    TotalPrice = p.TotalPrice,
                    TotalWeight = p.TotalWeight,
                    TransportValue = p.TransportValue,
                    WeightHValue = p.WeightHValue,
                    Status = p.Status
                });

                //Create a anominious object here to break the circular reference
                var oBoxEstimationToDisplay = new
                {
                    Pk_BoxEstimation = oAtualObject.Pk_BoxEstimation,
                    Date = oAtualObject.Date,
                    GrandTotal = oAtualObject.GrandTotal,
                    BoxEstimationChilds = Json(oBoxEstimationChild).Data
                };

                return Json(new { success = true, data = oBoxEstimationToDisplay });
            }
            else
            {
                var oBoxEstimationToDisplay = new est_BoxEstimation();
                return Json(new { success = true, data = oBoxEstimationToDisplay });
            }
        }
    

        //////[HttpPost]
        //////public JsonResult EstimationListByFiter(string Pk_Estimate = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        //////{
        //////    try
        //////    {
        //////        List<SearchParameter> oSearchParams = new List<SearchParameter>();

        //////        oSearchParams.Add(new SearchParameter("Pk_Estimate", Pk_Estimate));
        //////        oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
        //////        oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

        //////        SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);
        //////        List<EntityObject> oHeadToDisplayObjects = oSearchResult.ListOfRecords;

        //////        //List<est_Estimation> oHeadObjects = oHeadToDisplayObjects.Select(p => p).OfType<est_Estimation>().ToList();
        //////        List<est_BoxEstimation> oHeadObjects = oHeadToDisplayObjects.Select(p => p).OfType<est_BoxEstimation>().ToList();
        //////        //Create a anominious object here to break the circular reference
        //////        var oHeadToDisplay = oHeadObjects.Select(p => new
        //////        {
        //////            Pk_Estimate = p.Pk_BoxEstimation,
        //////            //Enquiry = p.Fk_Enquiry

        //////        }).ToList().OrderBy(s => s.Pk_Estimate);
        //////        return Json(new { Result = "OK", Records = oHeadToDisplay, TotalRecordCount = oSearchResult.RecordCount });
        //////    }
        //////    catch (Exception ex)
        //////    {
        //////        return Json(new { Result = "ERROR", Message = ex.Message });
        //////    }
        //////}


        [HttpPost]
        public JsonResult EstList(string Pk_BoxEstimation = "", string Fk_Enquiry = "", string Customer = "", string EstDate = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();

                oSearchParams.Add(new SearchParameter("Pk_BoxEstimation", Pk_BoxEstimation));
                oSearchParams.Add(new SearchParameter("Fk_Enquiry", Fk_Enquiry));
                oSearchParams.Add(new SearchParameter("EstDate", EstDate));
                oSearchParams.Add(new SearchParameter("Customer", Customer));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oEst.SearchEst(oSearchParams);
                List<EntityObject> oHeadToDisplayObjects = oSearchResult.ListOfRecords;

                //st<Vw_Estimate> oHeadObjects = oHeadToDisplayObjects.Select(p => p).OfType<Vw_Estimate>().ToList();
                List<Vw_Est_Det> oHeadObjects = oHeadToDisplayObjects.Select(p => p).OfType<Vw_Est_Det>().ToList();
                //Create a anominious object here to break the circular reference
                var oHeadToDisplay = oHeadObjects.Select(p => new
                {
                    Pk_BoxEstimation = p.Pk_BoxEstimation,
                    Enquiry = p.Fk_Enquiry,
                    CustomerName = p.CustomerName,
                  BoxName=p.Name,
                  BoxID=p.Pk_BoxID,
                  Quantity=p.Quantity,
                    Dimension = p.Length + " * " + p.Width+ " * " + p.Height,
                    //Length = p.Length,
                    //Width = p.Width,
                    //Height = p.Height,
                    //TWeight=p.TotalWeight,
                    BSVal=p.BoardBS,
                    GrandTotal = p.GrandTotal,
                   // CostPerBox = (p.GrandTotal/p.Quantity),
                    EstDATE = p.EstDATE != null ? DateTime.Parse(p.EstDATE.ToString()).ToString("dd/MM/yyyy") : "",


                }).ToList();
                return Json(new { Result = "OK", Records = oHeadToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult EstimationListByFiter(string Fk_BoxEstimation = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();

                oSearchParams.Add(new SearchParameter("Fk_BoxEstimation", Fk_BoxEstimation));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);
                List<EntityObject> oHeadToDisplayObjects = oSearchResult.ListOfRecords;

                List<Vw_EstDet1> oHeadObjects = oHeadToDisplayObjects.Select(p => p).OfType<Vw_EstDet1>().ToList();
                //Create a anominious object here to break the circular reference
                var oHeadToDisplay = oHeadObjects.Select(p => new
                {
                    Pk_BoxEstimationChild = p.Pk_BoxEstimationChild,
                    Fk_EnquiryChild = p.Fk_EnquiryChild,
                    Fk_BoxID = p.Fk_BoxID,
                    Name = p.Name
              

                }).ToList();
                return Json(new { Result = "OK", Records = oHeadToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        private void test(string Pk_BoxEstimation = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            List<SearchParameter> oSearchParams = new List<SearchParameter>();

            oSearchParams.Add(new SearchParameter("Pk_BoxEstimation", Pk_BoxEstimation));
            oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
            oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

            SearchResult oSearchResult = oEst.SearchEst(oSearchParams);
            List<EntityObject> oHeadToDisplayObjects = oSearchResult.ListOfRecords;

            //st<Vw_Estimate> oHeadObjects = oHeadToDisplayObjects.Select(p => p).OfType<Vw_Estimate>().ToList();
            List<Vw_Est_Det> oHeadObjects = oHeadToDisplayObjects.Select(p => p).OfType<Vw_Est_Det>().ToList();

            foreach (var result in oHeadObjects)
            {
                Order order = new Order()
                {
                    ID = result.Pk_BoxEstimation,
                    //CustomerName = result.CustomerName,
                    //SchoolNameId = result.EstDATE,                 
                    //Supplier = result.GrandTotal,
                };
                
                //_DBService.InsertOrder(order);
                //_DBService.DeleteOrderTemp(result);

            }

            foreach (var result in oHeadObjects)
            {
                Dictionary<string, object> omValues = null;

                omValues.Add("ID", result.Pk_BoxEstimation);
                omValues.Add("CustomerName", result.CustomerName);
                omValues.Add("EstDATE", result.EstDATE);
                omValues.Add("GrandTotal", result.GrandTotal);
            }

        }
    }
}
