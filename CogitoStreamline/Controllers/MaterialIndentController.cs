﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;
using System.IO;
using System.Data;
using CrystalDecisions.ReportAppServer.ReportDefModel;
using System.Drawing.Printing;
using CrystalDecisions.Shared;

namespace ManufacturingERP.Controllers
{
    public class MaterialIndentController : CommonController
    {
        //
        // GET: /MaterialIndent/
        readonly CogitoStreamLineEntities _oEntities = new CogitoStreamLineEntities();
        Mat_Category oMatCat = new Mat_Category();
        Customer oCustomer = new Customer();
        WebGareCore.DomainModel.Tenants oTanent = new WebGareCore.DomainModel.Tenants();
       Material oMaterial = new Material();
       MaterialIndent oMatInd = new MaterialIndent();
       readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        public MaterialIndentController()
        {
            oDoaminObject = new MaterialIndent();
        }

        public override ActionResult Index()
        {
            ViewBag.Header = "Material Indent";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }


        [HttpPost]
        public JsonResult getMatCat(string pId = "")
        {
            List<Inv_MaterialCategory> oListOfCountry = oMatCat.Search(null).ListOfRecords.OfType<Inv_MaterialCategory>().ToList();
            var oCountryToDisplay = oListOfCountry.Select(p => new
            {
                Name = p.Name,
                Id = p.Pk_MaterialCategory
            });

            return Json(oCountryToDisplay);
        }


        [HttpPost]
        public JsonResult getBranch(string pId = "")
        {
            List<WebGareCore.wgTenant> oListOfTanent = oTanent.Search(null).ListOfRecords.OfType<WebGareCore.wgTenant>().ToList();
            var oTanentToDisplay = oListOfTanent.Select(p => new
            {
                Name = p.TanentName,
                Id = p.Pk_Tanent
            });

            return Json(oTanentToDisplay);
        }

        [HttpPost]
        public JsonResult FindStock(string data)
        {
            CogitoStreamLineEntities dc = new CogitoStreamLineEntities();
            List<Vw_PStock> oPrdListChild = new List<Vw_PStock>();
            decimal? CCode = 0;

            if (data != "{}")
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int i = 0;
                decimal pId = Convert.ToDecimal(oValues["Pk_Material"]);

                int RCount = _oEntites.Vw_TotPaperStockList.Where(p => p.Pk_Material == pId).Count();
                //decimal? tempQty = 0;
                if (RCount > 0)
                {
                    //oPrdListChild = dc.Vw_TotPaperStockList.Where(p => p.Pk_Material == pId).Select(p => p).OfType<Vw_TotPaperStockList>().ToList();
                    //int ListC = oPrdListChild.Count();

                    //for (i = 0; i < ListC; i++)
                    //{
                    //    var CQty = oPrdListChild.ElementAt(i).TQty;
                    //    tempQty = tempQty + CQty;
                    //}

                    Vw_TotPaperStockList oQC = _oEntites.Vw_TotPaperStockList.Where(p => p.Pk_Material == pId).Single();

                    CCode = oQC.TQty;
                }
                else
                { CCode = 0; }


            }
            return Json(CCode);
        }

        [HttpPost]
        public JsonResult FindPendingPO(string data)
        {
            decimal? CCode = 0;
            try
            {
                if (data != "{}")
                {
                    Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

                    decimal pId = Convert.ToDecimal(oValues["Pk_Material"]);
                    decimal StVal = 1;

                    int RCount = _oEntites.Vw_PO.Where(p => p.Pk_Material == pId && p.Fk_Status == StVal).Count();
                    if (RCount > 0)
                    {


                        CogitoStreamLineEntities dc = new CogitoStreamLineEntities();
                        List<Vw_PO> oPrdListChild = new List<Vw_PO>();
                        oPrdListChild = _oEntites.Vw_PO.Where(p => p.Pk_Material == pId && p.Fk_Status == StVal).Select(p => p).OfType<Vw_PO>().ToList();
                        int ListC = oPrdListChild.Count();
                        int i = 0;
                        decimal PrdQty = 0;
                        decimal TempPrdQty = 0;
                        //  PurchaseOrderD oPurQty = _oEntities.PurchaseOrderD.Where(p => p.Fk_PONo == PoNo && p.Fk_Material == oNewInwardD.Fk_Material).Single();
                        //   PurchaseOrderM oPurM = _oEntites.PurchaseOrderM.Where(p => p.Pk_PONo == PoNo).Single();

                        for (i = 0; i < ListC; i++)
                        {
                            TempPrdQty = Convert.ToDecimal(oPrdListChild.ElementAt(i).Quantity) - Convert.ToDecimal(oPrdListChild.ElementAt(i).InwdQty);
                            PrdQty = PrdQty + TempPrdQty;
                        }


                        // Vw_PO oQC = _oEntites.Vw_PO.Where(p => p.Pk_Material == pId && p.Fk_Status == StVal).Single();

                        CCode = PrdQty;
                    }
                    else
                    { CCode = 0; }


                }
                return Json(CCode);
            }
            catch(Exception ex)
            {
                return null;

            }
        }

        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                MaterialIndent oMaterialIndent = oDoaminObject as MaterialIndent;
                oMaterialIndent.ID = decimal.Parse(pId);
                Inv_MaterialIndentMaster oAtualObject = oMaterialIndent.DAO as Inv_MaterialIndentMaster;

                int i = -1;

                var oMaterialIndentDetails = oAtualObject.Inv_MaterialIndentDetails.Select(p => new
                {
                    Pk_MaterialOrderDetailsId = p.Pk_MaterialOrderDetailsId,
                    slno = ++i,
                    Quantity = p.Quantity,

                    RequiredDate = p.RequiredDate!=null? DateTime.Parse(p.RequiredDate.ToString()).ToString("dd/MM/yyyy"):"",
                    Fk_Material = p.Fk_Material,
                    txtFk_Material=p.Inv_Material.Name,
                    Fk_CustomerOrder = p.Fk_CustomerOrder,
                    Unit=p.Inv_Material.gen_Unit.UnitName,
                    // txtFk_Vendor=p.gen_Vendor.VendorName,
                    //txtFk_Mill=p.gen_Mill.MillName,
                    //txtFkMill=p.Fk_Mill,
                    //txtFkVendor=p.Fk_Vendor,
                });

               
                //Create a anominious object here to break the circular reference
                var oMaterialIndentToDisplay = new
                {
                    Pk_MaterialOrderMasterId = oAtualObject.Pk_MaterialOrderMasterId,
                    MaterialIndentDate = DateTime.Parse(oAtualObject.MaterialIndentDate.ToString()).ToString("dd/MM/yyyy"),
                    Fk_VendorId = oAtualObject.Fk_VendorId,
                    Fk_Status = oAtualObject.Fk_Status,
                    Fk_Tanent = oAtualObject.Fk_Tanent,
                    Comments = oAtualObject.Comments,
                    MaterialIndentDetails = Json(oMaterialIndentDetails).Data
                      //Fk_VendorId,
                };

                return Json(new { success = true, data = oMaterialIndentToDisplay });
            }
            else
            {
                var oMaterialIndentToDisplay = new Inv_MaterialIndentMaster();
                return Json(new { success = true, data = oMaterialIndentToDisplay });
            }
        }

        [HttpPost]
        public JsonResult MaterialIndentListByFiter(string Pk_MaterialIndent = null, string OnlyPending = "",  string FromIndentDate = "", string ToIndentDate = "", string Branch = "", string Vendor = "", string Status = "", string OrderNumber = "", string Material = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("OnlyPending", OnlyPending));
                oSearchParams.Add(new SearchParameter("Pk_MaterialIndent", Pk_MaterialIndent));
                oSearchParams.Add(new SearchParameter("FromIndentDate", FromIndentDate));
                oSearchParams.Add(new SearchParameter("ToIndentDate", ToIndentDate));
                oSearchParams.Add(new SearchParameter("Branch", Branch));
                oSearchParams.Add(new SearchParameter("Vendor", Vendor));
                oSearchParams.Add(new SearchParameter("Status", Status));
                oSearchParams.Add(new SearchParameter("OrderNumber", OrderNumber));
                oSearchParams.Add(new SearchParameter("Material", Material));               

                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oMaterialIndentsToDisplayObjects = oSearchResult.ListOfRecords;
                List<Inv_MaterialIndentMaster> oMaterialIndentObjects = oMaterialIndentsToDisplayObjects.Select(p => p).OfType<Inv_MaterialIndentMaster>().ToList();

                //Create a anominious object here to break the circular reference
                var oMaterialIndentsToDisplay = oMaterialIndentObjects.Select(p => new
                {
                   
                    Pk_MaterialOrderMasterId = p.Pk_MaterialOrderMasterId,
                    MaterialIndentDate = p.MaterialIndentDate != null?  DateTime.Parse(p.MaterialIndentDate.ToString()).ToString("dd/MM/yyyy"): "",
                    Vendor = p.Fk_VendorId != null ? p.gen_Vendor.VendorName : "",
                    VendorID = p.Fk_VendorId,
                    StateName = p.wfState.State,
                    Branch = p.Fk_Tanent != null ? p.wgTenant.TanentName:"",
                    User = p.Fk_UserID != null ? p.ideUser.UserName : "",
                    Comments = p.Comments != null ? p.Comments : "",
                    //Branch = p.Fk_Tanent != null ? p.gen_Branch.BranchName : ""
                 //   Branch = p.Fk_Tanent != null ? p.wgTanent.TenantName : ""
                }).ToList();
                       
                return Json(new { Result = "OK", Records = oMaterialIndentsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        //Vw_IndentPOSearch

        [HttpPost]
        public JsonResult MaterialIndentPOList(string Pk_MaterialIndent = null, string OnlyPending = "", string FromIndentDate = "", string ToIndentDate = "", string Branch = "", string Vendor = "", string Status = "", string OrderNumber = "", string Material = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("OnlyPending", OnlyPending));
                oSearchParams.Add(new SearchParameter("Pk_MaterialIndent", Pk_MaterialIndent));
                oSearchParams.Add(new SearchParameter("FromIndentDate", FromIndentDate));
                oSearchParams.Add(new SearchParameter("ToIndentDate", ToIndentDate));
                oSearchParams.Add(new SearchParameter("Branch", Branch));
                oSearchParams.Add(new SearchParameter("Vendor", Vendor));
                oSearchParams.Add(new SearchParameter("Status", Status));
                oSearchParams.Add(new SearchParameter("OrderNumber", OrderNumber));
                oSearchParams.Add(new SearchParameter("Material", Material));

                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oMatInd.SearchIndentPO(oSearchParams);

                List<EntityObject> oMaterialIndentsToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_IndentPOSearch> oMaterialIndentObjects = oMaterialIndentsToDisplayObjects.Select(p => p).OfType<Vw_IndentPOSearch>().ToList();

                //Create a anominious object here to break the circular reference
                var oMaterialIndentsToDisplay = oMaterialIndentObjects.Where(p=>p.PendingQty>0||p.Fk_PONo==null).Select(p => new
                {
                    Pk_MaterialOrderMasterId = p.Pk_MaterialOrderMasterId,
                    MaterialIndentDate = p.MaterialIndentDate != null ? DateTime.Parse(p.MaterialIndentDate.ToString()).ToString("dd/MM/yyyy") : "",
                    //CustomerName = p.CustomerName,
                    MaterialName=p.Name,
                    Quantity=p.IndQty,
                    Color=p.ColorName,
                    Fk_pono=p.Fk_PONo,
                    PendingQty = p.PendingQty == null ? p.IndQty : p.PendingQty,
                    Pk_MaterialOrderDetailsId = p.Pk_MaterialOrderDetailsId,
                    //Mill=p.MillName,
                    //Vendor = p.Fk_VendorId != null ? p.gen_Vendor.VendorName : "",
                    //VendorID = p.Fk_VendorId,
                    //StateName = p.wfState.State,
                    //Branch = p.Fk_Tanent != null ? p.wgTenant.TanentName : "",
                    //User = p.Fk_UserID != null ? p.ideUser.UserName : "",
                    //Branch = p.Fk_Tanent != null ? p.gen_Branch.BranchName : ""
                    //   Branch = p.Fk_Tanent != null ? p.wgTanent.TenantName : ""
                }).ToList().OrderByDescending(s => s.Pk_MaterialOrderMasterId); ;

                return Json(new { Result = "OK", Records = oMaterialIndentsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult IndentGetRec(string Pk_MaterialIndent = "", string Vendor = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {


            try
            {

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_MaterialIndent", Pk_MaterialIndent));

                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oMatInd.SearchIndentDet(oSearchParams);

                List<EntityObject> oColorToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_MIndent> oColorObjects = oColorToDisplayObjects.Select(p => p).OfType<Vw_MIndent>().ToList();


                var oColorToDisplay = oColorObjects.Select(p => new
                {
                    //VendorName=p.VendorName,
                    //MillName=p.MillName,
                    MaterialName = p.Name,
                    Quantity = p.Quantity,
                    PoNo=p.Fk_CustomerOrder,
                    Customer=p.CustomerName,
                    Color=p.ColorName,
                    
                    Pk_Material=p.Pk_Material,
                    Fk_Mill=p.Pk_Mill,
                }).ToList();

                return Json(new { Result = "OK", Records = oColorToDisplay, TotalRecordCount = oSearchResult.RecordCount });

            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }


        }


        [HttpPost]
        public JsonResult IndentGetCertRec(string Pk_MaterialIndent = "", string Vendor = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {


            try
            {

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_MaterialIndent", Pk_MaterialIndent));

                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oMatInd.SearchIndentCertDet(oSearchParams);

                List<EntityObject> oColorToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_IndentCertificate> oColorObjects = oColorToDisplayObjects.Select(p => p).OfType<Vw_IndentCertificate>().ToList();


                var oColorToDisplay = oColorObjects.Select(p => new
                {
                    //VendorName=p.VendorName,
                    MillName = p.MillName,
                    MaterialName = p.Name,
                    Quantity = p.Quantity,
                    PoNo = p.Fk_CustomerOrder,
                    Customer = p.CustomerName,
                    Color = p.ColorName,

                    Pk_Material = p.Pk_Material,
                    Fk_Mill = p.Pk_Mill,
                }).ToList();

                return Json(new { Result = "OK", Records = oColorToDisplay, TotalRecordCount = oSearchResult.RecordCount });

            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }


        }

        public ActionResult IndentRep(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["Pk_MID"]);

                List<Vw_MIndent> BillList = new List<Vw_MIndent>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                //BillList = dc.VwInvoices.ToList();
                BillList = dc.Vw_MIndent.Where(x => x.Pk_MaterialOrderMasterId == sInvno).Select(x => x).OfType<Vw_MIndent>().ToList();


//SELECT     dbo.Inv_MaterialIndentMaster.Pk_MaterialOrderMasterId, dbo.Inv_MaterialIndentMaster.MaterialIndentDate,
                //dbo.gen_Vendor.VendorName, dbo.Inv_MaterialIndentDetails.RequiredDate, 
//                      dbo.Inv_MaterialIndentDetails.Quantity, dbo.Inv_Material.Name
//FROM         dbo.Inv_MaterialIndentDetails INNER JOIN
//                      dbo.Inv_MaterialIndentMaster ON dbo.Inv_MaterialIndentDetails.Fk_MaterialOrderMasterId = dbo.Inv_MaterialIndentMaster.Pk_MaterialOrderMasterId INNER JOIN
//                      dbo.Inv_Material ON dbo.Inv_MaterialIndentDetails.Fk_Material = dbo.Inv_Material.Pk_Material INNER JOIN
//                      dbo.gen_Vendor ON dbo.Inv_MaterialIndentMaster.Fk_VendorId = dbo.gen_Vendor.Pk_Vendor AND dbo.Inv_Material.VendorIdNumber = dbo.gen_Vendor.Pk_Vendor

                if (BillList.Count > 0)
                {
                    DataTable dt = new DataTable();

                    dt.Columns.Add("Name");
                    dt.Columns.Add("Fk_Material");
                    dt.Columns.Add("Pk_MaterialOrderMasterId");
                    dt.Columns.Add("MaterialIndentDate");
                    dt.Columns.Add("RequiredDate");
                    dt.Columns.Add("Quantity");
                    dt.Columns.Add("Pk_MaterialOrderDetailsId");
                    //dt.Columns.Add("VendorName");
                    dt.Columns.Add("MillName");
                    dt.Columns.Add("Fk_CustomerOrder");
                    dt.Columns.Add("CustomerName");
                    dt.Columns.Add("Cust_PO");

                    foreach (Vw_MIndent entity in BillList)
                    {
                        DataRow row = dt.NewRow();

                        row["Name"] = entity.Name;
                        row["Pk_MaterialOrderDetailsId"] = entity.Pk_MaterialOrderDetailsId;
                        row["Fk_Material"] = entity.Fk_Material;
                        row["Pk_MaterialOrderMasterId"] = entity.Pk_MaterialOrderMasterId;
                        row["MaterialIndentDate"] = entity.MaterialIndentDate;
                        row["RequiredDate"] = entity.RequiredDate;
                        row["Quantity"] = entity.Quantity;
                        //row["VendorName"] = entity.VendorName;
                        row["MillName"] = entity.MillName;
                        row["Fk_CustomerOrder"] = entity.Fk_CustomerOrder;
                        row["CustomerName"] = entity.CustomerName;
                        row["Cust_PO"] = entity.Cust_PO;

                        dt.Rows.Add(row);
                    }


                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.MIndent orp = new CogitoStreamline.Report.MIndent();

                    orp.Load("@\\Report\\MIndent.rpt");
                    orp.SetDataSource(dt.DefaultView);

                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "MIndent" + sInvno + ".pdf");
                    FileInfo file = new FileInfo(pdfPath);
                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();

                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }



        public ActionResult OpenStatus(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["MatID"]);

                // List<Vw_POReport> BillList = new List<Vw_POReport>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();


                Inv_MaterialIndentMaster oMatInd = _oEntities.Inv_MaterialIndentMaster.Where(p => p.Pk_MaterialOrderMasterId == sInvno).Single();
                //Purchase_Order oPOrder = new Purchase_Order();

                oMatInd.Fk_Status = 1;
                _oEntities.SaveChanges();

                return Json(new { success = true });
            }
            catch (Exception ex)
            {
                return null;

            }
        }

        public ActionResult StatusUpdate(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["MatID"]);

                // List<Vw_POReport> BillList = new List<Vw_POReport>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();


                Inv_MaterialIndentMaster oMatInd = _oEntities.Inv_MaterialIndentMaster.Where(p => p.Pk_MaterialOrderMasterId == sInvno).Single();
                //Purchase_Order oPOrder = new Purchase_Order();

                oMatInd.Fk_Status = 4;
                _oEntities.SaveChanges();

                return Json(new { success = true });
            }
            catch (Exception ex)
            {
                return null;

            }
        }

        public JsonResult POPendingList(string Pk_PONo = "", string MaterialName = "", string VendorName = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {

                int StrStatus = 1;
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_PONo", Pk_PONo));
                oSearchParams.Add(new SearchParameter("MaterialName", MaterialName));
                oSearchParams.Add(new SearchParameter("VendorName", VendorName));

                //oSearchParams.Add(new SearchParameter("Length", Length.ToString()));
                //oSearchParams.Add(new SearchParameter("Width", Width.ToString()));
                //oSearchParams.Add(new SearchParameter("GSM", GSM.ToString()));
                //oSearchParams.Add(new SearchParameter("BF", BF.ToString()));

                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oMatInd.SearchPO(oSearchParams);


                List<EntityObject> oVMaterialsToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_PO> oVMaterialObjects = oVMaterialsToDisplayObjects.Select(p => p).OfType<Vw_PO>().ToList();

                //Create a anominious object here to break the circular reference
                //var oVMaterialsToDisplay = oVMaterialObjects.Select(p => new

                var oVMaterialsToDisplay = oVMaterialObjects.Where(x => x.Fk_Status == StrStatus && x.Quantity - x.InwdQty > 0).Select(p => new
                {
                    Pk_PONo = p.Pk_PONo,
                    Pk_Material = p.Pk_Material,
                    Mat_Name = p.Name,
                    Quantity = p.Quantity,
                    Rate = p.Rate,
                    Amount = p.Amount,
                    VendorName = p.VendorName,
                    INQty = p.InwdQty,
                    PODate = p.PODate != null ? DateTime.Parse(p.PODate.ToString()).ToString("dd/MM/yyyy") : "",
                    PQuantity = (p.Quantity - p.InwdQty)

                }).ToList();

                return Json(new { Result = "OK", Records = oVMaterialsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = "Select PO No. & Proceed" });
            }
        }

    
    }
}
