﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;

namespace CogitoStreamline.Controllers
{
    public class ElectricalSparesController : CommonController
    {
        /*In controller has no Paper object, but it will use the Materials object Inv_Material table to store Information */

        Material oMat = new Material();

        public ElectricalSparesController()
        {
            oDoaminObject = new Material();

        }

        public override ActionResult Index()
        {
            ViewBag.Header = "Electrical Spares";

            return base.Index();
        }


        [HttpPost]
        public JsonResult getUnit(string pId = "")
        {
            List<gen_Unit> oListOfUnit = new Unit().Search(null).ListOfRecords.OfType<gen_Unit>().ToList();
            var oUnitToDisplay = oListOfUnit.Select(p => new
            {
                Name = p.UnitName,
                Id = p.Pk_Unit
            });

            return Json(oUnitToDisplay);
        }

        [HttpPost]
        public JsonResult SparesDuplicateChecking(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sName = oValues["Name"];

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Name", sName));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oPaperToDisplayObjects = oSearchResult.ListOfRecords;
                List<Inv_Material> oPaperObjects = oPaperToDisplayObjects.Select(p => p).OfType<Inv_Material>().ToList();

                var oPaperToDisplay = oPaperObjects.Select(p => new
                {
                    Pk_Material = p.Pk_Material,
                    Name = p.Name

                }).ToList().OrderBy(s => s.Name);

                return Json(new { Success = true, Records = oPaperToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }



        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            /* Pick Paper relevat fields from Material and create a structure */
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                Material oPaper = oDoaminObject as Material;
                oPaper.ID = decimal.Parse(pId);
                Inv_Material oAtualObject = oPaper.DAO as Inv_Material;
                //Create a anominious object here to break the circular reference
                var oPaperToDisplay = new
                {
                    Pk_Material = oAtualObject.Pk_Material,
                    Name = oAtualObject.Name,
                    Company = oAtualObject.Company,
                    Brand = oAtualObject.Brand,
                    MaterialType = oAtualObject.MaterialType,
                    Description = oAtualObject.Description,
                    


                };

                return Json(new { success = true, data = oPaperToDisplay });
            }
            else
            {
                var oPaperToDisplay = new Material();
                return Json(new { success = true, data = oPaperToDisplay });
            }
        }

        //[HttpPost]
        //public JsonResult PaperListByFiter(string Name = "", string Type = "", string Mill = "", string Fk_Color = "", string GSM = "", string BF = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        //{
        //    try
        //    {
        //        List<SearchParameter> oSearchParams = new List<SearchParameter>();

        //        oSearchParams.Add(new SearchParameter("Name", Name));
        //        oSearchParams.Add(new SearchParameter("PaperType", Type));
        //        oSearchParams.Add(new SearchParameter("GSM", GSM));
        //        oSearchParams.Add(new SearchParameter("BF", BF));
        //        oSearchParams.Add(new SearchParameter("Mill", Mill));
        //        oSearchParams.Add(new SearchParameter("Fk_Color", Fk_Color));
        //        oSearchParams.Add(new SearchParameter("Category", "Paper"));
        //        oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
        //        oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

        //        SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

        //        List<EntityObject> oPaperToDisplayObjects = oSearchResult.ListOfRecords;
        //        List<Inv_Material> oPaperObjects = oPaperToDisplayObjects.Select(p => p).OfType<Inv_Material>().ToList();

        //        //Create a anominious object here to break the circular reference
        //        var oRawMaterialsToDisplay = oPaperObjects.Select(p => new
        //        {
        //            Pk_Material = p.Pk_Material,
        //            Name = p.Name,
        //            Type = p.gen_PaperType.PaperTypeName,
        //            PaperColor = p.gen_Color.ColorName,
        //            Deckle = p.Deckle,
        //            Mill = p.gen_Mill.MillName,
        //            GSM = p.GSM,
        //            BF = p.BF,
        //            Unit = p.gen_Unit.UnitName,
        //            Description = p.Description

        //        }).ToList();

        //        return Json(new { Result = "OK", Records = oRawMaterialsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { Result = "ERROR", Message = ex.Message });
        //    }
        //}


        [HttpPost]
        public JsonResult SparesListByFiter(string Name = "", string Company = "", string Deckle = "", string Mill = "", string Fk_Color = "", string Brand = "", string BF = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Name", Name));
                oSearchParams.Add(new SearchParameter("Company", Company));
                oSearchParams.Add(new SearchParameter("Category", "Electric"));
                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oPaperToDisplayObjects = oSearchResult.ListOfRecords;
                List<Inv_Material> oPaperObjects = oPaperToDisplayObjects.Select(p => p).OfType<Inv_Material>().ToList();

                //Create a anominious object here to break the circular reference
                var oRawMaterialsToDisplay = oPaperObjects.Select(p => new
                {
                    Pk_Material = p.Pk_Material,
                    Name = p.Name,
                    Company = p.Company,
                    Brand = p.Brand,
                    MaterialType = p.MaterialType,
                    Description = p.Description,
                 
                }).ToList();

                return Json(new { Result = "OK", Records = oRawMaterialsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

  


   
        //[HttpPost]
        //public ActionResult MaterialDetails(string data = "")
        //{
        //    Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);


        //    string pId = oValues["fkMat"];
        //    if (pId != "")
        //    {
        //        var BoxID = int.Parse(pId);
        //        ModelManuplationResult oResult = new ModelManuplationResult();

        //        List<SearchParameter> oSearchParams = new List<SearchParameter>();
        //        oSearchParams.Add(new SearchParameter("fkMat", pId));

        //        SearchResult oSearchResult = oMat.SearchMat(oSearchParams);

        //        List<EntityObject> oEnquirysToDisplayObjects = oSearchResult.ListOfRecords;
        //        List<Inv_Material> oEnquiryObjects = oEnquirysToDisplayObjects.Select(p => p).OfType<Inv_Material>().ToList();

        //        var oEnquiryToDisplay = oEnquiryObjects.Select(p => new
        //        {

        //            Pk_Material = p.Pk_Material,
        //            Name = p.Name,
        //            Type = p.gen_PaperType.PaperTypeName,
        //            PaperColor = p.gen_Color.ColorName,
        //            Deckle = p.Deckle,
        //            Mill = p.gen_Mill.MillName,
        //            GSM = p.GSM,
        //            BF = p.BF,
        //            Unit = p.gen_Unit.UnitName,
        //            Description = p.Description

        //        });

        //        return Json(new { success = true, data = oEnquiryToDisplay });

        //    }
        //    return null;
        //}

    }
}