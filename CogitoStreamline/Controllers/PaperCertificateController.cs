﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;

using System.IO;
using Common.Logging.Configuration;
using System.Collections.Specialized;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Data;
using System.Drawing.Printing;

namespace CogitoStreamline.Controllers
{

    public class PaperCertificateController : CommonController
    {
        PaperCharacteristics oColor = new PaperCharacteristics();

        public PaperCertificateController()
        {
            oDoaminObject = new PaperCert();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Paper Quality Certificate";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            oColor.Dispose();
            base.Dispose(disposing);
        }


        [HttpPost]
        public JsonResult getChar(string pId = "")
        {
            List<PaperChar> oListOfBoxType = oColor.Search(null).ListOfRecords.OfType<PaperChar>().ToList();

            var oBoxToDisplay = oListOfBoxType.Select(p => new
            {
                Name = p.Name,
                Id = p.Pk_CharID

            });

            return Json(oBoxToDisplay);
        }
        [HttpPost]
        public override JsonResult Load(string data = "")
        {
           
          Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                PaperCert oPOrders = oDoaminObject as PaperCert;
               // PurchaseOrderMs oPOrders = oDoaminObject as PurchaseOrderM;
                oPOrders.ID = decimal.Parse(pId);
                PaperCertificate oAtualObject = oPOrders.DAO as PaperCertificate;

                int i = -1;

                var oMaterials = oAtualObject.PaperCertificateDetails.Select(p => new
                {
                    slno = ++i,
                    Pk_IdDet=p.Pk_IdDet,
      Fk_PkID=p.Fk_PkID,
      Fk_Characteristics=p.Fk_Characteristics,
      txtFMaterial=p.PaperChar.Name,
                    //TestMethod = p.TestMethod,
      Target=p.Target,
      Acceptance=p.Acceptance,
      Result1=p.Result1,
      Result2=p.Result2,
      Result3=p.Result3,
      Result4=p.Result4,
      Result5=p.Result5,
      Result6=p.Result6,
      MinVal=p.MinVal,
      MaxVal=p.MaxVal,
      AvgVal=p.AvgVal,
      Remarks=p.Remarks,
      TQty=p.TQty,
                   ReelNo=p.ReelNo,

                });

                var oPOrderDisplay = new
                {

                      Pk_Id= oAtualObject.Pk_Id,
                      Fk_Material= oAtualObject.Fk_Material,
                      MatName=oAtualObject.Inv_Material.Name,
                      BfVal=oAtualObject.Inv_Material.BF,
                      GSMval=oAtualObject.Inv_Material.GSM,
                      InvNo= oAtualObject.Invno,
                      InvDate= DateTime.Parse(oAtualObject.InvDate.ToString()).ToString("dd/MM/yyyy"), 
                      Quantity= oAtualObject.Quantity,
                      Tested= oAtualObject.Tested,
                      Approved= oAtualObject.Approved,
                      Fk_Vendor= oAtualObject.Fk_Vendor,

                    MaterialData = Json(oMaterials).Data
                };

                return Json(new { success = true, data = oPOrderDisplay });
            }
            else
            {
                var oReturnGoodFromSiteDisplay = new PaperCertificate();
                return Json(new { success = true, data = oReturnGoodFromSiteDisplay });
            }
        }
       




        [HttpPost]
        public JsonResult PCertListByFiter(string PoNoDisplay = "", string Name = "", string VName = "", string Pk_Id = "", string PONO = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
               List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_Id", Pk_Id));
                oSearchParams.Add(new SearchParameter("Name", Name));
                oSearchParams.Add(new SearchParameter("VName", VName));
                oSearchParams.Add(new SearchParameter("PoNoDisplay", PoNoDisplay));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oColorToDisplayObjects = oSearchResult.ListOfRecords;
                List<PaperCertificate> oColorObjects = oColorToDisplayObjects.Select(p => p).OfType<PaperCertificate>().ToList();

                //Create a anominious object here to break the circular reference
                var oColorToDisplay = oColorObjects.Select(p => new
                {
                    Pk_Id = p.Pk_Id,
                    Name = p.Inv_Material.Name,
            VendorName=p.gen_Vendor.VendorName,
            Invno=p.Invno,
            InvDate=    DateTime.Parse(p.InvDate.ToString()).ToString("dd/MM/yyyy"),
         Tested=p.Tested,
         Approved=p.Approved,
                    Fk_PoNo = p.Fk_PoNo,
                    PoNoDisplay=p.PurchaseOrderM.PkDisp,

                }).ToList().OrderByDescending(s => s.Pk_Id);

                return Json(new { Result = "OK", Records = oColorToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


        public ActionResult PCert(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

                int sInvno = 0;
                int PoNo = 0;

                if (oValues.ContainsKey("PoNO"))
                {
                    PoNo = Convert.ToInt32(oValues["PoNO"]);
                }

                if (oValues.ContainsKey("JCNO"))
                {
                  sInvno = Convert.ToInt32(oValues["JCNO"]);
                }

              


                List<Vw_PaperCertificate> BillList = new List<Vw_PaperCertificate>();


                List<Vw_PaperCertificate> BillList1 = new List<Vw_PaperCertificate>();

                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();


                BillList = dc.Vw_PaperCertificate.Where(x => x.Pk_Id == sInvno).Select(x => x).OfType<Vw_PaperCertificate>().ToList();
           //     BillList1 = dc.Vw_PaperCertificate.Where(x => x.f== sInvno).Select(x => x).OfType<Vw_PaperCertificate>().ToList();




                if (BillList.Count > 0)
                {
                    DataTable dt = new DataTable();

                    dt.Columns.Add("Pk_Id");
                    dt.Columns.Add("Invno");
                    dt.Columns.Add("InvDate");
                    dt.Columns.Add("Name");
                    dt.Columns.Add("TestMethod");
                    dt.Columns.Add("Target");
                    dt.Columns.Add("Acceptance");
                    dt.Columns.Add("Result1");
                    dt.Columns.Add("Result2");
                    dt.Columns.Add("Result3");
                    dt.Columns.Add("Result4");
                    dt.Columns.Add("Result5");
                    dt.Columns.Add("Result6");
                    dt.Columns.Add("MinVal");
                    dt.Columns.Add("MaxVal");
                    dt.Columns.Add("AvgVal");
                    dt.Columns.Add("Remarks");
                    dt.Columns.Add("Quantity");
                    dt.Columns.Add("Tested");
                    dt.Columns.Add("Approved");
                    dt.Columns.Add("VendorName");
                    dt.Columns.Add("UOM");
                    dt.Columns.Add("Pk_CharID");
                    dt.Columns.Add("Pk_IdDet");
                    dt.Columns.Add("Fk_Material");
                    dt.Columns.Add("CName");
                    dt.Columns.Add("ReelNo");


                    foreach (Vw_PaperCertificate entity in BillList)
                    {
                        DataRow row = dt.NewRow();

                        row["Pk_Id"] = entity.Pk_Id;
                        row["Invno"] = entity.Invno;
                        row["InvDate"] = entity.InvDate;
                        row["Name"] = entity.Name;
                        row["TestMethod"] = entity.TestMethod;
                        row["Target"] = entity.Target;
                        row["Acceptance"] = entity.Acceptance;
                        row["Result1"] = entity.Result1;
                        row["Result2"] = entity.Result2;
                        row["Result3"] = entity.Result3;
                        row["Result4"] = entity.Result4;
                        row["Result5"] = entity.Result5;
                        row["Result6"] = entity.Result6;
                        row["MinVal"] = entity.MinVal;
                        row["MaxVal"] = entity.MaxVal;
                        row["AvgVal"] = entity.AvgVal;
                        row["Remarks"] = entity.Remarks;
                        row["Quantity"] = entity.Quantity;
                        row["Tested"] = entity.Tested;
                        row["Approved"] = entity.Approved;
                        row["VendorName"] = entity.VendorName;
                        row["UOM"] = entity.UOM;
                        row["Pk_CharID"] = entity.Pk_CharID;
                        row["Pk_IdDet"] = entity.Pk_IdDet;
                        row["Fk_Material"] = entity.Fk_Material;
                        row["CName"] = entity.CName;
                        row["ReelNo"] = entity.ReelNo;
                        dt.Rows.Add(row);
                    }

                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.PaperCertificate orp = new CogitoStreamline.Report.PaperCertificate();

                    orp.Load("@\\Report\\PaperCertificate.rpt");
                    orp.SetDataSource(dt.DefaultView);


                    CrystalDecisions.CrystalReports.Engine.ReportDocument repDoc = orp;
                    

                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "PCertificate" + sInvno + ".pdf");
                    FileInfo file = new FileInfo(pdfPath);
                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();

                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }


        [HttpPost]

        public JsonResult SaveNew([System.Web.Http.FromBody]string data)
        {
            data = data.TrimStart('=');
            Dictionary<string, object> values = JsonConvert.DeserializeObject<Dictionary<string, object>>(data);
            data = JsonConvert.SerializeObject(values);
            return base.Save(data);


        }

        [HttpPost]
        public JsonResult ValDuplicate(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sName = oValues["ReelNo"];
                string sInvno = oValues["Invno"];
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Name", sName));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oBoardToDisplayObjects = oSearchResult.ListOfRecords;
                List<Inv_Material> oBoardObjects = oBoardToDisplayObjects.Select(p => p).OfType<Inv_Material>().ToList();

                var oBoardToDisplay = oBoardObjects.Select(p => new
                {
                    Pk_Material = p.Pk_Material,
                    Name = p.Name
                }).ToList().OrderBy(s => s.Name);

                return Json(new { Success = true, Records = oBoardToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }
    }

}
