﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;
using System.IO;
using System.Data;
using CrystalDecisions.ReportAppServer.ReportDefModel;
using System.Drawing.Printing;
using CrystalDecisions.Shared;
using WebGareCore.Controls;
using WebGareCore.CommonObjects.WorkFlow;



namespace CogitoStreamline.Controllers
{
    public class EnquiryController : CommonController
    {
        //
        // GET: /Enquiry/

        #region Variables
        public WgFileUpload oFileUpload = new WgFileUpload("documents", "uploads");
        CommunicationType oCommunication = new CommunicationType();
        Customer oCustomer = new Customer();
        Enquiry oGOrder = new Enquiry();
        #endregion Variables

        #region Constractor
        public EnquiryController()
        {
            oDoaminObject = new Enquiry();
        }
        #endregion Constractor

        #region Methods
        public override ActionResult Index()
        {
            ViewBag.Header = "Enquiry";
            return base.Index();
        }
        protected override void Dispose(bool disposing)
        {
            oCustomer.Dispose();
            oCommunication.Dispose();
            base.Dispose(disposing);
        }
        [HttpPost]
        public JsonResult FileUpload()
        {
            return Json(oFileUpload.SaveFile(Request.Files));
        }
        [HttpPost]
        public JsonResult getCustomers(string pId = "")
        {
            List<gen_Customer> oListOfCustomers = oCustomer.Search(null).ListOfRecords.OfType<gen_Customer>().ToList();
            var oCustomersToDisplay = oListOfCustomers.Select(p => new
            {
                Name = p.CustomerName,
                Id = p.Pk_Customer
            });

            return Json(oCustomersToDisplay);
        }



        [HttpPost]
        public JsonResult getCommunicationType(string pId = "")
        {
            List<gen_Communication> oListOfCommunicationTypes = oCommunication.Search(null).ListOfRecords.OfType<gen_Communication>().ToList();
            var oContriesToDisplay = oListOfCommunicationTypes.Select(p => new
            {
                Name = p.Type,
                Id = p.Pk_Communication
            });

            return Json(oContriesToDisplay);
        }
     
        
        
        
        
        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                Enquiry oEnquiry = oDoaminObject as Enquiry;
                oEnquiry.ID = decimal.Parse(pId);
                eq_Enquiry oAtualObject = oEnquiry.DAO as eq_Enquiry;

                int i = -1;

                //get Documents
                var oDocuments = oAtualObject.eq_Documents.Select(p => new
                {
                    FileName = p.FileName,
                    FileSize = p.FileSize
                });

                //var oDelivery = oAtualObject.gen_DeliverySchedule.Select(p => new
                //{
                //    Pk_DeliverySchedule = p.Pk_DeliverySechedule,
                //    slno = ++i,
                //    Quantity = p.Quantity,
                //    DeliveryDate = DateTime.Parse(p.DeliveryDate.ToString()).ToString("dd/MM/yyyy"),
                //    Comments = p.Comments,
                //    DeliveryCompleted = p.DeliveryCompleted,
                //    DeliveredDate = p.DeliveredDate != null ? DateTime.Parse(p.DeliveredDate.ToString()).ToString("dd/MM/yyyy") : null
                //});

                var oEnquiryChild = oAtualObject.eq_EnquiryChild.Select(p => new
                {
                    Pk_EnquiryChild = p.Pk_EnquiryChild,
                    slno = ++i,
                    Name = p.BoxMaster.Name,
                    Quantity = p.Quantity,
                    Fk_Enquiry = p.Fk_Enquiry,
                    Fk_BoxID = p.Fk_BoxID,
                    BType=p.BoxMaster.BoxType.Pk_BoxTypeID,
                 

                    Ddate=p.Ddate != null ? DateTime.Parse(p.Ddate.ToString()).ToString("dd/MM/yyyy") : "",
                    Amount = p.Amount
                    //ConvValue = p.ConvValue,
                    //GMarginPercentage = p.GMarginPercentage,
                    //GMarginValue = p.GMarginValue,
                    //TaxesPercntage = p.TaxesPercntage,
                    //TaxesValue = p.TaxesValue,
                    //TransportValue = p.TransportValue,
                    //WeightHValue = p.WeightHValue,
                    //HandlingChanrgesValue = p.HandlingChanrgesValue,
                    //PackingChargesValue = p.PackingChargesValue,
                    //RejectionPercentage = p.RejectionPercentage,
                    //RejectionValue = p.RejectionValue,
                    //TotalWeight = p.TotalWeight,
                    //TotalPrice = p.TotalPrice


                });

                //var oDelivery;
                //foreach (object oEqChild in oAtualObject.eq_EnquiryChild)
                //{
                //    oDelivery = oDelivery + ((eq_EnquiryChild)oEqChild).gen_DeliverySchedule.Select(p => new
                //    {
                //        Pk_DeliverySchedule = p.Pk_DeliverySechedule,
                //        slno = ++i,
                //        Quantity = p.Quantity,
                //        DeliveryDate = DateTime.Parse(p.DeliveryDate.ToString()).ToString("dd/MM/yyyy"),
                //        Comments = p.Comments,
                //        DeliveryCompleted = p.DeliveryCompleted,
                //        DeliveredDate = p.DeliveredDate != null ? DateTime.Parse(p.DeliveredDate.ToString()).ToString("dd/MM/yyyy") : null,
                //        Fk_EnquiryChild = p.Fk_EnquiryChild
                //    });

                //}

                    eq_EnquiryChild oActualEqChildObject = oAtualObject.eq_EnquiryChild.FirstOrDefault();
                    //var oDelivery = oActualEqChildObject.gen_DeliverySchedule.Select(p => new
                    //{
                    //    Pk_DeliverySchedule = p.Pk_DeliverySechedule,
                    //    slno = ++i,
                    //    Quantity = p.Quantity,
                    //    DeliveryDate = DateTime.Parse(p.DeliveryDate.ToString()).ToString("dd/MM/yyyy"),
                    //    Comments = p.Comments,
                    //    DeliveryCompleted = p.DeliveryCompleted,
                    //    DeliveredDate = p.DeliveredDate != null ? DateTime.Parse(p.DeliveredDate.ToString()).ToString("dd/MM/yyyy") : null,
                    //    Fk_EnquiryChild = p.Fk_EnquiryChild
                    //});



                //Create a anominious object here to break the circular reference
                var oEnquiryToDisplay = new
                {
                    Pk_Enquiry = oAtualObject.Pk_Enquiry,
                    CommunicationType = oAtualObject.CommunicationType,
                    Customer = oAtualObject.Customer,
                    CustomerName=oAtualObject.gen_Customer.CustomerName,
                    Description = oAtualObject.Description,
                    Date = DateTime.Parse(oAtualObject.Date.ToString()).ToString("dd/MM/yyyy"),
                    SpecialRequirements = oAtualObject.SpecialRequirements,
                    Fk_Status = oAtualObject.Fk_Status,
                    EqComments = oAtualObject.EqComments,
                    Fk_Status_Name = oAtualObject.wfState.State,
                    RefNo = oAtualObject.RefNo,
                    SampleRecd = oAtualObject.SampleRecd.Trim(),
                    //Length = oAtualObject.Length,
                    //GSM = oAtualObject.GSM,
                    //BF = oAtualObject.BF,
                    //NumberOfJoints = oAtualObject.NumberOfJoints,
                    //MoistureContent = oAtualObject.MoistureContent,
                    //NumberOfColors = oAtualObject.NumberOfColors,
                    //Ply = oAtualObject.Ply,
                    //Joints = oAtualObject.Joints,
                    //CapRequired = oAtualObject.CapRequired,
                    //LengthPartitionRequired = oAtualObject.LengthPartitionRequired ,
                    //WidthPartitionRequired = oAtualObject.WidthPartitionRequired,
                    //PrintingRequired = oAtualObject.PrintingRequired,
                    //PrintingDetails=oAtualObject.PrintingDetails,
                    //PlateRequired=oAtualObject.PlateRequired,
                    //Width = oAtualObject.Width,
                    //Height = oAtualObject.Height,
                    Documents = Json(oDocuments).Data,
                    //RepeatOrder=oAtualObject.RepeatOrder,
                    //ReferenceEnquiry=oAtualObject.ReferenceEnquiry,
                    //deliverySchedule = Json(oDelivery).Data,
                    enquiryChild = Json(oEnquiryChild).Data
                };

                return Json(new { success = true, data = oEnquiryToDisplay });
            }
            else
            {
                var oEnquiryToDisplay = new eq_Enquiry();
                return Json(new { success = true, data = oEnquiryToDisplay });
            }
        }
        [HttpPost]
        public JsonResult EnquiryListByFiter(string Pk_Enquiry = "", string EnquiryFromDate = "", string EnquiryToDate = "", string Customer = "", string CommunicationType = "", string State = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_Enquiry", Pk_Enquiry));
                oSearchParams.Add(new SearchParameter("EnquiryFromDate", EnquiryFromDate));
                oSearchParams.Add(new SearchParameter("EnquiryToDate", EnquiryToDate));
                oSearchParams.Add(new SearchParameter("Customer", Customer));
                oSearchParams.Add(new SearchParameter("CommunicationType", CommunicationType));
                oSearchParams.Add(new SearchParameter("State", State));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oEnquirysToDisplayObjects = oSearchResult.ListOfRecords;
                List<eq_Enquiry> oEnquiryObjects = oEnquirysToDisplayObjects.Select(p => p).OfType<eq_Enquiry>().ToList();

                //Create a anominious object here to break the circular reference
                var oEnquiryToDisplay = oEnquiryObjects.Select(p => new
                {
                    Pk_Enquiry = p.Pk_Enquiry,
                    CommunicationType = p.gen_Communication.Type,
                    Customer = p.gen_Customer.CustomerName,
                    Fk_Customer=p.Customer,
                    //Description = p.Description,
                    Date = DateTime.Parse(p.Date.ToString()).ToString("dd/MM/yyyy"),
                    //SpecialRequirements = p.SpecialRequirements,
                    Fk_Status = p.wfState.State,
                    //EqComments = p.EqComments

                }).ToList();

                return Json(new { Result = "OK", Records = oEnquiryToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [HttpPost]
        public ActionResult Upload(string data = "")
        {
            var file = Request.Files[0];

            if (file != null && file.ContentLength > 0)
            {
                var fileName = Path.GetFileName(file.FileName);
                var path = Path.Combine(Server.MapPath("~/EnquiryDocuments/"), fileName);
                file.SaveAs(path);
                Session["DocumentLink"] = path.ToString();
            }
            return RedirectToAction("Index");
        }
        //[HttpPost]
        //public JsonResult EnquiryDeliveryDetails(string Pk_Enquiry = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        //{
        //    try
        //    {
        //        Enquiry oEnquiry = oDoaminObject as Enquiry;
        //        oEnquiry.ID = decimal.Parse(Pk_Enquiry);
        //        eq_EnquiryChild oEnquiryObjects = oEnquiry.DAO as eq_EnquiryChild;

        //        var oDeliveryScheduleToDisplay = oEnquiryObjects.gen_DeliverySchedule.Select(p => new
        //        {
        //            Pk_DeliverySechedule = p.Pk_DeliverySechedule,
        //            DeliveryDate = DateTime.Parse(p.DeliveryDate.ToString()).ToString("dd/MM/yyyy"),
        //            Quantity = p.Quantity,
        //            DeliveredDate = p.DeliveredDate != null ? DateTime.Parse(p.DeliveredDate.ToString()).ToString("dd/MM/yyyy") : null,
        //            Comments = p.Comments

        //        }).ToList();

        //        return Json(new { Result = "OK", Records = oDeliveryScheduleToDisplay, TotalRecordCount = oDeliveryScheduleToDisplay.Count() });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { Result = "ERROR", Message = ex.Message });
        //    }
        //}

        [HttpPost]
        public JsonResult EnquiryChildDetails(string Pk_Enquiry = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                Enquiry oEnquiry = oDoaminObject as Enquiry;
                oEnquiry.ID = decimal.Parse(Pk_Enquiry);
                eq_Enquiry oEnquiryObjects = oEnquiry.DAO as eq_Enquiry;

                var oEnquiryChildToDisplay = oEnquiryObjects.eq_EnquiryChild.Select(p => new
                {
                    Pk_EnquiryChild = p.Pk_EnquiryChild,
                    //DeliveryDate = DateTime.Parse(p.DeliveryDate.ToString()).ToString("dd/MM/yyyy"),
                    Fk_BoxID = p.Fk_BoxID,
                    Name = p.BoxMaster.Name,
                    Quantity = p.Quantity,
                    Qty=p.ConvValue,
                    Ddate = p.Ddate != null ? DateTime.Parse(p.Ddate.ToString()).ToString("dd/MM/yyyy") : null,
                    //Rate = p.ConvRate

                }).ToList();

                return Json(new { Result = "OK", Records = oEnquiryChildToDisplay, TotalRecordCount = oEnquiryChildToDisplay.Count() });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult EnquiryBoxDetails(string Pk_Enquiry = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_Enquiry", Pk_Enquiry));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oGOrder.SearchDel(oSearchParams);

                List<EntityObject> oEnquirysToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_OrdQty> oEnquiryObjects = oEnquirysToDisplayObjects.Select(p => p).OfType<Vw_OrdQty>().ToList();

                var oEnquiryBoxToDisplay = oEnquiryObjects.Select(p => new
                {
                    Pk_EnquiryChild = p.Pk_EnquiryChild,
                    Fk_BoxID = p.Fk_BoxID,
                    Name = p.Name,
                 PName=p.PName,
                 PartQty=p.PartQty,
                 Pk_PartID=p.Pk_PartPropertyID,
                    BoxType = p.BType,
                    Quantity=p.Quantity
                }).ToList();



                return Json(new { Result = "OK", Records = oEnquiryBoxToDisplay, TotalRecordCount = oEnquiryBoxToDisplay.Count() });
            }
            catch (Exception ex)
            {
                //return Json(new { Result = "ERROR", Message = ex.Message });
                return Json(new { Result = "ERROR", Message = "Select Enquiry No. & Proceed" });
            }
        }
        [HttpPost]
        public ActionResult CheckEstimate(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            decimal enqChild = 0; decimal? temp = 0;

            var oHeadToDisplay = new
            {
                Estimated = false,
                EstBoxChild = enqChild,
                BoxEstimation = temp,
                EnqChild = enqChild
            };

            string pId = oValues["Pk_EnquiryChild"];
            if (pId != "")
            {

                Enquiry oQuote = oDoaminObject as Enquiry;
                //  oQuote.ID = decimal.Parse(pId);
                eq_EnquiryChild oAtualObject = oQuote.DAO as eq_EnquiryChild;
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

             

                List<eq_EnquiryChild> EstList = new List<eq_EnquiryChild>();
                int Estid = int.Parse(pId);
                ModelManuplationResult oResult = new ModelManuplationResult();
                EstList = dc.eq_EnquiryChild.Where(x => x.Pk_EnquiryChild == Estid).Select(x => x).OfType<eq_EnquiryChild>().ToList();
                if (EstList.Count > 0)
                {

                    if (EstList[0].Estimated.HasValue)
                    {

                        //var oHeadToDisplay;

                        oHeadToDisplay = new
                  {
                       //BoxEst = 0,
                       // BoxEstChild = 0 ,
                       //  EnqChild = 0


                      Estimated = EstList[0].Estimated.Value,

                      EstBoxChild = EstList[0].est_BoxEstimationChild.FirstOrDefault().Pk_BoxEstimationChild,

                      BoxEstimation = EstList[0].est_BoxEstimationChild.FirstOrDefault().Fk_BoxEstimation,
                      EnqChild = EstList[0].Pk_EnquiryChild,

                      //EnqChild = EstList[0].Pk_EnquiryChild.

                  };
                        return Json(new { success = true, data = oHeadToDisplay });


                    }
                }
               
            }
            return Json(new { success = true, data = oHeadToDisplay }); ;
        }

        [HttpPost]
        public JsonResult EstDetails(string Pk_Enquiry = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                Enquiry oBoxMaster = oDoaminObject as Enquiry;

                oBoxMaster.ID = decimal.Parse(Pk_Enquiry);

                eq_Enquiry oEnquiryObjects = oBoxMaster.DAO as eq_Enquiry;

                var oDeliveryScheduleToDisplay = oEnquiryObjects.est_Estimation.Select(p => new
                {
                    //Fk_DetailedEstimation=p.Fk_DetailedEstimation,
                    //Length=p.Length,
                    //Width=p.Width,
                    //Height=p.Height,
                    //BoardArea=p.BoardArea,
                    //Deckle=p.Deckle

                    Pk_Estimate = p.Pk_Estimate


                });

                return Json(new { success = true, data = oDeliveryScheduleToDisplay });
            }

            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }   
     
        public ActionResult EnqRep(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["Pk_EQID"]);

                List<Vw_EnqReport> BillList = new List<Vw_EnqReport>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                //BillList = dc.VwInvoices.ToList();
                BillList = dc.Vw_EnqReport.Where(x => x.Pk_Enquiry == sInvno).Select(x => x).OfType<Vw_EnqReport>().ToList();


//            SELECT     dbo.eq_Enquiry.Pk_Enquiry, dbo.eq_Enquiry.Date, dbo.gen_Customer.CustomerName, dbo.BoxMaster.Name,
                //dbo.eq_EnquiryChild.Quantity, dbo.BoxMaster.Pk_BoxID
//FROM         dbo.eq_Enquiry INNER JOIN
//                      dbo.eq_EnquiryChild ON dbo.eq_Enquiry.Pk_Enquiry = dbo.eq_EnquiryChild.Fk_Enquiry INNER JOIN
//                      dbo.gen_Customer ON dbo.eq_Enquiry.Customer = dbo.gen_Customer.Pk_Customer INNER JOIN
//                      dbo.BoxMaster ON dbo.eq_EnquiryChild.Fk_BoxID = dbo.BoxMaster.Pk_BoxID



                if (BillList.Count > 0)
                {
                    DataTable dt = new DataTable();

                    dt.Columns.Add("Name");
                    dt.Columns.Add("CustomerName");
                    dt.Columns.Add("Pk_Enquiry");
                    dt.Columns.Add("Date");
                    dt.Columns.Add("Quantity");
                    dt.Columns.Add("Pk_BoxID");
                    dt.Columns.Add("Ddate");


                    foreach (Vw_EnqReport entity in BillList)
                    {
                        DataRow row = dt.NewRow();

                        row["Name"] = entity.Name;
                        row["CustomerName"] = entity.CustomerName;
                        row["Pk_Enquiry"] = entity.Pk_Enquiry;
                        row["Pk_BoxID"] = entity.Pk_BoxID;
                        row["Date"] = entity.Date;
                        row["Quantity"] = entity.Quantity;
                        row["Ddate"] = entity.Ddate;

                        dt.Rows.Add(row);
                    }


                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.Enquiry orp = new CogitoStreamline.Report.Enquiry();

                    orp.Load("@\\Report\\Enquiry.rpt");
                    orp.SetDataSource(dt.DefaultView);

                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "Enquiry" + sInvno + ".pdf");
                    FileInfo file = new FileInfo(pdfPath);
                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();

                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }
    
        
        
        #endregion Methods


        [HttpPost]

        public JsonResult SaveNew([System.Web.Http.FromBody]string data)
        {
            data = data.TrimStart('=');
            Dictionary<string, object> values = JsonConvert.DeserializeObject<Dictionary<string, object>>(data);
            data = JsonConvert.SerializeObject(values);
            return base.Save(data);


        }
    }
}
