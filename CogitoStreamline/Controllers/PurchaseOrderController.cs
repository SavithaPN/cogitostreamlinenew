﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;
using System.IO;
using System.Data;
using CrystalDecisions.ReportAppServer.ReportDefModel;
using System.Drawing.Printing;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

namespace CogitoStreamline.Controllers
{

    public class PurchaseOrderController : CommonController
    {
        #region Variables
        Purchase_Order oPOrder = new Purchase_Order();
        //        Quality oQC = new Quality();

        readonly CogitoStreamLineEntities _oEntities = new CogitoStreamLineEntities();
        #endregion Variables

        #region Constractor
        public PurchaseOrderController()
        {
            oDoaminObject = new Purchase_Order();
        }
        #endregion Constractor

        #region Methods
        public override ActionResult Index()
        {
            ViewBag.Header = "Purchase Order";
            return base.Index();
        }
        protected override void Dispose(bool disposing)
        {
            oPOrder.Dispose();
            base.Dispose(disposing);
        }
        [HttpPost]
        public override JsonResult Load(string data = "")
        {

            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                Purchase_Order oPOrders = oDoaminObject as Purchase_Order;
               // PurchaseOrderMs oPOrders = oDoaminObject as PurchaseOrderM;
                oPOrders.ID = decimal.Parse(pId);
                PurchaseOrderM oAtualObject = oPOrders.DAO as PurchaseOrderM;

                int i = -1;

                var oMaterials = oAtualObject.PurchaseOrderD.Select(p => new
                {
                    slno = ++i,
                    Quantity = p.Quantity,
                    Fk_Material = p.Fk_Material,
                    Name = p.Inv_Material.Name,
                    Pk_PODet = p.Pk_PODet,
                    Rate=p.Rate,
                    Amount=p.Amount,
                     HSNCode=p.HSNCode,
                });

                var oPOrderDisplay = new
                {
                    Pk_PONo = oAtualObject.Pk_PONo,
                    ED=oAtualObject.ED,
                    PODate = DateTime.Parse(oAtualObject.PODate.ToString()).ToString("dd/MM/yyyy"),
                    GrandTotal=oAtualObject.GrandTotal,
                    NETVALUE = oAtualObject.NetValue,
                    Fk_Vendor = oAtualObject.Fk_Vendor,
                    Fk_Indent = oAtualObject.Fk_Indent,
                    TaxType=oAtualObject.TaxType,
                    Fk_Status=oAtualObject.Fk_Status,
                    Comments=oAtualObject.Comments,
                    MaterialData = Json(oMaterials).Data
                };

                return Json(new { success = true, data = oPOrderDisplay });
            }
            else
            {
                var oReturnGoodFromSiteDisplay = new PurchaseOrderM();
                return Json(new { success = true, data = oReturnGoodFromSiteDisplay });
            }
        }
        public JsonResult getMaterialDetails( string Pk_MaterialID = "", string MaterialName = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {

            try
            {
                vw_StockMaterial oIssueDetails = new vw_StockMaterial();
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
              
                oSearchParams.Add(new SearchParameter("Pk_MaterialID", Pk_MaterialID));
                oSearchParams.Add(new SearchParameter("MaterialName", MaterialName));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oPOrder.SearchIssueDetails(oSearchParams);

                List<EntityObject> oIssueDetailsToDisplayObjects = oSearchResult.ListOfRecords;
                List<vw_StockMaterial> oIssueObjects = oIssueDetailsToDisplayObjects.Select(p => p).OfType<vw_StockMaterial>().ToList();

                //Create a anominious object here to break the circular reference
                var oIssueDetailsToDisplay = oIssueObjects.Where(p => (p.Quantity) <= 0).Select(p => new
                {
                    Pk_Material = p.Pk_Material,
                    MaterialName = p.Name,
                    Quantity = p.Quantity 
                    

                }).ToList();

                return Json(new { Result = "OK", Records = oIssueDetailsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        public JsonResult getMaterialIndentDetails(string Indent = "", string Pk_MaterialID = "", string MaterialName = "", string GSM = "", string BF = "", string Dec = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {

            try
            {
                vw_StockMaterial oIssueDetails = new vw_StockMaterial();
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("IndentNo", Indent));
                oSearchParams.Add(new SearchParameter("Pk_MaterialID", Pk_MaterialID));
                oSearchParams.Add(new SearchParameter("MaterialName", MaterialName));
                oSearchParams.Add(new SearchParameter("GSM", GSM));
                oSearchParams.Add(new SearchParameter("BF", BF));
                oSearchParams.Add(new SearchParameter("Dec", Dec));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oPOrder.SearchIndentDetails(oSearchParams);

                List<EntityObject> oIssueDetailsToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_MIndent> oIssueObjects = oIssueDetailsToDisplayObjects.Select(p => p).OfType<Vw_MIndent>().ToList();

                //Create a anominious object here to break the circular reference
                var oIssueDetailsToDisplay = oIssueObjects.Select(p => new
                {
                    Pk_Material = p.Pk_Material,
                    MaterialName = p.Name,
                    Quantity = p.Quantity,
                   MatIndent= p.Pk_MaterialOrderMasterId,
                   Color=p.ColorName,
                   Mill=p.MillName,
                   GSM=p.GSM,
                   BF=p.BF,
                   Deckle=p.Deckle,

                }).ToList();

                return Json(new { Result = "OK", Records = oIssueDetailsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        public JsonResult PurchaseOrderListByFiter(string FkIndent="", string Pk_PONo = "", string PODate = null, string TxtFromDate = "", string TxtToDate = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_PONo", Pk_PONo));
                oSearchParams.Add(new SearchParameter("PODate", PODate));
                oSearchParams.Add(new SearchParameter("FkIndent", FkIndent));
                oSearchParams.Add(new SearchParameter("TxtFromDate", TxtFromDate));
                oSearchParams.Add(new SearchParameter("TxtToDate", TxtToDate));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oIssueReturnToDisplayObjects = oSearchResult.ListOfRecords;
                List<PurchaseOrderM> oIssueReturnObjects = oIssueReturnToDisplayObjects.Select(p => p).OfType<PurchaseOrderM>().ToList();

                var oIssureReturnToDisplay = oIssueReturnObjects.Where(p=>p.Fk_Status==1).Select(p => new
                {
                    Pk_PONo = p.Pk_PONo,
                    Pk_Disp=p.PkDisp,
                    Status = p.wfStates.State,
                    PODate = DateTime.Parse(p.PODate.ToString()).ToString("dd/MM/yyyy"),
                    Fk_Vendor = p.gen_Vendor.VendorName,
                    Fk_Indent=p.Fk_Indent,
                    Comments = p.Comments,

                }).ToList();

                return Json(new { Result = "OK", Records = oIssureReturnToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        public JsonResult PurchaseOrderList(string PkDisp = "", string PODate = null, string TxtFromDate = "", string TxtToDate = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("PkDisp", PkDisp));
                oSearchParams.Add(new SearchParameter("PODate", PODate));
                oSearchParams.Add(new SearchParameter("TxtFromDate", TxtFromDate));
                oSearchParams.Add(new SearchParameter("TxtToDate", TxtToDate));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oPOrder.SearchPOCert(oSearchParams);

                List<EntityObject> oIssueReturnToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_PODet> oIssueReturnObjects = oIssueReturnToDisplayObjects.Select(p => p).OfType<Vw_PODet>().ToList();

                var oIssureReturnToDisplay = oIssueReturnObjects.Select(p => new
                {
                    Pk_PONo = p.Pk_PONo,
                    PkDisp = p.PkDisp,
                    //Status = p.wfStates.State,
                    PODate = DateTime.Parse(p.PODate.ToString()).ToString("dd/MM/yyyy"),
                    Fk_Vendor = p.VendorName,
                    Fk_Indent = p.Fk_Indent,
                    Fk_PoNo=p.Pk_PONo,
                    Fk_Id = p.Pk_Id,
                    Name=p.Name,

                }).ToList();

                return Json(new { Result = "OK", Records = oIssureReturnToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }



        public JsonResult PurchaseOrderOthersList(string PkDisp = "", string MaterialName = "", string PODate = null, string TxtFromDate = "", string TxtToDate = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("PkDisp", PkDisp));
                oSearchParams.Add(new SearchParameter("MaterialName", MaterialName));
                
                oSearchParams.Add(new SearchParameter("PODate", PODate));
                oSearchParams.Add(new SearchParameter("TxtFromDate", TxtFromDate));
                oSearchParams.Add(new SearchParameter("TxtToDate", TxtToDate));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oPOrder.SearchPOConsumables(oSearchParams);

                List<EntityObject> oIssueReturnToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_PoDetOthers> oIssueReturnObjects = oIssueReturnToDisplayObjects.Select(p => p).OfType<Vw_PoDetOthers>().ToList();

                var oIssureReturnToDisplay = oIssueReturnObjects.Select(p => new
                {
                    Pk_PONo = p.Pk_PONo,
                    PkDisp=p.PkDisp,
                    //Status = p.wfStates.State,
                    PODate = DateTime.Parse(p.PODate.ToString()).ToString("dd/MM/yyyy"),
                    Fk_Vendor = p.VendorName,
                    Fk_Indent = p.Fk_Indent,
                    Pk_PODet=p.Pk_PODet,
                    Fk_PoNo = p.Pk_PONo,
                    Desc = p.Description,
                    Name = p.Name,
                    CatName = p.MatCatName,
                    //Pk_PODet = p.Pk_PODet,
                    UnitName = p.UnitName,
                }).ToList();

                return Json(new { Result = "OK", Records = oIssureReturnToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
      
        #endregion Methods

        public ActionResult POrderRep(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["Pk_PONO"]);

                List<Vw_POReport> BillList = new List<Vw_POReport>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                //BillList = dc.VwInvoices.ToList();
                BillList = dc.Vw_POReport.Where(x => x.Pk_PONo == sInvno).Select(x => x).OfType<Vw_POReport>().ToList();

                if (BillList.Count > 0)
                {
                    DataTable dt = new DataTable();

                    dt.Columns.Add("Name");
                    dt.Columns.Add("Quantity");
                    dt.Columns.Add("Pk_PONo");
                    dt.Columns.Add("PODate");
                    dt.Columns.Add("Rate");
                    dt.Columns.Add("Amount");
                    dt.Columns.Add("VendorName");
                    dt.Columns.Add("GrandTotal");
                    dt.Columns.Add("NetValue");
                    dt.Columns.Add("TaxType");
                    dt.Columns.Add("Fk_Indent");
                    dt.Columns.Add("Pk_Material");
                    dt.Columns.Add("MillName");
                    dt.Columns.Add("ColorName");
                    dt.Columns.Add("Fk_IndentVal");
                    dt.Columns.Add("Email");
                    dt.Columns.Add("Address_No_Street");
                    dt.Columns.Add("PinCode");
                    dt.Columns.Add("LandLine");
                    dt.Columns.Add("MobileNumber");
                    dt.Columns.Add("PkDisp");
                    dt.Columns.Add("OfficeContactNumber");
                    dt.Columns.Add("HSNCode");
                    dt.Columns.Add("Two");
                    dt.Columns.Add("Four");
                    dt.Columns.Add("Five");
                    dt.Columns.Add("Six");
                    dt.Columns.Add("Seven");
                    dt.Columns.Add("UnitName");
                    dt.Columns.Add("CGST");
                    dt.Columns.Add("SGST");
                    dt.Columns.Add("Comments");
                    foreach (Vw_POReport entity in BillList)
                    {
                        DataRow row = dt.NewRow();

                        row["Name"] = entity.Name;
                        row["Quantity"] = entity.Quantity;
                        row["Pk_PONo"] = entity.Pk_PONo;
                        row["PODate"] = entity.PODate;
                        row["Rate"] = entity.Rate;
                        row["Amount"] = entity.Amount;
                        row["VendorName"] = entity.VendorName;
                        row["GrandTotal"] = entity.GrandTotal;
                        row["NetValue"] = entity.NetValue;
                        row["TaxType"] = entity.TaxType;
                        row["Fk_Indent"] = entity.Fk_Indent;
                        row["Pk_Material"] = entity.Pk_Material;
                        row["MillName"] = entity.MillName;
                        row["ColorName"] = entity.ColorName;
                        row["Fk_IndentVal"] = entity.Fk_IndentVal;
                        row["Email"] = entity.Email;
                        row["Address_No_Street"] = entity.Address_No_Street;
                        row["PinCode"] = entity.PinCode;
                        row["LandLine"] = entity.LandLine;
                        row["MobileNumber"] = entity.MobileNumber;
                        row["PkDisp"] = entity.PkDisp;                     
                        row["OfficeContactNumber"] = entity.OfficeContactNumber;
                        row["HSNCode"] = entity.HSNCode;
                        row["Two"] = entity.Two;
                        row["Four"] = entity.Four;
                        row["Five"] = entity.Five;
                        row["Six"] = entity.Six;
                        row["Seven"] = entity.Seven;
                        row["Comments"] = entity.Comments;

                        dt.Rows.Add(row);
                    }


                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.POrder orp = new CogitoStreamline.Report.POrder();

                    orp.Load("@\\Report\\POrder.rpt");
                    orp.SetDataSource(dt.DefaultView);

                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "POrder" + sInvno + ".pdf");
                    FileInfo file = new FileInfo(pdfPath);
                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();

                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }

        public ActionResult POrderRepConsumable(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["Pk_PONO"]);

                List<Vw_POReport> BillList = new List<Vw_POReport>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                //BillList = dc.VwInvoices.ToList();
                BillList = dc.Vw_POReport.Where(x => x.Pk_PONo == sInvno).Select(x => x).OfType<Vw_POReport>().ToList();

                if (BillList.Count > 0)
                {
                    DataTable dt = new DataTable();

                    dt.Columns.Add("Name");
                    dt.Columns.Add("Quantity");
                    dt.Columns.Add("Pk_PONo");
                    dt.Columns.Add("PODate");
                    dt.Columns.Add("Rate");
                    dt.Columns.Add("Amount");
                    dt.Columns.Add("VendorName");
                    dt.Columns.Add("GrandTotal");
                    dt.Columns.Add("NetValue");
                    dt.Columns.Add("TaxType");
                    dt.Columns.Add("Fk_Indent");
                    dt.Columns.Add("Pk_Material");
                    dt.Columns.Add("MillName");
                    dt.Columns.Add("ColorName");
                    dt.Columns.Add("Fk_IndentVal");
                    dt.Columns.Add("Email");
                    dt.Columns.Add("Address_No_Street");
                    dt.Columns.Add("PinCode");
                    dt.Columns.Add("LandLine");
                    dt.Columns.Add("MobileNumber");
                    dt.Columns.Add("PkDisp");
                    dt.Columns.Add("OfficeContactNumber");
                    dt.Columns.Add("HSNCode");
                    foreach (Vw_POReport entity in BillList)
                    {
                        DataRow row = dt.NewRow();

                        row["Name"] = entity.Name;
                        row["Quantity"] = entity.Quantity;
                        row["Pk_PONo"] = entity.Pk_PONo;
                        row["PODate"] = entity.PODate;
                        row["Rate"] = entity.Rate;
                        row["Amount"] = entity.Amount;
                        row["VendorName"] = entity.VendorName;
                        row["GrandTotal"] = entity.GrandTotal;
                        row["NetValue"] = entity.NetValue;
                        row["TaxType"] = entity.TaxType;
                        row["Fk_Indent"] = entity.Fk_Indent;
                        row["Pk_Material"] = entity.Pk_Material;
                        row["MillName"] = entity.MillName;
                        row["ColorName"] = entity.ColorName;
                        row["Fk_IndentVal"] = entity.Fk_IndentVal;
                        row["Email"] = entity.Email;
                        row["Address_No_Street"] = entity.Address_No_Street;
                        row["PinCode"] = entity.PinCode;
                        row["LandLine"] = entity.LandLine;
                        row["MobileNumber"] = entity.MobileNumber;
                        row["PkDisp"] = entity.PkDisp;
                        row["OfficeContactNumber"] = entity.OfficeContactNumber;
                        row["HSNCode"] = entity.HSNCode;

                        
                        dt.Rows.Add(row);
                    }


                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.POrder_Consumables orp = new CogitoStreamline.Report.POrder_Consumables();


                    //ParameterFieldDefinitions crParameterFieldDefinitions4;
                    //ParameterFieldDefinition crParameterFieldDefinition4;
                    //ParameterValues crParameterValues4 = new ParameterValues();
                    //ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                    //crParameterDiscreteValue4.Value = "ExtraCopy";
                    //crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                    //crParameterFieldDefinition4 = crParameterFieldDefinitions4["CopyName"];
                    //crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                    //crParameterValues4.Clear();
                    //crParameterValues4.Add(crParameterDiscreteValue4);
                    //crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);


                    orp.Load("@\\Report\\POrder_Consumables.rpt");
                    orp.SetDataSource(dt.DefaultView);

                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "POrder_Consumables" + sInvno + ".pdf");
                    FileInfo file = new FileInfo(pdfPath);
                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();

                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }

        public ActionResult StatusUpdate(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["POno"]);

               // List<Vw_POReport> BillList = new List<Vw_POReport>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

           
                PurchaseOrderM oPurM = _oEntities.PurchaseOrderM.Where(p => p.Pk_PONo == sInvno).Single();
                //Purchase_Order oPOrder = new Purchase_Order();

                oPurM.Fk_Status = 4;
                _oEntities.SaveChanges();

                return Json(new { success = true });
            }
            catch (Exception ex)
            {
                return null;

            }
        }

        public ActionResult OpenStatus(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["POno"]);

                // List<Vw_POReport> BillList = new List<Vw_POReport>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();


                PurchaseOrderM oPurM = _oEntities.PurchaseOrderM.Where(p => p.Pk_PONo == sInvno).Single();
                //Purchase_Order oPOrder = new Purchase_Order();

                oPurM.Fk_Status = 1;
                _oEntities.SaveChanges();

                return Json(new { success = true });
            }
            catch (Exception ex)
            {
                return null;

            }
        }


        [HttpPost]
        public JsonResult POGetRec(string Pk_PONo = "", string Vendor = "", string GSM = "", string BF = "", string Dec = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {


            try
            {

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_PONo", Pk_PONo));
                oSearchParams.Add(new SearchParameter("GSM", GSM));
                oSearchParams.Add(new SearchParameter("BF", BF));
                oSearchParams.Add(new SearchParameter("Dec", Dec));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oPOrder.SearchPODet(oSearchParams);

                List<EntityObject> oColorToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_PoGetRec> oColorObjects = oColorToDisplayObjects.Select(p => p).OfType<Vw_PoGetRec>().ToList();

                 //RollNo = p.RollNo !=null ? p.RollNo : "" ,
                var oColorToDisplay = oColorObjects.Select(p => new
                {
                   Pk_Material= p.Pk_Material,
                    MaterialName=p.Name,
                    Quantity=p.Quantity,
                    Color=p.ColorName !=null ? p.ColorName : "" ,
                   Mill = p.MillName != null ? p.MillName : "",
                    Fk_IndentVal = p.Fk_IndentVal,
                   Pk_Mill = p.Pk_Mill != null ? p.Pk_Mill : 0,
                  CatName= p.CatName,
                   UnitName=p.UnitName,
                   Pk_PoDet=p.Pk_PODet,
                }).ToList();

                return Json(new { Result = "OK", Records = oColorToDisplay, TotalRecordCount = oSearchResult.RecordCount });

            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }


        }
    
        
        
        [HttpPost]
        public JsonResult OpenPOGetRec(string Pk_PONo = "", string Vendor = "", string GSM = "", string BF = "", string Dec = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {


            try
            {

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_PONo", Pk_PONo));
                oSearchParams.Add(new SearchParameter("GSM", GSM));
                oSearchParams.Add(new SearchParameter("BF", BF));
                oSearchParams.Add(new SearchParameter("Dec", Dec));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oPOrder.SearchOpenPODet(oSearchParams);

                List<EntityObject> oColorToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_OthersMaterial> oColorObjects = oColorToDisplayObjects.Select(p => p).OfType<Vw_OthersMaterial>().ToList();

                //RollNo = p.RollNo !=null ? p.RollNo : "" ,
                var oColorToDisplay = oColorObjects.Select(p => new
                {
                    Pk_Material = p.Pk_Material,
                    MaterialName = p.Name,
                  //  Quantity = p.Quantity,
                    Color = p.ColorName != null ? p.ColorName : "",
                  //  Mill = p.MillName != null ? p.MillName : "",
                 //   Fk_IndentVal = p.Fk_IndentVal,
                   // Pk_Mill = p.Pk_Mill != null ? p.Pk_Mill : 0,
                    CatName = p.CatName,
                    UnitName = p.UnitName,
                   // Pk_PoDet = p.Pk_PODet,
                }).ToList();

                return Json(new { Result = "OK", Records = oColorToDisplay, TotalRecordCount = oSearchResult.RecordCount });

            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }


        }

        public JsonResult POCertList(string Pk_PONo = "", string GSM = "", string PoNoDisp="", string BF = "", string MaterialName = "", string VendorName = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("PoNoDisp", PoNoDisp));
                oSearchParams.Add(new SearchParameter("MaterialName", MaterialName));
                oSearchParams.Add(new SearchParameter("VendorName", VendorName));
                //oSearchParams.Add(new SearchParameter("Length", Length.ToString()));
                //oSearchParams.Add(new SearchParameter("Width", Width.ToString()));
                oSearchParams.Add(new SearchParameter("GSM", GSM));
                oSearchParams.Add(new SearchParameter("BF", BF));

                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oPOrder.SearchPO(oSearchParams);


                List<EntityObject> oVMaterialsToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_POCertificate> oVMaterialObjects = oVMaterialsToDisplayObjects.Select(p => p).OfType<Vw_POCertificate>().ToList();

                //Create a anominious object here to break the circular reference
                //var oVMaterialsToDisplay = oVMaterialObjects.Select(p => new

                var oVMaterialsToDisplay = oVMaterialObjects.Where(p => p.SQty < p.Quantity).Select(p => new
                {
                    Pk_PONo = p.Pk_PONo,
                    Pk_Material = p.Fk_Material,
                    Mat_Name = p.Name,
                    Quantity = p.Quantity,
                    //Rate = p.Rate,
                    //Amount = p.Amount,
                    Pk_Vendor = p.Pk_Vendor,
                    VendorName = p.VendorName,
                    Color=p.ColorName,
                    GSM = p.GSM,
                    BF = p.BF,
                    PoDet=p.Pk_PODet,
                    PoNoDisp=p.PkDisp,


                }).ToList().OrderByDescending(x=>x.Pk_PONo);

                return Json(new { Result = "OK", Records = oVMaterialsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = "Select PO No. & Proceed" });
            }
        }

        [HttpPost]

        public JsonResult SaveNew([System.Web.Http.FromBody]string data)
        {
            data = data.TrimStart('=');
            Dictionary<string, object> values = JsonConvert.DeserializeObject<Dictionary<string, object>>(data);
            data = JsonConvert.SerializeObject(values);
            return base.Save(data);


        }

    }
}


