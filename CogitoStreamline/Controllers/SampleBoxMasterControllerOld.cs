﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;
using System.IO;
using System.Data;
using CrystalDecisions.ReportAppServer.ReportDefModel;
using System.Drawing.Printing;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;


namespace CogitoStreamLine.Controllers
{
    public class SampleBoxMasterController : CommonController
    {
        Samples_BoxMasterOld oBoxMaster = new Samples_BoxMasterOld();
        BoxTyp oBoxT = new BoxTyp();
        Samples_BoxMasterOld oItProp = new Samples_BoxMasterOld();
        Machine oMAC = new Machine();

        FType oFType = new FType();
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();


        public SampleBoxMasterController()
        {
            oDoaminObject = new Samples_BoxMasterOld();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Sample Box Master";
            return base.Index();
        }
        protected override void Dispose(bool disposing)
        {
            oBoxMaster.Dispose();
            base.Dispose(disposing);
        }


        [HttpPost]
        public JsonResult getBoxType(string pId = "")
        {
            List<BoxType> oListOfBoxType = oBoxT.Search(null).ListOfRecords.OfType<BoxType>().ToList();

            var oBoxToDisplay = oListOfBoxType.Select(p => new
            {
                Name = p.Name,
                Id = p.Pk_BoxTypeID

            });

            return Json(oBoxToDisplay);
        }


        [HttpPost]
        public JsonResult getFluteType(string pId = "")
        {
            List<FluteType> oListOfBoxType = oFType.Search(null).ListOfRecords.OfType<FluteType>().ToList();

            var oBoxToDisplay = oListOfBoxType.Select(p => new
            {
                Name = p.FluteName,
                Id = p.Pk_FluteID

            });

            return Json(oBoxToDisplay);
        }
        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                Samples_BoxMasterOld oBox = oDoaminObject as Samples_BoxMasterOld;
                oBox.ID = decimal.Parse(pId);
                SampleBoxMaster oAtualObject = oBox.DAO as SampleBoxMaster;


                int i = -1;

                //Create a anominious object here to break the circular reference
                var oHeadToDisplay = new
                {
                    Pk_BoxID = oAtualObject.Pk_BoxID,
                    Name = oAtualObject.Name,
                    Description = oAtualObject.Description,
                    Customer = oAtualObject.gen_Customer.CustomerName,
                    PartNo = oAtualObject.PartNo,
                    Fk_BoxType = oAtualObject.Fk_BoxType,
                    Fk_FluteType = oAtualObject.FluteType.FluteName

                };
                return Json(new { success = true, data = oHeadToDisplay });
            }
            else
            {
                var oHeadToDisplay = new SampleBoxMaster();
                return Json(new { success = true, data = oHeadToDisplay });
            }
        }
        [HttpPost]
        public JsonResult NameDuplicateChecking(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sName = oValues["Name"];
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Name", sName));
                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);
                List<EntityObject> oHeadToDisplayObjects = oSearchResult.ListOfRecords;
                List<SampleBoxMaster> oHeadObjects = oHeadToDisplayObjects.Select(p => p).OfType<SampleBoxMaster>().ToList();
                var oHeadToDisplay = oHeadObjects.Select(p => new
                {
                    Pk_BoxId = p.Pk_BoxID,
                    Name = p.Name
                }).ToList().OrderBy(s => s.Name);
                return Json(new { Success = true, Records = oHeadToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult BoxNameListByFiter(string Name = "", string CustName = "", string Pk_BoxID = "", string Pk_CustID = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Name", Name));
                oSearchParams.Add(new SearchParameter("CustName", CustName));
                oSearchParams.Add(new SearchParameter("Pk_CustID", Pk_CustID));
                oSearchParams.Add(new SearchParameter("Pk_BoxID", Pk_BoxID));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);
                List<EntityObject> oHeadToDisplayObjects = oSearchResult.ListOfRecords;

                List<SampleBoxMaster> oHeadObjects = oHeadToDisplayObjects.Select(p => p).OfType<SampleBoxMaster>().ToList();
                //Create a anominious object here to break the circular reference
                var oHeadToDisplay = oHeadObjects.Select(p => new
                {
                    Pk_BoxID = p.Pk_BoxID,
                    BTypeId = p.BoxType.Pk_BoxTypeID,
                    BoxType = p.BoxType.Name,
                    Customer = p.gen_Customer.CustomerName,
                    Name = p.Name,
                    Description = p.Description,
                    PartNo = p.PartNo,
                    Fk_FluteType = p.Fk_FluteType,



                }).ToList();
                return Json(new { Result = "OK", Records = oHeadToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


        //[HttpPost]
        //public JsonResult JobBoxDetails(string Name = "", string Pk_BoxID = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        //{
        //    try
        //    {
        //        List<SearchParameter> oSearchParams = new List<SearchParameter>();
        //        oSearchParams.Add(new SearchParameter("Name", Name));
        //        oSearchParams.Add(new SearchParameter("Pk_BoxID", Pk_BoxID));
        //        oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
        //        oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

        //        SearchResult oSearchResult = oBoxMaster.SearchJobBox(oSearchParams);
        //        List<EntityObject> oHeadToDisplayObjects = oSearchResult.ListOfRecords;

        //        List<Vw_JobBoxDetails> oHeadObjects = oHeadToDisplayObjects.Select(p => p).OfType<Vw_JobBoxDetails>().ToList();
        //        //Create a anominious object here to break the circular reference
        //        var oHeadToDisplay = oHeadObjects.Select(p => new
        //        {
        //            Pk_BoxID = p.Fk_BoxID,
        //            Name = p.Name,
        //            CustomerName = p.CustomerName,
        //            Pk_JobCardID = p.Pk_JobCardID,
        //            JDate = DateTime.Parse(p.JDate.ToString()).ToString("dd/MM/yyyy"),
        //            Quantity = p.Quantity,
        //            TotalPrice = p.TotalPrice

        //        }).ToList().OrderBy(s => s.Pk_BoxID);
        //        return Json(new { Result = "OK", Records = oHeadToDisplay, TotalRecordCount = oSearchResult.RecordCount });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { Result = "ERROR", Message = ex.Message });
        //    }
        //}

        [HttpPost]
        public JsonResult BoxDetails(string Pk_BoxID = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                Samples_BoxMasterOld oBoxMaster = oDoaminObject as Samples_BoxMasterOld;

                oBoxMaster.ID = decimal.Parse(Pk_BoxID);

                SampleBoxMaster oEnquiryObjects = oBoxMaster.DAO as SampleBoxMaster;

                var oDeliveryScheduleToDisplay = oEnquiryObjects.SampleBoxSpecs.Select(p => new
                {
                    Pk_BoxSpecID = p.Pk_BoxSpecID,
                    OYes = p.OuterShellYes,
                    CYes = p.CapYes,
                    LPYes = p.LenghtPartationYes,
                    WPYes = p.WidthPartationYes,
                    PYes = p.PlateYes,
                    TYes = p.TopYes,
                    BYes = p.BottomYes


                }).ToList();

                return Json(new { Result = "OK", Records = oDeliveryScheduleToDisplay, TotalRecordCount = oDeliveryScheduleToDisplay.Count() });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


        [HttpPost]
        public JsonResult BoxPartDetails(string Pk_BoxID = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {

                var BoxID = Pk_BoxID;

                ModelManuplationResult oResult = new ModelManuplationResult();

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_BoxID", BoxID));


                SearchResult oSearchResult = oItProp.SearchBoxSp(oSearchParams);

                List<EntityObject> oEnquirysToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_BoxSpec> oEnquiryObjects = oEnquirysToDisplayObjects.Select(p => p).OfType<Vw_BoxSpec>().ToList();


                //oBoxMaster.ID = decimal.Parse(Pk_BoxID);              

                var oDeliveryScheduleToDisplay = oEnquiryObjects.Select(p => new
                {
                    Pk_PartPropertyID = p.Pk_PartPropertyID,
                    PName = p.PName,
                    Length = p.Length,
                    Width = p.Width,
                    Height = p.Height,
                    Weight = p.Weight,
                    //BType=p.BType,
                    //CuttingSize=p.CuttingSize,
                    //BoardArea=p.BoardArea,
                    //Deck=p.Deckle
                }).ToList();
                return Json(new { Result = "OK", Records = oDeliveryScheduleToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }



        [HttpPost]
        public ActionResult SBoxDet(string data = "")
        {

            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string PartID = "";

            string pId = oValues["Pk_BoxID"];

            if (oValues.ContainsKey("PartID"))
            {
                PartID = oValues["PartID"];
            }
            else
            {
                PartID = "";
            }
            if (pId != "")
            {
                var BoxID = int.Parse(pId);
                ModelManuplationResult oResult = new ModelManuplationResult();

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_BoxID", pId));
                oSearchParams.Add(new SearchParameter("PartID", PartID));
                SearchResult oSearchResult = oItProp.SearchP(oSearchParams);

                List<EntityObject> oEnquirysToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_SampleBoxDet> oEnquiryObjects = oEnquirysToDisplayObjects.Select(p => p).OfType<Vw_SampleBoxDet>().ToList();

                var oEnquiryToDisplay = oEnquiryObjects.Select(p => new
                {
                    Pk_PartPropertyID = p.Pk_PartPropertyID,
                    PName = p.PName,
                    Length = p.Length,
                    Width = p.Width,
                    Height = p.Height,
                    BType = p.BType,
                    CuttingSize = p.CuttingSize,
                    BoardArea = p.BoardArea,
                    Deck = p.Deckle,
                    Quantity = p.Quantity,
                    Ply = p.PlyVal,
                    AsPer=p.AsPer,
                });

                return Json(new { success = true, data = oEnquiryToDisplay });

            }
            return null;
        }

 
        [HttpPost]
        public ActionResult SMachineDet(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);


            string pId = oValues["Pk_Machine"];
            if (pId != "")
            {
                var BoxID = int.Parse(pId);
                ModelManuplationResult oResult = new ModelManuplationResult();

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_Machine", pId));

                SearchResult oSearchResult = oBoxMaster.SearchMachine(oSearchParams);

                List<EntityObject> oEnquirysToDisplayObjects = oSearchResult.ListOfRecords;
                List<gen_Machine> oEnquiryObjects = oEnquirysToDisplayObjects.Select(p => p).OfType<gen_Machine>().ToList();

                var oEnquiryToDisplay = oEnquiryObjects.Select(p => new
                {

                    Capacity_Length = p.Capacity_Length,

                });

                return Json(new { success = true, data = oEnquiryToDisplay });

            }
            return null;
        }

        public ActionResult BoxRep(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["fkbox"]);

                List<Vw_BoxDet> BillList = new List<Vw_BoxDet>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                //BillList = dc.VwInvoices.ToList();
                BillList = dc.Vw_BoxDet.Where(x => x.Pk_BoxID == sInvno).Select(x => x).OfType<Vw_BoxDet>().ToList();

                if (BillList.Count > 0)
                {
                    DataTable dt = new DataTable();

                    dt.Columns.Add("Pk_BoxID");
                    dt.Columns.Add("Name");
                    dt.Columns.Add("Length");
                    dt.Columns.Add("Width");
                    dt.Columns.Add("Height");
                    dt.Columns.Add("Weight");
                    dt.Columns.Add("GSM");
                    dt.Columns.Add("BF");
                    dt.Columns.Add("Pk_PartPropertyID");
                    dt.Columns.Add("OuterShellYes");
                    dt.Columns.Add("CapYes");
                    dt.Columns.Add("LenghtPartationYes");
                    dt.Columns.Add("WidthPartationYes");
                    dt.Columns.Add("PlateYes");
                    dt.Columns.Add("MaterialName");
                    dt.Columns.Add("Customer");
                    dt.Columns.Add("PartNo");
                    dt.Columns.Add("PlyVal");
                    dt.Columns.Add("Deckle");
                    dt.Columns.Add("CuttingSize");
                    dt.Columns.Add("FluteName");
                    dt.Columns.Add("TakeUpFactor");
                    dt.Columns.Add("CustomerName");
                    dt.Columns.Add("BoardGSM");
                    dt.Columns.Add("BoardBS");
                    dt.Columns.Add("PName");
                    dt.Columns.Add("BType");
                    dt.Columns.Add("LayerTKF");
                    dt.Columns.Add("ColorName");


                    foreach (Vw_BoxDet entity in BillList)
                    {
                        DataRow row = dt.NewRow();

                        row["Pk_BoxID"] = entity.Pk_BoxID;
                        row["Name"] = entity.Name;
                        row["Length"] = entity.Length;
                        row["Width"] = entity.Width;
                        row["Height"] = entity.Height;
                        row["Weight"] = entity.Weight;
                        row["GSM"] = entity.GSM;
                        row["BF"] = entity.BF;
                        row["Pk_PartPropertyID"] = entity.Pk_PartPropertyID;
                        row["OuterShellYes"] = entity.OuterShellYes;
                        row["CapYes"] = entity.CapYes;
                        row["LenghtPartationYes"] = entity.LenghtPartationYes;
                        row["WidthPartationYes"] = entity.WidthPartationYes;
                        row["PlateYes"] = entity.PlateYes;
                        row["MaterialName"] = entity.MaterialName;
                        row["BType"] = entity.BType;
                        row["PName"] = entity.PName;
                        row["CustomerName"] = entity.CustomerName;
                        row["BoardBS"] = entity.BoardBS;
                        row["BoardGSM"] = entity.BoardGSM;
                        row["Deckle"] = entity.Deckle;
                        row["CuttingSize"] = entity.CuttingSize;
                        row["FluteName"] = entity.FluteName;
                        row["TakeUpFactor"] = entity.TakeUpFactor;
                        row["LayerTKF"] = entity.LayerTKF;
                        row["ColorName"] = entity.ColorName;
                        dt.Rows.Add(row);
                    }


                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.BoxReport orp = new CogitoStreamline.Report.BoxReport();

                    orp.Load("@\\Report\\BoxReport.rpt");
                    orp.SetDataSource(dt.DefaultView);

                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "BoxReport" + sInvno + ".pdf");
                    FileInfo file = new FileInfo(pdfPath);
                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();

                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }

        public ActionResult BoxDist(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["fkbox"]);


                List<SampleBoxMaster> BillList = new List<SampleBoxMaster>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                //BillList = dc.VwInvoices.ToList();
                BillList = dc.SampleBoxMaster.Where(x => x.Pk_BoxID == sInvno).Select(x => x).OfType<SampleBoxMaster>().ToList();

                if (BillList.Count > 0)
                {
                    DataTable dt = new DataTable();

                    dt.Columns.Add("Pk_BoxID");
                    dt.Columns.Add("Name");
                    //dt.Columns.Add("Length");
                    //dt.Columns.Add("Width");
                    //dt.Columns.Add("Height");
                    //dt.Columns.Add("Weight");
                    //dt.Columns.Add("GSM");
                    //dt.Columns.Add("BF");
                    //dt.Columns.Add("Pk_PartPropertyID");
                    //dt.Columns.Add("OuterShellYes");
                    //dt.Columns.Add("CapYes");
                    //dt.Columns.Add("LenghtPartationYes");
                    //dt.Columns.Add("WidthPartationYes");
                    //dt.Columns.Add("PlateYes");
                    //dt.Columns.Add("MaterialName");

                    //dt.Columns.Add("Customer");
                    //dt.Columns.Add("PartNo");
                    //dt.Columns.Add("PlyVal");
                    //dt.Columns.Add("Deckle");
                    //dt.Columns.Add("CuttingSize");
                    //dt.Columns.Add("FluteName");
                    //dt.Columns.Add("TakeUpFactor");
                    //dt.Columns.Add("CustomerName");


                    foreach (SampleBoxMaster entity in BillList)
                    {
                        DataRow row = dt.NewRow();

                        row["Pk_BoxID"] = entity.Pk_BoxID;
                        row["Name"] = entity.Name;
                        //row["Length"] = entity.Length;
                        //row["Width"] = entity.Width;
                        //row["Height"] = entity.Height;
                        //row["Weight"] = entity.Weight;
                        //row["GSM"] = entity.GSM;
                        //row["BF"] = entity.BF;
                        //row["Pk_PartPropertyID"] = entity.Pk_PartPropertyID;
                        //row["OuterShellYes"] = entity.OuterShellYes;
                        //row["CapYes"] = entity.CapYes;
                        //row["LenghtPartationYes"] = entity.LenghtPartationYes;
                        //row["WidthPartationYes"] = entity.WidthPartationYes;
                        //row["PlateYes"] = entity.PlateYes;
                        //row["MaterialName"] = entity.MaterialName;

                        //row["Customer"] = entity.Customer;
                        //row["CustomerName"] = entity.CustomerName;
                        //row["PartNo"] = entity.PartNo;
                        //row["PlyVal"] = entity.PlyVal;
                        //row["Deckle"] = entity.Deckle;
                        //row["CuttingSize"] = entity.CuttingSize;
                        //row["FluteName"] = entity.FluteName;
                        //row["TakeUpFactor"] = entity.TakeUpFactor;
                        //;


                        dt.Rows.Add(row);
                    }

                    var Pk_BoxID = sInvno;
                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.Box orp = new CogitoStreamline.Report.Box();

                    orp.Load("@\\Report\\Box.rpt");
                    orp.SetDataSource(dt.DefaultView);


                    CrystalDecisions.CrystalReports.Engine.ReportDocument repDoc = orp;

                    ParameterFieldDefinitions crParameterFieldDefinitions3;
                    ParameterFieldDefinition crParameterFieldDefinition3;
                    ParameterValues crParameterValues3 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue3 = new ParameterDiscreteValue();
                    crParameterDiscreteValue3.Value = Pk_BoxID;
                    crParameterFieldDefinitions3 = orp.DataDefinition.ParameterFields;
                    crParameterFieldDefinition3 = crParameterFieldDefinitions3["Pk_BoxID"];
                    crParameterValues3 = crParameterFieldDefinition3.CurrentValues;

                    crParameterValues3.Clear();
                    crParameterValues3.Add(crParameterDiscreteValue3);
                    crParameterFieldDefinition3.ApplyCurrentValues(crParameterValues3);

                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "Box" + sInvno + ".pdf");
                    FileInfo file = new FileInfo(pdfPath);
                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();

                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }
        [HttpPost]
        public JsonResult FindWeight(string data)
        {
            decimal? CCode = 0;
            int i = 0;
            decimal TempVal;
            decimal WtBox;


            if (data != "{}")
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

                decimal pId = Convert.ToDecimal(oValues["Boxid"]);
                TempVal = 0;
                WtBox = 0;

                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();
                List<Vw_BoxSpec> oPrdListChild = new List<Vw_BoxSpec>();
                oPrdListChild = dc.Vw_BoxSpec.Where(p => p.Pk_BoxID == pId).Select(p => p).OfType<Vw_BoxSpec>().ToList();


                int RCount = _oEntites.Vw_BoxSpec.Where(p => p.Pk_BoxID == pId).Count();
                if (RCount > 0)
                {

                    //Vw_BoxSpec oQC = _oEntites.Vw_BoxSpec.Where(p => p.Pk_BoxID == pId).Single();

                    for (i = 0; i < RCount; i++)
                    {
                        TempVal = Convert.ToDecimal(oPrdListChild.ElementAt(i).Weight);

                        WtBox = WtBox + TempVal;
                    }

                    CCode = WtBox;
                }

                else
                { CCode = 0; }

            }
            return Json(CCode);
        }

        [HttpPost]

        public JsonResult SaveNew([System.Web.Http.FromBody]string data)
        {
            data = data.TrimStart('=');
            Dictionary<string, object> values = JsonConvert.DeserializeObject<Dictionary<string, object>>(data);
            data = JsonConvert.SerializeObject(values);
            return base.Save(data);


        }
    }
}

