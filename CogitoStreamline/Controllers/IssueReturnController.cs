﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;
using System.IO;
using System.Data;
using CrystalDecisions.ReportAppServer.ReportDefModel;
using System.Drawing.Printing;
using CrystalDecisions.Shared;


namespace CogitoStreamline.Controllers
{
    
    public class IssueReturnController : CommonController
    {
        #region Variables
        IssueReturns oIssueReturn = new IssueReturns();
        #endregion Variables

        #region Constractor
        public IssueReturnController()
        {
            oDoaminObject = new IssueReturns();
        }
        #endregion Constractor

        #region Methods
        public override ActionResult Index()
        {
            ViewBag.Header = "Issue Return";
            return base.Index();
        }
        protected override void Dispose(bool disposing)
        {
            oIssueReturn.Dispose();
            base.Dispose(disposing);
        }
        [HttpPost]
        public override JsonResult Load(string data = "")
        {

            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                IssueReturns oIssueReturns = oDoaminObject as IssueReturns;
                oIssueReturns.ID = decimal.Parse(pId);
                IssueReturn oAtualObject = oIssueReturns.DAO as IssueReturn;

                int i = -1;

                var oMaterials = oAtualObject.IssueReturnDetails.Select(p => new
                {
                    slno = ++i,
                    ReturnQuantity = p.ReturnQuantity,
                    Fk_Material = p.Fk_Material,
                    Name = p.Inv_Material.Name,
                    Pk_StockID=p.Pk_StockID,
                    Pk_IssueReturnDetailsId = p.Pk_IssueReturnDetailsId,
                    RollNo = p.RollNo,
                    Color=p.Inv_Material.gen_Color.ColorName,
                    Mill=p.Inv_Material.gen_Mill.MillName,
                });

                var oIssueReturnDisplay = new
                {
                    Pk_IssueReturnMasterId = oAtualObject.Pk_IssueReturnMasterId,
                    Fk_EnteredByUserID = oAtualObject.Fk_EnteredUserId,
                    IssueReturnDate = DateTime.Parse(oAtualObject.IssueReturnDate.ToString()).ToString("dd/MM/yyyy"),
                    IssueDate = DateTime.Parse(oAtualObject.IssueDate.ToString()).ToString("dd/MM/yyyy"),
                    Fk_IssueId = oAtualObject.Fk_IssueId,
                    MaterialData = Json(oMaterials).Data
                };

                return Json(new { success = true, data = oIssueReturnDisplay });
            }
            else
            {
                var oReturnGoodFromSiteDisplay = new IssueReturn();
                return Json(new { success = true, data = oReturnGoodFromSiteDisplay });
            }
        }
        public JsonResult getIssueDetails(string Pk_MaterialIssueID = "", string MaterialName = "",  int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {

            try
            {
                IssueReturns oIssueDetails = new IssueReturns();
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_MaterialIssueID", Pk_MaterialIssueID));
                oSearchParams.Add(new SearchParameter("MaterialName", MaterialName));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oIssueDetails.SearchIssueDetails(oSearchParams);

                List<EntityObject> oIssueDetailsToDisplayObjects = oSearchResult.ListOfRecords;
                List<MaterialIssueDetail> oIssueObjects = oIssueDetailsToDisplayObjects.Select(p => p).OfType<MaterialIssueDetail>().ToList();

                //Create a anominious object here to break the circular reference
                var oIssueDetailsToDisplay = oIssueObjects.Where(p => (p.Quantity - p.ReturnQuantity) > 0).Select(p => new
                {
                    Fk_Material = p.Fk_Material,
                    MaterialName = p.Inv_Material.Name,
                    Quantity = (p.Quantity-p.ReturnQuantity),
                    Pk_StockID=p.Pk_StockID,
                    RollNo=p.RollNo,
                    Color=p.Inv_Material.gen_Color.ColorName,
                    Mill=p.Inv_Material.gen_Mill.MillName,
                }).ToList();

                return Json(new { Result = "OK", Records = oIssueDetailsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }

        }
        [HttpPost]
        public JsonResult getPendingDetails(string data = "")
        {

            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string Pk_MaterialIssueID = oValues["Pk_MaterialIssueID"];
            string Fk_Material = oValues["Fk_Material"];

            IssueReturns oIssueDetails = new IssueReturns();
            List<SearchParameter> oSearchParams = new List<SearchParameter>();
            oSearchParams.Add(new SearchParameter("Pk_MaterialIssueID", Pk_MaterialIssueID));
            oSearchParams.Add(new SearchParameter("Fk_Material", Fk_Material));
            oSearchParams.Add(new SearchParameter("StartIndex", "0"));
            oSearchParams.Add(new SearchParameter("PageSize", "10"));

            SearchResult oSearchResult = oIssueDetails.SearchIssueDetails(oSearchParams);

            List<EntityObject> oIssueDetailsToDisplayObjects = oSearchResult.ListOfRecords;
            List<MaterialIssueDetail> oIssueObjects = oIssueDetailsToDisplayObjects.Select(p => p).OfType<MaterialIssueDetail>().ToList();

            var oIssueDetailsToDisplay = oIssueObjects.Where(p => (p.Quantity - p.ReturnQuantity) > 0).Select(p => new
            {
                Fk_Material = p.Fk_Material,
                MaterialName = p.Inv_Material.Name,
                Quantity = (p.Quantity - p.ReturnQuantity)
                
            }).ToList();

            return Json(new { Result = "OK", Records = oIssueDetailsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
        }
        [HttpPost]
        public JsonResult IssueReturnListByFiter(string Pk_IssueReturnMasterId = "", string IssueReturnDate = null, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_IssueReturnMasterId", Pk_IssueReturnMasterId));
                oSearchParams.Add(new SearchParameter("IssueReturnDate", IssueReturnDate));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oIssueReturnToDisplayObjects = oSearchResult.ListOfRecords;
                List<IssueReturn> oIssueReturnObjects = oIssueReturnToDisplayObjects.Select(p => p).OfType<IssueReturn>().ToList();

                var oIssureReturnToDisplay = oIssueReturnObjects.Select(p => new
                {
                    Pk_IssueReturnMasterId = p.Pk_IssueReturnMasterId,
                    Fk_EnteredByUserID = p.ideUser.FirstName,
                    IssueReturnDate = DateTime.Parse(p.IssueReturnDate.ToString()).ToString("dd/MM/yyyy"),
                    Fk_IssueId=p.Fk_IssueId,
                    IssueDate = DateTime.Parse(p.IssueDate.ToString()).ToString("dd/MM/yyyy"),
                    CustName=p.MaterialIssue.gen_Order.gen_Customer.CustomerName,
                    JobCardID=p.MaterialIssue.Fk_JobCardID


                }).ToList();

                return Json(new { Result = "OK", Records = oIssureReturnToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        #endregion Methods


        public ActionResult ReturnRep(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["Pk_IssRet"]);

                List<Vw_Iss_Return> BillList = new List<Vw_Iss_Return>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                //BillList = dc.VwInvoices.ToList();
                BillList = dc.Vw_Iss_Return.Where(x => x.Pk_IssueReturnMasterId == sInvno).Select(x => x).OfType<Vw_Iss_Return>().ToList();

 //           SELECT "Vw_Iss_Return"."Pk_IssueReturnMasterId", "Vw_Iss_Return"."IssueReturnDate", "Vw_Iss_Return"."Fk_IssueId", 
 //               "Vw_Iss_Return"."IssueDate", "Vw_Iss_Return"."Name", "Vw_Iss_Return"."ReturnQuantity", "Vw_Iss_Return"."RollNo"
 //FROM   "ERP"."dbo"."Vw_Iss_Return" "Vw_Iss_Return"
 //ORDER BY "Vw_Iss_Return"."Fk_IssueId"



                if (BillList.Count > 0)
                {
                    DataTable dt = new DataTable();

                    dt.Columns.Add("Pk_IssueReturnMasterId");
                    dt.Columns.Add("IssueReturnDate");
                    dt.Columns.Add("Fk_IssueId");
                    dt.Columns.Add("IssueDate");
                    dt.Columns.Add("Name");
                    dt.Columns.Add("ReturnQuantity");
                    dt.Columns.Add("RollNo");
                    dt.Columns.Add("ColorName");
                    dt.Columns.Add("MillName");


                    foreach (Vw_Iss_Return entity in BillList)
                    {
                        DataRow row = dt.NewRow();

                        row["Pk_IssueReturnMasterId"] = entity.Pk_IssueReturnMasterId;
                        row["IssueReturnDate"] = entity.IssueReturnDate;
                        row["Fk_IssueId"] = entity.Fk_IssueId;
                        row["IssueDate"] = entity.IssueDate;
                        row["Name"] = entity.Name;
                        row["ReturnQuantity"] = entity.ReturnQuantity;
                        row["RollNo"] = entity.RollNo;
                        row["ColorName"] = entity.ColorName;
                        row["MillName"] = entity.MillName;
                        dt.Rows.Add(row);
                    }


                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.IssReturn orp = new CogitoStreamline.Report.IssReturn();

                    orp.Load("@\\Report\\IssReturn.rpt");
                    orp.SetDataSource(dt.DefaultView);

                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "IssReturn" + sInvno + ".pdf");
                    FileInfo file = new FileInfo(pdfPath);
                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();

                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }

        [HttpPost]

        public JsonResult SaveNew([System.Web.Http.FromBody]string data)
        {
            data = data.TrimStart('=');
            Dictionary<string, object> values = JsonConvert.DeserializeObject<Dictionary<string, object>>(data);
            data = JsonConvert.SerializeObject(values);
            return base.Save(data);


        }
    }
}


