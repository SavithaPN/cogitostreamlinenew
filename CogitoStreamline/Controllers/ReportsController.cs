﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using WebGareCore;
using CogitoStreamline.Controllers;
//using iTextSharp;
using System.IO;
//using iTextSharp.text;
//using iTextSharp.text.pdf;
using System.Data;
using Microsoft.Reporting.WebForms;
using CogitoStreamline;
using System.Globalization;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Drawing.Printing;
using System.Data.SqlClient;



namespace CogitoStreamLine.Controllers
{
    public class ReportsController : CommonController
    {

        CogitoStreamLineEntities _oEntities = new CogitoStreamLineEntities();
        Rept oEnq = new Rept();
        Cgen_DeliverySchedule OSch = new Cgen_DeliverySchedule();
         
        public ReportsController()
        {
            oDoaminObject = new Rept();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Reports";
            return base.Index();
        }
        protected override void Dispose(bool disposing)
        {
            //oBoxMaster.Dispose();
            base.Dispose(disposing);
        }

        //[HttpPost]
        //public JsonResult getSchedules(string pId = "")
        //{
        //    List<gen_DeliverySchedule> oListOfBoxType = OSch.Search(null).ListOfRecords.OfType<gen_DeliverySchedule>().ToList();

        //    var oBoxToDisplay = oListOfBoxType.Select(p => new
        //    {
        //        Name = p.gen_Order.Cust_PO,
        //        Id = p.Pk_DeliverySechedule

        //    });

        //    return Json(oBoxToDisplay);
        //}
               [HttpPost]
               public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                BoxM oBox = oDoaminObject as BoxM;
                oBox.ID = decimal.Parse(pId);
                BoxMaster oAtualObject = oBox.DAO as BoxMaster;


                int i = -1;

                //var oBoxDetails = oAtualObject.BoxChilds.Select(p => new
                //{
                //    Pk_BoxCID = p.Pk_BoxCID,
                //    slno = ++i,
                //    Fk_Material = p.Fk_Material,
                //    txtFk_Material = p.Inv_Material.Name,
                //    Name = p.Inv_Material.Name
                //});


                //Create a anominious object here to break the circular reference
                var oHeadToDisplay = new
                {
                    Pk_BoxID = oAtualObject.Pk_BoxID,
                    Name = oAtualObject.Name,
                    Description = oAtualObject.Description,
                    //Customer = oAtualObject.gen_Customer.CustomerName,
                    PartNo = oAtualObject.PartNo,
                    Fk_BoxType = oAtualObject.Fk_BoxType,
                    //Ply = oAtualObject.Ply,
                    //BoxDetails = Json(oBoxDetails).Data
                };
                return Json(new { success = true, data = oHeadToDisplay });
            }
            else
            {
                var oHeadToDisplay = new BoxMaster();
                return Json(new { success = true, data = oHeadToDisplay });
            }
        }
              
        public ActionResult OpenJobCards(string data = "")
               {
                   try
                   {
                      

                       List<Vw_JCard> BillList = new List<Vw_JCard>();
                       CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                       BillList = dc.Vw_JCard.Where(x => x.Fk_Status == 1).Select(x => x).OfType<Vw_JCard>().ToList();

                       if (BillList.Count > 0)
                       {
                           DataTable dt = new DataTable("JobCard");

                           dt.Columns.Add("CustomerName");
                           dt.Columns.Add("JDate");
                           dt.Columns.Add("BName");
                           dt.Columns.Add("Fk_Order");
                           dt.Columns.Add("PName");
                           dt.Columns.Add("TotalQty");
                           dt.Columns.Add("Pk_JobCardID");


                           foreach (Vw_JCard entity in BillList)
                           {
                               DataRow row = dt.NewRow();

                               row["CustomerName"] = entity.CustomerName;
                               row["JDate"] = entity.JDate;
                               row["BName"] = entity.BName;
                               row["PName"] = entity.PName;
                               row["Pk_JobCardID"] = entity.Pk_JobCardID;
                               row["Fk_Order"] = entity.Fk_Order;
                               row["TotalQty"] = entity.TotalQty;

                               dt.Rows.Add(row);

                           }

                           DataSet ds = new DataSet();
                           ds.Tables.Add(dt);
                           CogitoStreamline.Report.OpenJobCards orp = new CogitoStreamline.Report.OpenJobCards();

                           orp.Load("@\\Report\\OpenJobCards.rpt");
                           orp.SetDataSource(dt.DefaultView);



                           ReportDocument repDoc = orp;


                           Response.Buffer = false;
                           Response.ClearContent();
                           Response.ClearHeaders(); 
                           string pdfPath = Server.MapPath("~/ConvertPDF/" + "OpenJobCards" + ".pdf");
                           FileInfo file = new FileInfo(pdfPath);
                           if (file.Exists)
                           {
                               file.Delete();
                           }
                           var pd = new PrintDocument();


                           orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                           objDiskOpt.DiskFileName = pdfPath;

                           orp.ExportOptions.DestinationOptions = objDiskOpt;
                           orp.Export();

                       }
                       return null;
                   }
                   catch (Exception ex)
                   {
                       return null;
                       //  Response.Redirect("Index");D:\shantha\Billing\CogitoStreamline\CogitoStreamline\Controllers\CartonBillingController.cs
                   }
               }

               public ActionResult JobCardStock(string data = "")
               {
                   try
                   {

                       Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                       DateTime SFrom = Convert.ToDateTime(oValues["FDate"]);
                       DateTime STo = Convert.ToDateTime(oValues["TDate"]);

                       string[] a = oValues["FDate"].ToString().Split('/');
                       DateTime FromDate = DateTime.ParseExact(a[1] + '/' + a[0] + '/' + a[2] + " 12:00", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);

                       var FD = SFrom.Day;
                       var FM = SFrom.ToString("MMM");
                       var FY = SFrom.Year;

                       var SFromDVal = FD.ToString() + '/' + FM.ToString() + '/' + FY.ToString();



                       var TD = STo.Day;
                       var TM = STo.ToString("MMM");
                       var TY = STo.Year;

                       var SToDVal = TD.ToString() + '/' + TM.ToString() + '/' + TY.ToString();


                       int dDay = Convert.ToInt32(a[0]);
                       int dmonth = Convert.ToInt32(a[1]);
                       int dyear = Convert.ToInt32(a[2]);

                       string monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dmonth);
                       //string Mname=FromDate.ToString( "MMMM");   // this one will also work to get the month name

                       string Repfor = "PPC Between - " + " " + SFromDVal + " " + "- " + SToDVal + " ";

                       List<Vw_NetPapReqd> BillList = new List<Vw_NetPapReqd>();
                       CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

 //                       SELECT Vw_NetPapReqd.PaperReq, Vw_NetPapReqd.Pk_Material, Vw_NetPapReqd.Name, 
 //                      Vw_NetPapReqd.MillName, Vw_NetPapReqd.ColorName, Vw_NetPapReqd.stkqty, Vw_NetPapReqd.CustomerName,
 //                      Vw_NetPapReqd.Box Name, Vw_NetPapReqd.Length, Vw_NetPapReqd.Width, Vw_NetPapReqd.Height
 //FROM   V2Innovatives.dbo.Vw_NetPapReqd Vw_NetPapReqd
 //ORDER BY Vw_NetPapReqd.Pk_Material

                       BillList = dc.Vw_NetPapReqd.Where(x => x.DeliveryDate >= SFrom && x.DeliveryDate <= STo).Select(x => x).OfType<Vw_NetPapReqd>().OrderBy(p => p.stkqty).ToList();

                       if (BillList.Count > 0)
                       {
                           DataTable dt = new DataTable("Paper");

                           dt.Columns.Add("PaperReq");
                           dt.Columns.Add("Pk_Material");
                           dt.Columns.Add("Name");
                           dt.Columns.Add("ColorName");
                           dt.Columns.Add("stkqty");
                           dt.Columns.Add("MillName");

                           dt.Columns.Add("CustomerName");
                           dt.Columns.Add("Box_Name");
                           dt.Columns.Add("Length");
                           dt.Columns.Add("Width");
                           dt.Columns.Add("Height");
                          
                           foreach (Vw_NetPapReqd entity in BillList)
                           {
                               DataRow row = dt.NewRow();

                               row["Name"] = entity.Name;
                               row["Pk_Material"] = entity.Pk_Material;
                               row["ColorName"] = entity.ColorName;
                               row["stkqty"] = entity.stkqty;
                               row["PaperReq"] = entity.PaperReq;
                               row["MillName"] = entity.MillName;

                               row["CustomerName"] = entity.CustomerName;
                               row["Box_Name"] = entity.Box_Name;
                               row["Length"] = entity.Length;
                               row["Width"] = entity.Width;
                               row["Height"] = entity.Height;

                               //row["Quantity"] = entity.Quantity;

                               dt.Rows.Add(row);

                           }

                           DataSet ds = new DataSet();
                           ds.Tables.Add(dt);
                           CogitoStreamline.Report.StockVsJobCard orp = new CogitoStreamline.Report.StockVsJobCard();

                           orp.Load("@\\Report\\StockVsJobCard.rpt");
                           orp.SetDataSource(dt.DefaultView);



                           ReportDocument repDoc = orp;

                           ParameterFieldDefinitions crParameterFieldDefinitions4;
                           ParameterFieldDefinition crParameterFieldDefinition4;
                           ParameterValues crParameterValues4 = new ParameterValues();
                           ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                           crParameterDiscreteValue4.Value = Repfor;
                           crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                           crParameterFieldDefinition4 = crParameterFieldDefinitions4["period"];
                           crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                           crParameterValues4.Clear();
                           crParameterValues4.Add(crParameterDiscreteValue4);
                           crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);

                           Response.Buffer = false;
                           Response.ClearContent();
                           Response.ClearHeaders(); 
                           string pdfPath = Server.MapPath("~/ConvertPDF/" + "Paper_Requirement" + ".pdf");
                           FileInfo file = new FileInfo(pdfPath);
                           if (file.Exists)
                           {
                               file.Delete();
                           }
                           var pd = new PrintDocument();


                           orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                           objDiskOpt.DiskFileName = pdfPath;

                           orp.ExportOptions.DestinationOptions = objDiskOpt;
                           orp.Export();

                       }
                       return null;
                   }
                   catch (Exception ex)
                   {
                       return null;
                       //  Response.Redirect("Index");D:\shantha\Billing\CogitoStreamline\CogitoStreamline\Controllers\CartonBillingController.cs
                   }
               }
               public ActionResult PPCRep(string data = "")
               {
                   try
                   {

                       int i = 0;
                       Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                       DateTime SFrom = Convert.ToDateTime(oValues["FDate"]);
                       DateTime STo = Convert.ToDateTime(oValues["TDate"]);

                       string[] a = oValues["FDate"].ToString().Split('/');
                       DateTime FromDate = DateTime.ParseExact(a[1] + '/' + a[0] + '/' + a[2] + " 12:00", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);

                       var FD = SFrom.Day;
                       var FM = SFrom.ToString("MMM");
                       var FY = SFrom.Year;

                       var SFromDVal = FD.ToString() + '/' + FM.ToString() + '/' + FY.ToString();



                       var TD = STo.Day;
                       var TM = STo.ToString("MMM");
                       var TY = STo.Year;

                       var SToDVal = TD.ToString() + '/' + TM.ToString() + '/' + TY.ToString();


                       int dDay = Convert.ToInt32(a[0]);
                       int dmonth = Convert.ToInt32(a[1]);
                       int dyear = Convert.ToInt32(a[2]);

                       string monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dmonth);
                       //string Mname=FromDate.ToString( "MMMM");   // this one will also work to get the month name

                       string Repfor = "PPC Between - " + " " + SFromDVal + " " + "- " + SToDVal + " ";
                       CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                       SqlConnection con = new SqlConnection("Data Source=SQL5005.site4now.net;Initial Catalog=DB_A44946_V2Innovatives;User ID=DB_A44946_V2Innovatives_admin;");
                       //SqlConnection con = new SqlConnection("data Source= sql5005.site4now.net;Initial Catalog=BalajiTesting;User ID=sa;password=admin123;");

                       SqlCommand cmd = new SqlCommand("USP1", con);
                       cmd.CommandType = CommandType.StoredProcedure;


                       cmd.Parameters.Add("@SDate", SqlDbType.DateTime).Value = SFromDVal;
                       cmd.Parameters.Add("@EDate", SqlDbType.DateTime).Value = SToDVal;

                       con.Open();
                       cmd.ExecuteNonQuery();
                       con.Close();

                       List<tempPPC> BillList = new List<tempPPC>();


                       BillList = dc.tempPPC.Where(s => s.Pk_BoxID > 0).Select(s => s).OfType<tempPPC>().ToList();

                       var cList = BillList.Select(o => o.Pk_BoxID).Distinct();
                       int j = 0;
                       var PlyCount1 = 0;
                       int k = 0;
                       var LCount = cList.Count();
                       if (BillList.Count > 0)
                       {
                           for (i = 0; i < LCount; i++)
                           {
                               var CPosition = cList.ElementAtOrDefault(i);
                               List<tempPPC> Temp1 = new List<tempPPC>();
                               Temp1 = dc.tempPPC.Where(p => p.Pk_BoxID == CPosition).OrderBy(p => p.Pk_LayerID).ToList();
                               if(Temp1.Count>1)
                               {
                               PlyCount1 = Convert.ToInt32(Temp1[1].PlyVal);

                               TabPPCRep oPPCRep = new TabPPCRep();
                               oPPCRep.Pk_Material = Temp1[1].Pk_Materail;
                               oPPCRep.Name = Temp1[1].Name;
                               oPPCRep.DeliveryDate = Temp1[1].DeliveryDate;

                               oPPCRep.CustomerName = Temp1[1].CustomerName;
                               oPPCRep.Box_Name = Temp1[1].Box_Name;
                               oPPCRep.PName = Temp1[1].PName;

                               oPPCRep.Length = Temp1[1].Length;
                               oPPCRep.Width = Temp1[1].Width;
                               oPPCRep.Height = Temp1[1].Height;

                               oPPCRep.Quantity = Temp1[1].Quantity;
                               oPPCRep.Pk_BoxID = Temp1[1].Pk_BoxID;
                               oPPCRep.PlyVal = Temp1[1].PlyVal;

                               oPPCRep.Deckle = Temp1[1].Deckle;
                               oPPCRep.JCDeckle = Temp1[1].JCDeckle;
                               oPPCRep.Ups = Temp1[1].Ups;
                               oPPCRep.CL = Temp1[1].CL;
                               oPPCRep.FluteName = Temp1[1].FluteName.Trim();
                               oPPCRep.Weight = Temp1[1].Weight;


                               List<Vw_BoxDet> Temp2 = new List<Vw_BoxDet>();
                               Temp2 = dc.Vw_BoxDet.Where(p => p.Pk_BoxID == CPosition).OrderBy(p => p.Pk_LayerID).ToList();
                               var LCount1 = cList.Count();


                               var Fid = 1;
                               //for (j = 0; j < LCount1; j++)
                               //{

                               for (k = 0; k < PlyCount1; k++)
                               {
                                   if (Fid == 1)
                                   {

                                       oPPCRep.gsm1 = Convert.ToInt32(Temp2[k].GSM);
                                       oPPCRep.BF1 = Convert.ToInt32(Temp2[k].BF);
                                       oPPCRep.WT1 = Convert.ToDecimal(Temp2[k].LayerWt);
                                   }
                                   else if (Fid == 2)
                                   {
                                       oPPCRep.gsm2 = Convert.ToInt32(Temp2[k].GSM);
                                       oPPCRep.BF2 = Convert.ToInt32(Temp2[k].BF);
                                       oPPCRep.WT2 = Convert.ToDecimal(Temp2[k].LayerWt);
                                   }
                                   else if (Fid == 3)
                                   {
                                       oPPCRep.gsm3 = Convert.ToInt32(Temp2[k].GSM);
                                       oPPCRep.BF3 = Convert.ToInt32(Temp2[k].BF);
                                       oPPCRep.WT3 = Convert.ToDecimal(Temp2[k].LayerWt);
                                   }
                                   else if (Fid == 4)
                                   {
                                       oPPCRep.gsm4 = Convert.ToInt32(Temp2[k].GSM);
                                       oPPCRep.BF4 = Convert.ToInt32(Temp2[k].BF);
                                       oPPCRep.WT4 = Convert.ToDecimal(Temp2[k].LayerWt);
                                   }
                                   else if (Fid == 5)
                                   {
                                       oPPCRep.gsm5 = Convert.ToInt32(Temp2[k].GSM);
                                       oPPCRep.BF5 = Convert.ToInt32(Temp2[k].BF);
                                       oPPCRep.WT5 = Convert.ToDecimal(Temp2[k].LayerWt);
                                   }
                                   else if (Fid == 6)
                                   {
                                       oPPCRep.gsm6 = Convert.ToInt32(Temp2[k].GSM);
                                       oPPCRep.BF6 = Convert.ToInt32(Temp2[k].BF);
                                       oPPCRep.WT6 = Convert.ToDecimal(Temp2[k].LayerWt);
                                   }
                                   else if (Fid == 7)
                                   {
                                       oPPCRep.gsm7 = Convert.ToInt32(Temp2[k].GSM);
                                       oPPCRep.BF7 = Convert.ToInt32(Temp2[k].BF);
                                       oPPCRep.WT7 = Convert.ToDecimal(Temp2[k].LayerWt);
                                   }
                                   else if (Fid == 8)
                                   {
                                       oPPCRep.gsm8 = Convert.ToInt32(Temp2[k].GSM);
                                       oPPCRep.BF8 = Convert.ToInt32(Temp2[k].BF);
                                       oPPCRep.WT8 = Convert.ToDecimal(Temp2[k].LayerWt);
                                   }
                                   else if (Fid == 9)
                                   {
                                       oPPCRep.gsm9 = Convert.ToInt32(Temp2[k].GSM);
                                       oPPCRep.BF9 = Convert.ToInt32(Temp2[k].BF);
                                       oPPCRep.WT9 = Convert.ToDecimal(Temp2[k].LayerWt);
                                   }
                                   //oPPCRep.gsm2 =  Convert.ToInt32(Temp2[j].GSM);
                                   //oPPCRep.gsm3 = Convert.ToInt32(Temp2[j].GSM);
                                   Fid = Fid + 1;
                                   //oPPCRep.gsm4 = Convert.ToInt32(Temp2[j].GSM);
                                   if (k > (PlyCount1))
                                   {

                                       Fid = 1;
                                       break;

                                   }
                               }
                               _oEntities.AddToTabPPCRep(oPPCRep);
                               _oEntities.SaveChanges();



                               List<TabPPCRep> BillList1 = new List<TabPPCRep>();
                               BillList1 = dc.TabPPCRep.Select(x => x).OfType<TabPPCRep>().ToList();

                               if (BillList1.Count > 0)
                               {
                                   DataTable dt = new DataTable("Paper");
                                   //SELECT DeliveryDate, CustomerName, Box_Name, Length, Width,
                                   //                          Height, Quantity, Pk_BoxID, PlyVal,
                                   //                          Deckle, Ups, CL, FluteName, gsm1,
                                   //                          gsm2, gsm3, gsm4, gsm5, gsm6, 
                                   //                          gsm7, gsm8, gsm9, BF1, BF2,
                                   //                          BF3, BF4, BF5, BF6, BF7, 
                                   //                          BF8, BF9, WT1, WT2, WT3,
                                   //                          WT4, WT5, WT6, WT7, WT8,
                                   //                          WT9
                                   //FROM   BalajiLive.dbo.
                                   //ORDER BY Pk_BoxID



                                   dt.Columns.Add("DeliveryDate");
                                   dt.Columns.Add("CustomerName");
                                   dt.Columns.Add("Box_Name");
                                   dt.Columns.Add("Name");
                                   dt.Columns.Add("Length");
                                   dt.Columns.Add("Width");
                                   dt.Columns.Add("Height");
                                   dt.Columns.Add("Quantity");
                                   dt.Columns.Add("Pk_BoxID");
                                   dt.Columns.Add("PlyVal");
                                   dt.Columns.Add("Deckle");
                                   dt.Columns.Add("JCDeckle");
                                   dt.Columns.Add("Ups");
                                   dt.Columns.Add("CL");
                                   dt.Columns.Add("FluteName");
                                   dt.Columns.Add("gsm1");
                                   dt.Columns.Add("gsm2");
                                   dt.Columns.Add("gsm3");
                                   dt.Columns.Add("gsm4");
                                   dt.Columns.Add("gsm5");
                                   dt.Columns.Add("gsm6");
                                   dt.Columns.Add("gsm7");
                                   dt.Columns.Add("gsm8");
                                   dt.Columns.Add("gsm9");
                                   dt.Columns.Add("BF1");
                                   dt.Columns.Add("BF2");
                                   dt.Columns.Add("BF3");
                                   dt.Columns.Add("BF4");
                                   dt.Columns.Add("BF5");
                                   dt.Columns.Add("BF6");
                                   dt.Columns.Add("BF7");
                                   dt.Columns.Add("BF8");
                                   dt.Columns.Add("BF9");
                                   dt.Columns.Add("WT1");
                                   dt.Columns.Add("WT2");
                                   dt.Columns.Add("WT3");
                                   dt.Columns.Add("WT4");
                                   dt.Columns.Add("WT5");
                                   dt.Columns.Add("WT6");
                                   dt.Columns.Add("WT7");
                                   dt.Columns.Add("WT8");
                                   dt.Columns.Add("WT9");
                                   foreach (TabPPCRep entity in BillList1)
                                   {
                                       DataRow row = dt.NewRow();

                                       row["DeliveryDate"] = entity.DeliveryDate;
                                       row["CustomerName"] = entity.CustomerName;
                                       row["Box_Name"] = entity.Box_Name;
                                       row["Name"] = entity.Name;
                                       row["Width"] = entity.Width;
                                       row["Length"] = entity.Length;
                                       row["Quantity"] = entity.Quantity;


                                       row["Height"] = entity.Height;
                                       row["Pk_BoxID"] = entity.Pk_BoxID;
                                       row["PlyVal"] = entity.PlyVal;
                                       row["Deckle"] = entity.Deckle;
                                       row["JCDeckle"] = entity.JCDeckle;
                                       row["Ups"] = entity.Ups;
                                       row["CL"] = entity.CL;
                                       row["FluteName"] = entity.FluteName;


                                       row["gsm1"] = entity.gsm1;
                                       row["gsm2"] = entity.gsm2;
                                       row["gsm3"] = entity.gsm3;
                                       row["gsm4"] = entity.gsm4;
                                       row["gsm5"] = entity.gsm5;
                                       row["gsm6"] = entity.gsm6;
                                       row["gsm7"] = entity.gsm7;
                                       row["gsm8"] = entity.gsm8;
                                       row["gsm9"] = entity.gsm9;

                                       row["BF1"] = entity.BF1;
                                       row["BF2"] = entity.BF2;
                                       row["BF3"] = entity.BF3;
                                       row["BF4"] = entity.BF4;
                                       row["BF5"] = entity.BF5;
                                       row["BF6"] = entity.BF6;
                                       row["BF7"] = entity.BF7;
                                       row["BF8"] = entity.BF8;
                                       row["BF9"] = entity.BF9;

                                       row["WT1"] = entity.WT1;
                                       row["WT2"] = entity.WT2;
                                       row["WT3"] = entity.WT3;
                                       row["WT4"] = entity.WT4;
                                       row["WT5"] = entity.WT5;
                                       row["WT6"] = entity.WT6;
                                       row["WT7"] = entity.WT7;
                                       row["WT8"] = entity.WT8;
                                       row["WT9"] = entity.WT9;

                                       dt.Rows.Add(row);
                                   }
                              
                                   DataSet ds = new DataSet();
                                   ds.Tables.Add(dt);

                                   CogitoStreamline.Report.PPCv2 orp = new CogitoStreamline.Report.PPCv2();

                               orp.Load("@\\Report\\PPCv2.rpt");
                                   orp.SetDataSource(dt.DefaultView);


                            

                                    ReportDocument repDoc = orp;
                               


                                   ParameterFieldDefinitions crParameterFieldDefinitions4;
                                   ParameterFieldDefinition crParameterFieldDefinition4;
                                   ParameterValues crParameterValues4 = new ParameterValues();
                                   ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                                   crParameterDiscreteValue4.Value = Repfor;
                                   crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                                   crParameterFieldDefinition4 = crParameterFieldDefinitions4["period"];
                                   crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                                   crParameterValues4.Clear();
                                   crParameterValues4.Add(crParameterDiscreteValue4);
                                   crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);


                                   string pdfPath = Server.MapPath("~/ConvertPDF/" + "PPCv1" + ".pdf");
                                   FileInfo file = new FileInfo(pdfPath);
                                   if (file.Exists)
                                   {
                                       file.Delete();
                                   }
                                   var pd = new PrintDocument();


                                   orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                                   orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                                   DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                                   objDiskOpt.DiskFileName = pdfPath;

                                   orp.ExportOptions.DestinationOptions = objDiskOpt;
                                   orp.Export();

                               

}   }
                       }
                          
                       }

                       return null;
                   }
                   catch (Exception ex)
                   {
                       return null;
                       //  Response.Redirect("Index");D:\shantha\Billing\CogitoStreamline\CogitoStreamline\Controllers\CartonBillingController.cs
                   }
               }


               public JsonResult Print([System.Web.Http.FromBody]string data)
               {
                   try
                   {
                       data = data.TrimStart('=');
                       int i = 0;
                       Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                   //    DateTime SFrom = Convert.ToDateTime(oValues["FDate"]);

                       //DateTime STo = DateTime.ParseExact((oValues["TDate"].ToString(),"dd/mm/yyyy";

                       DateTime SFrom = Convert.ToDateTime(oValues["FDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                       
                       
                       
                       DateTime STo = Convert.ToDateTime(oValues["TDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

//                       Convert.ToDateTime(omValues["JDate"].ToString(),
//System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                       string[] a = oValues["FDate"].ToString().Split('/');
                       DateTime FromDate = DateTime.ParseExact(a[1] + '/' + a[0] + '/' + a[2] + " 12:00", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);
                       string SFromDVal;
                       var FD = SFrom.Day;
                       var FM = SFrom.ToString("MMM");
                       var FY = SFrom.Year;


                    
                        SFromDVal = FD.ToString() + '/' + FM.ToString() + '/' + FY.ToString();



                       var TD = STo.Day;
                       var TM = STo.ToString("MMM");
                       var TY = STo.Year;

                       string SToDVal;
                      SToDVal = TD.ToString() + '/' + TM.ToString() + '/' + TY.ToString();


                       int dDay = Convert.ToInt32(a[0]);
                       int dmonth = Convert.ToInt32(a[1]);
                       int dyear = Convert.ToInt32(a[2]);

                       string monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dmonth);
                       //string Mname=FromDate.ToString( "MMMM");   // this one will also work to get the month name

                       string Repfor = "PPC Between - " + " " + SFromDVal + " " + "- " + SToDVal + " ";
                       CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                      // SqlConnection con = new SqlConnection("Data Source=Ravi-PC;Initial Catalog=BalajiTesting;User ID=sa;");
                       SqlConnection con = new SqlConnection("data Source= sql5005.site4now.net;Initial Catalog=DB_A44946_V2Innovatives;User ID=DB_A44946_V2Innovatives_admin;password=admin123;");

                       SqlCommand cmd = new SqlCommand("USP1", con);
                       cmd.CommandType = CommandType.StoredProcedure;

                       if (FD.ToString().Length == 1 || FD <= 12 )
                       {
//&& SFrom.Month.ToString().Length == 1 || SFrom.Month<=12
                           //var SF = "0"+  SFrom.Month.ToString() + '/' + FD.ToString()+ '/' + FY.ToString();
                           //var ST = STo.Month.ToString() + '/' +  TD.ToString() + '/' + TY.ToString();


                           cmd.Parameters.Add("@SDate", SqlDbType.DateTime).Value = SFrom;
                           cmd.Parameters.Add("@EDate", SqlDbType.DateTime).Value = STo;

                       }
                       else
                       {
                           cmd.Parameters.Add("@SDate", SqlDbType.DateTime).Value = SFromDVal;
                           cmd.Parameters.Add("@EDate", SqlDbType.DateTime).Value = SToDVal;
                       }
                       con.Open();
                       cmd.ExecuteNonQuery();
                       con.Close();

                       List<tempPPC> BillList = new List<tempPPC>();


                       BillList = dc.tempPPC.Where(s => s.Pk_BoxID > 0).Select(s => s).OfType<tempPPC>().ToList();

                       var cList = BillList.Select(o => o.Pk_BoxID).Distinct();
                       int j = 0;
                       var PlyCount1 = 0;
                       int k = 0;
                       var LCount = cList.Count();
                       if (BillList.Count > 0)
                       {
                           for (i = 0; i < LCount; i++)
                           {
                               var CPosition = cList.ElementAtOrDefault(i);
                               List<tempPPC> Temp1 = new List<tempPPC>();
                               Temp1 = dc.tempPPC.Where(p => p.Pk_BoxID == CPosition).OrderBy(p => p.Pk_LayerID).ToList();
                               if (Temp1.Count > 1)
                               {
                                   PlyCount1 = Convert.ToInt32(Temp1[1].PlyVal);

                                   TabPPCRep oPPCRep = new TabPPCRep();
                                   oPPCRep.Pk_Material = Temp1[1].Pk_Materail;
                                   oPPCRep.Name = Temp1[1].Name;
                                   oPPCRep.DeliveryDate = Temp1[1].DeliveryDate;

                                   oPPCRep.CustomerName = Temp1[1].CustomerName;
                                   oPPCRep.Box_Name = Temp1[1].Box_Name;
                                   oPPCRep.PName = Temp1[1].PName;

                                   oPPCRep.Length = Temp1[1].Length;
                                   oPPCRep.Width = Temp1[1].Width;
                                   oPPCRep.Height = Temp1[1].Height;

                                   oPPCRep.Quantity = Temp1[1].Quantity;
                                   oPPCRep.Pk_BoxID = Temp1[1].Pk_BoxID;
                                   oPPCRep.PlyVal = Temp1[1].PlyVal;

                                   oPPCRep.Deckle = Temp1[1].Deckle;
                                   oPPCRep.JCDeckle = Temp1[1].JCDeckle;
                                   oPPCRep.Ups = Temp1[1].Ups;
                                   oPPCRep.CL = Temp1[1].CL;
                                   oPPCRep.FluteName = Temp1[1].FluteName.Trim();
                                   oPPCRep.Weight = Temp1[1].Weight;


                                   List<Vw_BoxDet> Temp2 = new List<Vw_BoxDet>();
                                   Temp2 = dc.Vw_BoxDet.Where(p => p.Pk_BoxID == CPosition).OrderBy(p => p.Pk_LayerID).ToList();
                                   var LCount1 = cList.Count();


                                   var Fid = 1;
                                   //for (j = 0; j < LCount1; j++)
                                   //{

                                   for (k = 0; k < PlyCount1; k++)
                                   {
                                       if (Fid == 1)
                                       {

                                           oPPCRep.gsm1 = Convert.ToInt32(Temp2[k].GSM);
                                           oPPCRep.BF1 = Convert.ToInt32(Temp2[k].BF);
                                           oPPCRep.WT1 = Convert.ToDecimal(Temp2[k].LayerWt);
                                       }
                                       else if (Fid == 2)
                                       {
                                           oPPCRep.gsm2 = Convert.ToInt32(Temp2[k].GSM);
                                           oPPCRep.BF2 = Convert.ToInt32(Temp2[k].BF);
                                           oPPCRep.WT2 = Convert.ToDecimal(Temp2[k].LayerWt);
                                       }
                                       else if (Fid == 3)
                                       {
                                           oPPCRep.gsm3 = Convert.ToInt32(Temp2[k].GSM);
                                           oPPCRep.BF3 = Convert.ToInt32(Temp2[k].BF);
                                           oPPCRep.WT3 = Convert.ToDecimal(Temp2[k].LayerWt);
                                       }
                                       else if (Fid == 4)
                                       {
                                           oPPCRep.gsm4 = Convert.ToInt32(Temp2[k].GSM);
                                           oPPCRep.BF4 = Convert.ToInt32(Temp2[k].BF);
                                           oPPCRep.WT4 = Convert.ToDecimal(Temp2[k].LayerWt);
                                       }
                                       else if (Fid == 5)
                                       {
                                           oPPCRep.gsm5 = Convert.ToInt32(Temp2[k].GSM);
                                           oPPCRep.BF5 = Convert.ToInt32(Temp2[k].BF);
                                           oPPCRep.WT5 = Convert.ToDecimal(Temp2[k].LayerWt);
                                       }
                                       else if (Fid == 6)
                                       {
                                           oPPCRep.gsm6 = Convert.ToInt32(Temp2[k].GSM);
                                           oPPCRep.BF6 = Convert.ToInt32(Temp2[k].BF);
                                           oPPCRep.WT6 = Convert.ToDecimal(Temp2[k].LayerWt);
                                       }
                                       else if (Fid == 7)
                                       {
                                           oPPCRep.gsm7 = Convert.ToInt32(Temp2[k].GSM);
                                           oPPCRep.BF7 = Convert.ToInt32(Temp2[k].BF);
                                           oPPCRep.WT7 = Convert.ToDecimal(Temp2[k].LayerWt);
                                       }
                                       else if (Fid == 8)
                                       {
                                           oPPCRep.gsm8 = Convert.ToInt32(Temp2[k].GSM);
                                           oPPCRep.BF8 = Convert.ToInt32(Temp2[k].BF);
                                           oPPCRep.WT8 = Convert.ToDecimal(Temp2[k].LayerWt);
                                       }
                                       else if (Fid == 9)
                                       {
                                           oPPCRep.gsm9 = Convert.ToInt32(Temp2[k].GSM);
                                           oPPCRep.BF9 = Convert.ToInt32(Temp2[k].BF);
                                           oPPCRep.WT9 = Convert.ToDecimal(Temp2[k].LayerWt);
                                       }
                                       //oPPCRep.gsm2 =  Convert.ToInt32(Temp2[j].GSM);
                                       //oPPCRep.gsm3 = Convert.ToInt32(Temp2[j].GSM);
                                       Fid = Fid + 1;
                                       //oPPCRep.gsm4 = Convert.ToInt32(Temp2[j].GSM);
                                       if (k > (PlyCount1))
                                       {

                                           Fid = 1;
                                           break;

                                       }
                                   }
                                   _oEntities.AddToTabPPCRep(oPPCRep);
                                   _oEntities.SaveChanges();



                                   List<TabPPCRep> BillList1 = new List<TabPPCRep>();
                                   BillList1 = dc.TabPPCRep.Select(x => x).OfType<TabPPCRep>().ToList();

                                   if (BillList1.Count > 0)
                                   {
                                       DataTable dt = new DataTable("Paper");
                                       //SELECT DeliveryDate, CustomerName, Box_Name, Length, Width,
                                       //                          Height, Quantity, Pk_BoxID, PlyVal,
                                       //                          Deckle, Ups, CL, FluteName, gsm1,
                                       //                          gsm2, gsm3, gsm4, gsm5, gsm6, 
                                       //                          gsm7, gsm8, gsm9, BF1, BF2,
                                       //                          BF3, BF4, BF5, BF6, BF7, 
                                       //                          BF8, BF9, WT1, WT2, WT3,
                                       //                          WT4, WT5, WT6, WT7, WT8,
                                       //                          WT9
                                       //FROM   BalajiLive.dbo.
                                       //ORDER BY Pk_BoxID



                                       dt.Columns.Add("DeliveryDate");
                                       dt.Columns.Add("CustomerName");
                                       dt.Columns.Add("Box_Name");
                                       dt.Columns.Add("Name");
                                       dt.Columns.Add("Length");
                                       dt.Columns.Add("Width");
                                       dt.Columns.Add("Height");
                                       dt.Columns.Add("Quantity");
                                       dt.Columns.Add("Pk_BoxID");
                                       dt.Columns.Add("PlyVal");
                                       dt.Columns.Add("Deckle");
                                       dt.Columns.Add("JCDeckle");
                                       dt.Columns.Add("Ups");
                                       dt.Columns.Add("CL");
                                       dt.Columns.Add("FluteName");
                                       dt.Columns.Add("gsm1");
                                       dt.Columns.Add("gsm2");
                                       dt.Columns.Add("gsm3");
                                       dt.Columns.Add("gsm4");
                                       dt.Columns.Add("gsm5");
                                       dt.Columns.Add("gsm6");
                                       dt.Columns.Add("gsm7");
                                       dt.Columns.Add("gsm8");
                                       dt.Columns.Add("gsm9");
                                       dt.Columns.Add("BF1");
                                       dt.Columns.Add("BF2");
                                       dt.Columns.Add("BF3");
                                       dt.Columns.Add("BF4");
                                       dt.Columns.Add("BF5");
                                       dt.Columns.Add("BF6");
                                       dt.Columns.Add("BF7");
                                       dt.Columns.Add("BF8");
                                       dt.Columns.Add("BF9");
                                       dt.Columns.Add("WT1");
                                       dt.Columns.Add("WT2");
                                       dt.Columns.Add("WT3");
                                       dt.Columns.Add("WT4");
                                       dt.Columns.Add("WT5");
                                       dt.Columns.Add("WT6");
                                       dt.Columns.Add("WT7");
                                       dt.Columns.Add("WT8");
                                       dt.Columns.Add("WT9");
                                       foreach (TabPPCRep entity in BillList1)
                                       {
                                           DataRow row = dt.NewRow();

                                           row["DeliveryDate"] = entity.DeliveryDate;
                                           row["CustomerName"] = entity.CustomerName;
                                           row["Box_Name"] = entity.Box_Name;
                                           row["Name"] = entity.Name;
                                           row["Width"] = entity.Width;
                                           row["Length"] = entity.Length;
                                           row["Quantity"] = entity.Quantity;


                                           row["Height"] = entity.Height;
                                           row["Pk_BoxID"] = entity.Pk_BoxID;
                                           row["PlyVal"] = entity.PlyVal;
                                           row["Deckle"] = entity.Deckle;
                                           row["JCDeckle"] = entity.JCDeckle;
                                           row["Ups"] = entity.Ups;
                                           row["CL"] = entity.CL;
                                           row["FluteName"] = entity.FluteName;


                                           row["gsm1"] = entity.gsm1;
                                           row["gsm2"] = entity.gsm2;
                                           row["gsm3"] = entity.gsm3;
                                           row["gsm4"] = entity.gsm4;
                                           row["gsm5"] = entity.gsm5;
                                           row["gsm6"] = entity.gsm6;
                                           row["gsm7"] = entity.gsm7;
                                           row["gsm8"] = entity.gsm8;
                                           row["gsm9"] = entity.gsm9;

                                           row["BF1"] = entity.BF1;
                                           row["BF2"] = entity.BF2;
                                           row["BF3"] = entity.BF3;
                                           row["BF4"] = entity.BF4;
                                           row["BF5"] = entity.BF5;
                                           row["BF6"] = entity.BF6;
                                           row["BF7"] = entity.BF7;
                                           row["BF8"] = entity.BF8;
                                           row["BF9"] = entity.BF9;

                                           row["WT1"] = entity.WT1;
                                           row["WT2"] = entity.WT2;
                                           row["WT3"] = entity.WT3;
                                           row["WT4"] = entity.WT4;
                                           row["WT5"] = entity.WT5;
                                           row["WT6"] = entity.WT6;
                                           row["WT7"] = entity.WT7;
                                           row["WT8"] = entity.WT8;
                                           row["WT9"] = entity.WT9;

                                           dt.Rows.Add(row);
                                       }

                                       DataSet ds = new DataSet();
                                       ds.Tables.Add(dt);

                                       CogitoStreamline.Report.PPCv1 orp = new CogitoStreamline.Report.PPCv1();

                                       orp.Load("@\\Report\\PPCv1.rpt");
                                       orp.SetDataSource(dt.DefaultView);




                                       ReportDocument repDoc = orp;



                                       ParameterFieldDefinitions crParameterFieldDefinitions4;
                                       ParameterFieldDefinition crParameterFieldDefinition4;
                                       ParameterValues crParameterValues4 = new ParameterValues();
                                       ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                                       crParameterDiscreteValue4.Value = Repfor;
                                       crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                                       crParameterFieldDefinition4 = crParameterFieldDefinitions4["period"];
                                       crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                                       crParameterValues4.Clear();
                                       crParameterValues4.Add(crParameterDiscreteValue4);
                                       crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);


                                       Response.Buffer = false;
                                       Response.ClearContent();
                                       Response.ClearHeaders(); 

                                       string pdfPath = Server.MapPath("~/ConvertPDF/" + "PPCv1" + ".pdf");
                                       FileInfo file = new FileInfo(pdfPath);
                                       if (file.Exists)
                                       {
                                           file.Delete();
                                       }
                                       var pd = new PrintDocument();


                                       orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                                       orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                                       DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                                       objDiskOpt.DiskFileName = pdfPath;

                                       orp.ExportOptions.DestinationOptions = objDiskOpt;
                                       orp.Export();



                                   }
                               }
                           }

                       }

                       return null;
                   }
                   catch (Exception ex)
                   {
                       return Json(new { Result = "ERROR", Message = ex.Message + "INCOMING DATA IS " + data });
                   }
               }


               public JsonResult WIPStat([System.Web.Http.FromBody]string data)
               {
                   try
                   {
                       data = data.TrimStart('=');
                       int i = 0;

                        List<Vw_JobProcessSteps> BillList = new List<Vw_JobProcessSteps>();
                       CogitoStreamLineEntities dc = new CogitoStreamLineEntities();
                         Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                       
                       DateTime SFrom = Convert.ToDateTime(oValues["FDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);



                       DateTime STo = Convert.ToDateTime(oValues["TDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       //                       Convert.ToDateTime(omValues["JDate"].ToString(),
                       //System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                       string[] a = oValues["FDate"].ToString().Split('/');
                       DateTime FromDate = DateTime.ParseExact(a[1] + '/' + a[0] + '/' + a[2] + " 12:00", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);
                       string SFromDVal;
                       var FD = SFrom.Day;
                       var FM = SFrom.ToString("MMM");
                       var FY = SFrom.Year;


                       
                       SFromDVal =   FD.ToString() + '/'+FM.ToString()  +'/' + FY.ToString();  
                       


                       var TD = STo.Day;
                       var TM = SFrom.ToString("MMM");
                       var TY = STo.Year;

                       string SToDVal;
                       SToDVal = TD.ToString() + '/' + TM.ToString() + '/' + TY.ToString(); 
                     


                       int dDay = Convert.ToInt32(a[0]);
                       int dmonth = Convert.ToInt32(a[1]);
                       int dyear = Convert.ToInt32(a[2]);

                       string monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dmonth);
                       //string Mname=FromDate.ToString( "MMMM");   // this one will also work to get the month name

                       string Repfor = "Work In Progress of Job Cards Between - " + " " + SFromDVal + " " + "- " + SToDVal + " ";
                  
                       
                       
 //                      SELECT Vw_JobProcessSteps.Pk_JobCardID, Vw_JobProcessSteps.Pk_ID, Vw_JobProcessSteps.ProcessName, Vw_JobProcessSteps.ProcessDate,
 //                      Vw_JobProcessSteps.PQuantity, Vw_JobProcessSteps.JDate, Vw_JobProcessSteps.CustomerName, Vw_JobProcessSteps.Name,
 //                      Vw_JobProcessSteps.PName, Vw_JobProcessSteps.Fk_Order, Vw_JobProcessSteps.TotalQty
 //FROM   BalajiTesting.dbo.Vw_JobProcessSteps Vw_JobProcessSteps
 //ORDER BY Vw_JobProcessSteps.Pk_JobCardID, Vw_JobProcessSteps.Pk_ID




                    
                         BillList = dc.Vw_JobProcessSteps.Where(x => x.JDate >= SFrom && x.JDate <= STo && x.Fk_Status != 11).Select(x => x).OfType<Vw_JobProcessSteps>().ToList();

                       if (BillList.Count > 0)
                       {
                           DataTable dt = new DataTable("JobCard");

                           dt.Columns.Add("CustomerName");
                           dt.Columns.Add("JDate");
                           dt.Columns.Add("Pk_ID");
                           dt.Columns.Add("Fk_Order");
                           dt.Columns.Add("PName");
                           dt.Columns.Add("TotalQty");
                           dt.Columns.Add("Pk_JobCardID");
                           dt.Columns.Add("ProcessName");
                           dt.Columns.Add("ProcessDate");
                           dt.Columns.Add("PQuantity");
                           dt.Columns.Add("Name");


                           foreach (Vw_JobProcessSteps entity in BillList)
                           {
                               DataRow row = dt.NewRow();

                               row["CustomerName"] = entity.CustomerName;
                               row["JDate"] = entity.JDate;
                               row["Pk_ID"] = entity.Pk_ID;
                               row["PName"] = entity.PName;
                               row["Pk_JobCardID"] = entity.Pk_JobCardID;
                               row["Fk_Order"] = entity.Fk_Order;
                               row["TotalQty"] = entity.TotalQty;

                               row["ProcessName"] = entity.ProcessName;
                               row["ProcessDate"] = entity.ProcessDate;
                               row["PQuantity"] = entity.PQuantity;
                               row["Name"] = entity.Name;

                               dt.Rows.Add(row);

                           }

                           DataSet ds = new DataSet();
                           ds.Tables.Add(dt);
                           CogitoStreamline.Report.DailyProduction orp = new CogitoStreamline.Report.DailyProduction();

                           orp.Load("@\\Report\\DailyProduction.rpt");
                           orp.SetDataSource(dt.DefaultView);



                           ReportDocument repDoc = orp;

                           ParameterFieldDefinitions crParameterFieldDefinitions4;
                           ParameterFieldDefinition crParameterFieldDefinition4;
                           ParameterValues crParameterValues4 = new ParameterValues();
                           ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                           crParameterDiscreteValue4.Value = Repfor;
                           crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                           crParameterFieldDefinition4 = crParameterFieldDefinitions4["period"];
                           crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                           crParameterValues4.Clear();
                           crParameterValues4.Add(crParameterDiscreteValue4);
                           crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);


                           Response.Buffer = false;
                           Response.ClearContent();
                           Response.ClearHeaders(); 


                           string pdfPath = Server.MapPath("~/ConvertPDF/" + "WorkInProgress" + ".pdf");
                           FileInfo file = new FileInfo(pdfPath);
                           if (file.Exists)
                           {
                               file.Delete();
                           }
                           var pd = new PrintDocument();


                           orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                           objDiskOpt.DiskFileName = pdfPath;

                           orp.ExportOptions.DestinationOptions = objDiskOpt;
                           orp.Export();

                       }
                       return null;
                           
                   }
                   catch (Exception ex)
                   {
                       return Json(new { Result = "ERROR", Message = ex.Message + "INCOMING DATA IS " + data });
                   }
               }

               public JsonResult PaperConsumed([System.Web.Http.FromBody]string data)
               {
                   try
                   {
                       data = data.TrimStart('=');
                       int i = 0;

                       List<Vw_PaperConsumption> BillList = new List<Vw_PaperConsumption>();
                       CogitoStreamLineEntities dc = new CogitoStreamLineEntities();
                       Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

                       DateTime SFrom = Convert.ToDateTime(oValues["FDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);



                       DateTime STo = Convert.ToDateTime(oValues["TDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       //                       Convert.ToDateTime(omValues["JDate"].ToString(),
                       //System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                       string[] a = oValues["FDate"].ToString().Split('/');
                       DateTime FromDate = DateTime.ParseExact(a[1] + '/' + a[0] + '/' + a[2] + " 12:00", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);
                       string SFromDVal;
                       var FD = SFrom.Day;
                       var FM = SFrom.ToString("MMM");
                       var FY = SFrom.Year;


                       //if (FD > 12)
                       //{ SFromDVal = FM.ToString() + '/' + FD.ToString() + '/' + FY.ToString(); }
                       //else
                           SFromDVal = FD.ToString() + '/' + FM.ToString() + '/' + FY.ToString();



                       var TD = STo.Day;
                       var TM = STo.ToString("MMM");
                       var TY = STo.Year;

                       string SToDVal;
                       //if (TD > 12)
                       //{ SToDVal = TD.ToString() + '/' + TM.ToString() + '/' + TY.ToString(); }
                       //else
                           SToDVal = TD.ToString() + '/' +  TM.ToString() + '/' +TY.ToString();


                       int dDay = Convert.ToInt32(a[0]);
                       int dmonth = Convert.ToInt32(a[1]);
                       int dyear = Convert.ToInt32(a[2]);

                       string monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dmonth);
                       //string Mname=FromDate.ToString( "MMMM");   // this one will also work to get the month name

                       string Repfor = "Paper Consumption of Job Cards Between - " + " " + SFromDVal + " " + "- " + SToDVal + " ";


                       BillList = dc.Vw_PaperConsumption.Where(x => x.JDate >= SFrom && x.JDate <= STo).Select(x => x).OfType<Vw_PaperConsumption>().ToList();

                       if (BillList.Count > 0)
                       {
                           DataTable dt = new DataTable("JobCard");

                           dt.Columns.Add("CustomerName");
                           dt.Columns.Add("JDate");                       
                           dt.Columns.Add("Fk_Order");
                           dt.Columns.Add("RollNo");
                           dt.Columns.Add("Quantity");
                           dt.Columns.Add("Pk_JobCardID");
                           dt.Columns.Add("ReturnQuantity");
                           dt.Columns.Add("OrderDate");



                           foreach (Vw_PaperConsumption entity in BillList)
                           {
                               DataRow row = dt.NewRow();

                               row["CustomerName"] = entity.CustomerName;
                               row["JDate"] = entity.JDate;
                               row["RollNo"] = entity.RollNo;
                               row["Quantity"] = entity.Quantity;
                               row["Pk_JobCardID"] = entity.Pk_JobCardID;
                               row["Fk_Order"] = entity.Fk_Order;
                               row["ReturnQuantity"] = entity.ReturnQuantity;
                               row["OrderDate"] = entity.OrderDate;
                                                           

                               dt.Rows.Add(row);

                           }

                           DataSet ds = new DataSet();
                           ds.Tables.Add(dt);
                           CogitoStreamline.Report.PaperConsumption orp = new CogitoStreamline.Report.PaperConsumption();

                           orp.Load("@\\Report\\PaperConsumption.rpt");
                           orp.SetDataSource(dt.DefaultView);



                           ReportDocument repDoc = orp;

                           ParameterFieldDefinitions crParameterFieldDefinitions4;
                           ParameterFieldDefinition crParameterFieldDefinition4;
                           ParameterValues crParameterValues4 = new ParameterValues();
                           ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                           crParameterDiscreteValue4.Value = Repfor;
                           crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                           crParameterFieldDefinition4 = crParameterFieldDefinitions4["period"];
                           crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                           crParameterValues4.Clear();
                           crParameterValues4.Add(crParameterDiscreteValue4);
                           crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);
                           Response.Buffer = false;
                           Response.ClearContent();
                           Response.ClearHeaders(); 
                           string pdfPath = Server.MapPath("~/ConvertPDF/" + "PaperConsumed" + ".pdf");
                           FileInfo file = new FileInfo(pdfPath);
                           if (file.Exists)
                           {
                               file.Delete();
                           }
                           var pd = new PrintDocument();


                           orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                           objDiskOpt.DiskFileName = pdfPath;

                           orp.ExportOptions.DestinationOptions = objDiskOpt;
                           orp.Export();

                       }
                       return null;

                   }
                   catch (Exception ex)
                   {
                       return Json(new { Result = "ERROR", Message = ex.Message + "INCOMING DATA IS " + data });
                   }
               }



               public JsonResult PrintPPCRep([System.Web.Http.FromBody]string data)
               {
                   try
                   {
                       data = data.TrimStart('=');
                       int i = 0;
                       
                       Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                       //var StrVal = int.TryParse(oValues["QStr"], out StrValue);
                       var StrVal = oValues["QStr"];
                       //DateTime SFrom = Convert.ToDateTime(oValues["FDate"]);
                       //DateTime STo = Convert.ToDateTime(oValues["TDate"]);

                       //string[] a = oValues["FDate"].ToString().Split('/');
                       //DateTime FromDate = DateTime.ParseExact(a[1] + '/' + a[0] + '/' + a[2] + " 12:00", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);

                       //var FD = SFrom.Day;
                       //var FM = SFrom.Month;
                       //var FY = SFrom.Year;

                       //var SFromDVal = FD.ToString() + '/' + FM.ToString() + '/' + FY.ToString();



                       //var TD = STo.Day;
                       //var TM = STo.Month;
                       //var TY = STo.Year;

                       //var SToDVal = TD.ToString() + '/' + TM.ToString() + '/' + TY.ToString();


                       //int dDay = Convert.ToInt32(a[0]);
                       //int dmonth = Convert.ToInt32(a[1]);
                       //int dyear = Convert.ToInt32(a[2]);

                       //string monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dmonth);
                       //string Mname=FromDate.ToString( "MMMM");   // this one will also work to get the month name

                       //string Repfor = "PPC Between - " + " " + SFromDVal + " " + "- " + SToDVal + " ";
                       CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                       SqlConnection con = new SqlConnection("Data Source=SQL5005.site4now.net;Initial Catalog=DB_A44946_V2Innovatives;User ID=DB_A44946_V2Innovatives_admin;password=admin123;");
                       //SqlConnection con = new SqlConnection("data Source= sql5005.site4now.net;Initial Catalog=DB_A44946_V2Innovatives;User ID=DB_A44946_V2Innovatives_admin;password=admin123;");

                       SqlCommand cmd = new SqlCommand("USP11", con);
                       cmd.CommandType = CommandType.StoredProcedure;


                       //cmd.Parameters.Add("@SDate", SqlDbType.DateTime).Value = SFromDVal;
                       //cmd.Parameters.Add("@EDate", SqlDbType.DateTime).Value = SToDVal;
                       cmd.Parameters.Add("@StrStr", SqlDbType.Char).Value = StrVal ;
                       con.Open();
                       cmd.ExecuteNonQuery();
                       con.Close();

                       List<tempPPC> BillList = new List<tempPPC>();


                       BillList = dc.tempPPC.Where(s => s.Pk_BoxID > 0).Select(s => s).OfType<tempPPC>().ToList();

                       var cList = BillList.Select(o => o.Pk_BoxID).Distinct();
                       int j = 0;
                       var PlyCount1 = 0;
                       int k = 0;
                       var LCount = cList.Count();
                       if (BillList.Count > 0)
                       {
                           for (i = 0; i < LCount; i++)
                           {
                               var CPosition = cList.ElementAtOrDefault(i);
                               List<tempPPC> Temp1 = new List<tempPPC>();
                               Temp1 = dc.tempPPC.Where(p => p.Pk_BoxID == CPosition).OrderBy(p => p.Pk_LayerID).ToList();
                               if (Temp1.Count > 1)
                               {
                                   PlyCount1 = Convert.ToInt32(Temp1[1].PlyVal);

                                   TabPPCRep oPPCRep = new TabPPCRep();
                                   oPPCRep.Pk_Material = Temp1[1].Pk_Materail;
                                   oPPCRep.Name = Temp1[1].Name;
                                   oPPCRep.DeliveryDate = Temp1[1].DeliveryDate;

                                   oPPCRep.CustomerName = Temp1[1].CustomerName;
                                   oPPCRep.Box_Name = Temp1[1].Box_Name;
                                   oPPCRep.PName = Temp1[1].PName;

                                   oPPCRep.Length = Temp1[1].Length;
                                   oPPCRep.Width = Temp1[1].Width;
                                   oPPCRep.Height = Temp1[1].Height;

                                   oPPCRep.Quantity = Temp1[1].Quantity;
                                   oPPCRep.Pk_BoxID = Temp1[1].Pk_BoxID;
                                   oPPCRep.PlyVal = Temp1[1].PlyVal;

                                   oPPCRep.Deckle = Temp1[1].Deckle;
                                   oPPCRep.JCDeckle = Temp1[1].JCDeckle;
                                   oPPCRep.Ups = Temp1[1].Ups;
                                   oPPCRep.CL = Temp1[1].CL;
                                   oPPCRep.FluteName = Temp1[1].FluteName.Trim();
                                   oPPCRep.Weight = Temp1[1].Weight;


                                   List<Vw_BoxDet> Temp2 = new List<Vw_BoxDet>();
                                   Temp2 = dc.Vw_BoxDet.Where(p => p.Pk_BoxID == CPosition).OrderBy(p => p.Pk_LayerID).ToList();
                                   var LCount1 = cList.Count();


                                   var Fid = 1;
                                   //for (j = 0; j < LCount1; j++)
                                   //{

                                   for (k = 0; k < PlyCount1; k++)
                                   {
                                       if (Fid == 1)
                                       {

                                           oPPCRep.gsm1 = Convert.ToInt32(Temp2[k].GSM);
                                           oPPCRep.BF1 = Convert.ToInt32(Temp2[k].BF);
                                           oPPCRep.WT1 = Convert.ToDecimal(Temp2[k].LayerWt);
                                       }
                                       else if (Fid == 2)
                                       {
                                           oPPCRep.gsm2 = Convert.ToInt32(Temp2[k].GSM);
                                           oPPCRep.BF2 = Convert.ToInt32(Temp2[k].BF);
                                           oPPCRep.WT2 = Convert.ToDecimal(Temp2[k].LayerWt);
                                       }
                                       else if (Fid == 3)
                                       {
                                           oPPCRep.gsm3 = Convert.ToInt32(Temp2[k].GSM);
                                           oPPCRep.BF3 = Convert.ToInt32(Temp2[k].BF);
                                           oPPCRep.WT3 = Convert.ToDecimal(Temp2[k].LayerWt);
                                       }
                                       else if (Fid == 4)
                                       {
                                           oPPCRep.gsm4 = Convert.ToInt32(Temp2[k].GSM);
                                           oPPCRep.BF4 = Convert.ToInt32(Temp2[k].BF);
                                           oPPCRep.WT4 = Convert.ToDecimal(Temp2[k].LayerWt);
                                       }
                                       else if (Fid == 5)
                                       {
                                           oPPCRep.gsm5 = Convert.ToInt32(Temp2[k].GSM);
                                           oPPCRep.BF5 = Convert.ToInt32(Temp2[k].BF);
                                           oPPCRep.WT5 = Convert.ToDecimal(Temp2[k].LayerWt);
                                       }
                                       else if (Fid == 6)
                                       {
                                           oPPCRep.gsm6 = Convert.ToInt32(Temp2[k].GSM);
                                           oPPCRep.BF6 = Convert.ToInt32(Temp2[k].BF);
                                           oPPCRep.WT6 = Convert.ToDecimal(Temp2[k].LayerWt);
                                       }
                                       else if (Fid == 7)
                                       {
                                           oPPCRep.gsm7 = Convert.ToInt32(Temp2[k].GSM);
                                           oPPCRep.BF7 = Convert.ToInt32(Temp2[k].BF);
                                           oPPCRep.WT7 = Convert.ToDecimal(Temp2[k].LayerWt);
                                       }
                                       else if (Fid == 8)
                                       {
                                           oPPCRep.gsm8 = Convert.ToInt32(Temp2[k].GSM);
                                           oPPCRep.BF8 = Convert.ToInt32(Temp2[k].BF);
                                           oPPCRep.WT8 = Convert.ToDecimal(Temp2[k].LayerWt);
                                       }
                                       else if (Fid == 9)
                                       {
                                           oPPCRep.gsm9 = Convert.ToInt32(Temp2[k].GSM);
                                           oPPCRep.BF9 = Convert.ToInt32(Temp2[k].BF);
                                           oPPCRep.WT9 = Convert.ToDecimal(Temp2[k].LayerWt);
                                       }
                                       //oPPCRep.gsm2 =  Convert.ToInt32(Temp2[j].GSM);
                                       //oPPCRep.gsm3 = Convert.ToInt32(Temp2[j].GSM);
                                       Fid = Fid + 1;
                                       //oPPCRep.gsm4 = Convert.ToInt32(Temp2[j].GSM);
                                       if (k > (PlyCount1))
                                       {

                                           Fid = 1;
                                           break;

                                       }
                                   }
                                   _oEntities.AddToTabPPCRep(oPPCRep);
                                   _oEntities.SaveChanges();



                                   List<TabPPCRep> BillList1 = new List<TabPPCRep>();
                                   BillList1 = dc.TabPPCRep.Select(x => x).OfType<TabPPCRep>().ToList();

                                   if (BillList1.Count > 0)
                                   {
                                       DataTable dt = new DataTable("Paper");
                                       //SELECT DeliveryDate, CustomerName, Box_Name, Length, Width,
                                       //                          Height, Quantity, Pk_BoxID, PlyVal,
                                       //                          Deckle, Ups, CL, FluteName, gsm1,
                                       //                          gsm2, gsm3, gsm4, gsm5, gsm6, 
                                       //                          gsm7, gsm8, gsm9, BF1, BF2,
                                       //                          BF3, BF4, BF5, BF6, BF7, 
                                       //                          BF8, BF9, WT1, WT2, WT3,
                                       //                          WT4, WT5, WT6, WT7, WT8,
                                       //                          WT9
                                       //FROM   BalajiLive.dbo.
                                       //ORDER BY Pk_BoxID



                                       dt.Columns.Add("DeliveryDate");
                                       dt.Columns.Add("CustomerName");
                                       dt.Columns.Add("Box_Name");
                                       dt.Columns.Add("Name");
                                       dt.Columns.Add("Length");
                                       dt.Columns.Add("Width");
                                       dt.Columns.Add("Height");
                                       dt.Columns.Add("Quantity");
                                       dt.Columns.Add("Pk_BoxID");
                                       dt.Columns.Add("PlyVal");
                                       dt.Columns.Add("Deckle");
                                       dt.Columns.Add("JCDeckle");
                                       dt.Columns.Add("Ups");
                                       dt.Columns.Add("CL");
                                       dt.Columns.Add("FluteName");
                                       dt.Columns.Add("gsm1");
                                       dt.Columns.Add("gsm2");
                                       dt.Columns.Add("gsm3");
                                       dt.Columns.Add("gsm4");
                                       dt.Columns.Add("gsm5");
                                       dt.Columns.Add("gsm6");
                                       dt.Columns.Add("gsm7");
                                       dt.Columns.Add("gsm8");
                                       dt.Columns.Add("gsm9");
                                       dt.Columns.Add("BF1");
                                       dt.Columns.Add("BF2");
                                       dt.Columns.Add("BF3");
                                       dt.Columns.Add("BF4");
                                       dt.Columns.Add("BF5");
                                       dt.Columns.Add("BF6");
                                       dt.Columns.Add("BF7");
                                       dt.Columns.Add("BF8");
                                       dt.Columns.Add("BF9");
                                       dt.Columns.Add("WT1");
                                       dt.Columns.Add("WT2");
                                       dt.Columns.Add("WT3");
                                       dt.Columns.Add("WT4");
                                       dt.Columns.Add("WT5");
                                       dt.Columns.Add("WT6");
                                       dt.Columns.Add("WT7");
                                       dt.Columns.Add("WT8");
                                       dt.Columns.Add("WT9");
                                       foreach (TabPPCRep entity in BillList1)
                                       {
                                           DataRow row = dt.NewRow();

                                           row["DeliveryDate"] = entity.DeliveryDate;
                                           row["CustomerName"] = entity.CustomerName;
                                           row["Box_Name"] = entity.Box_Name;
                                           row["Name"] = entity.Name;
                                           row["Width"] = entity.Width;
                                           row["Length"] = entity.Length;
                                           row["Quantity"] = entity.Quantity;


                                           row["Height"] = entity.Height;
                                           row["Pk_BoxID"] = entity.Pk_BoxID;
                                           row["PlyVal"] = entity.PlyVal;
                                           row["Deckle"] = entity.Deckle;
                                           row["JCDeckle"] = entity.JCDeckle;
                                           row["Ups"] = entity.Ups;
                                           row["CL"] = entity.CL;
                                           row["FluteName"] = entity.FluteName;


                                           row["gsm1"] = entity.gsm1;
                                           row["gsm2"] = entity.gsm2;
                                           row["gsm3"] = entity.gsm3;
                                           row["gsm4"] = entity.gsm4;
                                           row["gsm5"] = entity.gsm5;
                                           row["gsm6"] = entity.gsm6;
                                           row["gsm7"] = entity.gsm7;
                                           row["gsm8"] = entity.gsm8;
                                           row["gsm9"] = entity.gsm9;

                                           row["BF1"] = entity.BF1;
                                           row["BF2"] = entity.BF2;
                                           row["BF3"] = entity.BF3;
                                           row["BF4"] = entity.BF4;
                                           row["BF5"] = entity.BF5;
                                           row["BF6"] = entity.BF6;
                                           row["BF7"] = entity.BF7;
                                           row["BF8"] = entity.BF8;
                                           row["BF9"] = entity.BF9;

                                           row["WT1"] = entity.WT1;
                                           row["WT2"] = entity.WT2;
                                           row["WT3"] = entity.WT3;
                                           row["WT4"] = entity.WT4;
                                           row["WT5"] = entity.WT5;
                                           row["WT6"] = entity.WT6;
                                           row["WT7"] = entity.WT7;
                                           row["WT8"] = entity.WT8;
                                           row["WT9"] = entity.WT9;

                                           dt.Rows.Add(row);
                                       }

                                       DataSet ds = new DataSet();
                                       ds.Tables.Add(dt);

                                       CogitoStreamline.Report.PPCv2 orp = new CogitoStreamline.Report.PPCv2();

                                       orp.Load("@\\Report\\PPCv2.rpt");
                                       orp.SetDataSource(dt.DefaultView);




                                       ReportDocument repDoc = orp;



                                       //ParameterFieldDefinitions crParameterFieldDefinitions4;
                                       //ParameterFieldDefinition crParameterFieldDefinition4;
                                       //ParameterValues crParameterValues4 = new ParameterValues();
                                       //ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                                       //crParameterDiscreteValue4.Value = Repfor;
                                       //crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                                       //crParameterFieldDefinition4 = crParameterFieldDefinitions4["period"];
                                       //crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                                       //crParameterValues4.Clear();
                                       //crParameterValues4.Add(crParameterDiscreteValue4);
                                       //crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);
                                       Response.Buffer = false;
                                       Response.ClearContent();
                                       Response.ClearHeaders(); 

                                       string pdfPath = Server.MapPath("~/ConvertPDF/" + "PPCSelected" + ".pdf");
                                       FileInfo file = new FileInfo(pdfPath);
                                       if (file.Exists)
                                       {
                                           file.Delete();
                                       }
                                       var pd = new PrintDocument();


                                       orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                                       orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                                       DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                                       objDiskOpt.DiskFileName = pdfPath;

                                       orp.ExportOptions.DestinationOptions = objDiskOpt;
                                       orp.Export();



                                   }
                               }
                           }

                       }

                       return null;
                   }
                   catch (Exception ex)
                   {
                       return Json(new { Result = "ERROR", Message = ex.Message + "INCOMING DATA IS " + data });
                   }
               }

               public JsonResult Deliv_Sch([System.Web.Http.FromBody]string data)
               {
                   try
                   {

                                     //LensList = dc.Vw_PurchaseInwd.Where(x => x.PurDate >= SFrom && x.PurDate <= STo).Select(x => x).OfType<Vw_PurchaseInwd>().OrderBy(x => x.Pk_PurchaseID).ToList();
                       data = data.TrimStart('=');
                       int i = 0;

                       Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                       //DateTime SFrom = Convert.ToDateTime(oValues["FDate"]);
                       //DateTime STo = Convert.ToDateTime(oValues["TDate"]);

                       DateTime SFrom = Convert.ToDateTime(oValues["FDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);



                       DateTime STo = Convert.ToDateTime(oValues["TDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);


                       string[] a = oValues["FDate"].ToString().Split('/');
                       DateTime FromDate = DateTime.ParseExact(a[1] + '/' + a[0] + '/' + a[2] + " 12:00", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);

                       var FD = SFrom.Day;
                       var FM = SFrom.ToString("MMM");
                       var FY = SFrom.Year;

                      var SFromDVal = FD.ToString() + '/' + FM.ToString() + '/' + FY.ToString();



                      var TD = STo.Day;
                      var TM = STo.ToString("MMM");
                      var TY = STo.Year;

                      var SToDVal = TD.ToString() + '/' + TM.ToString() + '/' + TY.ToString();


                       int dDay = Convert.ToInt32(a[0]);
                       int dmonth = Convert.ToInt32(a[1]);
                       int dyear = Convert.ToInt32(a[2]);

                       string monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dmonth);
                       //string Mname=FromDate.ToString( "MMMM");   // this one will also work to get the month name

                       string Repfor = "Production Schedule List Between - " + " " + SFromDVal + " " + "- " + SToDVal + " ";

                       List<Vw_DeliveryScheduleReport> BillList = new List<Vw_DeliveryScheduleReport>();
                       CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                       //BillList = dc.Vw_DeliveryScheduleReport.Where(x => x.DeliveryDate.Month == dmonth && x.DeliveryDate.Year == dyear && x.DeliveryDate.Day == dDay).Select(x => x).OfType<Vw_DeliveryScheduleReport>().ToList();
                       BillList = dc.Vw_DeliveryScheduleReport.Where(x => x.DeliveryDate>=SFrom && x.DeliveryDate<=STo).OrderBy(x=>x.DeliveryDate).Select(x => x).OfType<Vw_DeliveryScheduleReport>().ToList();
                       if (BillList.Count > 0)
                       {
                           DataTable dt = new DataTable("DeliverySchedule");

                           dt.Columns.Add("Pk_Order");
                           dt.Columns.Add("OrderDate");
                           dt.Columns.Add("Name");
                           dt.Columns.Add("CustomerName");
                           dt.Columns.Add("OrdQty");
                           dt.Columns.Add("Quantity");
                           dt.Columns.Add("DeliveryDate");
                           dt.Columns.Add("Fk_BoxID");
                           dt.Columns.Add("PName");
                           dt.Columns.Add("Pk_DeliverySechedule");
                           foreach (Vw_DeliveryScheduleReport entity in BillList)
                           {
                               DataRow row = dt.NewRow();

                               row["Name"] = entity.Name;
                               row["Pk_Order"] = entity.Pk_Order;
                               row["OrderDate"] = entity.OrderDate;
                               row["CustomerName"] = entity.CustomerName;
                               row["OrdQty"] = entity.OrdQty;
                               row["Quantity"] = entity.Quantity;
                               row["DeliveryDate"] = entity.DeliveryDate;
                               row["Fk_BoxID"] = entity.Fk_BoxID;
                               row["PName"] = entity.PName;
                               row["Pk_DeliverySechedule"] = entity.Pk_DeliverySechedule;
                               dt.Rows.Add(row);

                           }

                           DataSet ds = new DataSet();
                           ds.Tables.Add(dt);
                           CogitoStreamline.Report.Prd_SchDay orp = new CogitoStreamline.Report.Prd_SchDay();

                           orp.Load("@\\Report\\Prd_SchDay.rpt");
                           orp.SetDataSource(dt.DefaultView);



                           ReportDocument repDoc = orp;


                           ParameterFieldDefinitions crParameterFieldDefinitions4;
                           ParameterFieldDefinition crParameterFieldDefinition4;
                           ParameterValues crParameterValues4 = new ParameterValues();
                           ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                           crParameterDiscreteValue4.Value = Repfor;
                           crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                           crParameterFieldDefinition4 = crParameterFieldDefinitions4["period"];
                           crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                           crParameterValues4.Clear();
                           crParameterValues4.Add(crParameterDiscreteValue4);
                           crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);

                           Response.Buffer = false;
                           Response.ClearContent();
                           Response.ClearHeaders(); 
                           string pdfPath = Server.MapPath("~/ConvertPDF/" + "Prod_Schedule" + ".pdf");
                           FileInfo file = new FileInfo(pdfPath);
                           if (file.Exists)
                           {
                               file.Delete();
                           }
                           var pd = new PrintDocument();


                           orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                           objDiskOpt.DiskFileName = pdfPath;

                           orp.ExportOptions.DestinationOptions = objDiskOpt;
                           orp.Export();

                       }
                       return null;
                   }
                   catch (Exception ex)
                   {
                       return null;
                       //  Response.Redirect("Index");D:\shantha\Billing\CogitoStreamline\CogitoStreamline\Controllers\CartonBillingController.cs
                   }
               }

               [HttpPost]
               public JsonResult EnquiryRep(string Pk_Enquiry = "", string EnquiryFromDate = "", string EnquiryToDate = "", string Customer = "", string CommunicationType = "", string State = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
               {
                   try
                   {
                       List<SearchParameter> oSearchParams = new List<SearchParameter>();
                       oSearchParams.Add(new SearchParameter("Pk_Enquiry", Pk_Enquiry));
                       oSearchParams.Add(new SearchParameter("EnquiryFromDate", EnquiryFromDate));
                       oSearchParams.Add(new SearchParameter("EnquiryToDate", EnquiryToDate));
                       oSearchParams.Add(new SearchParameter("Customer", Customer));
                       oSearchParams.Add(new SearchParameter("CommunicationType", CommunicationType));
                       oSearchParams.Add(new SearchParameter("State", State));
                       oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                       oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                       SearchResult oSearchResult = oEnq.SearchEnq(oSearchParams);

                       List<EntityObject> oEnquirysToDisplayObjects = oSearchResult.ListOfRecords;
                       List<eq_Enquiry> oEnquiryObjects = oEnquirysToDisplayObjects.Select(p => p).OfType<eq_Enquiry>().ToList();

                       //Create a anominious object here to break the circular reference
                       var oEnquiryToDisplay = oEnquiryObjects.Select(p => new
                       {
                           Pk_Enquiry = p.Pk_Enquiry,
                           CommunicationType = p.gen_Communication.Type,
                           Customer = p.gen_Customer.CustomerName,
                           Fk_Customer = p.Customer,
                           //Description = p.Description,
                           Date = DateTime.Parse(p.Date.ToString()).ToString("dd/MM/yyyy"),
                           //SpecialRequirements = p.SpecialRequirements,
                           Fk_Status = p.wfState.State,
                           //EqComments = p.EqComments

                       }).ToList();

                       return Json(new { Result = "OK", Records = oEnquiryToDisplay, TotalRecordCount = oSearchResult.RecordCount });
                   }
                   catch (Exception ex)
                   {
                       return Json(new { Result = "ERROR", Message = ex.Message });
                   }
               }




               public JsonResult IssueOfDay([System.Web.Http.FromBody]string data)
               {
                   try
                   {
                       Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

                       DateTime SFrom = Convert.ToDateTime(oValues["FDate"].ToString(),
     System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       DateTime STo = Convert.ToDateTime(oValues["TDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                       //DateTime SFrom = Convert.ToDateTime(oValues["From"]);
                     
                    //   DateTime STo = Convert.ToDateTime(oValues["To"]);
                       //LensList = dc.Vw_PurchaseInwd.Where(x => x.PurDate >= SFrom && x.PurDate <= STo).Select(x => x).OfType<Vw_PurchaseInwd>().OrderBy(x => x.Pk_PurchaseID).ToList();

                       string[] a = oValues["FDate"].ToString().Split('/');
                       DateTime FromDate = DateTime.ParseExact(a[1] + '/' + a[0] + '/' + a[2] + " 12:00", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);

                       string[] b = oValues["TDate"].ToString().Split('/');
                       DateTime ToDate = DateTime.ParseExact(b[1] + '/' + b[0] + '/' + b[2] + " 12:00", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);


                       var FD = SFrom.Day;
                       var FM = SFrom.ToString("MMM");
                       var FY = SFrom.Year;

                       var SFromDVal = FD.ToString() + '/' + FM.ToString() + '/' + FY.ToString();



                       var TD = STo.Day;
                       var TM = STo.ToString("MMM");
                       var TY = STo.Year;

                       var SToDVal = TD.ToString() + '/' + TM.ToString() + '/' + TY.ToString();


                       int dDay = Convert.ToInt32(a[0]);
                       int dmonth = Convert.ToInt32(a[1]);
                       int dyear = Convert.ToInt32(a[2]);

                       string monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dmonth);
                       //string Mname=FromDate.ToString( "MMMM");   // this one will also work to get the month name

                       string Repfor = "Issue List Between - " + " " + SFromDVal + " " + "- " + SToDVal + " ";

                       List<Vw_IssueDay> BillList = new List<Vw_IssueDay>();
                       CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                       BillList = dc.Vw_IssueDay.Where(x => x.IssueDate >= FromDate && x.IssueDate <= ToDate).Select(x => x).OfType<Vw_IssueDay>().ToList();

 //                       SELECT "Vw_IssueDay"."Pk_JobCardID", "Vw_IssueDay"."JDate", "Vw_IssueDay"."Fk_Material", "Vw_IssueDay"."RollNo", "Vw_IssueDay"."Name", "Vw_IssueDay"."Pk_MaterialIssueID", 
 //                      "Vw_IssueDay"."IssueDate", "Vw_IssueDay"."MillName", "Vw_IssueDay"."ColorName", "Vw_IssueDay"."IssQty", "Vw_IssueDay"."CustomerName", "Vw_IssueDay"."Pk_Order", 
 //                      "Vw_IssueDay"."OrderDate", "Vw_IssueDay"."DeliveryDate", "Vw_IssueDay"."ProdQty"
 //FROM   "Balaji"."dbo"."Vw_IssueDay" "Vw_IssueDay"


                       if (BillList.Count > 0)
                       {
                          
                           DataTable dt = new DataTable("Issue");

                           dt.Columns.Add("Pk_JobCardID");
                           dt.Columns.Add("JDate");
                           dt.Columns.Add("Name");
                           dt.Columns.Add("CustomerName");
                           dt.Columns.Add("Fk_Material");
                           //dt.Columns.Add("Pk_JobCardID");
                           dt.Columns.Add("RollNo");
                           dt.Columns.Add("Pk_MaterialIssueID");
                           dt.Columns.Add("IssueDate");
                           dt.Columns.Add("MillName");
                           dt.Columns.Add("ColorName");

                           dt.Columns.Add("IssQty");
                           //dt.Columns.Add("CustomerName");
                           dt.Columns.Add("Pk_Order");
                           dt.Columns.Add("OrderDate");
                           dt.Columns.Add("DeliveryDate");
                           dt.Columns.Add("ProdQty");

                           foreach (Vw_IssueDay entity in BillList)
                           {
                               DataRow row = dt.NewRow();

                               row["Name"] = entity.Name;
                               row["Pk_Order"] = entity.Pk_Order;
                               row["OrderDate"] = entity.OrderDate;
                               row["CustomerName"] = entity.CustomerName;
                               row["Pk_JobCardID"] = entity.Pk_JobCardID;
                               row["Pk_MaterialIssueID"] = entity.Pk_MaterialIssueID;
                               row["JDate"] = entity.JDate;
                               //row["DeliveryDate"] = entity.DeliveryDate;
                               row["Fk_Material"] = entity.Fk_Material;
                               row["RollNo"] = entity.RollNo;
                               row["IssueDate"] = entity.IssueDate;
                               row["ColorName"] = entity.ColorName;
                               row["MillName"] = entity.MillName;
                               row["IssQty"] = entity.IssQty;
                               row["JDate"] = entity.JDate;
                               //row["ProdQty"] = entity.ProdQty;

                               dt.Rows.Add(row);

                           }

                           DataSet ds = new DataSet();
                           ds.Tables.Add(dt);
                           CogitoStreamline.Report.IssueForDay orp = new CogitoStreamline.Report.IssueForDay();

                           orp.Load("@\\Report\\IssueForDay.rpt");
                           orp.SetDataSource(dt.DefaultView);



                           ReportDocument repDoc = orp;

                           ParameterFieldDefinitions crParameterFieldDefinitions4;
                           ParameterFieldDefinition crParameterFieldDefinition4;
                           ParameterValues crParameterValues4 = new ParameterValues();
                           ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                           crParameterDiscreteValue4.Value = Repfor;
                           crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                           crParameterFieldDefinition4 = crParameterFieldDefinitions4["period"];
                           crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                           crParameterValues4.Clear();
                           crParameterValues4.Add(crParameterDiscreteValue4);
                           crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);

                           Response.Buffer = false;
                           Response.ClearContent();
                           Response.ClearHeaders(); 
                           string pdfPath = Server.MapPath("~/ConvertPDF/" + "Issue" + ".pdf");
                           FileInfo file = new FileInfo(pdfPath);
                           if (file.Exists)
                           {
                               file.Delete();
                           }
                           var pd = new PrintDocument();


                           orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                           objDiskOpt.DiskFileName = pdfPath;

                           orp.ExportOptions.DestinationOptions = objDiskOpt;
                           orp.Export();

                       }
                       return null;
                   }
                   catch (Exception ex)
                   {
                       return null;
                       //  Response.Redirect("Index");D:\shantha\Billing\CogitoStreamline\CogitoStreamline\Controllers\CartonBillingController.cs
                   }
               }


               public JsonResult BoxList([System.Web.Http.FromBody]string data)
               {
                   try
                   {
                       data = data.TrimStart('=');
                       int i = 0;
                    
                       Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                       List<Vw_BoxDet> BillList = new List<Vw_BoxDet>();
                       CogitoStreamLineEntities dc = new CogitoStreamLineEntities();
                       
                       BillList = dc.Vw_BoxDet.OrderBy(x => x.CustomerName).Select(x => x).OfType<Vw_BoxDet>().ToList();
                       if (BillList.Count > 0)
                       {
                           DataTable dt = new DataTable("Box");


                            dt.Columns.Add("Name");
                           dt.Columns.Add("Length");
                           dt.Columns.Add("Width");
                           dt.Columns.Add("Height");                          
                           dt.Columns.Add("Weight");
                           dt.Columns.Add("GSM");
                           dt.Columns.Add("BF");
                           dt.Columns.Add("Pk_LayerID");
                           dt.Columns.Add("Pk_BoxID");
                           dt.Columns.Add("CustomerName");


                           dt.Columns.Add("BType");
                           dt.Columns.Add("BoardArea");
                           dt.Columns.Add("Deckle");
                           dt.Columns.Add("CuttingSize");
                           dt.Columns.Add("PName");
                           dt.Columns.Add("BoardBS");

                           dt.Columns.Add("BoardGSM");
                           dt.Columns.Add("FluteName");
                           dt.Columns.Add("FluteTKF");
                           dt.Columns.Add("PlyVal");
                           dt.Columns.Add("LayerWt");
                            

                           foreach (Vw_BoxDet entity in BillList)
                           {
                               DataRow row = dt.NewRow();

                               row["Length"] = entity.Length;
                               row["Width"] = entity.Width;
                               row["Height"] = entity.Height;
                               row["Weight"] = entity.Weight;
                               row["Name"] = entity.Name;
                               row["GSM"] = entity.GSM;
                               row["BF"] = entity.BF;
                               row["Pk_LayerID"] = entity.Pk_LayerID;
                               row["Pk_BoxID"] = entity.Pk_BoxID;
                               row["CustomerName"] = entity.CustomerName;
                               row["BType"] = entity.BType;
                               row["BoardArea"] = entity.BoardArea;
                               row["CuttingSize"] = entity.CuttingSize;
                               row["PName"] = entity.PName;
                               row["BoardBS"] = entity.BoardBS;
                               row["BoardGSM"] = entity.BoardGSM;
                               row["FluteName"] = entity.FluteName;
                               row["FluteTKF"] = entity.FluteTKF;
                               row["PlyVal"] = entity.PlyVal;
                               row["LayerWt"] = entity.LayerWt;
                            
                               //row["Pk_DeliverySechedule"] = entity.Pk_DeliverySechedule;
                               dt.Rows.Add(row);

                           }

                           DataSet ds = new DataSet();
                           ds.Tables.Add(dt);
                           CogitoStreamline.Report.Box orp = new CogitoStreamline.Report.Box();

                           orp.Load("@\\Report\\Box.rpt");
                           orp.SetDataSource(dt.DefaultView);



                           ReportDocument repDoc = orp;

                           Response.Buffer = false;
                           Response.ClearContent();
                           Response.ClearHeaders(); 

                           string pdfPath = Server.MapPath("~/ConvertPDF/" + "BoxList" + ".pdf");
                           FileInfo file = new FileInfo(pdfPath);
                           if (file.Exists)
                           {
                               file.Delete();
                           }
                           var pd = new PrintDocument();


                           ExportOptions rptOptions = new ExportOptions();
                           PdfFormatOptions pdffprnat = new PdfFormatOptions();
                           pdffprnat.CreateBookmarksFromGroupTree = true;
                           rptOptions.ExportFormatOptions = pdffprnat;
                           rptOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           rptOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           DiskFileDestinationOptions dest = new DiskFileDestinationOptions();
                           dest.DiskFileName = pdfPath;
                           rptOptions.ExportDestinationOptions = dest;
                           orp.Export(rptOptions);

                       }
                       return null;
                   }
                   catch (Exception ex)
                   {
                       return null;

                   }
               }

               public ActionResult TonnageVal(string data = "")
               {

                  

                   try
                   {
                     
                       Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                       DateTime SFrom = Convert.ToDateTime(oValues["FDate"]);
                       DateTime STo = Convert.ToDateTime(oValues["TDate"]);

                       string[] a = oValues["FDate"].ToString().Split('/');
                       DateTime FromDate = DateTime.ParseExact(a[1] + '/' + a[0] + '/' + a[2] + " 12:00", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);

                       var FD = SFrom.Day;
                       var FM = SFrom.ToString("MMM");
                       var FY = SFrom.Year;

                       var SFromDVal = FD.ToString() + '/' + FM.ToString() + '/' + FY.ToString();



                       var TD = STo.Day;
                       var TM = STo.ToString("MMM");
                       var TY = STo.Year;

                       var SToDVal = TD.ToString() + '/' + TM.ToString() + '/' + TY.ToString();


                       int dDay = Convert.ToInt32(a[0]);
                       int dmonth = Convert.ToInt32(a[1]);
                       int dyear = Convert.ToInt32(a[2]);

                       string monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dmonth);
                       //string Mname=FromDate.ToString( "MMMM");   // this one will also work to get the month name

                 //      string Repfor = " Paper Values in Tonnes Bet -  " + " " + SFromDVal + "& " + SToDVal;

                       TonnValues oTon = new TonnValues();
                    
                int RCount = _oEntities.TonnValues.Where(p => p.Pk_ID >0).Count();
                if (RCount > 0)
                {
                       TonnValues oTon1 = _oEntities.TonnValues.Where(p => p.Pk_ID >0).Single();

                    _oEntities.DeleteObject(oTon1);
                }

                       CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                       decimal SumStock =Convert.ToDecimal(dc.PaperStock.Where(x=>x.Quantity>0).Select(t => t.Quantity).Sum());


                       decimal Inward = Convert.ToDecimal(dc.Vw_InwardRep.Where(x => x.Quantity > 0 && x.Inward_Date >= SFrom && x.Inward_Date <= STo).Select(t => t.Quantity).Sum());
                      
                       decimal Issue = Convert.ToDecimal(dc.Vw_Issue.Where(x => x.Quantity > 0 && x.IssueDate >= SFrom && x.IssueDate <= STo).Select(t => t.Quantity).Sum());

                       decimal IssueReturn = Convert.ToDecimal(dc.Vw_Iss_Return.Where(x => x.ReturnQuantity > 0 && x.IssueReturnDate >= SFrom && x.IssueReturnDate <= STo).Select(t => t.ReturnQuantity).Sum());

                       //decimal DirectWastage = Convert.ToDecimal(dc.Vw_JCIssReport.Where(x => x.Weight > 0 && x.IssueDate >= SFrom && x.IssueDate <= STo).Select(t => t.Weight).Sum());


                       decimal PrdnWt = Convert.ToDecimal(dc.Vw_FinishingQty_Wt.Where(x => x.ProcessDate >= SFrom && x.ProcessDate <= STo).Select(t => t.PrdnWt).Sum());


                       //Issue - IssueReturn = Paper Consumption
                       var PaperConsumption = Issue - IssueReturn;
                       var WastageValue = PaperConsumption - PrdnWt;
                       //Paper Consumption - PrdnWt = wastage

                       oTon.ExStock = SumStock;                      
                       oTon.ProductionVal = PrdnWt;
                       oTon.IssRetStock = IssueReturn;
                       oTon.IssStock = Issue;
                       oTon.WastageVal = WastageValue;

                       _oEntities.AddToTonnValues(oTon);
                       _oEntities.SaveChanges();


                       return null;
                   }
                   catch (Exception ex)
                   {
                       return null;
                       //  Response.Redirect("Index");D:\shantha\Billing\CogitoStreamline\CogitoStreamline\Controllers\CartonBillingController.cs
                   }
               }

               public ActionResult PO_Tracker(string data = "")
               {
                   try
                   {

                      

                       string Repfor = "Purchase Orders List ";

                       List<Vw_PoBom> BillList = new List<Vw_PoBom>();
                       CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                       //BillList = dc.Vw_DeliveryScheduleReport.Where(x => x.DeliveryDate.Month == dmonth && x.DeliveryDate.Year == dyear && x.DeliveryDate.Day == dDay).Select(x => x).OfType<Vw_DeliveryScheduleReport>().ToList();
                       BillList = dc.Vw_PoBom.Where(x => x.POQty>0).OrderBy(x => x.Pk_PONo).Select(x => x).OfType<Vw_PoBom>().ToList();
                       if (BillList.Count > 0)
                       {
                           DataTable dt = new DataTable("DeliverySchedule");


                           dt.Columns.Add("Pk_PONo");
                           dt.Columns.Add("PODate");
                           dt.Columns.Add("Name");
                           dt.Columns.Add("VendorName");
                           dt.Columns.Add("POQty");
                           dt.Columns.Add("Pk_InwardDet");
                           dt.Columns.Add("Pk_Material");
                           dt.Columns.Add("AccQty");
                           dt.Columns.Add("PendQty");
                         dt.Columns.Add("Inward_Date");
                           foreach (Vw_PoBom entity in BillList)
                           {
                               DataRow row = dt.NewRow();

                               row["Name"] = entity.Name;
                               row["Pk_PONo"] = entity.Pk_PONo;
                               row["PODate"] = entity.PODate;
                               row["VendorName"] = entity.VendorName;
                               row["POQty"] = entity.POQty;
                               row["Pk_InwardDet"] = entity.Pk_InwardDet;
                               row["Pk_Material"] = entity.Pk_Material;
                               row["AccQty"] = entity.AccQty;
                               row["PendQty"] = entity.PendQty;
                               row["Inward_Date"] = entity.Inward_Date;
                               dt.Rows.Add(row);

                           }

                           DataSet ds = new DataSet();
                           ds.Tables.Add(dt);
                           CogitoStreamline.Report.PO_Tracker orp = new CogitoStreamline.Report.PO_Tracker();

                           orp.Load("@\\Report\\PO_Tracker.rpt");
                           orp.SetDataSource(dt.DefaultView);



                           ReportDocument repDoc = orp;

                           Response.Buffer = false;
                           Response.ClearContent();
                           Response.ClearHeaders(); 
                         

                           string pdfPath = Server.MapPath("~/ConvertPDF/" + "PO_Tracker" + ".pdf");
                           FileInfo file = new FileInfo(pdfPath);
                           if (file.Exists)
                           {
                               file.Delete();
                           }
                           var pd = new PrintDocument();


                           orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                           objDiskOpt.DiskFileName = pdfPath;

                           orp.ExportOptions.DestinationOptions = objDiskOpt;
                           orp.Export();

                       }
                       return null;
                   }
                   catch (Exception ex)
                   {
                       return null;
                       //  Response.Redirect("Index");D:\shantha\Billing\CogitoStreamline\CogitoStreamline\Controllers\CartonBillingController.cs
                   }
               }



               public JsonResult VPoList([System.Web.Http.FromBody]string data)
               {
                   try
                   {

                       data = data.TrimStart('=');
                       int i = 0;
                       //LensList = dc.Vw_PurchaseInwd.Where(x => x.PurDate >= SFrom && x.PurDate <= STo).Select(x => x).OfType<Vw_PurchaseInwd>().OrderBy(x => x.Pk_PurchaseID).ToList();

                       Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

                       DateTime SFrom = Convert.ToDateTime(oValues["FDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       DateTime STo = Convert.ToDateTime(oValues["TDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       string[] a = oValues["FDate"].ToString().Split('/');
                       DateTime FromDate = DateTime.ParseExact(a[1] + '/' + a[0] + '/' + a[2] + " 12:00", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);
                       string SFromDVal;
                       var FD = SFrom.Day;
                       var FM = SFrom.ToString("MMM");
                       var FY = SFrom.Year;


                       //if (FD > 12)
                       //{ SFromDVal = FM.ToString() + '/' + FD.ToString() + '/' + FY.ToString(); }
                       //else
                           SFromDVal = FD.ToString() + '/' + FM.ToString() + '/' + FY.ToString();



                       var TD = STo.Day;
                       var TM = STo.ToString("MMM");
                       var TY = STo.Year;

                       string SToDVal;
                       //if (TD > 12)
                       //{ SToDVal = TM.ToString() + '/' + TD.ToString() + '/' + TY.ToString(); }
                       //else
                           SToDVal = TD.ToString() + '/' + TM.ToString() + '/' + TY.ToString();


                       int dDay = Convert.ToInt32(a[0]);
                       int dmonth = Convert.ToInt32(a[1]);
                       int dyear = Convert.ToInt32(a[2]);

                       string monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dmonth);
                       //string Mname=FromDate.ToString( "MMMM");   // this one will also work to get the month name

                      string Repfor = "PO List Between - " + " " + SFromDVal + " " + "- " + SToDVal + " ";
                       CogitoStreamLineEntities dc = new CogitoStreamLineEntities();



                  //     string Repfor = "Purchase Orders List ";

                       List<Vw_PO> BillList = new List<Vw_PO>();
                      // CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                       //BillList = dc.Vw_DeliveryScheduleReport.Where(x => x.DeliveryDate.Month == dmonth && x.DeliveryDate.Year == dyear && x.DeliveryDate.Day == dDay).Select(x => x).OfType<Vw_DeliveryScheduleReport>().ToList();
                       BillList = dc.Vw_PO.Where(x => x.PODate >= SFrom && x.PODate <= STo && x.Fk_Status==1).OrderBy(x => x.VendorName).ThenBy(x => x.Pk_PONo).Select(x => x).OfType<Vw_PO>().ToList();
                       if (BillList.Count > 0)
                       {
                           DataTable dt = new DataTable("DeliverySchedule");
                           //SELECT     dbo.Inv_Material.Name, dbo.PurchaseOrderM.Pk_PONo, dbo.PurchaseOrderM.PODate, dbo.gen_Vendor.VendorName, 
                           //                           dbo.PurchaseOrderM.Fk_Status, dbo.PurchaseOrderD.Quantity AS POQty, 
                           //                      dbo.Inv_Material.Pk_Material, dbo.MaterialInwardD.Quantity AS AccQty, 
                           //                           dbo.PurchaseOrderD.Quantity - dbo.MaterialInwardD.Quantity AS PendQty, 


                           dt.Columns.Add("Pk_PONo");
                           dt.Columns.Add("PODate");
                           dt.Columns.Add("Name");
                           dt.Columns.Add("VendorName");
                           dt.Columns.Add("Quantity");
                           dt.Columns.Add("Pk_Vendor");
                           dt.Columns.Add("Pk_Material");
                           //dt.Columns.Add("Name");
                           //dt.Columns.Add("PendQty");
                           dt.Columns.Add("Fk_IndentVal");
                           //dt.Columns.Add("Inward_Date");
                           foreach (Vw_PO entity in BillList)
                           {
                               DataRow row = dt.NewRow();

                               row["Name"] = entity.Name;
                               row["Pk_PONo"] = entity.Pk_PONo;
                               row["PODate"] = entity.PODate;
                               row["VendorName"] = entity.VendorName;
                               row["Quantity"] = entity.Quantity;
                               row["Pk_Vendor"] = entity.Pk_Vendor;
                               row["Pk_Material"] = entity.Pk_Material;
                               //row["Name"] = entity.Name;
                               row["Fk_IndentVal"] = entity.Fk_IndentVal;
                               //row["PendQty"] = entity.PendQty;
                               //row["Inward_Date"] = entity.Inward_Date;
                               dt.Rows.Add(row);

                           }

                           DataSet ds = new DataSet();
                           ds.Tables.Add(dt);
                           CogitoStreamline.Report.PO_List_VendorWise orp = new CogitoStreamline.Report.PO_List_VendorWise();

                           orp.Load("@\\Report\\PO_List_VendorWise.rpt");
                           orp.SetDataSource(dt.DefaultView);



                           ReportDocument repDoc = orp;


                           ParameterFieldDefinitions crParameterFieldDefinitions4;
                           ParameterFieldDefinition crParameterFieldDefinition4;
                           ParameterValues crParameterValues4 = new ParameterValues();
                           ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                           crParameterDiscreteValue4.Value = Repfor;
                           crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                           crParameterFieldDefinition4 = crParameterFieldDefinitions4["period"];
                           crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                           crParameterValues4.Clear();
                           crParameterValues4.Add(crParameterDiscreteValue4);
                           crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);


                           Response.Buffer = false;
                           Response.ClearContent();
                           Response.ClearHeaders(); 

                           string pdfPath = Server.MapPath("~/ConvertPDF/" + "Vendor_PO_List" + ".pdf");
                           FileInfo file = new FileInfo(pdfPath);
                           if (file.Exists)
                           {
                               file.Delete();
                           }
                           var pd = new PrintDocument();


                           //orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           //orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           ////  CreateBookmarksFromGroupTree = true;
                           ////   pdffprnat.CreateBookmarksFromGroupTree = true;
                           //DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                           //objDiskOpt.DiskFileName = pdfPath;

                           //orp.ExportOptions.DestinationOptions = objDiskOpt;
                           //orp.Export();


                           ExportOptions rptOptions = new ExportOptions();
                           PdfFormatOptions pdffprnat = new PdfFormatOptions();
                           pdffprnat.CreateBookmarksFromGroupTree = true;
                           rptOptions.ExportFormatOptions = pdffprnat;
                           rptOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           rptOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           DiskFileDestinationOptions dest = new DiskFileDestinationOptions();
                           dest.DiskFileName = pdfPath;
                           rptOptions.ExportDestinationOptions = dest;
                           orp.Export(rptOptions);

                       }
                       return null;
                   }
                   catch (Exception ex)
                   {
                       return null;
                       //  Response.Redirect("Index");D:\shantha\Billing\CogitoStreamline\CogitoStreamline\Controllers\CartonBillingController.cs
                   }
               }

               public JsonResult VPoConsumablesList([System.Web.Http.FromBody]string data)
               {
                   try
                   {

                       data = data.TrimStart('=');
                       int i = 0;

                       Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

                       DateTime SFrom = Convert.ToDateTime(oValues["FDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       DateTime STo = Convert.ToDateTime(oValues["TDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       string[] a = oValues["FDate"].ToString().Split('/');
                       DateTime FromDate = DateTime.ParseExact(a[1] + '/' + a[0] + '/' + a[2] + " 12:00", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);
                       string SFromDVal;
                       var FD = SFrom.Day;
                       var FM = SFrom.ToString("MMM");
                       var FY = SFrom.Year;


                       SFromDVal = FD.ToString() + '/' + FM.ToString() + '/' + FY.ToString();



                       var TD = STo.Day;
                       var TM = STo.ToString("MMM");
                       var TY = STo.Year;

                       string SToDVal;
                       //if (TD > 12)
                       //{ SToDVal = TM.ToString() + '/' + TD.ToString() + '/' + TY.ToString(); }
                       //else
                       SToDVal = TD.ToString() + '/' + TM.ToString() + '/' + TY.ToString();


                       int dDay = Convert.ToInt32(a[0]);
                       int dmonth = Convert.ToInt32(a[1]);
                       int dyear = Convert.ToInt32(a[2]);

                       string monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dmonth);
                       //string Mname=FromDate.ToString( "MMMM");   // this one will also work to get the month name

                       string Repfor = "PO List Between - " + " " + SFromDVal + " " + "- " + SToDVal + " ";
                       CogitoStreamLineEntities dc = new CogitoStreamLineEntities();



                     
                       List<Vw_ConsPO> BillList = new List<Vw_ConsPO>();
                       // CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                       BillList = dc.Vw_ConsPO.Where(x => x.PODate >= SFrom && x.PODate <= STo && x.Fk_Status == 1).OrderBy(x => x.VendorName).ThenBy(x => x.Pk_PONo).Select(x => x).OfType<Vw_ConsPO>().ToList();
                       if (BillList.Count > 0)
                       {
                           DataTable dt = new DataTable("DeliverySchedule");
                         

                           dt.Columns.Add("Pk_PONo");
                           dt.Columns.Add("PODate");
                           dt.Columns.Add("Name");
                           dt.Columns.Add("VendorName");
                           dt.Columns.Add("Quantity");
                           dt.Columns.Add("Pk_Vendor");
                           dt.Columns.Add("Pk_Material");
                           dt.Columns.Add("Pk_MaterialCategory");
                           dt.Columns.Add("Fk_Material");
                           dt.Columns.Add("Fk_IndentVal");
                           dt.Columns.Add("MatName");
                           dt.Columns.Add("Pk_PODet");
                           dt.Columns.Add("Amount");
                           dt.Columns.Add("Fk_Status");
                           //dt.Columns.Add("Pk_Vendor");
                           
                           foreach (Vw_ConsPO entity in BillList)
                           {
                               DataRow row = dt.NewRow();

                               row["Name"] = entity.Name;
                               row["Pk_PONo"] = entity.Pk_PONo;
                               row["PODate"] = entity.PODate;
                               row["VendorName"] = entity.VendorName;
                               row["Quantity"] = entity.Quantity;
                               row["Pk_Vendor"] = entity.Pk_Vendor;
                               row["Pk_Material"] = entity.Pk_Material;
                               row["Pk_MaterialCategory"] = entity.Pk_MaterialCategory;
                               row["Fk_Material"] = entity.Fk_Material;
                             row["MatName"] = entity.MatName;
                             row["Pk_PODet"] = entity.Pk_PODet;
                             row["Amount"] = entity.Amount;
                             row["Fk_Status"] = entity.Fk_Status;
                             //row["Pk_Vendor"] = entity.Pk_Vendor;
                               
                               dt.Rows.Add(row);

                           }

                           DataSet ds = new DataSet();
                           ds.Tables.Add(dt);
                           CogitoStreamline.Report.Vend_PO_List orp = new CogitoStreamline.Report.Vend_PO_List();

                           orp.Load("@\\Report\\Vend_PO_List.rpt");
                           orp.SetDataSource(dt.DefaultView);



                           ReportDocument repDoc = orp;


                           ParameterFieldDefinitions crParameterFieldDefinitions4;
                           ParameterFieldDefinition crParameterFieldDefinition4;
                           ParameterValues crParameterValues4 = new ParameterValues();
                           ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                           crParameterDiscreteValue4.Value = Repfor;
                           crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                           crParameterFieldDefinition4 = crParameterFieldDefinitions4["period"];
                           crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                           crParameterValues4.Clear();
                           crParameterValues4.Add(crParameterDiscreteValue4);
                           crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);
                           Response.Buffer = false;
                           Response.ClearContent();
                           Response.ClearHeaders(); 

                           string pdfPath = Server.MapPath("~/ConvertPDF/" + "Vendorwise_Cons_PO_List" + ".pdf");
                           FileInfo file = new FileInfo(pdfPath);
                           if (file.Exists)
                           {
                               file.Delete();
                           }
                           var pd = new PrintDocument();


                           //orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           //orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           ////  CreateBookmarksFromGroupTree = true;
                           ////   pdffprnat.CreateBookmarksFromGroupTree = true;
                           //DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                           //objDiskOpt.DiskFileName = pdfPath;

                           //orp.ExportOptions.DestinationOptions = objDiskOpt;
                           //orp.Export();


                           ExportOptions rptOptions = new ExportOptions();
                           PdfFormatOptions pdffprnat = new PdfFormatOptions();
                           pdffprnat.CreateBookmarksFromGroupTree = true;
                           rptOptions.ExportFormatOptions = pdffprnat;
                           rptOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           rptOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           DiskFileDestinationOptions dest = new DiskFileDestinationOptions();
                           dest.DiskFileName = pdfPath;
                           rptOptions.ExportDestinationOptions = dest;
                           orp.Export(rptOptions);

                       }
                       return null;
                   }
                   catch (Exception ex)
                   {
                       return null;
                       //  Response.Redirect("Index");D:\shantha\Billing\CogitoStreamline\CogitoStreamline\Controllers\CartonBillingController.cs
                   }
               }



               public JsonResult ConsumablesIssueList([System.Web.Http.FromBody]string data)
               {
                   try
                   {

                       data = data.TrimStart('=');
                       int i = 0;

                       Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

                       DateTime SFrom = Convert.ToDateTime(oValues["FDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       DateTime STo = Convert.ToDateTime(oValues["TDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       string[] a = oValues["FDate"].ToString().Split('/');
                       DateTime FromDate = DateTime.ParseExact(a[1] + '/' + a[0] + '/' + a[2] + " 12:00", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);
                       string SFromDVal;
                       var FD = SFrom.Day;
                       var FM = SFrom.ToString("MMM");
                       var FY = SFrom.Year;


                       SFromDVal = FD.ToString() + '/' + FM.ToString() + '/' + FY.ToString();



                       var TD = STo.Day;
                       var TM = STo.ToString("MMM");
                       var TY = STo.Year;

                       string SToDVal;
                       //if (TD > 12)
                       //{ SToDVal = TM.ToString() + '/' + TD.ToString() + '/' + TY.ToString(); }
                       //else
                       SToDVal = TD.ToString() + '/' + TM.ToString() + '/' + TY.ToString();


                       int dDay = Convert.ToInt32(a[0]);
                       int dmonth = Convert.ToInt32(a[1]);
                       int dyear = Convert.ToInt32(a[2]);

                       string monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dmonth);
                       //string Mname=FromDate.ToString( "MMMM");   // this one will also work to get the month name

                       string Repfor = "Cons.Issue List Between - " + " " + SFromDVal + " " + "- " + SToDVal + " ";
                       CogitoStreamLineEntities dc = new CogitoStreamLineEntities();


 //                       SELECT Vw_ConsIssueList.CatName, Vw_ConsIssueList.Pk_Material, Vw_ConsIssueList.MatName, Vw_ConsIssueList.Pk_ID,
 //                      Vw_ConsIssueList.Pk_ID_Det, Vw_ConsIssueList.Quantity, Vw_ConsIssueList.Returned, Vw_ConsIssueList.IssuedBy, 
 //                      Vw_ConsIssueList.IssuedTo, Vw_ConsIssueList.IssueDate
 //FROM   BalajiTesting.dbo.Vw_ConsIssueList Vw_ConsIssueList
 //ORDER BY Vw_ConsIssueList.Pk_ID, Vw_ConsIssueList.CatName, Vw_ConsIssueList.Pk_Material




                       List<Vw_ConsIssueList> BillList = new List<Vw_ConsIssueList>();
                       // CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                       BillList = dc.Vw_ConsIssueList.Where(x => x.IssueDate >= SFrom && x.IssueDate <= STo).OrderBy(x => x.IssueDate).Select(x => x).OfType<Vw_ConsIssueList>().ToList();
                       if (BillList.Count > 0)
                       {
                           DataTable dt = new DataTable("DeliverySchedule");


                           dt.Columns.Add("CatName");
                           dt.Columns.Add("Pk_Material");
                           dt.Columns.Add("MatName");
                           dt.Columns.Add("Pk_ID");
                           dt.Columns.Add("Quantity");
                           dt.Columns.Add("Pk_ID_Det");
                           //dt.Columns.Add("Pk_Material");
                           dt.Columns.Add("Returned");
                           dt.Columns.Add("IssuedBy");
                           dt.Columns.Add("IssuedTo");
                           //dt.Columns.Add("MatName");
                           dt.Columns.Add("IssueDate");
                           //dt.Columns.Add("Amount");
                           //dt.Columns.Add("Fk_Status");

                           //dt.Columns.Add("Pk_Vendor");

                           foreach (Vw_ConsIssueList entity in BillList)
                           {
                               DataRow row = dt.NewRow();

                               row["CatName"] = entity.CatName;
                               row["Pk_Material"] = entity.Pk_Material;
                               row["MatName"] = entity.MatName;
                               row["Pk_ID"] = entity.Pk_ID;
                               row["Quantity"] = entity.Quantity;
                               row["Pk_ID_Det"] = entity.Pk_ID_Det;
                               row["Pk_Material"] = entity.Pk_Material;
                               row["Returned"] = entity.Returned;
                               row["IssuedBy"] = entity.IssuedBy;
                               row["IssueDate"] = entity.IssueDate;
                               row["IssuedTo"] = entity.IssuedTo;
                               //row["Amount"] = entity.Amount;
                               //row["Fk_Status"] = entity.Fk_Status;
                               //row["Pk_Vendor"] = entity.Pk_Vendor;

                               dt.Rows.Add(row);

                           }

                           DataSet ds = new DataSet();
                           ds.Tables.Add(dt);
                           CogitoStreamline.Report.ConsumableIssue orp = new CogitoStreamline.Report.ConsumableIssue();

                           orp.Load("@\\Report\\ConsumableIssue.rpt");
                           orp.SetDataSource(dt.DefaultView);



                           ReportDocument repDoc = orp;


                           ParameterFieldDefinitions crParameterFieldDefinitions4;
                           ParameterFieldDefinition crParameterFieldDefinition4;
                           ParameterValues crParameterValues4 = new ParameterValues();
                           ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                           crParameterDiscreteValue4.Value = Repfor;
                           crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                           crParameterFieldDefinition4 = crParameterFieldDefinitions4["period"];
                           crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                           crParameterValues4.Clear();
                           crParameterValues4.Add(crParameterDiscreteValue4);
                           crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);

                           Response.Buffer = false;
                           Response.ClearContent();
                           Response.ClearHeaders(); 


                           string pdfPath = Server.MapPath("~/ConvertPDF/" + "ConsumableIssueList" + ".pdf");
                           FileInfo file = new FileInfo(pdfPath);
                           if (file.Exists)
                           {
                               file.Delete();
                           }
                           var pd = new PrintDocument();


                           //orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           //orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           ////  CreateBookmarksFromGroupTree = true;
                           ////   pdffprnat.CreateBookmarksFromGroupTree = true;
                           //DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                           //objDiskOpt.DiskFileName = pdfPath;

                           //orp.ExportOptions.DestinationOptions = objDiskOpt;
                           //orp.Export();


                           ExportOptions rptOptions = new ExportOptions();
                           PdfFormatOptions pdffprnat = new PdfFormatOptions();
                           pdffprnat.CreateBookmarksFromGroupTree = true;
                           rptOptions.ExportFormatOptions = pdffprnat;
                           rptOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           rptOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           DiskFileDestinationOptions dest = new DiskFileDestinationOptions();
                           dest.DiskFileName = pdfPath;
                           rptOptions.ExportDestinationOptions = dest;
                           orp.Export(rptOptions);

                       }
                       else
                       { 

                       }
                       return null;
                   }
                   catch (Exception ex)
                   {
                       return null;
                       //  Response.Redirect("Index");D:\shantha\Billing\CogitoStreamline\CogitoStreamline\Controllers\CartonBillingController.cs
                   }
               }




               public JsonResult PO_To_Inward([System.Web.Http.FromBody]string data)
               {
                   try
                   {

                       data = data.TrimStart('=');
                       int i = 0;

                       Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

                       DateTime SFrom = Convert.ToDateTime(oValues["FDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       DateTime STo = Convert.ToDateTime(oValues["TDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       string[] a = oValues["FDate"].ToString().Split('/');
                       DateTime FromDate = DateTime.ParseExact(a[1] + '/' + a[0] + '/' + a[2] + " 12:00", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);
                       string SFromDVal;
                       var FD = SFrom.Day;
                       var FM = SFrom.ToString("MMM");
                       var FY = SFrom.Year;


                       SFromDVal = FD.ToString() + '/' + FM.ToString() + '/' + FY.ToString();



                       var TD = STo.Day;
                       var TM = STo.ToString("MMM");
                       var TY = STo.Year;

                       string SToDVal;
                       //if (TD > 12)
                       //{ SToDVal = TM.ToString() + '/' + TD.ToString() + '/' + TY.ToString(); }
                       //else
                       SToDVal = TD.ToString() + '/' + TM.ToString() + '/' + TY.ToString();


                       int dDay = Convert.ToInt32(a[0]);
                       int dmonth = Convert.ToInt32(a[1]);
                       int dyear = Convert.ToInt32(a[2]);

                       string monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dmonth);
                       //string Mname=FromDate.ToString( "MMMM");   // this one will also work to get the month name

                       string Repfor = "Inward Pending List Between - " + " " + SFromDVal + " " + "- " + SToDVal + " ";
                       CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

 //                       SELECT Vw_PO_Pending_Inward.Pk_InwardDet, Vw_PO_Pending_Inward.Name, Vw_PO_Pending_Inward.Fk_MaterialCategory, 
 //                      Vw_PO_Pending_Inward.Pk_PONo, Vw_PO_Pending_Inward.PODate, Vw_PO_Pending_Inward.OrdQty, 
 //                      Vw_PO_Pending_Inward.PurQty, Vw_PO_Pending_Inward.CategoryName
 //FROM   BalajiTesting.dbo.Vw_PO_Pending_Inward Vw_PO_Pending_Inward
 //ORDER BY Vw_PO_Pending_Inward.Pk_PONo, Vw_PO_Pending_Inward.Pk_InwardDet

                       List<Vw_PO_Pending_Inward> BillList = new List<Vw_PO_Pending_Inward>();
                       // CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                       BillList = dc.Vw_PO_Pending_Inward.Where(x => x.PODate >= SFrom && x.PODate <= STo).OrderBy(x => x.PODate).Select(x => x).OfType<Vw_PO_Pending_Inward>().ToList();
                       if (BillList.Count > 0)
                       {
                           DataTable dt = new DataTable("DeliverySchedule");


                           dt.Columns.Add("Pk_InwardDet");
                           dt.Columns.Add("Name");
                           dt.Columns.Add("Fk_MaterialCategory");
                           dt.Columns.Add("Pk_PONo");
                           dt.Columns.Add("PODate");
                           dt.Columns.Add("OrdQty");
                           dt.Columns.Add("Fk_Material");
                           dt.Columns.Add("CatName");
                    

                           foreach (Vw_PO_Pending_Inward entity in BillList)
                           {
                               DataRow row = dt.NewRow();

                               //row["Pk_InwardDet"] = entity.Pk_InwardDet;
                               row["Name"] = entity.Name;
                               row["Fk_MaterialCategory"] = entity.Fk_MaterialCategory;
                               row["Pk_PONo"] = entity.Pk_PONo;
                               row["PODate"] = entity.PODate;
                               row["OrdQty"] = entity.OrdQty;
                               row["Fk_Material"] = entity.Fk_Material;
                               row["CatName"] = entity.CatName;
                           

                               dt.Rows.Add(row);

                           }

                           DataSet ds = new DataSet();
                           ds.Tables.Add(dt);
                           CogitoStreamline.Report.PO_Inwd_Pending orp = new CogitoStreamline.Report.PO_Inwd_Pending();

                           orp.Load("@\\Report\\PO_Inwd_Pending.rpt");
                           orp.SetDataSource(dt.DefaultView);



                           ReportDocument repDoc = orp;


                           ParameterFieldDefinitions crParameterFieldDefinitions4;
                           ParameterFieldDefinition crParameterFieldDefinition4;
                           ParameterValues crParameterValues4 = new ParameterValues();
                           ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                           crParameterDiscreteValue4.Value = Repfor;
                           crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                           crParameterFieldDefinition4 = crParameterFieldDefinitions4["period"];
                           crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                           crParameterValues4.Clear();
                           crParameterValues4.Add(crParameterDiscreteValue4);
                           crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);

                           Response.Buffer = false;
                           Response.ClearContent();
                           Response.ClearHeaders(); 
                           string pdfPath = Server.MapPath("~/ConvertPDF/" + "PO_Inwd_PendingList" + ".pdf");
                           FileInfo file = new FileInfo(pdfPath);
                           if (file.Exists)
                           {
                               file.Delete();
                           }
                           var pd = new PrintDocument();


                           //orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           //orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           ////  CreateBookmarksFromGroupTree = true;
                           ////   pdffprnat.CreateBookmarksFromGroupTree = true;
                           //DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                           //objDiskOpt.DiskFileName = pdfPath;

                           //orp.ExportOptions.DestinationOptions = objDiskOpt;
                           //orp.Export();


                           ExportOptions rptOptions = new ExportOptions();
                           PdfFormatOptions pdffprnat = new PdfFormatOptions();
                           pdffprnat.CreateBookmarksFromGroupTree = true;
                           rptOptions.ExportFormatOptions = pdffprnat;
                           rptOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           rptOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           DiskFileDestinationOptions dest = new DiskFileDestinationOptions();
                           dest.DiskFileName = pdfPath;
                           rptOptions.ExportDestinationOptions = dest;
                           orp.Export(rptOptions);

                       }
                       else
                       {

                       }
                       return null;
                   }
                   catch (Exception ex)
                   {
                       return null;
                       //  Response.Redirect("Index");D:\shantha\Billing\CogitoStreamline\CogitoStreamline\Controllers\CartonBillingController.cs
                   }
               }
             
        public JsonResult IndentBetDates([System.Web.Http.FromBody]string data)
               {
                   try
                   {

                       data = data.TrimStart('=');
                       int i = 0;

                       Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

                       DateTime SFrom = Convert.ToDateTime(oValues["FDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       DateTime STo = Convert.ToDateTime(oValues["TDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       string[] a = oValues["FDate"].ToString().Split('/');
                       DateTime FromDate = DateTime.ParseExact(a[1] + '/' + a[0] + '/' + a[2] + " 12:00", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);
                       string SFromDVal;
                       var FD = SFrom.Day;
                       var FM = SFrom.ToString("MMM");
                       var FY = SFrom.Year;


                       SFromDVal = FD.ToString() + '/' + FM.ToString() + '/' + FY.ToString();



                       var TD = STo.Day;
                       var TM = STo.ToString("MMM");
                       var TY = STo.Year;

                       string SToDVal;
                       //if (TD > 12)
                       //{ SToDVal = TM.ToString() + '/' + TD.ToString() + '/' + TY.ToString(); }
                       //else
                       SToDVal = TD.ToString() + '/' + TM.ToString() + '/' + TY.ToString();


                       int dDay = Convert.ToInt32(a[0]);
                       int dmonth = Convert.ToInt32(a[1]);
                       int dyear = Convert.ToInt32(a[2]);

                       string monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dmonth);
                       //string Mname=FromDate.ToString( "MMMM");   // this one will also work to get the month name

                       string Repfor = "Indent Details Between - " + " " + SFromDVal + " " + "- " + SToDVal + " ";
                       CogitoStreamLineEntities dc = new CogitoStreamLineEntities();
 
                       
 //                      SELECT Vw_Indent.Name, Vw_Indent.UnitName, Vw_Indent.MaterialIndentDate, Vw_Indent.VendorName,
 //                      Vw_Indent.Pk_MaterialOrderMasterId, Vw_Indent.Pk_Material, Vw_Indent.Quantity, 
 //                      Vw_Indent.Fk_CustomerOrder
 //FROM   BalajiTesting.dbo.Vw_Indent Vw_Indent
 //ORDER BY Vw_Indent.VendorName, Vw_Indent.Pk_MaterialOrderMasterId


                       List<Vw_Indent> BillList = new List<Vw_Indent>();
                       // CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                       BillList = dc.Vw_Indent.Where(x => x.MaterialIndentDate >= SFrom && x.MaterialIndentDate <= STo).OrderBy(x => x.MaterialIndentDate).Select(x => x).OfType<Vw_Indent>().ToList();
                       if (BillList.Count > 0)
                       {
                           DataTable dt = new DataTable("IndentDetails");

                           dt.Columns.Add("Name");
                           dt.Columns.Add("UnitName");
                           dt.Columns.Add("MaterialIndentDate");
                           dt.Columns.Add("VendorName");
                           dt.Columns.Add("Pk_Material");
                         //  dt.Columns.Add("VendorName");
                           dt.Columns.Add("Pk_MaterialOrderMasterId");
                           dt.Columns.Add("Quantity");
                           dt.Columns.Add("Fk_CustomerOrder");



                           foreach (Vw_Indent entity in BillList)
                           {
                               DataRow row = dt.NewRow();

                               //row["Pk_InwardDet"] = entity.Pk_InwardDet;
                               row["Name"] = entity.Name;
                               row["UnitName"] = entity.UnitName;
                               row["MaterialIndentDate"] = entity.MaterialIndentDate;
                               row["VendorName"] = entity.VendorName;
                               row["Pk_MaterialOrderMasterId"] = entity.Pk_MaterialOrderMasterId;
                               row["Quantity"] = entity.Quantity;
                               row["Fk_CustomerOrder"] = entity.Fk_CustomerOrder;
                               row["Pk_Material"] = entity.Pk_Material;
                               dt.Rows.Add(row);

                           }

                           DataSet ds = new DataSet();
                           ds.Tables.Add(dt);
                           CogitoStreamline.Report.IndentDates orp = new CogitoStreamline.Report.IndentDates();

                           orp.Load("@\\Report\\IndentDates.rpt");
                           orp.SetDataSource(dt.DefaultView);



                           ReportDocument repDoc = orp;


                           ParameterFieldDefinitions crParameterFieldDefinitions4;
                           ParameterFieldDefinition crParameterFieldDefinition4;
                           ParameterValues crParameterValues4 = new ParameterValues();
                           ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                           crParameterDiscreteValue4.Value = Repfor;
                           crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                           crParameterFieldDefinition4 = crParameterFieldDefinitions4["period"];
                           crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                           crParameterValues4.Clear();
                           crParameterValues4.Add(crParameterDiscreteValue4);
                           crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);

                           Response.Buffer = false;
                           Response.ClearContent();
                           Response.ClearHeaders(); 


                           string pdfPath = Server.MapPath("~/ConvertPDF/" + "IndentDates" + ".pdf");
                           FileInfo file = new FileInfo(pdfPath);
                           if (file.Exists)
                           {
                               file.Delete();
                           }
                           var pd = new PrintDocument();


                           //orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           //orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           ////  CreateBookmarksFromGroupTree = true;
                           ////   pdffprnat.CreateBookmarksFromGroupTree = true;
                           //DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                           //objDiskOpt.DiskFileName = pdfPath;

                           //orp.ExportOptions.DestinationOptions = objDiskOpt;
                           //orp.Export();


                           ExportOptions rptOptions = new ExportOptions();
                           PdfFormatOptions pdffprnat = new PdfFormatOptions();
                           pdffprnat.CreateBookmarksFromGroupTree = true;
                           rptOptions.ExportFormatOptions = pdffprnat;
                           rptOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           rptOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           DiskFileDestinationOptions dest = new DiskFileDestinationOptions();
                           dest.DiskFileName = pdfPath;
                           rptOptions.ExportDestinationOptions = dest;
                           orp.Export(rptOptions);

                       }
                       else
                       {

                       }
                       return null;
                   }
                   catch (Exception ex)
                   {
                       return null;
                       //  Response.Redirect("Index");D:\shantha\Billing\CogitoStreamline\CogitoStreamline\Controllers\CartonBillingController.cs
                   }
               }


               public JsonResult Cust_OrderDates([System.Web.Http.FromBody]string data)
               {
                   try
                   {

                       data = data.TrimStart('=');
                       int i = 0;

                       Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

                       DateTime SFrom = Convert.ToDateTime(oValues["FDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       DateTime STo = Convert.ToDateTime(oValues["TDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       string[] a = oValues["FDate"].ToString().Split('/');
                       DateTime FromDate = DateTime.ParseExact(a[1] + '/' + a[0] + '/' + a[2] + " 12:00", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);
                       string SFromDVal;
                       var FD = SFrom.Day;
                       var FM = SFrom.ToString("MMM");
                       var FY = SFrom.Year;


                       SFromDVal = FD.ToString() + '/' + FM.ToString() + '/' + FY.ToString();



                       var TD = STo.Day;
                       var TM = STo.ToString("MMM");
                       var TY = STo.Year;

                       string SToDVal;
                       //if (TD > 12)
                       //{ SToDVal = TM.ToString() + '/' + TD.ToString() + '/' + TY.ToString(); }
                       //else
                       SToDVal = TD.ToString() + '/' + TM.ToString() + '/' + TY.ToString();


                       int dDay = Convert.ToInt32(a[0]);
                       int dmonth = Convert.ToInt32(a[1]);
                       int dyear = Convert.ToInt32(a[2]);

                       string monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dmonth);
                       //string Mname=FromDate.ToString( "MMMM");   // this one will also work to get the month name

                       string Repfor = "Order Details Between - " + " " + SFromDVal + " " + "- " + SToDVal + " ";
                       CogitoStreamLineEntities dc = new CogitoStreamLineEntities();


                       //                      SELECT Vw_OrderList.Pk_Order, Vw_OrderList.Product, Vw_OrderList.CustomerName, Vw_OrderList.Pk_Customer, 
                       //                      Vw_OrderList.OrderDate, Vw_OrderList.Name, Vw_OrderList.PName, Vw_OrderList.OrdQty, Vw_OrderList.Rate
                       //FROM   BalajiTesting.dbo.Vw_OrderList Vw_OrderList
                       //ORDER BY Vw_OrderList.CustomerName, Vw_OrderList.Pk_Order



                       List<Vw_OrderList> BillList = new List<Vw_OrderList>();
                       // CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                       BillList = dc.Vw_OrderList.Where(x => x.OrderDate >= SFrom && x.OrderDate <= STo).OrderBy(x => x.OrderDate).Select(x => x).OfType<Vw_OrderList>().ToList();
                       if (BillList.Count > 0)
                       {
                           DataTable dt = new DataTable("OrderDetails");

                           dt.Columns.Add("Name");
                           dt.Columns.Add("PName");
                           dt.Columns.Add("OrderDate");
                           dt.Columns.Add("CustomerName");
                           dt.Columns.Add("Rate");
                           dt.Columns.Add("Pk_OrderChild");
                           dt.Columns.Add("Pk_Order");
                           dt.Columns.Add("OrdQty");
                           dt.Columns.Add("Product");
                           dt.Columns.Add("Cust_PO");



                           foreach (Vw_OrderList entity in BillList)
                           {
                               DataRow row = dt.NewRow();

                               row["Pk_OrderChild"] = entity.Pk_OrderChild;
                               row["Name"] = entity.Name;
                               row["PName"] = entity.PName;
                               row["OrderDate"] = entity.OrderDate;
                               row["CustomerName"] = entity.CustomerName;
                               row["Pk_Order"] = entity.Pk_Order;
                               row["OrdQty"] = entity.OrdQty;
                               row["Product"] = entity.Product;
                               row["Rate"] = entity.Rate;
                               row["Cust_PO"] = entity.Cust_PO;
                               dt.Rows.Add(row);

                           }

                           DataSet ds = new DataSet();
                           ds.Tables.Add(dt);
                           CogitoStreamline.Report.Cust_Orders orp = new CogitoStreamline.Report.Cust_Orders();

                           orp.Load("@\\Report\\Cust_Orders.rpt");
                           orp.SetDataSource(dt.DefaultView);



                           ReportDocument repDoc = orp;


                           ParameterFieldDefinitions crParameterFieldDefinitions4;
                           ParameterFieldDefinition crParameterFieldDefinition4;
                           ParameterValues crParameterValues4 = new ParameterValues();
                           ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                           crParameterDiscreteValue4.Value = Repfor;
                           crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                           crParameterFieldDefinition4 = crParameterFieldDefinitions4["period"];
                           crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                           crParameterValues4.Clear();
                           crParameterValues4.Add(crParameterDiscreteValue4);
                           crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);

                           Response.Buffer = false;
                           Response.ClearContent();
                           Response.ClearHeaders();
                           string pdfPath = Server.MapPath("~/ConvertPDF/" + "Cust_OrderDates" + ".pdf");
                           FileInfo file = new FileInfo(pdfPath);
                           if (file.Exists)
                           {
                               file.Delete();
                           }
                           var pd = new PrintDocument();


                           //orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           //orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           ////  CreateBookmarksFromGroupTree = true;
                           ////   pdffprnat.CreateBookmarksFromGroupTree = true;
                           //DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                           //objDiskOpt.DiskFileName = pdfPath;

                           //orp.ExportOptions.DestinationOptions = objDiskOpt;
                           //orp.Export();


                           ExportOptions rptOptions = new ExportOptions();
                           PdfFormatOptions pdffprnat = new PdfFormatOptions();
                           pdffprnat.CreateBookmarksFromGroupTree = true;
                           rptOptions.ExportFormatOptions = pdffprnat;
                           rptOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           rptOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           DiskFileDestinationOptions dest = new DiskFileDestinationOptions();
                           dest.DiskFileName = pdfPath;
                           rptOptions.ExportDestinationOptions = dest;
                           orp.Export(rptOptions);

                       }
                       else
                       {

                       }
                       return null;
                   }
                   catch (Exception ex)
                   {
                       return null;
                       //  Response.Redirect("Index");D:\shantha\Billing\CogitoStreamline\CogitoStreamline\Controllers\CartonBillingController.cs
                   }
               }



               public JsonResult OrderDates([System.Web.Http.FromBody]string data)
               {
                   try
                   {

                       data = data.TrimStart('=');
                       int i = 0;

                       Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

                       DateTime SFrom = Convert.ToDateTime(oValues["FDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       DateTime STo = Convert.ToDateTime(oValues["TDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       string[] a = oValues["FDate"].ToString().Split('/');
                       DateTime FromDate = DateTime.ParseExact(a[1] + '/' + a[0] + '/' + a[2] + " 12:00", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);
                       string SFromDVal;
                       var FD = SFrom.Day;
                       var FM = SFrom.ToString("MMM");
                       var FY = SFrom.Year;


                       SFromDVal = FD.ToString() + '/' + FM.ToString() + '/' + FY.ToString();



                       var TD = STo.Day;
                       var TM = STo.ToString("MMM");
                       var TY = STo.Year;

                       string SToDVal;
                       //if (TD > 12)
                       //{ SToDVal = TM.ToString() + '/' + TD.ToString() + '/' + TY.ToString(); }
                       //else
                       SToDVal = TD.ToString() + '/' + TM.ToString() + '/' + TY.ToString();


                       int dDay = Convert.ToInt32(a[0]);
                       int dmonth = Convert.ToInt32(a[1]);
                       int dyear = Convert.ToInt32(a[2]);

                       string monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dmonth);
                       //string Mname=FromDate.ToString( "MMMM");   // this one will also work to get the month name

                       string Repfor = "Order Details Between - " + " " + SFromDVal + " " + "- " + SToDVal + " ";
                       CogitoStreamLineEntities dc = new CogitoStreamLineEntities();


 //                      SELECT Vw_OrderList.Pk_Order, Vw_OrderList.Product, Vw_OrderList.CustomerName, Vw_OrderList.Pk_Customer, 
 //                      Vw_OrderList.OrderDate, Vw_OrderList.Name, Vw_OrderList.PName, Vw_OrderList.OrdQty, Vw_OrderList.Rate
 //FROM   BalajiTesting.dbo.Vw_OrderList Vw_OrderList
 //ORDER BY Vw_OrderList.CustomerName, Vw_OrderList.Pk_Order



                       List<Vw_OrderList> BillList = new List<Vw_OrderList>();
                       // CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                       BillList = dc.Vw_OrderList.Where(x => x.OrderDate >= SFrom && x.OrderDate <= STo).OrderBy(x => x.OrderDate).Select(x => x).OfType<Vw_OrderList>().ToList();
                       if (BillList.Count > 0)
                       {
                           DataTable dt = new DataTable("OrderDetails");

                           dt.Columns.Add("Name");
                           dt.Columns.Add("PName");
                           dt.Columns.Add("OrderDate");
                           dt.Columns.Add("CustomerName");
                           dt.Columns.Add("Rate");
                             dt.Columns.Add("Pk_OrderChild");
                           dt.Columns.Add("Pk_Order");
                           dt.Columns.Add("OrdQty");
                           dt.Columns.Add("Product");
                           dt.Columns.Add("Cust_PO");
                           


                           foreach (Vw_OrderList entity in BillList)
                           {
                               DataRow row = dt.NewRow();

                               row["Pk_OrderChild"] = entity.Pk_OrderChild;
                               row["Name"] = entity.Name;
                               row["PName"] = entity.PName;
                               row["OrderDate"] = entity.OrderDate;
                               row["CustomerName"] = entity.CustomerName;
                               row["Pk_Order"] = entity.Pk_Order;
                               row["OrdQty"] = entity.OrdQty;
                               row["Product"] = entity.Product;
                               row["Rate"] = entity.Rate;
                               row["Cust_PO"] = entity.Cust_PO;
                               dt.Rows.Add(row);

                           }

                           DataSet ds = new DataSet();
                           ds.Tables.Add(dt);
                           CogitoStreamline.Report.OrdDates orp = new CogitoStreamline.Report.OrdDates();

                           orp.Load("@\\Report\\OrdDates.rpt");
                           orp.SetDataSource(dt.DefaultView);



                           ReportDocument repDoc = orp;


                           ParameterFieldDefinitions crParameterFieldDefinitions4;
                           ParameterFieldDefinition crParameterFieldDefinition4;
                           ParameterValues crParameterValues4 = new ParameterValues();
                           ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                           crParameterDiscreteValue4.Value = Repfor;
                           crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                           crParameterFieldDefinition4 = crParameterFieldDefinitions4["period"];
                           crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                           crParameterValues4.Clear();
                           crParameterValues4.Add(crParameterDiscreteValue4);
                           crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);

                           Response.Buffer = false;
                           Response.ClearContent();
                           Response.ClearHeaders(); 
                           string pdfPath = Server.MapPath("~/ConvertPDF/" + "OrderDates" + ".pdf");
                           FileInfo file = new FileInfo(pdfPath);
                           if (file.Exists)
                           {
                               file.Delete();
                           }
                           var pd = new PrintDocument();


                           //orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           //orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           ////  CreateBookmarksFromGroupTree = true;
                           ////   pdffprnat.CreateBookmarksFromGroupTree = true;
                           //DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                           //objDiskOpt.DiskFileName = pdfPath;

                           //orp.ExportOptions.DestinationOptions = objDiskOpt;
                           //orp.Export();


                           ExportOptions rptOptions = new ExportOptions();
                           PdfFormatOptions pdffprnat = new PdfFormatOptions();
                           pdffprnat.CreateBookmarksFromGroupTree = true;
                           rptOptions.ExportFormatOptions = pdffprnat;
                           rptOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           rptOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           DiskFileDestinationOptions dest = new DiskFileDestinationOptions();
                           dest.DiskFileName = pdfPath;
                           rptOptions.ExportDestinationOptions = dest;
                           orp.Export(rptOptions);

                       }
                       else
                       {

                       }
                       return null;
                   }
                   catch (Exception ex)
                   {
                       return null;
                       //  Response.Redirect("Index");D:\shantha\Billing\CogitoStreamline\CogitoStreamline\Controllers\CartonBillingController.cs
                   }
               }

               public JsonResult UnSchOrders([System.Web.Http.FromBody]string data)
               {
                   try
                   {

                       data = data.TrimStart('=');
                       int i = 0;

                       Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

                       DateTime SFrom = Convert.ToDateTime(oValues["FDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       DateTime STo = Convert.ToDateTime(oValues["TDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       string[] a = oValues["FDate"].ToString().Split('/');
                       DateTime FromDate = DateTime.ParseExact(a[1] + '/' + a[0] + '/' + a[2] + " 12:00", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);
                       string SFromDVal;
                       var FD = SFrom.Day;
                       var FM = SFrom.ToString("MMM");
                       var FY = SFrom.Year;


                       SFromDVal = FD.ToString() + '/' + FM.ToString() + '/' + FY.ToString();



                       var TD = STo.Day;
                       var TM = STo.ToString("MMM");
                       var TY = STo.Year;

                       string SToDVal;
                       //if (TD > 12)
                       //{ SToDVal = TM.ToString() + '/' + TD.ToString() + '/' + TY.ToString(); }
                       //else
                       SToDVal = TD.ToString() + '/' + TM.ToString() + '/' + TY.ToString();


                       int dDay = Convert.ToInt32(a[0]);
                       int dmonth = Convert.ToInt32(a[1]);
                       int dyear = Convert.ToInt32(a[2]);

                       string monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dmonth);
                       //string Mname=FromDate.ToString( "MMMM");   // this one will also work to get the month name

                       string Repfor = "Order Details Between - " + " " + SFromDVal + " " + "- " + SToDVal + " ";
                       CogitoStreamLineEntities dc = new CogitoStreamLineEntities();


                       //                      SELECT Vw_UnScheduled_Orders.Pk_Order, Vw_UnScheduled_Orders.Product, Vw_UnScheduled_Orders.CustomerName, Vw_UnScheduled_Orders.Pk_Customer, 
                       //                      Vw_UnScheduled_Orders.OrderDate, Vw_UnScheduled_Orders.Name, Vw_UnScheduled_Orders.PName, Vw_UnScheduled_Orders.OrdQty, Vw_UnScheduled_Orders.Rate
                       //FROM   BalajiTesting.dbo.Vw_UnScheduled_Orders Vw_UnScheduled_Orders
                       //ORDER BY Vw_UnScheduled_Orders.CustomerName, Vw_UnScheduled_Orders.Pk_Order



                       List<Vw_UnScheduled_Orders> BillList = new List<Vw_UnScheduled_Orders>();
                       // CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                       BillList = dc.Vw_UnScheduled_Orders.Where(x => x.OrderDate >= SFrom && x.OrderDate <= STo).OrderBy(x => x.OrderDate).Select(x => x).OfType<Vw_UnScheduled_Orders>().ToList();
                       if (BillList.Count > 0)
                       {
                           DataTable dt = new DataTable("OrderDetails");

                           dt.Columns.Add("Name");
                           //dt.Columns.Add("PName");
                           dt.Columns.Add("OrderDate");
                           dt.Columns.Add("CustomerName");
                           //dt.Columns.Add("Rate");
                           //dt.Columns.Add("Pk_OrderChild");
                           dt.Columns.Add("Pk_Order");
                           dt.Columns.Add("OrdQty");
                           //dt.Columns.Add("Product");



                           foreach (Vw_UnScheduled_Orders entity in BillList)
                           {
                               DataRow row = dt.NewRow();

                               //row["Pk_OrderChild"] = entity.Pk_OrderChild;
                               row["Name"] = entity.Name;
                               //row["PName"] = entity.PName;
                               row["OrderDate"] = entity.OrderDate;
                               row["CustomerName"] = entity.CustomerName;
                               row["Pk_Order"] = entity.Pk_Order;
                               row["OrdQty"] = entity.OrdQty;
                               //row["Product"] = entity.Product;
                               //row["Rate"] = entity.Rate;
                               dt.Rows.Add(row);

                           }

                           DataSet ds = new DataSet();
                           ds.Tables.Add(dt);
                           CogitoStreamline.Report.UnScheduled_Orders orp = new CogitoStreamline.Report.UnScheduled_Orders();

                           orp.Load("@\\Report\\UnScheduled_Orders.rpt");
                           orp.SetDataSource(dt.DefaultView);



                           ReportDocument repDoc = orp;


                           ParameterFieldDefinitions crParameterFieldDefinitions4;
                           ParameterFieldDefinition crParameterFieldDefinition4;
                           ParameterValues crParameterValues4 = new ParameterValues();
                           ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                           crParameterDiscreteValue4.Value = Repfor;
                           crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                           crParameterFieldDefinition4 = crParameterFieldDefinitions4["period"];
                           crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                           crParameterValues4.Clear();
                           crParameterValues4.Add(crParameterDiscreteValue4);
                           crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);

                           Response.Buffer = false;
                           Response.ClearContent();
                           Response.ClearHeaders(); 


                           string pdfPath = Server.MapPath("~/ConvertPDF/" + "UnSchOrd" + ".pdf");
                           FileInfo file = new FileInfo(pdfPath);
                           if (file.Exists)
                           {
                               file.Delete();
                           }
                           var pd = new PrintDocument();


                           //orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           //orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           ////  CreateBookmarksFromGroupTree = true;
                           ////   pdffprnat.CreateBookmarksFromGroupTree = true;
                           //DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                           //objDiskOpt.DiskFileName = pdfPath;

                           //orp.ExportOptions.DestinationOptions = objDiskOpt;
                           //orp.Export();


                           ExportOptions rptOptions = new ExportOptions();
                           PdfFormatOptions pdffprnat = new PdfFormatOptions();
                           pdffprnat.CreateBookmarksFromGroupTree = true;
                           rptOptions.ExportFormatOptions = pdffprnat;
                           rptOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           rptOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           DiskFileDestinationOptions dest = new DiskFileDestinationOptions();
                           dest.DiskFileName = pdfPath;
                           rptOptions.ExportDestinationOptions = dest;
                           orp.Export(rptOptions);

                       }
                       else
                       {

                       }
                       return null;
                   }
                   catch (Exception ex)
                   {
                       return null;
                       //  Response.Redirect("Index");D:\shantha\Billing\CogitoStreamline\CogitoStreamline\Controllers\CartonBillingController.cs
                   }
               }

               public JsonResult SchAgstPrdn([System.Web.Http.FromBody]string data)
               {
                   try
                   {

                       data = data.TrimStart('=');
                       int i = 0;

                       Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

                       DateTime SFrom = Convert.ToDateTime(oValues["FDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       DateTime STo = Convert.ToDateTime(oValues["TDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       string[] a = oValues["FDate"].ToString().Split('/');
                       DateTime FromDate = DateTime.ParseExact(a[1] + '/' + a[0] + '/' + a[2] + " 12:00", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);
                       string SFromDVal;
                       var FD = SFrom.Day;
                       var FM = SFrom.ToString("MMM");
                       var FY = SFrom.Year;


                       SFromDVal = FD.ToString() + '/' + FM.ToString() + '/' + FY.ToString();



                       var TD = STo.Day;
                       var TM = STo.ToString("MMM");
                       var TY = STo.Year;

                       string SToDVal;
                       //if (TD > 12)
                       //{ SToDVal = TM.ToString() + '/' + TD.ToString() + '/' + TY.ToString(); }
                       //else
                       SToDVal = TD.ToString() + '/' + TM.ToString() + '/' + TY.ToString();


                       int dDay = Convert.ToInt32(a[0]);
                       int dmonth = Convert.ToInt32(a[1]);
                       int dyear = Convert.ToInt32(a[2]);

                       string monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dmonth);
                       //string Mname=FromDate.ToString( "MMMM");   // this one will also work to get the month name

                       string Repfor = "Order Details Between - " + " " + SFromDVal + " " + "- " + SToDVal + " ";
                       CogitoStreamLineEntities dc = new CogitoStreamLineEntities();


                       //                      SELECT Vw_SchAndPrd.Pk_Order, Vw_SchAndPrd.Pk_JobCardID, Vw_SchAndPrd.CustomerName, Vw_SchAndPrd.Pk_Customer, 
                       //                      Vw_SchAndPrd.OrderDate, Vw_SchAndPrd.Name, Vw_SchAndPrd.PName, Vw_SchAndPrd.OrdQty, Vw_SchAndPrd.Pk_DeliverySechedule
                       //FROM   BalajiTesting.dbo.Vw_SchAndPrd Vw_SchAndPrd
                       //ORDER BY Vw_SchAndPrd.CustomerName, Vw_SchAndPrd.Pk_Order



                       List<Vw_SchAndPrd> BillList = new List<Vw_SchAndPrd>();
                       // CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                       BillList = dc.Vw_SchAndPrd.Where(x => x.OrderDate >= SFrom && x.OrderDate <= STo).OrderBy(x => x.OrderDate).Select(x => x).OfType<Vw_SchAndPrd>().ToList();
                       if (BillList.Count > 0)
                       {
                           DataTable dt = new DataTable("OrderDetails");

                           dt.Columns.Add("Name");
                           dt.Columns.Add("PName");
                           dt.Columns.Add("OrderDate");
                           dt.Columns.Add("CustomerName");
                           dt.Columns.Add("Pk_DeliverySechedule");
                           dt.Columns.Add("Pk_BoxID");
                           dt.Columns.Add("Pk_Order");
                           dt.Columns.Add("OrdQty");
                           dt.Columns.Add("Pk_JobCardID");
                           dt.Columns.Add("SchQty");
                           dt.Columns.Add("PrdnQty");
                           dt.Columns.Add("ActQty");


                           foreach (Vw_SchAndPrd entity in BillList)
                           {
                               DataRow row = dt.NewRow();

                               row["Pk_BoxID"] = entity.Pk_BoxID;
                               row["Name"] = entity.Name;
                               row["PName"] = entity.PName;
                               row["OrderDate"] = entity.OrderDate;
                               row["CustomerName"] = entity.CustomerName;
                               row["Pk_Order"] = entity.Pk_Order;
                               row["OrdQty"] = entity.OrdQty;
                               row["PrdnQty"] = entity.PrdnQty;
                               row["ActQty"] = entity.ActQty;
                               row["SchQty"] = entity.SchQty;
                               row["Pk_JobCardID"] = entity.Pk_JobCardID;
                               row["Pk_DeliverySechedule"] = entity.Pk_DeliverySechedule;
                               dt.Rows.Add(row);

                           }

                           DataSet ds = new DataSet();
                           ds.Tables.Add(dt);
                           CogitoStreamline.Report.SchAndPrd orp = new CogitoStreamline.Report.SchAndPrd();

                           orp.Load("@\\Report\\SchAndPrd.rpt");
                           orp.SetDataSource(dt.DefaultView);



                           ReportDocument repDoc = orp;


                           ParameterFieldDefinitions crParameterFieldDefinitions4;
                           ParameterFieldDefinition crParameterFieldDefinition4;
                           ParameterValues crParameterValues4 = new ParameterValues();
                           ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                           crParameterDiscreteValue4.Value = Repfor;
                           crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                           crParameterFieldDefinition4 = crParameterFieldDefinitions4["period"];
                           crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                           crParameterValues4.Clear();
                           crParameterValues4.Add(crParameterDiscreteValue4);
                           crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);

                           Response.Buffer = false;
                           Response.ClearContent();
                           Response.ClearHeaders();
                           string pdfPath = Server.MapPath("~/ConvertPDF/" + "Prdn_Diff" + ".pdf");
                           FileInfo file = new FileInfo(pdfPath);
                           if (file.Exists)
                           {
                               file.Delete();
                           }
                           var pd = new PrintDocument();


                           //orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           //orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           ////  CreateBookmarksFromGroupTree = true;
                           ////   pdffprnat.CreateBookmarksFromGroupTree = true;
                           //DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                           //objDiskOpt.DiskFileName = pdfPath;

                           //orp.ExportOptions.DestinationOptions = objDiskOpt;
                           //orp.Export();


                           ExportOptions rptOptions = new ExportOptions();
                           PdfFormatOptions pdffprnat = new PdfFormatOptions();
                           pdffprnat.CreateBookmarksFromGroupTree = true;
                           rptOptions.ExportFormatOptions = pdffprnat;
                           rptOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           rptOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           DiskFileDestinationOptions dest = new DiskFileDestinationOptions();
                           dest.DiskFileName = pdfPath;
                           rptOptions.ExportDestinationOptions = dest;
                           orp.Export(rptOptions);

                       }
                       else
                       {

                       }
                       return null;
                   }
                   catch (Exception ex)
                   {
                       return null;
                       //  Response.Redirect("Index");D:\shantha\Billing\CogitoStreamline\CogitoStreamline\Controllers\CartonBillingController.cs
                   }
               }

               public JsonResult InwardDates([System.Web.Http.FromBody]string data)
               {
                   try
                   {

                       data = data.TrimStart('=');
                       int i = 0;

                       Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

                       DateTime SFrom = Convert.ToDateTime(oValues["FDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       DateTime STo = Convert.ToDateTime(oValues["TDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       string[] a = oValues["FDate"].ToString().Split('/');
                       DateTime FromDate = DateTime.ParseExact(a[1] + '/' + a[0] + '/' + a[2] + " 12:00", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);
                       string SFromDVal;
                       var FD = SFrom.Day;
                       var FM = SFrom.ToString("MMM");
                       var FY = SFrom.Year;


                       SFromDVal = FD.ToString() + '/' + FM.ToString() + '/' + FY.ToString();



                       var TD = STo.Day;
                       var TM = STo.ToString("MMM");
                       var TY = STo.Year;

                       string SToDVal;
                       //if (TD > 12)
                       //{ SToDVal = TM.ToString() + '/' + TD.ToString() + '/' + TY.ToString(); }
                       //else
                       SToDVal = TD.ToString() + '/' + TM.ToString() + '/' + TY.ToString();


                       int dDay = Convert.ToInt32(a[0]);
                       int dmonth = Convert.ToInt32(a[1]);
                       int dyear = Convert.ToInt32(a[2]);

                       string monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dmonth);
                       //string Mname=FromDate.ToString( "MMMM");   // this one will also work to get the month name

                       string Repfor = "Inward Details Between - " + " " + SFromDVal + " " + "- " + SToDVal + " ";
                       CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

 //                       SELECT Vw_MatInwdDetails.MatName, Vw_MatInwdDetails.Pk_Inward, Vw_MatInwdDetails.Inward_Date, 
 //                      Vw_MatInwdDetails.PONo, Vw_MatInwdDetails.Sqty
 //FROM   BalajiTesting.dbo.Vw_MatInwdDetails Vw_MatInwdDetails
 //ORDER BY Vw_MatInwdDetails.Pk_Inward




                       List<Vw_MatInwdDetails> BillList = new List<Vw_MatInwdDetails>();
                       // CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                       BillList = dc.Vw_MatInwdDetails.Where(x => x.Inward_Date >= SFrom && x.Inward_Date <= STo).OrderBy(x => x.Inward_Date).Select(x => x).OfType<Vw_MatInwdDetails>().ToList();
                       if (BillList.Count > 0)
                       {
                           DataTable dt = new DataTable("DeliverySchedule");

                           dt.Columns.Add("MatName");
                           dt.Columns.Add("Pk_Inward");
                           dt.Columns.Add("Inward_Date");
                           dt.Columns.Add("PONo");
                           dt.Columns.Add("Sqty");
                           dt.Columns.Add("VendorName");



                           foreach (Vw_MatInwdDetails entity in BillList)
                           {
                               DataRow row = dt.NewRow();

                               //row["Pk_InwardDet"] = entity.Pk_InwardDet;
                               row["MatName"] = entity.MatName;
                               row["Pk_Inward"] = entity.Pk_Inward;
                               row["Inward_Date"] = entity.Inward_Date;
                               row["PONo"] = entity.PONo;
                               row["Sqty"] = entity.Sqty;
                               row["VendorName"] = entity.VendorName;


                               dt.Rows.Add(row);

                           }

                           DataSet ds = new DataSet();
                           ds.Tables.Add(dt);
                           CogitoStreamline.Report.InwdDates orp = new CogitoStreamline.Report.InwdDates();

                           orp.Load("@\\Report\\InwdDates.rpt");
                           orp.SetDataSource(dt.DefaultView);



                           ReportDocument repDoc = orp;


                           ParameterFieldDefinitions crParameterFieldDefinitions4;
                           ParameterFieldDefinition crParameterFieldDefinition4;
                           ParameterValues crParameterValues4 = new ParameterValues();
                           ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                           crParameterDiscreteValue4.Value = Repfor;
                           crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                           crParameterFieldDefinition4 = crParameterFieldDefinitions4["period"];
                           crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                           crParameterValues4.Clear();
                           crParameterValues4.Add(crParameterDiscreteValue4);
                           crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);


                           Response.Buffer = false;
                           Response.ClearContent();
                           Response.ClearHeaders(); 
                           string pdfPath = Server.MapPath("~/ConvertPDF/" + "InwdDates" + ".pdf");
                           FileInfo file = new FileInfo(pdfPath);
                           if (file.Exists)
                           {
                               file.Delete();
                           }
                           var pd = new PrintDocument();


                           //orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           //orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           ////  CreateBookmarksFromGroupTree = true;
                           ////   pdffprnat.CreateBookmarksFromGroupTree = true;
                           //DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                           //objDiskOpt.DiskFileName = pdfPath;

                           //orp.ExportOptions.DestinationOptions = objDiskOpt;
                           //orp.Export();


                           ExportOptions rptOptions = new ExportOptions();
                           PdfFormatOptions pdffprnat = new PdfFormatOptions();
                           pdffprnat.CreateBookmarksFromGroupTree = true;
                           rptOptions.ExportFormatOptions = pdffprnat;
                           rptOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           rptOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           DiskFileDestinationOptions dest = new DiskFileDestinationOptions();
                           dest.DiskFileName = pdfPath;
                           rptOptions.ExportDestinationOptions = dest;
                           orp.Export(rptOptions);

                       }
                       else
                       {

                       }
                       return null;
                   }
                   catch (Exception ex)
                   {
                       return null;
                       //  Response.Redirect("Index");D:\shantha\Billing\CogitoStreamline\CogitoStreamline\Controllers\CartonBillingController.cs
                   }
               }


               public JsonResult SalesOrder_Tracker([System.Web.Http.FromBody]string data)
               {
                   try
                   {
                       data = data.TrimStart('=');
                       int i = 0;
              
                       Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

                       DateTime SFrom = Convert.ToDateTime(oValues["FDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       DateTime STo = Convert.ToDateTime(oValues["TDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       string[] a = oValues["FDate"].ToString().Split('/');
                       DateTime FromDate = DateTime.ParseExact(a[1] + '/' + a[0] + '/' + a[2] + " 12:00", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);
                       string SFromDVal;
                       var FD = SFrom.Day;
                       var FM = SFrom.ToString("MMM");
                       var FY = SFrom.Year;


                       SFromDVal = FD.ToString() + '/' + FM.ToString() + '/' + FY.ToString();



                       var TD = STo.Day;
                       var TM = STo.ToString("MMM");
                       var TY = STo.Year;

                       string SToDVal;
                       //if (TD > 12)
                       //{ SToDVal = TM.ToString() + '/' + TD.ToString() + '/' + TY.ToString(); }
                       //else
                       SToDVal = TD.ToString() + '/' + TM.ToString() + '/' + TY.ToString();


                       int dDay = Convert.ToInt32(a[0]);
                       int dmonth = Convert.ToInt32(a[1]);
                       int dyear = Convert.ToInt32(a[2]);

                       string monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dmonth);
                       //string Mname=FromDate.ToString( "MMMM");   // this one will also work to get the month name

                       string Repfor = "Sales Order Tracking Between - " + " " + SFromDVal + " " + "- " + SToDVal + " ";
                       CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                       List<Vw_SalesOrderTracker> BillList = new List<Vw_SalesOrderTracker>();
                       //CogitoStreamLineEntities dc = new CogitoStreamLineEntities();
                       //STo = STo.AddDays(1);
                       //BillList = dc.Vw_DeliveryScheduleReport.Where(x => x.DeliveryDate.Month == dmonth && x.DeliveryDate.Year == dyear && x.DeliveryDate.Day == dDay).Select(x => x).OfType<Vw_DeliveryScheduleReport>().ToList();
                       BillList = dc.Vw_SalesOrderTracker.Where(x => x.OrderDate >= SFrom && x.OrderDate <= STo).OrderBy(x => x.OrderDate).Select(x => x).OfType<Vw_SalesOrderTracker>().ToList();
                       if (BillList.Count > 0)
                       {
                           DataTable dt = new DataTable("SalesOrder");



                           dt.Columns.Add("OrdQty");
                           dt.Columns.Add("Pk_Order");
                           dt.Columns.Add("OrderDate");
                           dt.Columns.Add("Name");
                           dt.Columns.Add("PName");
                            dt.Columns.Add("Pk_Invoice");
                            dt.Columns.Add("InvDate");
                            dt.Columns.Add("Quantity");
                            dt.Columns.Add("Pk_BoxID");
                          dt.Columns.Add("Cust_PO");
                          dt.Columns.Add("CustOrdDate");
                          dt.Columns.Add("CustomerName");

                           foreach (Vw_SalesOrderTracker entity in BillList)
                           {
                               DataRow row = dt.NewRow();

                               row["OrdQty"] = entity.OrdQty;
                               row["Pk_Order"] = entity.Pk_Order;
                               row["OrderDate"] = entity.OrderDate;
                               row["Name"] = entity.Name;
                               row["PName"] = entity.PName;
                               row["Pk_Invoice"] = entity.Pk_Invoice;
                               row["InvDate"] = entity.InvDate;
                               row["Quantity"] = entity.Quantity;
                               row["Pk_BoxID"] = entity.Pk_BoxID;
                               row["Cust_PO"] = entity.Cust_PO;
                               row["CustOrdDate"] = entity.CustOrdDate;
                               row["CustomerName"] = entity.CustomerName;
                               //row["Pk_DeliverySechedule"] = entity.Pk_DeliverySechedule;
                               dt.Rows.Add(row);

                           }

                           DataSet ds = new DataSet();
                           ds.Tables.Add(dt);
                           CogitoStreamline.Report.SalesOrder_Tracker orp = new CogitoStreamline.Report.SalesOrder_Tracker();

                           orp.Load("@\\Report\\SalesOrder_Tracker.rpt");
                           orp.SetDataSource(dt.DefaultView);



                           ReportDocument repDoc = orp;

                           ParameterFieldDefinitions crParameterFieldDefinitions4;
                           ParameterFieldDefinition crParameterFieldDefinition4;
                           ParameterValues crParameterValues4 = new ParameterValues();
                           ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                           crParameterDiscreteValue4.Value = Repfor;
                           crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                           crParameterFieldDefinition4 = crParameterFieldDefinitions4["period"];
                           crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                           crParameterValues4.Clear();
                           crParameterValues4.Add(crParameterDiscreteValue4);
                           crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);

                           Response.Buffer = false;
                           Response.ClearContent();
                           Response.ClearHeaders(); 


                           string pdfPath = Server.MapPath("~/ConvertPDF/" + "SalesOrder_Tracker" + ".pdf");
                           FileInfo file = new FileInfo(pdfPath);
                           if (file.Exists)
                           {
                               file.Delete();
                           }
                           var pd = new PrintDocument();

                           ExportOptions rptOptions = new ExportOptions();
                           PdfFormatOptions pdffprnat = new PdfFormatOptions();
                           pdffprnat.CreateBookmarksFromGroupTree = true;
                           rptOptions.ExportFormatOptions = pdffprnat;
                           rptOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           rptOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           DiskFileDestinationOptions dest = new DiskFileDestinationOptions();
                           dest.DiskFileName = pdfPath;
                           rptOptions.ExportDestinationOptions = dest;
                           orp.Export(rptOptions);




                           //orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           //orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           //DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                           //objDiskOpt.DiskFileName = pdfPath;

                           //orp.ExportOptions.DestinationOptions = objDiskOpt;
                           //orp.Export();

                       }
                       return null;
                   }
                   catch (Exception ex)
                   {
                       return null;
                    
                   }
               }

               public JsonResult TrackOrder([System.Web.Http.FromBody]string data)
               {
                   try
                   {

                       data = data.TrimStart('=');
                       int i = 0;

                       Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

                       DateTime SFrom = Convert.ToDateTime(oValues["FDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       DateTime STo = Convert.ToDateTime(oValues["TDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       string[] a = oValues["FDate"].ToString().Split('/');
                       DateTime FromDate = DateTime.ParseExact(a[1] + '/' + a[0] + '/' + a[2] + " 12:00", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);
                       string SFromDVal;




                       var FD = SFrom.Day;
                       var FM = SFrom.ToString("MMM");
                       var FY = SFrom.Year;
                       var FM1 = SFrom.Month;


                       SFromDVal = FD.ToString() + '/' + FM.ToString() + '/' + FY.ToString();



                       var TD = STo.Day;
                       var TM = STo.ToString("MMM");
                       var TY = STo.Year;
                       var TM1 = STo.Month;

                       string SToDVal;
                       //if (TD > 12)
                       //{ SToDVal = TM.ToString() + '/' + TD.ToString() + '/' + TY.ToString(); }
                       //else
                       SToDVal = TD.ToString() + '/' + TM.ToString() + '/' + TY.ToString();


                       int dDay = Convert.ToInt32(a[0]);
                       int dmonth = Convert.ToInt32(a[1]);
                       int dyear = Convert.ToInt32(a[2]);

                       string monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dmonth);
                       //string Mname=FromDate.ToString( "MMMM");   // this one will also work to get the month name

                       string Repfor = "Order/Bill Details Between - " + " " + SFromDVal + " " + "- " + SToDVal + " ";
                       CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                       //                       SELECT Vw_Sales_Details.Pk_Invoice, Vw_Sales_Details.InvDate, Vw_Sales_Details.CustomerName,
                       //                      Vw_Sales_Details.Quantity, Vw_Sales_Details.Price, Vw_Sales_Details.GrandTotal, Vw_Sales_Details.NETVALUE, 
                       //                      Vw_Sales_Details.TaxType, Vw_Sales_Details.TaxName
                       //FROM   BalajiTesting.dbo.Vw_Sales_Details Vw_Sales_Details


                       List<Vw_SOrderTracker> BillList = new List<Vw_SOrderTracker>();
                       // CogitoStreamLineEntities dc = new CogitoStreamLineEntities();
                       //STo = STo.AddDays(1);
                       BillList = dc.Vw_SOrderTracker.Where(x => x.OrderDate >= SFrom && x.OrderDate <= STo).OrderBy(x => x.OrderDate).Select(x => x).OfType<Vw_SOrderTracker>().ToList();

                       if (BillList.Count > 0)
                       {
                           //var CtrVal = BillList.Count;
                           DataTable dt = new DataTable("InvoiceBill");

                           dt.Columns.Add("Name");
                           dt.Columns.Add("Pk_Order");
                           dt.Columns.Add("OrderDate");
                           dt.Columns.Add("OrdQty");
                           dt.Columns.Add("Fk_PartID");
                           dt.Columns.Add("Pk_Invoice");
                           dt.Columns.Add("InvDate");
                           //dt.Columns.Add("Pk_DeliverySechedule");
                           //dt.Columns.Add("DeliveryDate");
                           dt.Columns.Add("Quantity");
                           //dt.Columns.Add("PrdnQty");
                           dt.Columns.Add("Pk_BoxID");
                           //dt.Columns.Add("JDate");


                           dt.Columns.Add("PName");

                           foreach (Vw_SOrderTracker entity in BillList)
                           {
                               DataRow row = dt.NewRow();


                               row["Pk_Order"] = entity.Pk_Order;
                               row["OrderDate"] = entity.OrderDate;
                               row["OrdQty"] = entity.OrdQty;
                               //row["SchQty"] = entity.SchQty;
                               row["Pk_Invoice"] = entity.Pk_Invoice;
                               row["InvDate"] = entity.InvDate;
                               //row["Pk_DeliverySechedule"] = entity.Pk_DeliverySechedule;
                               //row["DeliveryDate"] = entity.DeliveryDate;
                               row["Quantity"] = entity.Quantity;
                               //row["PrdnQty"] = entity.PrdnQty;
                               row["Pk_BoxID"] = entity.Pk_BoxID;
                               row["Fk_PartID"] = entity.Fk_PartID;
                               row["Name"] = entity.Name;
                               row["PName"] = entity.PName;
                               dt.Rows.Add(row);

                             

                           }

                           DataSet ds = new DataSet();
                           ds.Tables.Add(dt);
                           CogitoStreamline.Report.OrderTracker orp = new CogitoStreamline.Report.OrderTracker();

                           orp.Load("@\\Report\\OrderTracker.rpt");
                           orp.SetDataSource(dt.DefaultView);



                           ReportDocument repDoc = orp;


                           ParameterFieldDefinitions crParameterFieldDefinitions4;
                           ParameterFieldDefinition crParameterFieldDefinition4;
                           ParameterValues crParameterValues4 = new ParameterValues();
                           ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                           crParameterDiscreteValue4.Value = Repfor;
                           crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                           crParameterFieldDefinition4 = crParameterFieldDefinitions4["period"];
                           crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                           crParameterValues4.Clear();
                           crParameterValues4.Add(crParameterDiscreteValue4);
                           crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);

                           Response.Buffer = false;
                           Response.ClearContent();
                           Response.ClearHeaders();

                           string pdfPath = Server.MapPath("~/ConvertPDF/" + "OrderTracker" + ".pdf");
                           FileInfo file = new FileInfo(pdfPath);
                           if (file.Exists)
                           {
                               file.Delete();
                           }
                           var pd = new PrintDocument();


                           //orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           //orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           ////  CreateBookmarksFromGroupTree = true;
                           ////   pdffprnat.CreateBookmarksFromGroupTree = true;
                           //DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                           //objDiskOpt.DiskFileName = pdfPath;

                           //orp.ExportOptions.DestinationOptions = objDiskOpt;
                           //orp.Export();


                           ExportOptions rptOptions = new ExportOptions();
                           PdfFormatOptions pdffprnat = new PdfFormatOptions();
                           pdffprnat.CreateBookmarksFromGroupTree = true;
                           rptOptions.ExportFormatOptions = pdffprnat;
                           rptOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           rptOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           DiskFileDestinationOptions dest = new DiskFileDestinationOptions();
                           dest.DiskFileName = pdfPath;
                           rptOptions.ExportDestinationOptions = dest;
                           orp.Export(rptOptions);

                       }
                       else
                       {

                       }
                       return null;
                   }
                   catch (Exception ex)
                   {
                       return null;
                       //  Response.Redirect("Index");D:\shantha\Billing\CogitoStreamline\CogitoStreamline\Controllers\CartonBillingController.cs
                   }
               }

               public JsonResult IndentToPO([System.Web.Http.FromBody]string data)
               {
                   try
                   {
                       data = data.TrimStart('=');
                       int i = 0;
                     
                       Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                       List<Vw_IndentPOSearch> BillList = new List<Vw_IndentPOSearch>();
                       CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                       //BillList = dc.Vw_DeliveryScheduleReport.Where(x => x.DeliveryDate.Month == dmonth && x.DeliveryDate.Year == dyear && x.DeliveryDate.Day == dDay).Select(x => x).OfType<Vw_DeliveryScheduleReport>().ToList();
                       BillList = dc.Vw_IndentPOSearch.Where(p=>p.Fk_PONo==null).OrderBy(x => x.Pk_MaterialOrderMasterId).Select(x => x).OfType<Vw_IndentPOSearch>().ToList();
                      
                       
 //                       SELECT Vw_IndentPOSearch.Pk_MaterialOrderMasterId, Vw_IndentPOSearch.Name, Vw_IndentPOSearch.POQty,
 //                      Vw_IndentPOSearch.PendingQty, Vw_IndentPOSearch.MaterialIndentDate
 //FROM   BalajiTesting.dbo.Vw_IndentPOSearch Vw_IndentPOSearch
 //ORDER BY Vw_IndentPOSearch.Pk_MaterialOrderMasterId

                       
                       if (BillList.Count > 0)
                       {
                           DataTable dt = new DataTable("SalesOrder");
                           dt.Columns.Add("Pk_MaterialOrderMasterId");
                           dt.Columns.Add("Name");
                           dt.Columns.Add("POQty");
                           dt.Columns.Add("PendingQty");
                           dt.Columns.Add("MaterialIndentDate");
                          dt.Columns.Add("IndQty");
                           //dt.Columns.Add("InvDate");
                           //dt.Columns.Add("Quantity");
                           //dt.Columns.Add("Pk_BoxID");
                           //dt.Columns.Add("Cust_PO");
                           foreach (Vw_IndentPOSearch entity in BillList)
                           {
                               DataRow row = dt.NewRow();

                               row["Pk_MaterialOrderMasterId"] = entity.Pk_MaterialOrderMasterId;
                               row["Name"] = entity.Name;
                               row["POQty"] = entity.POQty;
                               row["PendingQty"] = entity.PendingQty;
                               row["MaterialIndentDate"] = entity.MaterialIndentDate;
                               row["IndQty"] = entity.IndQty;
                               //row["InvDate"] = entity.InvDate;
                               //row["Quantity"] = entity.Quantity;
                               //row["Pk_BoxID"] = entity.Pk_BoxID;
                               //row["Cust_PO"] = entity.Cust_PO;

                               //row["Pk_DeliverySechedule"] = entity.Pk_DeliverySechedule;
                               dt.Rows.Add(row);

                           }

                           DataSet ds = new DataSet();
                           ds.Tables.Add(dt);
                           CogitoStreamline.Report.Indent_To_PO orp = new CogitoStreamline.Report.Indent_To_PO();

                           orp.Load("@\\Report\\Indent_To_PO.rpt");
                           orp.SetDataSource(dt.DefaultView);



                           ReportDocument repDoc = orp;

                           Response.Buffer = false;
                           Response.ClearContent();
                           Response.ClearHeaders(); 
                           string pdfPath = Server.MapPath("~/ConvertPDF/" + "IndentToPO" + ".pdf");
                           FileInfo file = new FileInfo(pdfPath);
                           if (file.Exists)
                           {
                               file.Delete();
                           }
                           var pd = new PrintDocument();


                           orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                           objDiskOpt.DiskFileName = pdfPath;

                           orp.ExportOptions.DestinationOptions = objDiskOpt;
                           orp.Export();

                       }
                       return null;
                   }
                   catch (Exception ex)
                   {
                       return null;

                   }
               }

               public ActionResult Assigned(string data = "")
               {
                   try
                   {

                       List<Vw_AssignedStock> BillList = new List<Vw_AssignedStock>();
                       CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                       //BillList = dc.Vw_DeliveryScheduleReport.Where(x => x.DeliveryDate.Month == dmonth && x.DeliveryDate.Year == dyear && x.DeliveryDate.Day == dDay).Select(x => x).OfType<Vw_DeliveryScheduleReport>().ToList();
                       BillList = dc.Vw_AssignedStock.Where(x => x.Fk_Status != 4).OrderBy(x => x.Pk_JobCardID).Select(x => x).OfType<Vw_AssignedStock>().ToList();
                       if (BillList.Count > 0)
                       {
                           DataTable dt = new DataTable("AssignedStock");

                           //                          SELECT Vw_AssignedStock.RollNo, Vw_AssignedStock.Name, Vw_AssignedStock.Pk_JobCardID, Vw_AssignedStock.Pk_Material,
                           //                          Vw_AssignedStock.Quantity, Vw_AssignedStock.MillName, Vw_AssignedStock.ColorName
                           //FROM   V2Innovatives.dbo.Vw_AssignedStock Vw_AssignedStock
                           //ORDER BY Vw_AssignedStock.Pk_JobCardID, Vw_AssignedStock.Pk_Material




                           dt.Columns.Add("RollNo");
                           dt.Columns.Add("Name");
                           dt.Columns.Add("Pk_JobCardID");
                           dt.Columns.Add("Pk_Material");
                           dt.Columns.Add("Quantity");
                           dt.Columns.Add("MillName");
                           dt.Columns.Add("ColorName");
                           dt.Columns.Add("CustomerName");
                           dt.Columns.Add("BoxName");


                           foreach (Vw_AssignedStock entity in BillList)
                           {
                               DataRow row = dt.NewRow();

                               row["RollNo"] = entity.RollNo;
                               row["Name"] = entity.Name;
                               row["Pk_JobCardID"] = entity.Pk_JobCardID;
                               row["Pk_Material"] = entity.Pk_Material;
                               row["Quantity"] = entity.Quantity;
                               row["MillName"] = entity.MillName;
                               row["ColorName"] = entity.ColorName;

                               row["CustomerName"] = entity.CustomerName;
                               row["BoxName"] = entity.BoxName;
                               dt.Rows.Add(row);

                           }

                           DataSet ds = new DataSet();
                           ds.Tables.Add(dt);
                           CogitoStreamline.Report.AssignedStock orp = new CogitoStreamline.Report.AssignedStock();

                           orp.Load("@\\Report\\AssignedStock.rpt");
                           orp.SetDataSource(dt.DefaultView);



                           ReportDocument repDoc = orp;


                           //ParameterFieldDefinitions crParameterFieldDefinitions4;
                           //ParameterFieldDefinition crParameterFieldDefinition4;
                           //ParameterValues crParameterValues4 = new ParameterValues();
                           //ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                           //crParameterDiscreteValue4.Value = Repfor;
                           //crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                           //crParameterFieldDefinition4 = crParameterFieldDefinitions4["period"];
                           //crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                           //crParameterValues4.Clear();
                           //crParameterValues4.Add(crParameterDiscreteValue4);
                           //crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);
                           Response.Buffer = false;
                           Response.ClearContent();
                           Response.ClearHeaders(); 

                           string pdfPath = Server.MapPath("~/ConvertPDF/" + "AssignedStock" + ".pdf");
                           FileInfo file = new FileInfo(pdfPath);
                           if (file.Exists)
                           {
                               file.Delete();
                           }
                           var pd = new PrintDocument();


                           orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                           objDiskOpt.DiskFileName = pdfPath;

                           orp.ExportOptions.DestinationOptions = objDiskOpt;
                           orp.Export();

                       }
                       return null;
                   }
                   catch (Exception ex)
                   {
                       return null;

                   }
               }

               public ActionResult JobWStock(string data = "")
               {
                   try
                   {

                       List<Vw_JobWorkStock> BillList = new List<Vw_JobWorkStock>();
                       CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                       //BillList = dc.Vw_DeliveryScheduleReport.Where(x => x.DeliveryDate.Month == dmonth && x.DeliveryDate.Year == dyear && x.DeliveryDate.Day == dDay).Select(x => x).OfType<Vw_DeliveryScheduleReport>().ToList();
                       BillList = dc.Vw_JobWorkStock.Where(x => x.StockValue>0).OrderBy(x => x.CustomerName).Select(x => x).OfType<Vw_JobWorkStock>().ToList();
                       if (BillList.Count > 0)
                       {
                           DataTable dt = new DataTable("JobWork");

                           //                          SELECT Vw_AssignedStock.RollNo, Vw_AssignedStock.Name, Vw_AssignedStock.Pk_JobCardID, Vw_AssignedStock.Pk_Material,
                           //                          Vw_AssignedStock.Quantity, Vw_AssignedStock.MillName, Vw_AssignedStock.ColorName
                           //FROM   V2Innovatives.dbo.Vw_AssignedStock Vw_AssignedStock
                           //ORDER BY Vw_AssignedStock.Pk_JobCardID, Vw_AssignedStock.Pk_Material




                           dt.Columns.Add("RollNo");
                           dt.Columns.Add("Name");
                           dt.Columns.Add("CustomerName");
                           dt.Columns.Add("Pk_Material");
                           dt.Columns.Add("StockValue");
                           dt.Columns.Add("MillName");
                           dt.Columns.Add("ColorName");
                           dt.Columns.Add("GSM");
                           dt.Columns.Add("BF");
                           dt.Columns.Add("Deckle");


                           foreach (Vw_JobWorkStock entity in BillList)
                           {
                               DataRow row = dt.NewRow();

                               row["RollNo"] = entity.RollNo;
                               row["Name"] = entity.Name;
                               row["CustomerName"] = entity.CustomerName;
                               row["Pk_Material"] = entity.Pk_Material;
                               row["StockValue"] = entity.StockValue;
                               row["MillName"] = entity.MillName;
                               row["ColorName"] = entity.ColorName;

                               row["GSM"] = entity.GSM;
                               row["Deckle"] = entity.Deckle;
                               row["BF"] = entity.BF;

                               dt.Rows.Add(row);

                           }

                           DataSet ds = new DataSet();
                           ds.Tables.Add(dt);
                           CogitoStreamline.Report.JWStock orp = new CogitoStreamline.Report.JWStock();

                           orp.Load("@\\Report\\JWStock.rpt");
                           orp.SetDataSource(dt.DefaultView);



                           ReportDocument repDoc = orp;


                           //ParameterFieldDefinitions crParameterFieldDefinitions4;
                           //ParameterFieldDefinition crParameterFieldDefinition4;
                           //ParameterValues crParameterValues4 = new ParameterValues();
                           //ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                           //crParameterDiscreteValue4.Value = Repfor;
                           //crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                           //crParameterFieldDefinition4 = crParameterFieldDefinitions4["period"];
                           //crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                           //crParameterValues4.Clear();
                           //crParameterValues4.Add(crParameterDiscreteValue4);
                           //crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);
                           Response.Buffer = false;
                           Response.ClearContent();
                           Response.ClearHeaders(); 

                           string pdfPath = Server.MapPath("~/ConvertPDF/" + "JWStock" + ".pdf");
                           FileInfo file = new FileInfo(pdfPath);
                           if (file.Exists)
                           {
                               file.Delete();
                           }
                           var pd = new PrintDocument();


                           orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                           objDiskOpt.DiskFileName = pdfPath;

                           orp.ExportOptions.DestinationOptions = objDiskOpt;
                           orp.Export();

                       }
                       return null;
                   }
                   catch (Exception ex)
                   {
                       return null;

                   }
               }


               public ActionResult PendingDelivery(string data = "")
               {
                   try
                   {

                       List<Vw_BalanceOrdQty> BillList = new List<Vw_BalanceOrdQty>();
                       CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                       //BillList = dc.Vw_DeliveryScheduleReport.Where(x => x.DeliveryDate.Month == dmonth && x.DeliveryDate.Year == dyear && x.DeliveryDate.Day == dDay).Select(x => x).OfType<Vw_DeliveryScheduleReport>().ToList();
                       BillList = dc.Vw_BalanceOrdQty.Select(x => x).OrderBy(x => x.Pk_Order).Select(x => x).OfType<Vw_BalanceOrdQty>().ToList();
                       if (BillList.Count > 0)
                       {
                           DataTable dt = new DataTable("BalanceDelivery");

 //                           SELECT Vw_BalanceOrdQty.Pk_Order, Vw_BalanceOrdQty.OrderDate, Vw_BalanceOrdQty.CustomerName,
 //                          Vw_BalanceOrdQty.Pk_Invoice, Vw_BalanceOrdQty.InvDate, Vw_BalanceOrdQty.Quantity, 
 //                          Vw_BalanceOrdQty.OrdQty
 //FROM   V2Innovatives.dbo.Vw_BalanceOrdQty Vw_BalanceOrdQty
 //ORDER BY Vw_BalanceOrdQty.Pk_Order, Vw_BalanceOrdQty.Pk_Invoice

                           dt.Columns.Add("Pk_Order");
                           dt.Columns.Add("OrderDate");
                           dt.Columns.Add("InvDate");
                           dt.Columns.Add("CustomerName");
                           dt.Columns.Add("Fk_BoxID");
                           dt.Columns.Add("Fk_PartID");
                           dt.Columns.Add("Ex_Stock");
                           dt.Columns.Add("OrdQty");
                           dt.Columns.Add("PartName");
                           dt.Columns.Add("BoxName");
                           dt.Columns.Add("Pk_Invoice");
                           dt.Columns.Add("DelvQty");

                           foreach (Vw_BalanceOrdQty entity in BillList)
                           {
                               DataRow row = dt.NewRow();
                               row["Pk_Invoice"] = entity.Pk_Invoice;
                               row["Pk_Order"] = entity.Pk_Order;
                               row["InvDate"] = entity.InvDate;
                               row["OrderDate"] = entity.OrderDate;
                               row["CustomerName"] = entity.CustomerName;
                               row["Fk_PartID"] = entity.Fk_PartID;
                               row["Fk_BoxID"] = entity.Fk_BoxID;
                               row["Ex_Stock"] = entity.Ex_Stock;
                               row["OrdQty"] = entity.OrdQty;
                               row["PartName"] = entity.PartName;
                               row["BoxName"] = entity.BoxName;
                               row["DelvQty"] = entity.DelvQty;

                               dt.Rows.Add(row);

                           }

                           DataSet ds = new DataSet();
                           ds.Tables.Add(dt);
                           CogitoStreamline.Report.BalanceDelivery orp = new CogitoStreamline.Report.BalanceDelivery();

                           orp.Load("@\\Report\\BalanceDelivery.rpt");
                           orp.SetDataSource(dt.DefaultView);



                           ReportDocument repDoc = orp;


                           //ParameterFieldDefinitions crParameterFieldDefinitions4;
                           //ParameterFieldDefinition crParameterFieldDefinition4;
                           //ParameterValues crParameterValues4 = new ParameterValues();
                           //ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                           //crParameterDiscreteValue4.Value = Repfor;
                           //crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                           //crParameterFieldDefinition4 = crParameterFieldDefinitions4["period"];
                           //crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                           //crParameterValues4.Clear();
                           //crParameterValues4.Add(crParameterDiscreteValue4);
                           //crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);

                           Response.Buffer = false;
                           Response.ClearContent();
                           Response.ClearHeaders(); 
                           string pdfPath = Server.MapPath("~/ConvertPDF/" + "BalanceDelivery" + ".pdf");
                           FileInfo file = new FileInfo(pdfPath);
                           if (file.Exists)
                           {
                               file.Delete();
                           }
                           var pd = new PrintDocument();


                           orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                           objDiskOpt.DiskFileName = pdfPath;

                           orp.ExportOptions.DestinationOptions = objDiskOpt;
                           orp.Export();

                       }
                       return null;
                   }
                   catch (Exception ex)
                   {
                       return null;

                   }
               }


               public ActionResult PendDelv(string data = "")
               {
                   try
                   {

                       List<Vw_BalanceOrdQty> BillList = new List<Vw_BalanceOrdQty>();
                       CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                       //BillList = dc.Vw_DeliveryScheduleReport.Where(x => x.DeliveryDate.Month == dmonth && x.DeliveryDate.Year == dyear && x.DeliveryDate.Day == dDay).Select(x => x).OfType<Vw_DeliveryScheduleReport>().ToList();
                       BillList = dc.Vw_BalanceOrdQty.Select(x => x).OrderBy(x => x.Pk_Order).Select(x => x).OfType<Vw_BalanceOrdQty>().ToList();
                       if (BillList.Count > 0)
                       {
                           DataTable dt = new DataTable("BalanceDelivery");

                           //                           SELECT Vw_BalanceOrdQty.Pk_Order, Vw_BalanceOrdQty.OrderDate, Vw_BalanceOrdQty.CustomerName,
                           //                          Vw_BalanceOrdQty.Pk_Invoice, Vw_BalanceOrdQty.InvDate, Vw_BalanceOrdQty.Quantity, 
                           //                          Vw_BalanceOrdQty.OrdQty
                           //FROM   V2Innovatives.dbo.Vw_BalanceOrdQty Vw_BalanceOrdQty
                           //ORDER BY Vw_BalanceOrdQty.Pk_Order, Vw_BalanceOrdQty.Pk_Invoice

                           dt.Columns.Add("Pk_Order");                         
                           dt.Columns.Add("Pk_Invoice");
                           dt.Columns.Add("Quantity");
                           dt.Columns.Add("InvDate");                        
                           dt.Columns.Add("PartName");
                           dt.Columns.Add("BoxName");
                         

                           foreach (Vw_BalanceOrdQty entity in BillList)
                           {
                               DataRow row = dt.NewRow();

                               row["Pk_Order"] = entity.Pk_Order;
                               //row["OrderDate"] = entity.OrderDate;
                               //row["CustomerName"] = entity.CustomerName;
                               row["Pk_Invoice"] = entity.Pk_Invoice;
                               row["Quantity"] = entity.Quantity;
                               row["InvDate"] = entity.InvDate;
                               //row["OrdQty"] = entity.OrdQty;
                               row["PartName"] = entity.PartName;
                               row["BoxName"] = entity.BoxName;
                               //row["DelvQty"] = entity.DelvQty;

                               dt.Rows.Add(row);

                           }

                           DataSet ds = new DataSet();
                           ds.Tables.Add(dt);
                           CogitoStreamline.Report.DeliveryDetails orp = new CogitoStreamline.Report.DeliveryDetails();

                           orp.Load("@\\Report\\DeliveryDetails.rpt");
                           orp.SetDataSource(dt.DefaultView);



                           ReportDocument repDoc = orp;


                           //ParameterFieldDefinitions crParameterFieldDefinitions4;
                           //ParameterFieldDefinition crParameterFieldDefinition4;
                           //ParameterValues crParameterValues4 = new ParameterValues();
                           //ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                           //crParameterDiscreteValue4.Value = Repfor;
                           //crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                           //crParameterFieldDefinition4 = crParameterFieldDefinitions4["period"];
                           //crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                           //crParameterValues4.Clear();
                           //crParameterValues4.Add(crParameterDiscreteValue4);
                           //crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);


                           string pdfPath = Server.MapPath("~/ConvertPDF/" + "DeliveryDetails" + ".pdf");
                           FileInfo file = new FileInfo(pdfPath);
                           if (file.Exists)
                           {
                               file.Delete();
                           }
                           var pd = new PrintDocument();


                           orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                           objDiskOpt.DiskFileName = pdfPath;

                           orp.ExportOptions.DestinationOptions = objDiskOpt;
                           orp.Export();

                       }
                       return null;
                   }
                   catch (Exception ex)
                   {
                       return null;

                   }
               }


               public JsonResult SalesDates([System.Web.Http.FromBody]string data)
               {
                   try
                   {

                       data = data.TrimStart('=');
                       int i = 0;

                       Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

                       DateTime SFrom = Convert.ToDateTime(oValues["FDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       DateTime STo = Convert.ToDateTime(oValues["TDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       string[] a = oValues["FDate"].ToString().Split('/');
                       DateTime FromDate = DateTime.ParseExact(a[1] + '/' + a[0] + '/' + a[2] + " 12:00", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);
                       string SFromDVal;
                       var FD = SFrom.Day;
                       var FM = SFrom.ToString("MMM");
                       var FY = SFrom.Year;


                       SFromDVal = FD.ToString() + '/' + FM.ToString() + '/' + FY.ToString();



                       var TD = STo.Day;
                       var TM = STo.ToString("MMM");
                       var TY = STo.Year;

                       string SToDVal;
                       //if (TD > 12)
                       //{ SToDVal = TM.ToString() + '/' + TD.ToString() + '/' + TY.ToString(); }
                       //else
                       SToDVal = TD.ToString() + '/' + TM.ToString() + '/' + TY.ToString();


                       int dDay = Convert.ToInt32(a[0]);
                       int dmonth = Convert.ToInt32(a[1]);
                       int dyear = Convert.ToInt32(a[2]);

                       string monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dmonth);
                       //string Mname=FromDate.ToString( "MMMM");   // this one will also work to get the month name

                       string Repfor = "Sales Details Between - " + " " + SFromDVal + " " + "- " + SToDVal + " ";
                       CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                       //                       SELECT Vw_Sales_Details.Pk_Invoice, Vw_Sales_Details.InvDate, Vw_Sales_Details.CustomerName,
                       //                      Vw_Sales_Details.Quantity, Vw_Sales_Details.Price, Vw_Sales_Details.GrandTotal, Vw_Sales_Details.NETVALUE, 
                       //                      Vw_Sales_Details.TaxType, Vw_Sales_Details.TaxName
                       //FROM   BalajiTesting.dbo.Vw_Sales_Details Vw_Sales_Details


                       List<Vw_Sales_Details> BillList = new List<Vw_Sales_Details>();
                       // CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                       BillList = dc.Vw_Sales_Details.Where(x => x.InvDate >= SFrom && x.InvDate <= STo).OrderBy(x => x.InvDate).Select(x => x).OfType<Vw_Sales_Details>().ToList();
                       if (BillList.Count > 0)
                       {
                           DataTable dt = new DataTable("DeliverySchedule");

                           dt.Columns.Add("Pk_Invoice");
                           dt.Columns.Add("InvDate");
                           dt.Columns.Add("CustomerName");
                           dt.Columns.Add("Quantity");
                           dt.Columns.Add("Price");
                           dt.Columns.Add("GrandTotal");

                           dt.Columns.Add("NETVALUE");
                           dt.Columns.Add("TaxType");
                           //dt.Columns.Add("TaxName");

                           //dt.Columns.Add("TaxValue");
                           dt.Columns.Add("Fk_OrderNo");
                           dt.Columns.Add("PayMode");
                           //dt.Columns.Add("PName");
                           //dt.Columns.Add("BName");
                           dt.Columns.Add("Pk_Inv_Details");


                           foreach (Vw_Sales_Details entity in BillList)
                           {
                               DataRow row = dt.NewRow();

                               row["Pk_Invoice"] = entity.Pk_Invoice;
                               row["InvDate"] = entity.InvDate;
                               row["CustomerName"] = entity.CustomerName;
                               row["Quantity"] = entity.Quantity;
                               row["Price"] = entity.Price;
                               row["GrandTotal"] = entity.GrandTotal;
                               row["NETVALUE"] = entity.NETVALUE;
                               row["TaxType"] = entity.TaxType;
                               //row["TaxName"] = entity.TaxName;

                               //row["TaxValue"] = entity.TaxValue;
                               row["Fk_OrderNo"] = entity.Fk_OrderNo;
                               row["PayMode"] = entity.PayMode;
                               //row["PName"] = entity.PName;
                               //row["BName"] = entity.BName;
                               row["Pk_Inv_Details"] = entity.Pk_Inv_Details;

                               dt.Rows.Add(row);

                           }

                           DataSet ds = new DataSet();
                           ds.Tables.Add(dt);
                           CogitoStreamline.Report.Sales_Dates orp = new CogitoStreamline.Report.Sales_Dates();

                           orp.Load("@\\Report\\Sales_Dates.rpt");
                           orp.SetDataSource(dt.DefaultView);



                           ReportDocument repDoc = orp;


                           ParameterFieldDefinitions crParameterFieldDefinitions4;
                           ParameterFieldDefinition crParameterFieldDefinition4;
                           ParameterValues crParameterValues4 = new ParameterValues();
                           ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                           crParameterDiscreteValue4.Value = Repfor;
                           crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                           crParameterFieldDefinition4 = crParameterFieldDefinitions4["period"];
                           crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                           crParameterValues4.Clear();
                           crParameterValues4.Add(crParameterDiscreteValue4);
                           crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);

                           Response.Buffer = false;
                           Response.ClearContent();
                           Response.ClearHeaders(); 
                           string pdfPath = Server.MapPath("~/ConvertPDF/" + "Sales_Dates" + ".pdf");
                           FileInfo file = new FileInfo(pdfPath);
                           if (file.Exists)
                           {
                               file.Delete();
                           }
                           var pd = new PrintDocument();


                           //orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           //orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           ////  CreateBookmarksFromGroupTree = true;
                           ////   pdffprnat.CreateBookmarksFromGroupTree = true;
                           //DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                           //objDiskOpt.DiskFileName = pdfPath;

                           //orp.ExportOptions.DestinationOptions = objDiskOpt;
                           //orp.Export();


                           ExportOptions rptOptions = new ExportOptions();
                           PdfFormatOptions pdffprnat = new PdfFormatOptions();
                           pdffprnat.CreateBookmarksFromGroupTree = true;
                           rptOptions.ExportFormatOptions = pdffprnat;
                           rptOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           rptOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           DiskFileDestinationOptions dest = new DiskFileDestinationOptions();
                           dest.DiskFileName = pdfPath;
                           rptOptions.ExportDestinationOptions = dest;
                           orp.Export(rptOptions);

                       }
                       else
                       {

                       }
                       return null;
                   }
                   catch (Exception ex)
                   {
                       return null;
                       //  Response.Redirect("Index");D:\shantha\Billing\CogitoStreamline\CogitoStreamline\Controllers\CartonBillingController.cs
                   }
               }




               public JsonResult SalesDetailsDates([System.Web.Http.FromBody]string data)
               {
                   try
                   {

                       data = data.TrimStart('=');
                       int i = 0;

                       Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

                       DateTime SFrom = Convert.ToDateTime(oValues["FDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       DateTime STo = Convert.ToDateTime(oValues["TDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                    
                       string[] a = oValues["FDate"].ToString().Split('/');
                       DateTime FromDate = DateTime.ParseExact(a[1] + '/' + a[0] + '/' + a[2] + " 12:00", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);
                       string SFromDVal;

                     


                       var FD = SFrom.Day;
                       var FM = SFrom.ToString("MMM");
                       var FY = SFrom.Year;
                       var FM1 = SFrom.Month;


                       SFromDVal = FD.ToString() + '/' + FM.ToString() + '/' + FY.ToString();
                   


                       var TD = STo.Day;
                       var TM = STo.ToString("MMM");
                       var TY = STo.Year;
                       var TM1 = STo.Month;

                       string SToDVal;
                       //if (TD > 12)
                       //{ SToDVal = TM.ToString() + '/' + TD.ToString() + '/' + TY.ToString(); }
                       //else
                       SToDVal = TD.ToString() + '/' + TM.ToString() + '/' + TY.ToString();
                 

                       int dDay = Convert.ToInt32(a[0]);
                       int dmonth = Convert.ToInt32(a[1]);
                       int dyear = Convert.ToInt32(a[2]);

                       string monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dmonth);
                       //string Mname=FromDate.ToString( "MMMM");   // this one will also work to get the month name

                       string Repfor = "Sales Details Between - " + " " + SFromDVal + " " + "- " + SToDVal + " ";
                       CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                       //                       SELECT Vw_Sales_Details.Pk_Invoice, Vw_Sales_Details.InvDate, Vw_Sales_Details.CustomerName,
                       //                      Vw_Sales_Details.Quantity, Vw_Sales_Details.Price, Vw_Sales_Details.GrandTotal, Vw_Sales_Details.NETVALUE, 
                       //                      Vw_Sales_Details.TaxType, Vw_Sales_Details.TaxName
                       //FROM   BalajiTesting.dbo.Vw_Sales_Details Vw_Sales_Details


                       List<VwInvoice> BillList = new List<VwInvoice>();
                       // CogitoStreamLineEntities dc = new CogitoStreamLineEntities();
                       STo = STo.AddDays(1);
                       BillList = dc.VwInvoice.Where(x => x.InvDate >= SFrom && x.InvDate < STo).OrderBy(x => x.InvDate).Select(x => x).OfType<VwInvoice>().ToList();

                       if (BillList.Count > 0)
                       {
                           //var CtrVal = BillList.Count;
                           DataTable dt = new DataTable("InvoiceBill");

                           dt.Columns.Add("CustomerName");
                           dt.Columns.Add("CustomerAddress");
                           dt.Columns.Add("CityName");
                           dt.Columns.Add("StateName");
                           dt.Columns.Add("PinCode");
                           dt.Columns.Add("Pk_Invoice");
                           dt.Columns.Add("InvDate");
                           dt.Columns.Add("Fk_OrderNo");
                           dt.Columns.Add("TaxValue");
                           dt.Columns.Add("Quantity");
                           dt.Columns.Add("Price");
                           dt.Columns.Add("Fk_JobCardID");
                           dt.Columns.Add("NetAmount");
                           dt.Columns.Add("Name");
                           dt.Columns.Add("Description");
                           dt.Columns.Add("Pk_Inv_Details");
                           dt.Columns.Add("Esugam");
                           dt.Columns.Add("COA");
                           dt.Columns.Add("CreditLimit");
                           dt.Columns.Add("HsnCode");
                           dt.Columns.Add("LandLine");
                           dt.Columns.Add("VAT");
                           //dt.Columns.Add("");
                           dt.Columns.Add("TaxType");

                           dt.Columns.Add("DeliveryName");
                           dt.Columns.Add("DeliveryAdd1");
                           dt.Columns.Add("DeliveryAdd2");

                           dt.Columns.Add("PName");
                           dt.Columns.Add("Length");
                           dt.Columns.Add("Width");
                           dt.Columns.Add("Height");


                           dt.Columns.Add("Buyers_OrderNo");
                           dt.Columns.Add("BOrderDated");
                           dt.Columns.Add("DNTimeofInv");
                           dt.Columns.Add("MVehicleNo");
                           dt.Columns.Add("DNTimeofRemoval");
                           dt.Columns.Add("Pk_PartPropertyID");
                           dt.Columns.Add("UnitVal");

                           dt.Columns.Add("Despatched");
                           dt.Columns.Add("Destination");
                           //dt.Columns.Add("Pk_Inv_Details");

                           foreach (VwInvoice entity in BillList)
                           {
                               DataRow row = dt.NewRow();

                               row["CustomerName"] = entity.CustomerName;
                               row["CustomerAddress"] = entity.CustomerAddress;
                               row["CityName"] = entity.CityName;
                               row["StateName"] = entity.StateName;
                               row["PinCode"] = entity.PinCode;
                               row["Pk_Invoice"] = entity.Pk_Invoice;
                               row["InvDate"] = entity.InvDate;
                               row["Fk_OrderNo"] = entity.Fk_OrderNo;
                               row["TaxValue"] = entity.TaxValue;
                               row["Quantity"] = entity.Quantity;
                               row["Price"] = entity.Price;
                               row["VAT"] = entity.VAT;
                               row["NetAmount"] = entity.NetAmount;
                         //      NetValue = Convert.ToDecimal(NetValue + entity.NetAmount);
                               row["Name"] = entity.Name;
                               row["HsnCode"] = entity.HsnCode;
                               row["Pk_Inv_Details"] = entity.Pk_Inv_Details;
                               row["Esugam"] = entity.ESUGAM;
                               row["COA"] = entity.COA;
                               row["CreditLimit"] = entity.CreditLimit;
                               row["HsnCode"] = entity.HsnCode;
                               row["Description"] = entity.Description;

                               row["DeliveryName"] = entity.DeliveryName;
                               row["DeliveryAdd1"] = entity.DeliveryAdd1;
                               row["DeliveryAdd2"] = entity.DeliveryAdd2;


                               row["PName"] = entity.PName;
                               row["Length"] = entity.Length;
                               row["Width"] = entity.Width;
                               row["Height"] = entity.Height;


                               row["Buyers_OrderNo"] = entity.Buyers_OrderNo;
                               row["BOrderDated"] = entity.BOrderDated;
                               row["DNTimeofInv"] = entity.DNTimeofInv;
                               row["MVehicleNo"] = entity.MVehicleNo;
                               row["DNTimeofRemoval"] = entity.DNTimeofRemoval;
                               row["Pk_PartPropertyID"] = entity.Pk_PartPropertyID;

                               //row["Pk_Inv_Details"] = entity.Pk_Inv_Details;
                               row["LandLine"] = entity.LandLine;

                               dt.Rows.Add(row);

                           }

                           DataSet ds = new DataSet();
                           ds.Tables.Add(dt);
                           CogitoStreamline.Report.SalesDetails orp = new CogitoStreamline.Report.SalesDetails();

                           orp.Load("@\\Report\\SalesDetails.rpt");
                           orp.SetDataSource(dt.DefaultView);



                           ReportDocument repDoc = orp;


                           ParameterFieldDefinitions crParameterFieldDefinitions4;
                           ParameterFieldDefinition crParameterFieldDefinition4;
                           ParameterValues crParameterValues4 = new ParameterValues();
                           ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                           crParameterDiscreteValue4.Value = Repfor;
                           crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                           crParameterFieldDefinition4 = crParameterFieldDefinitions4["period"];
                           crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                           crParameterValues4.Clear();
                           crParameterValues4.Add(crParameterDiscreteValue4);
                           crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);

                           Response.Buffer = false;
                           Response.ClearContent();
                           Response.ClearHeaders();

                           string pdfPath = Server.MapPath("~/ConvertPDF/" + "SalesDetails" + ".pdf");
                           FileInfo file = new FileInfo(pdfPath);
                           if (file.Exists)
                           {
                               file.Delete();
                           }
                           var pd = new PrintDocument();


                           //orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           //orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           ////  CreateBookmarksFromGroupTree = true;
                           ////   pdffprnat.CreateBookmarksFromGroupTree = true;
                           //DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                           //objDiskOpt.DiskFileName = pdfPath;

                           //orp.ExportOptions.DestinationOptions = objDiskOpt;
                           //orp.Export();


                           ExportOptions rptOptions = new ExportOptions();
                           PdfFormatOptions pdffprnat = new PdfFormatOptions();
                           pdffprnat.CreateBookmarksFromGroupTree = true;
                           rptOptions.ExportFormatOptions = pdffprnat;
                           rptOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           rptOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           DiskFileDestinationOptions dest = new DiskFileDestinationOptions();
                           dest.DiskFileName = pdfPath;
                           rptOptions.ExportDestinationOptions = dest;
                           orp.Export(rptOptions);

                       }
                       else
                       {

                       }
                       return null;
                   }
                   catch (Exception ex)
                   {
                       return null;
                       //  Response.Redirect("Index");D:\shantha\Billing\CogitoStreamline\CogitoStreamline\Controllers\CartonBillingController.cs
                   }
               }

               public JsonResult SalesDetailsDates_RM([System.Web.Http.FromBody]string data)
               {
                   try
                   {

                       data = data.TrimStart('=');
                       int i = 0;

                       Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

                       DateTime SFrom = Convert.ToDateTime(oValues["FDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       DateTime STo = Convert.ToDateTime(oValues["TDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       string[] a = oValues["FDate"].ToString().Split('/');
                       DateTime FromDate = DateTime.ParseExact(a[1] + '/' + a[0] + '/' + a[2] + " 12:00", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);
                       string SFromDVal;


                       var FD = SFrom.Day;
                       var FM = SFrom.ToString("MMM");
                       var FY = SFrom.Year;
                       var FM1 = SFrom.Month;


                       SFromDVal = FD.ToString() + '/' + FM.ToString() + '/' + FY.ToString();



                       var TD = STo.Day;
                       var TM = STo.ToString("MMM");
                       var TY = STo.Year;
                       var TM1 = STo.Month;

                       string SToDVal;
                       //if (TD > 12)
                       //{ SToDVal = TM.ToString() + '/' + TD.ToString() + '/' + TY.ToString(); }
                       //else
                       SToDVal = TD.ToString() + '/' + TM.ToString() + '/' + TY.ToString();


                       int dDay = Convert.ToInt32(a[0]);
                       int dmonth = Convert.ToInt32(a[1]);
                       int dyear = Convert.ToInt32(a[2]);

                       string monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dmonth);
                       //string Mname=FromDate.ToString( "MMMM");   // this one will also work to get the month name

                       string Repfor = "Sales Details Between - " + " " + SFromDVal + " " + "- " + SToDVal + " ";
                       CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                       //                       SELECT Vw_Sales_Details.Pk_Invoice, Vw_Sales_Details.InvDate, Vw_Sales_Details.CustomerName,
                       //                      Vw_Sales_Details.Quantity, Vw_Sales_Details.Price, Vw_Sales_Details.GrandTotal, Vw_Sales_Details.NETVALUE, 
                       //                      Vw_Sales_Details.TaxType, Vw_Sales_Details.TaxName
                       //FROM   BalajiTesting.dbo.Vw_Sales_Details Vw_Sales_Details


                       List<Vw_InvRM> BillList = new List<Vw_InvRM>();
                       // CogitoStreamLineEntities dc = new CogitoStreamLineEntities();
                       STo = STo.AddDays(1);
                       BillList = dc.Vw_InvRM.Where(x => x.InvDate >= SFrom && x.InvDate < STo).OrderBy(x => x.InvDate).Select(x => x).OfType<Vw_InvRM>().ToList();

                       if (BillList.Count > 0)
                       {
                           //var CtrVal = BillList.Count;
                           DataTable dt1 = new DataTable("InvoiceBill");

                           dt1.Columns.Add("CustomerName");
                           dt1.Columns.Add("CustomerAddress");
                           dt1.Columns.Add("CityName");
                           dt1.Columns.Add("StateName");
                           dt1.Columns.Add("PinCode");
                           dt1.Columns.Add("Pk_Invoice");
                           dt1.Columns.Add("InvDate");
                           dt1.Columns.Add("Fk_OrderNo");
                           dt1.Columns.Add("TaxValue");
                           dt1.Columns.Add("Quantity");
                           dt1.Columns.Add("Price");
                           dt1.Columns.Add("Fk_JobCardID");
                           dt1.Columns.Add("NetAmount");
                           //dt1.Columns.Add("Name");
                           dt1.Columns.Add("Description");
                           dt1.Columns.Add("Pk_Inv_Details");
                           dt1.Columns.Add("Esugam");
                           dt1.Columns.Add("COA");
                           dt1.Columns.Add("CreditLimit");
                           dt1.Columns.Add("HsnCode");
                           dt1.Columns.Add("LandLine");
                           dt1.Columns.Add("VAT");
                           //dt1.Columns.Add("");
                           dt1.Columns.Add("TaxType");

                           dt1.Columns.Add("DeliveryName");
                           dt1.Columns.Add("DeliveryAdd1");
                           dt1.Columns.Add("DeliveryAdd2");

                           dt1.Columns.Add("PName");
                           dt1.Columns.Add("Length");
                           dt1.Columns.Add("Width");
                           dt1.Columns.Add("Height");


                           dt1.Columns.Add("Buyers_OrderNo");
                           dt1.Columns.Add("BOrderDated");
                           dt1.Columns.Add("DNTimeofInv");
                           dt1.Columns.Add("MVehicleNo");
                           dt1.Columns.Add("DNTimeofRemoval");
                           dt1.Columns.Add("Name");
                           dt1.Columns.Add("UnitVal");
                           dt1.Columns.Add("Despatched");
                           dt1.Columns.Add("Destination");



                           foreach (Vw_InvRM entity in BillList)
                           {
                               DataRow row = dt1.NewRow();

                               row["CustomerName"] = entity.CustomerName;
                               row["CustomerAddress"] = entity.CustomerAddress;
                               row["CityName"] = entity.CityName;
                               row["StateName"] = entity.StateName;
                               row["PinCode"] = entity.PinCode;
                               row["Pk_Invoice"] = entity.Pk_Invoice;
                               row["InvDate"] = entity.InvDate;
                               row["Fk_OrderNo"] = entity.Fk_OrderNo;
                               row["TaxValue"] = entity.TaxValue;
                               row["Quantity"] = entity.Quantity;
                               row["Price"] = entity.Price;
                               //row["Fk_JobCardID"] = entity.Fk_JobCardID;
                               row["NetAmount"] = entity.NetAmount;
                               //NetValue = Convert.ToDecimal(NetValue + entity.NetAmount);
                               //row["Name"] = entity.Name;
                               row["HsnCode"] = entity.HsnCode;
                               row["Pk_Inv_Details"] = entity.Pk_Inv_Details;
                               row["Esugam"] = entity.ESUGAM;
                               row["COA"] = entity.COA;
                               row["CreditLimit"] = entity.CreditLimit;
                               row["HsnCode"] = entity.HsnCode;
                               row["Description"] = entity.Description;

                               row["DeliveryName"] = entity.DeliveryName;
                               row["DeliveryAdd1"] = entity.DeliveryAdd1;
                               row["DeliveryAdd2"] = entity.DeliveryAdd2;


                               //row["PName"] = entity.PName;
                               //row["Length"] = entity.Length;
                               //row["Width"] = entity.Width;
                               //row["Height"] = entity.Height;


                               row["Buyers_OrderNo"] = entity.Buyers_OrderNo;
                               row["BOrderDated"] = entity.BOrderDated;
                               row["DNTimeofInv"] = entity.DNTimeofInv;
                               row["MVehicleNo"] = entity.MVehicleNo;
                               row["DNTimeofRemoval"] = entity.DNTimeofRemoval;
                               row["Name"] = entity.Name;

                               row["UnitVal"] = entity.UnitVal;
                               row["LandLine"] = entity.LandLine;


                               row["Despatched"] = entity.Despatched;
                               row["Destination"] = entity.Destination;
                               row["VAT"] = entity.VAT;
                               row["TaxType"] = entity.TaxType;

                               dt1.Rows.Add(row);

                           }

                           DataSet ds = new DataSet();
                           ds.Tables.Add(dt1);
                           CogitoStreamline.Report.SalesDetails_RM orp = new CogitoStreamline.Report.SalesDetails_RM();

                           orp.Load("@\\Report\\SalesDetails_RM.rpt");
                           orp.SetDataSource(dt1.DefaultView);



                           ReportDocument repDoc = orp;


                           ParameterFieldDefinitions crParameterFieldDefinitions4;
                           ParameterFieldDefinition crParameterFieldDefinition4;
                           ParameterValues crParameterValues4 = new ParameterValues();
                           ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                           crParameterDiscreteValue4.Value = Repfor;
                           crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                           crParameterFieldDefinition4 = crParameterFieldDefinitions4["period"];
                           crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                           crParameterValues4.Clear();
                           crParameterValues4.Add(crParameterDiscreteValue4);
                           crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);

                           Response.Buffer = false;
                           Response.ClearContent();
                           Response.ClearHeaders();

                           string pdfPath = Server.MapPath("~/ConvertPDF/" + "SalesDetails_RM" + ".pdf");
                           FileInfo file = new FileInfo(pdfPath);
                           if (file.Exists)
                           {
                               file.Delete();
                           }
                           var pd = new PrintDocument();


                           //orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           //orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           ////  CreateBookmarksFromGroupTree = true;
                           ////   pdffprnat.CreateBookmarksFromGroupTree = true;
                           //DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                           //objDiskOpt.DiskFileName = pdfPath;

                           //orp.ExportOptions.DestinationOptions = objDiskOpt;
                           //orp.Export();


                           ExportOptions rptOptions = new ExportOptions();
                           PdfFormatOptions pdffprnat = new PdfFormatOptions();
                           pdffprnat.CreateBookmarksFromGroupTree = true;
                           rptOptions.ExportFormatOptions = pdffprnat;
                           rptOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           rptOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           DiskFileDestinationOptions dest = new DiskFileDestinationOptions();
                           dest.DiskFileName = pdfPath;
                           rptOptions.ExportDestinationOptions = dest;
                           orp.Export(rptOptions);

                       }
                       else
                       {

                       }
                       return null;
                   }
                   catch (Exception ex)
                   {
                       return null;
                       //  Response.Redirect("Index");D:\shantha\Billing\CogitoStreamline\CogitoStreamline\Controllers\CartonBillingController.cs
                   }
               }



               public JsonResult SalesDetailsDatesAmount([System.Web.Http.FromBody]string data)
               {
                   try
                   {

                       data = data.TrimStart('=');
                       int i = 0;

                       Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

                       DateTime SFrom = Convert.ToDateTime(oValues["FDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       DateTime STo = Convert.ToDateTime(oValues["TDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       string[] a = oValues["FDate"].ToString().Split('/');
                       DateTime FromDate = DateTime.ParseExact(a[1] + '/' + a[0] + '/' + a[2] + " 12:00", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);
                       string SFromDVal;
                       var FD = SFrom.Day;
                       var FM = SFrom.ToString("MMM");
                       var FY = SFrom.Year;


                       SFromDVal = FD.ToString() + '/' + FM.ToString() + '/' + FY.ToString();



                       var TD = STo.Day;
                       var TM = STo.ToString("MMM");
                       var TY = STo.Year;

                       string SToDVal;
                       //if (TD > 12)
                       //{ SToDVal = TM.ToString() + '/' + TD.ToString() + '/' + TY.ToString(); }
                       //else
                       SToDVal = TD.ToString() + '/' + TM.ToString() + '/' + TY.ToString();


                       int dDay = Convert.ToInt32(a[0]);
                       int dmonth = Convert.ToInt32(a[1]);
                       int dyear = Convert.ToInt32(a[2]);

                       string monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dmonth);
                       //string Mname=FromDate.ToString( "MMMM");   // this one will also work to get the month name

                       string Repfor = "Sales Details Between - " + " " + SFromDVal + " " + "- " + SToDVal + " ";
                       CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                       //                       SELECT Vw_Sales_Details.Pk_Invoice, Vw_Sales_Details.InvDate, Vw_Sales_Details.CustomerName,
                       //                      Vw_Sales_Details.Quantity, Vw_Sales_Details.Price, Vw_Sales_Details.GrandTotal, Vw_Sales_Details.NETVALUE, 
                       //                      Vw_Sales_Details.TaxType, Vw_Sales_Details.TaxName
                       //FROM   BalajiTesting.dbo.Vw_Sales_Details Vw_Sales_Details


                       List<VwInvoice> BillList = new List<VwInvoice>();
                       // CogitoStreamLineEntities dc = new CogitoStreamLineEntities();
                       STo = STo.AddDays(1);
                       BillList = dc.VwInvoice.Where(x => x.InvDate >= SFrom && x.InvDate < STo).OrderBy(x => x.InvDate).Select(x => x).OfType<VwInvoice>().ToList();

                       if (BillList.Count > 0)
                       {
                           //var CtrVal = BillList.Count;
                           DataTable dt = new DataTable("InvoiceBill");

                           dt.Columns.Add("CustomerName");
                           dt.Columns.Add("CustomerAddress");
                           dt.Columns.Add("CityName");
                           dt.Columns.Add("StateName");
                           dt.Columns.Add("PinCode");
                           dt.Columns.Add("Pk_Invoice");
                           dt.Columns.Add("InvDate");
                           dt.Columns.Add("Fk_OrderNo");
                           dt.Columns.Add("TaxValue");
                           dt.Columns.Add("Quantity");
                           dt.Columns.Add("Price");
                           dt.Columns.Add("Fk_JobCardID");
                           dt.Columns.Add("NetAmount");
                           dt.Columns.Add("Name");
                           dt.Columns.Add("Description");
                           dt.Columns.Add("Pk_Inv_Details");
                           dt.Columns.Add("Esugam");
                           dt.Columns.Add("COA");
                           dt.Columns.Add("CreditLimit");
                           dt.Columns.Add("HsnCode");
                           dt.Columns.Add("LandLine");
                           dt.Columns.Add("VAT");
                           //dt.Columns.Add("");
                           dt.Columns.Add("TaxType");

                           dt.Columns.Add("GrandTotal");
                           dt.Columns.Add("NETVALUE");
                           dt.Columns.Add("DeliveryAdd2");

                           dt.Columns.Add("PName");
                           dt.Columns.Add("Length");
                           dt.Columns.Add("Width");
                           dt.Columns.Add("Height");


                           dt.Columns.Add("Buyers_OrderNo");
                           dt.Columns.Add("BOrderDated");
                           dt.Columns.Add("DNTimeofInv");
                           dt.Columns.Add("MVehicleNo");
                           dt.Columns.Add("DNTimeofRemoval");
                           dt.Columns.Add("Pk_PartPropertyID");
                           dt.Columns.Add("UnitVal");

                           dt.Columns.Add("Despatched");
                           dt.Columns.Add("Destination");
                           //dt.Columns.Add("Pk_Inv_Details");

                           foreach (VwInvoice entity in BillList)
                           {
                               DataRow row = dt.NewRow();

                               row["CustomerName"] = entity.CustomerName;
                               row["CustomerAddress"] = entity.CustomerAddress;
                               row["CityName"] = entity.CityName;
                               row["StateName"] = entity.StateName;
                               row["PinCode"] = entity.PinCode;
                               row["Pk_Invoice"] = entity.Pk_Invoice;
                               row["InvDate"] = entity.InvDate;
                               row["Fk_OrderNo"] = entity.Fk_OrderNo;
                               row["TaxValue"] = entity.TaxValue;
                               row["TaxType"] = entity.TaxType;
                               row["Quantity"] = entity.Quantity;
                               row["Price"] = entity.Price;
                               row["VAT"] = entity.VAT;
                               row["NetAmount"] = entity.NetAmount;
                               //      NetValue = Convert.ToDecimal(NetValue + entity.NetAmount);
                               row["NETVALUE"] = entity.NETVALUE;
                               row["GrandTotal"] = entity.GrandTotal;
                               row["Pk_Inv_Details"] = entity.Pk_Inv_Details;
                               row["Esugam"] = entity.ESUGAM;
                               row["COA"] = entity.COA;
                               row["CreditLimit"] = entity.CreditLimit;
                               row["HsnCode"] = entity.HsnCode;
                               row["Description"] = entity.Description;

                               //row["DeliveryName"] = entity.DeliveryName;
                               //row["DeliveryAdd1"] = entity.DeliveryAdd1;
                               //row["DeliveryAdd2"] = entity.DeliveryAdd2;


                               row["PName"] = entity.PName;
                               //row["Length"] = entity.Length;
                               //row["Width"] = entity.Width;
                               //row["Height"] = entity.Height;


                               row["Buyers_OrderNo"] = entity.Buyers_OrderNo;
                               row["BOrderDated"] = entity.BOrderDated;
                               //row["DNTimeofInv"] = entity.DNTimeofInv;
                               //row["MVehicleNo"] = entity.MVehicleNo;
                               //row["DNTimeofRemoval"] = entity.DNTimeofRemoval;
                               //row["Pk_PartPropertyID"] = entity.Pk_PartPropertyID;

                               row["Pk_Inv_Details"] = entity.Pk_Inv_Details;
                               //row["LandLine"] = entity.LandLine;

                               dt.Rows.Add(row);

                           }

                           DataSet ds = new DataSet();
                           ds.Tables.Add(dt);
                           CogitoStreamline.Report.SalesDetails_Amount orp = new CogitoStreamline.Report.SalesDetails_Amount();

                           orp.Load("@\\Report\\SalesDetails_Amount.rpt");
                           orp.SetDataSource(dt.DefaultView);



                           ReportDocument repDoc = orp;


                           ParameterFieldDefinitions crParameterFieldDefinitions4;
                           ParameterFieldDefinition crParameterFieldDefinition4;
                           ParameterValues crParameterValues4 = new ParameterValues();
                           ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                           crParameterDiscreteValue4.Value = Repfor;
                           crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                           crParameterFieldDefinition4 = crParameterFieldDefinitions4["period"];
                           crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                           crParameterValues4.Clear();
                           crParameterValues4.Add(crParameterDiscreteValue4);
                           crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);

                           Response.Buffer = false;
                           Response.ClearContent();
                           Response.ClearHeaders();

                           string pdfPath = Server.MapPath("~/ConvertPDF/" + "SalesDetails_Amt" + ".pdf");
                           FileInfo file = new FileInfo(pdfPath);
                           if (file.Exists)
                           {
                               file.Delete();
                           }
                           var pd = new PrintDocument();


                           //orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           //orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           ////  CreateBookmarksFromGroupTree = true;
                           ////   pdffprnat.CreateBookmarksFromGroupTree = true;
                           //DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                           //objDiskOpt.DiskFileName = pdfPath;

                           //orp.ExportOptions.DestinationOptions = objDiskOpt;
                           //orp.Export();


                           ExportOptions rptOptions = new ExportOptions();
                           PdfFormatOptions pdffprnat = new PdfFormatOptions();
                           pdffprnat.CreateBookmarksFromGroupTree = true;
                           rptOptions.ExportFormatOptions = pdffprnat;
                           rptOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           rptOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           DiskFileDestinationOptions dest = new DiskFileDestinationOptions();
                           dest.DiskFileName = pdfPath;
                           rptOptions.ExportDestinationOptions = dest;
                           orp.Export(rptOptions);

                       }
                       else
                       {

                       }
                       return null;
                   }
                   catch (Exception ex)
                   {
                       return null;
                       //  Response.Redirect("Index");D:\shantha\Billing\CogitoStreamline\CogitoStreamline\Controllers\CartonBillingController.cs
                   }
               }
 
        
        public JsonResult SalesDatesCustWise([System.Web.Http.FromBody]string data)
               {
                   try
                   {

                       data = data.TrimStart('=');
                       int i = 0;

                       Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

                       DateTime SFrom = Convert.ToDateTime(oValues["FDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       DateTime STo = Convert.ToDateTime(oValues["TDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       string[] a = oValues["FDate"].ToString().Split('/');
                       DateTime FromDate = DateTime.ParseExact(a[1] + '/' + a[0] + '/' + a[2] + " 12:00", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);
                       string SFromDVal;
                       var FD = SFrom.Day;
                       var FM = SFrom.ToString("MMM");
                       var FY = SFrom.Year;


                       SFromDVal = FD.ToString() + '/' + FM.ToString() + '/' + FY.ToString();



                       var TD = STo.Day;
                       var TM = STo.ToString("MMM");
                       var TY = STo.Year;

                       string SToDVal;
                       //if (TD > 12)
                       //{ SToDVal = TM.ToString() + '/' + TD.ToString() + '/' + TY.ToString(); }
                       //else
                       SToDVal = TD.ToString() + '/' + TM.ToString() + '/' + TY.ToString();


                       int dDay = Convert.ToInt32(a[0]);
                       int dmonth = Convert.ToInt32(a[1]);
                       int dyear = Convert.ToInt32(a[2]);

                       string monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dmonth);
                       //string Mname=FromDate.ToString( "MMMM");   // this one will also work to get the month name

                       string Repfor = "CustomerWise Sales Details Between - " + " " + SFromDVal + " " + "- " + SToDVal + " ";
                       CogitoStreamLineEntities dc = new CogitoStreamLineEntities();


                       List<Vw_Sales_Details> BillList = new List<Vw_Sales_Details>();
                       // CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                       BillList = dc.Vw_Sales_Details.Where(x => x.InvDate >= SFrom && x.InvDate <= STo).OrderBy(x => x.InvDate).Select(x => x).OfType<Vw_Sales_Details>().ToList();
                       if (BillList.Count > 0)
                       {
                           DataTable dt = new DataTable("DeliverySchedule");

                           dt.Columns.Add("Pk_Invoice");
                           dt.Columns.Add("InvDate");
                           dt.Columns.Add("CustomerName");
                           dt.Columns.Add("Quantity");
                           dt.Columns.Add("Price");
                           dt.Columns.Add("GrandTotal");

                           dt.Columns.Add("NETVALUE");
                           dt.Columns.Add("TaxType");
                           dt.Columns.Add("TaxName");

                           dt.Columns.Add("TaxValue");
                           dt.Columns.Add("Fk_OrderNo");
                           dt.Columns.Add("PayMode");
                           dt.Columns.Add("PName");
                           dt.Columns.Add("BName");
                           dt.Columns.Add("Pk_Inv_Details");


                           foreach (Vw_Sales_Details entity in BillList)
                           {
                               DataRow row = dt.NewRow();

                               row["Pk_Invoice"] = entity.Pk_Invoice;
                               row["InvDate"] = entity.InvDate;
                               row["CustomerName"] = entity.CustomerName;
                               row["Quantity"] = entity.Quantity;
                               row["Price"] = entity.Price;
                               row["GrandTotal"] = entity.GrandTotal;
                               row["NETVALUE"] = entity.NETVALUE;
                               row["TaxType"] = entity.TaxType;
                               //row["TaxName"] = entity.TaxName;

                               //row["TaxValue"] = entity.TaxValue;
                               row["Fk_OrderNo"] = entity.Fk_OrderNo;
                               row["PayMode"] = entity.PayMode;
                               //row["PName"] = entity.PName;
                               //row["BName"] = entity.BName;
                               row["Pk_Inv_Details"] = entity.Pk_Inv_Details;

                               dt.Rows.Add(row);

                           }

                           DataSet ds = new DataSet();
                           ds.Tables.Add(dt);
                           CogitoStreamline.Report.Sales_DatesCustWise orp = new CogitoStreamline.Report.Sales_DatesCustWise();

                           orp.Load("@\\Report\\Sales_DatesCustWise.rpt");
                           orp.SetDataSource(dt.DefaultView);



                           ReportDocument repDoc = orp;


                           ParameterFieldDefinitions crParameterFieldDefinitions4;
                           ParameterFieldDefinition crParameterFieldDefinition4;
                           ParameterValues crParameterValues4 = new ParameterValues();
                           ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                           crParameterDiscreteValue4.Value = Repfor;
                           crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                           crParameterFieldDefinition4 = crParameterFieldDefinitions4["period"];
                           crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                           crParameterValues4.Clear();
                           crParameterValues4.Add(crParameterDiscreteValue4);
                           crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);

                           Response.Buffer = false;
                           Response.ClearContent();
                           Response.ClearHeaders(); 

                           string pdfPath = Server.MapPath("~/ConvertPDF/" + "Sales_DatesCustWise" + ".pdf");
                           FileInfo file = new FileInfo(pdfPath);
                           if (file.Exists)
                           {
                               file.Delete();
                           }
                           var pd = new PrintDocument();


                           //orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           //orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           ////  CreateBookmarksFromGroupTree = true;
                           ////   pdffprnat.CreateBookmarksFromGroupTree = true;
                           //DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                           //objDiskOpt.DiskFileName = pdfPath;

                           //orp.ExportOptions.DestinationOptions = objDiskOpt;
                           //orp.Export();


                           ExportOptions rptOptions = new ExportOptions();
                           PdfFormatOptions pdffprnat = new PdfFormatOptions();
                           pdffprnat.CreateBookmarksFromGroupTree = true;
                           rptOptions.ExportFormatOptions = pdffprnat;
                           rptOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           rptOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           DiskFileDestinationOptions dest = new DiskFileDestinationOptions();
                           dest.DiskFileName = pdfPath;
                           rptOptions.ExportDestinationOptions = dest;
                           orp.Export(rptOptions);

                       }
                       else
                       {

                       }
                       return null;
                   }
                   catch (Exception ex)
                   {
                       return null;
                       //  Response.Redirect("Index");D:\shantha\Billing\CogitoStreamline\CogitoStreamline\Controllers\CartonBillingController.cs
                   }
               }

               public JsonResult DateWise_SalesSummary([System.Web.Http.FromBody]string data)
               {
                   try
                   {

                       data = data.TrimStart('=');
                       int i = 0;

                       Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

                       DateTime SFrom = Convert.ToDateTime(oValues["FDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       DateTime STo = Convert.ToDateTime(oValues["TDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       string[] a = oValues["FDate"].ToString().Split('/');
                       DateTime FromDate = DateTime.ParseExact(a[1] + '/' + a[0] + '/' + a[2] + " 12:00", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);
                       string SFromDVal;
                       var FD = SFrom.Day;
                       var FM = SFrom.ToString("MMM");
                       var FY = SFrom.Year;


                       SFromDVal = FD.ToString() + '/' + FM.ToString() + '/' + FY.ToString();



                       var TD = STo.Day;
                       var TM = STo.ToString("MMM");
                       var TY = STo.Year;

                       string SToDVal;
                       //if (TD > 12)
                       //{ SToDVal = TM.ToString() + '/' + TD.ToString() + '/' + TY.ToString(); }
                       //else
                       SToDVal = TD.ToString() + '/' + TM.ToString() + '/' + TY.ToString();


                       int dDay = Convert.ToInt32(a[0]);
                       int dmonth = Convert.ToInt32(a[1]);
                       int dyear = Convert.ToInt32(a[2]);

                       string monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dmonth);
                       //string Mname=FromDate.ToString( "MMMM");   // this one will also work to get the month name

                       string Repfor = "Sales Summary Details Between - " + " " + SFromDVal + " " + "- " + SToDVal + " ";
                       CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                       //                       SELECT Vw_Sales_Details.Pk_Invoice, Vw_Sales_Details.InvDate, Vw_Sales_Details.CustomerName,
                       //                      Vw_Sales_Details.Quantity, Vw_Sales_Details.Price, Vw_Sales_Details.GrandTotal, Vw_Sales_Details.NETVALUE, 
                       //                      Vw_Sales_Details.TaxType, Vw_Sales_Details.TaxName
                       //FROM   BalajiTesting.dbo.Vw_Sales_Details Vw_Sales_Details


                       List<Vw_Sales_Details> BillList = new List<Vw_Sales_Details>();
                       // CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                       BillList = dc.Vw_Sales_Details.Where(x => x.InvDate >= SFrom && x.InvDate <= STo).OrderBy(x => x.InvDate).Select(x => x).OfType<Vw_Sales_Details>().ToList();
                       if (BillList.Count > 0)
                       {
                           DataTable dt = new DataTable("DeliverySchedule");

                           dt.Columns.Add("Pk_Invoice");
                           dt.Columns.Add("InvDate");
                           dt.Columns.Add("CustomerName");
                           dt.Columns.Add("Quantity");
                           dt.Columns.Add("Price");
                           dt.Columns.Add("GrandTotal");

                           dt.Columns.Add("NETVALUE");
                           dt.Columns.Add("TaxType");
                           dt.Columns.Add("TaxName");

                           dt.Columns.Add("TaxValue");
                           dt.Columns.Add("Fk_OrderNo");
                           dt.Columns.Add("PayMode");
                           dt.Columns.Add("PName");
                           dt.Columns.Add("BName");
                           dt.Columns.Add("Pk_Inv_Details");


                           foreach (Vw_Sales_Details entity in BillList)
                           {
                               DataRow row = dt.NewRow();

                               row["Pk_Invoice"] = entity.Pk_Invoice;
                               row["InvDate"] = entity.InvDate;
                               row["CustomerName"] = entity.CustomerName;
                               row["Quantity"] = entity.Quantity;
                               row["Price"] = entity.Price;
                               row["GrandTotal"] = entity.GrandTotal;
                               row["NETVALUE"] = entity.NETVALUE;
                               row["TaxType"] = entity.TaxType;
                               //row["TaxName"] = entity.TaxName;

                               //row["TaxValue"] = entity.TaxValue;
                               row["Fk_OrderNo"] = entity.Fk_OrderNo;
                               row["PayMode"] = entity.PayMode;
                               //row["PName"] = entity.PName;
                               //row["BName"] = entity.BName;
                               row["Pk_Inv_Details"] = entity.Pk_Inv_Details;

                               dt.Rows.Add(row);

                           }

                           DataSet ds = new DataSet();
                           ds.Tables.Add(dt);
                           CogitoStreamline.Report.Sales_DateWiseSumm orp = new CogitoStreamline.Report.Sales_DateWiseSumm();

                           orp.Load("@\\Report\\Sales_DateWiseSumm.rpt");
                           orp.SetDataSource(dt.DefaultView);



                           ReportDocument repDoc = orp;


                           ParameterFieldDefinitions crParameterFieldDefinitions4;
                           ParameterFieldDefinition crParameterFieldDefinition4;
                           ParameterValues crParameterValues4 = new ParameterValues();
                           ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                           crParameterDiscreteValue4.Value = Repfor;
                           crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                           crParameterFieldDefinition4 = crParameterFieldDefinitions4["period"];
                           crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                           crParameterValues4.Clear();
                           crParameterValues4.Add(crParameterDiscreteValue4);
                           crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);

                           Response.Buffer = false;
                           Response.ClearContent();
                           Response.ClearHeaders(); 

                           string pdfPath = Server.MapPath("~/ConvertPDF/" + "DatesWise_SalesSummary" + ".pdf");
                           FileInfo file = new FileInfo(pdfPath);
                           if (file.Exists)
                           {
                               file.Delete();
                           }
                           var pd = new PrintDocument();


                           //orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           //orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           ////  CreateBookmarksFromGroupTree = true;
                           ////   pdffprnat.CreateBookmarksFromGroupTree = true;
                           //DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                           //objDiskOpt.DiskFileName = pdfPath;

                           //orp.ExportOptions.DestinationOptions = objDiskOpt;
                           //orp.Export();


                           ExportOptions rptOptions = new ExportOptions();
                           PdfFormatOptions pdffprnat = new PdfFormatOptions();
                           pdffprnat.CreateBookmarksFromGroupTree = true;
                           rptOptions.ExportFormatOptions = pdffprnat;
                           rptOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           rptOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           DiskFileDestinationOptions dest = new DiskFileDestinationOptions();
                           dest.DiskFileName = pdfPath;
                           rptOptions.ExportDestinationOptions = dest;
                           orp.Export(rptOptions);

                       }
                       else
                       {

                       }
                       return null;
                   }
                   catch (Exception ex)
                   {
                       return null;
                       //  Response.Redirect("Index");D:\shantha\Billing\CogitoStreamline\CogitoStreamline\Controllers\CartonBillingController.cs
                   }
               }


               public JsonResult InwardDates_VendorWise([System.Web.Http.FromBody]string data)
               {
                   try
                   {

                       data = data.TrimStart('=');
                       int i = 0;

                       Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

                       DateTime SFrom = Convert.ToDateTime(oValues["FDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       DateTime STo = Convert.ToDateTime(oValues["TDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       string[] a = oValues["FDate"].ToString().Split('/');
                       DateTime FromDate = DateTime.ParseExact(a[1] + '/' + a[0] + '/' + a[2] + " 12:00", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);
                       string SFromDVal;
                       var FD = SFrom.Day;
                       var FM = SFrom.ToString("MMM");
                       var FY = SFrom.Year;


                       SFromDVal = FD.ToString() + '/' + FM.ToString() + '/' + FY.ToString();



                       var TD = STo.Day;
                       var TM = STo.ToString("MMM");
                       var TY = STo.Year;

                       string SToDVal;
                       //if (TD > 12)
                       //{ SToDVal = TM.ToString() + '/' + TD.ToString() + '/' + TY.ToString(); }
                       //else
                       SToDVal = TD.ToString() + '/' + TM.ToString() + '/' + TY.ToString();


                       int dDay = Convert.ToInt32(a[0]);
                       int dmonth = Convert.ToInt32(a[1]);
                       int dyear = Convert.ToInt32(a[2]);

                       string monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dmonth);
                       //string Mname=FromDate.ToString( "MMMM");   // this one will also work to get the month name

                       string Repfor = "Inwards-Vendorwise Between - " + " " + SFromDVal + " " + "- " + SToDVal + " ";
                       CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                       //                       SELECT Vw_MatInwdDetails.MatName, Vw_MatInwdDetails.Pk_Inward, Vw_MatInwdDetails.Inward_Date, 
                       //                      Vw_MatInwdDetails.PONo, Vw_MatInwdDetails.Sqty
                       //FROM   BalajiTesting.dbo.Vw_MatInwdDetails Vw_MatInwdDetails
                       //ORDER BY Vw_MatInwdDetails.Pk_Inward




                       List<Vw_MatInwdDetails> BillList = new List<Vw_MatInwdDetails>();
                       // CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                       BillList = dc.Vw_MatInwdDetails.Where(x => x.Inward_Date >= SFrom && x.Inward_Date <= STo).OrderBy(x => x.Inward_Date).Select(x => x).OfType<Vw_MatInwdDetails>().ToList();
                       if (BillList.Count > 0)
                       {
                           DataTable dt = new DataTable("DeliverySchedule");

                           dt.Columns.Add("MatName");
                           dt.Columns.Add("Pk_Inward");
                           dt.Columns.Add("Inward_Date");
                           dt.Columns.Add("PONo");
                           dt.Columns.Add("Sqty");
                           dt.Columns.Add("VendorName");



                           foreach (Vw_MatInwdDetails entity in BillList)
                           {
                               DataRow row = dt.NewRow();

                               //row["Pk_InwardDet"] = entity.Pk_InwardDet;
                               row["MatName"] = entity.MatName;
                               row["Pk_Inward"] = entity.Pk_Inward;
                               row["Inward_Date"] = entity.Inward_Date;
                               row["PONo"] = entity.PONo;
                               row["Sqty"] = entity.Sqty;
                               row["VendorName"] = entity.VendorName;


                               dt.Rows.Add(row);

                           }

                           DataSet ds = new DataSet();
                           ds.Tables.Add(dt);
                           CogitoStreamline.Report.Vendorwise_InwdDates orp = new CogitoStreamline.Report.Vendorwise_InwdDates();

                           orp.Load("@\\Report\\Vendorwise_InwdDates.rpt");
                           orp.SetDataSource(dt.DefaultView);



                           ReportDocument repDoc = orp;


                           ParameterFieldDefinitions crParameterFieldDefinitions4;
                           ParameterFieldDefinition crParameterFieldDefinition4;
                           ParameterValues crParameterValues4 = new ParameterValues();
                           ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                           crParameterDiscreteValue4.Value = Repfor;
                           crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                           crParameterFieldDefinition4 = crParameterFieldDefinitions4["period"];
                           crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                           crParameterValues4.Clear();
                           crParameterValues4.Add(crParameterDiscreteValue4);
                           crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);


                           Response.Buffer = false;
                           Response.ClearContent();
                           Response.ClearHeaders(); 

                           string pdfPath = Server.MapPath("~/ConvertPDF/" + "Vendorwise_InwdDates" + ".pdf");
                           FileInfo file = new FileInfo(pdfPath);
                           if (file.Exists)
                           {
                               file.Delete();
                           }
                           var pd = new PrintDocument();


                           //orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           //orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           ////  CreateBookmarksFromGroupTree = true;
                           ////   pdffprnat.CreateBookmarksFromGroupTree = true;
                           //DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                           //objDiskOpt.DiskFileName = pdfPath;

                           //orp.ExportOptions.DestinationOptions = objDiskOpt;
                           //orp.Export();


                           ExportOptions rptOptions = new ExportOptions();
                           PdfFormatOptions pdffprnat = new PdfFormatOptions();
                           pdffprnat.CreateBookmarksFromGroupTree = true;
                           rptOptions.ExportFormatOptions = pdffprnat;
                           rptOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           rptOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           DiskFileDestinationOptions dest = new DiskFileDestinationOptions();
                           dest.DiskFileName = pdfPath;
                           rptOptions.ExportDestinationOptions = dest;
                           orp.Export(rptOptions);

                       }
                       else
                       {

                       }
                       return null;
                   }
                   catch (Exception ex)
                   {
                       return null;
                       //  Response.Redirect("Index");D:\shantha\Billing\CogitoStreamline\CogitoStreamline\Controllers\CartonBillingController.cs
                   }
               }


               [HttpPost]
               public JsonResult FillGrid(string Pk_ID = "",  int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
               {
                   try
                   {
                       List<SearchParameter> oSearchParams = new List<SearchParameter>();
                       oSearchParams.Add(new SearchParameter("Pk_ID", Pk_ID));
                       //oSearchParams.Add(new SearchParameter("EnquiryFromDate", EnquiryFromDate));
                       //oSearchParams.Add(new SearchParameter("EnquiryToDate", EnquiryToDate));
                       //oSearchParams.Add(new SearchParameter("Customer", Customer));
                       //oSearchParams.Add(new SearchParameter("CommunicationType", CommunicationType));
                       //oSearchParams.Add(new SearchParameter("State", State));
                       oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                       oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                       SearchResult oSearchResult = oEnq.SearchTon(oSearchParams);

                       List<EntityObject> oEnquirysToDisplayObjects = oSearchResult.ListOfRecords;
                       List<TonnValues> oEnquiryObjects = oEnquirysToDisplayObjects.Select(p => p).OfType<TonnValues>().ToList();

                       //Create a anominious object here to break the circular reference
                       var oEnquiryToDisplay = oEnquiryObjects.Select(p => new
                       {
                           Pk_ID = p.Pk_ID,
                           ExStock = p.ExStock,
                           IssStock = p.IssStock,
                           IssRetStock = p.IssRetStock,
                          WastageVal = p.WastageVal,
                           ProductionVal = p.ProductionVal,
                           ////SpecialRequirements = p.SpecialRequirements,
                           //Fk_Status = p.wfState.State,
                           //EqComments = p.EqComments

                       }).ToList();

                       return Json(new { Result = "OK", Records = oEnquiryToDisplay, TotalRecordCount = oSearchResult.RecordCount });
                   }
                   catch (Exception ex)
                   {
                       return Json(new { Result = "ERROR", Message = ex.Message });
                   }
               }

               public JsonResult JC_Dates([System.Web.Http.FromBody]string data)
               {
                   try
                   {

                       data = data.TrimStart('=');
                       int i = 0;

                       Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

                       DateTime SFrom = Convert.ToDateTime(oValues["FDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       DateTime STo = Convert.ToDateTime(oValues["TDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                       string[] a = oValues["FDate"].ToString().Split('/');
                       DateTime FromDate = DateTime.ParseExact(a[1] + '/' + a[0] + '/' + a[2] + " 12:00", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);
                       string SFromDVal;
                       var FD = SFrom.Day;
                       var FM = SFrom.ToString("MMM");
                       var FY = SFrom.Year;


                       SFromDVal = FD.ToString() + '/' + FM.ToString() + '/' + FY.ToString();



                       var TD = STo.Day;
                       var TM = STo.ToString("MMM");
                       var TY = STo.Year;

                       string SToDVal;
                       //if (TD > 12)
                       //{ SToDVal = TM.ToString() + '/' + TD.ToString() + '/' + TY.ToString(); }
                       //else
                       SToDVal = TD.ToString() + '/' + TM.ToString() + '/' + TY.ToString();


                       int dDay = Convert.ToInt32(a[0]);
                       int dmonth = Convert.ToInt32(a[1]);
                       int dyear = Convert.ToInt32(a[2]);

                       string monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dmonth);
                       //string Mname=FromDate.ToString( "MMMM");   // this one will also work to get the month name

                       string Repfor = "Job Card Details Between - " + " " + SFromDVal + " " + "- " + SToDVal + " ";
                       CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                       //                       SELECT Vw_JCReport.Pk_Invoice, Vw_JCReport.JDate, Vw_JCReport.CustomerName,
                       //                      Vw_JCReport.Quantity, Vw_JCReport.Price, Vw_JCReport.GrandTotal, Vw_JCReport.NETVALUE, 
                       //                      Vw_JCReport.TaxType, Vw_JCReport.TaxName
                       //FROM   BalajiTesting.dbo.Vw_JCReport Vw_JCReport


                       List<Vw_JCReport> BillList = new List<Vw_JCReport>();
                       // CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                       BillList = dc.Vw_JCReport.Where(x => x.JDate >= SFrom && x.JDate <= STo).OrderBy(x => x.JDate).Select(x => x).OfType<Vw_JCReport>().ToList();
                       if (BillList.Count > 0)
                       {
                           DataTable dt = new DataTable("DeliverySchedule");

                           dt.Columns.Add("Pk_JobCardID");
                           dt.Columns.Add("JDate");
                           dt.Columns.Add("BName");
                           dt.Columns.Add("CustomerName");
                           dt.Columns.Add("PName");
                           dt.Columns.Add("TotalQty");
                           dt.Columns.Add("Cust_PO");

                           foreach (Vw_JCReport entity in BillList)
                           {
                               DataRow row = dt.NewRow();

                               row["Pk_JobCardID"] = entity.Pk_JobCardID;
                               row["JDate"] = entity.JDate;
                               row["CustomerName"] = entity.CustomerName;
                               row["BName"] = entity.BName;
                               row["CustomerName"] = entity.CustomerName;
                               row["PName"] = entity.PName;
                               row["TotalQty"] = entity.TotalQty;
                               row["Cust_PO"] = entity.Cust_PO;
                           

                               dt.Rows.Add(row);

                           }

                           DataSet ds = new DataSet();
                           ds.Tables.Add(dt);
                           CogitoStreamline.Report.JC_List orp = new CogitoStreamline.Report.JC_List();

                           orp.Load("@\\Report\\JC_List.rpt");
                           orp.SetDataSource(dt.DefaultView);



                           ReportDocument repDoc = orp;


                           ParameterFieldDefinitions crParameterFieldDefinitions4;
                           ParameterFieldDefinition crParameterFieldDefinition4;
                           ParameterValues crParameterValues4 = new ParameterValues();
                           ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                           crParameterDiscreteValue4.Value = Repfor;
                           crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                           crParameterFieldDefinition4 = crParameterFieldDefinitions4["period"];
                           crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                           crParameterValues4.Clear();
                           crParameterValues4.Add(crParameterDiscreteValue4);
                           crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);

                           Response.Buffer = false;
                           Response.ClearContent();
                           Response.ClearHeaders();
                           string pdfPath = Server.MapPath("~/ConvertPDF/" + "JC_List" + ".pdf");
                           FileInfo file = new FileInfo(pdfPath);
                           if (file.Exists)
                           {
                               file.Delete();
                           }
                           var pd = new PrintDocument();


                           //orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           //orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           ////  CreateBookmarksFromGroupTree = true;
                           ////   pdffprnat.CreateBookmarksFromGroupTree = true;
                           //DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                           //objDiskOpt.DiskFileName = pdfPath;

                           //orp.ExportOptions.DestinationOptions = objDiskOpt;
                           //orp.Export();


                           ExportOptions rptOptions = new ExportOptions();
                           PdfFormatOptions pdffprnat = new PdfFormatOptions();
                           pdffprnat.CreateBookmarksFromGroupTree = true;
                           rptOptions.ExportFormatOptions = pdffprnat;
                           rptOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                           rptOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                           DiskFileDestinationOptions dest = new DiskFileDestinationOptions();
                           dest.DiskFileName = pdfPath;
                           rptOptions.ExportDestinationOptions = dest;
                           orp.Export(rptOptions);

                       }
                       else
                       {

                       }
                       return null;
                   }
                   catch (Exception ex)
                   {
                       return null;
                       //  Response.Redirect("Index");D:\shantha\Billing\CogitoStreamline\CogitoStreamline\Controllers\CartonBillingController.cs
                   }
               }

    
    
    
    
    }
}


