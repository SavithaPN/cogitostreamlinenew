﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;

namespace CogitoStreamline.Controllers
{
    public class PaperDistributionController : CommonController
    {
    
        Material oMat = new Material();
        InwardMaterial oInwd = new InwardMaterial();
        Issue oIssue = new Issue();
        IssueReturns oRet=new IssueReturns();


        public PaperDistributionController()
        {
            oDoaminObject = new Material();

        }

        public override ActionResult Index()
        {
            ViewBag.Header = "Paper Distribution";

            return base.Index();
        }


        [HttpPost]
        public JsonResult PaperDuplicateChecking(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sName = oValues["Name"];

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Name", sName));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oPaperToDisplayObjects = oSearchResult.ListOfRecords;
                List<Inv_Material> oPaperObjects = oPaperToDisplayObjects.Select(p => p).OfType<Inv_Material>().ToList();

                var oPaperToDisplay = oPaperObjects.Select(p => new
                {
                    Pk_Material = p.Pk_Material,
                    Name = p.Name

                }).ToList().OrderBy(s => s.Name);

                return Json(new { Success = true, Records = oPaperToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }



        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            /* Pick Paper relevat fields from Material and create a structure */
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                Material oPaper = oDoaminObject as Material;
                oPaper.ID = decimal.Parse(pId);
                Inv_Material oAtualObject = oPaper.DAO as Inv_Material;
                //Create a anominious object here to break the circular reference
                var oPaperToDisplay = new
                {
                    Pk_Material = oAtualObject.Pk_Material,
                    Name = oAtualObject.Name,
                    Fk_Mill = oAtualObject.Fk_Mill,
                    Fk_PaperType = oAtualObject.Fk_PaperType,
                    Fk_Color = oAtualObject.Fk_Color,
                    Deckle = oAtualObject.Deckle,
                    GSM = oAtualObject.GSM,
                    BF = oAtualObject.BF,
                    Fk_UnitId = oAtualObject.Fk_UnitId,
                    Description = oAtualObject.Description,
                    Moisture = oAtualObject.Moisture,
                    PartNo = oAtualObject.PartNo,
                    Min_Value = oAtualObject.Min_Value,
                    Max_Value = oAtualObject.Max_Value,
                    

                };

                return Json(new { success = true, data = oPaperToDisplay });
            }
            else
            {
                var oPaperToDisplay = new Material();
                return Json(new { success = true, data = oPaperToDisplay });
            }
        }
        [HttpPost]
        public JsonResult InwardRec(string Pk_Inward = "", string Pk_Mat = "", string MaterialName = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {

            try
            {
                //  QualityChild oIssueDetails = new QualityChild();
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_Inward", Pk_Inward));
                oSearchParams.Add(new SearchParameter("Pk_Mat", Pk_Mat));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oInwd.SearchInwdDetails(oSearchParams);

                List<EntityObject> oIssueDetailsToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_TotInwdQty> oIssueObjects = oIssueDetailsToDisplayObjects.Select(p => p).OfType<Vw_TotInwdQty>().ToList();

                //Create a anominious object here to break the circular reference
                var oIssueDetailsToDisplay = oIssueObjects.Select(p => new
                {
                    PkMaterial = p.Pk_Material,
                    PkInwd = p.Pk_Inward,
                    InwDate = p.Inward_Date != null ? DateTime.Parse(p.Inward_Date.ToString()).ToString("dd/MM/yyyy") : "",
                    Name = p.Name,
                    Quantity = p.Qty,
                    //QC = p.Fk_QC,
                    //Indent = p.Fk_Indent,
                    //PONo = p.PONo,
                    //ReelNo = p.RollNo

                }).ToList();

                return Json(new { Result = "OK", Records = oIssueDetailsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = "Select Quality Check No. and Proceed" });
            }
        }



        [HttpPost]
        public JsonResult IssueRec(string DateValue = "", string Pk_Mat = "", string MaterialName = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {

            try
            {
                //  QualityChild oIssueDetails = new QualityChild();
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("DateValue", DateValue));
                oSearchParams.Add(new SearchParameter("Pk_Mat", Pk_Mat));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oIssue.SearchIssueDetails(oSearchParams);

                List<EntityObject> oIssueDetailsToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_PapDistIssueList> oIssueObjects = oIssueDetailsToDisplayObjects.Select(p => p).OfType<Vw_PapDistIssueList>().ToList();

                //Create a anominious object here to break the circular reference
                var oIssueDetailsToDisplay = oIssueObjects.Select(p => new
                {

                    //SELECT     SUM(dbo.MaterialIssueDetails.Quantity) AS qty, dbo.MaterialIssue.Pk_MaterialIssueID, 
                    //dbo.MaterialIssue.IssueDate, dbo.Inv_Material.Pk_Material, dbo.Inv_Material.Name, 
                    //  dbo.JobCardMaster.Pk_JobCardID, dbo.JobCardMaster.JDate, dbo.JobCardMaster.Fk_Order, dbo.gen_Customer.CustomerName

                    PkMaterial = p.Pk_Material,
                    MatIssueID = p.Pk_MaterialIssueID,
                    IssueDate = p.IssueDate != null ? DateTime.Parse(p.IssueDate.ToString()).ToString("dd/MM/yyyy") : "",
                    JobCardID=p.Pk_JobCardID,
                    JobDate = p.JDate != null ? DateTime.Parse(p.JDate.ToString()).ToString("dd/MM/yyyy") : "",
                    Name = p.Name,
                    Quantity = p.qty,
                    OrderNo = p.Fk_Order,
                    Customer=p.CustomerName,

                    //Indent = p.Fk_Indent,
                    //PONo = p.PONo,
                    //ReelNo = p.RollNo

                }).ToList();

                return Json(new { Result = "OK", Records = oIssueDetailsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = "Select Quality Check No. and Proceed" });
            }
        }


        [HttpPost]
        public JsonResult IssueReturnsRec(string DateValue = "", string IssueID = "", string Pk_Mat = "", string MaterialName = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {

            try
            {
                //  QualityChild oIssueDetails = new QualityChild();
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("IssueID", IssueID));
                oSearchParams.Add(new SearchParameter("Pk_Mat", Pk_Mat));
                oSearchParams.Add(new SearchParameter("DateValue", DateValue));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oRet.SearchReturnDetails(oSearchParams);

                List<EntityObject> oIssueDetailsToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_Iss_Return> oIssueObjects = oIssueDetailsToDisplayObjects.Select(p => p).OfType<Vw_Iss_Return>().ToList();

                //Create a anominious object here to break the circular reference
                var oIssueDetailsToDisplay = oIssueObjects.Select(p => new
                {

                    PkMaterial = p.Pk_Material,
                    Pk_IssueReturnMasterId = p.Pk_IssueReturnMasterId,
                    IssueReturnDate = p.IssueReturnDate != null ? DateTime.Parse(p.IssueReturnDate.ToString()).ToString("dd/MM/yyyy") : "",
                    Fk_IssueId = p.Fk_IssueId,
                    IssueDate = p.IssueDate != null ? DateTime.Parse(p.IssueDate.ToString()).ToString("dd/MM/yyyy") : "",
                    Name = p.Name,
                    ReturnQuantity = p.ReturnQuantity,
                 
                }).ToList();

                return Json(new { Result = "OK", Records = oIssueDetailsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = "Select Quality Check No. and Proceed" });
            }
        }



        [HttpPost]
        public JsonResult PaperListByFiter(string Name = "", string Type = "", string Deckle = "", string Mill = "", string Fk_Color = "", string GSM = "", string BF = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Name", Name));
                oSearchParams.Add(new SearchParameter("PaperType", Type));
                oSearchParams.Add(new SearchParameter("GSM", GSM));
                oSearchParams.Add(new SearchParameter("BF", BF));
                oSearchParams.Add(new SearchParameter("Mill", Mill));
                oSearchParams.Add(new SearchParameter("Deckle", Deckle));
                oSearchParams.Add(new SearchParameter("Fk_Color", Fk_Color));
                oSearchParams.Add(new SearchParameter("Category", "Paper"));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oPaperToDisplayObjects = oSearchResult.ListOfRecords;
                List<Inv_Material> oPaperObjects = oPaperToDisplayObjects.Select(p => p).OfType<Inv_Material>().ToList();

                //Create a anominious object here to break the circular reference
                var oRawMaterialsToDisplay = oPaperObjects.Select(p => new
                {
                    Pk_Material = p.Pk_Material,
                    Name = p.Name,
                    Type = p.gen_PaperType.PaperTypeName,
                    PaperColor = p.gen_Color.ColorName,
                    Deckle = p.Deckle,
                    Mill = p.gen_Mill.MillName,
                    GSM = p.GSM,
                    BF = p.BF,
                    Unit = p.gen_Unit.UnitName,
                    Description = p.Description,
                    //Sname=p.Sname
                }).ToList();

                return Json(new { Result = "OK", Records = oRawMaterialsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult getColor(string pId = "")
        {
            List<gen_Color> oListOfColor = new Color().Search(null).ListOfRecords.OfType<gen_Color>().ToList();
            var oColorToDisplay = oListOfColor.Select(p => new
            {
                Name = p.ColorName,
                Id = p.Pk_Color
            });

            return Json(oColorToDisplay);
        }

        [HttpPost]
        public JsonResult getUnit(string pId = "")
        {
            List<gen_Unit> oListOfUnit = new Unit().Search(null).ListOfRecords.OfType<gen_Unit>().ToList();
            var oUnitToDisplay = oListOfUnit.Select(p => new
            {
                Name = p.UnitName,
                Id = p.Pk_Unit
            });

            return Json(oUnitToDisplay);
        }

        [HttpPost]
        public JsonResult getPaperType(string pId = "")
        {
            List<gen_PaperType> oListOfUnit = new PaperType().Search(null).ListOfRecords.OfType<gen_PaperType>().ToList();
            var oListOfUnitToDisplay = oListOfUnit.Select(p => new
            {
                Name = p.PaperTypeName,
                Id = p.Pk_PaperType
            });

            return Json(oListOfUnitToDisplay);
        }
        [HttpPost]
        public JsonResult getMill(string pId = "")
        {
            List<gen_Mill> oListOfUnit = new Mill().Search(null).ListOfRecords.OfType<gen_Mill>().ToList();
            var oListOfUnitToDisplay = oListOfUnit.Select(p => new
            {
                Name = p.MillName,
                Id = p.Pk_Mill
            });

            return Json(oListOfUnitToDisplay);
        }



        //[HttpPost] 
        //public ActionResult MaterialDetails (string data = "")
        //{
        //   Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);


        //    string pId = oValues["fkMat"];
        //    if (pId != "")
        //    {
        //        var BoxID=int.Parse(pId);
        //        List<SearchParameter> oSearchParams = new List<SearchParameter>();
        //        oSearchParams.Add(new SearchParameter("fkMat", pId));
        //        //oSearchParams.Add(new SearchParameter("BFval", BFval));
        //        //oSearchParams.Add(new SearchParameter("Pk_Mat", Pk_Mat));
        //        //oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
        //        //oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

        //        SearchResult oSearchResult = oMat.SearchMat(oSearchParams);
        //        List<EntityObject> oHeadToDisplayObjects = oSearchResult.ListOfRecords;

        //        List<Vw_JobBoxDetails> oHeadObjects = oHeadToDisplayObjects.Select(p => p).OfType<Vw_JobBoxDetails>().ToList();
        //        //Create a anominious object here to break the circular reference
        //        var oHeadToDisplay = oHeadObjects.Select(p => new
        //        {
        //            Pk_BoxID = p.Fk_BoxID,
        //            Name = p.Name,
        //            CustomerName = p.CustomerName,
        //            Pk_JobCardID = p.Pk_JobCardID,
        //            JDate = DateTime.Parse(p.JDate.ToString()).ToString("dd/MM/yyyy"),
        //            Quantity = p.Quantity,
        //            TotalPrice = p.TotalPrice

        //        }).ToList().OrderBy(s => s.Pk_BoxID);
        //        return Json(new { Result = "OK", Records = oHeadToDisplay, TotalRecordCount = oSearchResult.RecordCount });


        //}
        //}


        [HttpPost]
        public ActionResult MaterialDetails(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);


            string pId = oValues["fkMat"];
            if (pId != "")
            {
                var BoxID = int.Parse(pId);
                ModelManuplationResult oResult = new ModelManuplationResult();

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("fkMat", pId));

                SearchResult oSearchResult = oMat.SearchMat(oSearchParams);

                List<EntityObject> oEnquirysToDisplayObjects = oSearchResult.ListOfRecords;
                List<Inv_Material> oEnquiryObjects = oEnquirysToDisplayObjects.Select(p => p).OfType<Inv_Material>().ToList();

                var oEnquiryToDisplay = oEnquiryObjects.Select(p => new
                {

                    Pk_Material = p.Pk_Material,
                    Name = p.Name,
                    Type = p.gen_PaperType.PaperTypeName,
                    PaperColor = p.gen_Color.ColorName,
                    Deckle = p.Deckle,
                    Mill = p.gen_Mill.MillName,
                    GSM = p.GSM,
                    BF = p.BF,
                    Unit = p.gen_Unit.UnitName,
                    Description = p.Description

                });

                return Json(new { success = true, data = oEnquiryToDisplay });

            }
            return null;
        }

    }
}