﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;

namespace CogitoStreamline.Controllers
{
    public class InkController : CommonController
    {
        //
        // GET: /Mill/
        Material oMaterial = new Material();


        Color oColor = new Color();

        Unit oUnit = new Unit();


        public InkController()
        {
            oDoaminObject = new Material();
        }

        protected override void Dispose(bool disposing)
        {

            oColor.Dispose();

            oUnit.Dispose();
            base.Dispose(disposing);
        }

        public override ActionResult Index()
        {
            ViewBag.Header = "Ink";
            return base.Index();
        }



        [HttpPost]
        public JsonResult getUnit(string pId = "")
        {
            List<gen_Unit> oListOfUnit = oUnit.Search(null).ListOfRecords.OfType<gen_Unit>().ToList();
            var oUnitToDisplay = oListOfUnit.Select(p => new
            {
                Name = p.UnitName,
                Id = p.Pk_Unit
            });

            return Json(oUnitToDisplay);
        }


        [HttpPost]
        public JsonResult getColor(string pId = "")
        {
            List<gen_Color> oListOfColor = oColor.Search(null).ListOfRecords.OfType<gen_Color>().ToList();
            var oColorToDisplay = oListOfColor.Select(p => new
            {
                Name = p.ColorName,
                Id = p.Pk_Color
            });

            return Json(oColorToDisplay);
        }

        [HttpPost]
        public JsonResult InkDuplicateChecking(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sName = oValues["Name"];

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Name", sName));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oInkToDisplayObjects = oSearchResult.ListOfRecords;
                List<Inv_Material> oInkObjects = oInkToDisplayObjects.Select(p => p).OfType<Inv_Material>().ToList();

                var oInkToDisplay = oInkObjects.Select(p => new
                {
                    Pk_Material = p.Pk_Material,
                    Name = p.Name
                }).ToList().OrderBy(s => s.Name);

                return Json(new { Success = true, Records = oInkToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }
        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                Material oMaterial = oDoaminObject as Material;
                oMaterial.ID = decimal.Parse(pId);
                Inv_Material oAtualObject = oMaterial.DAO as Inv_Material;
                //Create a anominious object here to break the circular reference

                var oMaterialToDisplay = new
                {

                    Pk_Material = oAtualObject.Pk_Material,
                    Name = oAtualObject.Name,
                    Brand = oAtualObject.Brand,
                    Fk_Color = oAtualObject.Fk_Color,
                    Fk_UnitId = oAtualObject.Fk_UnitId,
                    Size = oAtualObject.Size,
                    Description = oAtualObject.Description,
                    PartNo = oAtualObject.PartNo,
                    Min_Value = oAtualObject.Min_Value,
                    Max_Value = oAtualObject.Max_Value

                };

                return Json(new { success = true, data = oMaterialToDisplay });
            }
            else
            {
                var oMaterialToDisplay = new Inv_Material();
                return Json(new { success = true, data = oMaterialToDisplay });
            }
        }

        [HttpPost]
        public JsonResult InkListByFiter(string Brand = "", string Name = "", string Fk_Color = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Name", Name));
                oSearchParams.Add(new SearchParameter("Fk_Color", Fk_Color));
                oSearchParams.Add(new SearchParameter("Brand", Brand));
                oSearchParams.Add(new SearchParameter("Category", "Ink"));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oMaterialToDisplayObjects = oSearchResult.ListOfRecords;
                List<Inv_Material> oMaterialObjects = oMaterialToDisplayObjects.Select(p => p).OfType<Inv_Material>().ToList();

                //Create a anominious object here to break the circular reference
                var oMaterialToDisplay = oMaterialObjects.Select(p => new
                {


                    Pk_Material = p.Pk_Material,
                    Name = p.Name,
                    Brand = p.Brand,
                    Fk_Color = p.Fk_Color!=null ? p.gen_Color.ColorName:"",
                    Fk_UnitId = p.gen_Unit.UnitName,
                    Size = p.Size,
                    Description = p.Description

                }).ToList().OrderBy(s => s.Name);

                return Json(new { Result = "OK", Records = oMaterialToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

    }
}

