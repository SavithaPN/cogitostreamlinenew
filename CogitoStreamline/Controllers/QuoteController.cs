﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;
using System.IO;
using System.Data;
using CrystalDecisions.ReportAppServer.ReportDefModel;
using System.Drawing.Printing;
using CrystalDecisions.Shared;
using System.Data.SqlClient;
namespace CogitoStreamLine.Controllers
{
    public class QuoteController : CommonController
    {
        public QuoteController()
        {
           
            oDoaminObject = new QuotationM();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Quotation";
            return base.Index();

            //DB.tablename.list
        }
        protected override void Dispose(bool disposing)
        {

            base.Dispose(disposing);
        }
         [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
         
            string pId = oValues["fkbox"];
            if (pId != "")
            {
                QuotationM oQuote = oDoaminObject as QuotationM;
              //  oQuote.ID = decimal.Parse(pId);
                QuotationMaster oAtualObject = oQuote.DAO as QuotationMaster;

                int i = -1;
                var oDelivery = oAtualObject.QuotationDetails.Select(p => new
                {
                    Pk_QuotationDetID = p.Pk_QuotationDetID,
                    slno = ++i,
                    Quantity = p.Quantity,
                    Price = p.CostPerBox,
                    Dimension = p.Dimension,
                    BSValue=p.BSValue,
                    BoxID=p.BoxID,
                    BName=p.BName,
                    Fk_EstimationID = p.Fk_EstimationID,
                    //Fk_Status = p.Fk_Status,
                    //Ddate = p.Ddate != null ? DateTime.Parse(p.Ddate.ToString()).ToString("dd/MM/yyyy") : "",
                });


                //Create a anominious object here to break the circular reference
                var oOrderToDisplay = new
                {
                    Grandtotal = oAtualObject.Grandtotal,
                    Description = oAtualObject.Description,
                    Fk_Enquiry = oAtualObject.Fk_Enquiry,
                    Pk_Quotation = oAtualObject.Pk_Quotation,
                    QuotationDate = DateTime.Parse(oAtualObject.QuotationDate.ToString()).ToString("dd/MM/yyyy"),
                    //Fk_Status = oAtualObject.Fk_Status,
                    deliverySchedule = Json(oDelivery).Data,
                    //State = oAtualObject.wfState.State,

                    //PONo = oAtualObject.Cust_PO

                };

                return Json(new { success = true, data = oOrderToDisplay });
            }
            else
            {
                var oOrderToDisplay = new gen_Order();
                return Json(new { success = true, data = oOrderToDisplay });
            }
        }


         //[HttpPost]
         //public ActionResult SQuoteDet(string data = "")
         //{
         //    Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);


         //    string pId = oValues["Pk_Machine"];
         //    if (pId != "")
         //    {
         //        var BoxID = int.Parse(pId);
         //        ModelManuplationResult oResult = new ModelManuplationResult();

         //        List<SearchParameter> oSearchParams = new List<SearchParameter>();
         //        oSearchParams.Add(new SearchParameter("Pk_Machine", pId));

         //        SearchResult oSearchResult = oBoxMaster.SearchMachine(oSearchParams);

         //        List<EntityObject> oEnquirysToDisplayObjects = oSearchResult.ListOfRecords;
         //        List<gen_Machine> oEnquiryObjects = oEnquirysToDisplayObjects.Select(p => p).OfType<gen_Machine>().ToList();

         //        var oEnquiryToDisplay = oEnquiryObjects.Select(p => new
         //        {

         //            Capacity_Length = p.Capacity_Length,

         //        });
         //        //PDFReport(oResult.Message);
         //        //QuotationM oNewInv = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(QuotationM), oValues1) as QuotationM;
         //        //QuotationM oNewInvFromShelf = _oEntities.QuotationM.Where(p => p.Pk_Quotation == oNewInv.Pk_Quotation).Single();
         //        //Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
         //        return Json(new { success = true, data = oEnquiryToDisplay });




         //    }
         //    return null;
         //}

        [HttpPost]
        public ActionResult SaveQuoteDet(string data = "")
        {
              Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);


              decimal enqChild = 0; decimal? temp = 0;

              var oHeadToDisplay = new
              {
                
                  QuoteNo = enqChild
                  
              };


              string pId = oValues["Fk_BoxEstimation"];
              if (pId != "")
              {

                  QuotationM oQuote = oDoaminObject as QuotationM;
                  //  oQuote.ID = decimal.Parse(pId);
                  QuotationMaster oAtualObject = oQuote.DAO as QuotationMaster;
                  CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                  //est_BoxEstimation oAtualObjectBoxEst = oEst.DAO as est_BoxEstimation;

                  List<QuotationMaster> EstList = new List<QuotationMaster>();
                  int Estid = int.Parse(pId);

                  EstList = dc.QuotationMaster.Where(x => x.Fk_Enquiry == Estid).Select(x => x).OfType<QuotationMaster>().ToList();
                  if (EstList.Count > 0)
                  {


                      oHeadToDisplay = new
                {
                    QuoteNo = EstList.FirstOrDefault().Pk_Quotation
                
                };
                      var QNo=(oHeadToDisplay.QuoteNo).ToString();

                    ////////////////////////////////////////////


                      string pdfPath = Server.MapPath("~/ConvertPDF/" + "Quote-" + " " + QNo + ".pdf");

                      FileInfo file = new FileInfo(pdfPath);

                      if (file.Exists)
                      {
                          var oHeadToDisplay1 = new
                          {

                              //Fk_BoxEstimation = decimal.Parse(pId),
                              QuoteNo = (oHeadToDisplay.QuoteNo).ToString()
                          };
                          return Json(new { success = true, data = oHeadToDisplay1 });
                      }
                      else
                      {
                          Boolean Result;
                          String Invoice = QNo.ToString();
                          //Result = CreatePDFReport(Invoice);
                      }



                      ///////////////////////////////////////////////
                      return Json(new { success = true, data = oHeadToDisplay });
                  }
                  else
                  {
                      Dictionary<string, object> oValues1 = new Dictionary<string, object>();
                      oValues1.Add("Fk_BoxEstimation", (object)pId);
                      oQuote.SetValues(oValues1);
                      ModelManuplationResult oResult = new ModelManuplationResult();
                      oResult = oQuote.CreateNew();
                      //CreatePDFReport(oResult.Message.ToString());


                    var  oHeadToDisplay1 = new
                      {

                          Fk_BoxEstimation = decimal.Parse(pId),
                          QuoteNo = oResult.Message
                      };
                      return Json(new { success = true, data = oHeadToDisplay1 });
                  }
                 
              }
            return null;
        }

        [HttpPost]
        public JsonResult QuotationListByFiter(string Pk_Quotation = "", string QuotationDate = "", string Customer = "", string FromDate = "", string ToDate = "", string CommunicationType = "", string State = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_Quotation", Pk_Quotation));
                oSearchParams.Add(new SearchParameter("QuotationDate", QuotationDate));
                //oSearchParams.Add(new SearchParameter("Customer", Customer));
                oSearchParams.Add(new SearchParameter("FromDate", FromDate));
                oSearchParams.Add(new SearchParameter("ToDate", ToDate));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);
                
                List<EntityObject> oEnquirysToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_Quote> oEnquiryObjects = oEnquirysToDisplayObjects.Select(p => p).OfType<Vw_Quote>().ToList();

                //Create a anominious object here to break the circular reference
                var oEnquiryToDisplay = oEnquiryObjects.Select(p => new
                {
                    Pk_Quotation = p.Pk_Quotation,
                    Customer = p .CustomerName,
                    Pk_Enquiry = p.Pk_Enquiry,
                    Quantity=p.Quantity,
                    Pk_BoxID=p.Pk_BoxID,
                    BoxName=p.Name,
                    QuotationDate = p.QuotationDate != null ? DateTime.Parse(p.QuotationDate.ToString()).ToString("dd/MM/yyyy") : "",

                }).ToList();

                return Json(new { Result = "OK", Records = oEnquiryToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }




        public ActionResult QuoteRep(string data = "")
        {
            try
            {


                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["Pk_Quotation"]);

                List<Vw_Quotation> QuotationList = new List<Vw_Quotation>();
                List<Vw_Quotation> EstList = new List<Vw_Quotation>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                //QuotationList = dc.VwInvoices.ToList();
                QuotationList = dc.Vw_Quotation.Where(x => x.Pk_Quotation == sInvno).Select(x => x).OfType<Vw_Quotation>().ToList();

                if (QuotationList.Count > 0)
                {
                    DataTable dt = new DataTable("Quotation");

                    dt.Columns.Add("CustomerName");
                    dt.Columns.Add("CustomerAddress");
                    dt.Columns.Add("CountryName");
                    dt.Columns.Add("StateName");
                    dt.Columns.Add("Name");
                    dt.Columns.Add("Pk_Quotation");
                    dt.Columns.Add("QuotationDate");
                    dt.Columns.Add("Fk_EstimationID");
                    dt.Columns.Add("LandLine");
                    dt.Columns.Add("Quantity");
                    dt.Columns.Add("CostPerBox");
                    dt.Columns.Add("Dimension");
                    dt.Columns.Add("Email");
                    dt.Columns.Add("BName");
                    dt.Columns.Add("BoardBS");
                    dt.Columns.Add("Description");
                    dt.Columns.Add("ContactPerson");
                    dt.Columns.Add("Terms");
                    foreach (Vw_Quotation entity in QuotationList)
                    {
                        DataRow row = dt.NewRow();

                        row["CustomerName"] = entity.CustomerName;
                        row["CustomerAddress"] = entity.CustomerAddress;
                        row["CountryName"] = entity.CountryName;
                        row["StateName"] = entity.StateName;
                        row["Name"] = entity.Name;
                        row["Pk_Quotation"] = entity.Pk_Quotation;
                        row["QuotationDate"] = entity.QuotationDate;
                        row["Fk_EstimationID"] = entity.Fk_EstimationID;
                        row["LandLine"] = entity.LandLine;
                        row["Email"] = entity.Email;
                        row["Quantity"] = entity.Quantity;
                        row["CostPerBox"] = entity.CostPerBox;
                        row["BoardBS"] = entity.BoardBS;
                        row["BName"] = entity.BName;
                        row["Description"] = entity.Description;
                        row["Dimension"] = entity.Dimension;
                        row["ContactPerson"] = entity.ContactPerson;
                        row["Terms"] = entity.Terms;

                        dt.Rows.Add(row);
                    }

                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.Quotation orp = new CogitoStreamline.Report.Quotation();

                    orp.Load("@\\Report\\Quotation.rpt");
                    orp.SetDataSource(dt.DefaultView);



                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "Quote-" + " " + sInvno + ".pdf");

                    FileInfo file = new FileInfo(pdfPath);

                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();


                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }
        public ActionResult QuoteRepSF(string data = "")
        {
            try
            {


                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["Pk_Quotation"]);

                List<Vw_Quotation> QuotationList = new List<Vw_Quotation>();
                List<Vw_Quotation> EstList = new List<Vw_Quotation>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                //QuotationList = dc.VwInvoices.ToList();
                QuotationList = dc.Vw_Quotation.Where(x => x.Pk_Quotation == sInvno).Select(x => x).OfType<Vw_Quotation>().ToList();

                if (QuotationList.Count > 0)
                {
                    DataTable dt = new DataTable("Quotation");

                    dt.Columns.Add("CustomerName");
                    dt.Columns.Add("CustomerAddress");
                    dt.Columns.Add("CountryName");
                    dt.Columns.Add("StateName");
                    dt.Columns.Add("Name");
                    dt.Columns.Add("Pk_Quotation");
                    dt.Columns.Add("QuotationDate");
                    dt.Columns.Add("Fk_EstimationID");
                    dt.Columns.Add("LandLine");
                    dt.Columns.Add("Quantity");
                    dt.Columns.Add("CostPerBox");
                    dt.Columns.Add("Dimension");
                    dt.Columns.Add("Email");
                    dt.Columns.Add("BName");
                    dt.Columns.Add("BoardBS");
                    dt.Columns.Add("Description");
                    dt.Columns.Add("ContactPerson");
                    dt.Columns.Add("Terms");

                    foreach (Vw_Quotation entity in QuotationList)
                    {
                        DataRow row = dt.NewRow();

                        row["CustomerName"] = entity.CustomerName;
                        row["CustomerAddress"] = entity.CustomerAddress;
                        row["CountryName"] = entity.CountryName;
                        row["StateName"] = entity.StateName;
                        row["Name"] = entity.Name;
                        row["Pk_Quotation"] = entity.Pk_Quotation;
                        row["QuotationDate"] = entity.QuotationDate;
                        row["Fk_EstimationID"] = entity.Fk_EstimationID;
                        row["LandLine"] = entity.LandLine;
                        row["Email"] = entity.Email;
                        row["Quantity"] = entity.Quantity;
                        row["CostPerBox"] = entity.CostPerBox;
                        row["BoardBS"] = entity.BoardBS;
                        row["BName"] = entity.BName;
                        row["Description"] = entity.Description;
                        row["Dimension"] = entity.Dimension;
                        row["ContactPerson"] = entity.ContactPerson;
                        row["Terms"] = entity.Terms;

                        dt.Rows.Add(row);
                    }

                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.QuotationSF orp = new CogitoStreamline.Report.QuotationSF();

                    orp.Load("@\\Report\\QuotationSF.rpt");
                    orp.SetDataSource(dt.DefaultView);



                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "SFQuote-" + " " + sInvno + ".pdf");

                    FileInfo file = new FileInfo(pdfPath);

                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();


                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }


        //private Boolean CreatePDFReport(string data = "")
        //{
        //    try
        //    {
        //        //Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        //        int sInvno = Convert.ToInt32(data);
        //        //int sEst = Convert.ToInt32(oValues["Fk_BoxEstimation"]);
        //        List<Vw_Quotation> QuotationList = new List<Vw_Quotation>();
        //        List<Vw_Quotation> EstList = new List<Vw_Quotation>();
        //        CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

        //        //QuotationList = dc.VwInvoices.ToList();
        //        QuotationList = dc.Vw_Quotation.Where(x => x.Pk_Quotation == sInvno).Select(x => x).OfType<Vw_Quotation>().ToList();

        //        if (QuotationList.Count > 0)
        //        {
        //            DataTable dt = new DataTable("Quotation");

        //            dt.Columns.Add("CustomerName");
        //            dt.Columns.Add("CustomerAddress");
        //            dt.Columns.Add("CountryName");
        //            dt.Columns.Add("StateName");
        //            dt.Columns.Add("Name");
        //            dt.Columns.Add("Pk_Quotation");
        //            dt.Columns.Add("QuotationDate");
        //            dt.Columns.Add("Fk_EstimationID");
        //            dt.Columns.Add("LandLine");
        //            dt.Columns.Add("Quantity");
        //            dt.Columns.Add("CostPerBox");
        //            dt.Columns.Add("Dimension");
        //            dt.Columns.Add("Email");
        //            dt.Columns.Add("BName");
        //            dt.Columns.Add("BSValue");
        //            dt.Columns.Add("Description");

        //            foreach (Vw_Quotation entity in QuotationList)
        //            {
        //                DataRow row = dt.NewRow();

        //                row["CustomerName"] = entity.CustomerName;
        //                row["CustomerAddress"] = entity.CustomerAddress;
        //                row["CountryName"] = entity.CountryName;
        //                row["StateName"] = entity.StateName;
        //                row["Name"] = entity.Name;
        //                row["Pk_Quotation"] = entity.Pk_Quotation;
        //                row["QuotationDate"] = entity.QuotationDate;
        //                row["Fk_EstimationID"] = entity.Fk_EstimationID;
        //                row["LandLine"] = entity.LandLine;
        //                row["Email"] = entity.Email;
        //                row["Quantity"] = entity.Quantity;
        //                row["CostPerBox"] = entity.CostPerBox;
        //                row["BSValue"] = entity.BSValue;
        //                row["BName"] = entity.BName;
        //                row["Description"] = entity.Description;
        //                dt.Rows.Add(row);
        //            }

        //            DataSet ds = new DataSet();
        //            ds.Tables.Add(dt);
        //            CogitoStreamline.Report.Quotation orp = new CogitoStreamline.Report.Quotation();

        //            orp.Load("@\\Report\\Quotation.rpt");
        //            orp.SetDataSource(dt.DefaultView);



        //            string pdfPath = Server.MapPath("~/ConvertPDF/" + "Quote-" + " " + sInvno + ".pdf");

        //            FileInfo file = new FileInfo(pdfPath);

        //            if (file.Exists)
        //            {
        //                file.Delete();
        //            }
        //            var pd = new PrintDocument();


        //            orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
        //            orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
        //            DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
        //            objDiskOpt.DiskFileName = pdfPath;

        //            orp.ExportOptions.DestinationOptions = objDiskOpt;
        //            orp.Export();


        //        }

        //        return true;
        //    }


        //    catch (Exception ex)
        //    {
        //        // this.LogError(ex);
        //        return false;
        //        //  Response.Redirect("Index");
        //    }

        //}

        //public ActionResult PDFReport(string data = "")
        //{
        //    try
        //    {
        //        Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        //        int sInvno = Convert.ToInt32(oValues["Pk_QuotationVal"]);
        //        //int sEst = Convert.ToInt32(oValues["Fk_BoxEstimation"]);
        //        List<Vw_Quotation> QuotationList = new List<Vw_Quotation>();
        //        List<Vw_Quotation> EstList = new List<Vw_Quotation>();
        //        CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

        //        EstList = dc.Vw_Quotation.Where(x => x.Pk_Quotation == sInvno).Select(x => x).OfType<Vw_Quotation>().ToList();
        //        if (EstList.Count > 0)
        //        {

        //            string pdfPath = Server.MapPath("~/ConvertPDF/" + "Quote-" + " " + sInvno + ".pdf");

        //            FileInfo file = new FileInfo(pdfPath);

        //            if (file.Exists)
        //            {
        //                var oHeadToDisplay1 = new
        //                {

        //                    //Fk_BoxEstimation = decimal.Parse(pId),
        //                   QuoteNo = sInvno.ToString()
        //                };
        //                return Json(new { success = true, data = oHeadToDisplay1 });
        //            }
        //            else
        //            {
        //                Boolean Result;
        //                String Invoice = sInvno.ToString();
        //                Result = CreatePDFReport(Invoice);
        //            }

        //        }
        //        else
        //        {
        //            Boolean Result;
        //            String Invoice = sInvno.ToString();
        //            Result = CreatePDFReport(Invoice);
        //            ////QuotationList = dc.VwInvoices.ToList();
        //            //QuotationList = dc.Vw_Quotation.Where(x => x.Pk_Quotation == sInvno).Select(x => x).OfType<Vw_Quotation>().ToList();

        //            //if (QuotationList.Count > 0)
        //            //{
        //            //    DataTable dt = new DataTable("Quotation");

        //            //    dt.Columns.Add("CustomerName");
        //            //    dt.Columns.Add("CustomerAddress");
        //            //    dt.Columns.Add("CountryName");
        //            //    dt.Columns.Add("StateName");
        //            //    dt.Columns.Add("Name");
        //            //    dt.Columns.Add("Pk_Quotation");
        //            //    dt.Columns.Add("QuotationDate");
        //            //    dt.Columns.Add("Fk_BoxEst");
        //            //    dt.Columns.Add("LandLine");
        //            //    dt.Columns.Add("Quantity");
        //            //    dt.Columns.Add("Price");
        //            //    dt.Columns.Add("Amount");
        //            //    dt.Columns.Add("Email");
        //            //    dt.Columns.Add("Grandtotal");

        //            //    foreach (Vw_Quotation entity in QuotationList)
        //            //    {
        //            //        DataRow row = dt.NewRow();

        //            //        row["CustomerName"] = entity.CustomerName;
        //            //        row["CustomerAddress"] = entity.CustomerAddress;
        //            //        row["CountryName"] = entity.CountryName;
        //            //        row["StateName"] = entity.StateName;
        //            //        row["Name"] = entity.Name;
        //            //        row["Pk_Quotation"] = entity.Pk_Quotation;
        //            //        row["QuotationDate"] = entity.QuotationDate;
        //            //        row["Fk_BoxEst"] = entity.Fk_BoxEst;
        //            //        row["LandLine"] = entity.LandLine;
        //            //        row["Email"] = entity.Email;
        //            //        row["Quantity"] = entity.Quantity;
        //            //        row["Price"] = entity.Price;
        //            //        row["Amount"] = entity.Amount;
        //            //        row["Grandtotal"] = entity.Grandtotal;
        //            //        dt.Rows.Add(row);
        //            //    }

        //            //    DataSet ds = new DataSet();
        //            //    ds.Tables.Add(dt);
        //            //    CogitoStreamline.Report.Quotation orp = new CogitoStreamline.Report.Quotation();

        //            //    orp.Load("@\\Report\\Quotation.rpt");
        //            //    orp.SetDataSource(dt.DefaultView);



        //            //    string pdfPath = Server.MapPath("~/ConvertPDF/" + "Quote-" + " " + sInvno + ".pdf");

        //            //    FileInfo file = new FileInfo(pdfPath);

        //            //    if (file.Exists)
        //            //    {
        //            //        file.Delete();
        //            //    }
        //            //    var pd = new PrintDocument();


        //            //    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
        //            //    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
        //            //    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
        //            //    objDiskOpt.DiskFileName = pdfPath;

        //            //    orp.ExportOptions.DestinationOptions = objDiskOpt;
        //            //    orp.Export();


        //            //}
        //        }
        //        return null;
        //    }
        

        //    catch (Exception ex)
        //    {
        //        // this.LogError(ex);
        //        return null;
        //        //  Response.Redirect("Index");
        //    }
        //}


        [HttpPost]

        public JsonResult SaveNew([System.Web.Http.FromBody]string data)
        {
            data = data.TrimStart('=');
            Dictionary<string, object> values = JsonConvert.DeserializeObject<Dictionary<string, object>>(data);
            data = JsonConvert.SerializeObject(values);
            return base.Save(data);


        }

    }
}