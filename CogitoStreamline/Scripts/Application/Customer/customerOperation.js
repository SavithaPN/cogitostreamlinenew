﻿
function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Customer List',
        paging: true,
        pageSize: 10,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Customer/CustomerListByFiter',
            //deleteAction: '',
            updateAction: '',
            createAction: ''
        },
        fields: {
            Details: {
                title: '',
                width: '1%',
                sorting: false,
                paging: true,
                pageSize: 5,
                edit: false,
                create: false,
                listClass: 'child-opener-image-column',
                display: function (data) {
                    var $img = $("<i class='icon-phone' title='Contacts'></i>");
                    $img.click(function () {
                        $('#MainSearchContainer').jtable('openChildTable',
                                    $img.closest('tr'),
                                    {
                                        title: 'Contacts List',
                                        paging: true,
                                        pageSize: 10,
                                        actions: {
                                            listAction: '/Customer/CustomerContacts?Pk_Customer=' + data.record.Pk_Customer
                                        },
                                        fields: {
                                            Pk_CustomerContacts: {
                                                key: true,
                                                create: false,
                                                edit: false,
                                                list: false
                                            },
                                            ContactPersonName: {
                                                title: 'Person Name',
                                                key: false
                                            },
                                            ContactPersonDesignation: {
                                                title: 'Designation'

                                            },
                                            ContactPersonNumber: {
                                                title: 'Contact Number',
                                                width: '2%'
                                            },
                                            ContactPersonEmailId: {
                                                title: 'Email',
                                                width: '2%'
                                            }
                                        },
                                        formClosed: function (event, data) {
                                            data.form.validationEngine('hide');
                                            data.form.validationEngine('detach');
                                        }
                                    }, function (data) { //opened handler
                                        data.childTable.jtable('load');
                                    });
                    });
                    return $img;
                }
            }, Pk_Customer: {
                title: 'Id',
                key: true,
                width: '2%'
            },
            CustomerType: {
                title: 'Customer Type',
                width: '5%'
            },
            CustomerName: {
                title: 'Customer Name',
                edit: false
            },
            City: {
                title: 'City',
                width: '2%'
            },
            CustomerContact: {
                title: 'Contact Number'
            },
            LandLine: {
                title: 'Land Line'
            },
            ContactPerson: {
                title: 'Contact Person'
            },
            Email: {
                title: 'Email'
            }
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_Customer: $('#txtSearchPkCustomer').val(),
            CustomerName: $('#txtSearchCustomerName').val(),
            CustomerContact: $('#txtSearchCustomerContact').val(),
            LandLine: $('#txtSearchLandLine').val()
        });
    });

    $('#LoadRecordsButton').click();

    $('#LoadRecordsButton').click();

    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });
  
    $('#txtSearchPkCustomer').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Pk_Customer: $('#txtSearchPkCustomer').val()
            });
        }
    });

    $('#txtSearchCustomerName').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                CustomerName: $('#txtSearchCustomerName').val()
            });
        }
    });

    $('#txtSearchCustomerType').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                CustomerType: $('#txtSearchCustomerType').val()
            });
        }
    });

    $('#txtSearchSCustomerContact').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                CustomerContact: $('#txtSearchCustomerContact').val()
            });
        }
    });
   
    $('#txtSearchLandLine').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                LandLine: $('#txtSearchLandLine').val()
            });
        }
    });
    
    $('#btnCloseLayout').click(function (e) {
        e.preventDefault();
        $("#frmDataDialog").validationEngine('hide');
    });
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("city", "getCity", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("State", "getState", null);
    _page.getViewByName('New').viewModel.addAddtionalDataSources("Country", "getCountry", null);  
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("Country", "getCountry", null);

    //_page.getViewByName('New').viewModel.addAddtionalDataSources("CountryP", "getCountryPop", null);
    //_page.getViewByName('Edit').viewModel.addAddtionalDataSources("CountryP", "getCountry", null);




}
function beforeModelSaveEx() {
    var viewModel = _page.getViewByName('New').viewModel;
    viewModel.data["CustomerType"] = $.trim($('#CustomerType').val());

    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel1.data["CustomerType"] = $.trim($('#CustomerType').val());
}

function afterNewShow(viewObject) {
   
    $("#cmdAddShipping").click(function (e) {
        e.preventDefault();
        $("#frmDataDialog").validationEngine('hide');
    });
    document.getElementById('CustomerType').value = "Gold";
    curViewModel = viewObject.viewModel;

    $("#Fk_Gold").prop("checked", true);

    $('#contacts').wgContact({
        viewModel: viewObject.viewModel,
        property: "Contacts"
    });
    $('#Fk_Country').change(function () {
        if ($('#Fk_Country').val() != '') {
            loadComboState('#Fk_State', $('#Fk_Country').val());
            $('#Fk_State').change();
        }
    });

    $('#Fk_State').change(function () {
        if ($('#Fk_State').val() != '') {
            loadComboCity('#Fk_City', $('#Fk_State').val());
            $('#Fk_City').change();
        }
    });




    //$('#ShippingCountry').change(function () {
    //    if ($('#ShippingCountry').val() != '') {
    //        loadComboStateShipping('#Fk_State', $('#ShippingCountry').val());
    //        $('#ShippingState').change();
    //    }
    //});

    //$('#ShippingState').change(function () {
    //    if ($('#ShippingState').val() != '') {
    //        loadComboCityShipping('#Fk_City', $('#ShippingState').val());
    //        $('#ShippingCity').change();
    //    }
    //});
  
    $('#divShippingDetails').jtable({
        title: 'Shipping Details',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Customer/Bounce',
            updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            ShippingCustomerName: {
                title: 'Name',
                key: false
            },
            ShippingCity: {
                title: 'City',
                width: '20%'
            },
            ShippingState: {
                title: 'State',
                width: '20%'
            },
            ShippingPincode: {
                title: 'Pincode',
                width: '20%'
            },
            ShippingMobile: {
                title: 'Mobile',
                width: '20%'
            },
            ShippingLandLine: {
                title: 'Land line',
                width: '20%'
            },
            ShippingEmailId: {
                title: 'Email',
                width: '20%'

            }
        }
    });


    configureOne2Many("#cmdAddShipping", '#divShippingDetails', "#cmdSaveShippingDetails", viewObject, "ShippingDetails", "Customer", "_AddShippingDetails", function () { return new CustomerShippingDetails() });

   
}

function afterEditShow(viewObject) {
    curViewModel = viewObject.viewModel;
    if ($.trim($('#CustomerType').val()) == "Gold") {
        $("#Fk_Gold").prop("checked", true);
    }
    else if ($.trim($('#CustomerType').val()) == "Silver") {

        $("#Fk_Silver").prop("checked", true);

    }
    else if ($.trim($('#CustomerType').val()) == "Normal") {
        $("#Fk_Normal").prop("checked", true);
    }

    $('#Fk_Country').change(function () {
        if ($('#Fk_Country').val() != '') {
            loadComboState('#Fk_State', $('#Fk_Country').val());
            $('#Fk_State').change();
        }
    });

    $('#Fk_State').change(function () {
        if ($('#Fk_State').val() != '') {
            loadComboCity('#Fk_City', $('#Fk_State').val());
            $('#Fk_City').change();
        }
    });

    $('#divShippingDetails').jtable({
        title: 'Shipping Details',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Customer/Bounce',
            updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            ShippingCustomerName: {
                title: 'Name',
                key: false
            },
            ShippingCity: {
                title: 'City',
                width: '20%'
            },
            ShippingState: {
                title: 'State',
                width: '20%'
            },
            ShippingPincode: {
                title: 'Pincode',
                width: '20%'
            },
            ShippingMobile: {
                title: 'Mobile',
                width: '20%'
            },
            ShippingLandLine: {
                title: 'Land line',
                width: '20%'
            },
            ShippingEmailId: {
                title: 'Email',
                width: '20%'

            }
        }
    });
    var oSCuts = viewObject.viewModel.data.ShippingDet();
    viewObject.viewModel.data["ShippingDetails"] = ko.observableArray();
    var i = 0;

    while (oSCuts[i]) {
        var oCut = new CustomerShippingDetails();
        //Pk_CustomerShippingId, ShippingCustomerName, ShippingAddress, ShippingPincode, ShippingMobile, ShippingLandLine, ShippingEmailId, ShippingCountry, ShippingState, ShippingCity) {
        oCut.load(oSCuts[i].Pk_CustomerShippingId, oSCuts[i].ShippingCustomerName, oSCuts[i].ShippingAddress, oSCuts[i].ShippingPincode, oSCuts[i].ShippingMobile, oSCuts[i].ShippingLandLine, oSCuts[i].ShippingEmailId, oSCuts[i].ShippingCountry, oSCuts[i].ShippingState, oSCuts[i].ShippingCity);
        viewObject.viewModel.data["ShippingDetails"].push(oCut);
        i++;
    }
    configureOne2Many("#cmdAddShipping", '#divShippingDetails', "#cmdSaveShippingDetails", viewObject, "ShippingDetails", "Customer", "_AddShippingDetails", function () { return new CustomerShippingDetails() });

    bEditContext = true;
    $('#contacts').wgContact({
        viewModel: viewObject.viewModel,
        property: "Contacts"
    });
}

function afterOneToManyDialogShow(property) {
    $("#frmHost").validationEngine('hide');
}
function afterModelSaveEx() {
    document.location.reload();
}


function radioClass(str1) {
    if (str1 == "Gold") {
        document.getElementById('CustomerType').value = str1;
    }
    else if (str1 == "Silver") {
        document.getElementById('CustomerType').value = str1;
    }
    else if (str1 == "Normal") {
        document.getElementById('CustomerType').value = str1;
    }
}

function showOnlyGold() {
    $('#MainSearchContainer').jtable('load', {
        CustomerType: "Gold",
    });
}
function showOnlySilver() {
    $('#MainSearchContainer').jtable('load', {
        CustomerType: "Silver",
    });
}
function showOnlyNormal() {
    $('#MainSearchContainer').jtable('load', {
        CustomerType: "Normal",
    });
}
function showAll() {
    $('#MainSearchContainer').jtable('load', {
        CustomerType: "",
    });
}
function CheckCustomerDuplicate() {
    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("CustomerName", $('#CustomerName').val());
    oResult = _comLayer.executeSyncAction("Customer/CustomerDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + "Customer Already added";
}

function ContactDuplicateChecking() {
    if ($("#Pk_Customer").val() != "") {
        _comLayer.parameters.clear();
        var oResult;
        _comLayer.parameters.add("ContactPersonName", $('#ContactPersonName').val());
        _comLayer.parameters.add("Pk_Customer", $("#Pk_Customer").val());
        oResult = _comLayer.executeSyncAction("Customer/ContactDuplicateChecking", _comLayer.parameters);
        if (oResult.TotalRecordCount > 0)
            return "* " + "Customer Contact Already added";
    }
}

function loadComboState(combo, parent) {
    var oResult = getStateComboValues(parent);
    $(combo).empty();

    var option = $("<option value=''>Select</option>");
    $(combo).append(option);

    if (oResult.Success) {
        var i = 0;

        while (oResult.Records[i]) {

            var option = $("<option value='" + oResult.Records[i].Pk_StateID + "'>" + oResult.Records[i].StateName + "</option>");
            $(combo).append(option);
            i++;
        }
    }
}

function getStateComboValues(parent) {
    _comLayer.parameters.clear();
    _comLayer.parameters.add("CountryId", parent);

    var sPath = _comLayer.buildURL("State", null, "StateListByCountry");
    var oResult = _comLayer.executeSyncAction(sPath, _comLayer.parameters);

    var oResultObject = oResult;

    return oResultObject;
}

function loadComboCity(combo, parent) {
    var oResult = getCityComboValues(parent);
    $(combo).empty();

    var option = $("<option value=''>Select</option>");
    $(combo).append(option);

    if (oResult.Success) {
        var i = 0;

        while (oResult.Records[i]) {
            var option = $("<option value='" + oResult.Records[i].Pk_CityID + "'>" + oResult.Records[i].CityName + "</option>");
            $(combo).append(option);
            i++;
        }
    }
}

function getCityComboValues(parent) {
    _comLayer.parameters.clear();
    _comLayer.parameters.add("StateId", parent);

    var sPath = _comLayer.buildURL("City", null, "CityListByState");
    var oResult = _comLayer.executeSyncAction(sPath, _comLayer.parameters);
    var oResultObject = oResult;

    return oResultObject;
}

//////////////////
//function loadComboStateShipping(combo, parent) {
//    var oResult = getStateComboValuesShipping(parent);
//    $(combo).empty();

//    var option = $("<option value=''>Select</option>");
//    $(combo).append(option);

//    if (oResult.Success) {
//        var i = 0;

//        while (oResult.Records[i]) {

//            var option = $("<option value='" + oResult.Records[i].Pk_StateID + "'>" + oResult.Records[i].StateName + "</option>");
//            $(combo).append(option);
//            i++;
//        }
//    }
//}

//function getStateComboValuesShipping(parent) {
//    _comLayer.parameters.clear();
//    _comLayer.parameters.add("CountryId", parent);

//    var sPath = _comLayer.buildURL("State", null, "StateListByCountry");
//    var oResult = _comLayer.executeSyncAction(sPath, _comLayer.parameters);

//    var oResultObject = oResult;

//    return oResultObject;
//}

//function loadComboCityShipping(combo, parent) {
//    var oResult = getCityComboValuesShipping(parent);
//    $(combo).empty();

//    var option = $("<option value=''>Select</option>");
//    $(combo).append(option);

//    if (oResult.Success) {
//        var i = 0;

//        while (oResult.Records[i]) {
//            var option = $("<option value='" + oResult.Records[i].Pk_CityID + "'>" + oResult.Records[i].CityName + "</option>");
//            $(combo).append(option);
//            i++;
//        }
//    }
//}

//function getCityComboValuesShipping(parent) {
//    _comLayer.parameters.clear();
//    _comLayer.parameters.add("StateId", parent);

//    var sPath = _comLayer.buildURL("City", null, "CityListByState");
//    var oResult = _comLayer.executeSyncAction(sPath, _comLayer.parameters);
//    var oResultObject = oResult;

//    return oResultObject;
//}