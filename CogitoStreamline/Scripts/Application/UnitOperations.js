﻿//Name          : Unit Operations ----javascript files
//Description   : Contains the  Unit Operations  definition
//                 1. Jtable Loading and Columns Descriptions 2.Button Click Events  3. CheckUnitDuplicate(duplication checking) 
//Author        : shantha
//Date 	        : 07/08/2015
//Crh Number    : SL0003
//Modifications : 


function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'UnitList',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Unit/UnitListByFiter',
            //deleteAction: '',
            updateAction: '',
            createAction: ''
        },
        fields: {
            Pk_Unit: {
                title: 'Id',
                key: true,
                width:'5%'
                            },
            UnitName: {
                title: 'Name'
            },

            //UnitType: {
            //    title: 'Type'
            //}

        }
    });

    //Re-load records when Staff click 'load records' button.

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_Unit: $('#TxtPkUnit').val(),
            UnitName: $('#TxtUnitName').val(),
            UnitType: $('#TxtUnitType').val()
        });

    });

    $('#TxtPkUnit').keypress(function (e) {
        if (e.keycode==13){
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Pk_Unit: $('#TxtPkUnit').val(),
                UnitName: $('#TxtUnitName').val(),
                UnitType: $('#TxtUnitType').val()
            });
        }
    });

    $('#TxtUnitName').keypress(function (e) {
        if (e.keycode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Pk_Unit: $('#TxtPkUnit').val(),
                UnitName: $('#TxtUnitName').val(),
                UnitType: $('#TxtUnitType').val()
            });
        }
    });

    $('#TxtUnitType').keypress(function (e) {
        if (e.keycode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Pk_Unit: $('#TxtPkUnit').val(),
                UnitName: $('#TxtUnitName').val(),
                UnitType: $('#TxtUnitType').val()
            });
        }
    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });


  

}





function CheckUnitDuplicate() {

    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("UnitName", $('#UnitName').val());
    oResult = _comLayer.executeSyncAction("Unit/UnitDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + "Unit Name Already added";

}


