﻿function DeliveryScheduleItem() {
    this.data = new Object();
    this.data["Pk_OrderChild"] = "";
    this.data["Fk_BoxID"] = "";
    this.data["EnqQty"] = "";
    this.data["Name"] = "";
    this.data["OrdQty"] = "";
    //this.data["DeliveryDate"] = "";
    this.data["Fk_PartID"] = "";
    this.data["txtPart"] = "";

    this.updateValues = function () {       
        //this.data["OrdQty"] = $('#OrdQty').val();
        //this.data["Fk_BoxID"] = $('#txtFk_Box').val();
        //this.data["Name"] = $('#txtFk_Box').val();
        //this.data["EnqQty"] = $('#EnqQty').val();
        ////this.data["Ddate"] = DateTime.Parse($('#dtDeliveryDate').ToString()).ToString("MM/dd/yyyy").val();
        //this.data["Fk_PartID"] = $('#Fk_PartID').val();
        //this.data["txtPart"] = $('#txtPart').val();

        this.data["OrdQty"] = $('#OrdQty').val();
        //this.data["Fk_BoxID"] = $('#txtFk_Box').val();
        this.data["Fk_PartID"] = $('#Fk_PartID').val();
        this.data["Name"] = $('#txtFk_Box').val();
        this.data["EnqQty"] = $('#EnqQty').val();
        this.data["txtPart"] = $('#txtPart').val();
        this.data["Ddate"] = DateTime.Parse($('#dtDeliveryDate').ToString()).ToString("MM/dd/yyyy").val();
    };

    this.load = function (Pk_OrderChild, OrdQty, txtFk_Box, Name, EnqQty, Ddate, Fk_PartID, txtPart) {
        this.data["Pk_OrderChild"] = Pk_OrderChild;
        this.data["Fk_BoxID"] = txtFk_Box;
        this.data["Name"] = Name;
        this.data["OrdQty"] = OrdQty;
        this.data["EnqQty"] = EnqQty;
        this.data["Ddate"] = Ddate;
        this.data["Fk_PartID"] = Fk_PartID;
        this.data["txtPart"] = txtPart;
        //this.data["DeliveryDate"] = DeliveryDate;

    };

}
