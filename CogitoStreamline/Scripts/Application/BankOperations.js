﻿//Name          : Bank Operations ----javascript files
//Description   : Contains the  Bank Operations  definition
//                 1. Jtable Loading and Columns Descriptions 2.Button Click Events  
//Author        : Shantha
//Date 	        : 27/10/2015
//Crh Number    : SL0002
//Modifications : 

function initialCRUDLoad() {
   
    $('#MainSearchContainer').jtable({
        title: 'Bank Accounts List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Bank/BankListByFiter',
            updateAction: '',
            createAction: ''
            
        },
        fields: {
            Pk_BankID: {
                title: 'Id',
                key: true,
            },
            Fk_AccountTypeID: {
                title: 'Account Type'
            },
            AccountName: {
                title: 'AccountName'
            },
            AccountNumber: {
                title: 'AccountNumber'
            },
            BranchName: {
                title: 'BranchName'
            }
            ,
            BankName: {
                title: 'BankName'
            }
        }
    });

    //Re-load records when Staff click 'load records' button.

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_BankID: $('#TxtBankID').val(),
            AccountName: $('#TxtAccName').val(),
            BankName: $('#TxtBankName').val()
        });

    });
    //$('#TxtPkColor').keypress(function (e) {
    //    if (e.KeyCode == 13) {
    //        e.preventDefault();
    //        $('#MainSearchContainer').jtable('load', {
    //            Pk_Color: $('#TxtPkColor').val(),
    //            ColorName: $('#TxtColorName').val()
    //        });
    //    }

    //});

    //$('#TxtColorName').keypress(function (e) {
    //    if (e.KeyCode == 13) {
    //        e.preventDefault();
    //        $('#MainSearchContainer').jtable('load', {
    //            Pk_Color: $('#TxtPkColor').val(),
    //            ColorName: $('#TxtColorName').val()
    //        });
    //    }

    //});

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });

    _page.getViewByName('New').viewModel.addAddtionalDataSources("AccType", "getAccount", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("AccType", "getAccount", null);
}




