﻿var Estimation = new Estimation();
var bLoadFromModel = false;
var prtSequence = -1;
var boxType = 0;
var totalWeight = 0;
var totRate = 0;
var totalPrice = 0;
var PartName;
var RateVal;
var sNewTitle;
$(document).ready(function () {

    $('#cmdPublish').click(function (e) {
        e.preventDefault();
        $("#Basic").removeClass("active");
        $("#Details").addClass("active");
        $("#pivot").pivot(1);
        createFormParts();
    });

    $('#cmdNext').click(function (e) {
        e.preventDefault();
        $("#pivot").pivot(1);
    });

    $(".ckhPart").click(function (e) {
        if (e.currentTarget.checked) {
            var stxtBox = $(e.currentTarget).val();
            Estimation[stxtBox](1);
        }
        else {
            var stxtBox = $(e.currentTarget).val();
            Estimation[stxtBox](0);
        }
    });

    ko.applyBindings(Estimation, document.getElementById("shellSelection"));

    var bstr = _util.getParameterByName("Enquiry");

    if (bstr.length > 58) {
        var bstr1 = bstr.search(",");
        var bstr2 = bstr.substr(0, bstr1);

        var bstr3 = bstr.search("=");

        var bstrs = bstr.substr((bstr3 + 1));
        var bstr4 = bstrs.search(",");
        var bstr5 = bstr.substr((bstr3 + 1), bstr4);

        var EnqNumber = bstr2;
        Estimation.Fk_BoxID = EnqNumber;


        var EnqChild = bstr5;
        Estimation.EnqChild = EnqChild;


        var bstr6 = bstr.search("Fk_BoxEstimation");
        var bstr7 = bstr.substr(bstr6 + 17);
        var bstr8 = bstr7.search(",");
        var bstr9 = bstr7.substr(bstr7, bstr8);

        Estimation.BoxEstimation = bstr9


        var bstrp = bstr.search("Pk_BoxEstimationChild");
        var bstr10 = bstr.substr(bstrp + 21);
        var bstr11 = bstr10.search("=");
        var bstr12 = bstr10.substr(bstr11 + 1);

        Estimation.BoxEstimationChild = bstr12

    }
    else {

        var bstr1 = bstr.search(",");
        var bstrp = bstr.search("EnqChild");
        var bstr2 = bstr.substr(0, bstr1);

        var bstr3 = bstr.search("=");

        var bstrs = bstr.substr((bstr3 + 1));
        var bstr4 = bstrs.search(",");
        var bstr5 = bstr.substr((bstr3 + 1), bstr4);

        var EnqNumber = bstr2;
        Estimation.Fk_BoxID = EnqNumber;


        var EnqChild = bstr5;
        Estimation.EnqChild = EnqChild;


        var bstr6 = bstr.search("BT");
        if (bstr6 == -1) {
            //alert('Hari');
        }
        else (bstr6 != -1)
        {
            var bstr7 = bstr.substr((bstr6 + 3));
            var BT = bstr7;
            Estimation.BT = BT;
        }

    }

    if (Estimation.fetch()) {
        bLoadFromModel = true;
        createFormParts();
        $("#Basic").removeClass("active");
        $("#Details").addClass("active");
        $("#pivot").pivot(1);
    }
});

function createFormParts() {

    $("#prtList").empty();

    //Check Outershell
    addPart($('#chkOuterShell')[0].checked, $('#txtOuterShell').val(), "Outer Shell", true, false, false);

    //Check Plate
    addPart($('#chkPlate')[0].checked, $('#txtPlate').val(), "Plate", false, false, true);

    //Check LengthPartation
    addPart($('#chkLengthPartation')[0].checked, $('#txtLPartation').val(), "Length Partation", false, false, false);

    //Check WidthPartation
    addPart($('#chkWidthPartation')[0].checked, $('#txtWidthPartation').val(), "Width Partation", false, false, false);

    //Check Cap
    addPart($('#chkCap')[0].checked, $('#txtCap').val(), "Cap", false, true);


    addPart($('#chkTop')[0].checked, $('#txtTop').val(), "Top", true, false, false);


    addPart($('#chkBott')[0].checked, $('#txtBott').val(), "Bottom", true, false, false);
    addPart($('#chkSleeve')[0].checked, $('#txtSleeve').val(), "Sleeve", true, false, false);
    //Now add one rate part to this
    addRatePart();

    //Load the existing model if available

}

function addRatePart() {
    var divId = "rateContainer";
    var rowMain = $("<tr></tr>");
    var colMain = $("<td></td>");

    rowMain.append(colMain);
    $("#prtList").append(rowMain);

    //Create a row for column for total weight
    var lblTotalWeight = $("<label>Total wt kgs</label>");
    var txtTotalWeight = $("<input class='input-mini validate[funcCall[validateNumeric]]' data-bind='value: totalWeight' type='text' placeholder='Weight' style='width: 70px' readonly />");
    var colTotalWeight = $("<td valign='bottom'></td>");

    colTotalWeight.append(lblTotalWeight);
    colTotalWeight.append(txtTotalWeight);
    rowMain.append(colTotalWeight);

    //Create a row for column for total rate
    var lblTotalRate = $("<label>Total Cost</label>");
    var txtTotalRate = $("<input class='input-mini validate[funcCall[validateNumeric]]' data-bind='value: totalPrice' type='text' placeholder='Price' style='width: 70px' readonly />");
    var lblTotalCost = $("<label>Tot.Addl.Cost</label>");
    var txtTotalAddRate = $("<input class='input-mini validate[funcCall[validateNumeric]]' data-bind='value: totalAddPrice' type='text' placeholder='Addl.Price' style='width: 70px' readonly />");
    var colTotalRate = $("<td valign='bottom'></td>");

    colTotalRate.append(lblTotalRate);
    colTotalRate.append(txtTotalRate);
    colTotalRate.append(lblTotalCost);
    colTotalRate.append(txtTotalAddRate);

    rowMain.append(colTotalRate);

    var control = $(colMain).slPart({
        viewTag: "FlexEst",
        title: "Addtional Costs",
        id: divId,
        estimationModel: Estimation,
        rate: true,
        rateChanged: rateChanged
    });

    var divId = "save";
    var rowMain = $("<tr id='saveBlock'></tr>");
    var colMain = $("<td colspan='3' align='center'></td>");

    rowMain.append(colMain);
    $("#prtList").append(rowMain);

    //Create a row for column for total weight
    var cmdCreate = $("<button type='submit' class='btn cmdCreate' data-bind='click: save' id='cmdCreate' >Save</button>");

    colMain.append(cmdCreate);
    rowMain.append(colMain);

    ko.applyBindings(Estimation, document.getElementById("saveBlock"));
}

function addPart(bConsider, iValue, sTitle, outherShell, cap, plate, top, bottom) {

    var localModel = null;
    if (bConsider) {

        for (i = 0; i < iValue; i++) {
            //let us sort out the model
            if (bLoadFromModel) {
                prtSequence++;
                localModel = Estimation.parts[prtSequence];
            }
            else {
                localModel = new part();
                Estimation.addParts(localModel);
            }

            var divId = "prtContainer" + i;
            var rowMain = $("<tr></tr>");

            var colMain = $("<td></td>");



            rowMain.append(colMain);

            $("#prtList").append(rowMain);






            //Create a row for column for total weight
            var lblTotalWeight = $("<label>Part wt kgs</label>");
            //var txtTotalWeight = $("<input class='input-mini weight validate[funcCall[validateNumeric]]' data-bind='value: weight' type='text' placeholder='Weight' style='width: 70px' readonly />");
            var txtTotalWeight = $("<input class='input-mini validate[funcCall[validateNumeric]]' data-bind='value: weight' type='text' placeholder='Weight' id='OWeight' style='width: 70px' readonly />");
            var colTotalWeight = $("<td></td>");

            colTotalWeight.append(lblTotalWeight);
            colTotalWeight.append(txtTotalWeight);
            rowMain.append(colTotalWeight);

            //Create a row for column for total rate
            var lblTotalRate = $("<label>Paper Cost</label>");
            //var txtTotalRate = $("<input class='input-mini rate validate[funcCall[validateNumeric]]' data-bind='value: rate' type='text' placeholder='Rate' style='width: 70px' readonly />");
            var txtTotalRate = $("<input class='input-mini validate[funcCall[validateNumeric]]' data-bind='value: rate' type='text' placeholder='Rate' id='ORate' style='width: 70px' readonly />");
            //var txtTotalRate1 = $("<input class='input-mini validate[funcCall[validateNumeric]]' data-bind='value: rate' type='text' placeholder='Rate' id='ORate1' style='width: 70px' readonly />");
            var colTotalRate = $("<td></td>");

            colTotalRate.append(lblTotalRate);
            colTotalRate.append(txtTotalRate);
            //colTotalRate.append(txtTotalRate1);
            rowMain.append(colTotalRate);

             sNewTitle = sTitle + "-" + (i + 1);

            var control = $(colMain).slPart({
                viewTag: "FlexEst",
                title: sNewTitle,
                id: divId,
                outershell: outherShell,
                cap: cap,
                plate: plate,
                top: top,
                bottom: bottom,
                //lenghtPartation:lenghtPartation,
                //sleeve:sleeve,
                model: localModel,
                loadFromModel: bLoadFromModel,
                partChanged: calculatePartChanged,
                layerChanged: calculateLayerChanged
            });

        }
    }

}

function calculatePartChanged(model, outerShell, cap, plate, top, bottom, sleeve, lenghtPartation) {
    //alert(title);
    if (outerShell) {
        PartName = "Outer Shell";
        outerShellCalculation(model)
    }
    //else if (lenghtPartation) {
    //    PartName = "Length Partation";
    //    plateCapCalculation(model); 
    //}
    else if (sleeve) {
        PartName = "Sleeve";
        outerShellCalculation(model)
    }
    else if (cap || plate || top || bottom) {
        plateCapCalculation(model);
    }
    else {
     
        partationCalculation(model);
    }

    rateChanged(Estimation);
}

function calculateLayerChanged(mainModel, model, outerShell, cap, plate, top, bottom, sleeve, title, sNewTitle) {
    alert(sNewTitle);
    if (outerShell) {
        PartName = "Outer Shell";
        outerShellCalculation(mainModel)
    }
    else if(sleeve)
    {
        PartName = "Sleeve";
        outerShellCalculation(mainModel)
    }
    else if (cap || plate || top || bottom) {
        plateCapCalculation(mainModel);
    }
    else {
        partationCalculation(mainModel);
    }

    rateChanged(Estimation);
}
//Outershell
function outerShellCalculation(model) {
    var length = model.length();
    var width = model.width();
    var height = model.height();
 
  

    var noBoards = 1//model.noBoards();
    
    if (PartName == 'Outer Shell') {


            var TLength = (Number(length) * 2 + Number(width) * 2) + Number(50);    /////////=cut length
            var deckel = ((Number(width) + Number(height) + 20) / 10);
            var boardArea = (((TLength * deckel) / 100000));
            model.boardArea(boardArea);
            model.deckle(deckel);


            //Cuttingsize
            var cuttingsize = Math.round(TLength / 10);

            model.cuttingSize(cuttingsize);

            var addl = ((TLength * deckel) / 1000000);
            var i = 0;
            //var TakeUpF = document.getElementById('TakeUpFactor').value;
            var weight = 0;

            while (model.layers[i]) {

                if (i % 2 != 0) {
                    //fluted
                    //alert("Tfactor=" + model.layers[i].tk_factor());
                    var layerWeight = addl * model.layers[i].tk_factor * model.layers[i].gsm();    //////////////////rate is takeup factor value
                    weight = weight + layerWeight;
                    //alert( i + "-"  + layerWeight);
                }
                else {
                    var layerWeight = addl * model.layers[i].gsm();
                    weight = weight + layerWeight;
                    //alert(layerWeight);
                }

                i++;
            }

            weight = (weight / 1000);
            // PName = STitleVal;
          
            var WtVal = model.weight();
            
            if (WtVal > 0)
            {
            }
            else
            {
                model.weight(weight);
            }
          
        }
        else if (PartName == 'Sleeve') {
            var cutLength = (((Number(length) * 2 + Number(width) * 2) + Number(50)) / 10);

            //BoardArea

            //Deckle
            var deckel = Math.round((Number(height * 3) + 20) / 10);
            model.deckle(deckel);

            var boardArea = (Number(cutLength / 100) * Number(deckel / 100));
            model.boardArea(boardArea);

            model.cuttingSize(cutLength);



            var i = 0;

            var weight = 0;
            var flutedParameter = 0;
            var nonFlutedParameter = 0;

            while (model.layers[i]) {

                if (i % 2 != 0) {
                    //fluted
                    var layerWeight = boardArea * model.layers[i].tk_factor() * model.layers[i].gsm();     //////////////////rate is takeup factor value
                    weight = weight + layerWeight;
                }
                else {
                    var layerWeight = boardArea * model.layers[i].gsm();
                    weight = weight + layerWeight;
                }

                i++;
            }

            weight = (weight / 1000);

           // model.weight(weight);
            //model.PName(STitleVal);
            var WtVal = model.weight();

            if (WtVal > 0) {
            }
            else {
                model.weight(weight);
            }


        }



    }
 


//partationcalculation
function partationCalculation(model) {



    var length = model.length();
    var width = model.width();
    var quantity = model.quantity();
    //var tk_factor = model.tk_factor();

    //if (PartName == 'Width Partation')
    //{ var cutLength = Math.round((Number(length * 2) + 20) / 10); }
    //else
    //{
        var cutLength = Math.round((Number(length * 3) + 20) / 10);
    //}
    //BoardArea

    //Deckle
    var deckel = Math.round((Number(width * 3) + 20) / 10);
    model.deckle(deckel);

    var boardArea = (Number(cutLength / 100) * Number(deckel / 100));
    model.boardArea(boardArea);



    //Cuttingsize

    model.cuttingSize(cutLength);

    var addl = ((boardArea * deckel) / 1000000);
    var i = 0;

    var weight = 0;
    var flutedParameter = 0;
    var nonFlutedParameter = 0;

    while (model.layers[i]) {

        if (i % 2 != 0) {
            //fluted
            var layerWeight = addl * model.layers[i].tk_factor() * model.layers[i].gsm();   //////////////////rate is takeup factor value
            flutedParameter = flutedParameter + layerWeight;
        }
        else {
            var layerWeight = addl * model.layers[i].gsm();
            nonFlutedParameter = nonFlutedParameter + layerWeight;
        }

        i++;
    }
    var flutedWeight = cutLength * deckel * flutedParameter * quantity;
    var nonflutedWeight = cutLength * deckel * nonFlutedParameter * quantity;

    weight = ((flutedWeight + nonflutedWeight) / 1000).toFixed(3);


    var WtVal = model.weight();

    if (WtVal > 0) {
    }
    else {
        model.weight(weight);
    }
  
}



//platecalculation
function plateCapCalculation(model) {

    var length = model.length();
    var width = model.width();
    var quantity = model.quantity();
    //var tk_factor = model.tk_factor();
    //var TakeUpF = document.getElementById('TakeUpFactor').value;

    var adjustedLength = (Number(length) + Number(240));
    var adjustedWidth = Number(Number(width) + Number(240));

    var boardArea = (Number(adjustedWidth) * Number(adjustedLength)) / 1000;
    model.boardArea(boardArea);

    //Deckle
    var deckel = Math.round((Number(width) + 20) / 100) * 100;
    model.deckle(deckel);




    //Cuttingsize
    var cuttingSize = adjustedLength + adjustedWidth + 50;
    model.cuttingSize(cuttingSize);


    var addl = ((adjustedLength * deckel) / 1000000);

    var i = 0;

    var weight = 0;
    var flutedParameter = 0;
    var nonFlutedParameter = 0;

    while (model.layers[i]) {

        if (i % 2 != 0) {
            //fluted
            var layerWeight = addl * model.layers[i].tk_factor() * model.layers[i].gsm();     //////////////////rate is takeup factor value
            flutedParameter = flutedParameter + layerWeight;
        }
        else {
            var layerWeight = addl * model.layers[i].gsm();
            nonFlutedParameter = nonFlutedParameter + layerWeight;
        }

        i++;
    }
    var flutedWeight = cuttingSize * deckel * flutedParameter * quantity;
    var nonflutedWeight = cuttingSize * deckel * nonFlutedParameter * quantity;

    weight = ((flutedWeight + nonflutedWeight) / 1000).toFixed(3);
    var WtVal = model.weight();

    if (WtVal > 0) {
    }
    else {
        model.weight(weight);
    }
}



//Final Rate calculation
function rateChanged(estModel) {
    var i = 0;
    var j = 0;
    nrate = 0;
    nweight = 0;
  
    if (Number(totalWeight) == 0 || Number(totalWeight) == 'NaN') {
        totalWeight = 0;
        totRate = 0;
        //while (estModel.parts[i]) {
           
            for (j = 0; j < estModel.parts[i].layers.length; j++)
            {
                

                //alert(j);
                    nrate = Number(estModel.parts[i].layers[j].Rate);
                    alert(nrate);
                    nweight = Number(estModel.parts[i].layers[j].Weight);  
                    alert(nweight);
                    npCost = nrate * nweight;
                    totRate = totRate + npCost;
                
                    document.getElementById('ORate').value = totRate
            
                //i++;
            }
            

        //   
        //}
       
    }

    else {
        //totalWeight = 0;
        //totRate = 0;
        //while (estModel.parts[i]) {
        //    totalWeight = Number(totalWeight) + Number(estModel.parts[i].weight());
        //    totRate = Number(totRate) + Number(estModel.parts[i].rate());
        //   // totRate = Number(totRate) + Number(estModel.rate);
        //    i++;
        //}
    }
    
    //estModel.totalWeight(totalWeight);
    //estModel.totalPrice(totRate);
    //estModel.totRate(totRate);
    //alert(totRate);
    //Now rate calculation
    //Declaring variables

    var convRate = estModel.convRate();
    var convValue = 0;

    var gMarginPercentage = estModel.gMarginPercentage();
    var gMarginValue = 0;

    var Rate = estModel.totRate();
    //var taxesPercntage = estModel.taxesPercntage();

    //var VATPercentage = estModel.VATPercentage();

    //var CSTPercentage = estModel.CSTPercentage();

    //var taxesValue = 0;
    //var VATValue = 0;
    //var CSTValue = 0;
    var transportValue = estModel.transportValue();
    var weightHValue = estModel.weightHValue();
    //var handlingChanrgesValue = estModel.handlingChanrgesValue();
    //var packingChargesValue = estModel.packingChargesValue();

    var rejectionPercentage = estModel.rejectionPercentage();
    var rejectionValue = 0;

    var TransportPercentage = estModel.TransportPercentage();

    var VATValue = estModel.VATValue();
    var CSTValue = estModel.CSTValue();


    //var rejectionValue = 0;
    //var totalPrice = 0;

    ////Calculations
    //convValue = (convRate * totalWeight);



    //rejectionValue = (convValue + totRate) * ((rejectionPercentage) / 100); 
    //gMarginValue = (convValue + totRate + rejectionValue) * ((gMarginPercentage) / 100);
    //taxesValue = (convValue + totRate + rejectionValue + gMarginValue) * (taxesPercntage / 100);

    //VATValue = (convValue + totRate + rejectionValue + gMarginValue + taxesValue) * ((VATPercentage) / 100);
    //CSTValue = (convValue + totRate + rejectionValue + gMarginValue + taxesValue) * ((CSTPercentage) / 100);

    //estModel.convValue(convValue);
    //estModel.gMarginValue(gMarginValue);
    //estModel.taxesValue(taxesValue);
    //estModel.rejectionValue(rejectionValue);
    convValue = document.getElementById('ConvRate').value;
    estModel.convValue(convValue);

    rejectionValue = document.getElementById('Rejection').value;
    estModel.rejectionValue(rejectionValue);

    gMarginValue = document.getElementById('MargV').value;
    estModel.gMarginValue(gMarginValue);

    taxesValue = document.getElementById('Taxes').value;
    estModel.taxesValue(taxesValue);


    VATValue = document.getElementById('VATV').value;
    estModel.VATValue(VATValue);

    var CSTValue = document.getElementById('CSTV').value;
    estModel.CSTValue(CSTValue);

    transportValue = document.getElementById('TransportValue').value;
    weightHValue = document.getElementById('WeightH').value;
    //handlingChanrgesValue = document.getElementById('HCharges').value;
    //packingChargesValue = document.getElementById('PCharges').value;

    if (convValue != 0) {

        totalAddPrice = (Number(convValue) +
                      Number(gMarginValue) +
                      Number(taxesValue) +
                      Number(transportValue) +
                      Number(weightHValue) +
                      Number(VATValue) +
                      Number(CSTValue) +
                      //Number(handlingChanrgesValue) +
                      //Number(packingChargesValue) +
                      Number(rejectionValue) +
                      Number(Rate));

        estModel.totalAddPrice(totalAddPrice);

    }
}






function CalcConvRate() {

    //Now rate calculation
    //Declaring variables

    var convRate = document.getElementById('Conv').value;

    //Calculations
    convValue = Number(convRate) * Number(totalWeight);
    document.getElementById('ConvRate').value = convValue;


    //totalPrice = (Number(convValue) +
    //              Number(gMarginValue) +
    //              Number(taxesValue) +
    //              Number(transportValue) +
    //              Number(weightHValue) +
    //              Number(handlingChanrgesValue) +
    //              Number(packingChargesValue) +
    //              Number(rejectionValue));

    //estModel.totalPrice(totalPrice);

    //alert(totalPrice);
}

function CalcRejValue() {

    //Now rate calculation
    //Declaring variables
    gMarginValue = document.getElementById('MargV').value;
    //estModel.gMarginValue(gMarginValue);


    transportValue = document.getElementById('TransportValue').value;

    convValue = document.getElementById('ConvRate').value;

    var rejectionPercentage = document.getElementById('Rej').value;

    var rejectionValue = (Number(convValue) + Number(totRate)) * (Number(rejectionPercentage) / 100);

    document.getElementById('Rejection').value = rejectionValue;
    weightHValue = document.getElementById('WeightH').value;
    //handlingChanrgesValue = document.getElementById('HCharges').value;
    //packingChargesValue = document.getElementById('PCharges').value;


    totalAddPrice = (Number(convValue) +
                  Number(gMarginValue) +
                  Number(taxesValue) +
                  Number(transportValue) +
                  Number(weightHValue) +
                  //Number(handlingChanrgesValue) +
                  //Number(packingChargesValue) +
                  Number(rejectionValue));

    //estModel.totalPrice(totalPrice);

    //alert(totalPrice);
}



function CalcGMarginValue() {

    //Now rate calculation
    //Declaring variables
    var convValue = document.getElementById('ConvRate').value;
    var rejectionValue = document.getElementById('Rejection').value;

    var gMarginPercentage = document.getElementById('Marg').value;

    var gMarginValue = (Number(convValue) + Number(totRate) + Number(rejectionValue)) * (Number(gMarginPercentage) / 100);
    document.getElementById('MargV').value = gMarginValue;


    transportValue = document.getElementById('TransportValue').value;

    totalAddPrice = (Number(convValue) +
                  Number(gMarginValue) +
                  Number(taxesValue) +
                  Number(transportValue) +
                  Number(weightHValue) +
                  //Number(handlingChanrgesValue) +
                  //Number(packingChargesValue) +
                  Number(rejectionValue));

    //estModel.totalPrice(totalPrice);

    //alert(totalPrice);
}

function CalcEDValue() {

    //Now rate calculation
    //Declaring variables
    var convValue = document.getElementById('ConvRate').value;
    var rejectionValue = document.getElementById('Rejection').value;

    var gMarginValue = document.getElementById('MargV').value;
    var taxesPercntage = document.getElementById('Tax').value;
    var taxesValue = (Number(convValue) + Number(totRate) + Number(rejectionValue) + Number(gMarginValue)) * (Number(taxesPercntage / 100));
    document.getElementById('Taxes').value = taxesValue;

    transportValue = document.getElementById('TransportValue').value;



    totalAddPrice = (Number(convValue) +
                  Number(gMarginValue) +
                  Number(taxesValue) +
                  Number(transportValue) +
                  Number(weightHValue) +
                  //Number(handlingChanrgesValue) +
                  //Number(packingChargesValue) +
                  Number(rejectionValue));

    //estModel.totalPrice(totalPrice);

    //alert(totalPrice);
}

function CalcVATValue() {

    //Now rate calculation
    //Declaring variables
    var convValue = document.getElementById('ConvRate').value;
    var rejectionValue = document.getElementById('Rejection').value;
    var gMarginValue = document.getElementById('MargV').value;
    var taxesValue = document.getElementById('Taxes').value;

    var VATPercentage = document.getElementById('VAT').value;
    var VatValue = (Number(convValue) + Number(totRate) + Number(rejectionValue) + Number(gMarginValue)) * (Number(VATPercentage) / 100);
    document.getElementById('VATV').value = VatValue;





    //totalPrice = (Number(convValue) +
    //              Number(gMarginValue) +
    //              Number(taxesValue) +
    //              Number(transportValue) +
    //              Number(weightHValue) +
    //              Number(handlingChanrgesValue) +
    //              Number(packingChargesValue) +
    //              Number(rejectionValue));

    //estModel.totalPrice(totalPrice);
    rateChanged(Estimation);
    //alert(totalPrice);
}


function CalcCSTValue() {

    //Now rate calculation
    //Declaring variables
    var convValue = document.getElementById('ConvRate').value;
    var rejectionValue = document.getElementById('Rejection').value;
    var gMarginValue = document.getElementById('MargV').value;
    var taxesValue = document.getElementById('Taxes').value;
    var VatValue = document.getElementById('VATV').value;
    TransportValue = document.getElementById('TransportValue').value;
    var weightHValue = document.getElementById('WeightH').value;
    //var handlingChanrgesValue = document.getElementById('HCharges').value;
    //var packingChargesValue = document.getElementById('PCharges').value;


    var CSTPercentage = document.getElementById('CST').value;
    var CstValue = (Number(convValue) + Number(totRate) + Number(rejectionValue) + Number(gMarginValue)) * (Number(CSTPercentage) / 100);
    document.getElementById('CSTV').value = CstValue;





    //totalPrice = (Number(convValue) +
    //              Number(gMarginValue) +
    //              Number(taxesValue) +
    //              Number(TransportValue) +
    //              Number(weightHValue) +
    //              Number(handlingChanrgesValue) +
    //              Number(packingChargesValue) +
    //              Number(rejectionValue));

    rateChanged(Estimation);
    // estModel.totalPrice(totalPrice);

    //alert(totalPrice);
}


function CalcTransValue() {

    //Now rate calculation
    //Declaring variables
    var convValue = document.getElementById('ConvRate').value;
    var rejectionValue = document.getElementById('Rejection').value;
    var gMarginValue = document.getElementById('MargV').value;
    var taxesValue = document.getElementById('Taxes').value;
    var VatValue = document.getElementById('VATV').value;
    TransportValue = document.getElementById('TransportValue').value;
    TransportPercentage = document.getElementById('Transport').value;
    var weightHValue = document.getElementById('WeightH').value;
    //var handlingChanrgesValue = document.getElementById('HCharges').value;
    //var packingChargesValue = document.getElementById('PCharges').value;



    if (TransportValue == null || TransportValue == 0 || TransportValue == '') {
        if (TransportPercentage != null || TransportPercentage != 0) {

            var TransportValue = (Number(convValue) + Number(totRate)) * (Number(TransportPercentage) / 100);
            document.getElementById('TransportValue').value = TransportValue;
        }
    }


    //var CSTPercentage = document.getElementById('CST').value;
    //var CstValue = (Number(convValue) + Number(totRate) + Number(rejectionValue) + Number(gMarginValue) + Number(taxesValue)) * (Number(CSTPercentage) / 100);
    //document.getElementById('CSTV').value = CstValue;






    //totalPrice = (Number(convValue) +
    //              Number(gMarginValue) +
    //              Number(taxesValue) +
    //              Number(TransportValue) +
    //              Number(weightHValue) +
    //              Number(handlingChanrgesValue) +
    //              Number(packingChargesValue) +
    //              Number(rejectionValue));

    rateChanged(Estimation);
    // estModel.totalPrice(totalPrice);

    //alert(totalPrice);
}