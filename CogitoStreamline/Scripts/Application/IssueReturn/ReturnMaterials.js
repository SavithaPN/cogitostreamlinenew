﻿function IssuedMaterial() {
    this.data = new Object();
    this.data["Name"] = "";
    this.data["Fk_MaterialId"] = "";
    this.data["Pk_IssueReturnDetailsId"] = "";
    this.data["ReturnQuantity"] = "";
    this.data["ReturnableQty"] = "";
    this.data["Pk_StockID"] = "";
    this.data["RollNo"] = "";
    this.data["Shade"] = "";
    this.data["Mill"] = "";

    this.updateValues = function () {
        this.data["Name"] = $('#txtFk_Material').val();
        this.data["Pk_StockID"] = $('#Pk_StockID').val();
        this.data["RollNo"] = $('#RollNo').val();
        this.data["Color"] = $('#Shade').val();
        this.data["Mill"] = $('#Mill').val();
    };

    this.load = function (Pk_IssueReturnDetailsId, Pk_StockID, Name, Fk_Material, ReturnQuantity, RollNo, Color,Mill) {
        this.data["Pk_IssueReturnDetailsId"] = Pk_IssueReturnDetailsId;
        this.data["Name"] = Name;
        this.data["Fk_Material"] = Fk_Material;
        this.data["Pk_StockID"] = Pk_StockID;
        this.data["ReturnQuantity"] = ReturnQuantity;
        this.data["RollNo"] = RollNo;
        this.data["Color"] = Color;
        this.data["Mill"] = Mill;



        if (Pk_IssueReturnDetailsId != null && Pk_IssueReturnDetailsId != 0) {
            _comLayer.parameters.clear();
            _comLayer.parameters.add("Pk_MaterialIssueID", $('#Fk_IssueId').val());
            _comLayer.parameters.add("Fk_Material", Fk_Material);

            var sPath = _comLayer.buildURL("IssueReturn", null, "getPendingDetails");
            var oResult = _comLayer.executeSyncAction(sPath, _comLayer.parameters);
            if (oResult.Records != "") {
                this.data["ReturnableQty"] = Number(ReturnQuantity) + Number(oResult.Records[0].Quantity);
            }
            else
                this.data["ReturnableQty"] = Number(ReturnQuantity);
        }
    };
}
