﻿var objShortCut = null;
function initialCRUDLoad() {
    $('#MainSearchContainer').jtable({
        title: 'Stock List',
        paging: true,
        pageSize: 20,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/PaperStockList/PaperStockListByFiter',
            //updateAction: '',
            //createAction: '',
            //deleteAction: ''
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                //StrQuery = StrQuery + row_id + ",";
                RollNo = $(this).data("record").RollNo;
                document.getElementById('txtPkPStock').value = $(this).data("record").Pk_PaperStock;
                document.getElementById('txtRollNo').value = RollNo;
                document.getElementById('txtPKMaterial').value = row_id;
                return true;
            });


        },
        fields: {
            Pk_Material: {
                title: 'Id',
                key: true

            },
            Pk_PaperStock: {
                title: 'PaperStockID',
               

            },
            SName: {
                title: 'Mill',
                width: '5%'
            },
            RollNo: {
                title: 'RollNo',
                width: '5%'
            },
            Fk_JobCardID: {
                title: 'JobCard ID',
                width: '5%'
            },
            MaterialName: {
                title: 'MaterialName',
                width: '25%'
            },
            GSM: {
                title: 'GSM',
                width: '10%'
            },
            BF: {
                title: 'BF',
                width: '10%'
            },
            Deckle: {
                title: 'Deckle',
                width: '10%'
            },
            Color: {
                title: 'Color',
                width: '5%'
            },
            Quantity: {
                title: 'StkQty',
                width: '25%'
            },
            ////////////////////////////////////
            Details: {
                title: 'Tot.Stock',
                width: '5%',
                sorting: false,
                edit: false,
                create: false,
                listClass: 'child-opener-image-column',
                display: function (data) {
                    //Create an image that will be used to open child table
                    // var $img = $('<img class="child-opener-image" src="/images/redSignal.jpg" style="width:25px;height:25px" title="Author" />');
                    var $img = $("<i class='icon-users'></i>");
                    //Open child table when user clicks the image
                    $img.click(function () {
                        $('#MainSearchContainer').jtable('openChildTable',
                                    $img.closest('tr'),
                                    {
                                        title: 'Total Stock',
                                        actions: {
                                            listAction: '/PaperStockList/MatIDStock?Pk_Material=' + data.record.Pk_Material
                                        },
                                        fields: {
                                            Pk_Material: {
                                                title: 'Pk_Material',
                                                list:false
                                            },
                                            Name: {
                                                title: 'Material Name'
                                            },
                                            StkQty: {
                                                title: 'Stock Qty'
                                            },

                                        },
                                        formClosed: function (event, data) {
                                            data.form.validationEngine('hide');
                                            data.form.validationEngine('detach');
                                        }
                                    }, function (data) { //opened handler
                                        data.childTable.jtable('load');
                                    });
                    });
                    //Return image to show on the person row
                    return $img;
                }
            },
            //////////////////////////////////////
        }

    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_Material: $('#txtPKMaterial').val(),
            MaterialName: $('#txtMaterialName').val(),
            GSM: $('#txtGSM').val(),
            BF: $('#txtBF').val(),
            Color: $('#txtColor').val(),
            Deckle: $('#txtDeckel').val(),
            Mill: $('#txtMill').val(),
            RollNo: $('#txtRollNo').val(),
           // RollNoP: $('#txtRollNoPart').val(),
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdPrint').click(function (e) {
        e.preventDefault();
        var fkBranch = "";
        _comLayer.parameters.add("fkBranch", fkBranch);
        _comLayer.executeSyncAction("PaperStockList/ShadeWiseRep", _comLayer.parameters);
        var strval = "ConvertPDF/ShadeWise" + ".pdf";

        window.open(strval, '_blank ', 'width=700,height=250');
        return true;
    });


    $('#cmdPrintMill').click(function (e) {
        e.preventDefault();
        var fkBranch = "";
        _comLayer.parameters.add("fkBranch", fkBranch);
        _comLayer.executeSyncAction("PaperStockList/MillWiseRep", _comLayer.parameters);
        var strval = "ConvertPDF/MillWise" + ".pdf";

        window.open(strval, '_blank ', 'width=700,height=250');
        return true;
    });

    $('#cmdPrintGSM').click(function (e) {
        e.preventDefault();
        var fkBranch = "";
        _comLayer.parameters.add("fkBranch", fkBranch);
        _comLayer.executeSyncAction("PaperStockList/GSMWiseRep", _comLayer.parameters);
        var strval = "ConvertPDF/GsmWise" + ".pdf";

        window.open(strval, '_blank ', 'width=700,height=250');
        return true;
    });
    $('#cmdPrintBF').click(function (e) {
        e.preventDefault();
        var fkBranch = "";
        _comLayer.parameters.add("fkBranch", fkBranch);
        _comLayer.executeSyncAction("PaperStockList/BFWiseRep", _comLayer.parameters);
        var strval = "ConvertPDF/BFWise" + ".pdf";

        window.open(strval, '_blank ', 'width=700,height=250');
        return true;
    });

    $('#cmdPrintDeck').click(function (e) {
        e.preventDefault();
        var fkBranch = "";
        _comLayer.parameters.add("fkBranch", fkBranch);
        _comLayer.executeSyncAction("PaperStockList/DeckelWiseRep", _comLayer.parameters);
        var strval = "ConvertPDF/DeckelWise" + ".pdf";

        window.open(strval, '_blank ', 'width=700,height=250');
        return true;
    });

    
    $('#cmdPrintAll').click(function (e) {
        e.preventDefault();
        var fkBranch = "";
        _comLayer.parameters.add("fkBranch", fkBranch);
        _comLayer.executeSyncAction("PaperStockList/StockRep", _comLayer.parameters);
        var strval = "ConvertPDF/StockList" + ".pdf";

        window.open(strval, '_blank ', 'width=700,height=250');
        return true;
    });
    $('#cmdPrintGYT').click(function (e) {
        e.preventDefault();
        var fkBranch = "";
        _comLayer.parameters.add("fkBranch", fkBranch);
        _comLayer.executeSyncAction("PaperStockList/StockRepGYT", _comLayer.parameters);
        var strval = "ConvertPDF/Stock_GYT" + ".xlsx"

        window.open(strval, '_blank ', 'width=700,height=250');
        return true;
    });
    $('#cmdPrintNAT').click(function (e) {
        e.preventDefault();
        var fkBranch = "";
        _comLayer.parameters.add("fkBranch", fkBranch);
        _comLayer.executeSyncAction("PaperStockList/StockRepNat", _comLayer.parameters);
        var strval = "ConvertPDF/Stock_NAT" + ".xlsx"

        window.open(strval, '_blank ', 'width=700,height=250');
        return true;
    });


    $('#cmdInsert').click(function (e) {
        e.preventDefault();
        var fkBranch = document.getElementById('txtstock').value;
        var MatID = document.getElementById('txtPKMaterial').value;
        
        var RollNo = document.getElementById('txtRollNo').value;
       _comLayer.parameters.add("fkBranch", fkBranch);
        _comLayer.parameters.add("RollNo", RollNo);
        _comLayer.parameters.add("MatID", MatID);

        _comLayer.executeSyncAction("PaperStockList/InsertRow", _comLayer.parameters);
        //var strval = "ConvertPDF/ShadeWise" + ".pdf";
        document.getElementById('txtstock').value = "";
        document.getElementById('txtPkPStock').value = "";
        document.getElementById('txtRollNo').value = "";
        document.getElementById('txtPKMaterial').value = "";
        
        //window.open(strval, '_blank ', 'width=700,height=250');
        //return true;
    });
    


    $('#cmddelete').click(function (e) {
        e.preventDefault();
        var fkBranch = document.getElementById('txtPkPStock').value;
      var  RollNo= document.getElementById('txtRollNo').value ;
        _comLayer.parameters.add("fkBranch", fkBranch);
        _comLayer.parameters.add("RollNo", RollNo);
        
        _comLayer.executeSyncAction("PaperStockList/DeleteRow", _comLayer.parameters);
        //var strval = "ConvertPDF/ShadeWise" + ".pdf";
        document.getElementById('txtstock').value = "";
        document.getElementById('txtPkPStock').value = "";
        document.getElementById('txtRollNo').value = "";
        //window.open(strval, '_blank ', 'width=700,height=250');
        //return true;
    });
    $('#cmdUpdate').click(function (e) {
        e.preventDefault();
        var fkBranch = document.getElementById('txtPkPStock').value;
        var RollNo = document.getElementById('txtRollNo').value;
        var Stock = document.getElementById('txtstock').value;
        var pkmat = document.getElementById('txtPKMaterial').value;
        _comLayer.parameters.add("fkBranch", fkBranch);
        _comLayer.parameters.add("RollNo", RollNo);
        _comLayer.parameters.add("Stock", Stock);
        _comLayer.parameters.add("pkmat", pkmat);
        _comLayer.executeSyncAction("PaperStockList/UpdateRow", _comLayer.parameters);
        document.getElementById('txtstock').value = "";
        document.getElementById('txtPkPStock').value="";
        document.getElementById('txtPKMaterial').value = "";

        //var strval = "ConvertPDF/ShadeWise" + ".pdf";

        //window.open(strval, '_blank ', 'width=700,height=250');
        //return true;
    });


    //$('#cmdPrintSel').click(function (e) {
    //    e.preventDefault();
    //    var fkBranch = "";
    //    var GSM=document.getElementById('txtGSM').value;
    //    var BF = document.getElementById('txtBF').value;
    //    var Deckel = document.getElementById('txtDeckel').value;
    //    var Mill = document.getElementById('txtMill').value;
    //    var Color = document.getElementById('txtColor').value;


    //    _comLayer.parameters.add("GSM", GSM);
    //    _comLayer.parameters.add("BF", BF);
    //    _comLayer.parameters.add("Deckel", Deckel);
    //    _comLayer.parameters.add("Mill", Mill);
    //    _comLayer.parameters.add("Color", Color);


    //    _comLayer.executeSyncAction("PaperStockList/SelectRep", _comLayer.parameters);
    //    var strval = "ConvertPDF/Selected List" + ".pdf";

    //    window.open(strval, '_blank ', 'width=700,height=250');
    //    return true;
    //});



}