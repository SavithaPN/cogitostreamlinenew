﻿$.fn.slWizPart = function (options) {

    this.options = $.extend({
        viewTag: "",
        title: "",
        id: "",
        outershell: false,
        cap: false,
        rate: false,
        model: new part(),
        loadFromModel: false
    }, options);

    this.model = this.options.model;

    _comLayer.parameters.clear();


    $(this).find('.partHeader').text(this.options.title);
    $(this).find('.mainPartContainer').attr("id", this.options.id);

    var that = this;

    this.getModel = function () {
        return this.model;
    }

    if (!this.options.rate) {
        $(this).find('.mainPartContainer').find(".smalltile").click(function () {

            var cntr = $($(this).parent()).parent();

            var ix = $(this).attr("value");

            displayLayers(cntr, ix, that);
        });
    }
    else {
        var cntr = $($(this).parent()[0]);

        ko.applyBindings(that.options.estimationModel, $(this).parent()[0]);

        that = this;

        $(cntr).find('.input-mini').change(function () {
            that.options.rateChanged(that.options.estimationModel);
        });
    }

    //check if are loading from model
    if (this.options.loadFromModel) {
        displayLayers($($(this).parent()[0]), this.model.layers.length, that);
    }

    return this;
}

function copyLayerValues(model, iSelected) {

    var i = 0;

    while (model.layers[i]) {

        var chk = "#chkLayer_" + i;

        if ($(chk).is(':checked')) {
            model.layers[i].gsm(model.layers[iSelected].gsm());
            model.layers[i].bf(model.layers[iSelected].bf());
            model.layers[i].color(model.layers[iSelected].color());
            model.layers[i].rate(model.layers[iSelected].rate());
        }

        i++;
    }
}

function copySelection(model, type) {

    var i = 0;

    while (model.layers[i]) {
        var chk = "#chkLayer_" + i;
        $(chk).prop('checked', false);

        if (type == "Fluted") {
            if ((i % 2) != 0) {
                $(chk).prop('checked', true);
            }
        }
        else if (type == "NonFluted") {
            if ((i % 2) == 0) {
                $(chk).prop('checked', true);
            }
        }
        else {
            $(chk).prop('checked', true);
        }

        i++;
    }

}

function displayLayers(cntr, ix, that) {
    $(cntr).find('.selPly').hide();
    var lar = null;
    var rwHeader = $("<tr><td align='center'><label>Layer</label></td><td align='center'><label>GSM</label></td><td align='center'><label>BF</label></td><td align='center'><label>Color</label></td><td align='center'><label>Rate</label></td></tr>");

    $(cntr).find(".dataContainer").append(rwHeader);

    for (i = 0; i < ix; i++) {
        if (!that.options.loadFromModel) {
            var lar = new layer(0, 0, "N", 0);
            that.model.addLayer(lar);
        }
        else {
            lar = that.model.layers[i];
        }

        var lblLayerName = $("<td align='center'><label class='GSM btn'>Layer - " + (i + 1) + "</label></td>");
        var lblGSm = $("<td align='center'><label class='GSM' data-bind='text: layers[" + i + "].gsm'></label></td>");
        var lblbf = $("<td align='center'><label class='GSM' data-bind='text: layers[" + i + "].bf'></label></td>");
        var lblcolor = $("<td align='center'><label class='GSM' data-bind='text: layers[" + i + "].color'></label></td>");
        var lblrate = $("<td align='center'><label class='GSM' data-bind='text: layers[" + i + "].rate'></label></td>");
        var dv = $("<tr class='selector' layerId ='" + i + "' ></tr>");

        if (i % 2 != 0) {
            lblLayerName.addClass("flt");
        }

        dv.append(lblLayerName);
        dv.append(lblGSm);
        dv.append(lblbf);
        dv.append(lblcolor);
        dv.append(lblrate);
        //var tr = $("<tr></tr>");
        //tr.append(dv);
        $(cntr).find(".dataContainer").append(dv);

    }



    $(cntr).find('.selector').click(function () {
        ko.cleanNode(document.getElementById("estDialog"));

        var iCount = $(this).attr("layerId");
        var model = that.model.layers[iCount];

        ko.applyBindings(model, document.getElementById("estDialog"));

        var layerNumber = Number(iCount) + 1;

        var header = "Details of Layer " + layerNumber;

        $("#lyrHeader").text(header);

        var i = 0;

        $("#dlgCopyOptions").empty();

        while (that.model.layers[i]) {
            var chk = $("<td><label class='checkbox inline'><input type='checkbox' value='" + i + "' id='chkLayer_" + i + "' /><span class='metro-checkbox'>" + (i + 1) + "</span></label><td>");
            $("#dlgCopyOptions").append(chk);
            i++;
        }

        $("#cmdCopyAll").click(function (e) {
            e.preventDefault();
            copySelection(that.model, "");
        });

        $("#cmdCopyFluted").click(function (e) {
            e.preventDefault();
            copySelection(that.model, "Fluted");
        });

        $("#cmdNonFluted").click(function (e) {
            e.preventDefault();
            copySelection(that.model, "NonFluted");
        });

        $("#cmdSaveLayers").click(function (e) {
            e.preventDefault();
            copyLayerValues(that.model, iCount);
            $("#estDialog").modal("hide");
        });


        $("#estDialog").find('.input-mini').change(function () {
            that.options.layerChanged(that.model, model, that.options.outershell, that.options.cap, that.options.plate);
        });

        $("#estDialog").modal("show");
    });

    $(cntr).find('.selElements').show();

    //ko.applyBindings(that.model, $(that).find('.mainPartContainer')[0]);
    //ko.applyBindings(that.model, $(that).parent()[0]);
    ko.applyBindings(that.model, $(that)[0]);

    $(cntr).find('.input-mini').change(function () {
        that.options.partChanged(that.model, that.options.outershell, that.options.cap, that.options.plate);
    });

}


