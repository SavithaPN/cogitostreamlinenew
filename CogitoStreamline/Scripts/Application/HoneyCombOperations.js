﻿//Name          : HoneyComb Operations ----javascript files
//Description   : Contains the  honeycomb Operations  definition
//                 1. Jtable Loading and Columns Descriptions 2.Button Click Events  3. CheckHoneyCombDuplicate(duplication checking) 
//Author        : shantha
//Date 	        : 07/08/2015
//Crh Number    : SL0005
//Modifications : 


function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Honey Comb List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/HoneyComb/HoneycombListByFiter',
            //deleteAction: '',
            updateAction: '',
            createAction: ''
        },
        fields: {
            Pk_Material: {
                title: 'Id',
                key: true,
                width: '5%'
            },
            Name: {
                title: 'Name'
            }
            ,
            Fk_UnitId: {
                title: 'Unit'
            }
            ,

            Fk_Color: {
                title: 'Color'
            }
            ,
            Brand: {
                title: 'Brand'
            }


        }
    });

    //Re-load records when Staff click 'load records' button.

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Name: $('#TxtHCName').val(),
            Color: $('#TxtColor').val(),
            Brand: $('#TxtBrand').val()
        });

    });

    $('#TxtHCName').keypress(function (e) {
        if (e.keycode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Name: $('#TxtHCName').val(),
                Color: $('#TxtColor').val(),
                Brand: $('#TxtBrand').val()
            });
        }
    });


    $('#TxtColor').keypress(function (e) {
        if (e.keycode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Name: $('#TxtHCName').val(),
                Color: $('#TxtColor').val(),
                Brand: $('#TxtBrand').val()
            });
        }
    });


    $('#TxtBrand').keypress(function (e) {
        if (e.keycode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Name: $('#TxtHCName').val(),
                Color: $('#TxtColor').val(),
                Brand: $('#TxtBrand').val()
            });
        }
    });
    //Load all records when page is first shown
    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });




    _page.getViewByName('New').viewModel.addAddtionalDataSources("Unit", "getUnit", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("Unit", "getUnit", null);

    _page.getViewByName('New').viewModel.addAddtionalDataSources("Color", "getColor", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("Color", "getColor", null);

}
function afterNewShow(viewObject) {
    viewObject.viewModel.data["Category"] = "Honey Comb";
}

function CheckHoneyCombDuplicate() {

    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("Name", $('#Name').val());
    oResult = _comLayer.executeSyncAction("HoneyComb/HoneyCombDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + "Honey Comb Name Already added";
}
