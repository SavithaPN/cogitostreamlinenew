﻿
//Name          : Country Operations ----javascript files
//Description   : Contains the  PaperType Operations  definition with following
//                 1. Jtable Loading and Columns Descriptions 2.Button Click Events  3. CheckCountryDuplicate(duplication checking)
//Author        : Nagesh V Rao
//Date 	        : 07/08/2015
//Crh Number    : SL0010
//Modifications : 
var curViewModel = null;
var objShortCut = null;
function initialCRUDLoad() {
    $('#MainSearchContainer').jtable({
        title: 'Payments List',
        paging: true,
        pageSize: 8,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Payment/PaymentListByFiter',
            updateAction: '',
            createAction: '',
            deleteAction: ''
        },
        fields: {
            Pk_PaymtID: {
                title: 'Id',
                key: true,
                list: true,
                width: '5%'     
            },
            Paymt_Date: {
                title: 'Date',
                width: '5%'
            },
            Fk_Invno: {
                title: 'Invoice Number',
                key: true,
                list: true,
                width: '5%'
            },
            BillAmt: {
                title: 'Bill Amt',
                key: true,
                list: true,
                width: '5%'
            },
            PaidAmt: {
                title: 'Paid Amt',
                width: '5%'
            },
            BalanceAmt: {
        title: 'Balance Amt',
        width: '5%'
    }

        }
    });
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_PaymtID: $('#txtPaymentId').val(),
            ChequeNo: $('#txtChequeNo').val(),
            BankName: $('#txtBankName').val(),
            Fk_Invno: $('#txtFk_Invno').val(),
            Paymt_Date: $('#txtPaymentDate').val()
        });
    });
    $('#LoadRecordsButton').click();
    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });

    $('#PaymentDate').datepicker({
        autoclose: true
    });


}
function afterNewShow(viewObject) {

    bEditContext = false;
    curViewModel = viewObject.viewModel;
    var dNow = new Date();
    document.getElementById('PaymentDate').value = (((dNow.getDate()) < 10) ? "0" + (dNow.getDate()) : (dNow.getDate())) + '/' + (((dNow.getMonth() + 1) < 10) ? "0" + (dNow.getMonth() + 1) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();
    $('#PaymentDate').datepicker({ autoclose: true });



    $('#cmdInvoiceSearch').click(function (e) {
        e.preventDefault();
        setUpInvoiceSearch();
        $("#searchInvoiceDialog").modal("show");
    });
}



function beforeOneToManySaveHook(objectContext) {
    var bValidation = true;

    if ($("#Quantity").val() == "") {
        $("#Quantity").validationEngine('showPrompt', 'Enter Quantity', 'error', true)
        bValidation = false;
    }
    else if (!$.isNumeric($("#Quantity").val())) {
        $("#Quantity").validationEngine('showPrompt', 'Enter Numbers', 'error', true)
        bValidation = false;
    }
    if ($("#txtFk_Material").val() == "") {
        $("#txtFk_Material").validationEngine('showPrompt', 'Select Material', 'error', true)
        bValidation = false;
    }

    return bValidation;


}

function afterEditShow(viewObject) {

    curViewModel = viewObject.viewModel;



    $('#PaymentDate').datepicker({
        autoclose: true
    });



}

function beforeOneToManyDataBind(property) {
    if (bEditContext) {
        _util.setDivPosition("divDeliveryStatus", "block");
    }

}

function setUpInvoiceSearch() {
    //Enquiry

    cleanSearchDialog();

    //Adding Search fields
    var txtFieldInvoiceNo = "<input type='text' id='txtdlgInvoiceNo' placeholder='InvoiceNo.' class='input-large search-query'/>&nbsp;&nbsp;";

    $("#dlgSearchFieldsInvoice").append(txtFieldInvoiceNo);


    $('#SearchInvoiceContainer').jtable({
        title: 'Invoice List',
        paging: true,
        pageSize: 8,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/InvBilling/BillingListByFiter',
        },
        fields: {
            Pk_Invoice: {
                title: 'Invoice Id',
                key: true
            },
            InvDate: {
                title: 'Date',
                edit: false
            },
            Invno: {
                title: 'Invoice Number'
            },
            GrandTotal: {
                title: 'GrandTotal'
            }
        }
    });


    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#SearchInvoiceContainer').jtable('load', {
            Pk_Invoice: $('#txtdlgInvoiceNo').val()
        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    $('#cmdInvSearch').click(function (e) {
        e.preventDefault();
        $('#SearchInvoiceContainer').jtable('load', {
            Pk_Invoice: $('#txtdlgInvoiceNo').val()
        });
    });

    $('#cmdInvoiceDone').click(function (e) {

        e.preventDefault();
        var rows = $('#SearchInvoiceContainer').jtable('selectedRows');
        $("#searchInvoiceDialog").modal("hide");
        document.getElementById('Fk_Invno').value = rows[0].keyValue;
        document.getElementById('BillAmt').value = rows[0].data.GrandTotal;


    });
}


function beforeModelSaveEx() {

    var viewModel = _page.getViewByName('New').viewModel;
    viewModel.data["Paymt_Date"] = $('#PaymentDate').val();
    viewModel.data["Fk_Invno"] = $('#Fk_Invno').val();

    viewModel.data["Fk_Indent"] = $('#TxtIndent').val();
    viewModel.data["PaidAmt"] = $('#PaidAmt').val();
    viewModel.data["BalanceAmt"] = $('#BalanceAmt').val();
    viewModel.data["BillAmt"] = $('#BillAmt').val();

    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel1.data["PaymentDate"] = $('#PaymentDate').val();
    viewModel1.data["Fk_Invno"] = $('#Fk_Invno').val();
    viewModel.data["PaidAmt"] = $('#PaidAmt').val();
    viewModel.data["BalanceAmt"] = $('#BalanceAmt').val();
    viewModel.data["BillAmt"] = $('#BillAmt').val();

}


function ReferenceFieldNotInitilized(viewModel) {

    $('#Fk_Invno').wgReferenceField({
        keyProperty: "Fk_Invno",
        displayProperty: "Fk_Invno",
        loadPath: "InvBilling/Load",
        viewModel: viewModel
    });
    $('#txtFk_Material').wgReferenceField({
        keyProperty: "Fk_Material",
        displayProperty: "Name",
        loadPath: "Material/Load",
        viewModel: viewModel
    });


    if (viewModel.data != null) {
        $('#Fk_Invno').wgReferenceField("setData", viewModel.data["Fk_Invno"]);
        //   $('#BillAmt').wgReferenceField("setData", viewModel.data["BillAmt"]);
    }
}

function cleanSearchDialog() {

    //Clean the search dialog of previous bindings and prepare for this selected dialog
    $('#MainSearhContainer').empty();
    $('#MainSearhContainer').append("<div id='SearchContainer'></div>");
    $("#dlgSearchFields").empty();
    $('#cmdSearch').off();
    $('#cmdDone').off();

    $('#dlgSearchFieldsInvoice').empty();

}

function CheckAmt() {

    if (Number($('#BillAmt').val()) < Number($('#PaidAmt').val())) {
        return "* " + "Paid Amount cannot Exceed Bill Amount :" + $('#BillAmt').val();
    }
    else {

    }
}



function BalanceAmt() {
    if (Number($('#BillAmt').val()) > 0 && Number($('#PaidAmt').val()) > 0) {
        var BalAmt = (Number($('#BillAmt').val()) - Number($('#PaidAmt').val()));
        document.getElementById('BalanceAmt').value = BalAmt;

    }
}