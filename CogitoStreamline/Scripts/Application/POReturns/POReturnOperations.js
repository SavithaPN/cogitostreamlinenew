﻿var curViewModel = null;
var curEdit = false;
var VendorID;
var gtot;
var EDVal;
var IndentNo;
var ssum = 0;
var tempamt = 0;
var intval = 0;
var Indent = 0;
//var invqty = null;
function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'PO Return List',
        paging: true,
        pageSize: 10,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/POReturn/PurchaseOrderReturnListByFiter',
            //deleteAction: '',            
            //createAction: '',
            updateAction: ''
        },
        fields: {
            Pk_PoRetID: {
                title: 'Ret ID',
                key: true,
                list: true,
                width: '4%'
            },
            PoRetDate: {
                title: 'Ret. Date',
                width: '4%'
            },

            //Fk_Vendor: {
            //    title: 'Vendor',
            //    width: '10%'
            //},
            Fk_PoNo: {
                title: 'PO No.',
                width: '10%'
            },
            Print: {
                title: 'Print',
                width: '2%',
                //display: function (data) {
                //    return '<button type="button" onclick="alert(' + data.record.Invno + ')">Create PDF</button> ';
                display: function (row) {
                    var button = $("<i class='icon-printer'></i>");
                    $(button).click(function () {

                        Pk_PONO = row.record.Pk_PONo;
                        _comLayer.parameters.add("Pk_PONO", Pk_PONO);
                        _comLayer.executeSyncAction("PurchaseOrder/POrderRep", _comLayer.parameters);
                        var strval = "ConvertPDF/POrder" + Pk_PONO + ".pdf"
                        ////////////////////////////


                        var xhr = new XMLHttpRequest();
                        var urlToFile = "ConvertPDF/POrder" + Pk_PONO + ".pdf"
                        xhr.open('HEAD', urlToFile, false);
                        xhr.send();

                        if (xhr.status == "404") {
                            alert('Data Not Available , File does not Exist');
                            return false;


                        } else {
                            window.open(strval, '_blank ', 'width=700,height=250');
                            return true;
                        }


                        /////////////////////////
                       

                    });
                    return button;
                }
            },
        }
    });

    //Re-load records when Customer click 'load records' button.
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_PONo: $('#txtPkPONo').val(),
            PODate: $('#txtPODate').val()
        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    $('#txtPODate').change(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            PODate: $('#txtPODate').val()
        });
    });
    $('#txtPODate').datepicker({
        autoclose: true
    });

    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });

    $('#dtDate').datepicker({ autoclose: true });


    $('#Pk_PONo').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();

            $('#MainSearchContainer').jtable('load', {
                Pk_PONo: $('#txtPkPONo').val()
            });
        }
    });

    $('#dtDate').change(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            PODate: $('#txtPODate').val()
        });
    });

}


function afterNewShow(viewObject) {
    var dNow = new Date();
    document.getElementById('dtDate').value = (((dNow.getDate()) < 10) ? "0" + dNow.getDate() : dNow.getDate()) + '/' + (((dNow.getMonth() + 1) < 10) ? "0" + (dNow.getMonth() + 1) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();
    curViewModel = viewObject.viewModel;

    $('#dtDate').datepicker({ autoclose: true });

    $('#tlbMaterials').jtable({
        title: 'Item List',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/POReturn/Bounce',
            deleteAction: '',
            updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            Fk_Material: {
                title: 'Id',
                key: false,
                list: false
            },
            Name: {
                title: 'Material Name'

            },
            Quantity: {
                title: 'Quantity',
                width: '1%'

            },

            Rate: {
                title: 'Rate',
                width: '1%'
            },
            Amount: {
                title: 'Amount',
                width: '1%'

            }

        }
    });


    configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "Materials", "PurchaseOrder", "_AddMaterial", function () { return new IssuedMaterial() });

    //$('#FORM_CT3').change(function () {
    //    if (!document.getElementById('NETVALUE').value > 0) {

    //        if (this.checked) {

    //            document.getElementById('ED').value = document.getElementById('GrandTotal').value;
    //            document.getElementById('NETVALUE').value = document.getElementById('ED').value;
    //        }
    //        else
    //            if (!this.checked) {
    //                document.getElementById('ED').value = 0;
    //                document.getElementById('ED').value = Number($('#GrandTotal').val()) + (Number($('#GrandTotal').val() * 0.06));
    //            }
    //    }
    //    else if (document.getElementById('NETVALUE').value > 0) {

    //        //document.getElementById('ED').value = 0;
    //        // document.getElementById('ED').value = document.getElementById('GrandTotal').value;
    //        if (this.checked) {

    //            document.getElementById('ED').value = document.getElementById('GrandTotal').value;
    //            document.getElementById('NETVALUE').value = document.getElementById('ED').value;
    //        }
    //        else
    //            if (!this.checked) {
    //                document.getElementById('ED').value = 0;
    //                document.getElementById('ED').value = Number($('#GrandTotal').val()) + (Number($('#GrandTotal').val() * 0.06));
    //            }
    //        document.getElementById("VAT").checked = false;
    //        document.getElementById("CST").checked = false;
    //        document.getElementById("FORM_H").checked = false;
    //        document.getElementById('NETVALUE').value = 0;

    //        document.getElementById('NETVALUE').value = document.getElementById('ED').value;

    //    }

    //});

    //$('#FORM_H').change(function () {
    //    if (this.checked) {

    //        document.getElementById('NETVALUE').value = document.getElementById('ED').value;
    //    }
    //    else
    //        if (!this.checked) {
    //            document.getElementById('NETVALUE').value = 0;
    //            document.getElementById('NETVALUE').value = Number($('#ED').val()) + (Number($('#ED').val() * 0.055));
    //        }

    //});
    //$('#cmdVendorMSearch').click(function (e) {
    //    e.preventDefault();
    //    setUpVendorSearch(viewObject);
    //    $("#searchDialog").modal("show");
    //    $("#searchDialog").width(600);
    //    $("#searchDialog").height(500);

    //});


    $('#cmdMIndentSearch').click(function (e) {
        e.preventDefault();
        setUpPOSearch();
        $("#searchIndentDialog").modal("show");
        //$("#dataDialog").width(700);
        //$("#dataDialog").height(500);

        $("#searchIndentDialog").css("top", "50px");
        $("#searchIndentDialog").css("left", "350px");
    });
}

function beforeModelSaveEx() {
    var viewModel = _page.getViewByName('New').viewModel;
    viewModel.data["PoRetDate"] = $('#dtDate').val();
    //viewModel.data["Fk_Vendor"] = VendorID;
    viewModel.data["Fk_PoNo"] = $('#Fk_PoNo').val();
    //viewModel.data["TaxType"] = $('#TaxType').val();
    //viewModel.data["NETVALUE"] = $('#NETVALUE').val();
    //viewModel.data["GrandTotal"] = $('#GrandTotal').val();
    //viewModel.data["ED"] = $('#ED').val();

    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel1.data["PoRetDate"] = $('#dtDate').val();
    viewModel1.data["Fk_PoNo"] = $('#Fk_PoNo').val();
    //viewModel1.data["Fk_Vendor"] = VendorID;
    //viewModel1.data["Fk_Indent"] = $('#Fk_Indent').val();
    //viewModel1.data["NETVALUE"] = $('#NETVALUE').val();
    //viewModel1.data["GrandTotal"] = $('#GrandTotal').val();
    //viewModel1.data["ED"] = $('#ED').val();
    //viewModel1.data["TaxType"] = $('#TaxType').val();
    //viewModel1.data["Amount"] = $('#Amount').val();
    //viewModel1.data["Rate"] = $('#Rate').val();

    //viewModel1.data["Fk_Status"] = $('#txtClose').val();






}
function afterEditShow(viewObject) {
    //curEdit = true;
    curViewModel = viewObject.viewModel;
    $('#dtDate').datepicker({ autoclose: true });
    curEdit = true;




    $('#tlbMaterials').jtable({
        title: 'Item List',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/POReturn/Bounce',
            deleteAction: '',
            updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            Fk_Material: {
                title: 'Id',
                key: false,
                list: false
            },
            Name: {
                title: 'Material Name',
                width: '5%'
            },
            Quantity: {
                title: 'Quantity',
                width: '1%'
            },

            Rate: {
                title: 'Rate',
                width: '1%'
            },
            Amount: {
                title: 'Amount',
                width: '1%'

            }
        }
    });

    //if ($.trim($('#TaxType').val()) == "VAT") {
    //    $("#VAT").prop("checked", true);
    //}
    //else if ($.trim($('#TaxType').val()) == "CST") {
    //    $("#CST").prop("checked", true);


    //}

    var oSCuts = viewObject.viewModel.data.MaterialData();
    viewObject.viewModel.data["Materials"] = ko.observableArray();
    var i = 0;
    ssum = 0;
    while (oSCuts[i]) {
        var oCut = new IssuedMaterial();

        oCut.load(oSCuts[i].Pk_PODet, oSCuts[i].Name, oSCuts[i].Fk_Material, oSCuts[i].Quantity, oSCuts[i].Rate, oSCuts[i].Amount);
        viewObject.viewModel.data["Materials"].push(oCut);

        i++;
    }

    configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "Materials", "PurchaseOrder", "_AddMaterial", function () { return new IssuedMaterial() });


    //var POstatus = viewObject.viewModel.data["Fk_Status"]();

    //if (POstatus == "4") {
    //    $("#chkCloseJC").attr('checked', true);
    //}
}

//function setUpVendorSearch() {
//    //Enquiry

//    // cleanSearchDialog();

//    $("#srchssHeader").text("Vendor Search");

//    //Adding Search fields
//    var txtFieldVendorName = "<input type='text' id='txtdlgVendorName' placeholder='Vendor Name' class='input-large search-query'/>&nbsp;&nbsp;";

//    $("#dlgSearchFields").append(txtFieldVendorName);


//    $('#searchDialog').jtable({
//        title: 'Vendor List',
//        paging: true,
//        pageSize: 15,
//        selecting: true, //Enable selecting
//        multiselect: false, //Allow multiple selecting
//        selectingCheckboxes: true,
//        defaultSorting: 'Name ASC',
//        actions: {
//            listAction: '/Vendor/VendorListByFiter',
//        },
//        recordsLoaded: function (event, data) {
//            $('.jtable-data-row').click(function () {
//                var row_id = $(this).attr('data-record-key');
//                $('#cmdDone').click();
//            });
//        },


//        fields: {
//            Pk_Vendor: {
//                title: 'Vendor ID',
//                key: true,
//                width: '2%'
//            },
//            VendorName: {
//                title: 'Vendor Name',
//                edit: false,
//                width: '5%'
//            }
//        }
//    });

//    $('#LoadRecordsButton').click(function (e) {
//        e.preventDefault();
//        $('#searchDialog').jtable('load', {
//            VendorName: $('#txtdlgVendorName').val()
//        });
//    });
//    $('#LoadRecordsButton').click();

//    $('#cmdSearch').click(function (e) {
//        e.preventDefault();
//        $('#searchDialog').jtable('load', {
//            VendorName: $('#txtdlgVendorName').val()
//        });
//    });

//    $('#cmdDone').click(function (e) {
//        e.preventDefault();
//        var rows = $('#searchDialog').jtable('selectedRows');
//        $('#Fk_Vendor').wgReferenceField("setData", rows[0].keyValue);
//        $('#Fk_Vendor').wgReferenceField("setData", rows[0].data.Fk_Vendor);
//        VendorID = rows[0].keyValue;
//        $("#searchDialog").modal("hide");
//    });

//}

function setUpIssueMaterialSearch() {
    Indent = document.getElementById('Fk_PoNo').value;
    $('#MaterialSearchContainer').jtable({
        title: 'Material List',
        paging: true,
        pageSize: 10,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/CheckQuality/POList?Pk_PONo=' + Indent,
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdMaterialDone').click();
            });
        },

        fields: {
            MaterialID: {
                title: 'Material Id',
                key: true
            },
            Mat_Name: {
                title: 'Material Name'
            },
            Quantity: {
                title: "Quantity"
            },
            Rate: {
        title: "Rate"
    },
    Amount: {
            title: "Amount"
    }
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            MaterialName: $('#txtMaterial').val(),
            Pk_MaterialIssueID: $('#Fk_Vendor').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdMaterialSearch').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            MaterialName: $('#txtMaterial').val(),
            Pk_MaterialIssueID: $('#Fk_Vendor').val()
        });
    });

    $('#cmdMaterialDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MaterialSearchContainer').jtable('selectedRows');
        $('#txtFk_Material').wgReferenceField("setData", rows[0].keyValue);
        document.getElementById('Quantity').value = rows[0].data["Quantity"];
        document.getElementById('Rate').value = rows[0].data["Rate"];
        document.getElementById('Amount').value = rows[0].data["Amount"];
        _util.setDivPosition("divCreateMaterial", "block");
        _util.setDivPosition("divSearchMaterial", "none");
        $("#cmdSaveMaterial").focus();
    });



}

function afterOneToManyDialogShow(property) {

    $('#cmdSearchMaterialsdetails').click(function (e) {
        e.preventDefault();
        setUpIssueMaterialSearch();
        _util.setDivPosition("divSearchMaterial", "block");
        _util.setDivPosition("divCreateMaterial", "none");
    });
}

function ReferenceFieldNotInitilized(viewModel) {

    //document.getElementById('ReturnableQty').style.display = "none";

    if (objContextEdit == true) {
        $('#cmdSearchMaterialsdetails').attr('disabled', true);
    }
    else if (objContextEdit == false) {
        $('#cmdSearchMaterialsdetails').attr('disabled', false);
    }
    $('#Fk_Vendor').wgReferenceField({
        keyProperty: "Fk_Vendor",
        displayProperty: "VendorName",
        loadPath: "Vendor/Load",
        viewModel: viewModel
    });
    $('#txtFk_Material').wgReferenceField({
        keyProperty: "Fk_Material",
        displayProperty: "Name",
        loadPath: "Material/Load",
        viewModel: viewModel
    });

    //$('#Fk_Indent').wgReferenceField({
    //    keyProperty: "Fk_Indent",
    //    displayProperty: "Fk_Indent",
    //    loadPath: "MaterialIndent/Load",
    //    viewModel: viewModel
    //});

    if (viewModel.data != null) {
        $('#txtFk_Material').wgReferenceField("setData", viewModel.data["Fk_Material"]);
        $('#Fk_Vendor').wgReferenceField("setData", viewModel.data["Fk_Vendor"]);
        //   $('#Fk_Indent').wgReferenceField("setData", viewModel.data["Fk_Indent"]);
    }
}

function checkDuplicate() {

    if (objContextEdit == false) {
        var i = 0;

        while (curViewModel.data["Materials"]()[i]) {
            if (objContext.data.Fk_Material == curViewModel.data["Materials"]()[i].data.Fk_Material && objContext.data.Fk_SiteId == curViewModel.data["Materials"]()[i].data.Fk_SiteId && $('#Fk_SiteIssueMasterId1').val() == curViewModel.data["Materials"]()[i].data.Fk_SiteIssueMasterId) {

                return "* " + "Item Already added";
            }
            i++;
        }
    }
}

function checkstock() {
    if (objContextEdit == false) {

        if (Number($('#ReturnableQty').val()) < Number($('#ReturnQuantity').val())) {
            return "* " + "Return Quantity Excceded Invoice Quantity. Return Quantity:" + $('#ReturnableQty').val();
        }
        else {

        }
    } else if (objContextEdit == true) {
        if (Number($('#ReturnableQty').val()) < Number($('#ReturnQuantity').val())) {
            return "* " + "Return Quantity Excceded Invoice Quantity. Return Quantity:" + $('#ReturnableQty').val();
        }
        else {
        }
    }
}




function setUpPOSearch() {
    //Branch

    //cleanSearchDialog();

    $("#srchHeader1").text("PO Search");

    //Adding Search fields
    var txtFieldIndentNo = "<input type='text' id='txtdlgIndent' placeholder='PONo' class='input-large search-query'/>&nbsp;&nbsp;";
    //var txtFieldVendor = "<input type='text' id='txtdlgVendor' placeholder='Vendor' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldFromDate = "<input type='text' id='txtdlgFromDate' placeholder='FromDate' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldToDate = "<input type='text' id='txtdlgToDate' placeholder='ToDate' class='input-large search-query'/>&nbsp;&nbsp;";




    $('#IndentSearhContainer').jtable({
        title: 'PO List',
        paging: true,
        pageSize: 7,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/PurchaseOrder/PurchaseOrderListByFiter'
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdIndentDone').click();
            });
        },

        fields: {
            Pk_PONo: {
                title: 'PO Number',
                key: true
            },

            PODate: {
                title: 'PO Date'
            },

            Fk_Vendor: {
                title: 'Vendor Name'
            },
            //VendorID: {
            //    title: 'VendorID'
            //},
            //Branch: {
            //    title: 'Branch'
            //},
            //StateName: {
            //    title: 'Status'
            //}
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#IndentSearhContainer').jtable('load', {
            Pk_PONo: $('#txtdlgIndent').val()

        });
    });

    $('#LoadRecordsButton').click();


    $('#cmdIndentSearch').click(function (e) {
        e.preventDefault();

        $('#IndentSearhContainer').jtable('load', {
            Pk_PONo: $('#txtdlgIndent').val(),
            //Vendor: $('#txtdlgVendor').val(),
            FromIndentDate: $('#txtdlgFromDate').val(),
            ToIndentDate: $('#txtdlgToDate').val()

        });
    });

    $('#cmdIndentDone').click(function (e) {
        e.preventDefault();
        var rows = $('#IndentSearhContainer').jtable('selectedRows');
        $("#searchIndentDialog").modal("hide");
        //$('#txtFk_IndentNumber') = document.getElementById(rows[0].keyValue
        document.getElementById('Fk_PoNo').value = rows[0].keyValue;
        IndentNo = rows[0].keyValue;

        //$('#txtFk_Vendor').wgReferenceField("setData", rows[0].data.VendorID);

    });

}




//function CalcAmt() {
//    //  curViewModel = viewObject.viewModel;
//    if (objContextEdit == false) {
//        if (Number($('#Quantity').val()) > 0 && Number($('#Rate').val()) > 0) {
//            document.getElementById('Amount').value = (Number($('#Quantity').val()) * Number($('#Rate').val()));


//        }

//        else {
//        }
//    } else if (objContextEdit == true) {
//        if (Number($('#Quantity').val()) > 0 && Number($('#Rate').val()) > 0) {
//            document.getElementById('Amount').value = (Number($('#Quantity').val()) * Number($('#Rate').val()));
//        }
//        else {
//        }



//    }
//}

//function radioClass1(intval) {
//    if (intval == 1) {


//        document.getElementById('TaxType').value = "VAT";
//        if (FORM_CT3.checked == true)
//        { document.getElementById('ED').value = Number($('#GrandTotal').val()); }
//        else {
//            document.getElementById('ED').value = Number($('#GrandTotal').val()) + (Number($('#GrandTotal').val() * 0.06));
//        }

//        FORM_CT3.style.visibility = 'visible';
//        FORM_H.style.visibility = 'visible';

//        //    fct3.style.visibility = 'hidden';
//        fct3.style.visibility = 'visible';
//        fh.style.visibility = 'visible';

//        if (Number($('#ED').val()) > 0) {
//            document.getElementById('NETVALUE').value = 0;
//            document.getElementById('NETVALUE').value = Number($('#ED').val()) + (Number($('#ED').val() * 0.055));
//        }

//        else if (Number($('#ED').val()) == 0) {
//            document.getElementById('ED').value = Number($('#GrandTotal').val()) + (Number($('#GrandTotal').val() * 0.06));

//            document.getElementById('NETVALUE').value = Number($('#ED').val()) + Number($('#GrandTotal').val() * 0.055);
//            //  NETVALUE
//        }

//    }
//    else if (intval == 2) {

//        document.getElementById('TaxType').value = "CST";

//        FORM_H.style.visibility = 'hidden';

//        fh.style.visibility = 'hidden';


//        if (Number($('#ED').val()) > 0) {

//            document.getElementById('NETVALUE').value = 0;
//            document.getElementById('NETVALUE').value = Number($('#ED').val()) + (Number($('#ED').val() * 0.02));
//        }

//        else if (Number($('#ED').val()) == 0) {
//            document.getElementById('ED').value = Number($('#GrandTotal').val()) + (Number($('#GrandTotal').val() * 0.06));
//            document.getElementById('NETVALUE').value = Number($('#ED').val()) + Number($('#GrandTotal').val() * 0.02);
//        }

//    }


//}

