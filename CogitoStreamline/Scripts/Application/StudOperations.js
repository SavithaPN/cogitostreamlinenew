﻿var TotAmt = 0;
var Lens1 = 0;
var Lens2 = 0;
var Others = 0;
var AdvAmt = 0;
var VATVal = 0;
var AmtVal = 0;
var Dedn = 0;
var NetDedn = 0;
var FrameVal = 0;
var TotC = 0;
function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'JobCard List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Stud/StudentListByFiter',
            updateAction: '',
            createAction: '',
            //deleteAction: ''
        },
        fields: {
            Ref_Num: {
                title: 'Id',
                key: true,
                list: false
            },
            JobCardNo: {
                title: 'JobCard No'
            },
            Customer: {
                title: 'Name'
            },
            JobDate: {
                title: 'JC Date'
            },


        }
    });

    //Re-load records when Staff click 'load records' button.

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            JobCardNo: $('#TxtJCID').val(),
            CustName: $('#TxtCustName').val(),
            JobDate: $('#TxtFromDate').val()
        });

    });
    $('#TxtJCID').keypress(function (e) {
        if (e.KeyCode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                JobCardNo: $('#TxtJCID').val(),
                CustName: $('#TxtCustName').val(),
                JobDate: $('#TxtFromDate').val()
            });
        }

    });

    $('#TxtCustName').keypress(function (e) {
        if (e.KeyCode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                JobCardNo: $('#TxtJCID').val(),
                CustName: $('#TxtCustName').val(),
                JobDate: $('#TxtFromDate').val()
            });
        }

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });

    $('#dtBillDate').datepicker({ autoclose: true });
    $('#TxtFromDate').datepicker({
        autoclose: true
    });


 
        //$('#fileupload').change(function (e) {
        //    e.preventDefault();
        //    //window.open("PUCAdmission/FileUpload");
        //    var Url = "/stud/Upload";
        //    var obj = new Object();
        //    obj["FileName"] = $('#fileupload').val();
        //    var oReturn = executeSyncAction(Url, JSON.stringify(obj));
        //    //window.open('../DegreeStudents/' + oReturn.data + '.pdf');
        //    //window.close('PrintIdCard?StudentId=' + oReturn.data);
        //});



}



//function GetDet() {
//    var VisionClub;


//    VisionClub = document.getElementById('VCNo').value;
//    _comLayer.parameters.add("VisionClub", VisionClub);

//    oResult = _comLayer.executeSyncAction("Customer/CustDetails", _comLayer.parameters);
//    //document.getElementById('Age').value = 40;
//    document.getElementById('Age').value = Number(oResult.Records[0].Age);
//    document.getElementById('Cust_Name').value = oResult.Records[0].FirstName;
//    document.getElementById('ResAdd').value = oResult.Records[0].ResAdd;
//    document.getElementById('ResCity').value = oResult.Records[0].ResCity;
//    document.getElementById('ResPinCode').value = oResult.Records[0].ResPinCode;
//    document.getElementById('DOB').value = oResult.Records[0].DOB;

//    if (oResult.Records[0].Fk_Gender != null) {

//        if (oResult.Records[0].Fk_Gender == 1)
//        { document.getElementById('Gender').value = "Male"; }

//        else
//            if (oResult.Records[0].Fk_Gender == 2)
//            { document.getElementById('Gender').value = "Female"; }
//            else
//                document.getElementById('Gender').value = "Others";

//    }

//    document.getElementById('Fk_Customer').value = oResult.Records[0].Pk_Customer;
//    document.getElementById('Anniversary').value = oResult.Records[0].Anniversary;
//    document.getElementById('LandLine').value = oResult.Records[0].LandLine;
//    document.getElementById('Mobile').value = oResult.Records[0].Mobile;

//}
//function GetDetails() {
//    var VisionClub;


//    VisionClub = document.getElementById('Fk_Customer').value;
//    _comLayer.parameters.add("Fk_Customer", VisionClub);

//    oResult = _comLayer.executeSyncAction("Customer/CustDetailsPK", _comLayer.parameters);
//    //document.getElementById('Age').value = 40;
//    document.getElementById('VCNo').value = oResult.Records[0].VisionClub;
//    document.getElementById('Age').value = Number(oResult.Records[0].Age);
//    document.getElementById('Cust_Name').value = oResult.Records[0].FirstName;
//    document.getElementById('ResAdd').value = oResult.Records[0].ResAdd;
//    document.getElementById('ResCity').value = oResult.Records[0].ResCity;
//    document.getElementById('ResPinCode').value = oResult.Records[0].ResPinCode;
//    document.getElementById('DOB').value = oResult.Records[0].DOB;

//    if (oResult.Records[0].Fk_Gender != null) {
//        if (oResult.Records[0].Fk_Gender == 1) {
//            document.getElementById('Gender').value = "Male";
//        }
//        else
//            if (oResult.Records[0].Fk_Gender == 2)
//            { document.getElementById('Gender').value = "Female"; }
//            else
//                document.getElementById('Gender').value = "Others";

//    }

//    document.getElementById('Fk_Customer').value = oResult.Records[0].Pk_Customer;
//    document.getElementById('Anniversary').value = oResult.Records[0].Anniversary;
//    document.getElementById('LandLine').value = oResult.Records[0].LandLine;
//    document.getElementById('Mobile').value = oResult.Records[0].Mobile;

//}
//function beforeModelSaveEx() {


//    var viewModel = _page.getViewByName('New').viewModel;
//    viewModel.data["JobDate"] = $('#dtJCDate').val();
//    viewModel.data["DueDate"] = $('#dtDueDate').val();
//    viewModel.data["DueTime"] = $('#DueTime').val();
//    viewModel.data["Fk_Branch"] = $('#txtFk_Branch').val();
//    viewModel.data["Fk_Customer"] = $('#Fk_Customer').val();
//    viewModel.data["TotalCost"] = $('#TotalCost').val();
//    viewModel.data["TotAmt"] = $('#TotAmt').val();
//    viewModel.data["Balance"] = $('#Balance').val();
//    viewModel.data["AdvAmt"] = $('#AdvAmt').val();
//    viewModel.data["Amount"] = $('#TAmount').val();

//    var viewModel1 = _page.getViewByName('Edit').viewModel;
//    viewModel1.data["JobDate"] = $('#dtJCDate').val();
//    viewModel1.data["DueDate"] = $('#dtDueDate').val();
//    viewModel1.data["DueTime"] = $('#DueTime').val();
//    viewModel1.data["Fk_Branch"] = $('#txtFk_Branch').val();
//    viewModel1.data["Fk_Customer"] = $('#Fk_Customer').val();
//    viewModel1.data["TotalCost"] = $('#TotalCost').val();
//    viewModel1.data["TotAmt"] = $('#TotAmt').val();
//    viewModel1.data["Balance"] = $('#Balance').val();
//    viewModel1.data["AdvAmt"] = $('#AdvAmt').val();
//    viewModel1.data["Amount"] = $('#TAmount').val();
//}

//function ReferenceFieldNotInitilized(viewModel) {


//    $('#txtFk_Branch').wgReferenceField({
//        keyProperty: "Fk_Branch",
//        displayProperty: "BranchName",
//        loadPath: "Branch/Load",
//        viewModel: viewModel
//    });


//    if (viewModel.data != null) {

//        $('#txtFk_Branch').wgReferenceField("setData", viewModel.data["Fk_Branch"]);
//    }



//}
function afterNewShow(viewObject) {

    $('#fileupload').click(function (e) {
        e.preventDefault();

        var fkBranch = "";
        _comLayer.parameters.add("fkBranch", $('#FileUpload1').val());
        _comLayer.executeSyncAction("stud/Upload", _comLayer.parameters);

    });

    $('#Stockupload').click(function (e) {
        e.preventDefault();

        var fkBranch = "";
        _comLayer.parameters.add("fkBranch", $('#FileUpload2').val());
        _comLayer.executeSyncAction("stud/StockUpload", _comLayer.parameters);

    });
}
//    var dNow = new Date();
//    document.getElementById('dtJCDate').value = (((dNow.getDate()) < 10) ? "0" + dNow.getDate() : dNow.getDate()) + '/' + (((dNow.getMonth() + 1) < 10) ? "0" + (dNow.getMonth() + 1) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();
//    curViewModel = viewObject.viewModel;
//    var d = new Date();
//    var n = d.toLocaleTimeString();
//    document.getElementById('DueTime').value = n;

//    document.getElementById('dtDueDate').value = (((dNow.getDate()) < 10) ? "0" + dNow.getDate() : dNow.getDate()) + '/' + (((dNow.getMonth() + 1) < 10) ? "0" + (dNow.getMonth() + 1) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();
//    $('#dtBillDate').datepicker({ autoclose: true });


//    $("#dropzone").wgUpload({
//        wgTag: "JCard",
//        upLoadUrl: "/FileUpload",
//        property: "Documents",
//        viewModel: viewObject.viewModel,
//        fileLocation: _comLayer.curServer() + "/uploads/"
//    });



//    $('#dtJCDate').datepicker({ autoclose: true });
//    $('#dtDueDate').datepicker({ autoclose: true });

//    $('#tlbMaterials').jtable({
//        title: 'Item List',
//        paging: true,
//        pageSize: 10,
//        defaultSorting: 'Name ASC',
//        actions: {
//            listAction: '/JCard/Bounce',
//            //deleteAction: '',
//            //updateAction: ''
//        },
//        fields: {
//            slno: {
//                title: 'slno',
//                key: true,
//                list: false
//            },
//            TagNumber: {
//                title: 'Id',
//                key: false,
//                list: true
//            },

//            FrameDetails: {
//                title: 'Frame Details',
//                width: '1%'

//            },

//            FrameCost: {
//                title: 'Price',
//                width: '1%'
//            }

//        }
//    });


//    configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "Items", "JCard", "_AddMaterial", function () { return new IssuedMaterial() });





//    $('#tlbMaterials1').jtable({
//        title: 'Payment List',
//        paging: true,
//        pageSize: 10,
//        defaultSorting: 'Name ASC',
//        actions: {
//            listAction: '/JCard/Bounce',
//            //deleteAction: '',
//            //updateAction: ''
//        },
//        fields: {
//            slno: {
//                title: 'slno',
//                key: true,
//                list: false
//            },
//            BillDate: {
//                title: 'InvDate',
//                key: false,
//                list: true
//            },
//            BillNo: {
//                title: 'InvNo'

//            },
//            Amount: {
//                title: 'Amount',
//                width: '1%'

//            }



//        }
//    });


//    configureOne2Many("#cmdAdv", '#tlbMaterials1', "#cmdSaveAdvance", viewObject, "Adv", "JCard", "_AddAdvance", function () { return new Advance() });


//    $('#cmdCreate').hide();

//    $('#MyTab a').click(function (e) {
//        e.preventDefault();

//        if ($(this).attr("href") == "#tbCustomerDetails") {

//            $('#cmdCreate').hide();
//        }
//        else if
//            ($(this).attr("href") == "#tbSpecification") {
//            $('#cmdCreate').hide();

//        }
//        else if 
//            ($(this).attr("href") == "#tbPayment") {
//            $('#cmdCreate').show();

//        }
//        else if 
//           ($(this).attr("href") == "#tbDocument") {
//            $('#cmdCreate').show();
//        }
//    })




//    $('#cmdNext1').click(function (e) {
//        e.preventDefault();
//        $('#MyTab a[href="#tbSpecification"]').tab('show');
//    })

//    $('#cmdNext2').click(function (e) {
//        e.preventDefault();
//        $('#MyTab a[href="#tbPayment"]').tab('show');
//    })

//    $('#cmdNext').click(function (e) {
//        e.preventDefault();
//        $('#MyTab a[href="#tbDocument"]').tab('show');
//    })





//}
//function afterOneToManyDialogShow(property) {
//    $('#dtBillDate').datepicker({ autoclose: true });

//    $('#cmdSearchBranch').click(function (e) {
//        e.preventDefault();
//        setUpBranchSearch();
//        _util.setDivPosition("divSearchMaterial", "block");
//        _util.setDivPosition("divCreateMaterial", "none");
//    });
//}
//function setUpBranchSearch(viewObject) {
//    //Enquiry

//    //cleanSearchDialog();

//    //Adding Search fields
//    var txtFieldBoxName = "<input type='text' id='txtdlgBoxName' placeholder='Box Name' class='input-large search-query'/>&nbsp;&nbsp;";
//    //var txtFieldPartNo = "<input type='text' id='txtdlgPartNo' placeholder='PartNo' class='input-large search-query'/>";


//    $('#MaterialSearchContainer').jtable({
//        title: 'Branch List',
//        paging: true,
//        pageSize: 8,
//        selecting: true, //Enable selecting
//        multiselect: false, //Allow multiple selecting
//        selectingCheckboxes: true,
//        actions: {
//            listAction: '/Branch/BranchListByFiter'
//        },
//        recordsLoaded: function (event, data) {
//            $('.jtable-data-row').click(function () {
//                var row_id = $(this).attr('data-record-key');
//                $('#cmdBranchDone').click();
//                $("#AmountValue").focus();
//            });
//        },
//        fields: {
//            BranchID: {
//                title: 'Id',
//                key: true,
//            },
//            BranchName: {
//                title: 'Name'
//            }


//        }
//    });


//    $('#LoadRecordsButton').click(function (e) {
//        e.preventDefault();
//        $('#MaterialSearchContainer').jtable('load', {
//            BranchName: $('#txtBranch').val()//,
//            //PartNo: $('#txtdlgPartNo').val()

//        });

//    });
//    //$('#txtdlgEnquiryDate').datepicker({
//    //    autoclose: true
//    //});
//    //Load all records when page is first shown
//    $('#LoadRecordsButton').click();

//    $('#cmdBranchSearch').click(function (e) {
//        e.preventDefault();
//        $('#MaterialSearchContainer').jtable('load', {
//            BranchName: $('#txtBranch').val()//,
//            //PartNo: $('#txtdlgPartNo').val()

//        });
//    });


//    $('#cmdBranchDone').click(function (e) {
//        e.preventDefault();
//        var rows = $('#MaterialSearchContainer').jtable('selectedRows');
//        $('#txtFk_Branch').wgReferenceField("setData", rows[0].keyValue);
//        //document.getElementById('ReturnableQty').value = rows[0].data["Quantity"];
//        _util.setDivPosition("divCreateMaterial", "block");
//        _util.setDivPosition("divSearchMaterial", "none");
//        $("#AmountValue").focus();
//    });

//}

//function Calc() {


//    if (document.getElementById('TotAmt').value != null && document.getElementById('TotAmt').value != "") {
//        TotAmt = document.getElementById('TotAmt').value;
//    }
//    else { TotAmt = 0; }

//    document.getElementById('TotAmt').value = Number(TotAmt);



//    if (document.getElementById('LensCost').value != null && document.getElementById('LensCost').value != "")
//    { Lens1 = document.getElementById('LensCost').value; }
//    else
//    { Lens1 = 0; }

//    document.getElementById('TotAmt').value = +Number(Lens1);

//    if (document.getElementById('Lens2Cost').value != null && document.getElementById('Lens2Cost').value != "")
//    { Lens2 = document.getElementById('Lens2Cost').value; }
//    else
//    { Lens2 = 0; }
//    document.getElementById('TotAmt').value = Number(Lens1) + Number(Lens2);


//    if (document.getElementById('OAmount').value != null && document.getElementById('OAmount').value != "")
//    { Others = document.getElementById('OAmount').value; }
//    else

//    { Others = 0; }

//    document.getElementById('TotAmt').value = Number(Lens1) + Number(Lens2) + Number(Others);


//    if (document.getElementById('TAmount').value != null && document.getElementById('TAmount').value != "")
//    { FrameVal = document.getElementById('TAmount').value; }
//    else

//    { FrameVal = 0; }

//    document.getElementById('TotAmt').value = Number(Lens1) + Number(Lens2) + Number(Others) + Number(FrameVal);


//    if (document.getElementById('AdvAmt').value != null && document.getElementById('AdvAmt').value != "")
//    { AdvAmt = document.getElementById('AdvAmt').value; }
//    else
//    { AdvAmt = 0; }

//    //document.getElementById('TotAmt').value = Number(TotAmt) + Number(Lens1) + Number(Lens2) + Number(Others) + Number(AdvAmt);

//    // TotAmt = 0;

//    document.getElementById('TotAmt').value = Number(Lens1) + Number(Lens2) + Number(Others) + Number(FrameVal);
//    TotAmt = document.getElementById('TotAmt').value;




//    if (document.getElementById('VAT').value != null && document.getElementById('VAT').value != "") {
//        VATVal = document.getElementById('VAT').value;
//        document.getElementById('TotalCost').value = (Number(VATVal) / 100) * Number(TotAmt);
//        AmtVal = document.getElementById('TotalCost').value;

//        //document.getElementById('Amount').value = Number(TotAmt) + Number(AmtVal);
//        document.getElementById('TotalCost').value = Number(TotAmt) + Number(AmtVal);
//    }
//    else {
//        VATVal = 0;
//        document.getElementById('TotalCost').value = TotAmt;
//        AmtVal = document.getElementById('TotalCost').value;
//    }

//    if (document.getElementById('Deduction').value != null && document.getElementById('Deduction').value != "") {
//        Dedn = document.getElementById('Deduction').value;

//        NetDedn = Number(Dedn) + Number(AdvAmt);
//        TotC = document.getElementById('TotalCost').value;
//        document.getElementById('Balance').value = Number(TotC) - Number(NetDedn);
//    }
//    else {
//        NetDedn = Number(AdvAmt);
//        TotC = document.getElementById('TotalCost').value;
//        document.getElementById('Balance').value = Number(TotC) - Number(NetDedn);
//    }



//}

//function afterEditShow(viewObject) {
//    curViewModel = viewObject.viewModel;


//    $("#dropzone").wgUpload({
//        wgTag: "JCard",
//        upLoadUrl: "/FileUpload",
//        property: "Documents",
//        viewModel: viewObject.viewModel,
//        fileLocation: _comLayer.curServer() + "/uploads/"

//    });

//    $('#dtJCDate').datepicker({ autoclose: true });
//    $('#dtDueDate').datepicker({ autoclose: true });

//    $('#tlbMaterials').jtable({
//        title: 'Item List',
//        paging: true,
//        pageSize: 10,
//        defaultSorting: 'Name ASC',
//        actions: {
//            listAction: '/JCard/Bounce',
//            //deleteAction: '',
//            //updateAction: ''
//        },
//        fields: {
//            slno: {
//                title: 'slno',
//                key: true,
//                list: false
//            },
//            TagNumber: {
//                title: 'Id',
//                key: false,
//                list: true
//            },

//            FrameDetails: {
//                title: 'Frame Details',
//                width: '1%'

//            },

//            FrameCost: {
//                title: 'Price',
//                width: '1%'
//            }

//        }
//    });

//    var oSCuts = viewObject.viewModel.data.FrameDet();
//    viewObject.viewModel.data["Items"] = ko.observableArray();
//    var i = 0;
//    ssum = 0;
//    while (oSCuts[i]) {
//        var oCut = new IssuedMaterial();
//        //RefNum1, TagNumber, FrameDetails, FrameCost
//        oCut.load(oSCuts[i].RefNum1, oSCuts[i].TagNumber, oSCuts[i].FrameDetails, oSCuts[i].FrameCost);
//        viewObject.viewModel.data["Items"].push(oCut);

//        i++;
//    }

//    configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "Items", "JCard", "_AddMaterial", function () { return new IssuedMaterial() });

//    $('#tlbMaterials1').jtable({
//        title: 'Payment List',
//        paging: true,
//        pageSize: 10,
//        defaultSorting: 'Name ASC',
//        actions: {
//            listAction: '/JCard/Bounce',
//            //deleteAction: '',
//            //updateAction: ''
//        },
//        fields: {
//            slno: {
//                title: 'slno',
//                key: true,
//                list: false
//            },
//            BillDate: {
//                title: 'InvDate',
//                key: false,
//                list: true
//            },
//            BillNo: {
//                title: 'InvNo'

//            },
//            Amount: {
//                title: 'Amount',
//                width: '1%'

//            }



//        }
//    });
//    var oSCuts = viewObject.viewModel.data.Payment();
//    viewObject.viewModel.data["Adv"] = ko.observableArray();
//    var i = 0;
//    ssum = 0;
//    while (oSCuts[i]) {
//        var oCut = new Advance();
//        //RefNum1, BillDate, BillNo, Amount, 
//        oCut.load(oSCuts[i].RefNum1, oSCuts[i].BillDate, oSCuts[i].BillNo, oSCuts[i].Amount, oSCuts[i].Fk_Branch);
//        viewObject.viewModel.data["Adv"].push(oCut);

//        i++;
//    }

//    configureOne2Many("#cmdAdv", '#tlbMaterials1', "#cmdSaveAdvance", viewObject, "Adv", "JCard", "_AddAdvance", function () { return new Advance() });


//    $('#cmdCreate').hide();

//    $('#MyTab a').click(function (e) {
//        e.preventDefault();

//        if ($(this).attr("href") == "#tbCustomerDetails") {

//            $('#cmdCreate').hide();
//        }
//        else if 
//            ($(this).attr("href") == "#tbSpecification") {
//            $('#cmdCreate').hide();

//        }
//        else if
//            ($(this).attr("href") == "#tbPayment") {
//            $('#cmdCreate').show();

//        }
//        else if
//           ($(this).attr("href") == "#tbDocument") {
//            $('#cmdCreate').show();
//        }
//    })




//    $('#cmdNext1').click(function (e) {
//        e.preventDefault();
//        $('#MyTab a[href="#tbSpecification"]').tab('show');
//    })

//    $('#cmdNext2').click(function (e) {
//        e.preventDefault();
//        $('#MyTab a[href="#tbPayment"]').tab('show');
//    })

//    $('#cmdNext').click(function (e) {
//        e.preventDefault();
//        $('#MyTab a[href="#tbDocument"]').tab('show');
//    })

//    GetDetails();


//}
