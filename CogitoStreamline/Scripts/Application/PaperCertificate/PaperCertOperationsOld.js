﻿var curViewModel = null;
var curEdit = false;
var objShortCut = null;
var ChID ;
var CharID;
var CharName;

var CharcID;
var CName ;
var TestMethod;
var Target;
var Acceptance ;
var Result1;
var Result2 ;
var Result3;
var Result4;
var Result5 ;
var Result6;
var MinVal;
var MaxVal;
var AvgVal ;
var Remarks;







function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Box Name List',
        paging: true,
        pageSize: 8,
        actions: {
            listAction: 'PaperCertificate/PCertListByFiter',
            updateAction: '',
            createAction: '',
            //deleteAction: ''

        },
        fields: {
            Pk_Id: {
                title: 'Pk_Id',
                key: true,
                list: true
            },
            Name: {
                title: 'Mat.Name',
                width: '25%'
            },
            Invno: {
                title: 'Invno',
                width: '25%'
            },
            InvDate: {
                title: 'InvDate',
                width: '25%',
                list: false
            },
            VendorName: {
                title: 'VName',
                width: '25%'
            },    
       
        },
    });
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_Id: $('#txtCust').val(),
            Name: $('#txtName').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');


    });
    //$('#cmdpdf').click(function (e) {
    //    e.preventDefault();
    //    fkbox = document.getElementById('txtBoxid').value;
    //    _comLayer.parameters.add("fkbox", fkbox);
    //    _comLayer.executeSyncAction("BoxMaster/BoxDist", _comLayer.parameters);
    //    var strval = "ConvertPDF/Box" + fkbox + ".pdf"
    //    window.open(strval, '_blank ', 'width=700,height=250');

    //});


    _page.getViewByName('New').viewModel.addAddtionalDataSources("Characteristics", "getChar", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("Characteristics", "getChar", null);

    //_page.getViewByName('New').viewModel.addAddtionalDataSources("FluteType", "getFluteType", null);
    //_page.getViewByName('Edit').viewModel.addAddtionalDataSources("FluteType", "getFluteType", null);

    //setUpVendorSearch();
}

function afterModelSaveEx(result) {
    //e.preventDefault();
    //alert(result.Message);
    if (result.Message != null) {
        //   fkbox1 = document.getElementById('hidPkBox').value;

        var bstr = result.Message;

        //var strlen=length(
        var bstr1 = bstr.search("-");
        var bstr2 = bstr.substr(0, bstr1);
        fkbox1 = bstr2;

        var bstr3 = bstr.search(".");
        var bstr4 = bstr.substr((bstr1 + 1))
        // fkbox1 = result.Message;
    }
    else {
        fkbox1 = document.getElementById('hidPkBox').value;

    }
    //window.open("/FlexEst?Enquiry=" + BoxId + ", EnqChild," + oResult1.data.EnqChild + ', Fk_BoxEstimation,' + oResult1.data.BoxEstimation + ', Pk_BoxEstimationChild,' + oResult1.data.EstBoxChild);

    var BType = document.getElementById('Fk_BoxType').value;

    var FType = document.getElementById('Fk_FluteType').value;

    window.open("/BoxSpec?BoxMaster=" + fkbox1 + ", Type=" + BType + ", TakeUp~" + bstr4 + "; FType=" + FType);



};

function afterOneToManyDialogShow() {
    $('#cmdVendorSearch').click(function (e) {
        e.preventDefault();
        setUpVendorSearch(viewObject);
        $("#searchDialog").modal("show");
        $("#searchDialog").width(600);
        $("#searchDialog").height(500);

    });
    //$("#cmdMaterialSearch").click(function (e) {
    //    e.preventDefault();
    //    setUpMaterialSearch();
    //    _util.setDivPosition("divSearchMaterial", "block");
    //    _util.setDivPosition("divCreateMaterial", "none");
    //});
    $('#cmdMaterialMSearch').click(function (e) {
        e.preventDefault();
        setUpMaterialSearch();
        $("#searchIndentDialog").modal("show");
        //$("#dataDialog").width(700);
        //$("#dataDialog").height(500);

        $("#searchIndentDialog").css("top", "100px");
        $("#searchIndentDialog").css("left", "350px");
    });
}


function setUpMaterialSearch() {
    //Branch

    cleanSearchDialog();

    $("#srchHeader1").text("Paper Search");

    //Adding Search fields
    var txtFieldMaterialName = "<input type='text' id='txtdlgMaterialName' placeholder='Paper Name' class='input-large search-query'/>&nbsp;&nbsp;";

    $("#dlgMaterialSearchFields").append(txtFieldMaterialName);

    $('#IndentSearhContainer').jtable({
        title: 'Material List',
        paging: true,
        pageSize: 8,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/Paper/PaperListByFiter'
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdIndentDone').click();
            });
        },

        fields: {
            Pk_Material: {
                title: 'Material Id',
                key: true
            },
            Name: {
                title: 'Material Name',
                edit: false
            },

            GSM: {
                title: 'GSM',
                edit: false
            },
            BF: {
                title: 'BF',
                edit: false

            },
            //Deckle: {
            //    title: 'Deckle',
            //    edit: false
            //},


        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#IndentSearhContainer').jtable('load', {
            Name: $('#txtdlgMaterialName').val()
        });
    });

    $('#LoadRecordsButton').click();

    $('#cmdIndentSearch').click(function (e) {
        e.preventDefault();
        $('#IndentSearhContainer').jtable('load', {
            Name: $('#txtMaterial').val()
        });
    });



    $('#cmdIndentDone').click(function (e) {
        e.preventDefault();
        var rows = $('#IndentSearhContainer').jtable('selectedRows');

      

        $("#searchIndentDialog").modal("hide");
        $('#txtFk_Material').wgReferenceField("setData", rows[0].keyValue);

        
        $("#searchIndentDialog").modal("hide");
        //$("#dataDialog").width(700);
        //$("#dataDialog").height(500);

        //$("#GSM").val(rows[0].data.GSM);
        //$("#BF").val(rows[0].data.BF);
        //$("#Deckel").val(rows[0].data.Deckle);

        //_util.setDivPosition("divCreateMaterial", "block");
        //_util.setDivPosition("divSearchMaterial", "none");

    });
}



function resetOneToManyForm(property) {

    $("#txtFk_Material").val("");

}


function AddData() {

    var rows = "";
     CharcID = CharID;
     CName = CharName;
     TestMethod = document.getElementById("TestMethod").value;
     Target = document.getElementById("Target").value;
     Acceptance = document.getElementById("Acceptance").value;
     Result1 = document.getElementById("Result1").value;
     Result2 = document.getElementById("Result2").value;
     Result3 = document.getElementById("Result3").value;
     Result4 = document.getElementById("Result4").value;
     Result5 = document.getElementById("Result5").value;
     Result6 = document.getElementById("Result6").value;
     MinVal = document.getElementById("MinVal").value;
     MaxVal = document.getElementById("MaxVal").value;
     AvgVal = document.getElementById("AvgVal").value;
     Remarks = document.getElementById("Remarks").value;


    rows += "<tr><td>" + CharcID + "</td><td>" + CName + "</td><td>" + TestMethod + "</td><td>" + Target + "</td><td>" + Acceptance + "</td><td>" + Result1 + "</td><td>" + Result2 + "</td><td>" + Result3 + "</td><td>" + Result4 + "</td><td>" + Result5 + "</td><td>" + Result6 + "</td><td>" + MinVal + "</td><td>" + MaxVal + "</td><td>" + AvgVal + "</td><td>" + Remarks + "</td>"
    //  rows += "<tr><td>" + name + "</td><td>" + gender + "</td><td>" + age + "</td><td>" + city + "</td></tr>";
    $(rows).appendTo("#list tbody");

    //var TestMethod = document.getElementById("TestMethod").value;
    //var Target = document.getElementById("Target").value;
    //var Acceptance = document.getElementById("Acceptance").value;
    //var Result1 = document.getElementById("Result1").value;
    //var Result2 = document.getElementById("Result2").value;
    //var Result3 = document.getElementById("Result3").value;
    //var Result4 = document.getElementById("Result4").value;
    //var Result5 = document.getElementById("Result5").value;
    //var Result6 = document.getElementById("Result6").value;
    //var MinVal = document.getElementById("MinVal").value;
    //var MaxVal = document.getElementById("MaxVal").value;
    //var AvgVal = document.getElementById("AvgVal").value;
    //var Remarks = document.getElementById("Remarks").value;


    document.getElementById("TestMethod").value = "";
    document.getElementById("Target").value = "";
    document.getElementById("Acceptance").value = "";
    document.getElementById("Result1").value = "";
    document.getElementById("Result2").value = "";
    document.getElementById("Result3").value = "";
    document.getElementById("Result4").value = "";
    document.getElementById("Result5").value = "";
    document.getElementById("Result6").value = "";
    document.getElementById("MinVal").value = "";
    document.getElementById("MaxVal").value = "";
    document.getElementById("AvgVal").value = "";
    document.getElementById("Remarks").value = "";

    //FunCalc();
}

function afterNewShow(viewObject) {


    $('#dtDate').datepicker({ autoclose: true });
    //var dNow = new Date();
    //curViewModel = viewObject.viewModel;
    //bEditContext = false;
    //$('#tlbMaterials').jtable({
    //    title: 'Papers',
    //    paging: true,
    //    pageSize: 10,
    //    defaultSorting: 'Name ASC',
    //    actions: {
    //        listAction: '/BoxMaster/Bounce',
    //        //updateAction: ''
    //    },
    //    fields: {
    //        slno: {
    //            title: 'slno',
    //            key: true,
    //            list: false
    //        },
    //        Fk_Material: {
    //            title: 'Material',
    //            list: false
    //        },
    //        Name: {
    //            title: 'Paper Name'

    //        },

    //        GSM: {
    //            title: 'GSM',
    //            edit: false
    //        },
    //        BF: {
    //            title: 'BF',
    //            edit: false

    //        },
    //    }
    //});

    //$('#cmdCustomerSearch').click(function (e) {
    //    e.preventDefault();
    //    $("#searchCustomerDialog").modal("show");
    //});
    $('#cmdVendorSearch').click(function (e) {
        e.preventDefault();
        setUpVendorSearch(viewObject);
        $("#searchDialog").modal("show");
        $("#searchDialog").width(600);
        $("#searchDialog").height(500);

    });

    $('#cmdMaterialMSearch').click(function (e) {
        e.preventDefault();
        setUpMaterialSearch();
        $("#searchIndentDialog").modal("show");
        //$("#dataDialog").width(700);
        //$("#dataDialog").height(500);

        $("#searchIndentDialog").css("top", "100px");
        $("#searchIndentDialog").css("left", "350px");
    });

    //configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "BoxDetails", "BoxMaster", "_AddMaterial", function () { return new MaterialIndent() });
    $('#Fk_Characteristics').change(function () {
        ChID = document.getElementById("Fk_Characteristics");


        CharID = ChID.options[ChID.selectedIndex].value;
        CharName = ChID.options[ChID.selectedIndex].text;

        //alert("CharName" + CharName);
        //alert("CharID" + CharID);
    });

}

function setUpVendorSearch() {
    //Enquiry

    // cleanSearchDialog();

    $("#srchssHeader").text("Vendor Search");

    //Adding Search fields
    var txtFieldVendorName = "<input type='text' id='txtdlgVendorName' placeholder='Vendor Name' class='input-large search-query'/>&nbsp;&nbsp;";

    $("#dlgSearchFields").append(txtFieldVendorName);


    $('#searchDialog').jtable({
        title: 'Vendor List',
        paging: true,
        pageSize: 15,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Vendor/VendorListByFiter',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdDone').click();
            });
        },


        fields: {
            Pk_Vendor: {
                title: 'Vendor ID',
                key: true,
                width: '2%'
            },
            VendorName: {
                title: 'Vendor Name',
                edit: false,
                width: '5%'
            }
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#searchDialog').jtable('load', {
            VendorName: $('#txtdlgVendorName').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdSearch').click(function (e) {
        e.preventDefault();
        $('#searchDialog').jtable('load', {
            VendorName: $('#txtdlgVendorName').val()
        });
    });

    $('#cmdDone').click(function (e) {
        e.preventDefault();
        var rows = $('#searchDialog').jtable('selectedRows');
        $('#Fk_Vendor').wgReferenceField("setData", rows[0].keyValue);
        $('#Fk_Vendor').wgReferenceField("setData", rows[0].data.Fk_Vendor);
        VendorID = rows[0].keyValue;
        $("#searchDialog").modal("hide");
    });

}



//function setUpCustomerSearch() {
//    //Enquiry

//    //cleanSearchDialog();

//    //Adding Search fields
//    var txtFieldCustomerName = "<input type='hidden' id='txtdlgCustomerName' placeholder='Customer Id' class='input-large search-query'/>&nbsp;&nbsp;";
//    var txtFieldCustomerNam = "<input type='text' id='txtdlgCustomerNam' placeholder='Customer Name' class='input-large search-query'/>&nbsp;&nbsp;";
//    var txtFieldCustomerNumber = "<input type='hidden' id='txtdlgCustomerContact' placeholder='Contact number' class='input-large search-query'/>&nbsp;&nbsp;";
//    $("#dlgSearchFields").append(txtFieldCustomerName);
//    $("#dlgSearchFields").append(txtFieldCustomerNam);
//    $("#dlgSearchFields").append(txtFieldCustomerNumber);

//    $('#SearchContainer').jtable({
//        title: 'Customer List',
//        paging: true,
//        pageSize: 8,
//        selecting: true, //Enable selecting
//        multiselect: false, //Allow multiple selecting
//        selectingCheckboxes: true,
//        defaultSorting: 'Name ASC',
//        actions: {
//            listAction: '/Customer/CustomerListByFiter',
//        },

//        recordsLoaded: function (event, data) {
//            $('.jtable-data-row').click(function () {
//                var row_id = $(this).attr('data-record-key');
//                $('#cmdCustDone').click();
//            });
//        },

//        fields: {
//            Pk_Customer: {
//                title: 'CustomerID',
//                key: true,
//                list: false
//            },
//            CustomerName: {
//                title: 'Customer Name',
//                edit: false
//            },
//            //CustomerAddress: {
//            //    title: 'Address'
//            //},
//            City: {
//                title: 'City'
//            }
//            //CustomerContact: {
//            //    title: 'Office Contact'
//            //},
//            //Email: {
//            //    title: 'Email'
//            //}
//        }
//    });


//    $('#LoadRecordsButton').click(function (e) {
//        e.preventDefault();
//        $('#SearchContainer').jtable('load', {
//            Pk_Customer: $('#txtdlgCustomerName').val(),
//            CustomerName: $('#txtdlgCustomerNam').val(),
//            CustomerContact: $('#txtdlgCustomerContact').val()
//        });

//    });

//    //Load all records when page is first shown
//    $('#LoadRecordsButton').click();

//    $('#cmdCustSearch').click(function (e) {
//        e.preventDefault();
//        $('#SearchContainer').jtable('load', {
//            Pk_Customer: $('#txtdlgCustomerName').val(),
//            CustomerName: $('#txtdlgCustomerNam').val(),
//            CustomerContact: $('#txtdlgCustomerContact').val()
//        });
//    });

//    $('#cmdCustDone').click(function (e) {
//        e.preventDefault();
//        var rows = $('#SearchContainer').jtable('selectedRows');
//        if (rows.length > 0)
//        { $('#Customer').wgReferenceField("setData", rows[0].keyValue); }
//        $("#searchCustomerDialog").modal("hide");
//    });

//}
function afterEditShow(viewObject) {
    curViewModel = viewObject.viewModel;
    $('#tlbMaterials').jtable({
        title: 'Papers',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/BoxMaster/Bounce',
            //updateAction: ''
            deleteAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },

            Fk_Material: {
                title: 'Material',
                list: false
            },
            Name: {
                title: 'Paper Name'

            },
            GSM: {
                title: 'GSM',
                edit: false
            },
            BF: {
                title: 'BF',
                edit: false

            },
        }
    });


    var oSCuts = viewObject.viewModel.data.BoxDetails();
    viewObject.viewModel.data["BoxDetails"] = ko.observableArray();
    var i = 0;

    while (oSCuts[i]) {
        var oCut = new MaterialIndent();
        oCut.load(oSCuts[i].Pk_BoxCID, oSCuts[i].Name, oSCuts[i].Fk_Material, oSCuts[i].txtFk_Material, oSCuts[i].GSM, oSCuts[i].BF);
        viewObject.viewModel.data["BoxDetails"].push(oCut);
        i++;
    }

    //  configureOne2Many("#cmdAddMaterial", '#divMaterialDetail', "#cmdMaterialIndent", viewObject, "IndentDetails", "MaterialIndent", "_AddMaterialIndent", function () { return new MaterialIndent() });
    //configureOne2Many("#cmdAddMaterial", '#divMaterialDetail', "#cmdMaterialIndent", viewObject, "BoxDetails", "BoxMaster", "NOTUSED", function () { return new MaterialIndent() }, "divCreateMaterialIndent");
    configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "BoxDetails", "BoxMaster", "_AddMaterial", function () { return new MaterialIndent() });

    bEditContext = true;

    $('#cmdBoxSpecs').click(function (e) {
        e.preventDefault();
        //window.open("/Estimation?Enquiry=" + $('#hidPkEnquiry').val());
        fkbox = $('#hidPkBox').val()
        _comLayer.parameters.add("fkbox", fkbox);
        _comLayer.executeSyncAction("BoxSpec/RecvFk", _comLayer.parameters);
        window.open("/BoxSpec?BoxMaster=" + $('#hidPkBox').val() + "&Ply=" + $('#Ply').val());


    });
}
//function ReferenceFieldNotInitilized(viewModel) {

//    $('#txtFk_Material').wgReferenceField({
//        keyProperty: "Fk_Material",
//        displayProperty: "Name",
//        loadPath: "Material/Load",
//        viewModel: viewModel
//    });

//    $('#Customer').wgReferenceField({
//        keyProperty: "Customer",
//        displayProperty: "CustomerName",
//        loadPath: "Customer/Load",
//        viewModel: viewModel
//    });


//    if (viewModel.data != null) {
//        //$('#txtFk_Material').wgReferenceField("setData", viewModel.data["Fk_Material"]);
//        $('#txtFk_Material').wgReferenceField("setData", viewModel.data["Fk_Material"]);

//        $('#Customer').wgReferenceField("setData", viewModel.data["Fk_Indent"]);
//    }
//}

function CheckHeadDuplicate() {
    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("Name", $('#txtName').val());
    oResult = _comLayer.executeSyncAction("BoxMaster/NameDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + "Box Name Already added";
}

//function beforeOneToManySaveHook(objectContext) {
//    var bValidation = true;


//    if ($("#txtMaterial").val() == "") {
//        $("#txtMaterial").validationEngine('showPrompt', 'Select Paper', 'error', true)
//        bValidation = false;
//    }

//    return bValidation;
//    //curViewModel = viewObject.viewModel;

//}

function cleanSearchDialog() {

    //Clean the search dialog of previous bindings and prepare for this selected dialog
    $('#MainSearhContainer').empty();
    $('#MainSearhContainer').append("<div id='SearchContainer'></div>");
    $("#dlgSearchFields").empty();
    $('#cmdSearch').off();
    $('#cmdDone').off();


}


function ReferenceFieldNotInitilized(viewModel) {

    //document.getElementById('ReturnableQty').style.display = "none";

    if (objContextEdit == true) {
        $('#cmdSearchMaterialsdetails').attr('disabled', true);
    }
    else if (objContextEdit == false) {
        $('#cmdSearchMaterialsdetails').attr('disabled', false);
    }
    $('#Fk_Vendor').wgReferenceField({
        keyProperty: "Fk_Vendor",
        displayProperty: "VendorName",
        loadPath: "Vendor/Load",
        viewModel: viewModel
    });
    $('#txtFk_Material').wgReferenceField({
        keyProperty: "Fk_Material",
        displayProperty: "Name",
        loadPath: "Material/Load",
        viewModel: viewModel
    });

    //$('#Fk_Indent').wgReferenceField({
    //    keyProperty: "Fk_Indent",
    //    displayProperty: "Fk_Indent",
    //    loadPath: "MaterialIndent/Load",
    //    viewModel: viewModel
    //});

    if (viewModel.data != null) {
        $('#txtFk_Material').wgReferenceField("setData", viewModel.data["Fk_Material"]);
        $('#Fk_Vendor').wgReferenceField("setData", viewModel.data["Fk_Vendor"]);
        //   $('#Fk_Indent').wgReferenceField("setData", viewModel.data["Fk_Indent"]);
    }
}

function checkDuplicate() {
    //curViewModel = viewObject.viewModel;
    if (objContextEdit == false) {
        var i = 0;

        while (curViewModel.data["BoxDetails"]()[i]) {
            if (objContext.data.Fk_Material == curViewModel.data["BoxDetails"]()[i].data.Fk_Material) {

                return "* " + "Item Already added";
            }
            i++;
        }
    }
}

function beforeModelSaveEx()
{

    //var postData = [{ "Fk_Characteristics": CharcID, "TestMethod": TestMethod, "Target": Target, "Acceptance": Acceptance, "Result1": Result1, "Result2": Result2, "Result3": Result3, "Result4": Result4, "Result5": Result5, "Result6": Result6, "MinVal": MinVal, "MaxVal": MaxVal, "AvgVal": AvgVal, "Remarks": Remarks }];
    //console.log(postData);

    //$.ajax({
    //    url: '@Url.Action("AddChartoDetails", "PaperCertificate")',
    //    contentType: "application/json",
    //    async: true,
    //    data: JSON.stringify(postData),
    //    dataType: "json",
    //    type: "POST"
    //});

    //var html_table_data = "";
    //var bRowStarted = true;
    //$('#list tbody>tr').each(function () {
    //    $('td', this).each(function () {
    //        if (html_table_data.length == 0 || bRowStarted == true) {
    //            html_table_data += $(this).text();
    //            bRowStarted = false;
    //        }
    //        else
    //            html_table_data += " | " + $(this).text();
    //    });
    //    html_table_data += "\n";
    //    bRowStarted = true;
    //});   working fine


    var html_table_data = "";
    var bRowStarted = true;
    var TableData = new Array();
    $('#list tbody>tr').each(function (row,tr) {
        $('td', this).each(function () {
            if (html_table_data.length == 0 || bRowStarted == true) {
                html_table_data += $(this).text();
                bRowStarted = false;
            }
            else
                html_table_data += " | " + $(this).text();
           // TableData.aEntry = new Array();
            TableData[row] = {
             "CharcID": $(tr).find('td:eq(0)').text()
            , "CName": $(tr).find('td:eq(1)').text()
            , "TestMethod": $(tr).find('td:eq(2)').text()
            , "Target": $(tr).find('td:eq(3)').text()
            , "Acceptance": $(tr).find('td:eq(4)').text()
            , "Result1": $(tr).find('td:eq(5)').text()
            , "Result2": $(tr).find('td:eq(6)').text()
            , "Result3": $(tr).find('td:eq(7)').text()
            , "Result4": $(tr).find('td:eq(8)').text()
            , "Result5": $(tr).find('td:eq(9)').text()
            , "Result6": $(tr).find('td:eq(10)').text()
            , "MinVal": $(tr).find('td:eq(11)').text()
            , "MaxVal": $(tr).find('td:eq(12)').text()
            , "AvgVal": $(tr).find('td:eq(13)').text()
            , "Remarks": $(tr).find('td:eq(14)').text()
                }
        });
        //TableData.shift(); ////first row will be empty - so remove

        html_table_data += "\n";
        bRowStarted = true;
    });
  alert(html_table_data);
    

  $.ajax({
      type: "POST",
      async:true,
      url: "/PaperCertificate/Save",
      data: JSON.stringify(TableData),
      //data: '{"parms":"' + + '" , "attList":"' + TableData + '"}',
      dataType: "json",
      traditional: true,
      contentType: "application/json; charset=utf-8",
      success: function () { alert("Mapping Successful") },
      failure: function () { alert("not working..."); }
  });
 
    //alert(TableData);
}