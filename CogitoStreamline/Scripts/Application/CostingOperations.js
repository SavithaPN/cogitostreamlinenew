﻿/// <reference path="CostingOperations.js" />
var curViewModel = null;
var curEdit = false;
var objShortCut = null;
var fkbox = 0;
var BName;
var TopID = 0;
var TopName = 0;
var PPly = 0;
var fkbox1 = 0;




function initialCRUDLoad() {


    //var bstr1 = TopID.search(",");
    //fkbox1 = TopID.substr(0, bstr1);


    _page.showView('New');

}



function afterModelSaveEx(result) {
    //e.preventDefault();
    //alert(result.Message);
    if (result.Message != null) {
        //   fkbox1 = document.getElementById('hidPkBox').value;

        var bstr = result.Message;

        //var strlen=length(
        var bstr1 = bstr.search("-");
        var bstr2 = bstr.substr(0, bstr1);
        fkbox1 = bstr2;

        var bstr3 = bstr.search(".");
        var bstr4 = bstr.substr((bstr1 + 1))
        // fkbox1 = result.Message;
    }
    else {
        fkbox1 = document.getElementById('hidPkBox').value;

    }
    //window.open("/FlexEst?Enquiry=" + BoxId + ", EnqChild," + oResult1.data.EnqChild + ', Fk_BoxEstimation,' + oResult1.data.BoxEstimation + ', Pk_BoxEstimationChild,' + oResult1.data.EstBoxChild);

    var BType = document.getElementById('Fk_BoxType').value;

    var FType = document.getElementById('Fk_FluteType').value;

    window.open("/BoxSpec?BoxMaster=" + fkbox1 + ", Type=" + BType + ", TakeUp~" + bstr4 + "; FType=" + FType);



};

function afterOneToManyDialogShow() {

    //$("#cmdMaterialSearch").click(function (e) {
    //    e.preventDefault();
    //    setUpMaterialSearch();
    //    _util.setDivPosition("divSearchMaterial", "block");
    //    _util.setDivPosition("divCreateMaterial", "none");
    //});
    $('#cmdSearchMaterialsdetails').click(function (e) {
        e.preventDefault();
        setUpMaterialSearch();
        _util.setDivPosition("divSearchMaterial", "block");
        _util.setDivPosition("divCreateMaterial", "none");
    });


}
//$(document).ready(function () {
//    loadMainSearchContainer();
//    $('#Qty').focus();
//});


function beforeModelSaveEx() {
    var viewModel = _page.getViewByName('New').viewModel;
    viewModel.data["Total"] = $('#SumVal').val();
    viewModel.data["convValue"] = $('#ConvRate').val();
    viewModel.data["transportValue"] = $('#TransportValue').val();
    viewModel.data["gMarginValue"] = $('#MargV').val();
    viewModel.data["taxesValue"] = $('#Taxes').val();
    viewModel.data["VATValue"] = $('#VATV').val();
    viewModel.data["CSTValue"] = $('#CSTV').val();
    viewModel.data["NetTotal"] = $('#NetTotal').val();

    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel1.data["Total"] = $('#SumVal').val();
    viewModel1.data["convValue"] = $('#ConvRate').val();
    viewModel1.data["transportValue"] = $('#TransportValue').val();
    viewModel1.data["gMarginValue"] = $('#MargV').val();
    viewModel1.data["taxesValue"] = $('#Taxes').val();
    viewModel1.data["VATValue"] = $('#VATV').val();
    viewModel1.data["CSTValue"] = $('#CSTV').val();
    viewModel1.data["NetTotal"] = $('#NetTotal').val();

    //viewModel.data["Fk_Indent"] = IndentNo;
}




function setUpMaterialSearch() {
    //Branch

    cleanSearchDialog();

    $("#srchMHeader").text("Paper Search");

    //Adding Search fields
    var txtFieldMaterialName = "<input type='text' id='txtdlgMaterialName' placeholder='Paper Name' class='input-large search-query'/>&nbsp;&nbsp;";

    $("#dlgMaterialSearchFields").append(txtFieldMaterialName);

    $('#MaterialSearchContainer').jtable({
        title: 'Material List',
        paging: true,
        pageSize: 8,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/Paper/PaperListByFiter'
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdMaterialDone').click();
            });
        },

        fields: {
            Pk_Material: {
                title: 'Material Id',
                key: true
            },
            Name: {
                title: 'Material Name',
                edit: false
            },

            GSM: {
                title: 'GSM',
                edit: false
            },
            BF: {
                title: 'BF',
                edit: false

            },
            //Deckle: {
            //    title: 'Deckle',
            //    edit: false
            //},


        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            Name: $('#txtdlgMaterialName').val()
        });
    });

    $('#LoadRecordsButton').click();

    $('#cmdMaterialSearch').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            Name: $('#txtMaterial').val()
        });
    });



    $('#cmdMaterialDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MaterialSearchContainer').jtable('selectedRows');

        $('#txtFk_Material').wgReferenceField("setData", rows[0].keyValue);
        //$("#GSM").val(rows[0].data.GSM);
        //$("#BF").val(rows[0].data.BF);
        //$("#Deckel").val(rows[0].data.Deckle);

        _util.setDivPosition("divCreateMaterial", "block");
        _util.setDivPosition("divSearchMaterial", "none");

    });
}
function resetOneToManyForm(property) {

    $("#txtFk_Material").val("");

}
function afterNewShow(viewObject) {
   

 
        //var x = document.getElementById('divSearchArea');
        //if (x.style.display == 'none') {
        //    x.style.display = 'block';
        //} else {
        //    x.style.visibility = 'hidden';
        //}

}


function Calc1() {

    var BBSVal;
    var qtyVal = document.getElementById('Qty').value;

    if (qtyVal > 0) {
        var Rate = document.getElementById('Rate').value;
        var qty = document.getElementById('PReq').value;
        document.getElementById('Cost').value = Math.round(Rate * qty);


      
    }
}
function deleteRowM(r) {
    var i = r.parentNode.parentNode.rowIndex;
    
    //var ExWt=document.getElementById('txtBoxWt').value;
    //var ExBS = document.getElementById('txtBS').value;
    //var ExSVal = document.getElementById('SumVal').value;
  




    document.getElementById("list").deleteRow(i);
    FunCalc();
   // makeSumryFromColumn();
};


//function makeSumryFromColumn() {
//    var arr = [];
//    var qty = [];
//    var arrQty = [];
//    //$("tr td:nth-child(1)").addClass('date');
//    //$("#ItemsTable tr").each(function(){
//    $("#list tr td:nth-child(1)").each(function () {
//        var indx = arr.indexOf($(this).text());
//        var qty;
//        var str;
//        //if ($.inArray($(this).text(), arr) == -1) {
//        if (indx == -1) {
//            arr.push($(this).text());
//            str = $(this).parent().find("td:nth-child(5)").text(); //$(this).next().text();
//            qty = isNaN(str) ? 0 : parseInt(str);
//            arrQty.push(qty);
//        }
//        else {
//            str = $(this).parent().find("td:nth-child(5)").text(); //$(this).next().text();
//            qty = isNaN(str) ? 0 : parseInt(str);
//            arrQty[indx] += qty;
//        }
//    });

//    //$("#listSumry > tbody").html("");

//    //var rows = "";
//    //for (var i = 0; i < arr.length; i++) {
//    //    rows += "<tr><td>" + arr[i] + "</td>" + "<td>" + arrQty[i] + "</td></tr>";
//    //}

//    //$(rows).appendTo("#listSumry tbody");

//};


function AddData() {

    var rows = "";
    var GSM = document.getElementById("GSM").value;
    var BF = document.getElementById("BF").value;
    var TKF = document.getElementById("TKF").value;
    var BS = document.getElementById("BS").value;
    var Weight = document.getElementById("Weight").value;
    var Rate = document.getElementById("Rate").value;
    var Cost = document.getElementById("Cost").value;
    var PapReq = document.getElementById("PReq").value;
    var total = document.getElementById("Total").value;

    rows += "<tr><td>" + GSM + "</td><td>" + BF + "</td><td>" + TKF + "</td><td>" + PapReq + "</td><td>" + Weight + "</td><td>" + BS + "</td><td>" + Rate + "</td><td>" + Cost + "</td></td><td><input type='button' value='Delete' onclick='deleteRowM(this)'> </td>"
    //  rows += "<tr><td>" + name + "</td><td>" + gender + "</td><td>" + age + "</td><td>" + city + "</td></tr>";
    $(rows).appendTo("#list tbody");

    document.getElementById("GSM").value = "";
    document.getElementById("BF").value = "";
    document.getElementById("TKF").value = "";
    document.getElementById("Weight").value = "";
    document.getElementById("Cost").value = "";
    document.getElementById("PReq").value = "";
    document.getElementById("Rate").value = "";
    document.getElementById("BS").value = "";


    FunCalc();
}

function FunCalc() {
    var WastePer = 0;
    var WasteValue = 0;

    var n1 = document.getElementById("list").rows.length;

    var i = 0, j = 0;
    var str = "";
    var SVal = 0;
    var WVal = 0;
    var BSVal = 0;


    for (i = 0; i < n1; i++) {

        var n2 = document.getElementById("list").rows[i].cells.length;

        for (j = 0; j < n2; j++) {

            var x = document.getElementById("list").rows[i].cells.item(j).innerHTML;

            str = str + x + ":";


            if (i > 0) {
                if (j == 4) {
                    var Temp = 0;
                    Temp = x;
                    WVal = Number(WVal) + Number(Temp);
                }
                if (j == 5) {
                    var Temp = 0;
                    Temp = x;
                    BSVal = Number(BSVal) + Number(Temp);
                }
                if (j == 7) {
                    var Temp = 0;
                    Temp = x;
                    SVal = Number(SVal) + Number(Temp);
                }
            }
        }
        str = str + "#";


    }
     document.getElementById("SumVal").innerHTML=SVal;
    WastePer = document.getElementById('Wastage').value;
    if (WastePer < 0 || WastePer == undefined)
    { WastePer = 1; }
    else
    { WasteValue = Number(WastePer / 100) * SVal; }

    SVal = SVal + WasteValue;

    document.getElementById('SumVal').value = SVal;
    document.getElementById('txtBoxWt').value = WVal;
    document.getElementById('txtBS').value = BSVal;
}

function CalcWeight() {

    var BBSVal;
    var length = document.getElementById('Length').value;
    var width = document.getElementById('Width').value;
    var BF = document.getElementById("BF").value;
    var height = document.getElementById('Height').value;
    var gsm = document.getElementById('GSM').value;
    var tkf = document.getElementById('TKF').value;

    var TLength = (Number(length) * 2 + Number(width) * 2) + Number(50);    /////////=cut length
    var deckel = ((Number(width) + Number(height) + 20) / 10);
    var addl = ((TLength * deckel) / 100000);

    //var GSM = document.getElementById("GSM").value;
    //var BF = document.getElementById("BF").value;
    //var TKF = document.getElementById("TKF").value;


    //if (tkf % 2 != 0) {
    //    BBSVal = ((Number(gsm) * Number(BF)) / 1000)
    //}
    //else {

    //    BBSVal = ((Number(gsm) * Number(BF)) / 1000) / 2
    //}
    if (tkf == 1) {
        BBSVal = ((Number(gsm) * Number(BF)) / 1000)
    }
    else {

        BBSVal = ((Number(gsm) * Number(BF)) / 1000) / 2
    }
    document.getElementById("BS").value = BBSVal;

    var CWeight =((addl * Number(tkf) * Number(gsm)) / 1000).toFixed(2);

    document.getElementById('Weight').value = CWeight;




    CalcPReq();
}

function CalcPReq() {
    var QtyVal = document.getElementById('Qty').value;
    var wt = document.getElementById('Weight').value;

    var PaperReq = QtyVal * wt;

    document.getElementById('PReq').value = PaperReq;

    if (QtyVal > 0) {
        $("#Rate").focus();
    }
    else {
        $("#Qty").focus();
    }
    
}

function CalcConvRate() {

    var totalWeight = document.getElementById('txtBoxWt').value;
    var totRate = document.getElementById('SumVal').value;
    var QtyVal = document.getElementById('Qty').value;
    var taxesValue = 0;
    var transportValue = 0;
    var weightHValue = 0;


    var convRate = document.getElementById('Conv').value;


    ///////////////////

    //var n1 = document.getElementById("list").rows.length;
    //WastePer = document.getElementById('Wastage').value;

    var i = 0, j = 0;
    var str = "";
    var PVal = 0;
    var CVal = 0;
    var TempVal = 0;
    var NetConv = 0;
    //for (i = 0; i < n1; i++) {

    //    var n2 = document.getElementById("list").rows[i].cells.length;

    //    for (j = 0; j < n2; j++) {

    //        var x = document.getElementById("list").rows[i].cells.item(j).innerHTML;

    //        str = str + x + ":";


    //        if (i > 0) {
    //            if (j == 2) {
    //                var Temp = 0;
    //                Temp = x;
    //                PVal = Number(PVal) + Number(Temp);

    //                CVal = PVal + (PVal * (WastePer / 100))


    //                TempVal = TempVal + CVal;


    //            }

    //        }
    //    }
    //    str = str + "#";
    //    NetConv = Math.round((Number(CVal) * Number(convRate)));

    //}
    NetConv = Math.round((Number(totalWeight) * Number(convRate) ));
    document.getElementById('ConvRate').value = NetConv;

    var RPBVal = document.getElementById('RPB').value;

    if (RPBVal > 0) {
        SubChangeCalc();
    }
}



//function CalcConvRate() {

//    var totalWeight = document.getElementById('txtBoxWt').value;
//    var totRate = document.getElementById('SumVal').value;
//    var QtyVal = document.getElementById('Qty').value;
//    var taxesValue = 0;
//    var transportValue = 0;
//    var weightHValue = 0;


//    var convRate = document.getElementById('Conv').value;

//    ///////////////////

//    var n1 = document.getElementById("list").rows.length;
//    WastePer = document.getElementById('Wastage').value;

//    var i = 0, j = 0;
//    var str = "";
//    var PVal = 0;
//    var CVal = 0;
//    var TempVal = 0;
//    var NetConv = 0;
//    for (i = 0; i < n1; i++) {

//        var n2 = document.getElementById("list").rows[i].cells.length;

//        for (j = 0; j < n2; j++) {

//            var x = document.getElementById("list").rows[i].cells.item(j).innerHTML;

//            str = str + x + ":";


//            if (i > 0) {
//                if (j == 2) {
//                    var Temp = 0;
//                    Temp = x;
//                    PVal = Number(PVal) + Number(Temp);

//                    CVal = PVal + (PVal * (WastePer / 100))


//                    TempVal = TempVal + CVal;


//                }

//            }
//        }
//        str = str + "#";
//        NetConv = Math.round((Number(CVal) * Number(convRate)));

//    }

//    document.getElementById('ConvRate').value = NetConv;

//    //var RPBVal = document.getElementById('RPB').value;

//    //if (RPBVal > 0) {
//    //    SubChangeCalc();
//    //}
//}

//inventory-- - rejectionvalue
//cgst--taxesvalue
//sgst--vatvalue




function CalcGMarginValue() {
    var totalWeight = document.getElementById('txtBoxWt').value;
    var totRate = document.getElementById('SumVal').value;
    var taxesValue = 0;
    var transportValue = 0;
    var weightHValue = 0;
    //SumVal
    //Now rate calculation
    //Declaring variables
    var convValue = document.getElementById('ConvRate').value;
    var PaperCost = document.getElementById('SumVal').value;
    var Printing = document.getElementById('Printing').value;
    var Inventory = document.getElementById('Inventory').value;
    var Development = document.getElementById('Development').value;
    var TransVal = document.getElementById('TransportValue').value;
    var gMarginPercentage = document.getElementById('Marg').value;


    var gMarginValue = (Number(convValue) + Number(PaperCost) + Number(Printing) + Number(Development) + Number(TransVal)) * Number(gMarginPercentage) / 100;
    document.getElementById('MargV').value = Math.round(gMarginValue);

    var Margin = document.getElementById('MargV').value;

    document.getElementById('NetAmt').value = Number(convValue) + Number(PaperCost) + Number(Printing) + Number(Inventory) + Number(Development) + Number(TransVal) + Number(Margin);

    transportValue = document.getElementById('TransportValue').value;

    totalAddPrice = (Number(convValue) +
                  Number(gMarginValue) +
                  Number(taxesValue) +
                  Number(transportValue) +
                  Number(weightHValue) +
                  Number(Inventory) +
                  Number(Printing) +
                  Number(Development)

                 );

    //estModel.totalPrice(totalPrice);

    //alert(totalPrice);
}

function CalcEDValue() {
    var totalWeight = document.getElementById('txtBoxWt').value;
    var totRate = document.getElementById('SumVal').value;
    var taxesValue = 0;
    var transportValue = 0;
    var weightHValue = 0;

    //Now rate calculation
    //Declaring variables
    var convValue = document.getElementById('ConvRate').value;
    var Transport = document.getElementById('TransportValue').value;
    var Printing = document.getElementById('Printing').value;
    var Inventory = document.getElementById('Inventory').value;
    var Development = document.getElementById('Development').value;


    var gMarginValue = document.getElementById('MargV').value;
    var taxesPercntage = document.getElementById('Tax').value;
    document.getElementById('VAT').value = taxesPercntage;
    //var taxesValue = (Number(convValue) + Number(totRate)  + Number(gMarginValue)) * (Number(taxesPercntage / 100));

    var taxesValue = (Number(totRate) + Number(convValue) + Number(Printing) + Number(Development) + Number(Inventory) + Number(Transport) + Number(gMarginValue)) * (Number(taxesPercntage) / 100);
    document.getElementById('Taxes').value = taxesValue;

    transportValue = document.getElementById('TransportValue').value;

    var VATPercentage = document.getElementById('Tax').value;
    var VatValue = (Number(totRate) + Number(convValue) + Number(Printing) + Number(Development) + Number(Inventory) + Number(Transport) + Number(gMarginValue)) * (Number(VATPercentage) / 100);
    document.getElementById('VATV').value = VatValue;


    totalAddPrice = (Number(convValue) +
                  Number(gMarginValue) +
                  Number(taxesValue) +
                  Number(transportValue) +
                  Number(weightHValue) +
                  Number(Printing) +
                  Number(Inventory) +
                  Number(Development)
                 );

    //estModel.totalPrice(totalPrice);

    //alert(totalPrice);
    CalcNet();
}




function CalcVATValue() {
    var totalWeight = document.getElementById('txtBoxWt').value;
    var totRate = document.getElementById('SumVal').value;
    var taxesValue = 0;
    var transportValue = 0;
    var weightHValue = 0;

    //Now rate calculation
    //Declaring variables
    var convValue = document.getElementById('ConvRate').value;
    // var rejectionValue = document.getElementById('Rejection').value;
    var Development = document.getElementById('Development').value;
    var Printing = document.getElementById('Printing').value;
    var Inventory = document.getElementById('Inventory').value;
    var Transport = document.getElementById('TransportValue').value;
    var gMarginValue = document.getElementById('MargV').value;

    var taxesValue = document.getElementById('Taxes').value;

    //var VATPercentage = document.getElementById('VAT').value;


    //var VatValue = (Number(totRate) + Number(convValue) + Number(Printing) + Number(Development) + Number(Inventory) + Number(Transport) + Number(gMarginValue)) * (Number(VATPercentage) / 100);
    //document.getElementById('VATV').value = VatValue;



    totalPrice = (Number(convValue) +
                  Number(gMarginValue) +
                  Number(taxesValue) +
                  Number(transportValue) +
                  Number(weightHValue) +
                    Number(Inventory) +
                  Number(Printing) +
                  Number(Development)
                 );

    //estModel.totalPrice(totalPrice);
    // rateChanged(Estimation);
    //alert(totalPrice);

    CalcNet();
}


function CalcCSTValue() {

    //Now rate calculation
    //Declaring variables
    var convValue = document.getElementById('ConvRate').value;
    // var rejectionValue = document.getElementById('Rejection').value;
    var gMarginValue = document.getElementById('MargV').value;
    var taxesValue = document.getElementById('Taxes').value;
    var VatValue = document.getElementById('VATV').value;
    TransportValue = document.getElementById('TransportValue').value;
    //   var weightHValue = document.getElementById('WeightH').value;
    var Development = document.getElementById('Development').value;
    var Printing = document.getElementById('Printing').value;
    var Inventory = document.getElementById('Inventory').value;

    //var handlingChanrgesValue = document.getElementById('HCharges').value;
    //var packingChargesValue = document.getElementById('PCharges').value;


    var CSTPercentage = document.getElementById('CST').value;
    var CstValue = (Number(convValue) + Number(totRate) + Number(gMarginValue)) * (Number(CSTPercentage) / 100);
    document.getElementById('CSTV').value = CstValue;





    totalPrice = (Number(convValue) +
                  Number(gMarginValue) +
                  Number(taxesValue) +
                  Number(TransportValue) +
               Number(Inventory) +
                  Number(Printing) +
                  Number(Development)
                  );

    // rateChanged(Estimation);
    // estModel.totalPrice(totalPrice);

    //alert(totalPrice);
}


function CalcTransValue() {
    var totalWeight = document.getElementById('txtBoxWt').value;
    var totalQty = document.getElementById('Qty').value;
    var totRate = document.getElementById('SumVal').value;
    var taxesValue = 0;
    var transportValue = 0;
    var weightHValue = 0;
    //Now rate calculation
    //Declaring variables
    var convValue = document.getElementById('ConvRate').value;
    //var rejectionValue = document.getElementById('Rejection').value;
    var gMarginValue = document.getElementById('MargV').value;
    var taxesValue = document.getElementById('Taxes').value;
    var VatValue = document.getElementById('VATV').value;
    TransportValue = document.getElementById('TransportValue').value;
    TransportPercentage = document.getElementById('Transport').value;
    // var weightHValue = document.getElementById('WeightH').value;
    //var handlingChanrgesValue = document.getElementById('HCharges').value;
    //var packingChargesValue = document.getElementById('PCharges').value;
    var Development = document.getElementById('Development').value;
    var Printing = document.getElementById('Printing').value;
    var Inventory = document.getElementById('Inventory').value;



    if (TransportValue == null || TransportValue == 0 || TransportValue == '') {
        if (TransportPercentage != null || TransportPercentage != 0) {

            //   var TransportValue = (Number(convValue) + Number(totRate)) * Number(TransportPercentage);
            var TransportValue = Number(totalQty) * Number(TransportPercentage);
            document.getElementById('TransportValue').value = TransportValue;
        }


    }

    CalcInv();
    //var CSTPercentage = document.getElementById('CST').value;
    //var CstValue = (Number(convValue) + Number(totRate) + Number(rejectionValue) + Number(gMarginValue) + Number(taxesValue)) * (Number(CSTPercentage) / 100);
    //document.getElementById('CSTV').value = CstValue;






    totalPrice = (Number(convValue) +
                  Number(gMarginValue) +
                  Number(taxesValue) +
                  Number(TransportValue) +
                     Number(Inventory) +
                  Number(Printing) +
                  Number(Development)
                );

    // rateChanged(Estimation);
    // estModel.totalPrice(totalPrice);

    //alert(totalPrice);
}

function CalcPrint() {
    var Qtyval = document.getElementById('Qty').value;
    var PrintVal = document.getElementById('PrintingPer').value;
    document.getElementById('Printing').value = Number(Qtyval) * Number(PrintVal);


    var RPBVal = document.getElementById('RPB').value;

    if (RPBVal > 0) {
        SubChangeCalc();
    }
}

function CalcDev() {
    var Qtyval = document.getElementById('Qty').value;
    var DevVal = document.getElementById('DevelopmentPer').value;
    document.getElementById('Development').value = Number(Qtyval) * Number(DevVal);

    var RPBVal = document.getElementById('RPB').value;

    if (RPBVal > 0) {
        SubChangeCalc();
    }
}
function CalcInv() {
    var Qtyval = document.getElementById('Qty').value;
    var DevVal = document.getElementById('Development').value;
    var PapVal = document.getElementById('SumVal').value;
    var ConvVal = document.getElementById('ConvRate').value;
    var TransVal = document.getElementById('TransportValue').value;
    var Invper = document.getElementById('InvPer').value;
    document.getElementById('Inventory').value = Math.round(Number((Invper / 100)) * (Number(Qtyval) + Number(DevVal) + Number(PapVal) + Number(ConvVal) + Number(TransVal)));

    var RPBVal = document.getElementById('RPB').value;

    if (RPBVal > 0) {
        SubChangeCalc();
    }
}

function CalcNet() {
    var a = document.getElementById('ConvRate').value;
    //var b= document.getElementById('Rejection').value;
    var c = document.getElementById('MargV').value;
    var d = document.getElementById('Taxes').value;
    var e = document.getElementById('VATV').value;
    var f = document.getElementById('CSTV').value;
    var g = document.getElementById('TransportValue').value;
    //  var h = document.getElementById('WeightH').value;
    var i = document.getElementById('Printing').value;
    var j = document.getElementById('Development').value;
    var k = document.getElementById('Inventory').value;

    var NetVal = Number(a) + Number(c) + Number(d) + Number(e) + Number(f) + Number(g) + Number(i) + Number(j) + Number(k);

    document.getElementById('Total').value = NetVal;

    //document.getElementById('SumVal').value



    var PC = document.getElementById('SumVal').value;



    var NVal = Number(NetVal) + Number(PC);

    document.getElementById('NetTotal').value = Math.round(NVal);



    var QtyBoxes = document.getElementById('Qty').value;
    var PerPiece = Number(NVal) / Number(QtyBoxes);
    document.getElementById('RPB').value = Math.round(PerPiece);

    
}

function SubChangeCalc()
{

 
    var convValue = document.getElementById('ConvRate').value;
    var PaperCost = document.getElementById('SumVal').value;
    var Printing = document.getElementById('Printing').value;
    var Inventory = document.getElementById('Inventory').value;
    var Development = document.getElementById('Development').value;
    var TransVal = document.getElementById('TransportValue').value;
    var gMarginPercentage = document.getElementById('Marg').value;


    var gMarginValue = (Number(convValue) + Number(PaperCost) + Number(Printing) + Number(Development) + Number(TransVal)) * Number(gMarginPercentage) / 100;
    document.getElementById('MargV').value = Math.round(gMarginValue);

    var Margin = document.getElementById('MargV').value;
    document.getElementById('NetAmt').value = Number(convValue) + Number(PaperCost) + Number(Printing) + Number(Inventory) + Number(Development) + Number(TransVal) + Number(Margin);

    var RPBVal = document.getElementById('RPB').value;

    if (RPBVal > 0) {

        CalcNet();
    }
}