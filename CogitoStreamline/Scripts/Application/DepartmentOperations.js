﻿//Name          : Department Operations ----javascript files 
//Description   : Contains the  Department Operations  definition with following
//                 1. Jtable Loading and Columns Descriptions 2.Button Click Events  3. Department Duplicate Checking
//Author        : Nagesh V Rao
//Date 	        : 22/10/2015
//Crh Number    : SL0009
//Modifications : 



var objShortCut = null;
function initialCRUDLoad() {
    $('#MainSearchContainer').jtable({
        title: 'Department List',
        paging: true,
        pageSize: 8,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Department/DepartmentListByFiter',
            updateAction: '',
            createAction: '',
            deleteAction: ''
        },
        fields: {
            Pk_DepartmentId: {
                title: 'Id',
                key: true,
                list: true
            },
            Name: {
                title: 'Name',
                width: '25%'
            },
            Description: {
                title: 'Description',
                width: '25%'
            }

        }
    });
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_DepartmentId: $('#txtDeptID').val(),
            Name: $('#txtDeptName').val()
        });
    });
    $('#LoadRecordsButton').click();
    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });
}
function CheckDeptDuplicate() {
    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("Name", $('#DeptName').val());
    oResult = _comLayer.executeSyncAction("Department/DeptDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + "Department Already added";
}









