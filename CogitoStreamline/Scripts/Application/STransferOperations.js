﻿//Name          : Inward Operations ----javascript files
//Description   : Contains the  material indent Operations  definition
//                 1. Jtable Loading and Columns Descriptions 2.Button Click Events  3. Check date validations
//Author        : shantha
//Date            :   03/09/2015
//CRH Number      :   SL0047
//Modifications : 


var curViewModel = null;
var bFromSave = false;
var bEditContext = false;



function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Stock Transfer List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/STransfer/TransferListByFiter',
            updateAction: '',
            createAction: ''
            //deleteAction: ''
        },
        fields: {
            Pk_TransferID: {
                title: 'Id',
                key: true
            },
            TransferDate: {
                title: 'Date',
                width: '25%'
            },
            Fk_FromBranchID: {
                title: 'From Branch',
                width: '25%'
            },
            Fk_ToBranchID: {
                title: 'To Branch',
                width: '25%'
            },
            Fk_Material: {
                title: 'Material',
                width: '25%'
            },
            TransferQty: {
                title: 'Quantity',
                width: '25%'
            },
        }
    });



    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_TransferID: $('#TransferID').val()
        });
    });
    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });

    _page.getViewByName('New').viewModel.addAddtionalDataSources("Branches", "getFromBranch", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("Branches", "getFromBranch", null);
    _page.getViewByName('New').viewModel.addAddtionalDataSources("Branches", "getToBranch", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("Branches", "getToBranch", null);
}


function afterNewShow(viewObject) {

    bEditContext = false;
    curViewModel = viewObject.viewModel;
    var dNow = new Date();
    document.getElementById('TransferDate').value = (((dNow.getDate()) < 10) ? "0" + (dNow.getDate()) : (dNow.getDate())) + '/' + (((dNow.getMonth() + 1) < 10) ? "0" + (dNow.getMonth() + 1) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();
    $('#TransferDate').datepicker({ autoclose: true });

    $('#cmdMaterialSearch').click(function (e) {
        e.preventDefault();
        setUpMaterialSearch();
        $("#searchMaterialDialog").modal("show");
    });
}



function beforeOneToManySaveHook(objectContext) {
    var bValidation = true;

    if ($("#trQuantity").val() == "") {
        $("#trQuantity").validationEngine('showPrompt', 'Enter Quantity', 'error', true)
        bValidation = false;
    }
    else if (!$.isNumeric($("#trQuantity").val())) {
        $("#trQuantity").validationEngine('showPrompt', 'Enter Numbers', 'error', true)
        bValidation = false;
    }
    if ($("#txtFk_Material").val() == "") {
        $("#txtFk_Material").validationEngine('showPrompt', 'Select Material', 'error', true)
        bValidation = false;
    }

    return bValidation;


}

function afterEditShow(viewObject) {

    curViewModel = viewObject.viewModel;



    $('#TransferDate').datepicker({
        autoclose: true
    });



}



function setUpMaterialSearch() {

    $("#srchsHeader").text("Material Search");

    //Adding Search fields
    var txtFieldMaterialName = "<input type='text' id='txtdlgMaterialName' placeholder='Material Name' class='input-large search-query'/>&nbsp;&nbsp;";
    // var txtFieldIndentNo = "<input type='text' id='txtdlgIndentNo' placeholder='Indent No.' class='input-large search-query'/>&nbsp;&nbsp;";
    //var txtFieldVendorName = "<input type='text' id='txtdlgVedndorName' placeholder='Vendor Name' class='input-large search-query'/>&nbsp;&nbsp;";


    $("#dlgSearchMaterialFields").append(txtFieldMaterialName);
    // $("#dlgSearchMaterialFields").append(txtFieldIndentNo);
    // $("#dlgSearchMaterialFields").append(txtFieldVendorName);

    $('#MaterialSearhContainer').jtable({
        title: 'Material List',
        paging: true,
        pageSize: 8,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/STransfer/TransferList'
        },
        fields: {
            Pk_Material: {
                title: 'MaterialID',
                key: true
                //list: false
            },
            Name: {
                title: 'Material',
                List: true
            },
            StockQty: {
                title: 'Stock'

            }
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearhContainer').jtable('load', {
            MaterialName: $('#txtdlgMaterialName').val()
        });
    });

    $('#LoadRecordsButton').click();

    $('#cmdMatSearch').click(function (e) {
        e.preventDefault();
        $('#MaterialSearhContainer').jtable('load', {
            MaterialName: $('#txtdlgMaterialName').val(),

        });
    });



    $('#cmdMaterialDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MaterialSearhContainer').jtable('selectedRows');
        $("#searchMaterialDialog").modal("hide");


        $('#txtFk_Material').wgReferenceField("setData", rows[0].data.Pk_Material);


        document.getElementById('TxtQuantity').value = rows[0].data.StockQty;


    });
}





function beforeModelSaveEx() {

    var viewModel = _page.getViewByName('New').viewModel;
    viewModel.data["TransferDate"] = $('#TransferDate').val();
    //viewModel.data["Fk_QualityCheck"] = $('#txtQualityCheck').val();
    //// viewModel.data["Fk_Material"] = $('#txtFk_Material').val();

    //viewModel.data["Fk_Indent"] = $('#TxtIndent').val();

    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel1.data["TransferDate"] = $('#TransferDate').val();
    //viewModel1.data["Inward_Date"] = $('#dtPInwardDate').val();
    //viewModel1.data["Fk_QualityCheck"] = $('#txtQualityCheck').val();
    //// viewModel1.data["Fk_Material"] = $('#txtFk_Material').val();

    //viewModel.data["Fk_Indent"] = $('#TxtIndent').val();
}

function checkstock() {
    //if (objContextEdit == false) {
    if (Number($('#TxtQuantity').val()) < Number($('#TransferQty').val())) {
        return "* " + "Transfer cannot Exceed Available Qty :" + $('#TxtQuantity').val();
    }
    else {

    }
}
function ReferenceFieldNotInitilized(viewModel) {


    $('#txtFk_Material').wgReferenceField({
        keyProperty: "Fk_Material",
        displayProperty: "Name",
        loadPath: "Material/Load",
        viewModel: viewModel
    });


    if (viewModel.data != null) {
        $('#txtFk_Material').wgReferenceField("setData", viewModel.data["Fk_Material"]);

    }
}
function cleanSearchDialog() {

    //Clean the search dialog of previous bindings and prepare for this selected dialog
    $('#MainSearhContainer').empty();
    $('#MainSearhContainer').append("<div id='SearchContainer'></div>");
    //  $("#dlgSearchFields").empty();
    //  $('#cmdSearch').off();
    // $('#cmdDone').off();

    $('#dlgSearchMaterialFields').empty();

}


//function checkstock() {
//    if (objContextEdit == false) {
//        if (Number($('#Quantity').val()) < Number($('#InwQuantity').val())) {
//            return "* " + "Inward Quantity cannot Exceed Quality Checked Qty :" + $('#Quantity').val();
//        }
//        else {

//        }
//    }
//}
