﻿
function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'LengthPartitions List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/LPartition/LPartListByFiter',
            updateAction: '',
            createAction: '',
            //deleteAction: ''
        },
        fields: {
            Pk_Material: {
                title: 'Id',
                key: true,
                width: '5%'
            },
            Name: {
                title: 'Name'
            },
            Length: {
                title: 'Length'
            },
            Width: {
                title: 'Width'
            },
            Height: {
                title: 'Height'
            },
            Weight: {
                title: 'Weight'
            },
            GSM: {
                title: 'GSM',
                width: '5%'
            },
            BF: {
                title: 'BF'
            },
            PPly: {
                title: 'PPLY',
                width: '5%'
            }


        }
    });

    //Re-load records when Staff click 'load records' button.

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Name: $('#TxtCapName').val(),
            Length: $('#TxtLength').val(),
            Width: $('#TxtWidth').val(),
            GSM: $('#TxtGSM').val(),
            BF: $('#TxtBF').val()


        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });



}

function beforeModelSaveEx() {

    var viewModel = _page.getViewByName('New').viewModel;
    viewModel.data["Weight"] = $('#Weight').val();

    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel1.data["Weight"] = $('#Weight').val();
}

function afterNewShow(viewObject) {
    viewObject.viewModel.data["Category"] = "LengthPartition";

    $(".input-large").focusout(function () {
        try {
            var length = document.getElementById('Length').value;
            var width = document.getElementById('Width').value;
            var height = document.getElementById('Height').value;
            var BF = document.getElementById('BF').value;
            var GSM = document.getElementById('GSM').value;

            var PPly = document.getElementById('PPly').value;

            var cal1 = Number(length) + Number(width) + 50;
            cal1 = (Math.round(Number(cal1) / 100)) * 100;
            var cal2 = (Math.round(Number(height) / 100)) * 100;

            var weight = Number(cal1) * 2 * Number(cal2) * Number(GSM) * Number(PPly) * 8;
            document.getElementById('Weight').value = Number(weight) / 1000000;
        }
        catch (e) { }

    });
}
function afterEditShow(viewObject) {

    $(".input-large").focusout(function () {
        try {
            var length = document.getElementById('Length').value;
            var width = document.getElementById('Width').value;
            var height = document.getElementById('Height').value;
            var BF = document.getElementById('BF').value;
            var GSM = document.getElementById('GSM').value;

            var PPly = document.getElementById('PPly').value;

            var cal1 = Number(length) + Number(width) + 50;
            cal1 = (Math.round(Number(cal1) / 100)) * 100;
            var cal2 = (Math.round(Number(height) / 100)) * 100;

            var weight = Number(cal1) * 2 * Number(cal2) * Number(GSM) * Number(PPly) * 8;
            document.getElementById('Weight').value = Number(weight) / 1000000;
        }
        catch (e) { }
    });
}
function CheckLDuplicate() {

    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("Name", $('#Name').val());
    oResult = _comLayer.executeSyncAction("LPartition/LPartDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + "Name Already added";

}
