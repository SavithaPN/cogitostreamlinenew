﻿function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Transporter List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Transporter/TransporterListByFiter',
            updateAction: '',
            createAction: '',
            //deleteAction: ''
        },
             fields: {
            Pk_ID: {
                title: 'Id',
                key: true,
            },
            TransporterName: {
                title: 'Name'
            },
            Address1: {
                title: 'Address'
            },
            
            LLine: {
                title: 'Land Line'
            },
            MobileNo: {
        title: 'Mobile'
    },
        }
    });

    //Re-load records when Staff click 'load records' button.

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_ID: $('#TxtPkId').val(),
            TransporterName: $('#TxtName').val()
        });

    });
    $('#TxtPkId').keypress(function (e) {
        if (e.KeyCode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Pk_ID: $('#TxtPkId').val(),
                TransporterName: $('#TxtName').val()
            });
        }

    });

    $('#TxtName').keypress(function (e) {
        if (e.KeyCode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Pk_ID: $('#TxtPkId').val(),
                TransporterName: $('#TxtName').val()
            });
        }

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });
}


function CheckNameDuplicate() {

    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("TransporterName", $('#TransporterName').val());
    oResult = _comLayer.executeSyncAction("Transporter/TNameDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + "Name Already added";

}


