﻿var curViewModel = null;
var bFromSave = false;
var bEditContext = false;
var CustCode = "";
var BoxValue = "";
var PrintID = "";
var DPRScID = "";
var QtyVal = 0;
var ProcessID = 0;
var BoxID = 0;
var PartID=0;
function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Job Card List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/JobCard/JobListByFiter',
            updateAction: '',
            createAction: ''
        },
        fields: {
            Pk_JobCardID: {
                title: 'J.Card No',
                key: true,
                width: '3%'
            },
            JDate: {
                title: 'J.Card Date',
                width: '3%'
            },
            Customer: {
                title: 'Cust_Name',
                width: '12%'
            },
            BType: {
                title: 'BType',
                width: '8%',
                list:false
            },
            BoxName: {
                title: 'BoxName',
                width: '8%'
            },
            PartName: {
                title: 'PartName',
                width: '5%'
            },
            Fk_Order: {
                title: 'Cust_Order No',
                width: '3%'
            },
            Fk_Status: {
                title: 'J.C.Status',
                width: '5%'
            },
            Print: {
                title: 'Print',
                width: '2%',
                //display: function (data) {
                //    return '<button type="button" onclick="alert(' + data.record.Invno + ')">Create PDF</button> ';
                display: function (row) {
                    var button = $("<i class='icon-printer'></i>");
                    $(button).click(function () {

                        JCNO = row.record.Pk_JobCardID;
                        var Btype = row.record.BType;

                  

                        if (Btype != "Plain Board") {
                            _comLayer.parameters.add("JCNO", JCNO);
                            _comLayer.executeSyncAction("JobCard/JCRep1", _comLayer.parameters);

                            var strval = "ConvertPDF/JCRep" + JCNO + ".pdf"
                        }
                        else {
                            _comLayer.parameters.add("JCNO", JCNO);
                            _comLayer.executeSyncAction("JobCard/JCRepPlnBrd", _comLayer.parameters);

                            var strval = "ConvertPDF/JCRep_PlnBrd" + JCNO + ".pdf"
                        }

                        


                     //   window.parent.opener.focus();
                   

                       _comLayer.parameters.add("JCNO", JCNO);
                        OrdNo = row.record.Fk_Order;
                        _comLayer.parameters.add("OrdNo", OrdNo);
                        _comLayer.executeSyncAction("JobCard/IssueRep1", _comLayer.parameters);
                        var strval1 = "ConvertPDF/IssueRep" + JCNO + ".pdf"


                      

                        _comLayer.parameters.add("JCNO", JCNO);
                        JNo = row.record.Pk_JobCardID;
                        _comLayer.parameters.add("JNo", JNo);
                        _comLayer.executeSyncAction("JobCard/ProcessRep1", _comLayer.parameters);
                        var strval2 = "ConvertPDF/ProcessRep" + JNo + ".pdf"

                     //   var strval = "ConvertPDF/JCRep" + JCNO + ".pdf"

                        var xhr1 = new XMLHttpRequest();
                        var urlToFile = strval;
                        //var urlToFile = "ConvertPDF/JCRep" + JCNO + ".pdf"
                        xhr1.open('HEAD', urlToFile, false);
                        xhr1.send();

                        if (xhr1.status == "404") {
                            alert('Data Not Available , File does not Exist');
                            //return false;


                        } else {
                            window.open(strval, '_blank ', 'width=900,height=250');
                           // return true;
                        }




                          var xhr2 = new XMLHttpRequest();
                        var urlToFile = "ConvertPDF/IssueRep" + JCNO + ".pdf"


                        xhr2.open('HEAD', urlToFile, false);
                        xhr2.send();

                        if (xhr2.status == "404") {
                            alert('No Issues Against this Job Card');
                            //return false;


                        } else {
                            window.open(strval1, '_blank', 'width=700,height=250');
                           
                        }



                        var xhr3 = new XMLHttpRequest();
                        var urlToFile = "ConvertPDF/ProcessRep" + JNo + ".pdf"
                        xhr3.open('HEAD', urlToFile, false);
                        xhr3.send();

                        if (xhr3.status == "404") {
                            alert('No Processes Entered Against this Job Card');
                            //return false;


                        } else {
                            window.open(strval2, '_blank', 'width=700,height=250');
                            //return true;
                        }


                    


                        //return true;
                        ///////////////////////////
                      

                    });
                    return button;
                }

            },
            Wastage_Rep: {
                title: "Wast.Rep",
                display: function (row) {
                    var button1 = $("<button class='bnt'>Wast.Rep</button>");
                    $(button1).click(function () {
                        JCNO = row.record.Pk_JobCardID;
                        _comLayer.parameters.add("JCNO", JCNO);
                        _comLayer.executeSyncAction("JobCard/DirectWastage", _comLayer.parameters);
                        var strval1 = "ConvertPDF/DirectWastage" + JCNO + ".pdf"
                    //    window.open(strval1, '_blank ', 'width=900,height=350');




                        JCNO = row.record.Pk_JobCardID;
                        _comLayer.parameters.add("JCNO", JCNO);
                        _comLayer.executeSyncAction("JobCard/WastageRep", _comLayer.parameters);
                        var strval = "ConvertPDF/ProcessWastage-" + JCNO + ".pdf"
                      //  window.open(strval, '_blank ', 'width=500,height=250');



                        var xhr1 = new XMLHttpRequest();
                        var urlToFile = "ConvertPDF/DirectWastage" + JCNO + ".pdf"
                        xhr1.open('HEAD', urlToFile, false);
                        xhr1.send();

                        if (xhr1.status == "404") {
                            alert('Issues Not Entered for this JobCard To find Direct Wastage');
                         


                        } else {
                            var parentOpener = window.opener;
                            window.opener = null;
                            window.open(strval1, "_blank");
                            window.opener = parentOpener;
                          

                          //window.open(strval1, '_blank','width=800,height=350');
                      
                          //window.resizeTo(0, 0);
                        }

                        //////////////////
                        var xhr2 = new XMLHttpRequest();
                        var urlToFile = "ConvertPDF/ProcessWastage-" + JCNO + ".pdf"
                        xhr2.open('HEAD', urlToFile, false);
                        xhr2.send();

                        if (xhr2.status == "404") {
                            alert('Processes Not Entered for this JobCard To find Process Wastage');
                            //return false;


                        } else {
                            var parentOpener = window.opener;
                            window.opener = null;
                            window.open(strval, "_blank");
                            window.opener = parentOpener;
                            //window.open(strval, '_blank ', 'pp2', 2, 'width=500,height=250');
                            // return true;
                        }



                    });
                    return button1;
                }
            }
        }
    });

    $('#dtJCDate').datepicker({
        autoclose: true
    });

    //Re-load records when Order click 'load records' button.
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_JobcardID: $('#txtPKJob').val(),
            Customer: $('#txtCust').val(),
            BoxName: $('#txtOrder').val(),
            FromJDate: $('#TxtFromDate').val(),
            ToJDate: $('#TxtToDate').val()
        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });

  


    $('#TxtFromDate').datepicker({ autoclose: true });
    $('#TxtToDate').datepicker({ autoclose: true });
     
  //  $('#cmdCreate').hide();
    setUpOrderSearch();

    //_comLayer.parameters.clear();
    //var CustID = 15;
    //_comLayer.parameters.add("CustId", 15);
    //_comLayer.executeSyncAction("JobCard/getBoxdata", _comLayer.parameters);

    //_page.getViewByName('New').viewModel.addAddtionalDataSources("BoxM", "getBoxdata", CustID);
    //_page.getViewByName('Edit').viewModel.addAddtionalDataSources("BoxM", "getBoxdata", CustID);


}

function afterNewShow(viewObject) {
 

    bEditContext = false;

    $("#dropzone").wgUpload({
        wgTag: "JobCard",
        upLoadUrl: "/FileUpload",
        property: "Documents",
        viewModel: viewObject.viewModel,
        fileLocation: _comLayer.curServer() + "/uploads/"
    });
  
    curViewModel = viewObject.viewModel;

    var dNow = new Date();
    document.getElementById('dtJCDate').value = ((dNow.getDate() < 10) ? ("0" + dNow.getDate()) : dNow.getDate()) + '/' + (((dNow.getMonth() + 1) < 10) ? ("0" + (dNow.getMonth() + 1)) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();

    $('#dtJCDate').datepicker({ autoclose: true });

    $('#tlbMaterials').jtable({
        title: 'Item List',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/JobCard/Bounce',
            deleteAction: '',
            //updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            Fk_Material: {
                title: 'Material Id',
                key: true,
                list: false
            },
            Pk_PaperStock: {
                title: 'Pk PaperStock',
                key: false,
                list: false
            },
            Name: {
                title: 'Material Name'
            },
            Color: {
                title: 'Shade'
            },
            RollNo: {
                title: 'Reel No'
            },
            Quantity: {
                title: "Quantity"
            }

        }
    });


    configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "JobDetails", "JobCard", "_AddMaterial", function () { return new IssuedMaterial() });


    configureOne2Many("#cmdAddPaper", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "JobDetails", "JobCard", "_AddPaper", function () { return new IssuedMaterial() });

    _util.setDivPosition("tlbMaterials", "none");


    $('#tlbMaterials1').jtable({
        title: 'Process List',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/JobCard/Bounce',
            //deleteAction: '',
            //updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            ProcessDate: {
                title: 'Process Dt.'

            },
            Name: {
                title: 'Name',
                key: false,
                list: true
            },
            Fk_ProcessID: {
                title: 'Pr.ID',
                list: false
            },

            //Fk_JobCardID: {
            //    title: 'JobCardNo',
            //    key: false,
            //    list: true
            //},
            Quantity: {
                title: 'Qty'

            },

            WastageQty: {
                title: 'Wastage'

            },

        }
    });


    configureOne2Many("#cmdAddProcess", '#tlbMaterials1', "#cmdSaveProcess", viewObject, "Process", "JobCard", "_AddProcess", function () { return new Process() });


 
 
    //$('#cmdCreate').click(function (e) {
    //    e.preventDefault();
    //    var oResult = _comLayer.executeSyncAction("JobCard/Save", _comLayer.parameters);
    //    //window.open("/BoxSpec?BoxMaster=" + $('#hidPkBox').val() + "&Ply=" + $('#Ply').val());
    //    if (oResult.Success) {
    //        //_util.showSuccess("Data Saved successfully", function () { document.location.href = "/Estimation"; });
    //        //  alert("Data Saved successfully");
    //        //  var x = window.open('/Estimation');
    //        //  x.focus();
    //        //  window.parent.close();
    //        //  window.location.reload(false);
    //    }
    //});
    $('#cmdShowBoxDetails').click(function (e) {

        window.open("/BoxSpec?BoxMaster=" + $('#BoxID').val());

    });
    $('#cmdOrderMSearch').click(function (e) {
        e.preventDefault();
     
        //setUpOrderSearch(viewObject);
       
        $("#searchBoxDialog").height(700);
        $("#searchBoxDialog").width(1000);
        $("#searchBoxDialog").modal("show");

    });
  
 
    //$('#cmdPaper').click(function (e) {
    //    e.preventDefault();

    //    configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "JobDetails", "JobCard", "_AddMaterial", function () { return new IssuedMaterial() });

    //    //cmdSearchMaterialsdetails.click();
    //    setUpPaperSearch(viewObject);

    //    //$("#dataDialog").width(900);
    //    ////$("#dataDialog").css("left", "60px");
    //    //$("#divSearchMaterial").width(800);
    //    //_util.setDivPosition("divSearchMaterial", "block");
    //    //_util.setDivPosition("divCreateMaterial", "none");


    //    $("#searchPaperDialog").modal("show");
    //});

    $('#cmdBoxMSearch').click(function (e) {
        e.preventDefault();
        setUpOrderBoxSearch(viewObject);
        $("#searchBoxDialog1").height(700);
    //    $("#searchBoxDialog1").width(900);
        $("#searchBoxDialog1").modal("show");

    });


    $("#chkChkQuality1").on("click", function () {
        if (chkChkQuality1.checked) {

            var length = document.getElementById('Length').value;
            var width = document.getElementById('Width').value;
            //     document.getElementById('Height').value = oResult1.data[0].Height;


            var TLength = (Number(length) + Number(width) + 50)/10;    /////////=cut length
            var ScQty = document.getElementById('POQuantity').value;
            document.getElementById('Ups').value = "0.5";
            document.getElementById('CuttingSize').value = Number(TLength) ;
            document.getElementById('TQty').value = Number(ScQty) * 2

        }

        else {

            var length = document.getElementById('Length').value;
            var width = document.getElementById('Width').value;
            //     document.getElementById('Height').value = oResult1.data[0].Height;


            var TLength = (Number(length) *2+ Number(width)*2 + 50) / 10;      /////////=cut length
            var ScQty = document.getElementById('POQuantity').value;

            document.getElementById('CuttingSize').value = Number(TLength);
            document.getElementById('TQty').value = Number(ScQty);


            var DeckVal = document.getElementById('Deckle').value;
            var CalDeck = Math.floor(182 / Number(DeckVal));
            if (CalDeck != 0)

            { document.getElementById('Ups').value = CalDeck };


            //var PDeck = ((Number(oResult1.data[0].Width) + Number(oResult1.data[0].Height)) * CalDeck) + 20
            //document.getElementById('Deckle').value = PDeck;
        }
    });
  //  $('#cmdCreate').hide();
    $("#chkChkQuality2").on("click", function () {
        if (chkChkQuality2.checked) {

            var length = document.getElementById('Length').value;
            var width = document.getElementById('Width').value;
            //     document.getElementById('Height').value = oResult1.data[0].Height;

            document.getElementById('Width').value = length;
            document.getElementById('Length').value = width;

          width   = document.getElementById('Width').value;
        length     = document.getElementById('Length').value;
            //var TLength = (Number(length))  / 10;    /////////=cut length
            //var ScQty = document.getElementById('POQuantity').value;
            ////document.getElementById('Ups').value = "0.5";
            //document.getElementById('CuttingSize').value = Number(TLength);
            //document.getElementById('TQty').value = Number(ScQty) * 2
            var UpVal = Math.floor(1320 / (Number(length)));
            document.getElementById('Ups').value = UpVal;
       var ReqPieces=     document.getElementById('PieceReq').value ;
            var TLength = (Number(width) * (Number(UpVal))+ 20)/10 ;
            document.getElementById('CuttingSize').value = Number(TLength);


            var UpsCalc = (Number(UpVal)) * (Number(UpVal))
            document.getElementById('Ups').value = UpsCalc;
            var PQty = document.getElementById('POQuantity').value;
            var ReqQty = Number(PQty) * Number(ReqPieces);
            var getCalcVal = Math.floor(Number(ReqQty) / Number(UpsCalc));
            document.getElementById('TQty').value = getCalcVal;
        }

        else {

            //var length = document.getElementById('Length').value;
            //var width = document.getElementById('Width').value;
            ////     document.getElementById('Height').value = oResult1.data[0].Height;


            //var TLength = (Number(length) * 2 + Number(width) * 2 + 50) / 10;      /////////=cut length
            //var ScQty = document.getElementById('POQuantity').value;

            //document.getElementById('CuttingSize').value = Number(TLength);
            //document.getElementById('TQty').value = Number(ScQty);


            //var DeckVal = document.getElementById('Deckle').value;
            //var CalDeck = Math.floor(132 / Number(DeckVal));
            //if (CalDeck != 0)

            //{ document.getElementById('Ups').value = CalDeck };

        }
    });

    //$('#MyTab a').click(function (e) {
    //    e.preventDefault();
    //    //$(this).tab('show')
    //    //alert($('.nav-tabs .active').text());
    //    if ($('.nav-tabs .active').text() == "Details Page 2") {
    //        //$('input:submit').show();
    //        $('#cmdCreate').hide();
    //    }
    //    else if 
    //        ($('.nav-tabs .active').text() == "Details Page 1") {
    //        $('#cmdCreate').show();
           
    //    }
    //})

   
 
}

function PrintChange(selectObj) {
    var selectIndex = selectObj.selectedIndex;
    var selectValue = selectObj.options[selectIndex].text;
    if (selectValue == "Select")
    {
        document.getElementById('PrintVal').value;
        //var PVal=  document.getElementById('PrintVal').value;

     
    }
    else
    {
        document.getElementById('PrintVal').value = selectValue;
    }
    
}
function DPChange(selectObj) {
    var selectIndex = selectObj.selectedIndex;
    var selectValue = selectObj.options[selectIndex].text;
    //document.getElementById('DPRScVal').value = selectValue;
    if (selectValue == "Select") {
        document.getElementById('DPRScVal').value;
        //var PVal = document.getElementById('DPRScVal').value;
            }
    else {
        document.getElementById('DPRScVal').value = selectValue;
    }
}
function beforeOneToManySaveHook(objectContext) {
    var bValidation = true;

    if ($("#TQty").val() =="") {
        $("#TQty").validationEngine('showPrompt', 'Enter Production Quantity', 'error', true)
        bValidation = false;
    }
    else if (!$.isNumeric($("#TQty").val())) {
        $("#TQty").validationEngine('showPrompt', 'Enter Numbers', 'error', true)
        bValidation = false;
    }


    //if ($("#dtRequiredDate").val() == "") {
    //    $("#dtRequiredDate").validationEngine('showPrompt', 'Enter Required Date', 'error', true)
    //    bValidation = false;
    //}

    if ($("#Fk_BoxID").val() == "") {
        $("#Fk_BoxID").validationEngine('showPrompt', 'Select Box Name And Proceed', 'error', true)
        bValidation = false;
    }

    return bValidation;


}

function setUpOrderBoxSearch(viewObject) {

    //cleanSearchDialog();
    EnqID = document.getElementById('Fk_Enquiry').value;


    if (EnqID == '') {
        $('#MainSearhContainer2').empty();
        $('#MainSearhContainer2').jtable({
            title: 'Box List',
            paging: true,
            pageSize: 5,
            selecting: true, //Enable selecting
            multiselect: false, //Allow multiple selecting
            selectingCheckboxes: true,
            actions: {
                listAction: '/BoxMaster/BoxNameListByFiter'
            },
            recordsLoaded: function (event, data) {
                $('.jtable-data-row').click(function () {
                    var row_id = $(this).attr('data-record-key');
                    $('#cmdBoxDone').click();
                });
            },
            fields: {
                Pk_BoxID: {
                    title: 'Id',
                    key: true,
                    list: true
                },
                BoxType: {
                    title: 'BoxType',

                    list: true
                },
                Pk_EnquiryChild: {
                    title: 'Pk_EnquiryChild',
                    width: '25%',
                    list: false
                },
                //PartNo: {
                //    title: 'PartNo',
                //    width: '25%'
                //},
                Name: {
                    title: 'Name',
                    width: '25%'
                },
                //Quantity: {
                //    title: 'Quantity',
                //    width: '25%'
                //},
            }
        });

    }
    else {
        $('#MainSearhContainer2').empty();
        $('#MainSearhContainer2').jtable({
            title: 'Box List',
            paging: true,
            pageSize: 5,
            selecting: true, //Enable selecting
            multiselect: false, //Allow multiple selecting
            selectingCheckboxes: true,
            actions: {
                listAction: '/Enquiry/EnquiryBoxDetails?Pk_Enquiry=' + EnqID
            },
            recordsLoaded: function (event, data) {
                $('.jtable-data-row').click(function () {
                    var row_id = $(this).attr('data-record-key');
                    $('#cmdBoxDone').click();
                });
            },
            fields: {
                Fk_BoxID: {
                    title: 'Id',
                    key: true,
                    list: true
                },
                Pk_EnquiryChild: {
                    title: 'Pk_EnquiryChild',
                    width: '25%',
                    list: false
                },
                BoxType: {
                    title: 'BoxType',
                    width: '25%',
                    list: true
                },
                //PartNo: {
                //    title: 'PartNo',
                //    width: '25%'
                //},
                Name: {
                    title: 'Name',
                    width: '25%'
                },
                Quantity: {
                    title: 'Quantity',
                    width: '25%'
                },
            }
        });
    }


    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearhContainer2').jtable('load', {
            BoxName: $('#txtdlgBoxName').val(),
            //Pk_MaterialIssueID: $('#Fk_Vendor').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdBoxSearch').click(function (e) {
        e.preventDefault();
        $('#MainSearhContainer2').jtable('load', {
            BoxName: $('#txtdlgBoxName').val(),
            //Pk_MaterialIssueID: $('#Fk_Vendor').val()
        });
    });

    $('#cmdBoxDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MainSearhContainer2').jtable('selectedRows');
        $('#Fk_BoxID').wgReferenceField("setData", rows[0].keyValue);
        //document.getElementById('Quantity').value = rows[0].data["Quantity"];

        BoxValue = rows[0].keyValue;
        document.getElementById('BoxID').value = rows[0].keyValue;
        if (EnqID == '') {
            document.getElementById('POQuantity').value = 0;
         QtyVal = 1;
        }
        else {
            document.getElementById('POQuantity').value = rows[0].data["Quantity"];
            QtyVal = document.getElementById('TQty').value;
        }
        document.getElementById('BoxType').value = rows[0].data["BoxType"];


        $("#searchBoxDialog1").modal("hide");


        checkValues();
   

      var  QtyVal = document.getElementById('TQty').value;

        $('#divPaperStock').jtable({
            title: 'Box Paper List',
            paging: true,
            pageSize: 15,
            sorting: false,
            defaultSorting: 'Name ASC',
            actions: {

                listAction: '/Order/OrderPaperDetails',
                //updateAction: ''
            },
            fields: {
                slno: {
                    title: 'slno',
                    key: true,
                    list: false
                },
                Pk_Material: {
                    title: 'Pk_Material',
                    key: false,
                    list: false
                },

                MName: {
                    title: 'Mat.Name'

                },
                StkQty: {
                    title: 'Ex.Stock',
                    key: false
                },
                PQty: {
                    title: 'Part Qty. in Box ',
                    list: false

                },
                PaperWt: {
                    title: 'Paper Wt./Box',
                    key: false,
                    list: false
                },

                PWt: {
                    title: 'Paper Wt./Box',
                    key: false,
                    list: true
                },
                PaperReq: {
                    title: 'Paper Req.',
                    display: function (data) {
                        if (data.record.PaperWt > 0) {
                            return (data.record.PaperWt * QtyVal * data.record.PQty);
                        }
                    }
                }

            }
        });

        $('#LoadRecordsButton').click(function (e) {
            e.preventDefault();

            $('#divPaperStock').jtable('load', {
                Fk_BoxID: $('#BoxID').val()
               //PartId: BoxID

            });
        });
        //Load all records when page is first shown
        $('#LoadRecordsButton').click();


    });
}


function setUpIssueMaterialSearch() {
    //Adding Search fields
    //var txtFieldMaterialName = "<input type='text' id='txtdlgMaterialName' placeholder='Material Name' class='input-large search-query'/>&nbsp;&nbsp;";

    //$("#dlgMaterialSearchFields").append(txtFieldMaterialName);
    $('#MaterialSearchContainer').jtable({
        title: 'Material List',
        paging: true,
        pageSize: 10,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/MaterialCategory/MaterialSearchListByFiter',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                //$(this).css('background', 'red');
                $('#cmdMaterialDone').click();
            });
        },
        fields: {
            Pk_Material: {
                title: 'Material Id',
                key: true,
                width:'2%'
            },
            Pk_PaperStock: {
                title: 'PKStock',
                key: false,
                width: '2%',
                list:false
            },
            RollNo: {
                title: 'Reel No',
                edit: false,
                width:'2%'
            },
            Name: {
                title: 'Material Name',
                edit: false
            },
            Color: {
                title: 'Shade',
                edit: false,
                width: '2%'
            },
            Quantity: {
                title: 'Ex.Stk.Qty',
                edit: false,
                width:'2%'
            },
          
            Category: {
                title: 'Cat',
                edit: false,
                list:false
            },
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
           Name: $('#txtMaterial').val(),
            //Pk_MaterialIssueID: $('#Fk_Vendor').val()
           MaterialCategory: $('#SType').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdMaterialSearch').click(function (e) {
        e.preventDefault();
       
        if (document.getElementById('SType').value == '')
        { alert("Select any one of the Material Type and Press Search"); }
        else
        {
            $('#MaterialSearchContainer').jtable('load', {
                Name: $('#txtMaterial').val(),
                MaterialCategory: $('#SType').val()

            });
        }
    });

    $('#cmdMaterialDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MaterialSearchContainer').jtable('selectedRows');
        //var rctr = $('#MaterialSearchContainer').jtable('selectedRows').TotalRecordCount;
        if (rows.length > 0) {
            $('#txtFk_Material').wgReferenceField("setData", rows[0].keyValue);
            document.getElementById('txtMaterial').value = rows[0].data.Pk_Material;
            if (rows[0].data["Category"] != "Paper") {
                document.getElementById('RollNo').value = "";
            }
            else {
                document.getElementById('RollNo').value = rows[0].data["RollNo"];
            }
            document.getElementById('Quantity').value = rows[0].data["Quantity"];
            document.getElementById('Pk_PaperStock').value = rows[0].data["Pk_PaperStock"];
            document.getElementById('Pk_Material').value = rows[0].data["Pk_Material"];
            document.getElementById('Color').value = rows[0].data["Color"];
        }
        _util.setDivPosition("divCreateMaterial", "block");
        _util.setDivPosition("divSearchMaterial", "none");
        $("#dtDeliveryDate").focus();
    });



}

function setUpProcessSearch() {
    //Adding Search fields

    var JCID=document.getElementById('Pk_JobCardID').value;

    $('#MaterialSearchContainer1').jtable({
        title: 'Process List',
        paging: true,
        pageSize: 10,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/ProcessMast/ProcessList?Pk_JobCardID=' + JCID,
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                //$(this).css('background', 'red');
                $('#cmdPDone').click();
            });
        },
        fields: {
            Pk_Process: {
                title: 'Process Id',
                key: true,
                width: '2%',
                list:false
            },
           
            ProcessName: {
                title: 'Name',
                edit: false,
                width: '2%'
            },
            //Name: {
            //    title: 'Material Name',
            //    edit: false
            //},
            //Color: {
            //    title: 'Shade',
            //    edit: false,
            //    width: '2%'
            //},
            //Quantity: {
            //    title: 'Ex.Stk.Qty',
            //    edit: false,
            //    width: '2%'
            //},

            //Category: {
            //    title: 'Cat',
            //    edit: false,
            //    list: false
            //},
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer1').jtable('load', {
            ProcessName: $('#txtProcess').val(),
            //Pk_MaterialIssueID: $('#Fk_Vendor').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdPSearch').click(function (e) {
        e.preventDefault();

        //if (document.getElementById('SType').value == '')
        //{ alert("Select any one of the Material Type and Press Search"); }
        //else
        //{
            $('#MaterialSearchContainer1').jtable('load', {
                Name: $('#txtMaterial').val(),
                ProcessName: $('#txtProcess').val()

            });
        //}
    });

    $('#cmdPDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MaterialSearchContainer1').jtable('selectedRows');
        //var rctr = $('#MaterialSearchContainer').jtable('selectedRows').TotalRecordCount;
        if (rows.length > 0) {
            $('#txtFk_Process').wgReferenceField("setData", rows[0].keyValue);
            document.getElementById('txtProcess').value = rows[0].data.Pk_Process;
            ProcessID = rows[0].data.Pk_Process;
            

            var JCID = document.getElementById('Pk_JobCardID').value

            _comLayer.parameters.add("JCID", JCID);
            oResult2 = _comLayer.executeSyncAction("JobCard/GetMax", _comLayer.parameters);
            if (oResult2[0] == undefined) {
                document.getElementById('Quantity').value = document.getElementById('TQty').value;
            }
            else {
                document.getElementById('Quantity').value = oResult2[0];
            }


        }


        _util.setDivPosition("divCreateMaterial", "block");
        _util.setDivPosition("divSearchMaterial", "none");
        $("#dtDeliveryDate").focus();


    });



}


function afterOneToManyDialogShow() {
    $('#dtJCDate').datepicker({
        autoclose: true
    });
    $('#dtDeliveryDate').datepicker({ autoclose: true });

    $('#cmdSearchMaterialsdetails').click(function (e) {
        e.preventDefault();

        $("#dataDialog").width(900);
        //$("#dataDialog").css("left", "60px");
        $("#divSearchMaterial").width(800);

        setUpIssueMaterialSearch();
        
        _util.setDivPosition("divSearchMaterial", "block");
        _util.setDivPosition("divCreateMaterial", "none");
    });

    $('#cmdSearchProcess').click(function (e) {
        e.preventDefault();

        $("#dataDialog").width(900);
        //$("#dataDialog").css("left", "60px");
        $("#divSearchMaterial").width(800);

        setUpProcessSearch();

        _util.setDivPosition("divSearchMaterial", "block");
        _util.setDivPosition("divCreateMaterial", "none");
    });
    $('#cmdSearchPaperdetails').click(function (e) {
        e.preventDefault();

        $("#dataDialog").width(900);
        //$("#dataDialog").css("left", "60px");
        $("#divSearchMaterial").width(800);
        $("#divSearchMaterial").height(1100);
        setUpPaperSearch();

        _util.setDivPosition("divSearchMaterial", "block");
        _util.setDivPosition("divCreateMaterial", "none");
    });

    //$('.clockpicker').clockpicker();

    var input = $('#input-a');
    input.clockpicker({
        autoclose: true
    });

    // Manual operations
    $('#button-a').click(function (e) {
        // Have to stop propagation here
        e.stopPropagation();
        input.clockpicker('show')
                .clockpicker('toggleView', 'minutes');
    });
    $('#button-b').click(function (e) {
        // Have to stop propagation here
        e.stopPropagation();
        input.clockpicker('show')
                .clockpicker('toggleView', 'hours');
    });


    $('#cmdUpd').click(function (e) {
        e.preventDefault();
        var i = 0;

        //_comLayer.parameters.add("CustId", CustCode);
        //oResult2 = _comLayer.executeSyncAction("JobCard/getBoxdata", _comLayer.parameters);

        var MatID = document.getElementById('BoxID').value;
        var PartID = document.getElementById('PartID').value;
        var StockVal = document.getElementById('RemainingQty').value;
        //var StockID = document.getElementById('txtPkPStock').value;
        //var PartId = document.getElementById('txtPartId').value;

        _comLayer.parameters.add("BoxID", MatID);
        _comLayer.parameters.add("PartID", PartID);
        _comLayer.parameters.add("StockVal", StockVal);
        //_comLayer.parameters.add("StockID", StockID);
        //_comLayer.parameters.add("PartId", PartId);


        _comLayer.executeSyncAction("BoxStockList/UpdateStk", _comLayer.parameters);

        document.getElementById('txtMatID').value = "";
        document.getElementById('txtPkPStock').value = "";
        document.getElementById('txtstock').value = "";

    });


}

function afterEditShow(viewObject) {
    bEditContext = true;
    curViewModel = viewObject.viewModel;
    $('#dtDeliveryDate').datepicker({ autoclose: true });

    // Based on State Can Disable the Save Button
    var status = viewObject.viewModel.data["Fk_Status"]();
    var Corr = viewObject.viewModel.data["Corr"]();
    if (Corr == "Yes")
    { $("#chkCorr").attr('checked', true); }
    else
    { $("#chkCorr").attr('checked', false); }

    var JCID = document.getElementById('Pk_JobCardID').value;

    var TopP = viewObject.viewModel.data["TopP"]();

    if (TopP == "Yes")
    { $("#chkTop").attr('checked', true); }
    else
    { $("#chkTop").attr('checked', false); }



    var Pasting = viewObject.viewModel.data["Pasting"]();
    if (Pasting == "Yes")
    { $("#chkPasting").attr('checked', true); }
    else
    { $("#chkPasting").attr('checked', false); }


    var Rotary = viewObject.viewModel.data["Rotary"]();

    if (Rotary == "Yes")
    { $("#chkRotary").attr('checked', true); }
    else
    { $("#chkRotary").attr('checked', false); }


    var Punching = viewObject.viewModel.data["Punching"]();
    if (Punching == "Yes")
    { $("#chkPunching").attr('checked', true); }
    else
    { $("#chkPunching").attr('checked', false); }



    var PrintingP = viewObject.viewModel.data["PrintingP"]();

    if (PrintingP == "Yes")
    { $("#chkPrinting1").attr('checked', true); }
    else
    { $("#chkPrinting1").attr('checked', false); }



    var Slotting = viewObject.viewModel.data["Slotting"]();
    if (Slotting == "Yes")
    { $("#chkSlotting").attr('checked', true); }
    else
    { $("#chkSlotting").attr('checked', false); }


    var Pinning = viewObject.viewModel.data["Pinning"]();
    if (Pinning == "Yes")
    { $("#chkPinning").attr('checked', true); }
    else
    { $("#chkPinning").attr('checked', false); }


    var Gumming = viewObject.viewModel.data["Gumming"]();

    if (Gumming == "Yes")
    { $("#chkGumming").attr('checked', true); }
    else
    { $("#chkGumming").attr('checked', false); }


    var Finishing = viewObject.viewModel.data["Finishing"]();

    if (Finishing == "Yes")
    { $("#chkFinishing").attr('checked', true); }
    else
    { $("#chkFinishing").attr('checked', false); }

    var Bundling = viewObject.viewModel.data["Bundling"]();

    if (Bundling == "Yes")
    { $("#chkBundling").attr('checked', true); }
    else
    { $("#chkBundling").attr('checked', false); }




    if (status == "Completed") {

        $(".modelControl").attr('disabled', 'disabled');
        $("#cmdAddSchedule").attr('disabled', true);
        //   $("#cmdCreate").attr('disabled', true);
    }

    var QCstatus = viewObject.viewModel.data["ChkQuality"]();



    if (QCstatus == "1") {


        $("#chQuality").attr('checked', true);

    }
    else
        if (QCstatus == "0") {


            $("#chQuality").attr('checked', false);

        }
    
    //var chkQ = document.getElementById('cqty').value;

   
    var JCstatus = viewObject.viewModel.data["Fk_Status"]();

    if (JCstatus == "4") {
        //if (chkQ == 1) {

        $("#chkCloseJC").attr('checked', true);
        $("#chkCancelJC").attr('checked', false);
    }
    else if (JCstatus == "11") {
        //if (chkQ == 1) {

        $("#chkCancelJC").attr('checked', true);
        $("#chkCloseJC").attr('checked', false);
    }
        //}
    else
        //if (chkQ == 0) {

    {
        $("#chkCloseJC").attr('checked', false);
        $("#chkCancelJC").attr('checked', false);
    }

        

    document.getElementById('PName').value = viewObject.viewModel.data["PName"]();

    $('#cmdAddressSearch').click(function (e) {
        e.preventDefault();
        CustCode = viewObject.viewModel.data["fk_Customer"]();
        setUpAddressSearch(viewObject);
        $("#searchAddressDialog").modal("show");
    });

    $("#cmdDeliverySchedule").click(function (e) {
        e.preventDefault();
        var OrderNumber = $("#hidPkOrder").val();
        window.open("/DeliverySchedule?Order=" + OrderNumber, "Schedule");
    });

    $('#dtJCDate').datepicker({
        autoclose: true
    });

    //$('#cmdOrderMSearch').click(function (e) {
    //    e.preventDefault();
    //    alert('hed');
    //    setUpOrderSearch(viewObject);
    //    $("#searchBoxDialog").modal("show");
    //});

    //$('#cmdPaper').click(function (e) {
    //    e.preventDefault();
    //  
    //    $("#searchPaperDialog").modal("show");
    //});
    
   
    if ($('#Machine').length > 0) {
        //alert($('#Machine').val());

        var MacID = $('#Machine').val();
        if (MacID != "") {
            _comLayer.parameters.add("Pk_Machine", MacID);
            var oResult1 = _comLayer.executeSyncAction("BoxMaster/SMachineDet", _comLayer.parameters);

            document.getElementById('Capacity_Length').value = oResult1.data[0].Capacity_Length;
        }
    };
    var Bid = document.getElementById('BoxID').value;
    _comLayer.parameters.add("Pk_BoxID", Bid);
    var PartID = document.getElementById('Fk_PartID').value;
    _comLayer.parameters.add("PartID", PartID);
    var oResult1 = _comLayer.executeSyncAction("BoxMaster/SBoxDet", _comLayer.parameters);
    document.getElementById('BoxType').value = oResult1.data[0].BType;

    //var oResult1 = _comLayer.executeSyncAction("BoxMaster/SBoxDet", _comLayer.parameters);
    //alert(oResult1.data[0].Length);
    //if (oResult1.) {
    document.getElementById('Length').value = oResult1.data[0].Length;
    document.getElementById('Width').value = oResult1.data[0].Width;
    document.getElementById('Height').value = oResult1.data[0].Height;

    document.getElementById('Deckle').value = oResult1.data[0].Deck;
    document.getElementById('BoardSize').value = oResult1.data[0].BoardArea;
    //document.getElementById('PartID').value = oResult1.data[0].PartID;
    document.getElementById('Ply').value = oResult1.data[0].Ply;



    var OrdID = $('#Fk_Order').val();
    if (OrdID != "") {
        _comLayer.parameters.add("Pk_Order", OrdID);
        var oResult1 = _comLayer.executeSyncAction("Order/OrdDet", _comLayer.parameters);
        //alert(oResult1.data[0].Fk_Enquiry);
        //alert(oResult1.data[0].CustomerName);
        //alert(oResult1.data[0].PONo);
        //$('#Fk_Enquiry').val() = oResult1.data[0].Fk_Enquiry;
        CustCode = oResult1.data[0].Fk_Customer;
        document.getElementById('Fk_Enquiry').value = oResult1.data[0].Fk_Enquiry;
        document.getElementById('txtfk_Customer').value = oResult1.data[0].CustomerName;
        document.getElementById('PO').value = oResult1.data[0].PONo;
        //document.getElementById('POQuantity').value = oResult1.data[0].Quantity;
    }
    
    $('#tlbMaterials').jtable({
        title: 'Interim Stock Updates',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {

            listAction: '/JobCard/UpdBoxStock?Fk_JobCardID=' + JCID,
            //updateAction: ''
        },
        fields: {
            ProcessName: {
                title: 'Process'
            },

            StockUpd: {
                title: 'Quantity',
                key: true,
            },
            //RollNo: {
            //    title: 'RollNo',
            //    key: true,
            //},
            //Pk_JobCardID: {
            //    title: 'JobCard',
            //    key: true,
            //},
            //Fk_Order: {
            //    title: 'Order',
            //    key: true,
            //},


        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();

        $('#tlbMaterials').jtable('load', {
            Fk_JobCardID: JCID,


        });
    });
    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    ////////////////////////////commented only to accommodate interim stock update ////////////////////////////
    //$('#tlbMaterials').jtable({
    //    title: 'Assigned Paper List',
    //    paging: true,
    //    pageSize: 10,
    //    defaultSorting: 'Name ASC',
    //    actions: {
    //        listAction: '/JobCard/Bounce',
    //        deleteAction: '',
    //        //updateAction: ''
    //    },
    //    fields: {
    //        slno: {
    //            title: 'slno',
    //            key: true,
    //            list: false
    //        },
    //        Fk_Material: {
    //            title: 'Material Id',
    //            key: true,
    //            list: false
    //        },
    //        Pk_PaperStock: {
    //            title: 'Pk PaperStock',
    //            key: false,
    //            list: false
    //        },
    //        Name: {
    //            title: 'Material Name'
    //        },
    //        Color: {
    //            title: 'Shade'
    //        },
    //        RollNo: {
    //            title: 'Reel No'
    //        },
    //        Quantity: {
    //            title: "Quantity"
    //        },


            

    //    }
    //});

    //var oSCuts = viewObject.viewModel.data.MaterialData();
    //viewObject.viewModel.data["JobDetails"] = ko.observableArray();
    //var i = 0;
    //ssum = 0;
    //while (oSCuts[i]) {
    //    var oCut = new IssuedMaterial();
    //    //     (Pk_JobCardDet, Name, Fk_Material, Pk_PaperStock, Color, RollNo, Quantity)
    //    oCut.load(oSCuts[i].Pk_JobCardDet, oSCuts[i].Name, oSCuts[i].Fk_Material, oSCuts[i].Pk_PaperStock, oSCuts[i].Color,oSCuts[i].RollNo, oSCuts[i].Quantity);
    //    viewObject.viewModel.data["JobDetails"].push(oCut);

    //    i++;
    //}

    //// configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "JobDetails", "JobCard", "_AddMaterial", function () { return new IssuedMaterial() });

    //configureOne2Many("#cmdAddPaper", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "JobDetails", "JobCard", "_AddPaper", function () { return new IssuedMaterial() });


    ////////////////////////////commented only to accommodate interim stock update ////////////////////////////
    bEditContext = true;

   
    //$('#wfTransDisplay').wgWorkFlowHistory({
    //    wgTag: "JobCard",
    //    viewModel: viewObject.viewModel
    //});

    //$('#wfButtons').wgWorkFlowButtons({
    //    wgTag: "JobCard",
    //    viewModel: viewObject.viewModel,
    //    stateField: "Fk_Status",
    //    belongsTo: "JobCard",
    //    pkField: "Pk_JobCardID"
    //});
   


    $('#tlbMaterials1').jtable({
        title: 'Process List',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/JobCard/Bounce',
            //deleteAction: '',
            //updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            ProcessDate: {
                title: 'Process Dt.'

            },
            ProcessName: {
                title: 'Name',
                key: false,
                list: true
            },
            Fk_ProcessID: {
                title: 'Pr.ID',
                list: false
            },
            PSTime: {
                title: 'Start Time',
                width: '3%'
                     
            },
            PETime: {
                title: 'End Time',
                width: '3%'

            },
            //Fk_JobCardID: {
            //    title: 'JobCardNo',
            //    key: false,
            //    list: true
            //},
            Quantity: {
                title: 'Plan Qty',
                width: '5%'
            },
            PQuantity: {
                title: 'Prdn.Qty',
                width: '5%',
            },
            WastageQty: {
                title: 'Wastage in Nos'

            },
            WQty: {
                title: 'Wastage in Kgs'

            },
            R1Wastage: {
                title: 'R1Wastage',
                list: false
            },
            R2Wastage: {
                title: 'R2Wastage',
                list: false
            },
            R3Wastage: {
                title: "R3Wastage",
                list: false
            },
            R4Wastage: {
                title: "R4Wastage",
                list: false
            },
            RemainingQty: {
                title: 'Rem.Qty'

            },
        
            BalanceQty: {
                title: 'Bal.Qty',
                display: function (data) {
                    if (data.record.Quantity > 0) {
            return (data.record.Quantity - data.record.PQuantity);
        }
    }
            },
        }
    });



    var oSCuts = viewObject.viewModel.data.ProcessData();
    viewObject.viewModel.data["Process"] = ko.observableArray();
    var i = 0;
    ssum = 0;

    var TSumWqty = 0;

    while (oSCuts[i]) {
        var oCut = new Process();
        //(Pk_ID, Name, Fk_ProcessID, txtProcess, WastageQty, ProcessDate, Quantity)
        oCut.load(oSCuts[i].Pk_ID, oSCuts[i].ProcessName, oSCuts[i].Fk_ProcessID, oSCuts[i].txtProcess, oSCuts[i].WastageQty, oSCuts[i].WQty, oSCuts[i].ProcessDate, oSCuts[i].Quantity, oSCuts[i].PQuantity, oSCuts[i].PSTime, oSCuts[i].PETime, oSCuts[i].ReasonWastage, oSCuts[i].RemainingQty);
        viewObject.viewModel.data["Process"].push(oCut);

        TSumWqty = TSumWqty+Number(oSCuts[i].WQty)

        i++;
    }

       
    configureOne2Many("#cmdAddProcess", '#tlbMaterials1', "#cmdSaveProcess", viewObject, "Process", "JobCard", "_AddProcess", function () { return new Process() });
      
        
    $('#tlbIssueMaterials').jtable({
        title: 'Paper Issue List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {

            listAction: '/MatIssue/IssueGetRec?Pk_JobCardID=' + JCID,
            //updateAction: ''
        },
        fields: {
            MaterialName: {
                title: 'MaterialName'
            },

            Quantity: {
                title: 'Quantity',
                key: true,
            },
            RollNo: {
                title: 'RollNo',
                key: true,
            },
            //Pk_JobCardID: {
            //    title: 'JobCard',
            //    key: true,
            //},
            //Fk_Order: {
            //    title: 'Order',
            //    key: true,
            //},
         

        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();

        $('#tlbIssueMaterials').jtable('load', {
            Pk_JobCardID: JCID,
             

        });
    });
    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

       
    _comLayer.parameters.add("JCNO", JCID);
    oResult2 = _comLayer.executeSyncAction("JobCard/GetIssueCount", _comLayer.parameters);
         
    if (oResult2>0)
 {       oResult1 = _comLayer.executeSyncAction("JobCard/GetDirectWaste", _comLayer.parameters);

    //alert(oResult1);

    document.getElementById('DWaste').value=oResult1;
    var DWastage = document.getElementById('DWaste').value;
    document.getElementById('ProWaste').value = TSumWqty;
    document.getElementById('TWastage').value = Number(TSumWqty) + Number(DWastage);
}


    $('#divPaperStock').jtable({
        title: 'Box Paper List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {

            listAction: '/Order/OrderPaperDetails',
            //updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            Pk_Material: {
                title: 'Pk_Material',
                key: false,
                list: false
            },

            MName: {
                title: 'Mat.Name'

            },
            PName: {
                title: 'Part Name'

            },
            PQty: {
                title: 'Part Qty. in Box '
            },
            StkQty: {
                title: 'Ex.Stock',
                key: false
            },
            PaperWt: {
                title: 'Paper Wt./Box',
                key: false,
                list: false
            },

            PWt: {
                title: 'Paper Wt./Box',
                key: false,
                list: true
            },
            PaperReq: {
                title: 'Paper Req.',
                display: function (data) {
                    if (data.record.PaperWt > 0) {
                        return (data.record.PaperWt * QtyVal * data.record.PQty);
                    }
                }
            }

        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();

        $('#divPaperStock').jtable('load', {
            Fk_BoxID: $('#BoxID').val(),
            PartID: $('#PartID').val()

        });
    });
    //Load all records when page is first shown
    $('#LoadRecordsButton').click();
  

        $("#dropzone").wgUpload({
            wgTag: "JobCard",
            upLoadUrl: "/FileUpload",
            property: "Documents",
            viewModel: viewObject.viewModel,
            fileLocation: _comLayer.curServer() + "/uploads/"
        });

    $('#cmdNext').click(function (e) {
        e.preventDefault();
        $('#MyTab a[href="#tbDocument"]').tab('show');
    })


    $('#cmdLoadBox').click(function (e) {
        e.preventDefault();
        var i=0;
     
        _comLayer.parameters.add("CustId", CustCode);
        oResult2 = _comLayer.executeSyncAction("JobCard/getBoxdata", _comLayer.parameters);
        
        for (i = 0; i < oResult2.length; i++) {
         var combo = document.getElementById("combo");

              var option = document.createElement("option");
        option.text = oResult2[i].Name;
        option.value = oResult2[i].Id;
            try {
                combo.add(option, null); //Standard 
            }

            catch (error) {
                combo.add(option); // IE only
            }
         }
    
    });
    $('#cmdUpdStock').click(function (e) {
        e.preventDefault();
        var i = 0;
        
        var JCId = document.getElementById('Pk_JobCardID').value;
        var MatID = document.getElementById('BoxID').value;
        var PartID = document.getElementById('Fk_PartID').value;
        var StockVal = document.getElementById('StkQty').value;
     
            var ItemID = document.getElementById('itemid').value;
      

        _comLayer.parameters.add("BoxID", MatID);
        _comLayer.parameters.add("PartID", PartID);
        _comLayer.parameters.add("StockVal", StockVal);
        _comLayer.parameters.add("Pk_JobCardID", JCId);
        _comLayer.parameters.add("ItemID", ItemID);

        if (StockVal > 0 && ItemID > 0) {
            _comLayer.executeSyncAction("JobCard/UpdateStk", _comLayer.parameters);
        }
        else {
            alert('Select BoxName/Enter Stock Value to Proceed');
            
            var parentOpener = window.opener;
            window.opener = null;
            window.open('JobCard');
            window.opener = parentOpener;
        }
        document.getElementById('StkQty').value = "";
        document.getElementById('itemid').value = "";

        alert('Stock Updated Successfully');
    });

    //cmdUpdStock
}
function BalQty()
{
    document.getElementById('BalanceQty').value = document.getElementById('Quantity').value - document.getElementById('PQuantity').value
    //this.data["BalanceQty"] = $('#Quantity').val() - $('#PQuantity').val();
}

function addCombo() {

    //_comLayer.parameters.add("Custval", CustCode);
    //oResult2 = _comLayer.executeSyncAction("JobCard/getBoxdata1", _comLayer.parameters);

    //alert(oResult2.TotalRecordCount);


    var textb = document.getElementById("txtCombo");
    var combo = document.getElementById("combo");

    var option = document.createElement("option");
    option.text = textb.value;
    option.value = textb.value;
    try {
        combo.add(option, null); //Standard 
    } catch (error) {
        combo.add(option); // IE only
    }
    textb.value = "";
}



function CallMe(selectObj) {
    //alert('CallMe');

    var selectIndex = selectObj.selectedIndex;
    var selectID = selectObj.value;
    var selectValue = selectObj.options[selectIndex].text;
    //document.getElementById('DPRScVal').value = selectValue;
    if (selectValue == "Select") {
        document.getElementById('itemname').value;
        document.getElementById('itemid').value;
        //var PVal = document.getElementById('DPRScVal').value;
    }
    else {
        document.getElementById('itemname').value =  selectValue;
        document.getElementById('itemid').value = selectID;
    }
}
function beforeOneToManyDataBind(property) {
    if (bEditContext) {
        _util.setDivPosition("divDeliveryStatus", "block");
    }

}

function beforeNewShow(viewObject) {
    $('#Fk_Order').wgReferenceField({
        keyProperty: "Fk_Order",
        displayProperty: "Pk_Order",
        loadPath: "Order/Load",
        viewModel: viewObject.viewModel
    });

}

function afterModelSaveEx(result) {
    alertify.set({
        labels: {
            ok: "Yes",
            cancel: "No"
        },
        buttonReverse: true

    });



   // strvalcheck = "http://www.smsjust.com/blank/sms/user/urlsmstemp.php?username=usermaedhaa&pass=maedhaa@123&senderid=MAEDHA&dest_mobileno=9900444553&tempid=72692&F1=1&F2=2&F3=3&F4=4&F5=5&F6=6&response=Y"

    strvalcheck = "http://123.63.33.43/blank/sms/user/urlsms.php?username=usermaedhaa&pass=maedhaa@123&senderid=MAEDHA&dest_mobileno=9844092320&message=" + "JobCard  " + result.Message + "&" + "response=Y"

    //strvalcheck = "http://123.63.33.43/blank/sms/user/urlsmstemp.php?username= maedhaa&pass=kap@user!123&senderid=MAEDHA&dest_mobileno=9900444553&message=" + "JobCard  " + result.Message + "&" + "response=Y"

 //window.open(strvalcheck, '_blank ', 'width=1,height=1');
   var x = window.open(strvalcheck, '_self ').close();
    //open(location, '_self').close();


//    window.close();
     
    //var bValidation = true;
    //if ($("#Fk_ShippingId").val() < 1) {
    //    $("#Fk_ShippingId").validationEngine('showPrompt', 'Select Shipping Address', 'error', true)
    //    bValidation = false;
    //}
    //return bValidation;
}

function setUpOrderSearch(viewObject) {
  
    //Adding Search fields
    var txtFieldOrdNo = "<input type='text' id='txtdlgOrdNo' placeholder='OrderNo' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldEnqNo = "<input type='text' id='txtdlgEnq' placeholder='EnqNo' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldCustomerName = "<input type='text' id='txtdlgCustomerName' placeholder='Customer Name' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldPoNo = "<input type='text' id='txtdlgPoNo' placeholder='PONo' class='input-large search-query'/>";
    var txtFieldSchNo = "<input type='text' id='txtdlgSchNo' placeholder='SchNo' class='input-large search-query'/>";

    $('#searchBoxDialog').empty;
    $('#searchBoxDialog').jtable({
        title: 'Orders List',
        paging: true,
        pageSize: 4,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/DelSchedule/ScheduleList'
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdOrderDone').click();
            });
        },
        fields: {
            OrderNo: {
                title: 'Order Sl.No.',
                key: true
            },
            CustPO: {
                title: 'Cust_PONo.'
            },
            EnquiryNo: {
                title: 'Enq No',
                edit: false,
                width:'2%'
            },           
            CustomerName: {
                title: 'Customer',
                width:'5%'
            },
            BoxName: {
                title: 'BoxName'
            },
            PName: {
                title: 'P.Name'
            },
            OrdQty: {
                title: 'OrdQty.'
            },
            Pk_DelID: {
                title: 'Sch.No'
            },
            DeliveryDate: {
                title: 'Del.Date'
            },
            Quantity: {
                title: 'Sch.Qty'
            },
            PartID: {
                title: 'PartId',
                list:false
            },
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#searchBoxDialog').jtable('load', {
            Pk_Order: $('#txtdlgOrdNo').val(),
            Fk_Enquiry: $('#txtdlgEnq').val(),
            CustomerName: $('#txtdlgCustomerName').val(),
            PONo: $('#txtdlgPoNo').val(),
            BoxName: $('#txtdlgBoxName').val()
        });

    });
    //$('#txtdlgEnquiryDate').datepicker({
    //    autoclose: true
    //});
    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    $('#cmdOrderSearch').click(function (e) {
        e.preventDefault();
        $('#searchBoxDialog').jtable('load', {
            Pk_Order: $('#txtdlgOrdNo').val(),
            Fk_Enquiry: $('#txtdlgEnq').val(),
            CustomerName: $('#txtdlgCustomerName').val(),
            PONo: $('#txtdlgPoNo').val(),
            BoxName: $('#txtdlgBoxName').val(),
            SchNo:$('#txtdlgSchNo').val()
        });
    });


    $('#cmdOrderDone').click(function (e) {
        e.preventDefault();
        var rows = $('#searchBoxDialog').jtable('selectedRows');
 
        //curViewModel.data["Fk_Order"] = ko.observable(rows[0].keyValue);
        $("#Fk_Order").val(rows[0].keyValue);
        $('#Fk_Enquiry').val(rows[0].data.EnquiryNo);
        //$('#txtfk_Customer').wgReferenceField("setData", rows[0].data.CustomerName);
        $("#txtfk_Customer").val(rows[0].data.CustomerName);
        $("#Fk_Schedule").val(rows[0].data.Pk_DelID);
        $("#PO").val(rows[0].data.CustPO);
        $("#POQuantity").val(rows[0].data.Quantity);
        $("#BoxID").val(rows[0].data.Pk_BoxID);
        document.getElementById('PName').type = 'text';
        $("#PName").val(rows[0].data.PName);
        $("#Fk_BoxID").val(rows[0].data.BoxName);
        $("#Fk_Enquiry").val(rows[0].data.EnquiryNo);
        $("#TQty").val(rows[0].data.Quantity);
        $("#PartID").val(rows[0].data.PartID);

       

        var QtyVal = document.getElementById('TQty').value;
        //var Part= document.getElementById('PartID').value;
        $("#searchBoxDialog").modal("hide");

        checkValues();


        $('#divPaperStock').jtable({
            title: 'Box Paper List',
            paging: true,
            pageSize: 15,
            sorting: false,
            defaultSorting: 'Name ASC',
            actions: {

                listAction: '/Order/OrderPaperDetails',
                //updateAction: ''
            },
            fields: {
                slno: {
                    title: 'slno',
                    key: true,
                    list: false
                },
                Pk_Material: {
                    title: 'Pk_Material',
                    key: false,
                    list: false
                },

                MName: {
                    title: 'Mat.Name'

                },
                PName: {
                    title: 'Part Name'

                },
                PQty: {
                    title: 'Part Qty. in Box '
                },
                StkQty: {
                    title: 'Ex.Stock',
                    key: false
                },
                PaperWt: {
                    title: 'Paper Wt./Box',
                    key: false,
                    list: false
                },

                PWt: {
                    title: 'Paper Wt./Box',
                    key: false,
                    list: true
                },
                PaperReq: {
                    title: 'Paper Req.',
                    display: function (data) {
                        if (data.record.PaperWt > 0) {
                            return (data.record.PaperWt * QtyVal * data.record.PQty);
                        }
                    }
                }

            }
        });

        $('#LoadRecordsButton').click(function (e) {
            e.preventDefault();

            $('#divPaperStock').jtable('load', {
                Fk_BoxID: $('#BoxID').val(),
                PartID: $('#PartID').val()

            });
        });
        //Load all records when page is first shown
        $('#LoadRecordsButton').click();

    });

}

function setUpPaperSearch(viewObject) {

    //Adding Search fields


    $('#MaterialSearchContainer').empty;
    $('#MaterialSearchContainer').jtable({
        title: 'Paper List',
        paging: true,
        pageSize: 12,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/PaperStockList/JCPaperList'
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                //$('#cmdMaterialDone').click();
                $('#cmdAddPaperToList').click();

                $('#MaterialSearchContainer').find(".jtable tbody tr:eq(" + $(this).index() + ")").css({ "background": "LightPink" });

            });
        },
        fields: {
            Pk_Material: {
                title: 'Id',
                key: true

            },
            Pk_PaperStock: {
                title: 'Pk_PaperStock',
               list:false

            },
            SName: {
                title: 'Mill',
                width: '35%'
            },
            RollNo: {
                title: 'RollNo',
                width: '5%'
            },

            MaterialName: {
                title: 'MaterialName',
                width: '25%'
            },
            GSM: {
                title: 'GSM',
                width: '10%'
            },
            BF: {
                title: 'BF',
                width: '10%'
            },
            Deckle: {
                title: 'Deckle',
                width: '10%'
            },
            Color: {
                title: 'Color',
                width: '5%'
            },
            Quantity: {
                title: 'Stock Qty',
                width: '25%'
            },
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            Fk_BoxID: $('#BoxID').val(),
            PartID: $('#PartID').val(),
            MaterialName: $('#txtMaterial').val(),
            GSM: $('#txtGSM').val(),
            BF: $('#txtBF').val(),
            Deckle: $('#txtDeckel').val(),
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdMaterialSearch').click(function (e) {
        e.preventDefault();

        $('#MaterialSearchContainer').jtable('load', {
            Fk_BoxID: $('#BoxID').val(),
            PartID: $('#PartID').val(),
            MaterialName: $('#txtMaterial').val(),
            GSM: $('#txtGSM').val(),
            BF: $('#txtBF').val(),
            Deckle: $('#txtDeckel').val(),
        });

    });

    $('#cmdMaterialDone').click(function (e) {
        e.preventDefault();
        var selPapRows = $('#MaterialSearchContainer').jtable('selectedRows');

        //var rctr = $('#MaterialSearchContainer').jtable('selectedRows').TotalRecordCount;
        if (selPapRows.length > 0) {
            $('#txtFk_Material').wgReferenceField("setData", selPapRows[0].keyValue);
            document.getElementById('txtMaterial').value = selPapRows[0].data.Pk_Material;

            document.getElementById('RollNo').value = selPapRows[0].data["RollNo"];

            document.getElementById('Quantity').value = selPapRows[0].data["Quantity"];
            document.getElementById('Pk_PaperStock').value = selPapRows[0].data["Pk_PaperStock"];
            document.getElementById('Pk_Material').value = selPapRows[0].data["Pk_Material"];

            var RowV = selPapRows[0].data["Quantity"];

            //document.getElementById('RowVal').value = Number(RowV);

        }

        var rows = "";
        this.paperRecList = [];
        //$('#listMain tbody').remove();
        paperRecList = getTableData($('#list'));

        var arrayLength = paperRecList.length;


        //$("tbody").append("<tr><td>" + numRows + "</td><td>" + $("#rollno").val() + "</td><td>1</td></tr>");


        for (var i = 0; i < arrayLength; i++) {

            //check if row for material id is already added, if so then the addition of row is skipped 
            numRows = $("#listMain tr").length;
            var rowexists = false;
            for (var j = 1 ; j < numRows ; j++) {
                var Fk_PaperStock = $("#listMain tr:nth-child(" + j + ") td:nth-child(3)").html();
                //var qty = $("#listMain  tr:nth-child(" + j + ") td:nth-child(5)").html();
                if (Fk_PaperStock == paperRecList[i][2]) {
                    //$("tr:nth-child(" + j + ") td:nth-child(4)").html(parseInt(qty) + paperRecList[i][4]);
                    rowexists = true;
                    break;
                }
            }

            if (!rowexists) {
                rows += "<tr><td>" + paperRecList[i][0]
                    + "</td><td style='display:none;'>" + paperRecList[i][1]
                    + "</td><td style='display:none;'>" + paperRecList[i][2]
                    + "</td><td >" + paperRecList[i][3]
                    //+ "</td><td class = 'count-me'>" + paperRecList[i][4]
                    + "</td><td >" + paperRecList[i][4]
                    + "</td><td><input type='button' value='Delete' onclick='deleteRowM(this)'> </td></tr>"

            }
        }

        $(rows).appendTo("#listMain tbody");
        //$("#Quantity").focus();
        //$('#cmdCreate').click();

        var btnSaveMaterial = document.getElementById("cmdSaveMaterial");
        // Programmatically click the save button
        btnSaveMaterial.click();

        _util.setDivPosition("divCreateMaterial", "block");
        _util.setDivPosition("divSearchMaterial", "none");
        _util.setDivPosition("tab", "none");

        makeSumryFromColumn();

    });

    $('#cmdAddPaperToList').click(function (e) {
        e.preventDefault();

        var rows = "";

        var selPaprows = $('#MaterialSearchContainer').jtable('selectedRows');

        if (selPaprows.length > 0) {

            rows += "<tr><td>" + selPaprows[0].data.MaterialName
                + "</td><td style='display:none;'>" + selPaprows[0].data["Pk_Material"]
                + "</td><td style='display:none;'>" + selPaprows[0].data["Pk_PaperStock"]
                + "</td><td>" + selPaprows[0].data["RollNo"]
                + "</td><td>" + selPaprows[0].data["Quantity"]
                + "</td><td>" + "<td><input type='button' value='Delete' onclick='deleteRow(this)'></td>"

            $(rows).appendTo("#list tbody");

            //$('#MaterialSearchContainer').jtable('deleteRows', selPaprows);
        }
         
    });

    //$('#cmdMaterialDone').click(function (e) {
    //    e.preventDefault();
    //    var rows = $('#MaterialSearchContainer').jtable('selectedRows');
    //    //var rctr = $('#MaterialSearchContainer').jtable('selectedRows').TotalRecordCount;
    //    if (rows.length > 0) {
    //        $('#txtFk_Material').wgReferenceField("setData", rows[0].keyValue);
    //        document.getElementById('txtMaterial').value = rows[0].data.Pk_Material;

    //        document.getElementById('RollNo').value = rows[0].data["RollNo"];

    //        document.getElementById('Quantity').value = rows[0].data["Quantity"];
    //        document.getElementById('Pk_PaperStock').value = rows[0].data["Pk_PaperStock"];
    //        document.getElementById('Pk_Material').value = rows[0].data["Pk_Material"];
    //        document.getElementById('Color').value = rows[0].data["Color"];
    //        var RowV = rows[0].data["Quantity"];

    //       // document.getElementById('RowVal').value = Number(RowV);



    //    }
    //    _util.setDivPosition("divCreateMaterial", "block");
    //    _util.setDivPosition("divSearchMaterial", "none");
    //    $("#Quantity").focus();

    //});

}

function makeSumryFromColumn() {
    var arr = [];
    var qty = [];
    var arrQty = [];
    //$("tr td:nth-child(1)").addClass('date');
    //$("#ItemsTable tr").each(function(){
    $("#listMain tr td:nth-child(1)").each(function () {
        var indx = arr.indexOf($(this).text());
        var qty;
        var str;
        //if ($.inArray($(this).text(), arr) == -1) {
        if (indx == -1) {
            arr.push($(this).text());
            str = $(this).parent().find("td:nth-child(5)").text(); //$(this).next().text();
            qty = isNaN(str) ? 0 : parseInt(str);
            arrQty.push(qty);
        }
        else {
            str = $(this).parent().find("td:nth-child(5)").text(); //$(this).next().text();
            qty = isNaN(str) ? 0 : parseInt(str);
            arrQty[indx] += qty;
        }
    });

    $("#listSumry > tbody").html("");

    var rows = "";
    for (var i = 0; i < arr.length; i++) {
        rows += "<tr><td>" + arr[i] + "</td>" + "<td>" + arrQty[i] + "</td></tr>";
    }

    $(rows).appendTo("#listSumry tbody");

}

function deleteRowM(r) {
    var i = r.parentNode.parentNode.rowIndex;
    document.getElementById("listMain").deleteRow(i);
    makeSumryFromColumn();
}

function deleteRow(r) {
    var i = r.parentNode.parentNode.rowIndex;
    document.getElementById("list").deleteRow(i);
}

function getTableData(table) {
    var data = [];
    table.find('tr:gt(0)').each(function (rowIndex, r) {
        var cols = [];
        $(this).find('td').each(function (colIndex, c) {
            cols.push(c.textContent);
        });
        data.push(cols);
    });
    return data;
}

function beforeModelSaveEx() {

    var viewModel = _page.getViewByName('New').viewModel;
    viewModel.data["JDate"] = $('#dtJCDate').val();
    viewModel.data["OrderDate"] = $('#dtPOrderDate').val();
    viewModel.data["Fk_Schedule"] = $('#Fk_Schedule').val();
    viewModel.data["InvDate"] = $('#dtInvDate').val();
    viewModel.data["Fk_Order"] = $('#Fk_Order').val();
    viewModel.data["Fk_BoxID"] = $('#BoxID').val();
    viewModel.data["DPRSc"] = $('#DPRScVal').val();
    viewModel.data["Printing"] = $('#PrintVal').val();
    viewModel.data["TotalQty"] = $('#TQty').val();
    viewModel.data["Machine"] = $('#MDeckel').val();
    viewModel.data["UpsVal"] = $('#Ups').val();
    viewModel.data["CutLength"] = $('#CuttingSize').val();

    this.paperRecList = [];
    paperRecList = getTableData($('#listMain'));

    var arrayLength = paperRecList.length;

    //viewModel.data["JobDetails"]().length = 0;

    for (var i = 0; i < arrayLength; i++) {

        //this.paperRec = JSON.parse(JSON.stringify(viewModel.data["JobDetails"]()[0]));
        this.paperRec = new IssuedMaterial();

        this.paperRec.data.Name = paperRecList[i][0];
        this.paperRec.data.Fk_Material = paperRecList[i][1];
        this.paperRec.data.Fk_PaperStock = paperRecList[i][2];
        this.paperRec.data.RollNo = paperRecList[i][3];
        this.paperRec.data.Quantity = paperRecList[i][4];

        viewModel.data["JobDetails"]().push(this.paperRec);

    }


    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel1.data["OrderDate"] = $('#dtPOrderDate').val();
    viewModel1.data["JDate"] = $('#dtJCDate').val();
       viewModel1.data["fk_Customer"] = CustCode;
    viewModel1.data["Fk_ShippingId"] = $('#Fk_ShippingId').val();
    viewModel1.data["RM_Consumed"] = $('#Quantity').val();
    //viewModel1.data["Fk_Status"] = $('#cjc').val();
    viewModel1.data["Fk_Schedule"] = $('#Fk_Schedule').val();
    //viewModel1.data["ChkQuality"] = $('#cqty').val();
    viewModel1.data["Fk_BoxID"] = $('#BoxID').val();
    viewModel1.data["TotalQty"] = $('#TQty').val();
    viewModel1.data["InvDate"] = $('#dtInvDate').val();
    viewModel1.data["UpsVal"] = $('#Ups').val();
    viewModel1.data["CutLength"] = $('#CuttingSize').val();
    if ($('#cjc').val() == 4)
    {  viewModel1.data["Fk_Status"] = $('#cjc').val();}
    else if($('#Canceljc').val()==11)
    { viewModel1.data["Fk_Status"] = $('#Canceljc').val(); }
}

function ReferenceFieldNotInitilized(viewModel) {


    $('#txtfk_Customer').wgReferenceField({
        keyProperty: "fk_Customer",
        displayProperty: "CustomerName",
        loadPath: "Customer/Load",
        viewModel: viewModel
    });


    $('#Fk_BoxID').wgReferenceField({
        keyProperty: "Fk_BoxID",
        displayProperty: "Name",
        loadPath: "BoxMaster/Load",
        viewModel: viewModel
    });

    $('#txtFk_Material').wgReferenceField({
        keyProperty: "Fk_Material",
        displayProperty: "Name",
        loadPath: "Material/Load",
        viewModel: viewModel
    });

    $('#txtFk_Process').wgReferenceField({
        keyProperty: "Fk_ProcessID",
        displayProperty: "ProcessName",
        loadPath: "ProcessMast/Load",
        viewModel: viewModel
    });

    if (viewModel.data != null) {
        $('#Fk_BoxID').wgReferenceField("setData", viewModel.data["Fk_BoxID"]);
        $('#txtFk_Material').wgReferenceField("setData", viewModel.data["Fk_Material"]);
        $('#txtFk_Process').wgReferenceField("setData", viewModel.data["Fk_ProcessID"]);
        //$('#txtFk_Process').wgReferenceField("setData", viewModel.data["ProcessName"]);


    }
}

function cleanSearchDialog() {

    //Clean the search dialog of previous bindings and prepare for this selected dialog
    $('#MainSearhContainer').empty();
    $('#MainSearhContainer').append("<div id='SearchContainer'></div>");
    $("#dlgSearchFields").empty();
    $('#cmdSearch').off();
    $('#cmdDone').off();

    $('#MainSearhContainer2').empty();
    //$('#searchBoxDialog').empty();
    $('#MaterialSearchContainer').empty();
    slistaction = '';
    sFldName = '';

}

function CheckInvDuplicate() {

    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("invno", $('#Invoice').val());
    oResult = _comLayer.executeSyncAction("JobCard/InvDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + "Invoice No Already added";
}



function checkValues() {
    _comLayer.parameters.clear();
    var BoxID = document.getElementById('BoxID').value;

    if (BoxID != 0 || BoxID > 0) {
        _comLayer.parameters.add("Pk_BoxID", BoxID);
    }
    else {

        _comLayer.parameters.add("Pk_BoxID", BoxValue);
    }

    //var PartID = document.getElementById('Fk_PartID').value;
    //_comLayer.parameters.add("PartID", PartID);
    var oResult1 = _comLayer.executeSyncAction("BoxMaster/SBoxDet", _comLayer.parameters);
    //alert(oResult1.data[0].Length);
    //if (oResult1.) {
    document.getElementById('Length').value = oResult1.data[0].Length;
    document.getElementById('Width').value = oResult1.data[0].Width;
    document.getElementById('Height').value = oResult1.data[0].Height;
    var BoxType = oResult1.data[0].BType;
    document.getElementById('Deckle').value = oResult1.data[0].Deck;
    document.getElementById('BoardSize').value = oResult1.data[0].BoardArea;
    document.getElementById('CuttingSize').value = oResult1.data[0].CuttingSize;
    document.getElementById('Ply').value = oResult1.data[0].Ply;
    document.getElementById('PieceReq').value = oResult1.data[0].Quantity;
    document.getElementById('Ups').value = oResult1.data[0].UpsVal;
    document.getElementById('MDeckel').value = "182";

    var DeckVal = oResult1.data[0].Deck;

    //if (1820 / Number(DeckVal) < 1) {
    //    var CalDeck =0.5 ;
    //}
    //else {
        var Wid=oResult1.data[0].Width;
        var Ht=oResult1.data[0].Height
   //    Ups = Math.Round(Convert.ToDecimal(1820 / (oPart.Height + oPart.Width)));
        DeckVal1 = Number(Wid) + Number(Ht)
        var CalDeck = Math.floor(1820 / Number(DeckVal1));
    //}


        if (CalDeck != 0)

    { document.getElementById('Ups').value = CalDeck };

        var PDeck = DeckVal;

    if (BoxType == 'Plain Box') {
        document.getElementById('Deckle').value = DeckVal;
        document.getElementById('CuttingSize').value = oResult1.data[0].CuttingSize ;
        document.getElementById('Ups').value = Math.floor(1820 / Number(oResult1.data[0].Width));
    }
    else {
        document.getElementById('Deckle').value = DeckVal;
        document.getElementById('CuttingSize').value = oResult1.data[0].CuttingSize;
    }
    
    
}




function orderDateValidate(field, rules, i, options) {
    if (bFromSave) {
        var isDateValid = _util.isDate(field.val());

        if (isDateValid.valid == false) {

            return "* " + isDateValid.Message;
        }

        if (!_util.dateGraterThanToday(field.val())) {
            return "* Order date cannot be later than today";
        }
    }
    bFromSave = false;
}

function deliveryDateValidate(field, rules, i, options) {

    var isDateValid = _util.isDate(field.val());

    if (isDateValid.valid == false) {
        return "* " + isDateValid.Message;
    }

    if (field.val() == undefined || $('#dtPOrderDate').val() == undefined) {
        return "* Please select the order date before adding the delivery schedule";
    }

    var orderDate = $('#dtPOrderDate').val();


    if (_util.dateGraterThan(field.val(), orderDate)) {
        return "* Delivery date cannot be earlier than order date";
    }

}

function showOnlyOpen() {
    $('#MainSearchContainer').jtable('load', {
        OnlyPending: $('#open').val()
    });
}

function showOnlyInProgress() {
    //document.getElementById('InProgress').value = 'Slotting' + ',' + 'Punching' + ',' + 'Printing' + ',' + 'Corrugation' + ',' + 'T.Paper';
    
    $('#MainSearchContainer').jtable('load', {
    
        OnlyPending: $('#InProgress').val()
    });
}

function showOnlyCompleted() {
    $('#MainSearchContainer').jtable('load', {
        OnlyPending: $('#Completed').val()
    });
}

function showOnlyCancelled() {
    $('#MainSearchContainer').jtable('load', {
        OnlyPending: $('#Cancelled').val()
    });
}

function showOnlyClosed() {
    $('#MainSearchContainer').jtable('load', {
        OnlyPending: $('#closed').val()
    });
}

function showAll() {
    $('#MainSearchContainer').jtable('load', {
        CustomerName: $('#CustomerName').val()
    });
}

function validatebuttonActionHook() {
    _comLayer.parameters.clear();
    _comLayer.parameters.add("Id", $("#Pk_Order").val());

    var oResult1 = _comLayer.executeSyncAction("/Order/Load", _comLayer.parameters);
    if (oResult1.data.deliverySchedule.length == 0)
        return true;
    else
        return false;
}



function showOnlyBoard() {
    $('#MaterialSearchContainer').jtable('load', {
        MaterialCategory: $('#Board').val()
    });
    document.getElementById('SType').value = "Board";
}
function showOnlyGum() {
    $('#MaterialSearchContainer').jtable('load', {
        MaterialCategory: $('#Gum').val()
    });
    document.getElementById('SType').value = "Gum";
}
function showOnlyInk() {
    $('#MaterialSearchContainer').jtable('load', {
        MaterialCategory: $('#Ink').val()
    });
    document.getElementById('SType').value = "Ink";
}


function showOnlyPaper() {
    $('#MaterialSearchContainer').jtable('load', {
        MaterialCategory: $('#Paper').val()
    });
    document.getElementById('SType').value = "Paper";
}

function showOnlyHoneycomb() {
    $('#MaterialSearchContainer').jtable('load', {
        MaterialCategory: $('#Honeycomb').val()
    });
    document.getElementById('SType').value = "Honeycomb";
}

function checkStatus() {
    //if ($('#chQuality').is(':checked') == true) {
    //    document.getElementById('cqty').value = 1;
    //}
    //else { document.getElementById('cqty').value = 0; }


    if ($('#chkCloseJC').is(':checked') == true) {
        document.getElementById('cjc').value = 4;
        $("#chkCancelJC").attr('checked', false);
    }
    else { document.getElementById('cjc').value = 1;
    $("#chkCancelJC").attr('checked', false);
 }

}
function checkCStatus() {


    if ($('#chkCancelJC').is(':checked') == true) {
        document.getElementById('Canceljc').value = 11;
        $("#chkCloseJC").attr('checked', false);
    }
    else {
        document.getElementById('Canceljc').value = 1;
        if ($('#chkCloseJC').is(':checked') == false) {
            $("#chkCloseJC").attr('checked', false);
        }
    }


}



function beforeOneToManyDeleteHook(objectContext) {
    var bValidation = true;

   // //TagNum = objectContext.data.TagNumber;
   // ItemQty = objectContext.data.Quantity;

   // ////////////////////////////////


   // var TotalWt = document.getElementById('TotalWt').value;

   // var tempQty = Number(TotalWt) - Number(ItemQty);

   //// document.getElementById('TAmount').value = 0;

   // document.getElementById('TotalWt').value = Number(tempQty);



    

    //////////////////////////////////
    bValidation = true;


    return bValidation;


}




function VisibleTrue() {
    document.getElementById('LWt1').type = 'text';
    document.getElementById('LWt2').type = 'text';
    document.getElementById('WQty').type = 'text';
    L1.style.visibility = 'visible';
    L2.style.visibility = 'visible';
    wt.style.visibility = 'visible';


}
function VisibleTOP() {
    document.getElementById('LWt1').type = 'text';
    //document.getElementById('LWt2').type = 'text';
    document.getElementById('WQty').type = 'text';
    L1.style.visibility = 'visible';

    document.getElementById('L1').innerHTML = 'Top Layer Wt.';
   // L1.text = 'hAI';
    //L2.style.visibility = 'visible';
    wt.style.visibility = 'visible';


}
function CalcWastage() {

    var BoxID = document.getElementById('BoxID').value;
    if (ProcessID == 2) {                     ///top paper
        VisibleTOP();
        //    alert(BoxID);
        _comLayer.parameters.add("Pk_BoxID", BoxID)
        //_comLayer.parameters.add("Pk_BoxID", Pk_BoxID);
        //OrdNo = row.record.Fk_Order;
        //_comLayer.parameters.add("OrdNo", OrdNo);
        oResult = _comLayer.executeSyncAction("BoxMaster/SBoxDet", _comLayer.parameters);

        var RecCtr = oResult.data.length;

        var x1 = 0;
        var x2 = 0;
        for (i = 0; i <= (RecCtr - 1) ; i++) {

            if (i == 0) {
                x1 = oResult.data[i].LayerWt;

                document.getElementById('LWt1').value = x1;

                if (x1 > 0) {
                    var TotWt = Number(x1);

                    var WastageQty = document.getElementById('WastageQty').value;
                    var TotWastageWt = Number(WastageQty) * Number(TotWt);


                    document.getElementById('WQty').value = TotWastageWt.toFixed(2);
                }

                $('#ReasonWastage').focus();

            }

        }
         
    }
    else if (ProcessID == 4) { //Rotary

        VisibleTOP();
        document.getElementById('L1').innerHTML = 'Board Wt.';
       
        _comLayer.parameters.add("Pk_BoxID", BoxID)
      
        oResult = _comLayer.executeSyncAction("BoxMaster/SBoxDet", _comLayer.parameters);

        var RecCtr = oResult.data.length;

        var x1 = 0;
        var x2 = 0;
        for (i = 0; i <= (RecCtr - 1) ; i++) {

            x1 = Number(x1) + Number(oResult.data[i].LayerWt);

        }
        document.getElementById('LWt1').value = x1;
        if (x1 > 0) {
            var TotWt = Number(x1);

            var WastageQty = document.getElementById('WastageQty').value;
            var TotWastageWt = Number(WastageQty) * Number(TotWt);


            document.getElementById('WQty').value = TotWastageWt.toFixed(2);
            $('#ReasonWastage').focus();
        }
    }
    else if (ProcessID == 5) { //Printing

        VisibleTOP();
        document.getElementById('L1').innerHTML = 'Board Wt.';

        _comLayer.parameters.add("Pk_BoxID", BoxID)

        oResult = _comLayer.executeSyncAction("BoxMaster/SBoxDet", _comLayer.parameters);

        var RecCtr = oResult.data.length;

        var x1 = 0;
        var x2 = 0;
        for (i = 0; i <= (RecCtr - 1) ; i++) {

            x1 = Number(x1) + Number(oResult.data[i].LayerWt);

        }
        document.getElementById('LWt1').value = x1;
        if (x1 > 0) {
            var TotWt = Number(x1);

            var WastageQty = document.getElementById('WastageQty').value;
            var TotWastageWt = Number(WastageQty) * Number(TotWt);


            document.getElementById('WQty').value = TotWastageWt.toFixed(2);
            $('#ReasonWastage').focus();
        }
    }
    else if (ProcessID == 7) { //Slotting

        VisibleTOP();
        document.getElementById('L1').innerHTML = 'Board Wt.';

        _comLayer.parameters.add("Pk_BoxID", BoxID)

        oResult = _comLayer.executeSyncAction("BoxMaster/SBoxDet", _comLayer.parameters);

        var RecCtr = oResult.data.length;

        var x1 = 0;
        var x2 = 0;
        for (i = 0; i <= (RecCtr - 1) ; i++) {

            x1 = Number(x1) + Number(oResult.data[i].LayerWt);

        }
        document.getElementById('LWt1').value = x1;
        if (x1 > 0) {
            var TotWt = Number(x1);

            var WastageQty = document.getElementById('WastageQty').value;
            var TotWastageWt = Number(WastageQty) * Number(TotWt);


            document.getElementById('WQty').value = TotWastageWt.toFixed(2);
            $('#ReasonWastage').focus();
        }
    }
    else if (ProcessID == 8) { //Punching

        VisibleTOP();
        document.getElementById('L1').innerHTML = 'Board Wt.';

        _comLayer.parameters.add("Pk_BoxID", BoxID)

        oResult = _comLayer.executeSyncAction("BoxMaster/SBoxDet", _comLayer.parameters);

        var RecCtr = oResult.data.length;

        var x1 = 0;
        var x2 = 0;
        for (i = 0; i <= (RecCtr - 1) ; i++) {

            x1 = Number(x1) + Number(oResult.data[i].LayerWt);

        }
        document.getElementById('LWt1').value = x1;
        if (x1 > 0) {
            var TotWt = Number(x1);

            var WastageQty = document.getElementById('WastageQty').value;
            var TotWastageWt = Number(WastageQty) * Number(TotWt);


            document.getElementById('WQty').value = TotWastageWt.toFixed(2);
            $('#ReasonWastage').focus();
        }
    }
    else if (ProcessID == 9) { //Pinning

        VisibleTOP();
        document.getElementById('L1').innerHTML = 'Board Wt.';

        _comLayer.parameters.add("Pk_BoxID", BoxID)

        oResult = _comLayer.executeSyncAction("BoxMaster/SBoxDet", _comLayer.parameters);

        var RecCtr = oResult.data.length;

        var x1 = 0;
        var x2 = 0;
        for (i = 0; i <= (RecCtr - 1) ; i++) {

            x1 = Number(x1) + Number(oResult.data[i].LayerWt);

        }
        document.getElementById('LWt1').value = x1;
        if (x1 > 0) {
            var TotWt = Number(x1);

            var WastageQty = document.getElementById('WastageQty').value;
            var TotWastageWt = Number(WastageQty) * Number(TotWt);


            document.getElementById('WQty').value = TotWastageWt.toFixed(2);
            $('#ReasonWastage').focus();
        }
    }
    else if (ProcessID == 10) { //Gumming

        VisibleTOP();
        document.getElementById('L1').innerHTML = 'Board Wt.';

        _comLayer.parameters.add("Pk_BoxID", BoxID)

        oResult = _comLayer.executeSyncAction("BoxMaster/SBoxDet", _comLayer.parameters);

        var RecCtr = oResult.data.length;

        var x1 = 0;
        var x2 = 0;
        for (i = 0; i <= (RecCtr - 1) ; i++) {

            x1 = Number(x1) + Number(oResult.data[i].LayerWt);

        }
        document.getElementById('LWt1').value = x1;
        if (x1 > 0) {
            var TotWt = Number(x1);

            var WastageQty = document.getElementById('WastageQty').value;
            var TotWastageWt = Number(WastageQty) * Number(TotWt);


            document.getElementById('WQty').value = TotWastageWt.toFixed(2);
            $('#ReasonWastage').focus();
        }
    }
    else if (ProcessID == 11) { //Finishing

        VisibleTOP();
        document.getElementById('L1').innerHTML = 'Board Wt.';

        _comLayer.parameters.add("Pk_BoxID", BoxID)

        oResult = _comLayer.executeSyncAction("BoxMaster/SBoxDet", _comLayer.parameters);

        var RecCtr = oResult.data.length;

        var x1 = 0;
        var x2 = 0;
        for (i = 0; i <= (RecCtr - 1) ; i++) {

            x1 = Number(x1) + Number(oResult.data[i].LayerWt);

        }
        document.getElementById('LWt1').value = x1;
        if (x1 > 0) {
            var TotWt = Number(x1);
            var TotWastageWt =0;
            var WastageQty = document.getElementById('WastageQty').value;
            if (Number(WastageQty)>0)
             TotWastageWt = Number(WastageQty) * Number(TotWt);
           
            else if((Number(WastageQty) == 0))
            TotWastageWt = Number(TotWt);


            document.getElementById('WQty').value = TotWastageWt.toFixed(2);
            $('#ReasonWastage').focus();
        }
    }
    else if (ProcessID == 13) {
        VisibleTrue();
        //    alert(BoxID);
        _comLayer.parameters.add("Pk_BoxID", BoxID)
        //_comLayer.parameters.add("Pk_BoxID", Pk_BoxID);
        //OrdNo = row.record.Fk_Order;
        //_comLayer.parameters.add("OrdNo", OrdNo);
        oResult = _comLayer.executeSyncAction("BoxMaster/SBoxDet", _comLayer.parameters);

        var RecCtr = oResult.data.length;

        var x1 = 0;
        var x2 = 0;
        for (i = 0; i <= (RecCtr - 1) ; i++) {

            if (i == 1) {
                x1 = oResult.data[i].LayerWt;

                document.getElementById('LWt1').value = x1;
            }
            if (i == 2) {
                x2 = oResult.data[i].LayerWt;
                document.getElementById('LWt2').value = x2;

                if (x1 > 0 && x2 > 0) {
                    var TotWt = Number(x1) + Number(x2);

                    var WastageQty = document.getElementById('WastageQty').value;
                    var TotWastageWt = Number(WastageQty) * Number(TotWt);


                    document.getElementById('WQty').value = TotWastageWt.toFixed(2);
                    $('#ReasonWastage').focus();
                }
            }

        }

    }
    else if (ProcessID == 14) {
        VisibleTrue();
        //    alert(BoxID);
        _comLayer.parameters.add("Pk_BoxID", BoxID)
        //_comLayer.parameters.add("Pk_BoxID", Pk_BoxID);
        //OrdNo = row.record.Fk_Order;
        //_comLayer.parameters.add("OrdNo", OrdNo);
        oResult = _comLayer.executeSyncAction("BoxMaster/SBoxDet", _comLayer.parameters);

        var RecCtr = oResult.data.length;

        var x1 = 0;
        var x2 = 0;
        for (i = 0; i <= (RecCtr - 1) ; i++) {

            if (i == 3) {
                x1 = oResult.data[i].LayerWt;

                document.getElementById('LWt1').value = x1;
            }
            if (i == 4) {
                x2 = oResult.data[i].LayerWt;
                document.getElementById('LWt2').value = x2;

                if (x1 > 0 && x2 > 0) {
                    var TotWt = Number(x1) + Number(x2);

                    var WastageQty = document.getElementById('WastageQty').value;
                    var TotWastageWt = Number(WastageQty) * Number(TotWt);


                    document.getElementById('WQty').value = TotWastageWt.toFixed(2);
                    $('#ReasonWastage').focus();
                }
            }

        }

    }


    else if (ProcessID == 15) {
        VisibleTrue();
        //    alert(BoxID);
        _comLayer.parameters.add("Pk_BoxID", BoxID)
        //_comLayer.parameters.add("Pk_BoxID", Pk_BoxID);
        //OrdNo = row.record.Fk_Order;
        //_comLayer.parameters.add("OrdNo", OrdNo);
        oResult = _comLayer.executeSyncAction("BoxMaster/SBoxDet", _comLayer.parameters);

        var RecCtr = oResult.data.length;

        var x1 = 0;
        var x2 = 0;
        for (i = 0; i <= (RecCtr - 1) ; i++) {

            if (i == 5) {
                x1 = oResult.data[i].LayerWt;

                document.getElementById('LWt1').value = x1;
            }
            if (i == 6) {
                x2 = oResult.data[i].LayerWt;
                document.getElementById('LWt2').value = x2;

                if (x1 > 0 && x2 > 0) {
                    var TotWt = Number(x1) + Number(x2);

                    var WastageQty = document.getElementById('WastageQty').value;
                    var TotWastageWt = Number(WastageQty) * Number(TotWt);


                    document.getElementById('WQty').value = TotWastageWt.toFixed(2);
                    $('#ReasonWastage').focus();
                }
            }

        }

    }



    else if (ProcessID == 16) {
        VisibleTrue();
        //    alert(BoxID);
        _comLayer.parameters.add("Pk_BoxID", BoxID)
        //_comLayer.parameters.add("Pk_BoxID", Pk_BoxID);
        //OrdNo = row.record.Fk_Order;
        //_comLayer.parameters.add("OrdNo", OrdNo);
        oResult = _comLayer.executeSyncAction("BoxMaster/SBoxDet", _comLayer.parameters);

        var RecCtr = oResult.data.length;

        var x1 = 0;
        var x2 = 0;
        for (i = 0; i <= (RecCtr - 1) ; i++) {

            if (i == 7) {
                x1 = oResult.data[i].LayerWt;

                document.getElementById('LWt1').value = x1;
            }
            if (i == 8) {
                x2 = oResult.data[i].LayerWt;
                document.getElementById('LWt2').value = x2;

                if (x1 > 0 && x2 > 0) {
                    var TotWt = Number(x1) + Number(x2);

                    var WastageQty = document.getElementById('WastageQty').value;
                    var TotWastageWt = Number(WastageQty) * Number(TotWt);


                    document.getElementById('WQty').value = TotWastageWt.toFixed(2);
                    $('#ReasonWastage').focus();
                }
            }

        }

    }


    else if (ProcessID == 17) {
        VisibleTrue();
        //    alert(BoxID);
        _comLayer.parameters.add("Pk_BoxID", BoxID)
        //_comLayer.parameters.add("Pk_BoxID", Pk_BoxID);
        //OrdNo = row.record.Fk_Order;
        //_comLayer.parameters.add("OrdNo", OrdNo);
        oResult = _comLayer.executeSyncAction("BoxMaster/SBoxDet", _comLayer.parameters);

        var RecCtr = oResult.data.length;

        var x1 = 0;
        var x2 = 0;
        for (i = 0; i <= (RecCtr - 1) ; i++) {

            if (i == 9) {
                x1 = oResult.data[i].LayerWt;

                document.getElementById('LWt1').value = x1;
            }
            if (i == 10) {
                x2 = oResult.data[i].LayerWt;
                document.getElementById('LWt2').value = x2;

                if (x1 > 0 && x2 > 0) {
                    var TotWt = Number(x1) + Number(x2);

                    var WastageQty = document.getElementById('WastageQty').value;
                    var TotWastageWt = Number(WastageQty) * Number(TotWt);


                    document.getElementById('WQty').value = TotWastageWt.toFixed(2);
                    $('#ReasonWastage').focus();
                }
            }

        }

    }




        /////Pasting Process


    else if (ProcessID == 18) {
        VisibleTrue();
        //    alert(BoxID);
        _comLayer.parameters.add("Pk_BoxID", BoxID)
        //_comLayer.parameters.add("Pk_BoxID", Pk_BoxID);
        //OrdNo = row.record.Fk_Order;
        //_comLayer.parameters.add("OrdNo", OrdNo);
        oResult = _comLayer.executeSyncAction("BoxMaster/SBoxDet", _comLayer.parameters);

        var RecCtr = oResult.data.length;

        var x1 = 0;
        var x2 = 0;
        for (i = 0; i <= (RecCtr - 1) ; i++) {

            if (i == 1) {
                x1 = oResult.data[i].LayerWt;

                document.getElementById('LWt1').value = x1;
            }
            if (i == 2) {
                x2 = oResult.data[i].LayerWt;
                document.getElementById('LWt2').value = x2;

                if (x1 > 0 && x2 > 0) {
                    var TotWt = Number(x1) + Number(x2);

                    var WastageQty = document.getElementById('WastageQty').value;
                    var TotWastageWt = Number(WastageQty) * Number(TotWt);


                    document.getElementById('WQty').value = TotWastageWt.toFixed(2);
                    $('#ReasonWastage').focus();
                }
            }

        }

    }

    else if (ProcessID == 19) {
        VisibleTrue();
        //    alert(BoxID);
        _comLayer.parameters.add("Pk_BoxID", BoxID)
        //_comLayer.parameters.add("Pk_BoxID", Pk_BoxID);
        //OrdNo = row.record.Fk_Order;
        //_comLayer.parameters.add("OrdNo", OrdNo);
        oResult = _comLayer.executeSyncAction("BoxMaster/SBoxDet", _comLayer.parameters);

        var RecCtr = oResult.data.length;

        var x1 = 0;
        var x2 = 0;
        for (i = 0; i <= (RecCtr - 1) ; i++) {

            if (i == 3) {
                x1 = oResult.data[i].LayerWt;

                document.getElementById('LWt1').value = x1;
            }
            if (i == 4) {
                x2 = oResult.data[i].LayerWt;
                document.getElementById('LWt2').value = x2;

                if (x1 > 0 && x2 > 0) {
                    var TotWt = Number(x1) + Number(x2);

                    var WastageQty = document.getElementById('WastageQty').value;
                    var TotWastageWt = Number(WastageQty) * Number(TotWt);


                    document.getElementById('WQty').value = TotWastageWt.toFixed(2);
                    $('#ReasonWastage').focus();
                }
            }

        }

    }
    else if (ProcessID == 20) {
        VisibleTrue();
        //    alert(BoxID);
        _comLayer.parameters.add("Pk_BoxID", BoxID)
        //_comLayer.parameters.add("Pk_BoxID", Pk_BoxID);
        //OrdNo = row.record.Fk_Order;
        //_comLayer.parameters.add("OrdNo", OrdNo);
        oResult = _comLayer.executeSyncAction("BoxMaster/SBoxDet", _comLayer.parameters);

        var RecCtr = oResult.data.length;

        var x1 = 0;
        var x2 = 0;
        for (i = 0; i <= (RecCtr - 1) ; i++) {

            if (i == 5) {
                x1 = oResult.data[i].LayerWt;

                document.getElementById('LWt1').value = x1;
            }
            if (i == 6) {
                x2 = oResult.data[i].LayerWt;
                document.getElementById('LWt2').value = x2;

                if (x1 > 0 && x2 > 0) {
                    var TotWt = Number(x1) + Number(x2);

                    var WastageQty = document.getElementById('WastageQty').value;
                    var TotWastageWt = Number(WastageQty) * Number(TotWt);


                    document.getElementById('WQty').value = TotWastageWt.toFixed(2);
                    $('#ReasonWastage').focus();
                }
            }

        }

    }
    else if (ProcessID == 21) {
        VisibleTrue();
        //    alert(BoxID);
        _comLayer.parameters.add("Pk_BoxID", BoxID)
        //_comLayer.parameters.add("Pk_BoxID", Pk_BoxID);
        //OrdNo = row.record.Fk_Order;
        //_comLayer.parameters.add("OrdNo", OrdNo);
        oResult = _comLayer.executeSyncAction("BoxMaster/SBoxDet", _comLayer.parameters);

        var RecCtr = oResult.data.length;

        var x1 = 0;
        var x2 = 0;
        for (i = 0; i <= (RecCtr - 1) ; i++) {

            if (i == 7) {
                x1 = oResult.data[i].LayerWt;

                document.getElementById('LWt1').value = x1;
            }
            if (i == 8) {
                x2 = oResult.data[i].LayerWt;
                document.getElementById('LWt2').value = x2;

                if (x1 > 0 && x2 > 0) {
                    var TotWt = Number(x1) + Number(x2);

                    var WastageQty = document.getElementById('WastageQty').value;
                    var TotWastageWt = Number(WastageQty) * Number(TotWt);


                    document.getElementById('WQty').value = TotWastageWt.toFixed(2);
                    $('#ReasonWastage').focus();
                }
            }

        }

    }
    else if (ProcessID == 22) {
        VisibleTrue();
        //    alert(BoxID);
        _comLayer.parameters.add("Pk_BoxID", BoxID)
        //_comLayer.parameters.add("Pk_BoxID", Pk_BoxID);
        //OrdNo = row.record.Fk_Order;
        //_comLayer.parameters.add("OrdNo", OrdNo);
        oResult = _comLayer.executeSyncAction("BoxMaster/SBoxDet", _comLayer.parameters);

        var RecCtr = oResult.data.length;

        var x1 = 0;
        var x2 = 0;
        for (i = 0; i <= (RecCtr - 1) ; i++) {

            if (i == 9) {
                x1 = oResult.data[i].LayerWt;

                document.getElementById('LWt1').value = x1;
            }
            if (i == 10) {
                x2 = oResult.data[i].LayerWt;
                document.getElementById('LWt2').value = x2;

                if (x1 > 0 && x2 > 0) {
                    var TotWt = Number(x1) + Number(x2);

                    var WastageQty = document.getElementById('WastageQty').value;
                    var TotWastageWt = Number(WastageQty) * Number(TotWt);


                    document.getElementById('WQty').value = TotWastageWt.toFixed(2);
                    $('#ReasonWastage').focus();
                }
            }

        }

    }
    else if (ProcessID == 23) {///Pasting_Top
        VisibleTOP();
        //    alert(BoxID);
        _comLayer.parameters.add("Pk_BoxID", BoxID)
        //_comLayer.parameters.add("Pk_BoxID", Pk_BoxID);
        //OrdNo = row.record.Fk_Order;
        //_comLayer.parameters.add("OrdNo", OrdNo);
        oResult = _comLayer.executeSyncAction("BoxMaster/SBoxDet", _comLayer.parameters);

        var RecCtr = oResult.data.length;

        var x1 = 0;
        var x2 = 0;
        for (i = 0; i <= (RecCtr - 1) ; i++) {

            if (i == 0) {
                x1 = oResult.data[i].LayerWt;

                document.getElementById('LWt1').value = x1;


                if (x1 > 0) {
                    var TotWt = Number(x1);

                    var WastageQty = document.getElementById('WastageQty').value;
                    var TotWastageWt = Number(WastageQty) * Number(TotWt);


                    document.getElementById('WQty').value = TotWastageWt.toFixed(2);
                    $('#ReasonWastage').focus();
                }

            }
            //if (i == 12) {
            //    x2 = oResult.data[i].LayerWt;
            //    document.getElementById('LWt2').value = x2;


            //}

        }

    }

    else if (ProcessID == 24) { //Pasting_Board

        VisibleTOP();
        document.getElementById('L1').innerHTML = 'Board Wt.';
        //    alert(BoxID);
        _comLayer.parameters.add("Pk_BoxID", BoxID)
        //_comLayer.parameters.add("Pk_BoxID", Pk_BoxID);
        //OrdNo = row.record.Fk_Order;
        //_comLayer.parameters.add("OrdNo", OrdNo);
        oResult = _comLayer.executeSyncAction("BoxMaster/SBoxDet", _comLayer.parameters);

        var RecCtr = oResult.data.length;

        var x1 = 0;
        var x2 = 0;
        for (i = 0; i <= (RecCtr - 1) ; i++) {

            x1 = Number(x1) + Number(oResult.data[i].LayerWt);

            //if (i == 0) {
            //    x1 = oResult.data[i].LayerWt;

            //    document.getElementById('LWt1').value = x1;




            //}
            //if (i == 12) {
            //    x2 = oResult.data[i].LayerWt;
            //    document.getElementById('LWt2').value = x2;


            //}

        }
        document.getElementById('LWt1').value = x1;
        if (x1 > 0) {
            var TotWt = Number(x1);

            var WastageQty = document.getElementById('WastageQty').value;
            var TotWastageWt = Number(WastageQty) * Number(TotWt);


            document.getElementById('WQty').value = TotWastageWt.toFixed(2);
            $('#ReasonWastage').focus();
        }
    }
    var PrdnQty = document.getElementById('PQuantity').value;
    var WastQty = document.getElementById('WastageQty').value;
    if (Number(PrdnQty) > 0 && Number(WastQty) > 0)
    { document.getElementById('RemainingQty').value = Number(PrdnQty) - Number(WastQty); }




}


//function checkDuplicate() {

//    if (objContextEdit == false) {
//        var i = 0;

//        while (curViewModel.data["Materials"]()[i]) {
//            if (objContext.data.Fk_Material == curViewModel.data["Materials"]()[i].data.Fk_Material && objContext.data.Fk_SiteId == curViewModel.data["Materials"]()[i].data.Fk_SiteId && $('#Fk_SiteIssueMasterId1').val() == curViewModel.data["Materials"]()[i].data.Fk_SiteIssueMasterId) {

//                return "* " + "Item Already added";
//            }
//            i++;
//        }
//    }
//}