﻿
function Process() {
    var PlQty = document.getElementById('TQty').value;
    this.data = new Object();
    this.data["ProcessName"] = "";
    this.data["Fk_ProcessID"] = "";
    this.data["dtDeliveredDate"] = "";
    this.data["WastageQty"] = "";
    this.data["Quantity"] = Number(PlQty);
    this.data["WQty"] = "";
    this.data["PSTime"] = "";
    this.data["PETime"] = "";
    this.data["RemainingQty"] = "";
    this.data["ReasonWastage"] = "";

    this.data["R1Wastage"] = "";
    this.data["R2Wastage"] = "";
    this.data["R3Wastage"] = "";
    this.data["R4Wastage"] = "";

   
  
    this.updateValues = function () {
        this.data["ProcessName"] = $('#txtFk_Process').val();
        if (this.data["ProcessName"] == "")
        {
            this.data["ProcessName"] = $('#ProcessName').val();
        }
     //   this.data["Fk_ProcessID"] = $('#Fk_ProcessID').val();
        this.data["Fk_ProcessID"] = $('#txtProcess').val();
        this.data["WastageQty"] = $('#WastageQty').val();
        this.data["Quantity"] = $('#Quantity').val();
        this.data["PQuantity"] = $('#PQuantity').val();
        this.data["WQty"] = $('#WQty').val();        
        this.data["PSTime"] = $('#PSTime').val();
        this.data["PETime"] = $('#PETime').val();
        this.data["RemainingQty"] = $('#RemainingQty').val();
        this.data["ReasonWastage"] = $('#ReasonWastage').val();

        this.data["R1Wastage"] = $('#R1Wastage').val();
        this.data["R2Wastage"] = $('#R2Wastage').val();
        this.data["R3Wastage"] = $('#R3Wastage').val();
        this.data["R4Wastage"] = $('#R4Wastage').val();

        this.data["BalanceQty"] = $('#Quantity').val() - $('#PQuantity').val();
        //data.record.Quantity - data.record.PQuantity
        this.data["ProcessDate"] = DateTime.Parse($('#dtDeliveredDate').ToString()).ToString("MM/dd/yyyy").val();


    
    };

    this.load = function (Pk_ID, ProcessName, Fk_ProcessID, txtProcess, WastageQty, WQty, ProcessDate, Quantity, PQuantity, PSTime, PETime, ReasonWastage, RemainingQty, R1Wastage, R2Wastage, R3Wastage, R4Wastage) {
        this.data["Pk_ID"] = Pk_ID;
        this.data["ProcessName"] = ProcessName;
        if (this.data["ProcessName"] == "") {
            this.data["ProcessName"] = $('#ProcessName').val();
        }
     //   this.data["Fk_ProcessID"] = Fk_ProcessID;
        this.data["Fk_ProcessID"] = txtProcess;
        this.data["WastageQty"] = WastageQty;
        this.data["WQty"] = WQty;
        this.data["ProcessDate"] = ProcessDate;
        this.data["Quantity"] = Quantity;
        this.data["PQuantity"] = PQuantity;
        this.data["PSTime"] = PSTime;
        this.data["PETime"] = PETime;
        this.data["ReasonWastage"] = ReasonWastage;
        this.data["RemainingQty"] = RemainingQty;
       
        this.data["R1Wastage"] = R1Wastage;
        this.data["R2Wastage"] = R2Wastage;
        this.data["R3Wastage"] = R3Wastage;
        this.data["R4Wastage"] = R4Wastage;
    };
}
