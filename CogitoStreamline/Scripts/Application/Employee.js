﻿//Name          : Employee Operations ----javascript files
//Description   : Contains the  Employee Operations  definition, every page in the application will be instance of this class with following
//                 1. Jtable Loading and Columns Descriptions 2.Button Click Events  
//Author        : CH.V.N. Ravi Kumar
//Date 	        : 26/10//2015
//Crh Number    : 
//Modifications : 

var curViewModel = null;

function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Employee List',
        paging: true,
        pageSize: 15,
        sorting: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Employee/EmployeeListByFiter',
            deleteAction: '',
            updateAction: '',
            createAction: ''
        },
        fields: {
            Pk_EmployeeId: {
                title: 'Id',
                key: true,
                width:'5%'
            },
            FirstName: {
                title: 'First Name'
            },
            LastName: {
                title: 'Last Name'
            },
            FatherName: {
                title: 'Father Name'
            },
            Address: {
                title:'Address'
            },
            City: {
                title: 'City'
            },
            Mobile: {
                title: 'Mobile'
            },
            Email: {
                title:'Email'
            }
        }
    });

    //Re-load records when Employee click 'load records' button.
 
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_EmployeeId:$('#txtSearchPkEmployeeId').val(),
            FirstName: $('#txtSearchFirstName').val(),
            LastName: $('#txtSearchLastName').val(),
            Mobile: $('#txtSearchMobile').val()
        });

    });


    //Load all records when page is first shown
    $('#LoadRecordsButton').click();


    $('#txtSearchPkEmployeeId').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Pk_EmployeeId: $('#txtSearchPkEmployeeId').val()
            });
        }
    });

    $('#txtSearchFirstName').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                FirstName: $('#txtSearchFirstName').val()
            });
        }
    });

    $('#txtSearchLastName').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                LastName: $('#txtSearchLastName').val()
            });
        }
    });

    $('#txtSearchMobile').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Mobile: $('#txtSearchMobile').val()
            });
        }
    });

    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });

   
    //Add addtional data sources (Combobox methods)
    _page.getViewByName('New').viewModel.addAddtionalDataSources("EmployeeDesignation", "getEmployeeDesignation", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("EmployeeDesignation", "getEmployeeDesignation", null);

    _page.getViewByName('New').viewModel.addAddtionalDataSources("EmployeeDepartment", "getEmployeeDepartment", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("EmployeeDepartment", "getEmployeeDepartment", null);

    _page.getViewByName('New').viewModel.addAddtionalDataSources("city", "getCity", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("city", "getCity", null);

    _page.getViewByName('New').viewModel.addAddtionalDataSources("State", "getState", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("State", "getState", null);


}

function beforeModelSaveEx() {
    var viewModel = _page.getViewByName('New').viewModel;
    
    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel1.data["IsActive"] = $('#IsActive').is(':checked');
}




function afterNewShow(viewObject) {

    curViewModel = viewObject.viewModel;
    $("#dropzone").wgUpload({
        wgTag: "Employee",
        upLoadUrl: "/ImageUpload",
        property: "Documents",
        viewModel: viewObject.viewModel,
        fileLocation: _comLayer.curServer() + "/EmployeeImages/"
    });
}

function afterEditShow(viewObject) {

    curViewModel = viewObject.viewModel;
    $("#dropzone").wgUpload({
        wgTag: "Employee",
        upLoadUrl: "/ImageUpload",
        property: "Documents",
        viewModel: viewObject.viewModel,
        fileLocation: _comLayer.curServer() + "/EmployeeImages/"
    });
}