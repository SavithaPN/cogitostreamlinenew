﻿var curViewModel = null;
var bFromSave = false;
var bEditContext = false;
var gMaterials = "";
var VendorID = "";
var TaxVal = 0;
var INVNO = 0;
var div = null;
var CashVal = 0;
var AccAmtVal = 0;
var ConVal = "";
var CustId = ""
var Box;
var OrdNo;

function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Invoice List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/InvBilling/BillingListByFiter',
            updateAction: '',
            createAction: ''
        },
        fields: {
            Pk_Invoice: {
                title: 'Inv.No',
                key: true,
                width: '3%'
            },

            //Invno: {
            //    title: 'InvNo'
            //},
            InvDate: {
                title: 'Inv Date',
                width: '3%'
            },
            Pk_Order: {
                title: 'Order No',
                width: '5%'
            },
            Fk_Customer: {
                title: 'Customer',
                width: '12%'
            },
            GrandTotal: {
                title: 'Net Value',
                width: '3%'
            },
            ED: {
                title: 'Bill Amt',
                width: '3%'
            },
            Print: {
                title: 'Original',
                width: '2%',
                //display: function (data) {
                //    return '<button type="button" onclick="alert(' + data.record.Invno + ')">Create PDF</button> ';
                display: function (row) {
                    var button = $("<i class='icon-printer'></i>");
                    $(button).click(function () {

                        INVNO = row.record.Pk_Invoice;
                        _comLayer.parameters.add("Invno", INVNO);
                        _comLayer.executeSyncAction("Billing_Others/InvReport", _comLayer.parameters);
                        var strval = "ConvertPDF/OthersOriginal-" + " " + INVNO + ".pdf"
                        window.open(strval, '_blank ', 'width=960,height=576');

                    });
                    return button;
                }

            },

            Select_A: {
                title: "Duplicate",
                display: function (row) {
                    var button1 = $("<button class='bnt'>Duplicate</button>");
                    $(button1).click(function () {
                        INVNO = row.record.Pk_Invoice;
                        _comLayer.parameters.add("Invno", INVNO);
                        _comLayer.executeSyncAction("InvBilling/InvReportDuplicate", _comLayer.parameters);
                        var strval = "ConvertPDF/DC-"+ " " +  INVNO + ".pdf"
                        window.open(strval, '_blank ', 'width=960,height=576');
                    });
                    return button1;
                }
            },
            Select_b: {
                title: "Extra Copy",
                display: function (row) {
                    var button1 = $("<button class='bnt'>Extra</button>");
                    $(button1).click(function () {
                        INVNO = row.record.Pk_Invoice;
                        _comLayer.parameters.add("Invno", INVNO);
                        _comLayer.executeSyncAction("InvBilling/InvReportExtra", _comLayer.parameters);
                        var strval = "ConvertPDF/BillExtra-" + " " + INVNO + ".pdf"
                        window.open(strval, '_blank ', 'width=960,height=576');
                    });
                    return button1;
                }
            }
        }
    });

    $('#dtDate').datepicker({
        autoclose: true
    });
    $('#dtOrdDate').datepicker({
        autoclose: true
    });


    $('#dtDate').datepicker({
        autoclose: true
    });

    $('#dtRemDate').datepicker({
        autoclose: true
    });
    //Re-load records when Order click 'load records' button.
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_Invoice: $('#InvoiceNumber').val(),
            Customer: $('#txtCustomer').val(),
            OrderNo: $('#OrderNo').val(),
            TxtFromDate: $('#TxtFromDate').val(),
            TxtToDate: $('#TxtToDate').val()
        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });

    //_page.getViewByName('New').viewModel.addAddtionalDataSources("Taxes", "getTax", null);
    //_page.getViewByName('Edit').viewModel.addAddtionalDataSources("Taxes", "getTax", null);

    $('#TxtFromDate').datepicker({ autoclose: true });
    $('#TxtToDate').datepicker({ autoclose: true });



    $("#searchCustomerDialog").width(1000);
    $("#searchCustomerDialog").css("top", "50px");
    $("#searchCustomerDialog").css("left", "300px");


    setUpCustomerSearch();
  
}
function resetOneToManyForm(property) {

    $("#Fk_TaxID").selected = -1;
    $("#TxtNetTotal").val("");
    $("#TxtTax").val("");
}
function afterNewShow(viewObject) {
    //$('#GrandTotal').val(0);
    //$('#NETVALUE').val(0);
    //$("#cmdCreate").attr('disabled', 'disabled');
    var dNow = new Date();
    document.getElementById('dtDate').value = (((dNow.getDate()) < 10) ? "0" + (dNow.getDate()) : (dNow.getDate())) + '/' + (((dNow.getMonth() + 1) < 10) ? "0" + (dNow.getMonth() + 1) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();


   
    $('#cmdCustomerSearch').click(function (e) {
        e.preventDefault();
        $("#searchCustomerDialog").modal("show");
    });

    $('#dtDate').datepicker({
        autoclose: true
    });
  
}

function setUpJobCardSearch(viewObject) {
    //Branch

    cleanSearchDialog();

    $("#srchCHeader").text("JobCard Search");

    //Adding Search fields
    var txtFieldJC = "<input type='text' id='txtdlgJobCard' placeholder='JobCardNo.' class='input-large search-query'/>&nbsp;&nbsp;";

    $("#dlgJobCardSearchFields").append(txtFieldJC);
    //  var CustName = document.getElementById('txtCustomer').value;

    var BoxID = document.getElementById('BoxID').value;

    $('#JobCardSearchContainer').jtable({
        title: 'JobCard List',
        paging: true,
        pageSize: 8,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/JobCard/JobListByFiter?BoxID=' + BoxID,
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdJobCardDone').click();
                $("#txtDescription").focus();
            });
        },
        fields: {
            Pk_JobCardID: {
                title: 'CardID',
                key: true,
                width: '2%'
            },
            JDate: {
                title: 'J.Card Date',
                width: '3%'
            },
            Fk_Order: {
                title: 'Cust_Order No',
                width: '2%'
            },
            SchNo: {
                title: 'Sch.No',
                width: '2%'
            },
            BoxName: {
                title: 'BoxName',
                width: '5%'
            },
            PartName: {
                title: 'PartName',
                width: '5%'
            }
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#JobCardSearchContainer').jtable('load', {
            Pk_JobCardID: $('#txtdlgJobCard').val()
        });
    });

    $('#LoadRecordsButton').click();


    $('#cmdJobCardSearch').click(function (e) {
        e.preventDefault();

        $('#JobCardSearchContainer').jtable('load', {
            Pk_JobCardID: $('#txtdlgJobCard').val()
        });
    });

    $('#cmdJobCardDone').click(function (e) {
        e.preventDefault();
        var rows = $('#JobCardSearchContainer').jtable('selectedRows');
        document.getElementById('Fk_JobCardID').value = rows[0].data["Pk_JobCardID"];
      
        //OrdNo = rows[0].data["Fk_Order"];
        $("#dataDialog").modal("hide");
        $("#txtDescription").focus();
    });




}



function afterOneToManyDialogShow() {

    $('#dtDate').datepicker({ autoclose: true });
    $('#dtInvDate').datepicker({ autoclose: true });
    $('#dtOrdDate').datepicker({ autoclose: true });
    $('#dtRemDate').datepicker({ autoclose: true });

    //$('#Fk_TaxID').change(function () {
    //    _comLayer.parameters.clear();

    //    _comLayer.parameters.add("Id", $('#Fk_TaxID').val());
    //    var oResult = _comLayer.executeSyncAction("/Taxx/Load", _comLayer.parameters);
    //    TaxVal = oResult.data["TaxValue"];
    //    document.getElementById('TxtTax').value = TaxVal;
    //    CalcNetAmt();

    //});


    $('#cmdBoxSearch').click(function (e) {
        e.preventDefault();

        _util.displayView("InvBilling", "_AddMaterial", "dataDialogArea");
        $('#dataDialogHeading').text("Search");
        $("#dataDialog").modal("show");
        setUpBoxSearch();

    });


}

function beforeOneToManySaveHook(objectContext) {
    var bValidation = true;

    if ($("#TxtQuantity").val() == "") {
        $("#TxtQuantity").validationEngine('showPrompt', 'Enter Quantity', 'error', true)
        bValidation = false;
    }
    else if (!$.isNumeric($("#TxtQuantity").val())) {
        $("#TxtQuantity").validationEngine('showPrompt', 'Enter Numbers', 'error', true)
        bValidation = false;
    }
    if (!$.isNumeric($("#Discount").val())) {
        $("#Discount").val(0);
        //$("#Discount").validationEngine('showPrompt', 'Enter Numbers', 'error', true)
        bValidation = false;
    }
    if ($("#Price").val() == "") {
        $("#Price").validationEngine('showPrompt', 'Enter Price', 'error', true)
        bValidation = false;
    }
    else if (!$.isNumeric($("#Price").val())) {
        $("#Price").validationEngine('showPrompt', 'Enter Numbers', 'error', true)
        bValidation = false;
    }
    if ($("#txtCustomer").val() == "") {
        $("#txtCustomer").validationEngine('showPrompt', 'Select Customer', 'error', true)
        bValidation = false;
    }
    if ($('#PayMt').val() == 0)
        $("#PayMt").validationEngine('showPrompt', 'Select Payment Mode', 'error', true)
    return bValidation;

}


function afterEditShow(viewObject) {

    curViewModel = viewObject.viewModel;



    $('#dtDate').datepicker({
        autoclose: true
    });

    $('#dtOrdDate').datepicker({
        autoclose: true
    });


    $('#dtInvDate').datepicker({
        autoclose: true
    });

    $('#dtRemDate').datepicker({
        autoclose: true
    });

    $('#divMaterialDetail').jtable({
        title: 'Bill Details',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/InvBilling/Bounce',
            updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            BoxID: {
                title: 'BoxID',
                key: false
            },
            Fk_PartID: {
                title: 'PartID',
                key: false
            },
            Description: {
                title: 'Description',
                key: false
            },
            HsnCode: {
                title: 'HsnCode',
                key: false
            },
            Quantity: {
                title: 'Quanity',

            },
            Price: {
                title: 'Price'

            },
            //Discount: {
            //    title: 'Discount'

            //},

            TotalAmount: {
                title: 'TotalAmount'
            },

            //Fk_TaxID: {
            //    title: 'Tax'
            //},
            //TaxValue: {
            //    title: 'TaxValue'
            //},
            NetAmount: {
                title: 'Net Amount',
                width: '20%'
            }

        }
    });
    if ($.trim($('#PayMode').val()) == "Cash") {
        $("#Cash").prop("checked", true);
    }
    else if ($.trim($('#PayMode').val()) == "Cheque") {
        $("#Cheque").prop("checked", true);
    }
    else if ($.trim($('#PayMode').val()) == "Card") {
        $("#Card").prop("checked", true);
    }
    else if ($.trim($('#PayMode').val()) == "NetBanking") {
        $("#NetBanking").prop("checked", true);
    }
    else {
        $("#Cash").prop("checked", false);
        $("#Card").prop("checked", false);
        $("#Cheque").prop("checked", false);
        $("#NetBanking").prop("checked", false);
    }



    if ($.trim($('#TaxType').val()) == "SGST") {
        $("#VAT").prop("checked", true);
    }
    else if ($.trim($('#TaxType').val()) == "IGST") {
        $("#CST").prop("checked", true);

    }



    if ($.trim($('#TaxType').val()) == "SGST") {
        document.getElementById('NETVALUE').value = Number($('#GrandTotal').val()) + ((Number($('#GrandTotal').val()) * 0.06) + (Number($('#GrandTotal').val()) * 0.06));
        document.getElementById('CGST').value = (Number($('#GrandTotal').val()) * 0.06);
        document.getElementById('ED').value = (Number($('#GrandTotal').val()) * 0.06);
    }
    else if ($.trim($('#TaxType').val()) == "IGST") {
        document.getElementById('NETVALUE').value = Number($('#GrandTotal').val()) +  (Number($('#GrandTotal').val()) * 0.12);
        document.getElementById('CGST').value = 0;
        document.getElementById('ED').value = 0;
    }
   


    //$('#FORM_H').click(function () {
    //    if ($(this).is(':checked')) {
    //       // document.getElementById('ED').value = document.getElementById('GrandTotal').value;
    //        document.getElementById('NETVALUE').value = document.getElementById('ED').value;
    //       // $("#VAT").prop("checked", false);
    //       // $("#CST").prop("checked", false);
    //    }

    //    else {
    //        document.getElementById('NETVALUE').value = Number($('#ED').val()) + (Number($('#ED').val() * 0.055));
    //    }
    //});

    var oSCuts = viewObject.viewModel.data.Inv_BillingDetails();
    viewObject.viewModel.data["InvoiceDetails"] = ko.observableArray();
    var i = 0;

    while (oSCuts[i]) {
        
        var oCut = new InvoiceBill();
        oCut.load(oSCuts[i].Pk_Inv_Details, oSCuts[i].Quantity, oSCuts[i].Price, oSCuts[i].Discount, oSCuts[i].NetAmount, oSCuts[i].Description, oSCuts[i].TotalAmount, oSCuts[i].BoxID, oSCuts[i].Fk_PartID, oSCuts[i].HsnCode, oSCuts[i].Fk_BoxID, oSCuts[i].PName, oSCuts[i].BName);
        viewObject.viewModel.data["InvoiceDetails"].push(oCut);
        i++;
    }

    configureOne2Many("#cmdAddMaterial", '#divMaterialDetail', "#cmdMaterialIndent", viewObject, "InvoiceDetails", "InvBilling", "NOTUSED", function () { return new InvoiceBill() }, "divCreateMaterialIndent");

    bEditContext = true;


}

function beforeOneToManyDataBind(property) {
    if (bEditContext) {
        _util.setDivPosition("divDeliveryStatus", "block");
    }

}

function beforeNewShow(viewObject) {
    $('#txtCustomer').wgReferenceField({
        keyProperty: "Pk_Customer",
        displayProperty: "CustomerName",
        loadPath: "Customer/Load",
        viewModel: viewModel
    });
    //$('#txtCustomerOrder').wgReferenceField({
    //    keyProperty: "Pk_Order",
    //    displayProperty: "Pk_Order",
    //    loadPath: "Order/Load",
    //    viewModel: viewModel
    //});
}


function setUpCustomerSearch() {
    //Enquiry

    //cleanSearchDialog();

    //Adding Search fields
    var txtFieldCustomerName = "<input type='text' id='txtdlgCustomerName' placeholder='Customer Name' class='input-large search-query'/>&nbsp;&nbsp;";
    //var txtFieldCustomerNam = "<input type='text' id='txtdlgCustomerNam' placeholder='Customer Name' class='input-large search-query'/>&nbsp;&nbsp;";
    //var txtFieldpart = "<input type='text' id='txtdlgPName' placeholder='Part Name' class='input-large search-query'/>&nbsp;&nbsp;";


    //var txtFieldCustomerNumber = "<input type='text' id='txtdlgCustomerContact' placeholder='Contact number' class='input-large search-query'/>&nbsp;&nbsp;";
    $("#dlgSearchFields").append(txtFieldCustomerName);  
    //$("#dlgSearchFields").append(txtFieldpart);
    //$("#dlgSearchFields").append(txtFieldCustomerNam);

    $('#SearchContainer').jtable({
        title: 'Customer List',
        paging: true,
        pageSize: 10,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Customer/CustomerListByFiter',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdDone').click();
            });
        },
        fields: {
            Pk_Customer : {
                title: 'Id',
                key: true,
                width: '2%',
                list:true
            },  CustomerName: {
                title: 'Customer Name',
                edit: false
            },
          
        //Pk_Order   : {
        //    title: 'Pk_Order',
        //    },  OrderDate: {
        //        title: 'Ord.Date',
        //    },
          
        //    MatName: {
        //        title: 'MatName',
        //    },
        //    Quantity: {
        //        title: 'Quantity',
        //    },
        //    CatName: {
        //        title: 'CatName',
        //    },

        }
    });


    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#SearchContainer').jtable('load', {
            CustomerName: $('#txtdlgCustomerName').val(),
            //CustomerName: $('#txtdlgCustomerNam').val(),
            //PName: $('#txtdlgPName').val(),
            //CustomerContact: $('#txtdlgCustomerContact').val()
        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    $('#cmdSearch').click(function (e) {
        e.preventDefault();
        $('#SearchContainer').jtable('load', {
            CustomerName: $('#txtdlgCustomerName').val(),
            //CustomerContact: $('#txtdlgCustomerContact').val()
        });
    });

    $('#cmdDone').click(function (e) {
        e.preventDefault();
        var rows = $('#SearchContainer').jtable('selectedRows');
        $('#txtCustomer').wgReferenceField("setData", rows[0].keyValue);
        //document.getElementById('Fk_OrderNo').value = rows[0].data["Pk_Order"];
        //document.getElementById('dtOrdDate').value = rows[0].data["OrderDate"];
        CustId = rows[0].keyValue;

        checkAddress();


        $("#searchCustomerDialog").modal("hide");
    });

}


function beforeModelSaveEx() {

    var viewModel = _page.getViewByName('New').viewModel;
    viewModel.data["InvDate"] = $('#dtDate').val();
    //viewModel.data["BOrderDated"] = $('#dtOrdDate').val();
    //viewModel.data["DNTimeofInv"] = $('#dtInvDate').val();
    //viewModel.data["DNTimeofRemoval"] = $('#dtRemDate').val();
    //viewModel.data["Fk_OrderNo"] = $('#Fk_OrderNo').val();
    //viewModel.data["Fk_BoxID"] = Box;
    //viewModel.data["InvDate"] = $('#dtInvDate').val();
    viewModel.data["Fk_Customer"] = CustId;
    //viewModel.data["PayMode"] = $('#PayMt').val();
    //viewModel.data["TaxType"] = $('#TaxType').val();
    //viewModel.data["Fk_PartID"] = $('#Fk_PartID').val();
    
    //viewModel.data["FORM_H"] = $('#FORM_H').is(':checked');
    //viewModel.data["FORM_CT3"] = $('#FORM_CT3').is(':checked');
    viewModel.data["StkID"] = $('#StkID').val();
    viewModel.data["StkQty"] = $('#StkQty').val();
    //viewModel.data["PartID"] = $('#PartID').val();
    
    viewModel.data["NETVALUE"] = $('#NETVALUE').val();

    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel1.data["InvDate"] = $('#dtDate').val();
    viewModel1.data["BOrderDated"] = $('#dtOrdDate').val();
    viewModel1.data["DNTimeofInv"] = $('#dtInvDate').val();
    viewModel1.data["DNTimeofRemoval"] = $('#dtRemDate').val();
    viewModel1.data["Fk_Customer"] = CustId;
    viewModel1.data["PayMode"] = $('#PayMt').val();
    viewModel1.data["TaxType"] = $('#TaxType').val();
    viewModel1.data["FORM_H"] = $('#FORM_H').is(':checked');
    viewModel1.data["FORM_CT3"] = $('#FORM_CT3').is(':checked');
    viewModel1.data["NETVALUE"] = $('#NETVALUE').val();
    viewModel1.data["Fk_OrderNo"] = OrdNo;
    viewModel1.data["InvDate"] = $('#dtInvDate').val();
    viewModel1.data["Fk_BoxID"] = Box;
    viewModel1.data["Fk_PartID"] = $('#Fk_PartID').val();
}


function ReferenceFieldNotInitilized(viewModel) {

    $('#txtCustomer').wgReferenceField({
        keyProperty: "Fk_Customer",
        displayProperty: "CustomerName",
        loadPath: "Customer/Load",
        viewModel: viewModel
    });


    if (viewModel.data != null) {

        //$('#txtCustomerOrder').wgReferenceField("setData", viewModel.data["Fk_OrderNo"]);
        $('#txtCustomer').wgReferenceField("setData", viewModel.data["Fk_Customer"]);
    }

    //$('#Fk_BoxID').wgReferenceField({
    //    keyProperty: "Fk_BoxID",
    //    displayProperty: "Name",
    //    loadPath: "Inv_Material/Load",
    //    viewModel: viewModel
    //});



    //if (viewModel.data != null) {
    //    $('#Fk_BoxID').wgReferenceField("setData", viewModel.data["Fk_BoxID"]);

    //}   
   
}



function cleanSearchDialog() {

    //Clean the search dialog of previous bindings and prepare for this selected dialog
    $('#MainSearhContainer').empty();
    $('#MainSearhContainer').append("<div id='SearchContainer'></div>");
    $("#dlgSearchFields").empty();
    $('#cmdSearch').off();
    $('#cmdDone').off();


}


function radioClass1(intval) {
    if (intval == 1) {


        document.getElementById('TaxType').value = "SGST";


        if (Number($('#ED').val()) > 0) {
            document.getElementById('NETVALUE').value = 0;
            document.getElementById('NETVALUE').value = Number($('#ED').val()) + (Number($('#ED').val() * 0.06));
        }

        else if (Number($('#ED').val()) == 0) {

            document.getElementById('ED').value = Number($('#GrandTotal').val() * 0.06);
            var ed = document.getElementById('ED').value;
            document.getElementById('CGST').value = Number($('#GrandTotal').val() * 0.06);
            var cgst = document.getElementById('CGST').value;
            document.getElementById('NETVALUE').value = Number($('#GrandTotal').val()) + Number($('#ED').val()) + Number($('#CGST').val());
            //  NETVALUE
        }

    }
    else if (intval == 2) {

        document.getElementById('TaxType').value = "IGST";


        //   FORM_CT3.style.visibility = 'visible';
        // FORM_C.style.visibility = 'visible';
        //FORM_H.style.visibility = 'hidden';

        //  fct3.style.visibility = 'visible';
        //    fc.style.visibility = 'visible';
        //fh.style.visibility = 'hidden';



        if (Number($('#GrandTotal').val()) > 0) {

        //    document.getElementById('NETVALUE').value = 0;
        //    document.getElementById('NETVALUE').value = Number($('#ED').val()) + (Number($('#ED').val() * 0.02));
        //}
            var TaxCal = Number($('#GrandTotal').val()) * 0.12;
            var TAmt= Number($('#GrandTotal').val())+Number(TaxCal);
        //else if (Number($('#ED').val()) == 0) {
            //document.getElementById('ED').value = Number($('#GrandTotal').val() * 0.12);
            //var ed = document.getElementById('ED').value;
            //document.getElementById('CGST').value = 0;

            document.getElementById('NETVALUE').value = Number(TAmt);
        }
        //  NETVALUE}

    }

    //document.getElementById('NETVALUE').value = 0;
    //// document.getElementById('NETVALUE').value = Number($('#ED').val()) + (Number($('#ED').val() * 0.02));
    //document.getElementById('NETVALUE').value = (Number($('#GrandTotal').val()) + Number($('#GrandTotal').val() * 0.06) + Number($('#GrandTotal').val() * 0.02));
}


function radioClass(str1) {
    if (str1 == "Cash") {
        document.getElementById('PayMt').value = "Cash";

    }
    else if (str1 == "Card") {
        document.getElementById('PayMt').value = "Card";
    }
    else if (str1 == "Cheque") {
        document.getElementById('PayMt').value = "Cheque";
    }
    else if (str1 == "NetBanking") {
        document.getElementById('PayMt').value = "NetBanking";
    }
}


function CalcAmt() {
    if (Number($('#Tax').val()) > 0) {

        document.getElementById('BillVal').value = (Number($('#GrandTotal').val()) + (Number($('#GrandTotal').val())) * ((Number($('#Tax').val())) / 100) * 2);

    }

 


    }



function CalcNetAmt() {

    if (Number($('#TxtQuantity').val()) > 0 && Number($('#Price').val()) > 0) {
        //var AfterDiscAmt = (Number($('#TxtQuantity').val()) * Number($('#Price').val())) - ((Number($('#TxtQuantity').val()) * Number($('#Price').val()) * (Number($('#Discount').val()) / 100)));
        var AfterDiscAmt = (Number($('#TxtQuantity').val()) * Number($('#Price').val()));
        var TaxCal = Number($('#TxtTax').val()) / 100;
        var TaxAmt = Number(AfterDiscAmt * TaxCal);
        var NetAmt = Number(AfterDiscAmt) + Number(TaxAmt);
        document.getElementById('TxtNetTotal').value = AfterDiscAmt;
    }
    else

        if (Number($('#TxtQuantity').val()) > 0 && Number($('#Price').val()) > 0) {
            var AfterDiscAmt = (Number($('#TxtQuantity').val()) * Number($('#Price').val()));

            var TaxCal = Number($('#TxtTax').val()) / 100;
            var TaxAmt = Number(AfterDiscAmt * TaxCal);
            var NetAmt = Number(AfterDiscAmt) + Number(TaxAmt);
            document.getElementById('TxtNetTotal').value = AfterDiscAmt;

        }

        else
            if (Number($('#TxtQuantity').val()) > 0 && Number($('#Price').val()) > 0) {
                var AfterDiscAmt = (Number($('#TxtQuantity').val()) * Number($('#Price').val()));
                var TaxAmt = Number(AfterDiscAmt);
                var NetAmt = Number(AfterDiscAmt);
                document.getElementById('TxtNetTotal').value = AfterDiscAmt;

            }
}

function setUpBoxSearch(viewObject) {


    var txtFieldBoxName = "<input type='text' id='txtdlgBoxName' placeholder='ItemName' class='input-large search-query'/>&nbsp;&nbsp;";

    $("#dlgCustomerSearchFields").append(txtFieldBoxName);
    var OrdNo = document.getElementById('Fk_OrderNo').value;
    $('#MaterialSearchContainer').jtable({
        title: 'Items Stock List',
        paging: true,
        pageSize: 5,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/Order/OrderForOtherBills',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdMaterialDone').click();
            });
        },
        fields: {

            Pk_Material: {
                title: 'Pk_Material',
                key: true,
                list: false
            },
            MatName: {
                title: 'Mat.Name',
                edit: false
            },
            Quantity: {
                title: 'Stk.Qty',

            },

            CatName: {
                title: 'Cat.Name'
            },
            Pk_InwardDet: {
                title: 'Pk_InwardDet',
                list:false
            },
        }
    });


    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            MatName: $('#txtdlgBoxName').val()//,   

        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    $('#cmdMaterialSearch').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            MatName: $('#txtdlgBoxName').val()//,
            //PartNo: $('#txtdlgPartNo').val()

        });
    });


    $('#cmdMaterialDone').click(function (e) {

        e.preventDefault();
        var rows = $('#MaterialSearchContainer').jtable('selectedRows');
        //$('#Fk_BoxID').wgReferenceField("setData", rows[0].keyValue);
        document.getElementById('Fk_BoxID').value = rows[0].data["Pk_Material"];
        document.getElementById('BName').value = rows[0].data["MatName"];
     //   document.getElementById('PName').value = rows[0].data["PName"];
        //Box = rows[0].data["BoxID"];
        document.getElementById('StkQty').value = rows[0].data["Quantity"];
        //document.getElementById('Fk_PartID').value = rows[0].data["PartID"];
        //document.getElementById('StkID').value = rows[0].data["StockID"];
        $("#dataDialog").modal("hide");
        $("#QuantityBox").focus();
    });

}




function checkAddress() {
    $('#tlbCustShipping').jtable({
        title: 'Shipping List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {

            listAction: '/Customer/CustomerShipping?Pk_Customer=' + CustId,
            //updateAction: ''
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                document.getElementById('DeliveryName').value = $(this).data("record").ShippingCustomerName;
                var addval = $(this).data("record").ShippingAddress + " , " + $(this).data("record").ShippingCity + " , " + $(this).data("record").ShippingState + " , " + $(this).data("record").ShippingPincode;
                document.getElementById('DeliveryAdd1').value = addval;
            });
        },
        fields: {
            ShippingCustomerName: {
                title: 'Name',
                key: false
            },
            ShippingAddress: {
                title: 'Address',
                width: '20%'
            },
            ShippingCity: {
                title: 'City',
                width: '20%'
            },
            ShippingState: {
                title: 'State',
                width: '20%'
            },
            ShippingPincode: {
                title: 'Pincode',
                width: '20%'
            },
            ShippingMobile: {
                title: 'Mobile',
                width: '20%'
            },
            ShippingLandLine: {
                title: 'Land line',
                width: '20%'
            },
            ShippingEmailId: {
                title: 'Email',
                width: '20%'

            }
        }
       
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();

        $('#tlbCustShipping').jtable('load', {
            Pk_Customer: CustId,


        });
    });
    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

}