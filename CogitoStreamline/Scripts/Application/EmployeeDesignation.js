﻿//Name          : Employee Designation Operations ----javascript files 
//Description   : Contains the  Employee Designation Operations  definition with following
//                 1. Jtable Loading and Columns Descriptions 2.Button Click Events  3. Check Designation Duplicate(duplication checking)
//Author        : CH.V.N.Ravi Kumar
//Date 	        : 21/10/2015
//Crh Number    : 
//Modifications : 

function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Designation List',
        paging: true,
        pageSize: 15,
        sorting: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/EmployeeDesignation/EmployeeDesignationListByFiter',
            deleteAction: '',
            updateAction: '',
            createAction: ''
        },
        fields: {
            Pk_EmployeeDesignation: {
                title: 'Id',
                key: true,
                width:'5%'

            },
            DesignationName: {
                title: 'Designation Name'
            }
            
        }
    });

    //Re-load records when EmployeeDesignation click 'load records' button.
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_EmployeeDesignationId: $('#txtSearchPkEmployeeDesignationId').val(),
            EmployeeDesignationName: $('#txtSearchDesignationName').val()
        });

    });

    $('#txtSearchPkEmployeeDesignationId').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Pk_EmployeeDesignationId: $('#txtSearchPkEmployeeDesignationId').val()
            });
        }
    });

    $('#txtSearchDesignationName').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                EmployeeDesignationName: $('#txtSearchDesignationName').val()
            });
        }
    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });
}

function CheckEmployeeDesignationDuplicate() {
    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("DesignationName", $('#DesignationName').val());
    oResult = _comLayer.executeSyncAction("EmployeeDesignation/EmployeeDesignationDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + "Designation Already added";
}
