﻿

var curViewModel = null;
var curEdit = false;
var VendorID;
var gtot;
var EDVal;
var IndentNo;
var QCValue;
var ssum = 0;
var rowcount = 0;
var tempamt = 0;
var GSM = 0;
var BF = 0;
var Dec = 0;
var MCat = "";
var CustomerID = 0;

function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Inward List',
        paging: true,
        pageSize: 10,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/JW_Inwd/MaterialInwardListByFiter',
            //deleteAction: '',
            //updateAction: '',
            createAction: ''
        },
        fields: {
            Pk_Inward: {
                title: 'Inward No',
                key: true,
                list: true,
                width: '4%'
            },
       
            Inward_Date: {
                title: 'Inwd.Date',
                width: '4%'
            },
            //PONo: {
            //    title: 'PONo',
                
            //    list: true,
            //    width: '4%'
            //},
            VendorName: {
                title: 'Vendor',
                width: '10%'
            },
            //IndentNo: {
            //    title: 'IndentNo',

            //    list: true,
            //    width: '4%'
            //},
            //Pur_InvDate: {
            //    title: 'Pur_InvDate',
            //    list: true,
            //    width: '4%'
            //},
            //Pur_InvNo: {
            //    title: 'Pur_InvNo',
            //    list: true,
            //    width: '4%'
            //},
            Print: {
                title: 'Print',
                width: '2%',
                //display: function (data) {
                //    return '<button type="button" onclick="alert(' + data.record.Invno + ')">Create PDF</button> ';
                display: function (row) {
                    var button = $("<i class='icon-printer'></i>");
                    $(button).click(function () {

                        Pk_Inw = row.record.Pk_Inward;
                        _comLayer.parameters.add("Pk_Inw", Pk_Inw);
                        _comLayer.executeSyncAction("JW_Inwd/InwdRep", _comLayer.parameters);
                        var strval = "ConvertPDF/MInward" + Pk_Inw + ".pdf"
                        ////////////////////////////


                        var xhr = new XMLHttpRequest();
                        var urlToFile = "ConvertPDF/MInward" + Pk_Inw + ".pdf"
                        xhr.open('HEAD', urlToFile, false);
                        xhr.send();

                        if (xhr.status == "404") {
                            alert('Data Not Available , File does not Exist');
                            return false;


                        } else {
                            window.open(strval, '_blank ', 'width=700,height=250');
                            return true;
                        }


                        /////////////////////////
                        

                    });
                    return button;
                }
            },

            //////////////////////////////////////////////////
            Details: {
                title: 'Details',
                width: '5%',
                sorting: false,
                edit: false,
                create: false,
                listClass: 'child-opener-image-column',
                display: function (data) {
                    //Create an image that will be used to open child table
                    // var $img = $('<img class="child-opener-image" src="/images/redSignal.jpg" style="width:25px;height:25px" title="Author" />');
                    var $img = $("<i class='icon-users'></i>");
                    //Open child table when user clicks the image
                    $img.click(function () {
                        $('#MainSearchContainer').jtable('openChildTable',
                                    $img.closest('tr'),
                                    {
                                        title: 'Material List',
                                        actions: {
                                            listAction: '/JW_Inwd/InwardGetRec?Pk_Inward=' + data.record.Pk_Inward

                                        },
                                        fields: {
                                           
                                     
                                            PkMaterial: {
                                                title: 'MaterialID',
                                                list: true
                                            },
                                            InwardDet: {
                                                title: 'InwardDet',
                                                list:false
                                            },
                                            PkInwd: {
                                                title: 'PkInwd',
                                                list: false
                                            },
                                            //InwDate: {
                                            //    title: 'InwDate',
                                            //    list: false
                                            //},
                                            Name: {
                                                title: 'MaterialName'
                                            },
                                            //Color: {
                                            //    title: 'Shade'
                                            //},
                                            //Mill: {
                                            //    title: 'Mill'

                                            //},
                                            Quantity: {
                                                title: 'Qty',
                                                key: true,
                                            },
                                            ReelNo: {
                                                title: 'ReelNo',
                                                key: true,
                                            },
                                            //Indent: {
                                            //    title: 'Indent ID',
                                            //    key: true,
                                            //},
                                            //PONo: {
                                            //    title: 'PONo',
                                            //    key: true,
                                            //},
                                            //QC: {
                                            //    title: 'QC ID',
                                            //    key: true,
                                            //},

                                            /////////////////////////////
                                            //Distribution: {
                                            //    title: 'Distribution',
                                            //    width: '2%',
                                            //    //display: function (data) {
                                            //    //    return '<button type="button" onclick="alert(' + data.record.Invno + ')">Create PDF</button> ';
                                            //    display: function (row) {
                                            //        //var button = $("<i class='icon-printer'></i>");
                                            //        var button = $("<button class='bnt'>Distribution</button>");
                                            //        $(button).click(function () {

                                            //            PkMaterial = row.record.PkMaterial;
                                            //            PkInwd = row.record.PkInwd;
                                            //            //PName = row.record.Name;
                                            //            //document.getElementById('pk').value = PkEnqChildId;
                                            //            //_comLayer.parameters.add("PkMaterial", PkMaterial);
                                            //            //comLayer.parameters.add("PName", PName);
                                            //            //$('#cmdNew').click();
                                            //            //var oResult1 = _comLayer.executeSyncAction("Enquiry/CheckEstimate", _comLayer.parameters);


                                            //            //if (oResult1.data.Estimated) {

                                            //            //    alert('Already Estimated!!');
                                            //            //    BoxId = row.record.Fk_BoxID;
                                            //            //    window.open("/Estimation");
                                            //            //}
                                            //            //else {
                                            //            //    BoxId = row.record.Fk_BoxID;

                                            //            //    // window.open("/EnqEstimate?Box=" + BoxId + ", EnqChild=" + PkEnqChildId + ", BT=" + BType);
                                            //            window.open("/PaperDistribution?Material=" + PkMaterial + ", Inwd=" + PkInwd);
                                            //            //}

                                            //        });
                                            //        return button;
                                            //    }

                                            //}

                                            //////////////////////////////////
                                        },
                                        formClosed: function (event, data) {
                                            data.form.validationEngine('hide');
                                            data.form.validationEngine('detach');
                                        }
                                    }, function (data) { //opened handler
                                        data.childTable.jtable('load');
                                    });
                    });
                    //Return image to show on the person row
                    return $img;
                }
            },


            //////////////////////////////////////////////////



        }
    });

      

    //Re-load records when Customer click 'load records' button.
    $('#TxtFromDate').datepicker({ autoclose: true });
        $('#TxtToDate').datepicker({ autoclose: true });

        $('#dtPInwardDate').datepicker({
            autoclose: true
        });

        //Re-load records when Order click 'load records' button.
        $('#LoadRecordsButton').click(function (e) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Pk_Inward: $('#InwardNumber').val(),
                Vendor: $('#VendorName').val(),
                Material: $('#Material').val(),
                FromDate: $('#TxtFromDate').val(),
                ToDate: $('#TxtToDate').val(),
            });

        });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });

    $('#dtPInwardDate').datepicker({ autoclose: true });


    //$('#Pk_PONo').keypress(function (e) {
    //    if (e.keyCode == 13) {
    //        e.preventDefault();

    //        $('#MainSearchContainer').jtable('load', {
    //            Pk_PONo: $('#txtPkPONo').val()
    //        });
    //    }
    //});

    //$('#dtDate').change(function (e) {
    //    e.preventDefault();
    //    $('#MainSearchContainer').jtable('load', {
    //        PODate: $('#txtPODate').val()
    //    });
    //});
        $("#searchDialog").width(900);
    $("#searchDialog").css("top", "10px");
    $("#searchDialog").css("left", "400px");

    setUpQualitySearch();
 
}


function afterNewShow(viewObject) {
    var dNow = new Date();
    document.getElementById('dtPInwardDate').value = (((dNow.getDate()) < 10) ? "0" + dNow.getDate() : dNow.getDate()) + '/' + (((dNow.getMonth() + 1) < 10) ? "0" + (dNow.getMonth() + 1) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();
    curViewModel = viewObject.viewModel;

    $('#dtPInwardDate').datepicker({ autoclose: true });
    $('#dtPur_InvDate').datepicker({ autoclose: true });
    $('#tlbMaterials').jtable({
        title: 'Item List',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/JW_Inwd/Bounce',
            deleteAction: '',
            //updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            Fk_Material: {
                title: 'Id',
                key: false,
                list: false
            },
            Name: {
                title: 'Material Name'

            },
            //Color: {
            //    title: 'Shade'

            //},
            //SName: {
            //    title: 'Mill',
            //    width: '1%'
            //},
                RollNo: {
                    title: 'ReelNo',
                    width: '1%'
                },
            Quantity: {
                title: 'Quantity',
                width: '1%'

            },

          
            
        //    QCNo: {
        //        title: 'QCNo',
        //width: '1%'
        //    },

            //Fk_Mill: {
            //    title: 'Fk_Mill',
            //    width: '1%',
            //    list:false
            //}
        }
    });

  
    configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "Materials", "JW_Inwd", "_AddMaterial", function () { return new IssuedMaterial() });


    $('#cmdJWSearch').click(function (e) {
                e.preventDefault();
                setUpPOSearch();
                $("#searchQualityDialog").modal("show");
            });

    $('#cmdCustomerSearch').click(function (e) {
        e.preventDefault();
        setUpCustomerSearch();
        $("#searchCustomerDialog").modal("show");

    });


    $('#cmdNewCustomer').click(function (e) {
        e.preventDefault();
        window.open("/Customer");
        //setUpCustomerSearch();
        //$("#searchCustomerDialog").modal("show");

    });
    

}

function beforeModelSaveEx() {
    var viewModel = _page.getViewByName('New').viewModel;
    viewModel.data["Inward_Date"] = $('#dtDate').val();
    viewModel.data["Fk_Customer"] = CustomerID;
    viewModel.data["JWNo"] = $('#JWNo').val();
    //viewModel.data["Fk_Indent"] = $('#Indent').val();
    //viewModel.data["Fk_Mill"] = $('#Fk_Mill').val();
    //viewModel.data["NETVALUE"] = $('#NETVALUE').val();
    //viewModel.data["GrandTotal"] = $('#GrandTotal').val();
    //viewModel.data["ED"] = $('#ED').val();

    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel1.data["Inward_Date"] = $('#dtDate').val();
    viewModel1.data["Fk_Customer"] = CustomerID;
    viewModel1.data["JWNo"] = $('#JWNo').val();
    //viewModel1.data["Fk_QC"] = $('#txtQualityCheck').val();
    //viewModel1.data["Fk_Mill"] = $('#Fk_Mill').val();
    //viewModel1.data["PONo"] = $('#TxtPO').val();
    //QCValue = $('#TxtPO').val();
    //viewModel1.data["Fk_Indent"] = $('#Indent').val();
    //viewModel.data["Fk_Indent"] = IndentNo;
}
function afterEditShow(viewObject) {
    curViewModel = viewObject.viewModel;
    $('#dtDate').datepicker({ autoclose: true });
    curEdit = true;
    $('#tlbMaterials').jtable({
        title: 'Item List',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/JW_Inwd/Bounce',
            //deleteAction: '',
            updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            Fk_Material: {
                title: 'Id',
                key: false,
                list: false
            },
            Name: {
                title: 'Material Name',
                width: '5%'
            },

            //Color: {
            //    title: 'Shade',
            //    width: '5%'
            //},
            //Mill: {
            //    title: 'Mill',
            //    width: '5%'
            //},
            RollNo: {
                title: 'ReelNo',
                width: '1%'
            },
            Quantity: {
                title: 'Quantity',
                width: '1%'
            },

            //Price: {
            //    title: 'Rate',
            //    width: '1%'
            //}
        }
    });

    //if ($.trim($('#TaxType').val()) == "VAT") {
    //    $("#VAT").prop("checked", true);
    //}
    //else if ($.trim($('#TaxType').val()) == "CST") {
    //    $("#CST").prop("checked", true);


    //}

    var oSCuts = viewObject.viewModel.data.MaterialData();
    viewObject.viewModel.data["Materials"] = ko.observableArray();
    var i = 0;
    ssum = 0;
    while (oSCuts[i]) {
        var oCut = new IssuedMaterial();

        oCut.load(oSCuts[i].Pk_InwardDet, oSCuts[i].Name, oSCuts[i].Fk_Material, oSCuts[i].Quantity,  oSCuts[i].AccQty, oSCuts[i].RollNo, oSCuts[i].Fk_Mill, oSCuts[i].Color, oSCuts[i].Mill);
        viewObject.viewModel.data["Materials"].push(oCut);

        i++;
    }

    configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "Materials", "JW_Inwd", "_AddMaterial", function () { return new IssuedMaterial() });

}


function setUpPOSearch() {
    //Branch

    //cleanSearchDialog();

    $("#srchHeader").text("JW Search");

    //Adding Search fields
    var txtFieldIndentNo = "<input type='text' id='txtdlgIndent' placeholder='JW NO' class='input-large search-query'/>&nbsp;&nbsp;";
    //var txtFieldVendor = "<input type='text' id='txtdlgVendor' placeholder='Vendor' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldFromDate = "<input type='text' id='txtdlgFromDate' placeholder='FromDate' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldToDate = "<input type='text' id='txtdlgToDate' placeholder='ToDate' class='input-large search-query'/>&nbsp;&nbsp;";




    $('#QualitySearhContainer').jtable({
        title: 'JW List',
        paging: true,
        pageSize: 4,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/PartJobs/PJobsListByFiter',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdQCheckDone').click();
            });
        },

        fields: {
            Pk_ID: {
                title: 'Job ID',
                key: true,
                list: true,
                width: '4%'
            },
            RecdDate: {
                title: 'Recd Date',
                width: '4%'
            },

            Fk_Customer: {
                title: 'Customer',
                width: '10%'
            },
            Description: {
                title: 'Description',
                width: '10%'
            },
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#QualitySearhContainer').jtable('load', {
            Pk_PONo: $('#txtdlgInvoice').val()

        });
    });

    $('#LoadRecordsButton').click();


    $('#cmdQCheckSearch').click(function (e) {
        e.preventDefault();

        $('#QualitySearhContainer').jtable('load', {
            Pk_PONo: $('#txtdlgInvoice').val(),
            //Vendor: $('#txtdlgVendor').val(),
            FromIndentDate: $('#txtdlgFromDate').val(),
            ToIndentDate: $('#txtdlgToDate').val()

        });
    });

    $('#cmdQCheckDone').click(function (e) {
        e.preventDefault();
        var rows = $('#QualitySearhContainer').jtable('selectedRows');
     
        //$('#txtFk_IndentNumber') = document.getElementById(rows[0].keyValue
        document.getElementById('JWNo').value = rows[0].keyValue;
        IndentNo = rows[0].data.Pk_ID;
        document.getElementById('JWNo').value = IndentNo;
        //$('#txtFk_Vendor').wgReferenceField("setData", rows[0].data.VendorID);
   $("#searchQualityDialog").modal("hide");
    });

}




function setUpIssueMaterialSearch() {
    var rowCount = $('#MaterialSearchContainer tr').length;
    if (rowCount > 0) {
        $('#MaterialSearchContainer').jtable('destroy');

    }

    PONo = document.getElementById ('JWNo').value;
    //SaveSearch();
    if (GSM > 0 & BF > 0 & Dec > 0) {
        document.getElementById('txtMaterial').value = GSM;
        document.getElementById('txtBF').value = BF;
        document.getElementById('txtDec').value = Dec;
    }

    $('#MaterialSearchContainer').jtable({
        title: 'Material List',
        paging: true,
        pageSize:7,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/MaterialCategory/MaterialIndentSearch?MaterialCategory='+MCat,
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                //$(this).css('background', 'red');
                
                $('#cmdMaterialDone').click();
            });
        },

       

        fields: {
            Pk_Material: {
                title: 'Material Id',
                key: true
            },

            Name: {
                title: 'Mat.Name'
            },
            //PONo: {
            //    title: 'PoNo'
            //},
            Color: {
                title: 'Shade'
            },
            Mill: {
                title: 'MillName',
                list: true
            },
            //Quantity: {
            //    title: "Quantity"
            //},
            Pk_Mill: {
                title: "Fk_Mill",
                list: false

            }
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            GSM: $('#txtMaterial').val(),
            BF: $('#txtBF').val(),
            Dec: $('#txtDec').val(),
            //Pk_MaterialIssueID: $('#Fk_Vendor').val()
        });
    });

    $('#LoadRecordsButton').click();

    $('#cmdMaterialSearch').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            GSM: $('#txtMaterial').val(),
            BF: $('#txtBF').val(),
            Dec: $('#txtDec').val(),
        });
    });

    $('#cmdMaterialDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MaterialSearchContainer').jtable('selectedRows');
        var $selectedRows = $('#MaterialSearchContainer').jtable('selectedRows');
        $('#txtFk_Material').wgReferenceField("setData", rows[0].keyValue);
        //document.getElementById('Quantity').value = rows[0].data["Quantity"];
        document.getElementById('SName').value = rows[0].data["Mill"];
        document.getElementById('Fk_Mill').value = rows[0].data["Pk_Mill"];
        //document.getElementById('RollNo').value = rows[0].data["ReelNo"];
        document.getElementById('Shade').value = rows[0].data["Color"];
        //document.getElementById('Mill').value = rows[0].data["Mill"];
        //var KVal = rows[0].data["Pk_Material"];
     //   $('#MaterialSearchContainer').jtable('deleteRows',$selectedRows);
        //$('#MaterialSearchContainer').jtable('deleteRecord', { key: KVal, clientOnly: true });

SaveSearch();
        _util.setDivPosition("divCreateMaterial", "block");
        _util.setDivPosition("divSearchMaterial", "none");
        
        $("#Quantity").focus();
    });



}


function setUpCustomerSearch() {
    //Enquiry

    cleanSearchDialog();

    //Adding Search fields
    var txtFieldCustomerName = "<input type='text' id='txtdlgCustomerName' placeholder='Vendor Name' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldCustomerNam = "<input type='text' id='txtdlgCustomerNam' placeholder='Land Line' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldCustomerNumber = "<input type='text' id='txtdlgCustomerContact' placeholder='Contact number' class='input-large search-query'/>&nbsp;&nbsp;";

    $("#dlgSearchFields").append(txtFieldCustomerName);
    $("#dlgSearchFields").append(txtFieldCustomerNam);
    //$("#dlgSearchFields").append(txtFieldCustomerNumber);

    $('#SearchContainer').jtable({
        title: 'Vendor List',
        paging: true,
        pageSize: 5,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Vendor/VendorListByFiter',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdDone').click();
            });
        },

        fields: {
            Pk_Vendor: {
                title: 'Id',
                key: true,
                width: '2%'
            },
            //VendorType: {
            //    title: 'Vendor Type',
            //    width: '5%'
            //},
            VendorName: {
                title: 'Vendor Name'
            },
            City: {
                title: 'City',
                width: '4%'
            },
            LandLine: {
                title: 'Land Line'
            },
           
        }
    });


    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#SearchContainer').jtable('load', {
            VendorName: $('#txtdlgCustomerName').val(),
            LandLine: $('#txtdlgCustomerNam').val(),
            //CustomerContact: $('#txtdlgCustomerContact').val()
        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    $('#cmdSearch').click(function (e) {
        e.preventDefault();
        $('#SearchContainer').jtable('load', {
            VendorName: $('#txtdlgCustomerName').val(),
            LandLine: $('#txtdlgCustomerNam').val(),
            //CustomerContact: $('#txtdlgCustomerContact').val()
        });
    });

    $('#cmdDone').click(function (e) {
        e.preventDefault();
        var rows = $('#SearchContainer').jtable('selectedRows');
        $('#txtVendor').wgReferenceField("setData", rows[0].keyValue);

        CustomerID = rows[0].keyValue;

        $("#searchCustomerDialog").modal("hide");
        //$("#ReferenceEnquiry").focus();
    });

}


function afterOneToManyDialogShow(property) {

    $('#cmdSearchMaterialsdetails').click(function (e) {
        e.preventDefault();
        //setUpIssueMaterialSearch();
        showOnlyBoard();
        _util.setDivPosition("divSearchMaterial", "block");
        _util.setDivPosition("divCreateMaterial", "none");
      
    });
}

function SaveSearch()
{
    GSM = document.getElementById('txtMaterial').value;
    BF = document.getElementById('txtBF').value;
    Dec = document.getElementById('txtDec').value;
}
function ReferenceFieldNotInitilized(viewModel) {

    //$('#txtFk_Vendor').wgReferenceField({
    //    keyProperty: "Fk_Vendor",
    //    displayProperty: "VendorName",
    //    loadPath: "Vendor/Load",
    //    viewModel: viewModel
    //});
    $('#txtFk_Material').wgReferenceField({
        keyProperty: "Fk_Material",
        displayProperty: "Name",
        loadPath: "Material/Load",
        viewModel: viewModel
    });
    $('#txtVendor').wgReferenceField({
        keyProperty: "Fk_Vendor",
        displayProperty: "VendorName",
        loadPath: "Vendor/Load",
        viewModel: viewModel
    });


    if (viewModel.data != null) {
        $('#txtFk_Material').wgReferenceField("setData", viewModel.data["Fk_Material"]);
        $('#txtCustomer').wgReferenceField("setData", viewModel.data["Fk_Customer"]);
       
    }
}

function cleanSearchDialog() {

    //Clean the search dialog of previous bindings and prepare for this selected dialog
    $('#MainSearhContainer').empty();
    $('#MainSearhContainer').append("<div id='SearchContainer'></div>");
    $("#dlgSearchFields").empty();
    $('#cmdSearch').off();
    $('#cmdDone').off();

    $('#dlgSearchQualityFields').empty();

}

function checkDuplicate() {

    if (objContextEdit == false) {
        var i = 0;

        while (curViewModel.data["Materials"]()[i]) {
            if (objContext.data.Fk_Material == curViewModel.data["Materials"]()[i].data.Fk_Material && objContext.data.RollNo == curViewModel.data["Materials"]()[i].data.RollNo) {

                return "* " + "Item Already added";
            }
            i++;
        }
    }
}

function checkValue() {
    if (objContextEdit == false) {

        if (Number($('#Quantity').val()) > Number($('#AccQuantity').val())) {
            return "* " + "Inward Quantity Exceeded Quality Checked Qty. Quality Checked Quantity:" + $('#AccQuantity').val();
        }
        else {
           
        }
    } else if (objContextEdit == true) {
        if (Number($('#Quantity').val()) > Number($('#AccQuantity').val())) {
            return "* " + "Inward Quantity Exceeded Quality Checked Qty. Quality Checked Quantity:" + $('#AccQuantity').val();
        }
        else {
        }
    }
}

function showOnlyPaper() {
 MCat = "Paper";
    setUpIssueMaterialSearch();
   
}
function showOnlyBoard() {


    var rowCount = $('#MaterialSearchContainer tr').length;
    if (rowCount > 0) {
        $('#MaterialSearchContainer').jtable('destroy');

    }


    document.getElementById('SType').value = "Board";


    $('#MaterialSearchContainer').jtable({
        title: 'Material List',
        paging: true,
        pageSize: 10,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/MaterialCategory/MaterialSearchListByFiter',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                //$(this).css('background', 'red');
                $('#cmdMaterialDone').click();
            });
        },
        fields: {
            Pk_Material: {
                title: 'Material Id',
                key: true,
                width: '2%'
            },
            Pk_PaperStock: {
                title: 'PKStock',
                key: false,
                width: '2%',
                list: false
            },
            RollNo: {
                title: 'Reel No',
                edit: false,
                width: '2%'
            },
            Name: {
                title: 'Material Name',
                edit: false
            },
            Color: {
                title: 'Shade',
                edit: false,
                width: '2%'
            },
            Quantity: {
                title: 'Ex.Stk.Qty',
                edit: false,
                width: '2%'
            },

            Category: {
                title: 'Cat',
                edit: false,
                list: false
            },
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            //Name: $('#txtMaterial').val(),
            //Pk_MaterialIssueID: $('#Fk_Vendor').val()
            MaterialCategory: $('#SType').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdMaterialSearch').click(function (e) {
        e.preventDefault();

        if (document.getElementById('SType').value == '')
        { alert("Select any one of the Material Type and Press Search"); }
        else
        {
            $('#MaterialSearchContainer').jtable('load', {
                //Name: $('#txtMaterial').val(),
                MaterialCategory: $('#SType').val()

            });
        }
    });

 
    $('#cmdMaterialDone').click(function (e) {

        valCat=document.getElementById('SType').value;
        if (valCat == "Board") {
            e.preventDefault();
            var rows = $('#MaterialSearchContainer').jtable('selectedRows');
            //var rctr = $('#MaterialSearchContainer').jtable('selectedRows').TotalRecordCount;
            if (rows.length > 0) {
                $('#txtFk_Material').wgReferenceField("setData", rows[0].keyValue);
                document.getElementById('txtMaterial').value = rows[0].data.Pk_Material;
                if (rows[0].data["Category"] != "Paper") {
                    document.getElementById('RollNo').value = "";
                }
                else {
                    document.getElementById('RollNo').value = rows[0].data["RollNo"];
                }
                //document.getElementById('Quantity').value = rows[0].data["Quantity"];
                //document.getElementById('Pk_PaperStock').value = rows[0].data["Pk_PaperStock"];
                //document.getElementById('Pk_Material').value = rows[0].data["Pk_Material"];
                //document.getElementById('Color').value = rows[0].data["Color"];
            }
            _util.setDivPosition("divCreateMaterial", "block");
            _util.setDivPosition("divSearchMaterial", "none");
            $("#Quantity").focus();
        }
    });






}