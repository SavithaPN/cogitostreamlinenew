﻿
//Name          : Country Operations ----javascript files
//Description   : Contains the  PaperType Operations  definition with following
//                 1. Jtable Loading and Columns Descriptions 2.Button Click Events  3. CheckCountryDuplicate(duplication checking)
//Author        : Nagesh V Rao
//Date 	        : 07/08/2015
//Crh Number    : SL0010
//Modifications : 
var curViewModel = null;
var objShortCut = null;
function initialCRUDLoad() {
    $('#MainSearchContainer').jtable({
        title: 'Rejections List',
        paging: true,
        pageSize: 8,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Rejections/RejectionListByFiter',
            updateAction: '',
            createAction: '',
            deleteAction: ''
        },
        fields: {
            Pk_RejectionId: {
                title: 'Id',
                key: true,
                list: true,
                width: '5%'
            },
            RejectionDate: {
                title: 'Date',
                width: '5%'
            },
            Fk_OrderNo: {
                title: 'Order No',
                key: true,
                list: true,
                width: '5%'
            },
            Fk_Customer: {
                title: 'Name',
                key: true,
                list: true,
                width: '15%'
            },
            
            
            Rejected_Quantity: {
                title: 'Quantity',
                width: '2%'
            }
            

        }
    });
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_RejectionId: $('#txtRejId').val(),
            Fk_OrderNo: $('#OrderNumber').val(),
            Fk_Customer: $('#Customer').val(),
            RejectionDate: $('#RejectionDate').val(),
            
        });
    });
    $('#LoadRecordsButton').click();
    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });




    $('#RejectionDate').datepicker({
        autoclose: true
    });


}
function afterNewShow(viewObject) {

    bEditContext = false;
    curViewModel = viewObject.viewModel;
    var dNow = new Date();
    document.getElementById('RejectionDate').value = (((dNow.getDate()) < 10) ? "0" + (dNow.getDate()) : (dNow.getDate())) + '/' + (((dNow.getMonth() + 1) < 10) ? "0" + (dNow.getMonth() + 1) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();
    $('#RejectionDate').datepicker({ autoclose: true });



    $('#cmdCustomerOrderSearch').click(function (e) {
        e.preventDefault();
        setUpCustomerSearch();
        $("#searchCustomerDialog").modal("show");
    });
}



function beforeOneToManySaveHook(objectContext) {
    var bValidation = true;

    if ($("#Quantity").val() == "") {
        $("#Quantity").validationEngine('showPrompt', 'Enter Quantity', 'error', true)
        bValidation = false;
    }
    else if (!$.isNumeric($("#Quantity").val())) {
        $("#Quantity").validationEngine('showPrompt', 'Enter Numbers', 'error', true)
        bValidation = false;
    }
    //if ($("#txtFk_Material").val() == "") {
    //    $("#txtFk_Material").validationEngine('showPrompt', 'Select Material', 'error', true)
    //    bValidation = false;
    //}

    return bValidation;


}

function afterEditShow(viewObject) {

    curViewModel = viewObject.viewModel;



    $('#RejectionDate').datepicker({
        autoclose: true
    });



}

function beforeOneToManyDataBind(property) {
    if (bEditContext) {
        _util.setDivPosition("divDeliveryStatus", "block");
    }

}

//function setUpInvoiceSearch() {
//    //Enquiry

//    //cleanSearchDialog();

//    //Adding Search fields
//    var txtFieldInvoiceNo = "<input type='text' id='txtdlgInvoiceNo' placeholder='InvoiceNo.' class='input-large search-query'/>&nbsp;&nbsp;";

//    $("#dlgSearchFieldsInvoice").append(txtFieldInvoiceNo);


//    $('#SearchInvoiceContainer').jtable({
//        title: 'Invoice List',
//        paging: true,
//        pageSize: 15,
//        selecting: true, //Enable selecting
//        multiselect: false, //Allow multiple selecting
//        selectingCheckboxes: true,
//        defaultSorting: 'Name ASC',
//        actions: {
//            listAction: '/InvBilling/BillingListByFiter',
//        },
//        fields: {
//            Pk_Invoice: {
//                title: 'Invoice Id',
//                key: true
//            },
//            InvDate: {
//                title: 'Date',
//                edit: false
//            },
//            Invno: {
//                title: 'Invoice Number'
//            }

//        }
//    });


//    $('#LoadRecordsButton').click(function (e) {
//        e.preventDefault();
//        $('#SearchInvoiceContainer').jtable('load', {
//            Pk_Invoice: $('#txtFieldInvoiceNo').val()
//        });

//    });

//    //Load all records when page is first shown
//    $('#LoadRecordsButton').click();

//    $('#cmdInvSearch').click(function (e) {
//        e.preventDefault();
//        $('#SearchInvoiceContainer').jtable('load', {
//            Pk_Invoice: $('#txtFieldInvoiceNo').val()
//        });
//    });

//    $('#cmdInvoiceDone').click(function (e) {
//        //e.preventDefault();
//        //var rows = $('#SearchInvoiceContainer').jtable('selectedRows');
//        //$("#Fk_Invno").val(rows[0].data.Pk_Invoice);
//        //$("#searchInvoiceDialog").modal("hide");
//        //$('#Fk_Invno').wgReferenceField("setData", rows[0].keyValue);

//        e.preventDefault();
//        var rows = $('#SearchInvoiceContainer').jtable('selectedRows');
//        $("#searchInvoiceDialog").modal("hide");
//        document.getElementById('Fk_Invno').value = rows[0].keyValue;
//    });
//}
function setUpCustomerSearch() {
    //Branch

    //cleanSearchDialog();

    $("#srchCHeader").text("Customer Search");

    //Adding Search fields
    var txtFieldCustomerName = "<input type='text' id='txtdlgCustomerName' placeholder='Customer Name' class='input-large search-query'/>&nbsp;&nbsp;";

    $("#dlgCustomerSearchFields").append(txtFieldCustomerName);

    $('#CustSearchContainer').jtable({
        title: 'Customer List',
        paging: true,
        pageSize: 8,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/Order/OrderListByFiter'
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdCustDone').click();
            });
        },
        fields: {
            Pk_Order: {
                title: 'Order No',
                key: true
            },
            OrderDate: {
                title: 'Ord Date'
            },
            CustomerName: {
                title: 'CustomerName'
            },
            Fk_Customer: {
                title: 'CustomerID',
                list:false,
            },
            //CustID: {
            //    title: 'CustomerID'
            //},
            //ProductName: {
            //    title: 'ProductName',
            //    edit: false
            //},
            //Quantity: {
            //    title: 'Quantity'
            //},
            //Price: {
            //    title: 'Price'
            //},
            //State: {
            //    title: 'State'
            //}
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#CustSearchContainer').jtable('load', {
            CustomerName: $('#txtdlgCustomerName').val()
        });
    });

    $('#LoadRecordsButton').click();


    $('#cmdCustSearch').click(function (e) {
        e.preventDefault();

        $('#CustSearchContainer').jtable('load', {
            CustomerName: $('#txtdlgCustomerName').val()
        });
    });

    $('#cmdCustDone').click(function (e) {
        e.preventDefault();
        var rows = $('#CustSearchContainer').jtable('selectedRows');
        //$("#dataDialog").modal("hide");
        $("#searchCustomerDialog").modal("hide");
        $('#txtCustomerOrder').wgReferenceField("setData", rows[0].keyValue);
        $('#txtCustomer').wgReferenceField("setData", rows[0].data.CustID);
        // document.getElementById('txtCustomer').value = rows[0].data.CustomerName;

       

    });

}

function beforeModelSaveEx() {

    var viewModel = _page.getViewByName('New').viewModel;
    viewModel.data["RejectionDate"] = $('#RejectionDate').val();
    viewModel.data["Fk_OrderNo"] = $('#txtCustomerOrder').val();
    // viewModel.data["Fk_Material"] = $('#txtFk_Material').val();

    //viewModel.data["Fk_Indent"] = $('#TxtIndent').val();

    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel.data["RejectionDate"] = $('#RejectionDate').val();
    viewModel.data["Fk_OrderNo"] = $('#txtCustomerOrder').val();
    // viewModel1.data["Fk_Material"] = $('#txtFk_Material').val();

    //  viewModel.data["Fk_Indent"] = $('#TxtIndent').val();
}



function ReferenceFieldNotInitilized(viewModel) {

    $('#txtCustomer').wgReferenceField({
        keyProperty: "Fk_Customer",
        displayProperty: "CustomerName",
        loadPath: "Customer/Load",
        viewModel: viewModel
    });
    $('#txtCustomerOrder').wgReferenceField({
        keyProperty: "Pk_Order",
        displayProperty: "Pk_Order",
        loadPath: "Order/Load",
        viewModel: viewModel
    });

    if (viewModel.data != null) {

        $('#txtCustomerOrder').wgReferenceField("setData", viewModel.data["Fk_OrderNo"]);
        $('#txtCustomer').wgReferenceField("setData", viewModel.data["Fk_Customer"]);
    }
}


function cleanSearchDialog() {

    //Clean the search dialog of previous bindings and prepare for this selected dialog
    $('#MainSearhContainer').empty();
    $('#MainSearhContainer').append("<div id='SearchContainer'></div>");
    $("#dlgSearchFields").empty();
    $('#cmdSearch').off();
    $('#cmdDone').off();

    $('#dlgCustomerSearchFields').empty();
   
}



//function radioClass(str1) {
//    if (str1 == "Pass") {
//        document.getElementById('Result').value = "True";

//    }
//    else if (str1 == "Fail") {
//        document.getElementById('Result').value = "False";
//    }
//}


function beforeNewShow(viewObject) {
    $('#txtCustomer').wgReferenceField({
        keyProperty: "Pk_Customer",
        displayProperty: "CustomerName",
        loadPath: "Customer/Load",
        viewModel: viewModel
    });
    $('#txtCustomerOrder').wgReferenceField({
        keyProperty: "Pk_Order",
        displayProperty: "Pk_Order",
        loadPath: "Order/Load",
        viewModel: viewModel
    });
}


