﻿var curViewModel = null;
var curEdit = false;
var VendorID;
var gtot;
var EDVal;
var IndentNo;
var ssum = 0;
var tempamt = 0;
var intval = 0;
var Indent = 0;
//var invqty = null;
function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Jobs List',
        paging: true,
        pageSize: 10,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/PartJobs/PJobsListByFiter',
            //deleteAction: '',            
            //createAction: '',
            //updateAction: ''
        },
        fields: {
            Pk_ID: {
                title: 'Job Work No.',
                key: true,
                list: true,
                width: '4%'
            },
            RecdDate: {
                title: 'Recd Date',
                width: '4%'
            },

            Fk_Customer: {
                title: 'Customer',
                width: '10%'
            },
            Description: {
                title: 'Description',
                width: '10%'
            },
            Select_A: {
                title: "Close Job Manually",
                display: function (row) {
                    var button1 = $("<button class='bnt'>Close Job</button>");
                    $(button1).click(function () {
                        PO = row.record.Pk_ID;
                        _comLayer.parameters.add("Pk_ID", PO);
                       oResult=_comLayer.executeSyncAction("PartJobs/StatusUpdate", _comLayer.parameters);
                       if(oResult=true)
                           {alert("Job Status Updated Successfully")}
                      
                    });
                    return button1;
                }
            },
            Select_B: {
                title: "Open Job Manually",
                display: function (row) {
                    var button1 = $("<button class='bnt'>Open Job</button>");
                    $(button1).click(function () {
                        Pk_ID = row.record.Pk_ID;
                        _comLayer.parameters.add("Pk_ID", Pk_ID);
                        oResult = _comLayer.executeSyncAction("PartJobs/OpenStatus", _comLayer.parameters);
                        if (oResult = true)
                        { alert("Task Status Updated Successfully") }

                    });
                    return button1;
                }
            },
            Print: {
                title: 'Print',
                width: '2%',
                //display: function (data) {
                //    return '<button type="button" onclick="alert(' + data.record.Invno + ')">Create PDF</button> ';
                display: function (row) {
                    var button = $("<i class='icon-printer'></i>");
                    $(button).click(function () {

                        Pk_ID = row.record.Pk_ID;
                        _comLayer.parameters.add("Pk_ID", Pk_ID);
                        _comLayer.executeSyncAction("PartJobs/JobRep", _comLayer.parameters);
                        var strval = "ConvertPDF/JobWork" + Pk_ID + ".pdf"
                        window.open(strval, '_blank ', 'width=700,height=250');
                        ////////////////////////////



                        /////////////////////////
                      

                    });
                    return button;
                }
            },
            //////////////////////////////////////////////////
            Details: {
                title: 'Details',
                width: '5%',
                sorting: false,
                edit: false,
                create: false,
                listClass: 'child-opener-image-column',
                display: function (data) {
                    //Create an image that will be used to open child table
                    // var $img = $('<img class="child-opener-image" src="/images/redSignal.jpg" style="width:25px;height:25px" title="Author" />');
                    var $img = $("<i class='icon-users'></i>");
                    //Open child table when user clicks the image
                    $img.click(function () {
                        $('#MainSearchContainer').jtable('openChildTable',
                                    $img.closest('tr'),
                                    {
                                        title: 'Materials List',
                                        actions: {
                                            listAction: '/PartJobs/TaskGetRec?Pk_ID=' + data.record.Pk_ID

                                        },
                                        fields: {
                                            
                                          
                                            MaterialName: {
                                                title: 'MaterialName'
                                            },
                                          
                                            ReelNo: {
                                                title: 'ReelNo',
                                                key: true,
                                            },
                                            Weight: {
                                                title: 'Recd Qty.',
                                                key: true,
                                            },
                                            Length: {
                                                title: 'Length',
                                                key: true,
                                            },
                                            Width: {
                                                title: 'Width',
                                                key: true,
                                            },
                                            LayerWt: {
                                                title: 'Board Wt',
                                                key: true,
                                                edit:true,
                                            },

                                        },
                                        formClosed: function (event, data) {
                                            data.form.validationEngine('hide');
                                            data.form.validationEngine('detach');
                                        }
                                    }, function (data) { //opened handler
                                        data.childTable.jtable('load');
                                    });
                    });
                    //Return image to show on the person row
                    return $img;
                }
            },

            ///////////////////////////////////////////////
        }
    });

    //Re-load records when Customer click 'load records' button.
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_ID: $('#txtPkPONo').val(),
            Cust: $('#txtCust').val(),
            FromDate: $('#TxtFromDate').val(),
            ToDate: $('#TxtToDate').val()
        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    $('#TxtFromDate').change(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            FromDate: $('#TxtFromDate').val()
        });
    });
    $('#TxtFromDate').datepicker({
        autoclose: true
    });

    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });

    $('#dtDate').datepicker({ autoclose: true });


    $('#Pk_ID').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();

            $('#MainSearchContainer').jtable('load', {
                Pk_ID: $('#txtPkPONo').val()
            });
        }
    });

    $('#dtDate').change(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            FromDate: $('#TxtFromDate').val()
        });
    });

    $('#cmdPrint').click(function (e) {
        e.preventDefault();
        var FromDate = document.getElementById('TxtFromDate').value;
        var ToDate = document.getElementById('TxtFromDate').value;

        _comLayer.parameters.add("FromDate", FromDate);
        _comLayer.parameters.add("ToDate", ToDate);
        _comLayer.executeSyncAction("PartJobs/JobRepDates", _comLayer.parameters);
        var strval = "ConvertPDF/JobWork" + Pk_ID + ".pdf"
        window.open(strval, '_blank ', 'width=700,height=250');
    });
}


function afterNewShow(viewObject) {

    $('#cmdVendorMSearch').click(function (e) {
        e.preventDefault();
        setUpVendorSearch(viewObject);
        $("#searchDialog").modal("show");
        $("#searchDialog").width(600);
        $("#searchDialog").height(500);

    });


    var dNow = new Date();
    document.getElementById('dtDate').value = (((dNow.getDate()) < 10) ? "0" + dNow.getDate() : dNow.getDate()) + '/' + (((dNow.getMonth() + 1) < 10) ? "0" + (dNow.getMonth() + 1) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();
    curViewModel = viewObject.viewModel;

    $('#dtDate').datepicker({ autoclose: true });

    $('#tlbMaterials').jtable({
        title: 'Item List',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/PartJobs/Bounce',
            deleteAction: '',
            updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            Fk_Material: {
                title: 'Id',
                key: false,
                list: false
            },
            Name: {
                title: 'Material Name'

            },
            RecdWeight: {
                title: 'Recd. Weight',
                width: '1%'
            
        },

            ReelNo: {
                title: 'ReelNo',
            width: '1%'
        },

            Length: {
                title: 'Length',
                width: '1%'
            },
            Width: {
                title: 'Width',
                width: '1%'
            },

            //AppNos: {
            //    title: 'Exp.Nos',
            //    width: '1%'
            //},
            LayerWt: {
                title: 'Board Wt',
                width: '1%'
            },
        }
    });

   
    configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "Materials", "PartJobs", "_AddMaterial", function () { return new IssuedMaterial() });

 
    configureOne2Many("#cmdAddBoard", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "Materials", "PartJobs", "_AddBoard", function () { return new IssuedMaterial() });


    configureOne2Many("#cmdAddInk", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "Materials", "PartJobs", "_AddInk", function () { return new IssuedMaterial() });

    configureOne2Many("#cmdAddDie", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "Materials", "PartJobs", "_AddDye", function () { return new IssuedMaterial() });

    configureOne2Many("#cmdAddStereo", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "Materials", "PartJobs", "_AddStereo", function () { return new IssuedMaterial() });

    //$('#cmdMIndentSearch').click(function (e) {
    //    e.preventDefault();
    //    setUpIndentSearch();
    //    $("#searchIndentDialog").modal("show");
    //    //$("#dataDialog").width(700);
    //    //$("#dataDialog").height(500);

    //    $("#searchIndentDialog").css("top", "50px");
    //    $("#searchIndentDialog").css("left", "350px");
    //});
}


function beforeModelSaveEx() {
    var viewModel = _page.getViewByName('New').viewModel;

    //Calc();

    viewModel.data["RecdDate"] = $('#dtDate').val();
    viewModel.data["Fk_Customer"] = VendorID;
    viewModel.data["AppNos"] = $('#AppNos').val();
    viewModel.data["[LayerWt]"] = $('#LayerWeight').val();
    viewModel.data["RecdWeight"] = $('#IssueWeight').val();
   // viewModel.data["GrandTotal"] = $('#GrandTotal').val();
   // viewModel.data["CGST"] = $('#CGST').val();
   // viewModel.data["SGST"] = $('#SGST').val();

    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel1.data["RecdDate"] = $('#dtDate').val();
    viewModel1.data["Fk_Customer"] = $('#Fk_Customer').val();
    viewModel1.data["Fk_Customer"] = VendorID;
    viewModel1.data["AppNos"] = $('#AppNos').val();
    viewModel1.data["[LayerWt]"] = $('#LayerWeight').val();
    viewModel.data["RecdWeight"] = $('#IssueWeight').val();
   // viewModel1.data["Fk_Indent"] = $('#Fk_Indent').val();
   // viewModel1.data["NETVALUE"] = $('#NETVALUE').val();
   // viewModel1.data["GrandTotal"] = $('#GrandTotal').val();
   // viewModel.data["CGST"] = $('#CGST').val();
   // viewModel.data["SGST"] = $('#SGST').val();
   //// viewModel1.data["TaxType"] = $('#TaxType').val();
   // viewModel1.data["Amount"] = $('#Amount').val();
   // viewModel1.data["Rate"] = $('#Rate').val();

    //var statVal = document.getElementById('txtClose').value;

    //if (statVal == 1) {
    //    viewModel1.data["Fk_Status"] = 4;
    //}

  
    //else { viewModel1.data["Fk_Status"] = 1; }
}

function Calc() {
    var GTot = document.getElementById('GrandTotal').value;
    var TaxVal =  Number(GTot * 0.06);
    document.getElementById('CGST').value = TaxVal;
    document.getElementById('SGST').value = TaxVal;

    document.getElementById('NETVALUE').value = Number(TaxVal * 2) + Number(GTot);

}
function afterEditShow(viewObject) {
    //curEdit = true;
    curViewModel = viewObject.viewModel;
    $('#dtDate').datepicker({ autoclose: true });
    curEdit = true;


    $('#tlbMaterials').jtable({
        title: 'Item List',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/PartJobs/Bounce',
            deleteAction: '',
            updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            Fk_Material: {
                title: 'Id',
                key: false,
                list: false
            },
            Name: {
                title: 'Material Name',
                width: '5%'
            },
            Quantity: {
                title: 'Quantity',
                width: '1%'
            },

            Rate: {
                title: 'Rate',
                width: '1%'
            },
            Amount: {
                title: 'Amount',
                width: '1%'

            }
        }
    });

    if ($.trim($('#TaxType').val()) == "VAT") {
        $("#VAT").prop("checked", true);
    }
    else if ($.trim($('#TaxType').val()) == "CST") {
        $("#CST").prop("checked", true);

        

        
    }

    var oSCuts = viewObject.viewModel.data.MaterialData();
    viewObject.viewModel.data["Materials"] = ko.observableArray();
    var i = 0;
    ssum = 0;
    while (oSCuts[i]) {
        var oCut = new IssuedMaterial();
      
        oCut.load(oSCuts[i].Pk_PODet, oSCuts[i].Name, oSCuts[i].Fk_Material, oSCuts[i].Quantity, oSCuts[i].Rate, oSCuts[i].Amount);
        viewObject.viewModel.data["Materials"].push(oCut);

        i++;
    }
  
    configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "Materials", "PartJobs", "_AddMaterial", function () { return new IssuedMaterial() });


    //var POstatus = viewObject.viewModel.data["Fk_Status"]();

    //if (POstatus == "4") {
    //    $("#chkCloseJC").attr('checked', true);
    //}
}

function setUpVendorSearch() {
    //Enquiry

    // cleanSearchDialog();

    $("#srchssHeader").text("Vendor Search");

    //Adding Search fields
    var txtFieldVendorName = "<input type='text' id='txtdlgVendorName' placeholder='Vendor Name' class='input-large search-query'/>&nbsp;&nbsp;";

    $("#dlgSearchFields").append(txtFieldVendorName);


    $('#searchDialog').jtable({
        title: 'Customer List',
        paging: true,
        pageSize: 15,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Customer/CustomerListByFiter',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdDone').click();
            });
        },


        fields: {
            Pk_Customer: {
                title: 'Id',
                key: true,
                width: '2%'
            },
           
            CustomerName: {
                title: 'Customer Name',
                edit: false
            },
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#searchDialog').jtable('load', {
            CustomerName: $('#txtdlgVendorName').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdSearch').click(function (e) {
        e.preventDefault();
        $('#searchDialog').jtable('load', {
            CustomerName: $('#txtdlgVendorName').val()
        });
    });

    $('#cmdDone').click(function (e) {
        e.preventDefault();
        var rows = $('#searchDialog').jtable('selectedRows');
        $('#Fk_Customer').wgReferenceField("setData", rows[0].keyValue);
        $('#Fk_Customer').wgReferenceField("setData", rows[0].data.Fk_Customer);
        VendorID = rows[0].keyValue;
        $("#searchDialog").modal("hide");
    });

}

function setUpIssueMaterialSearch() {
    //Indent = document.getElementById('Fk_Indent').value;
    $('#MaterialSearchContainer').jtable({
        title: 'Material List',
        paging: true,
        pageSize: 10,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Paper/PaperList',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdMaterialDone').click();
            });
        },

        fields: {
            Pk_Material: {
                title: 'Material Id',
                key: true,
                width: '2%'
            },
            //Pk_PaperStock: {
            //    title: 'PKStock',
            //    key: false,
            //    width: '2%',
            //    list: false
            //},
            //RollNo: {
            //    title: 'Reel No',
            //    edit: false,
            //    width: '2%'
            //},
            Name: {
                title: 'Paper Name',
                edit: false
            },
            GSM: {
                title: 'GSM',
                width: '5%'
            },
            BF: {
                title: 'BF',
                width: '5%'
            },
            Deckle: {
                title: 'Deckle',
                width: '5%'
            },
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            Name: $('#txtMaterial').val(),
            //Pk_MaterialIssueID: $('#Fk_Customer').val(),
            Pk_Material: $('#txtPkMaterial').val(),
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdMaterialSearch').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            Name: $('#txtMaterial').val(),
            //Pk_MaterialIssueID: $('#Fk_Customer').val(),
            Pk_Material: $('#txtPkMaterial').val(),
        });
    });

    $('#cmdMaterialDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MaterialSearchContainer').jtable('selectedRows');
        $('#txtFk_Material').wgReferenceField("setData", rows[0].keyValue);
        //document.getElementById('IssueWeight').value = rows[0].data["Quantity"];
        
        //document.getElementById('ReelNo').value = rows[0].data["RollNo"];
        //document.getElementById('GSM').value = rows[0].data["GSM"];
        //document.getElementById('Ply').value = 1;

      
        _util.setDivPosition("divCreateMaterial", "block");
        _util.setDivPosition("divSearchMaterial", "none");
        $("#Length").focus();
    });



}


function setUpBoardSearch() {
    //Indent = document.getElementById('Fk_Indent').value;
    $('#MaterialSearchContainer').jtable({
        title: 'Material List',
        paging: true,
        pageSize: 10,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/MaterialCategory/MaterialSearchListByFiter?MaterialCategory=Board',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdMaterialDone').click();
            });
        },
        //Pk_Material = p.Pk_Material,
        //Name = p.MaterialName,
        //Category = p.Name,
        //Quantity = p.Quantity,
        fields: {
            Pk_Material: {
                title: 'Material Id',
                key: true,
                width: '2%'
            },
            //Pk_PaperStock: {
            //    title: 'PKStock',
            //    key: false,
            //    width: '2%',
            //    list: false
            //},
            //RollNo: {
            //    title: 'Reel No',
            //    edit: false,
            //    width: '2%'
            //},
            Name: {
                title: 'Mat.Name',
                edit: false
            },
            Quantity: {
                title: 'Ex.Stk.Qty',
                edit: false,
                width: '2%'
            },
            Length: {
                title: 'Length',
                list: true
            },
            Width: {
                title: 'Width',
                list: true
            },
            Weight: {
                title: 'Weight',
                list: true
            },
            Ply: {
                title: 'Ply',
                list: true
            },
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            MaterialName: $('#txtMaterial').val(),
            Pk_MaterialIssueID: $('#Fk_Vendor').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdMaterialSearch').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            MaterialName: $('#txtMaterial').val(),
            Pk_MaterialIssueID: $('#Fk_Vendor').val()
        });
    });

    $('#cmdMaterialDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MaterialSearchContainer').jtable('selectedRows');
        $('#txtFk_Material').wgReferenceField("setData", rows[0].keyValue);
        document.getElementById('LayerWt').value = rows[0].data["Weight"];

        document.getElementById('Length').value = rows[0].data["Length"];
        document.getElementById('Width').value = rows[0].data["Width"];
        document.getElementById('Ply').value = rows[0].data["Ply"];


        _util.setDivPosition("divCreateMaterial", "block");
        _util.setDivPosition("divSearchMaterial", "none");
        $("#IssueWeight").focus();
    });



}
function setUpInkSearch() {
    //Indent = document.getElementById('Fk_Indent').value;
    $('#MaterialSearchContainer').jtable({
        title: 'Material List',
        paging: true,
        pageSize: 10,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/MaterialCategory/MaterialSearchListByFiter?MaterialCategory=Ink',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdMaterialDone').click();
            });
        },
        //Pk_Material = p.Pk_Material,
        //Name = p.MaterialName,
        //Category = p.Name,
        //Quantity = p.Quantity,
        fields: {
            Pk_Material: {
                title: 'Material Id',
                key: true,
                width: '2%'
            },
            //Pk_PaperStock: {
            //    title: 'PKStock',
            //    key: false,
            //    width: '2%',
            //    list: false
            //},
            //RollNo: {
            //    title: 'Reel No',
            //    edit: false,
            //    width: '2%'
            //},
            Name: {
                title: 'Mat.Name',
                edit: false
            },

            Brand: {
                title: 'Brand',
                list: true
            },
            ColorName: {
                title: 'Color',
                list: true


            },

        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
           Name: $('#txtMaterial').val(),
            Pk_MaterialIssueID: $('#Fk_Vendor').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdMaterialSearch').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
           Name: $('#txtMaterial').val(),
            Pk_MaterialIssueID: $('#Fk_Vendor').val()
        });
    });

    $('#cmdMaterialDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MaterialSearchContainer').jtable('selectedRows');
        $('#txtFk_Material').wgReferenceField("setData", rows[0].keyValue);
        document.getElementById('Brand').value = rows[0].data["Brand"];
        document.getElementById('ColorName').value = rows[0].data["ColorName"];
        //document.getElementById('Width').value = rows[0].data["Width"];
        //document.getElementById('Ply').value = rows[0].data["Ply"];


        _util.setDivPosition("divCreateMaterial", "block");
        _util.setDivPosition("divSearchMaterial", "none");
        $("#IssueWeight").focus();
    });



}


function setUpDieSearch() {
    //Indent = document.getElementById('Fk_Indent').value;
    $('#MaterialSearchContainer').jtable({
        title: 'Material List',
        paging: true,
        pageSize: 10,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/MaterialCategory/MaterialSearchListByFiter?MaterialCategory=Die',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdMaterialDone').click();
            });
        },
        //Pk_Material = p.Pk_Material,
        //Name = p.MaterialName,
        //Category = p.Name,
        //Quantity = p.Quantity,
        fields: {
            Pk_Material: {
                title: 'Material Id',
                key: true,
                width: '2%'
            },
            //Pk_PaperStock: {
            //    title: 'PKStock',
            //    key: false,
            //    width: '2%',
            //    list: false
            //},
            //RollNo: {
            //    title: 'Reel No',
            //    edit: false,
            //    width: '2%'
            //},
            Name: {
                title: 'Mat.Name',
                edit: false
            },

            //Brand: {
            //    title: 'Brand',
            //    list: true
            //},
            //ColorName: {
            //    title: 'Color',
            //    list: true


            //},

        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            Name: $('#txtMaterial').val(),
            Pk_MaterialIssueID: $('#Fk_Vendor').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdMaterialSearch').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            Name: $('#txtMaterial').val(),
            Pk_MaterialIssueID: $('#Fk_Vendor').val()
        });
    });

    $('#cmdMaterialDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MaterialSearchContainer').jtable('selectedRows');
        $('#txtFk_Material').wgReferenceField("setData", rows[0].keyValue);
        //  document.getElementById('ReelNo').value = rows[0].data["Name"];



        _util.setDivPosition("divCreateMaterial", "block");
        _util.setDivPosition("divSearchMaterial", "none");
        $("#IssueWeight").focus();
    });



}


function setUpStereoSearch() {
    //Indent = document.getElementById('Fk_Indent').value;
    $('#MaterialSearchContainer').jtable({
        title: 'Material List',
        paging: true,
        pageSize: 10,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/MaterialCategory/MaterialSearchListByFiter?MaterialCategory=Stereo',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdMaterialDone').click();
            });
        },
        //Pk_Material = p.Pk_Material,
        //Name = p.MaterialName,
        //Category = p.Name,
        //Quantity = p.Quantity,
        fields: {
            Pk_Material: {
                title: 'Material Id',
                key: true,
                width: '2%'
            },
            //Pk_PaperStock: {
            //    title: 'PKStock',
            //    key: false,
            //    width: '2%',
            //    list: false
            //},
            //RollNo: {
            //    title: 'Reel No',
            //    edit: false,
            //    width: '2%'
            //},
            Name: {
                title: 'Mat.Name',
                edit: false
            },

            //Brand: {
            //    title: 'Brand',
            //    list: true
            //},
            //ColorName: {
            //    title: 'Color',
            //    list: true


            //},

        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            Name: $('#txtMaterial').val(),
            //Pk_MaterialIssueID: $('#Fk_Vendor').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdMaterialSearch').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            Name: $('#txtMaterial').val(),
            //Pk_MaterialIssueID: $('#Fk_Vendor').val()
        });
    });

    $('#cmdMaterialDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MaterialSearchContainer').jtable('selectedRows');
        $('#txtFk_Material').wgReferenceField("setData", rows[0].keyValue);
        //  document.getElementById('ReelNo').value = rows[0].data["Name"];



        _util.setDivPosition("divCreateMaterial", "block");
        _util.setDivPosition("divSearchMaterial", "none");
        $("#IssueWeight").focus();
    });



}


//var addl = Math.Round(Convert.ToDecimal(oPart.Length) * Convert.ToDecimal(oPart.Width), 2);
//layerWeight = Convert.ToDecimal((addl * Convert.ToDecimal(oLayerProperties["gsm"]) / 100) * Convert.ToDecimal(oLayerProperties["rate"]));    //////////////////rate is takeup factor value

function CalcLWt()
{
    var Length = document.getElementById('Length').value;
    var Width = document.getElementById('Width').value;
    //var GSM = document.getElementById('GSM').value;
    var RWt = document.getElementById('IssueWeight').value;

    var addl = Math.round(Number(Length) * Number(Width), 2);
    var layerWeight =(Number(addl) * Number(GSM)) / 100000000;

    document.getElementById('LayerWt').value = layerWeight;
    document.getElementById('AppNos').value = Math.round((Number(RWt) / Number(layerWeight)));
}

function afterOneToManyDialogShow(property) {

    $('#cmdSearchMaterialsdetails').click(function (e) {
        e.preventDefault();
        setUpIssueMaterialSearch();
        _util.setDivPosition("divSearchMaterial", "block");
        _util.setDivPosition("divCreateMaterial", "none");
    });

    $('#cmdSearchBoarddetails').click(function (e) {
        e.preventDefault();
        setUpBoardSearch();
        _util.setDivPosition("divSearchMaterial", "block");
        _util.setDivPosition("divCreateMaterial", "none");
    });

    $('#cmdSearchInkdetails').click(function (e) {
        e.preventDefault();
        setUpInkSearch();
        _util.setDivPosition("divSearchMaterial", "block");
        _util.setDivPosition("divCreateMaterial", "none");
    });

    $('#cmdSearchDiedetails').click(function (e) {
        e.preventDefault();
        setUpDieSearch();
        _util.setDivPosition("divSearchMaterial", "block");
        _util.setDivPosition("divCreateMaterial", "none");
    });
    $('#cmdSearchStereo').click(function (e) {
        e.preventDefault();
        setUpStereoSearch();
        _util.setDivPosition("divSearchMaterial", "block");
        _util.setDivPosition("divCreateMaterial", "none");
    });
}

function ReferenceFieldNotInitilized(viewModel) {

    //document.getElementById('ReturnableQty').style.display = "none";

    if (objContextEdit == true) {
        $('#cmdSearchMaterialsdetails').attr('disabled', true);
    }
    else if (objContextEdit == false) {
        $('#cmdSearchMaterialsdetails').attr('disabled', false);
    }
    $('#Fk_Customer').wgReferenceField({
        keyProperty: "Fk_Customer",
        displayProperty: "CustomerName",
        loadPath: "Customer/Load",
        viewModel: viewModel
    });
    $('#txtFk_Material').wgReferenceField({
        keyProperty: "Fk_Material",
        displayProperty: "Name",
        loadPath: "Material/Load",
        viewModel: viewModel
    });

    //$('#Fk_Indent').wgReferenceField({
    //    keyProperty: "Fk_Indent",
    //    displayProperty: "Fk_Indent",
    //    loadPath: "MaterialIndent/Load",
    //    viewModel: viewModel
    //});

    if (viewModel.data != null) {
        $('#txtFk_Material').wgReferenceField("setData", viewModel.data["Fk_Material"]);
        $('#Fk_Customer').wgReferenceField("setData", viewModel.data["Fk_Customer"]);
     //   $('#Fk_Indent').wgReferenceField("setData", viewModel.data["Fk_Indent"]);
    }
}

function checkDuplicate() {

    if (objContextEdit == false) {
        var i = 0;

        while (curViewModel.data["Materials"]()[i]) {
        
            if (objContext.data.Fk_Material == curViewModel.data["Materials"]()[i].data.Fk_Material  &&  objContext.data.ReelNo == curViewModel.data["Materials"]()[i].data.ReelNo) {

                return "* " + "Item Already added";
            }
            i++;
        }
    }
}

function checkstock() {
    if (objContextEdit == false) {

        if (Number($('#ReturnableQty').val()) < Number($('#ReturnQuantity').val())) {
            return "* " + "Return Quantity Excceded Invoice Quantity. Return Quantity:" + $('#ReturnableQty').val();
        }
        else {

        }
    } else if (objContextEdit == true) {
        if (Number($('#ReturnableQty').val()) < Number($('#ReturnQuantity').val())) {
            return "* " + "Return Quantity Excceded Invoice Quantity. Return Quantity:" + $('#ReturnableQty').val();
        }
        else {
        }
    }
}
function setUpIndentSearch() {
    //Branch

    //cleanSearchDialog();

    $("#srchHeader1").text("Indent Search");

    //Adding Search fields
    var txtFieldIndentNo = "<input type='text' id='txtdlgIndent' placeholder='IndentNo' class='input-large search-query'/>&nbsp;&nbsp;";
    //var txtFieldVendor = "<input type='text' id='txtdlgVendor' placeholder='Vendor' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldFromDate = "<input type='text' id='txtdlgFromDate' placeholder='FromDate' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldToDate = "<input type='text' id='txtdlgToDate' placeholder='ToDate' class='input-large search-query'/>&nbsp;&nbsp;";




    $('#IndentSearhContainer').jtable({
        title: 'Indent List',
        paging: true,
        pageSize: 7,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/MaterialIndent/MaterialIndentPOList'
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdIndentDone').click();
            });
        },

        fields: {
            Pk_MaterialOrderMasterId: {
                title: 'Indent Number',
                key: true
            },

            MaterialIndentDate: {
                title: 'Indent Date'
            },
            //CustomerName=p.CustomerName,
            //MaterialName=p.Name,
            CustomerName: {
                title: 'CustomerName'
            },
            MaterialName: {
                title: 'MaterialName'
            },
            Quantity: {
                title: 'Qty'
            },
            //StateName: {
            //    title: 'Status'
            //}
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#IndentSearhContainer').jtable('load', {
            Pk_MaterialIndent: $('#txtdlgIndent').val()

        });
    });

    $('#LoadRecordsButton').click();


    $('#cmdIndentSearch').click(function (e) {
        e.preventDefault();

        $('#IndentSearhContainer').jtable('load', {
            Pk_MaterialIndent: $('#txtdlgIndent').val(),
            Vendor: $('#txtdlgVendor').val(),
            FromIndentDate: $('#txtdlgFromDate').val(),
            ToIndentDate: $('#txtdlgToDate').val()

        });
    });

    $('#cmdIndentDone').click(function (e) {
        e.preventDefault();
        var rows = $('#IndentSearhContainer').jtable('selectedRows');
        $("#searchIndentDialog").modal("hide");
        //$('#txtFk_IndentNumber') = document.getElementById(rows[0].keyValue
        document.getElementById('Fk_Indent').value = rows[0].keyValue;
         IndentNo = rows[0].keyValue;

        $('#txtFk_Vendor').wgReferenceField("setData", rows[0].data.VendorID);

    });

}
function CalcAmt() {
  //  curViewModel = viewObject.viewModel;
    if  (objContextEdit == false) {
        if (Number($('#Quantity').val()) > 0 && Number($('#Rate').val()) > 0) {
            document.getElementById('Amount').value = (Number($('#Quantity').val()) * Number($('#Rate').val()));

            
                   }
   
        else {
        }
    }else if (objContextEdit == true) {
        if (Number($('#Quantity').val()) > 0 && Number($('#Rate').val()) > 0) {
            document.getElementById('Amount').value = (Number($('#Quantity').val()) * Number($('#Rate').val()));
        }
        else {
        }


    
    }
}

function radioClass1(intval) {
    if (intval == 1) {


        document.getElementById('TaxType').value = "VAT";
        if (FORM_CT3.checked == true)
        { document.getElementById('ED').value = Number($('#GrandTotal').val());}
        else {
            document.getElementById('ED').value = Number($('#GrandTotal').val()) + (Number($('#GrandTotal').val() * 0.06));
        }

        FORM_CT3.style.visibility = 'visible';
        FORM_H.style.visibility = 'visible';

        //    fct3.style.visibility = 'hidden';
        fct3.style.visibility = 'visible';
        fh.style.visibility = 'visible';

        if (Number($('#ED').val()) > 0) {
            document.getElementById('NETVALUE').value = 0;
            document.getElementById('NETVALUE').value = Number($('#ED').val()) + (Number($('#ED').val() * 0.055));
        }

        else if (Number($('#ED').val()) == 0) {
            document.getElementById('ED').value = Number($('#GrandTotal').val()) + (Number($('#GrandTotal').val() * 0.06));

            document.getElementById('NETVALUE').value = Number($('#ED').val()) + Number($('#GrandTotal').val() * 0.055);
            //  NETVALUE
        }

    }
    else if (intval == 2) {

        document.getElementById('TaxType').value = "CST";

        FORM_H.style.visibility = 'hidden';

        fh.style.visibility = 'hidden';


        if (Number($('#ED').val()) > 0) {

            document.getElementById('NETVALUE').value = 0;
            document.getElementById('NETVALUE').value = Number($('#ED').val()) + (Number($('#ED').val() * 0.02));
        }

        else if (Number($('#ED').val()) == 0) {
            document.getElementById('ED').value = Number($('#GrandTotal').val()) + (Number($('#GrandTotal').val() * 0.06));
            document.getElementById('NETVALUE').value = Number($('#ED').val()) + Number($('#GrandTotal').val() * 0.02);
        }

    }

 
}

