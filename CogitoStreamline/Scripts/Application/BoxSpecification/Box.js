﻿
function layer(gsm, bf, color, rate, Fk_Material, Weight, Tk_Factor, deckel, mill, Rate) {
    this.id = null;
    this.gsm = ko.observable(gsm);
    this.bf = ko.observable(bf);
    this.color = ko.observable(color);
    this.deckel = ko.observable(deckel);
    this.mill = ko.observable(mill);
    this.rate = ko.observable(rate);
    this.Fk_Material = ko.observable(Fk_Material);
    this.Weight = ko.observable(Weight);
    //this.otakef = ko.observable(otakef);
    this.Rate = ko.observable(Rate);
}
function part() {
    this.id = null;
    this.layers = Array();
    this.length = ko.observable();
    this.width = ko.observable();
    this.height = ko.observable();
    //this.otakef = ko.observable();
    this.boardArea = ko.observable();
    this.deckle = ko.observable();
    this.cuttingSize = ko.observable();
    this.quantity = ko.observable(1);
    this.noBoards = ko.observable();
    this.weight = ko.observable();
    this.Rate = ko.observable();
    this.rate = ko.observable();
    this.TakeUpfactor = ko.observable();
    this.PlyVal = ko.observable();
    this.PName = ko.observable();
    this.OLength1 = ko.observable();
    this.OWidth1 = ko.observable();
    this.OHeight1 = ko.observable();
    this.AddBLength = ko.observable();
    this.BoardBS = ko.observable();
    this.BoardGSM = ko.observable();
    //this.Rate = ko.observable();

    this.addNewChild = function (sChildType) {
        if (sChildType == "layers") {
            var lyr = new layer(0, 0, "N", 0);
            this.addLayer(lyr);
            return lyr;
        }

        return null;
    }

    this.addLayer = function (layer) {
        this.layers.push(layer);
    }
}

function Estimation() {
    this.id = null;
    this.plateYes = ko.observable();
    this.widthPartationYes = ko.observable();
    this.lenghtPartationYes = ko.observable();
    this.outerShellYes = ko.observable();
    this.capYes = ko.observable();
    this.topYes = ko.observable();
    this.bottomYes = ko.observable();
    this.sleeveYes = ko.observable();
    this.outerShell = ko.observable();
    this.sleeve = ko.observable();
    this.plate = ko.observable();
    this.widthPartation = ko.observable();
    this.lenghtPartation = ko.observable();
    this.cap = ko.observable();
    this.topval = ko.observable();
    this.bottval = ko.observable();
    this.parts = Array();
    this.Fk_BoxID = null;
    this.convRate = ko.observable(9.5);
    this.convValue = ko.observable();
    this.gMarginPercentage = ko.observable(15);
    this.gMarginValue = ko.observable(0);
    this.taxesPercntage = ko.observable(12);
    this.taxesValue = ko.observable();
    this.transportValue = ko.observable();
    this.weightHValue = ko.observable();
    this.handlingChanrgesValue = ko.observable();
    this.packingChargesValue = ko.observable();
    this.rejectionPercentage = ko.observable(5);
    this.rejectionValue = ko.observable();
    this.totalWeight = ko.observable();
    this.totalPrice = ko.observable();
    this.partsCopy = null;

    this.addParts = function (part) {
        this.parts.push(part);
    };

    this.addNewChild = function (sChildType) {
        if (sChildType == "parts") {
            var prt = new part();
            this.addParts(prt);
            return prt;
        }

        return null;
    }

    this.save = function () {

        var ToPass = ko.toJSON(this);

        var sPath = "";

        //if ($("#frmHost").validationEngine('validate')) {
        _util.showMessageDialog("", false);

        if (this.ID == null) {
            sPath = _comLayer.buildURL(this.modelName, "BoxSpec", "Save");
        }
        else { sPath = _comLayer.buildURL(this.modelName, "BoxSpec", "Update"); }

        _comLayer.parameters.clear();
        _comLayer.parameters.setJSON(ToPass);


        var oResult = _comLayer.executeSyncAction(sPath, _comLayer.parameters);

        if (oResult.Success) {
            //  _util.showSuccess("Data Saved successfully", function () { document.location.reload(); });
            _util.showSuccess("Data Saved successfully", function () { document.location.href = "/BoxMaster"; });
        }
        else {
            _util.showError("Unable to save, please contact administrator", function () { });
        }
        //}
    };
    this.back = function () {


        window.open("/BoxMaster");

        //var ToPass = ko.toJSON(this);
        //var sPath = "";

        ////if ($("#frmHost").validationEngine('validate')) {
        //_util.showMessageDialog("", false);

        //if (this.ID == null) {
        //    sPath = _comLayer.buildURL(this.modelName, "BoxSpec", "Save");
        //}
        //else { sPath = _comLayer.buildURL(this.modelName, "BoxSpec", "Update"); }

        //_comLayer.parameters.clear();
        //_comLayer.parameters.setJSON(ToPass);

        //var oResult = _comLayer.executeSyncAction(sPath, _comLayer.parameters);

        //if (oResult.Success) {
        //    //  _util.showSuccess("Data Saved successfully", function () { document.location.reload(); });
        //    _util.showSuccess("Data Saved successfully", function () { document.location.href = "/BoxMaster"; });
        //}
        //else {
        //    _util.showError("Unable save please contact administrator", function () { });
        //}
        ////}
    };
    this.deleteRecord = function () {
        if (_util.fireSubscribedHooks(this.hookSubscribers, 'beforeModelDelete', { Name: this.modelName, Tag: this.modelTag })) {
            var ToPass = ko.toJSON(this.data);
            var sPath = "";

            if (this.id != null) {
                sPath = _comLayer.buildURL(this.modelName, this.modelTag, "Delete");
                _comLayer.parameters.clear();
                _comLayer.parameters.setJSON(ToPass);

                var oResult = _comLayer.executeSyncAction(sPath, _comLayer.parameters);

                _util.fireSubscribedHooks(this.hookSubscribers, 'afterModelDelete', { result: oResult });
            }

        };
    };

    this.fetch = function () {
        if (this.Fk_BoxID == null || this.Fk_BoxID == "") return;
        var oResult = this._getDataFromSource(null);

        if (oResult.success == true) {
            _util.deepObjectCopy(oResult.data, this);
            return true;
        }

        return false;
    };

    this._getDataFromSource = function (filters) {
        //if filters are set just go with the filters
        if (filters != null) {
            _comLayer.parameters = filters;
        }
        else {

            _comLayer.parameters.clear();
            _comLayer.parameters.add("BoxId", this.Fk_BoxID);
        }

        var sPath = _comLayer.buildURL(this.modelName, "BoxSpec", "LoadBySpecID");
        var oResult = _comLayer.executeSyncAction(sPath, _comLayer.parameters);

        var oResultObject = oResult;

        return oResultObject;

    };


}