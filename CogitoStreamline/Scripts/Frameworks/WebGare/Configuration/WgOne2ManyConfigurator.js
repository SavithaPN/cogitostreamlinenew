﻿var objContext = null;
var objContextEdit = false;
var objContextEditValue = false;

function configureOne2Many(bntAddItem, tlbGrid, bntSave, viewObject, property, tag, subViewName, p_objContext) {
    that = viewObject;

    $(bntAddItem).click(function (e) {
        e.preventDefault();
        objContextEdit = false;
        objContext = p_objContext();
        showDialog(objContext, viewObject, tag, subViewName, property, bntSave, tlbGrid);
    });

    if (typeof (viewObject.viewModel.data[property]) == "undefined" || viewObject.viewModel.data[property]() == "")
    { viewObject.viewModel.data[property] = ko.observableArray(); }

     $(tlbGrid).jtable('subscribeHook', "onEditRecord", function (params) {
        objContextEdit = true;
        objContext = viewObject.viewModel.data[property]()[params.keyValue];

        var sBeforeEditMessage = null;

        try{
            sBeforeEditMessage = beforeOneToManyEditHook(objContext);
        }
        catch(e) {}

        if (sBeforeEditMessage ==null) {
            objContextEditValue = viewObject.viewModel.data[property]()[params.keyValue];
       
            showDialog(objContext, viewObject, tag, subViewName, property, bntSave, tlbGrid);
        }
        else {
            alert(sBeforeEditMessage);
        }
    });

    $(tlbGrid).jtable('subscribeHook', "onDeleteRecord", function (params) {

        alertify.confirm("Are you sure you want to delete?", function (e) {
            if (e) {

                var objContext = viewObject.viewModel.data[property]()[params.keyValue];

                var sBeforeDeleteMessage = null;

                try {
                    sBeforeDeleteMessage = beforeOneToManyDeleteHook(objContext);
                }
                catch (e) { }

                if (sBeforeDeleteMessage == null) {
                    viewObject.viewModel.data[property].remove(objContext);

                    $(tlbGrid).jtable('load', {
                        data: ko.toJSON(that.viewModel.data[property]())
                    });

                }
                else {
                   // alert(sBeforeDeleteMessage);
                    if (sBeforeDeleteMessage == true)
                    {
                        viewObject.viewModel.data[property].remove(objContext);

                        $(tlbGrid).jtable('load', {
                            data: ko.toJSON(that.viewModel.data[property]())
                        });
                    }
                }
            }
            else {

            }
        });

    });


    $(tlbGrid).jtable('load', {
        data: ko.toJSON(viewObject.viewModel.data[property]())
    });
}

function onShotcutShow(viewObject, property, saveButton, tlbGrid, objContext) {

    if (typeof (that.viewModel.data[property]) == "undefined")
    { that.viewModel.data[property] = ko.observableArray(); }


    $(saveButton).click(function (e) {
        e.preventDefault();

        var bSaveRow = true;
        try {
            bSaveRow = beforeOneToManySaveHook(objContext);
        }
        catch (e) { }

        if ($('#frmDataDialog').validationEngine('validate') && bSaveRow) {
            if (that.viewModel.data[property]().indexOf(objContext) < 0) {

                that.viewModel.data[property].push(objContext);
            }

            try {
                objContext.updateValues();
            } catch (e) { }

            $(tlbGrid).jtable('load', {
                data: ko.toJSON(that.viewModel.data[property]())
            });

            $("#dataDialog").modal("hide");
        }
    });
}

function showDialog(objContext, viewObject, tag, subViewName, property, savebutton, tlbGrid) {
   
    _util.displayView(tag, subViewName, "dataDialogArea");
    
    $("#dataDialog").modal("show");

    try {
        beforeOneToManyDataBind(property);
    }
    catch (e) { }

    ko.cleanNode(document.getElementById("dataDialogArea"));
    ko.applyBindings(objContext, document.getElementById("dataDialogArea"));

    onShotcutShow(viewObject, property, savebutton, tlbGrid, objContext);
    
    $('#dataDialog').on('hide', function () {
        $('#frmDataDialog').validationEngine('hide')
    });


    try {
        
        afterOneToManyDialogShow(property);
    }
    catch (e) { }

}





