﻿function TESTConfiguration() {

    this.configure = function (pageTag) {
        var oTestView = new WgView("test", pageTag, "divDataArea", {}, null);
        oTestView.viewModel.subscribeHook('beforeModelSave', this.beforeModelSave);
        //this is the service main path
        //oTestView.viewModel.modelName = "desktopservice";
        _page.addView(oTestView);
        _page.showView(_page.views[0].viewName);
        
        //		var filters = new WgParameters();
        //		filters.add("userid", 1);
        //		filters.add("moduleid", 1);
        //		oTestView.viewModel.filters = filters;
        //		oTestView.dataBind();
        //		
        //		$("#frmHost").validationEngine();
    };
}