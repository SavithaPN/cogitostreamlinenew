
function WgView(pName,pTag,pTarget,pActions,pOptions)
{
    this.viewName = pName;
	this.viewTag = pTag;
	this.viewTarget = pTarget;
	this.options = pOptions;
	this.dependentViews = new Array();
	this.viewLoaded = false;
	this.hookSubscribers = new Array(); 
	
	if(this.viewTarget == null || typeof this.viewTarget == 'undefined') this.viewTarget = 'divDataArea';
	
	//to be used later
	this.viewActions = pActions;
	
	this.viewModel = new WgViewModel(this.viewName,null,this.viewTag);
	
	this.layout = null;
	
	this.subscribeHook = function(pHookName,pCallbackFunction)
	{
		var hookSub = {Hook: pHookName, CallBack: pCallbackFunction};
		this.hookSubscribers.push(hookSub);
	};
	
	this.fadeInView = function()
	{
		$("#"+this.viewTarget).fadeIn("slow");
	};
	
	this.handelDependentViews = function()
	{
		var i=0;
		while(this.dependentViews[i])
		{
			try
				{
				if(this.dependentViews[i].action == "hide")
				{
					var that = this;
					$("#"+this.dependentViews[i].view.viewTarget).fadeOut("slow",function() {
						that.fadeInView();
					});
				}
				else
				{
					$("#"+this.dependentViews[i].view.viewTarget).fadeIn("slow");
				}
			}catch(e){}
			i++;
		}
		
		
	};


	this.render = function () {
	    //in this case just show the target
	    if (this.options != null && this.options.reloadOnShow == false && this.viewLoaded) {
	        this.handelDependentViews();
	        _util.fireSubscribedHooks(this.hookSubscribers, 'viewActivated', { Name: this.viewName });
	        return;
	    }

	    this.clearBindings();

	    if (g_isPartialViewSupported) {
	        _comLayer.parameters.clear();
	        var viewName = "_" + this.viewName;

	        try {
	            if (this.options.subViewName != undefined) {
	                viewName = "_" + this.options.subViewName;
	            }

	        } catch (e) { }

	        _comLayer.parameters.add("ViewName", viewName);

	        if (this.options != null) {
	            _comLayer.parameters.add("Context", this.options.context);
	        }
	        var sPath = _comLayer.buildURL(this.viewTag, "LoadViewLayout");
	        var oResult = _comLayer.executeSyncAction(sPath, _comLayer.parameters);

	        if (oResult.success == true) {
	            this.layout = oResult.data;
	            $('#' + this.viewTarget).empty();
	            $('#' + this.viewTarget).html(this.layout);
	            this.viewLoaded = true;
	        }
	    }
	    else {
	        var sPath = _comLayer.buildURL(this.viewTag, "_" + this.viewName + ".html");

	        var oResult = _comLayer.getPartialLayout(sPath);

	        if (oResult) {
	            this.layout = oResult;
	            $('#' + this.viewTarget).html(this.layout);
	            this.viewLoaded = true;
	        }
	    }

	    this.handelDependentViews();

	    //Set up datetime pickers
	    try {
	        $(".datepicker").datepicker({ dateFormat: "dd/MM/yyyy" });
	    }
	    catch (e) { }

	    _util.fireSubscribedHooks(this.hookSubscribers, 'afterRender', { Name: this.viewName });
	};
	
	this.dataBind = function()
	{
		//Fetch the data
		try{
		this.viewModel.fetch();
		}catch(e) {}
		
		//bind the data
		ko.applyBindings(this.viewModel,document.getElementById(this.viewTarget));
	};
	
	this.clearBindings = function()
	{
		this.viewModel.clear();
		ko.cleanNode(document.getElementById(this.viewTarget));
		
	};
	
}