﻿wgDragLists = new Object();

$(document).ready(function () {

    
});

$.fn.wgDragList = function (options) {

    wgDragLists[this[0].id] = this;

    this.options = $.extend({
        loadUrl: "/LoadList",
        filters: null
    }, options);

    this.actions = $.extend({
        _getDataFromSource: function (filters, url) {
            _comLayer.parameters.clear();

            if (filters != null) {
                _comLayer.parameters = filters;
            }

            var oResult = _comLayer.executeSyncAction(url, _comLayer.parameters);

            var oResultObject = oResult;

            return oResultObject;

        }
    });

    //add a drag container
    var list = $("<ul></ul>").addClass("wgDragList");

    var oResult = this.actions._getDataFromSource(this.options.filters, this.options.loadUrl);

    $.each(oResult, function (index, item) {
        var listItem = $("<li dbId ='" + item.Id +"' title='" + item.Details + "'>" + item.Name + "<br>" + item.Value + "</li>").addClass("ui-state-default ui-widget-content");
        list.append(listItem);
        listItem.draggable({
            helper: "clone"
        });
        listItem.tooltip();
    });

    list.selectable();

    $(this).append(list);

    return this;
}