﻿/* Helps to show serarch dialog to search one are many elements
   Generally used with parent and child records */
$(document).ready(function () {

    //Append the dialog at runtime
    var frm = $("<form id='frmSelect'></form>");
    var div = $("<div id='searchDialog' class='modal hide fade' tabindex='-1' role='dialog' aria-hidden='true'></div>");
    var tbl = $("<table cellpadding='5' cellspacing='0' border='0' width='100%'></table>");

    var str = "<tr><td align='left' colspan='2'>"
              + "<div id='divErrorSearch' style='display:none;'><img src='../../Images/Wrong.jpg' height=20; width=20 />Copy failed !</div>"
              + "<div id='divSuccessSearch' style='display:none;'>&nbsp;&nbsp;"
              + "<img src='../../Images/Tick.jpg' height=20; width=20 />&nbsp;&nbsp;Copy successful you may search another or click done</div>"
              + "</td></tr>";

    var tr = $(str);
    var tr2 = $("<tr><td>&nbsp;</td><td style='width:95%'><div id='dlgSearchContent'></div></td></tr>");
    var tr3 = $("<tr><td align='right' colspan='2'><button type='submit' class='btn' id='cmdCloseSearchDialog'>Close</button></td></tr>");

    tbl.append(tr);
    tbl.append(tr2);
    tbl.append(tr3);

    div.append(tbl);

    frm.append(div);

    $('body').append(frm);

    $('#cmdCloseSearchDialog').click(function (e) {
        e.preventDefault();
        $("#searchDialog").modal('hide');
    });
});

function WgMultiSelectSearch() {

    this._selectionOptions = null;
    this._executionFailed = false;

    this.hookSubscribers = new Array();

    jQuery('#frmHost').validationEngine('hide');

    this.subscribeHook = function (pHookName, pCallbackFunction) {
        var hookSub = { Hook: pHookName, CallBack: pCallbackFunction }
        this.hookSubscribers.push(hookSub);
    };

    this.showSearchSelection = function (options) {
        if (this._selectionOptions == null) {
            this._selectionOptions = options;
            this._selectionOptions.div = '#' + options.div;
            this._selectionOptions.searchButton = "#" + options.searchButton;
            this._selectionOptions.saveButton = "#" + options.saveButton;
            this._selectionOptions.doneButton = "#" + options.doneButton;
        }
       
        _util.fireSubscribedHooks(this.hookSubscribers, 'search_beforeDialogShow', this._selectionOptions)
       
        _comLayer.parameters.clear();
        var viewName = "_" + options.showView;
        _comLayer.parameters.add("ViewName", viewName);
        var sPath = _comLayer.buildURL(options.wgTag, "LoadViewLayout");
        var layout = _comLayer.executeSyncAction(sPath, _comLayer.parameters);
             
        this._resumeShowSelection(layout);

    }

    this._resumeShowSelection = function (layout) {
        $('#dlgSearchContent').html(layout.data);
        var searchDialog = this;
        this._setUpSearchDialog(this._selectionOptions.div, this._selectionOptions.jTableAction, this._selectionOptions.jtableFields, this._selectionOptions.multiSelection);

        $('#searchDialog').modal('show');

        $(this._selectionOptions.searchButton).click(function (e) {
            e.preventDefault();
            $('#divSuccessSearch').hide();
            $(searchDialog._selectionOptions.div).jtable('load', searchDialog._selectionOptions.searchFields());
        });

        $(this._selectionOptions.saveButton).click(function (e) {
            e.preventDefault();
          
            _util.fireSubscribedHooks(this.hookSubscribers, 'search_beforeExecuteSave', this._selectionOptions)
        

            var selectedRows = searchDialog.getAllselectedKeys(searchDialog);

            if (selectedRows != null && selectedRows != "") {
                try {
                    var pageTag = $('#divMainSection').attr('wgTag');
                    _comLayer.parameters.clear();
                    _comLayer.parameters.add("Basket", options.basket);
                    _comLayer.parameters.add("List", selectedRows);
                    var sPath = _comLayer.buildURL(pageTag, "AddDetail");
                    var oResult = _comLayer.executeSyncAction(sPath, _comLayer.parameters);
                } catch (e) { searchDialog._executionFailed = true; }

                searchDialog._actionExecuted(searchDialog);
            }
        });

        $(this._selectionOptions.doneButton).click(function (e) {
            e.preventDefault();
            $("#searchDialog").modal('hide');
        });

        _util.fireSubscribedHooks(this.hookSubscribers, 'search_afterDialogShow', this._selectionOptions)
       
    }

    this.getAllselectedKeys = function (searchDialog) {
        var $selectedRows = $(searchDialog._selectionOptions.div).jtable('selectedRows');
        var allSelectedRows = null;
        if ($selectedRows.length > 0) {
            //Show selected rows
            $selectedRows.each(function () {
                var record = $(this).data('record');

                $.each(searchDialog._selectionOptions.jtableFields, function (name, props) {

                    if (props.key == true) {
                        var Pid = record[name];
                        if (allSelectedRows == null) {
                            allSelectedRows = Pid;
                        }
                        else {
                            allSelectedRows += "~" + Pid;
                        }
                    }

                });
            });
        }

        return allSelectedRows;
    }

    this._actionExecuted = function (searchDialog) {
        try {
            var options = searchDialog._selectionOptions;
            var selectedRows = $(searchDialog._selectionOptions.div).jtable('selectedRows');
            _util.fireSubscribedHooks(this.hookSubscribers, 'search_afterExecuteSave', {options: options, selectedRows, rows: selectedRows});
        } catch (e) { }

        //if multi selection then this is allowed then Show status if Not close dialog
        if (searchDialog._selectionOptions.multiSelection) {
            $('#divSuccessSearch').show();
        }
        else {
            $("#searchDialog").modal('hide');
        }
    }

    this._setUpSearchDialog = function (divName, jtableAction, jtableFields, multiSelect) {
        $(divName).jtable({
            title: 'Search Items',
            paging: true,
            pageSize: 5,
            selecting: true,
            multiselect: multiSelect,
            selectingCheckboxes: true,
            actions: {
                listAction: jtableAction
            },
            fields: jtableFields
        });

    }
}
