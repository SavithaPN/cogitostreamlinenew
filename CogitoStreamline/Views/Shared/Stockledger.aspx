﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta name="viewport" content="width=device-width" />
    <title>Stock Ledger</title>
    <script runat="server">
        void Page_Load(Object Sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                List<CogitoStreamLineModel.vw_StockLedger> oStockLedger = null;
                using (CogitoStreamLineModel.CogitoStreamLineEntities dc = new CogitoStreamLineModel.CogitoStreamLineEntities())
                {
                    int tantid = Convert.ToInt32(HttpContext.Current.Session["Tanent"]);
                    oStockLedger = dc.vw_StockLedger.Where(s=>s.Fk_Tanent==tantid).OrderBy(s => s.Pk_Material).ToList();
                    ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Report/StockLedger.rdlc");
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rdc = new ReportDataSource("ReportDataSet", oStockLedger);
                    ReportViewer1.LocalReport.DataSources.Add(rdc);
                    ReportViewer1.LocalReport.Refresh();
                }
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" AsyncRendering="false" SizeToReportContent="true" Height="572px" Width="1112px">
        </rsweb:ReportViewer>
        
    </div>
    </form>
</body>
</html>
