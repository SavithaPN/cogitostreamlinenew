(function ($) {
   
    $.jqChartDateFormat = {
        dayNames: [
            "Ne", "Po", "Ut", "St", "Št", "Pi", "So",
            "Nedela", "Pondelok", "Utorok", "Streda", "Štvrtok", "Piatek", "Sobota"
        ],
        monthNames: [
            "Jan", "Feb", "Mar", "Apr", "Máj", "Jún", "Júl", "Aug", "Sep", "Okt", "Nov", "Dec",
            "Január", "Február", "Marec", "Apríl", "Máj", "Jún", "Júl", "August", "September", "Október", "November", "December"
        ],
        amPm: ["do", "od", "DO", "OD"],
        s: function (j) { return j < 11 || j > 13 ? ['st', 'nd', 'rd', 'th'][Math.min((j - 1) % 10, 3)] : 'th' },
        masks: {
            shortDate: "m/d/yyyy",
            shortTime: "h:MM TT",
            longTime: "h:MM:ss TT"
        }
    };
})(jQuery);
