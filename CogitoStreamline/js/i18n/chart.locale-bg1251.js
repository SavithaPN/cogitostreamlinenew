(function ($) {
  
    $.jqChartDateFormat = {
        dayNames: [
            "���", "���", "��", "��", "���", "���", "���",
            "������", "����������", "�������", "�����", "���������", "�����", "������"
        ],
        monthNames: [
            "���", "���", "���", "���", "���", "���", "���", "���", "���", "���", "���", "���",
            "������", "��������", "����", "�����", "���", "���", "���", "������", "���������", "��������", "�������", "��������"
        ],
        amPm: ["", "", "", ""],
        s: function (j) {
            if (j == 7 || j == 8 || j == 27 || j == 28) {
                return '��';
            }
            return ['��', '��', '��'][Math.min((j - 1) % 10, 2)];
        },
        masks: {
            shortDate: "d/m/yyyy",
            shortTime: "H:MM",
            longTime: "H:MM:ss"
        }
    };
})(jQuery);
