
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 10/03/2019 14:32:32
-- Generated from EDMX file: D:\ERP BK\bk\V2 cU 03072019 before inclg prd group master\ERP\CogitoStreamLineModel\CogitoSteamLineEntities.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [BalajiTesting];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_att_RegisterAttendance_att_RegisterMain]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[att_RegisterAttendance] DROP CONSTRAINT [FK_att_RegisterAttendance_att_RegisterMain];
GO
IF OBJECT_ID(N'[dbo].[FK_att_RegisterAttendance_emp_EmployeeMaster]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[att_RegisterAttendance] DROP CONSTRAINT [FK_att_RegisterAttendance_emp_EmployeeMaster];
GO
IF OBJECT_ID(N'[dbo].[FK_Bank_AccountType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Bank] DROP CONSTRAINT [FK_Bank_AccountType];
GO
IF OBJECT_ID(N'[dbo].[FK_BoardDesc_Inv_Material]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[BoardDesc] DROP CONSTRAINT [FK_BoardDesc_Inv_Material];
GO
IF OBJECT_ID(N'[dbo].[FK_Box_Documents_BoxMaster]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Box_Documents] DROP CONSTRAINT [FK_Box_Documents_BoxMaster];
GO
IF OBJECT_ID(N'[dbo].[FK_BoxMaster_BoxType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[BoxMaster] DROP CONSTRAINT [FK_BoxMaster_BoxType];
GO
IF OBJECT_ID(N'[dbo].[FK_BoxMaster_FluteType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[BoxMaster] DROP CONSTRAINT [FK_BoxMaster_FluteType];
GO
IF OBJECT_ID(N'[dbo].[FK_BoxMaster_gen_Customer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[BoxMaster] DROP CONSTRAINT [FK_BoxMaster_gen_Customer];
GO
IF OBJECT_ID(N'[dbo].[FK_BoxSpecs_BoxMast]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[BoxSpecs] DROP CONSTRAINT [FK_BoxSpecs_BoxMast];
GO
IF OBJECT_ID(N'[dbo].[FK_BoxStock_BoxStock]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[BoxStock] DROP CONSTRAINT [FK_BoxStock_BoxStock];
GO
IF OBJECT_ID(N'[dbo].[FK_BoxStock_ItemPartProperty]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[BoxStock] DROP CONSTRAINT [FK_BoxStock_ItemPartProperty];
GO
IF OBJECT_ID(N'[dbo].[FK_Branch_wgTenant]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Branch] DROP CONSTRAINT [FK_Branch_wgTenant];
GO
IF OBJECT_ID(N'[dbo].[FK_COA_Inv_Billing]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[COA] DROP CONSTRAINT [FK_COA_Inv_Billing];
GO
IF OBJECT_ID(N'[dbo].[FK_COADetails_COA]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[COADetails] DROP CONSTRAINT [FK_COADetails_COA];
GO
IF OBJECT_ID(N'[dbo].[FK_COADetails_COA_Parameters]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[COADetails] DROP CONSTRAINT [FK_COADetails_COA_Parameters];
GO
IF OBJECT_ID(N'[dbo].[FK_ConsumableIssueDet_ConsumablesIssue]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ConsumableIssueDet] DROP CONSTRAINT [FK_ConsumableIssueDet_ConsumablesIssue];
GO
IF OBJECT_ID(N'[dbo].[FK_ConsumableIssueDet_Inv_Material]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ConsumableIssueDet] DROP CONSTRAINT [FK_ConsumableIssueDet_Inv_Material];
GO
IF OBJECT_ID(N'[dbo].[FK_CustomerComplaints_gen_Customer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CustomerComplaints] DROP CONSTRAINT [FK_CustomerComplaints_gen_Customer];
GO
IF OBJECT_ID(N'[dbo].[FK_CustomerComplaints_gen_Order]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CustomerComplaints] DROP CONSTRAINT [FK_CustomerComplaints_gen_Order];
GO
IF OBJECT_ID(N'[dbo].[FK_CustomerRejections_gen_Customer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CustomerRejections] DROP CONSTRAINT [FK_CustomerRejections_gen_Customer];
GO
IF OBJECT_ID(N'[dbo].[FK_CustomerRejections_gen_Order]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CustomerRejections] DROP CONSTRAINT [FK_CustomerRejections_gen_Order];
GO
IF OBJECT_ID(N'[dbo].[FK_DispatchChild_BoxMaster]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DispatchChild] DROP CONSTRAINT [FK_DispatchChild_BoxMaster];
GO
IF OBJECT_ID(N'[dbo].[FK_DispatchChild_DispatchMast]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DispatchChild] DROP CONSTRAINT [FK_DispatchChild_DispatchMast];
GO
IF OBJECT_ID(N'[dbo].[FK_DispatchMast_gen_Customer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DispatchMast] DROP CONSTRAINT [FK_DispatchMast_gen_Customer];
GO
IF OBJECT_ID(N'[dbo].[FK_emp_EmployeeImage_emp_EmployeeMaster]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[emp_EmployeeImage] DROP CONSTRAINT [FK_emp_EmployeeImage_emp_EmployeeMaster];
GO
IF OBJECT_ID(N'[dbo].[FK_emp_EmployeeMaster_emp_Department]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[emp_EmployeeMaster] DROP CONSTRAINT [FK_emp_EmployeeMaster_emp_Department];
GO
IF OBJECT_ID(N'[dbo].[FK_emp_EmployeeMaster_emp_EmployeeDesignation]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[emp_EmployeeMaster] DROP CONSTRAINT [FK_emp_EmployeeMaster_emp_EmployeeDesignation];
GO
IF OBJECT_ID(N'[dbo].[FK_emp_EmployeeMaster_gen_City]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[emp_EmployeeMaster] DROP CONSTRAINT [FK_emp_EmployeeMaster_gen_City];
GO
IF OBJECT_ID(N'[dbo].[FK_emp_EmployeeMaster_gen_State]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[emp_EmployeeMaster] DROP CONSTRAINT [FK_emp_EmployeeMaster_gen_State];
GO
IF OBJECT_ID(N'[dbo].[FK_eq_Documents_eq_Enquiry]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[eq_Documents] DROP CONSTRAINT [FK_eq_Documents_eq_Enquiry];
GO
IF OBJECT_ID(N'[dbo].[FK_eq_Enquiry_Branch]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[eq_Enquiry] DROP CONSTRAINT [FK_eq_Enquiry_Branch];
GO
IF OBJECT_ID(N'[dbo].[FK_eq_Enquiry_gen_Communication]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[eq_Enquiry] DROP CONSTRAINT [FK_eq_Enquiry_gen_Communication];
GO
IF OBJECT_ID(N'[dbo].[FK_eq_Enquiry_gen_Customer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[eq_Enquiry] DROP CONSTRAINT [FK_eq_Enquiry_gen_Customer];
GO
IF OBJECT_ID(N'[dbo].[FK_eq_Enquiry_wfStates]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[eq_Enquiry] DROP CONSTRAINT [FK_eq_Enquiry_wfStates];
GO
IF OBJECT_ID(N'[dbo].[FK_eq_Enquiry_wgTenant]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[eq_Enquiry] DROP CONSTRAINT [FK_eq_Enquiry_wgTenant];
GO
IF OBJECT_ID(N'[dbo].[FK_eq_EnquiryChild_BoxMaster1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[eq_EnquiryChild] DROP CONSTRAINT [FK_eq_EnquiryChild_BoxMaster1];
GO
IF OBJECT_ID(N'[dbo].[FK_eq_EnquiryChild_eq_Enquiry1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[eq_EnquiryChild] DROP CONSTRAINT [FK_eq_EnquiryChild_eq_Enquiry1];
GO
IF OBJECT_ID(N'[dbo].[FK_est_BoxEstimationChild_eq_EnquiryChild]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[est_BoxEstimationChild] DROP CONSTRAINT [FK_est_BoxEstimationChild_eq_EnquiryChild];
GO
IF OBJECT_ID(N'[dbo].[FK_est_BoxEstimationChild_est_BoxEstimation]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[est_BoxEstimationChild] DROP CONSTRAINT [FK_est_BoxEstimationChild_est_BoxEstimation];
GO
IF OBJECT_ID(N'[dbo].[FK_est_Estimation_eq_Enquiry]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[est_Estimation] DROP CONSTRAINT [FK_est_Estimation_eq_Enquiry];
GO
IF OBJECT_ID(N'[dbo].[FK_est_EstimationPart_est_Estimation1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[est_EstimationPart] DROP CONSTRAINT [FK_est_EstimationPart_est_Estimation1];
GO
IF OBJECT_ID(N'[dbo].[FK_est_Layer_est_EstimationPart1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[est_Layer] DROP CONSTRAINT [FK_est_Layer_est_EstimationPart1];
GO
IF OBJECT_ID(N'[dbo].[FK_FinishedGoodsStock_BoxMast]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[FinishedGoodsStock] DROP CONSTRAINT [FK_FinishedGoodsStock_BoxMast];
GO
IF OBJECT_ID(N'[dbo].[FK_gen_City_gen_Country]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[gen_City] DROP CONSTRAINT [FK_gen_City_gen_Country];
GO
IF OBJECT_ID(N'[dbo].[FK_gen_City_gen_State]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[gen_City] DROP CONSTRAINT [FK_gen_City_gen_State];
GO
IF OBJECT_ID(N'[dbo].[FK_gen_ContactPerson_gen_City]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[gen_ContactPerson] DROP CONSTRAINT [FK_gen_ContactPerson_gen_City];
GO
IF OBJECT_ID(N'[dbo].[FK_gen_ContactPerson_gen_State]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[gen_ContactPerson] DROP CONSTRAINT [FK_gen_ContactPerson_gen_State];
GO
IF OBJECT_ID(N'[dbo].[FK_gen_Customer_gen_City]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[gen_Customer] DROP CONSTRAINT [FK_gen_Customer_gen_City];
GO
IF OBJECT_ID(N'[dbo].[FK_gen_Customer_gen_Country]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[gen_Customer] DROP CONSTRAINT [FK_gen_Customer_gen_Country];
GO
IF OBJECT_ID(N'[dbo].[FK_gen_Customer_gen_State]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[gen_Customer] DROP CONSTRAINT [FK_gen_Customer_gen_State];
GO
IF OBJECT_ID(N'[dbo].[FK_gen_CustomerContacts_gen_ContactPerson]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[gen_CustomerContacts] DROP CONSTRAINT [FK_gen_CustomerContacts_gen_ContactPerson];
GO
IF OBJECT_ID(N'[dbo].[FK_gen_CustomerContacts_gen_Customer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[gen_CustomerContacts] DROP CONSTRAINT [FK_gen_CustomerContacts_gen_Customer];
GO
IF OBJECT_ID(N'[dbo].[FK_gen_CustomerShippingDetails_gen_Customer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[gen_CustomerShippingDetails] DROP CONSTRAINT [FK_gen_CustomerShippingDetails_gen_Customer];
GO
IF OBJECT_ID(N'[dbo].[FK_gen_DeliverySchedule_BoxMaster]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[gen_DeliverySchedule] DROP CONSTRAINT [FK_gen_DeliverySchedule_BoxMaster];
GO
IF OBJECT_ID(N'[dbo].[FK_gen_DeliverySchedule_gen_Order]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[gen_DeliverySchedule] DROP CONSTRAINT [FK_gen_DeliverySchedule_gen_Order];
GO
IF OBJECT_ID(N'[dbo].[FK_gen_DeliverySchedule_Inv_Material]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[gen_DeliverySchedule] DROP CONSTRAINT [FK_gen_DeliverySchedule_Inv_Material];
GO
IF OBJECT_ID(N'[dbo].[FK_gen_DeliverySchedule_ItemPartProperty]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[gen_DeliverySchedule] DROP CONSTRAINT [FK_gen_DeliverySchedule_ItemPartProperty];
GO
IF OBJECT_ID(N'[dbo].[FK_gen_Events_gen_DeliverySchedule]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[gen_Events] DROP CONSTRAINT [FK_gen_Events_gen_DeliverySchedule];
GO
IF OBJECT_ID(N'[dbo].[FK_gen_MillContacts_gen_ContactPerson]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[gen_MillContacts] DROP CONSTRAINT [FK_gen_MillContacts_gen_ContactPerson];
GO
IF OBJECT_ID(N'[dbo].[FK_gen_MillContacts_gen_Mill]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[gen_MillContacts] DROP CONSTRAINT [FK_gen_MillContacts_gen_Mill];
GO
IF OBJECT_ID(N'[dbo].[FK_gen_Order_eq_Enquiry1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[gen_Order] DROP CONSTRAINT [FK_gen_Order_eq_Enquiry1];
GO
IF OBJECT_ID(N'[dbo].[FK_gen_Order_gen_Customer1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[gen_Order] DROP CONSTRAINT [FK_gen_Order_gen_Customer1];
GO
IF OBJECT_ID(N'[dbo].[FK_gen_Order_gen_CustomerShippingDetails]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[gen_Order] DROP CONSTRAINT [FK_gen_Order_gen_CustomerShippingDetails];
GO
IF OBJECT_ID(N'[dbo].[FK_gen_Order_wfStates]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[gen_Order] DROP CONSTRAINT [FK_gen_Order_wfStates];
GO
IF OBJECT_ID(N'[dbo].[FK_Gen_OrderChild_BoxMaster]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Gen_OrderChild] DROP CONSTRAINT [FK_Gen_OrderChild_BoxMaster];
GO
IF OBJECT_ID(N'[dbo].[FK_Gen_OrderChild_gen_Order1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Gen_OrderChild] DROP CONSTRAINT [FK_Gen_OrderChild_gen_Order1];
GO
IF OBJECT_ID(N'[dbo].[FK_Gen_OrderChild_Inv_Material]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Gen_OrderChild] DROP CONSTRAINT [FK_Gen_OrderChild_Inv_Material];
GO
IF OBJECT_ID(N'[dbo].[FK_Gen_OrderChild_ItemPartProperty]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Gen_OrderChild] DROP CONSTRAINT [FK_Gen_OrderChild_ItemPartProperty];
GO
IF OBJECT_ID(N'[dbo].[FK_gen_State_gen_Country]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[gen_State] DROP CONSTRAINT [FK_gen_State_gen_Country];
GO
IF OBJECT_ID(N'[dbo].[FK_gen_Vendor_gen_City]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[gen_Vendor] DROP CONSTRAINT [FK_gen_Vendor_gen_City];
GO
IF OBJECT_ID(N'[dbo].[FK_gen_Vendor_gen_Country]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[gen_Vendor] DROP CONSTRAINT [FK_gen_Vendor_gen_Country];
GO
IF OBJECT_ID(N'[dbo].[FK_gen_Vendor_gen_State]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[gen_Vendor] DROP CONSTRAINT [FK_gen_Vendor_gen_State];
GO
IF OBJECT_ID(N'[dbo].[FK_gen_VendorContacts_gen_ContactPerson]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[gen_VendorContacts] DROP CONSTRAINT [FK_gen_VendorContacts_gen_ContactPerson];
GO
IF OBJECT_ID(N'[dbo].[FK_gen_VendorContacts_gen_Vendor]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[gen_VendorContacts] DROP CONSTRAINT [FK_gen_VendorContacts_gen_Vendor];
GO
IF OBJECT_ID(N'[dbo].[FK_gen_VendorMaterials_gen_Vendor]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[gen_VendorMaterials] DROP CONSTRAINT [FK_gen_VendorMaterials_gen_Vendor];
GO
IF OBJECT_ID(N'[dbo].[FK_gen_VendorMaterials_Inv_Material]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[gen_VendorMaterials] DROP CONSTRAINT [FK_gen_VendorMaterials_Inv_Material];
GO
IF OBJECT_ID(N'[dbo].[FK_GlueCertificate_gen_Vendor]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GlueCertificate] DROP CONSTRAINT [FK_GlueCertificate_gen_Vendor];
GO
IF OBJECT_ID(N'[dbo].[FK_GlueCertificate_Inv_Material]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GlueCertificate] DROP CONSTRAINT [FK_GlueCertificate_Inv_Material];
GO
IF OBJECT_ID(N'[dbo].[FK_GlueCertificateDetails_GlueCertificate]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GlueCertificateDetails] DROP CONSTRAINT [FK_GlueCertificateDetails_GlueCertificate];
GO
IF OBJECT_ID(N'[dbo].[FK_GRN_Details_GRN_Mast]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GRN_Details] DROP CONSTRAINT [FK_GRN_Details_GRN_Mast];
GO
IF OBJECT_ID(N'[dbo].[FK_GRN_Details_Inv_Material]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GRN_Details] DROP CONSTRAINT [FK_GRN_Details_Inv_Material];
GO
IF OBJECT_ID(N'[dbo].[FK_GRN_Mast_MaterialInwardM]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GRN_Mast] DROP CONSTRAINT [FK_GRN_Mast_MaterialInwardM];
GO
IF OBJECT_ID(N'[dbo].[FK_ideActivity_wgTenant]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ideActivity] DROP CONSTRAINT [FK_ideActivity_wgTenant];
GO
IF OBJECT_ID(N'[dbo].[FK_ideActivityPermission_ideActivity]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ideActivityPermission] DROP CONSTRAINT [FK_ideActivityPermission_ideActivity];
GO
IF OBJECT_ID(N'[dbo].[FK_ideActivityPermission_idePermission]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ideActivityPermission] DROP CONSTRAINT [FK_ideActivityPermission_idePermission];
GO
IF OBJECT_ID(N'[dbo].[FK_ideActivityPermission_ideRole]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ideActivityPermission] DROP CONSTRAINT [FK_ideActivityPermission_ideRole];
GO
IF OBJECT_ID(N'[dbo].[FK_ideRoleUser_ideRole]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ideRoleUser] DROP CONSTRAINT [FK_ideRoleUser_ideRole];
GO
IF OBJECT_ID(N'[dbo].[FK_ideRoleUser_ideUser]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ideRoleUser] DROP CONSTRAINT [FK_ideRoleUser_ideUser];
GO
IF OBJECT_ID(N'[CogitoModelStoreContainer].[FK_ideShortCut_ideUser]', 'F') IS NOT NULL
    ALTER TABLE [CogitoModelStoreContainer].[ideShortCut] DROP CONSTRAINT [FK_ideShortCut_ideUser];
GO
IF OBJECT_ID(N'[dbo].[FK_ideUser_Branch]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ideUser] DROP CONSTRAINT [FK_ideUser_Branch];
GO
IF OBJECT_ID(N'[dbo].[FK_ideUser_wgTenant]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ideUser] DROP CONSTRAINT [FK_ideUser_wgTenant];
GO
IF OBJECT_ID(N'[dbo].[FK_InkCertificate_Inv_Material]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[InkCertificate] DROP CONSTRAINT [FK_InkCertificate_Inv_Material];
GO
IF OBJECT_ID(N'[dbo].[FK_InkCertificateDetails_InkCertificate]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[InkCertificateDetails] DROP CONSTRAINT [FK_InkCertificateDetails_InkCertificate];
GO
IF OBJECT_ID(N'[dbo].[FK_InkCertificateDetails_InkChar]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[InkCertificateDetails] DROP CONSTRAINT [FK_InkCertificateDetails_InkChar];
GO
IF OBJECT_ID(N'[dbo].[FK_Inv_Billing_gen_Customer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Inv_Billing] DROP CONSTRAINT [FK_Inv_Billing_gen_Customer];
GO
IF OBJECT_ID(N'[dbo].[FK_Inv_Billing_gen_Order]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Inv_Billing] DROP CONSTRAINT [FK_Inv_Billing_gen_Order];
GO
IF OBJECT_ID(N'[dbo].[FK_Inv_BillingDetails_BoxMaster]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Inv_BillingDetails] DROP CONSTRAINT [FK_Inv_BillingDetails_BoxMaster];
GO
IF OBJECT_ID(N'[dbo].[FK_Inv_BillingDetails_Inv_Billing]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Inv_BillingDetails] DROP CONSTRAINT [FK_Inv_BillingDetails_Inv_Billing];
GO
IF OBJECT_ID(N'[dbo].[FK_Inv_BillingDetails_Inv_Material]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Inv_BillingDetails] DROP CONSTRAINT [FK_Inv_BillingDetails_Inv_Material];
GO
IF OBJECT_ID(N'[dbo].[FK_Inv_BillingDetails_ItemPartProperty]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Inv_BillingDetails] DROP CONSTRAINT [FK_Inv_BillingDetails_ItemPartProperty];
GO
IF OBJECT_ID(N'[dbo].[FK_Inv_Material_gen_Color]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Inv_Material] DROP CONSTRAINT [FK_Inv_Material_gen_Color];
GO
IF OBJECT_ID(N'[dbo].[FK_Inv_Material_gen_Mill]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Inv_Material] DROP CONSTRAINT [FK_Inv_Material_gen_Mill];
GO
IF OBJECT_ID(N'[dbo].[FK_Inv_Material_gen_PaperType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Inv_Material] DROP CONSTRAINT [FK_Inv_Material_gen_PaperType];
GO
IF OBJECT_ID(N'[dbo].[FK_Inv_Material_gen_Unit]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Inv_Material] DROP CONSTRAINT [FK_Inv_Material_gen_Unit];
GO
IF OBJECT_ID(N'[dbo].[FK_Inv_Material_gen_Vendor]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Inv_Material] DROP CONSTRAINT [FK_Inv_Material_gen_Vendor];
GO
IF OBJECT_ID(N'[dbo].[FK_Inv_Material_Inv_MaterialCategory]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Inv_Material] DROP CONSTRAINT [FK_Inv_Material_Inv_MaterialCategory];
GO
IF OBJECT_ID(N'[dbo].[FK_Inv_MaterialIndentDetails_gen_Mill]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Inv_MaterialIndentDetails] DROP CONSTRAINT [FK_Inv_MaterialIndentDetails_gen_Mill];
GO
IF OBJECT_ID(N'[dbo].[FK_Inv_MaterialIndentDetails_gen_Vendor]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Inv_MaterialIndentDetails] DROP CONSTRAINT [FK_Inv_MaterialIndentDetails_gen_Vendor];
GO
IF OBJECT_ID(N'[dbo].[FK_Inv_MaterialIndentDetails_Inv_Material]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Inv_MaterialIndentDetails] DROP CONSTRAINT [FK_Inv_MaterialIndentDetails_Inv_Material];
GO
IF OBJECT_ID(N'[dbo].[FK_Inv_MaterialIndentMaster_Branch]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Inv_MaterialIndentMaster] DROP CONSTRAINT [FK_Inv_MaterialIndentMaster_Branch];
GO
IF OBJECT_ID(N'[dbo].[FK_Inv_MaterialIndentMaster_ideUser]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Inv_MaterialIndentMaster] DROP CONSTRAINT [FK_Inv_MaterialIndentMaster_ideUser];
GO
IF OBJECT_ID(N'[dbo].[FK_Inv_MaterialIndentMaster_wfStates]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Inv_MaterialIndentMaster] DROP CONSTRAINT [FK_Inv_MaterialIndentMaster_wfStates];
GO
IF OBJECT_ID(N'[dbo].[FK_Inv_MaterialIndentMaster_wgTenant]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Inv_MaterialIndentMaster] DROP CONSTRAINT [FK_Inv_MaterialIndentMaster_wgTenant];
GO
IF OBJECT_ID(N'[dbo].[FK_Inv_PaymentDetails_Inv_Billing]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Inv_PaymentDetails] DROP CONSTRAINT [FK_Inv_PaymentDetails_Inv_Billing];
GO
IF OBJECT_ID(N'[dbo].[FK_IssueReturn_ideUser]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[IssueReturn] DROP CONSTRAINT [FK_IssueReturn_ideUser];
GO
IF OBJECT_ID(N'[dbo].[FK_IssueReturnDetails_Inv_Material]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[IssueReturnDetails] DROP CONSTRAINT [FK_IssueReturnDetails_Inv_Material];
GO
IF OBJECT_ID(N'[dbo].[FK_IssueReturnDetails_IssueReturnMaster]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[IssueReturnDetails] DROP CONSTRAINT [FK_IssueReturnDetails_IssueReturnMaster];
GO
IF OBJECT_ID(N'[dbo].[FK_IssueReturnMaster_MaterialIssue]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[IssueReturn] DROP CONSTRAINT [FK_IssueReturnMaster_MaterialIssue];
GO
IF OBJECT_ID(N'[dbo].[FK_ItemPartProperty_BoxSpecs]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ItemPartProperty] DROP CONSTRAINT [FK_ItemPartProperty_BoxSpecs];
GO
IF OBJECT_ID(N'[dbo].[FK_Items_Layers]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Items_Layers] DROP CONSTRAINT [FK_Items_Layers];
GO
IF OBJECT_ID(N'[dbo].[FK_JC_Documents_JobCardMaster]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JC_Documents] DROP CONSTRAINT [FK_JC_Documents_JobCardMaster];
GO
IF OBJECT_ID(N'[dbo].[FK_JobCardDetails_Inv_Material]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JobCardDetails] DROP CONSTRAINT [FK_JobCardDetails_Inv_Material];
GO
IF OBJECT_ID(N'[dbo].[FK_JobCardDetails_JobCardMaster]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JobCardDetails] DROP CONSTRAINT [FK_JobCardDetails_JobCardMaster];
GO
IF OBJECT_ID(N'[dbo].[FK_JobCardDetails_PaperStock]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JobCardDetails] DROP CONSTRAINT [FK_JobCardDetails_PaperStock];
GO
IF OBJECT_ID(N'[dbo].[FK_JobCardMaster_BoxMaster]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JobCardMaster] DROP CONSTRAINT [FK_JobCardMaster_BoxMaster];
GO
IF OBJECT_ID(N'[dbo].[FK_JobCardMaster_gen_DeliverySchedule]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JobCardMaster] DROP CONSTRAINT [FK_JobCardMaster_gen_DeliverySchedule];
GO
IF OBJECT_ID(N'[dbo].[FK_JobCardMaster_gen_Order1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JobCardMaster] DROP CONSTRAINT [FK_JobCardMaster_gen_Order1];
GO
IF OBJECT_ID(N'[dbo].[FK_JobCardMaster_wfStates]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JobCardMaster] DROP CONSTRAINT [FK_JobCardMaster_wfStates];
GO
IF OBJECT_ID(N'[dbo].[FK_JobCardPartsDetails_Inv_Material]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JobCardPartsDetails] DROP CONSTRAINT [FK_JobCardPartsDetails_Inv_Material];
GO
IF OBJECT_ID(N'[dbo].[FK_JobCardPartsDetails_JobCardPartsMaster]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JobCardPartsDetails] DROP CONSTRAINT [FK_JobCardPartsDetails_JobCardPartsMaster];
GO
IF OBJECT_ID(N'[dbo].[FK_JobCardPartsDetails_PaperStock]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JobCardPartsDetails] DROP CONSTRAINT [FK_JobCardPartsDetails_PaperStock];
GO
IF OBJECT_ID(N'[dbo].[FK_JobCardPartsMaster_gen_DeliverySchedule]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JobCardPartsMaster] DROP CONSTRAINT [FK_JobCardPartsMaster_gen_DeliverySchedule];
GO
IF OBJECT_ID(N'[dbo].[FK_JobCardPartsMaster_gen_Order1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JobCardPartsMaster] DROP CONSTRAINT [FK_JobCardPartsMaster_gen_Order1];
GO
IF OBJECT_ID(N'[dbo].[FK_JobCardPartsMaster_wfStates]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JobCardPartsMaster] DROP CONSTRAINT [FK_JobCardPartsMaster_wfStates];
GO
IF OBJECT_ID(N'[dbo].[FK_JobProcess_JobCardMaster]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JobProcess] DROP CONSTRAINT [FK_JobProcess_JobCardMaster];
GO
IF OBJECT_ID(N'[dbo].[FK_JobProcess_ProcessMaster]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JobProcess] DROP CONSTRAINT [FK_JobProcess_ProcessMaster];
GO
IF OBJECT_ID(N'[dbo].[FK_JobProcess_ProcessMaster_Samp]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SampJobProcess] DROP CONSTRAINT [FK_JobProcess_ProcessMaster_Samp];
GO
IF OBJECT_ID(N'[dbo].[FK_JobProcess_SampJobCardMaster]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SampJobProcess] DROP CONSTRAINT [FK_JobProcess_SampJobCardMaster];
GO
IF OBJECT_ID(N'[dbo].[FK_JobWorkChild_Inv_Material]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JobWorkChild] DROP CONSTRAINT [FK_JobWorkChild_Inv_Material];
GO
IF OBJECT_ID(N'[dbo].[FK_JobWorkChild_JobWorkChild]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JobWorkChild] DROP CONSTRAINT [FK_JobWorkChild_JobWorkChild];
GO
IF OBJECT_ID(N'[dbo].[FK_JobWorkReceivables_JW_JobCardMaster]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JobWorkReceivables] DROP CONSTRAINT [FK_JobWorkReceivables_JW_JobCardMaster];
GO
IF OBJECT_ID(N'[dbo].[FK_JobWorkReceivables_wfStates]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JobWorkReceivables] DROP CONSTRAINT [FK_JobWorkReceivables_wfStates];
GO
IF OBJECT_ID(N'[dbo].[FK_JobWorkRMStock_gen_Customer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JobWorkRMStock] DROP CONSTRAINT [FK_JobWorkRMStock_gen_Customer];
GO
IF OBJECT_ID(N'[dbo].[FK_JobWorkRMStock_Inv_Material1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JobWorkRMStock] DROP CONSTRAINT [FK_JobWorkRMStock_Inv_Material1];
GO
IF OBJECT_ID(N'[dbo].[FK_JProcess_JobCardMaster]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JProcess] DROP CONSTRAINT [FK_JProcess_JobCardMaster];
GO
IF OBJECT_ID(N'[dbo].[FK_JProcess_ProcessMaster]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JProcess] DROP CONSTRAINT [FK_JProcess_ProcessMaster];
GO
IF OBJECT_ID(N'[dbo].[FK_JProcess_ProcessMaster_Samp]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SampJProcess] DROP CONSTRAINT [FK_JProcess_ProcessMaster_Samp];
GO
IF OBJECT_ID(N'[dbo].[FK_JProcess_Sample_JobCardMaster]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SampJProcess] DROP CONSTRAINT [FK_JProcess_Sample_JobCardMaster];
GO
IF OBJECT_ID(N'[dbo].[FK_JW_JobCardMaster_BoxMaster]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JW_JobCardMaster] DROP CONSTRAINT [FK_JW_JobCardMaster_BoxMaster];
GO
IF OBJECT_ID(N'[dbo].[FK_JW_JobCardMaster_gen_Order]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JW_JobCardMaster] DROP CONSTRAINT [FK_JW_JobCardMaster_gen_Order];
GO
IF OBJECT_ID(N'[dbo].[FK_JW_JobCardMaster_gen_Vendor]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JW_JobCardMaster] DROP CONSTRAINT [FK_JW_JobCardMaster_gen_Vendor];
GO
IF OBJECT_ID(N'[dbo].[FK_JW_JobCardMaster_wfStates]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JW_JobCardMaster] DROP CONSTRAINT [FK_JW_JobCardMaster_wfStates];
GO
IF OBJECT_ID(N'[dbo].[FK_JW_RM_RetDet_Inv_Material]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JW_RM_RetDet] DROP CONSTRAINT [FK_JW_RM_RetDet_Inv_Material];
GO
IF OBJECT_ID(N'[dbo].[FK_JW_RM_RetDet_JW_RM_RetMast]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JW_RM_RetDet] DROP CONSTRAINT [FK_JW_RM_RetDet_JW_RM_RetMast];
GO
IF OBJECT_ID(N'[dbo].[FK_JW_RM_RetMast_gen_Customer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JW_RM_RetMast] DROP CONSTRAINT [FK_JW_RM_RetMast_gen_Customer];
GO
IF OBJECT_ID(N'[dbo].[FK_JW_RM_RetMast_PartJobs]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JW_RM_RetMast] DROP CONSTRAINT [FK_JW_RM_RetMast_PartJobs];
GO
IF OBJECT_ID(N'[dbo].[FK_JWMat_InwdD_Inv_Material]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JWMat_InwdD] DROP CONSTRAINT [FK_JWMat_InwdD_Inv_Material];
GO
IF OBJECT_ID(N'[dbo].[FK_JWMat_InwdD_JWMat_InwdM]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JWMat_InwdD] DROP CONSTRAINT [FK_JWMat_InwdD_JWMat_InwdM];
GO
IF OBJECT_ID(N'[dbo].[FK_JWMat_InwdM_gen_Customer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JWMat_InwdM] DROP CONSTRAINT [FK_JWMat_InwdM_gen_Customer];
GO
IF OBJECT_ID(N'[dbo].[FK_JWMat_InwdM_gen_Vendor]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JWMat_InwdM] DROP CONSTRAINT [FK_JWMat_InwdM_gen_Vendor];
GO
IF OBJECT_ID(N'[dbo].[FK_JWStocks_Branch]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JWStocks] DROP CONSTRAINT [FK_JWStocks_Branch];
GO
IF OBJECT_ID(N'[dbo].[FK_JWStocks_Inv_Material]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JWStocks] DROP CONSTRAINT [FK_JWStocks_Inv_Material];
GO
IF OBJECT_ID(N'[dbo].[FK_JWStocks_wgTenant]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JWStocks] DROP CONSTRAINT [FK_JWStocks_wgTenant];
GO
IF OBJECT_ID(N'[dbo].[FK_MaterialIndent_gen_Vendor]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Inv_MaterialIndentMaster] DROP CONSTRAINT [FK_MaterialIndent_gen_Vendor];
GO
IF OBJECT_ID(N'[dbo].[FK_MaterialIndentDetails_gen_Order]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Inv_MaterialIndentDetails] DROP CONSTRAINT [FK_MaterialIndentDetails_gen_Order];
GO
IF OBJECT_ID(N'[dbo].[FK_MaterialIndentDetails_MaterialIndent]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Inv_MaterialIndentDetails] DROP CONSTRAINT [FK_MaterialIndentDetails_MaterialIndent];
GO
IF OBJECT_ID(N'[dbo].[FK_MaterialInwardD_gen_Mill]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MaterialInwardD] DROP CONSTRAINT [FK_MaterialInwardD_gen_Mill];
GO
IF OBJECT_ID(N'[dbo].[FK_MaterialInwardD_Inv_Material]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MaterialInwardD] DROP CONSTRAINT [FK_MaterialInwardD_Inv_Material];
GO
IF OBJECT_ID(N'[dbo].[FK_MaterialInwardD_MaterialInwardM]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MaterialInwardD] DROP CONSTRAINT [FK_MaterialInwardD_MaterialInwardM];
GO
IF OBJECT_ID(N'[dbo].[FK_MaterialInwardD_Tax]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MaterialInwardD] DROP CONSTRAINT [FK_MaterialInwardD_Tax];
GO
IF OBJECT_ID(N'[dbo].[FK_MaterialInwardM_ideUser]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MaterialInwardM] DROP CONSTRAINT [FK_MaterialInwardM_ideUser];
GO
IF OBJECT_ID(N'[dbo].[FK_MaterialInwardM_Inv_MaterialIndentMaster]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MaterialInwardM] DROP CONSTRAINT [FK_MaterialInwardM_Inv_MaterialIndentMaster];
GO
IF OBJECT_ID(N'[dbo].[FK_MaterialInwardM_PurchaseOrderM]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MaterialInwardM] DROP CONSTRAINT [FK_MaterialInwardM_PurchaseOrderM];
GO
IF OBJECT_ID(N'[dbo].[FK_MaterialIssue_Branch]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MaterialIssue] DROP CONSTRAINT [FK_MaterialIssue_Branch];
GO
IF OBJECT_ID(N'[dbo].[FK_MaterialIssue_gen_Order]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MaterialIssue] DROP CONSTRAINT [FK_MaterialIssue_gen_Order];
GO
IF OBJECT_ID(N'[dbo].[FK_MaterialIssue_ideUser]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MaterialIssue] DROP CONSTRAINT [FK_MaterialIssue_ideUser];
GO
IF OBJECT_ID(N'[dbo].[FK_MaterialIssue_JobCardMaster]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MaterialIssue] DROP CONSTRAINT [FK_MaterialIssue_JobCardMaster];
GO
IF OBJECT_ID(N'[dbo].[FK_MaterialIssue_wgTenant]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MaterialIssue] DROP CONSTRAINT [FK_MaterialIssue_wgTenant];
GO
IF OBJECT_ID(N'[dbo].[FK_MaterialIssue_WorkOrder]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MaterialIssue] DROP CONSTRAINT [FK_MaterialIssue_WorkOrder];
GO
IF OBJECT_ID(N'[dbo].[FK_MaterialIssueDetails_gen_Mill]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MaterialIssueDetails] DROP CONSTRAINT [FK_MaterialIssueDetails_gen_Mill];
GO
IF OBJECT_ID(N'[dbo].[FK_MaterialIssueDetails_Inv_Material]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MaterialIssueDetails] DROP CONSTRAINT [FK_MaterialIssueDetails_Inv_Material];
GO
IF OBJECT_ID(N'[dbo].[FK_MaterialIssueDetails_MaterialIssue]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MaterialIssueDetails] DROP CONSTRAINT [FK_MaterialIssueDetails_MaterialIssue];
GO
IF OBJECT_ID(N'[dbo].[FK_MaterialQuotationDetails_Inv_Material]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MaterialQuotationDetails] DROP CONSTRAINT [FK_MaterialQuotationDetails_Inv_Material];
GO
IF OBJECT_ID(N'[dbo].[FK_MaterialQuotationDetails_MaterialQuotationMaster]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MaterialQuotationDetails] DROP CONSTRAINT [FK_MaterialQuotationDetails_MaterialQuotationMaster];
GO
IF OBJECT_ID(N'[dbo].[FK_MaterialQuotationMaster_MaterialQuotationMaster]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MaterialQuotationMaster] DROP CONSTRAINT [FK_MaterialQuotationMaster_MaterialQuotationMaster];
GO
IF OBJECT_ID(N'[dbo].[FK_MatInwD_BoxMaster]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MatInwD] DROP CONSTRAINT [FK_MatInwD_BoxMaster];
GO
IF OBJECT_ID(N'[dbo].[FK_MatInwd_ideUser]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MatInwM] DROP CONSTRAINT [FK_MatInwd_ideUser];
GO
IF OBJECT_ID(N'[dbo].[FK_MatInwD_MatInwM]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MatInwD] DROP CONSTRAINT [FK_MatInwD_MatInwM];
GO
IF OBJECT_ID(N'[dbo].[FK_MatInwD_Tax]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MatInwD] DROP CONSTRAINT [FK_MatInwD_Tax];
GO
IF OBJECT_ID(N'[dbo].[FK_MatInwM_gen_Vendor]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MatInwM] DROP CONSTRAINT [FK_MatInwM_gen_Vendor];
GO
IF OBJECT_ID(N'[dbo].[FK_OD_ID_FluteType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OD_ID] DROP CONSTRAINT [FK_OD_ID_FluteType];
GO
IF OBJECT_ID(N'[dbo].[FK_OursourcingChild_Inv_Material]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OutsourcingChild] DROP CONSTRAINT [FK_OursourcingChild_Inv_Material];
GO
IF OBJECT_ID(N'[dbo].[FK_Outsource_gen_Vendor]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Outsourcing] DROP CONSTRAINT [FK_Outsource_gen_Vendor];
GO
IF OBJECT_ID(N'[dbo].[FK_Outsourcing_JW_JobCardMaster]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Outsourcing] DROP CONSTRAINT [FK_Outsourcing_JW_JobCardMaster];
GO
IF OBJECT_ID(N'[dbo].[FK_Outsourcing_wfStates]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Outsourcing] DROP CONSTRAINT [FK_Outsourcing_wfStates];
GO
IF OBJECT_ID(N'[dbo].[FK_OutsourcingChild_Outsourcing]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OutsourcingChild] DROP CONSTRAINT [FK_OutsourcingChild_Outsourcing];
GO
IF OBJECT_ID(N'[dbo].[FK_PaperCertificate_gen_Vendor]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PaperCertificate] DROP CONSTRAINT [FK_PaperCertificate_gen_Vendor];
GO
IF OBJECT_ID(N'[dbo].[FK_PaperCertificate_Inv_Material]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PaperCertificate] DROP CONSTRAINT [FK_PaperCertificate_Inv_Material];
GO
IF OBJECT_ID(N'[dbo].[FK_PaperCertificate_PurchaseOrderM]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PaperCertificate] DROP CONSTRAINT [FK_PaperCertificate_PurchaseOrderM];
GO
IF OBJECT_ID(N'[dbo].[FK_PaperCertificateDetails_PaperCertificate]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PaperCertificateDetails] DROP CONSTRAINT [FK_PaperCertificateDetails_PaperCertificate];
GO
IF OBJECT_ID(N'[dbo].[FK_PaperCertificateDetails_PaperChar]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PaperCertificateDetails] DROP CONSTRAINT [FK_PaperCertificateDetails_PaperChar];
GO
IF OBJECT_ID(N'[dbo].[FK_PaperStock_Inv_Material]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PaperStock] DROP CONSTRAINT [FK_PaperStock_Inv_Material];
GO
IF OBJECT_ID(N'[dbo].[FK_PartJobs_gen_Customer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PartJobs] DROP CONSTRAINT [FK_PartJobs_gen_Customer];
GO
IF OBJECT_ID(N'[dbo].[FK_PartJobs_wfStates]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PartJobs] DROP CONSTRAINT [FK_PartJobs_wfStates];
GO
IF OBJECT_ID(N'[dbo].[FK_PartJobsReturns_Inv_MaterialCategory]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PartJobsReturns] DROP CONSTRAINT [FK_PartJobsReturns_Inv_MaterialCategory];
GO
IF OBJECT_ID(N'[dbo].[FK_PartJobsReturns_PartJobs]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PartJobsReturns] DROP CONSTRAINT [FK_PartJobsReturns_PartJobs];
GO
IF OBJECT_ID(N'[dbo].[FK_PartJobsReturns_wfStates]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PartJobsReturns] DROP CONSTRAINT [FK_PartJobsReturns_wfStates];
GO
IF OBJECT_ID(N'[dbo].[FK_Payment_Bank]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Payment] DROP CONSTRAINT [FK_Payment_Bank];
GO
IF OBJECT_ID(N'[dbo].[FK_Payment_gen_Vendor]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Payment] DROP CONSTRAINT [FK_Payment_gen_Vendor];
GO
IF OBJECT_ID(N'[dbo].[FK_POReturnDetails_Inv_Material]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[POReturnDetails] DROP CONSTRAINT [FK_POReturnDetails_Inv_Material];
GO
IF OBJECT_ID(N'[dbo].[FK_POReturnDetails_POReturnMaster]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[POReturnDetails] DROP CONSTRAINT [FK_POReturnDetails_POReturnMaster];
GO
IF OBJECT_ID(N'[dbo].[FK_POReturnMaster_PurchaseOrderM]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[POReturnMaster] DROP CONSTRAINT [FK_POReturnMaster_PurchaseOrderM];
GO
IF OBJECT_ID(N'[dbo].[FK_PurchaseOrderD_Inv_Material]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PurchaseOrderD] DROP CONSTRAINT [FK_PurchaseOrderD_Inv_Material];
GO
IF OBJECT_ID(N'[dbo].[FK_PurchaseOrderD_PurchaseOrderM1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PurchaseOrderD] DROP CONSTRAINT [FK_PurchaseOrderD_PurchaseOrderM1];
GO
IF OBJECT_ID(N'[dbo].[FK_PurchaseOrderM_gen_Vendor]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PurchaseOrderM] DROP CONSTRAINT [FK_PurchaseOrderM_gen_Vendor];
GO
IF OBJECT_ID(N'[dbo].[FK_PurchaseOrderM_Inv_MaterialIndentMaster]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PurchaseOrderM] DROP CONSTRAINT [FK_PurchaseOrderM_Inv_MaterialIndentMaster];
GO
IF OBJECT_ID(N'[dbo].[FK_PurchaseOrderM_wfStates]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PurchaseOrderM] DROP CONSTRAINT [FK_PurchaseOrderM_wfStates];
GO
IF OBJECT_ID(N'[dbo].[FK_QuotationDetails_BoxMaster]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[QuotationDetails] DROP CONSTRAINT [FK_QuotationDetails_BoxMaster];
GO
IF OBJECT_ID(N'[dbo].[FK_QuotationDetails_est_BoxEstimation]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[QuotationDetails] DROP CONSTRAINT [FK_QuotationDetails_est_BoxEstimation];
GO
IF OBJECT_ID(N'[dbo].[FK_QuotationDetails_QuotationMaster]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[QuotationDetails] DROP CONSTRAINT [FK_QuotationDetails_QuotationMaster];
GO
IF OBJECT_ID(N'[dbo].[FK_QuotationMaster_eq_Enquiry]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[QuotationMaster] DROP CONSTRAINT [FK_QuotationMaster_eq_Enquiry];
GO
IF OBJECT_ID(N'[dbo].[FK_Receipt_gen_Customer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Receipt] DROP CONSTRAINT [FK_Receipt_gen_Customer];
GO
IF OBJECT_ID(N'[dbo].[FK_Sample_JobCardDetails_Inv_Material]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Sample_JobCardDetails] DROP CONSTRAINT [FK_Sample_JobCardDetails_Inv_Material];
GO
IF OBJECT_ID(N'[dbo].[FK_Sample_JobCardDetails_Sample_JobCardMaster]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Sample_JobCardDetails] DROP CONSTRAINT [FK_Sample_JobCardDetails_Sample_JobCardMaster];
GO
IF OBJECT_ID(N'[dbo].[FK_Sample_JobCardMaster_SampleBoxMaster]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Sample_JobCardMaster] DROP CONSTRAINT [FK_Sample_JobCardMaster_SampleBoxMaster];
GO
IF OBJECT_ID(N'[dbo].[FK_SampleBoxMaster_BoxType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SampleBoxMaster] DROP CONSTRAINT [FK_SampleBoxMaster_BoxType];
GO
IF OBJECT_ID(N'[dbo].[FK_SampleBoxMaster_FluteType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SampleBoxMaster] DROP CONSTRAINT [FK_SampleBoxMaster_FluteType];
GO
IF OBJECT_ID(N'[dbo].[FK_SampleBoxMaster_gen_Customer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SampleBoxMaster] DROP CONSTRAINT [FK_SampleBoxMaster_gen_Customer];
GO
IF OBJECT_ID(N'[dbo].[FK_SampleBoxSpecs_SampleBoxMaster]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SampleBoxSpecs] DROP CONSTRAINT [FK_SampleBoxSpecs_SampleBoxMaster];
GO
IF OBJECT_ID(N'[dbo].[FK_SampleItemPartProperty_SampleBoxSpecs]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SampleItemPartProperty] DROP CONSTRAINT [FK_SampleItemPartProperty_SampleBoxSpecs];
GO
IF OBJECT_ID(N'[dbo].[FK_SampleItems_Layers_Inv_Material]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SampleItems_Layers] DROP CONSTRAINT [FK_SampleItems_Layers_Inv_Material];
GO
IF OBJECT_ID(N'[dbo].[FK_SampleItems_Layers_SampleItemPartProperty]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SampleItems_Layers] DROP CONSTRAINT [FK_SampleItems_Layers_SampleItemPartProperty];
GO
IF OBJECT_ID(N'[dbo].[FK_SampMaterialIssue_Branch]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SampMaterialIssue] DROP CONSTRAINT [FK_SampMaterialIssue_Branch];
GO
IF OBJECT_ID(N'[dbo].[FK_SampMaterialIssue_ideUser]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SampMaterialIssue] DROP CONSTRAINT [FK_SampMaterialIssue_ideUser];
GO
IF OBJECT_ID(N'[dbo].[FK_SampMaterialIssue_Sample_JobCardMaster]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SampMaterialIssue] DROP CONSTRAINT [FK_SampMaterialIssue_Sample_JobCardMaster];
GO
IF OBJECT_ID(N'[dbo].[FK_SampMaterialIssue_wgTenant]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SampMaterialIssue] DROP CONSTRAINT [FK_SampMaterialIssue_wgTenant];
GO
IF OBJECT_ID(N'[dbo].[FK_SampMaterialIssueDetails_gen_Mill]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SampMaterialIssueDetails] DROP CONSTRAINT [FK_SampMaterialIssueDetails_gen_Mill];
GO
IF OBJECT_ID(N'[dbo].[FK_SampMaterialIssueDetails_Inv_Material]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SampMaterialIssueDetails] DROP CONSTRAINT [FK_SampMaterialIssueDetails_Inv_Material];
GO
IF OBJECT_ID(N'[dbo].[FK_SampMaterialIssueDetails_SampMaterialIssue]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SampMaterialIssueDetails] DROP CONSTRAINT [FK_SampMaterialIssueDetails_SampMaterialIssue];
GO
IF OBJECT_ID(N'[dbo].[FK_Semi_FinishedGoodsStock_Inv_MaterialCategory]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Semi_FinishedGoodsStock] DROP CONSTRAINT [FK_Semi_FinishedGoodsStock_Inv_MaterialCategory];
GO
IF OBJECT_ID(N'[dbo].[FK_Stocks_Inv_Material]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Stocks] DROP CONSTRAINT [FK_Stocks_Inv_Material];
GO
IF OBJECT_ID(N'[dbo].[FK_StockTransfer_Branch]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[StockTransfer] DROP CONSTRAINT [FK_StockTransfer_Branch];
GO
IF OBJECT_ID(N'[dbo].[FK_StockTransfer_Branch1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[StockTransfer] DROP CONSTRAINT [FK_StockTransfer_Branch1];
GO
IF OBJECT_ID(N'[dbo].[FK_StockTransfer_Inv_Material]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[StockTransfer] DROP CONSTRAINT [FK_StockTransfer_Inv_Material];
GO
IF OBJECT_ID(N'[dbo].[FK_Trans_Invoice_TransporterM]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Trans_Invoice] DROP CONSTRAINT [FK_Trans_Invoice_TransporterM];
GO
IF OBJECT_ID(N'[dbo].[FK_Trans_Payment_TransporterM]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Trans_Payment] DROP CONSTRAINT [FK_Trans_Payment_TransporterM];
GO
IF OBJECT_ID(N'[dbo].[FK_Voucher_acc_AccountHeads]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Voucher] DROP CONSTRAINT [FK_Voucher_acc_AccountHeads];
GO
IF OBJECT_ID(N'[dbo].[FK_wfWorkflowPaths_wfStates]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[wfWorkflowPaths] DROP CONSTRAINT [FK_wfWorkflowPaths_wfStates];
GO
IF OBJECT_ID(N'[dbo].[FK_wfWorkflowPaths_wfWorkflowRoles1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[wfWorkflowPaths] DROP CONSTRAINT [FK_wfWorkflowPaths_wfWorkflowRoles1];
GO
IF OBJECT_ID(N'[dbo].[FK_WireCertificate_gen_Vendor]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[WireCertificate] DROP CONSTRAINT [FK_WireCertificate_gen_Vendor];
GO
IF OBJECT_ID(N'[dbo].[FK_WireCertificate_Inv_Material]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[WireCertificate] DROP CONSTRAINT [FK_WireCertificate_Inv_Material];
GO
IF OBJECT_ID(N'[dbo].[FK_WireCertificateDetails_WireCertificate]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[WireCertificateDetails] DROP CONSTRAINT [FK_WireCertificateDetails_WireCertificate];
GO
IF OBJECT_ID(N'[dbo].[FK_WireCertificateDetails_WireChar]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[WireCertificateDetails] DROP CONSTRAINT [FK_WireCertificateDetails_WireChar];
GO
IF OBJECT_ID(N'[dbo].[FK_WO_Documents_WorkOrder]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[WO_Documents] DROP CONSTRAINT [FK_WO_Documents_WorkOrder];
GO
IF OBJECT_ID(N'[dbo].[FK_WorkOrder_BoxMaster]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[WorkOrder] DROP CONSTRAINT [FK_WorkOrder_BoxMaster];
GO
IF OBJECT_ID(N'[dbo].[FK_WorkOrder_gen_DeliverySchedule]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[WorkOrder] DROP CONSTRAINT [FK_WorkOrder_gen_DeliverySchedule];
GO
IF OBJECT_ID(N'[dbo].[FK_WorkOrder_gen_Order1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[WorkOrder] DROP CONSTRAINT [FK_WorkOrder_gen_Order1];
GO
IF OBJECT_ID(N'[dbo].[FK_WorkOrder_wfStates]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[WorkOrder] DROP CONSTRAINT [FK_WorkOrder_wfStates];
GO
IF OBJECT_ID(N'[dbo].[FK_WorkOrderDetails_Inv_Material]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[WorkOrderDetails] DROP CONSTRAINT [FK_WorkOrderDetails_Inv_Material];
GO
IF OBJECT_ID(N'[dbo].[FK_WorkOrderDetails_PaperStock]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[WorkOrderDetails] DROP CONSTRAINT [FK_WorkOrderDetails_PaperStock];
GO
IF OBJECT_ID(N'[dbo].[FK_WorkOrderDetails_WorkOrder]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[WorkOrderDetails] DROP CONSTRAINT [FK_WorkOrderDetails_WorkOrder];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[acc_AccountHeads]', 'U') IS NOT NULL
    DROP TABLE [dbo].[acc_AccountHeads];
GO
IF OBJECT_ID(N'[dbo].[AccountType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AccountType];
GO
IF OBJECT_ID(N'[dbo].[att_RegisterAttendance]', 'U') IS NOT NULL
    DROP TABLE [dbo].[att_RegisterAttendance];
GO
IF OBJECT_ID(N'[dbo].[att_RegisterMain]', 'U') IS NOT NULL
    DROP TABLE [dbo].[att_RegisterMain];
GO
IF OBJECT_ID(N'[dbo].[Bank]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Bank];
GO
IF OBJECT_ID(N'[dbo].[BoardDesc]', 'U') IS NOT NULL
    DROP TABLE [dbo].[BoardDesc];
GO
IF OBJECT_ID(N'[dbo].[Box_Documents]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Box_Documents];
GO
IF OBJECT_ID(N'[dbo].[BoxChild]', 'U') IS NOT NULL
    DROP TABLE [dbo].[BoxChild];
GO
IF OBJECT_ID(N'[dbo].[BoxMaster]', 'U') IS NOT NULL
    DROP TABLE [dbo].[BoxMaster];
GO
IF OBJECT_ID(N'[dbo].[BoxSpecs]', 'U') IS NOT NULL
    DROP TABLE [dbo].[BoxSpecs];
GO
IF OBJECT_ID(N'[dbo].[BoxStock]', 'U') IS NOT NULL
    DROP TABLE [dbo].[BoxStock];
GO
IF OBJECT_ID(N'[dbo].[BoxType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[BoxType];
GO
IF OBJECT_ID(N'[dbo].[Branch]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Branch];
GO
IF OBJECT_ID(N'[dbo].[COA]', 'U') IS NOT NULL
    DROP TABLE [dbo].[COA];
GO
IF OBJECT_ID(N'[dbo].[COA_Parameters]', 'U') IS NOT NULL
    DROP TABLE [dbo].[COA_Parameters];
GO
IF OBJECT_ID(N'[dbo].[COADetails]', 'U') IS NOT NULL
    DROP TABLE [dbo].[COADetails];
GO
IF OBJECT_ID(N'[dbo].[ConsumableIssueDet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ConsumableIssueDet];
GO
IF OBJECT_ID(N'[dbo].[ConsumablesIssue]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ConsumablesIssue];
GO
IF OBJECT_ID(N'[dbo].[CustomerComplaints]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CustomerComplaints];
GO
IF OBJECT_ID(N'[dbo].[CustomerRejections]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CustomerRejections];
GO
IF OBJECT_ID(N'[dbo].[DispatchChild]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DispatchChild];
GO
IF OBJECT_ID(N'[dbo].[DispatchMast]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DispatchMast];
GO
IF OBJECT_ID(N'[dbo].[emp_Department]', 'U') IS NOT NULL
    DROP TABLE [dbo].[emp_Department];
GO
IF OBJECT_ID(N'[dbo].[emp_EmployeeDesignation]', 'U') IS NOT NULL
    DROP TABLE [dbo].[emp_EmployeeDesignation];
GO
IF OBJECT_ID(N'[dbo].[emp_EmployeeImage]', 'U') IS NOT NULL
    DROP TABLE [dbo].[emp_EmployeeImage];
GO
IF OBJECT_ID(N'[dbo].[emp_EmployeeMaster]', 'U') IS NOT NULL
    DROP TABLE [dbo].[emp_EmployeeMaster];
GO
IF OBJECT_ID(N'[dbo].[eq_Documents]', 'U') IS NOT NULL
    DROP TABLE [dbo].[eq_Documents];
GO
IF OBJECT_ID(N'[dbo].[eq_Enquiry]', 'U') IS NOT NULL
    DROP TABLE [dbo].[eq_Enquiry];
GO
IF OBJECT_ID(N'[dbo].[eq_EnquiryChild]', 'U') IS NOT NULL
    DROP TABLE [dbo].[eq_EnquiryChild];
GO
IF OBJECT_ID(N'[dbo].[est_BoxEstimation]', 'U') IS NOT NULL
    DROP TABLE [dbo].[est_BoxEstimation];
GO
IF OBJECT_ID(N'[dbo].[est_BoxEstimationChild]', 'U') IS NOT NULL
    DROP TABLE [dbo].[est_BoxEstimationChild];
GO
IF OBJECT_ID(N'[dbo].[est_EstimateAdditionalCost]', 'U') IS NOT NULL
    DROP TABLE [dbo].[est_EstimateAdditionalCost];
GO
IF OBJECT_ID(N'[dbo].[est_EstimateTax]', 'U') IS NOT NULL
    DROP TABLE [dbo].[est_EstimateTax];
GO
IF OBJECT_ID(N'[dbo].[est_Estimation]', 'U') IS NOT NULL
    DROP TABLE [dbo].[est_Estimation];
GO
IF OBJECT_ID(N'[dbo].[est_EstimationPart]', 'U') IS NOT NULL
    DROP TABLE [dbo].[est_EstimationPart];
GO
IF OBJECT_ID(N'[dbo].[est_Layer]', 'U') IS NOT NULL
    DROP TABLE [dbo].[est_Layer];
GO
IF OBJECT_ID(N'[dbo].[FinishedGoodsStock]', 'U') IS NOT NULL
    DROP TABLE [dbo].[FinishedGoodsStock];
GO
IF OBJECT_ID(N'[dbo].[FluteType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[FluteType];
GO
IF OBJECT_ID(N'[dbo].[gen_City]', 'U') IS NOT NULL
    DROP TABLE [dbo].[gen_City];
GO
IF OBJECT_ID(N'[dbo].[gen_Color]', 'U') IS NOT NULL
    DROP TABLE [dbo].[gen_Color];
GO
IF OBJECT_ID(N'[dbo].[gen_Communication]', 'U') IS NOT NULL
    DROP TABLE [dbo].[gen_Communication];
GO
IF OBJECT_ID(N'[dbo].[gen_ContactPerson]', 'U') IS NOT NULL
    DROP TABLE [dbo].[gen_ContactPerson];
GO
IF OBJECT_ID(N'[dbo].[gen_Country]', 'U') IS NOT NULL
    DROP TABLE [dbo].[gen_Country];
GO
IF OBJECT_ID(N'[dbo].[gen_Customer]', 'U') IS NOT NULL
    DROP TABLE [dbo].[gen_Customer];
GO
IF OBJECT_ID(N'[dbo].[gen_CustomerContacts]', 'U') IS NOT NULL
    DROP TABLE [dbo].[gen_CustomerContacts];
GO
IF OBJECT_ID(N'[dbo].[gen_CustomerShippingDetails]', 'U') IS NOT NULL
    DROP TABLE [dbo].[gen_CustomerShippingDetails];
GO
IF OBJECT_ID(N'[dbo].[gen_DeliverySchedule]', 'U') IS NOT NULL
    DROP TABLE [dbo].[gen_DeliverySchedule];
GO
IF OBJECT_ID(N'[dbo].[gen_Events]', 'U') IS NOT NULL
    DROP TABLE [dbo].[gen_Events];
GO
IF OBJECT_ID(N'[dbo].[gen_Machine]', 'U') IS NOT NULL
    DROP TABLE [dbo].[gen_Machine];
GO
IF OBJECT_ID(N'[dbo].[gen_Mill]', 'U') IS NOT NULL
    DROP TABLE [dbo].[gen_Mill];
GO
IF OBJECT_ID(N'[dbo].[gen_MillContacts]', 'U') IS NOT NULL
    DROP TABLE [dbo].[gen_MillContacts];
GO
IF OBJECT_ID(N'[dbo].[gen_Order]', 'U') IS NOT NULL
    DROP TABLE [dbo].[gen_Order];
GO
IF OBJECT_ID(N'[dbo].[Gen_OrderChild]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Gen_OrderChild];
GO
IF OBJECT_ID(N'[dbo].[gen_PaperType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[gen_PaperType];
GO
IF OBJECT_ID(N'[dbo].[gen_State]', 'U') IS NOT NULL
    DROP TABLE [dbo].[gen_State];
GO
IF OBJECT_ID(N'[dbo].[gen_Unit]', 'U') IS NOT NULL
    DROP TABLE [dbo].[gen_Unit];
GO
IF OBJECT_ID(N'[dbo].[gen_Vendor]', 'U') IS NOT NULL
    DROP TABLE [dbo].[gen_Vendor];
GO
IF OBJECT_ID(N'[dbo].[gen_VendorContacts]', 'U') IS NOT NULL
    DROP TABLE [dbo].[gen_VendorContacts];
GO
IF OBJECT_ID(N'[dbo].[gen_VendorMaterials]', 'U') IS NOT NULL
    DROP TABLE [dbo].[gen_VendorMaterials];
GO
IF OBJECT_ID(N'[dbo].[GenReports]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GenReports];
GO
IF OBJECT_ID(N'[dbo].[GlueCertificate]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GlueCertificate];
GO
IF OBJECT_ID(N'[dbo].[GlueCertificateDetails]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GlueCertificateDetails];
GO
IF OBJECT_ID(N'[dbo].[GRN_Details]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GRN_Details];
GO
IF OBJECT_ID(N'[dbo].[GRN_Mast]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GRN_Mast];
GO
IF OBJECT_ID(N'[dbo].[ideActivity]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ideActivity];
GO
IF OBJECT_ID(N'[dbo].[ideActivityPermission]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ideActivityPermission];
GO
IF OBJECT_ID(N'[dbo].[idePermission]', 'U') IS NOT NULL
    DROP TABLE [dbo].[idePermission];
GO
IF OBJECT_ID(N'[dbo].[ideRole]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ideRole];
GO
IF OBJECT_ID(N'[dbo].[ideRoleUser]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ideRoleUser];
GO
IF OBJECT_ID(N'[dbo].[ideUser]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ideUser];
GO
IF OBJECT_ID(N'[dbo].[InkCertificate]', 'U') IS NOT NULL
    DROP TABLE [dbo].[InkCertificate];
GO
IF OBJECT_ID(N'[dbo].[InkCertificateDetails]', 'U') IS NOT NULL
    DROP TABLE [dbo].[InkCertificateDetails];
GO
IF OBJECT_ID(N'[dbo].[InkChar]', 'U') IS NOT NULL
    DROP TABLE [dbo].[InkChar];
GO
IF OBJECT_ID(N'[dbo].[Inv_Billing]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Inv_Billing];
GO
IF OBJECT_ID(N'[dbo].[Inv_BillingDetails]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Inv_BillingDetails];
GO
IF OBJECT_ID(N'[dbo].[Inv_Material]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Inv_Material];
GO
IF OBJECT_ID(N'[dbo].[Inv_MaterialCategory]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Inv_MaterialCategory];
GO
IF OBJECT_ID(N'[dbo].[Inv_MaterialIndentDetails]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Inv_MaterialIndentDetails];
GO
IF OBJECT_ID(N'[dbo].[Inv_MaterialIndentMaster]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Inv_MaterialIndentMaster];
GO
IF OBJECT_ID(N'[dbo].[Inv_PaymentDetails]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Inv_PaymentDetails];
GO
IF OBJECT_ID(N'[dbo].[IssueReturn]', 'U') IS NOT NULL
    DROP TABLE [dbo].[IssueReturn];
GO
IF OBJECT_ID(N'[dbo].[IssueReturnDetails]', 'U') IS NOT NULL
    DROP TABLE [dbo].[IssueReturnDetails];
GO
IF OBJECT_ID(N'[dbo].[ItemPartProperty]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ItemPartProperty];
GO
IF OBJECT_ID(N'[dbo].[Items_Layers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Items_Layers];
GO
IF OBJECT_ID(N'[dbo].[JC_Documents]', 'U') IS NOT NULL
    DROP TABLE [dbo].[JC_Documents];
GO
IF OBJECT_ID(N'[dbo].[JobCardDetails]', 'U') IS NOT NULL
    DROP TABLE [dbo].[JobCardDetails];
GO
IF OBJECT_ID(N'[dbo].[JobCardMaster]', 'U') IS NOT NULL
    DROP TABLE [dbo].[JobCardMaster];
GO
IF OBJECT_ID(N'[dbo].[JobCardPartsDetails]', 'U') IS NOT NULL
    DROP TABLE [dbo].[JobCardPartsDetails];
GO
IF OBJECT_ID(N'[dbo].[JobCardPartsMaster]', 'U') IS NOT NULL
    DROP TABLE [dbo].[JobCardPartsMaster];
GO
IF OBJECT_ID(N'[dbo].[JobProcess]', 'U') IS NOT NULL
    DROP TABLE [dbo].[JobProcess];
GO
IF OBJECT_ID(N'[dbo].[JobWorkChild]', 'U') IS NOT NULL
    DROP TABLE [dbo].[JobWorkChild];
GO
IF OBJECT_ID(N'[dbo].[JobWorkReceivables]', 'U') IS NOT NULL
    DROP TABLE [dbo].[JobWorkReceivables];
GO
IF OBJECT_ID(N'[dbo].[JobWorkRMStock]', 'U') IS NOT NULL
    DROP TABLE [dbo].[JobWorkRMStock];
GO
IF OBJECT_ID(N'[dbo].[JProcess]', 'U') IS NOT NULL
    DROP TABLE [dbo].[JProcess];
GO
IF OBJECT_ID(N'[dbo].[JW_JobCardMaster]', 'U') IS NOT NULL
    DROP TABLE [dbo].[JW_JobCardMaster];
GO
IF OBJECT_ID(N'[dbo].[JW_RM_RetDet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[JW_RM_RetDet];
GO
IF OBJECT_ID(N'[dbo].[JW_RM_RetMast]', 'U') IS NOT NULL
    DROP TABLE [dbo].[JW_RM_RetMast];
GO
IF OBJECT_ID(N'[dbo].[JWMat_InwdD]', 'U') IS NOT NULL
    DROP TABLE [dbo].[JWMat_InwdD];
GO
IF OBJECT_ID(N'[dbo].[JWMat_InwdM]', 'U') IS NOT NULL
    DROP TABLE [dbo].[JWMat_InwdM];
GO
IF OBJECT_ID(N'[dbo].[JWStocks]', 'U') IS NOT NULL
    DROP TABLE [dbo].[JWStocks];
GO
IF OBJECT_ID(N'[dbo].[MaterialInwardD]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MaterialInwardD];
GO
IF OBJECT_ID(N'[dbo].[MaterialInwardM]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MaterialInwardM];
GO
IF OBJECT_ID(N'[dbo].[MaterialIssue]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MaterialIssue];
GO
IF OBJECT_ID(N'[dbo].[MaterialIssueDetails]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MaterialIssueDetails];
GO
IF OBJECT_ID(N'[dbo].[MaterialQuotationDetails]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MaterialQuotationDetails];
GO
IF OBJECT_ID(N'[dbo].[MaterialQuotationMaster]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MaterialQuotationMaster];
GO
IF OBJECT_ID(N'[dbo].[MatInwD]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MatInwD];
GO
IF OBJECT_ID(N'[dbo].[MatInwM]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MatInwM];
GO
IF OBJECT_ID(N'[dbo].[OD_ID]', 'U') IS NOT NULL
    DROP TABLE [dbo].[OD_ID];
GO
IF OBJECT_ID(N'[dbo].[Outsourcing]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Outsourcing];
GO
IF OBJECT_ID(N'[dbo].[OutsourcingChild]', 'U') IS NOT NULL
    DROP TABLE [dbo].[OutsourcingChild];
GO
IF OBJECT_ID(N'[dbo].[PaperCertificate]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PaperCertificate];
GO
IF OBJECT_ID(N'[dbo].[PaperCertificateDetails]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PaperCertificateDetails];
GO
IF OBJECT_ID(N'[dbo].[PaperChar]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PaperChar];
GO
IF OBJECT_ID(N'[dbo].[PaperStock]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PaperStock];
GO
IF OBJECT_ID(N'[dbo].[PartJobs]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PartJobs];
GO
IF OBJECT_ID(N'[dbo].[PartJobsReturns]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PartJobsReturns];
GO
IF OBJECT_ID(N'[dbo].[Payment]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Payment];
GO
IF OBJECT_ID(N'[dbo].[POReturnDetails]', 'U') IS NOT NULL
    DROP TABLE [dbo].[POReturnDetails];
GO
IF OBJECT_ID(N'[dbo].[POReturnMaster]', 'U') IS NOT NULL
    DROP TABLE [dbo].[POReturnMaster];
GO
IF OBJECT_ID(N'[dbo].[ProcessMaster]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ProcessMaster];
GO
IF OBJECT_ID(N'[dbo].[PurchaseOrderD]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PurchaseOrderD];
GO
IF OBJECT_ID(N'[dbo].[PurchaseOrderM]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PurchaseOrderM];
GO
IF OBJECT_ID(N'[dbo].[QuotationDetails]', 'U') IS NOT NULL
    DROP TABLE [dbo].[QuotationDetails];
GO
IF OBJECT_ID(N'[dbo].[QuotationMaster]', 'U') IS NOT NULL
    DROP TABLE [dbo].[QuotationMaster];
GO
IF OBJECT_ID(N'[dbo].[Receipt]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Receipt];
GO
IF OBJECT_ID(N'[dbo].[SampJobProcess]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SampJobProcess];
GO
IF OBJECT_ID(N'[dbo].[SampJProcess]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SampJProcess];
GO
IF OBJECT_ID(N'[dbo].[Sample_JobCardDetails]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Sample_JobCardDetails];
GO
IF OBJECT_ID(N'[dbo].[Sample_JobCardMaster]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Sample_JobCardMaster];
GO
IF OBJECT_ID(N'[dbo].[SampleBoxMaster]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SampleBoxMaster];
GO
IF OBJECT_ID(N'[dbo].[SampleBoxSpecs]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SampleBoxSpecs];
GO
IF OBJECT_ID(N'[dbo].[SampleItemPartProperty]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SampleItemPartProperty];
GO
IF OBJECT_ID(N'[dbo].[SampleItems_Layers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SampleItems_Layers];
GO
IF OBJECT_ID(N'[dbo].[SampMaterialIssue]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SampMaterialIssue];
GO
IF OBJECT_ID(N'[dbo].[SampMaterialIssueDetails]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SampMaterialIssueDetails];
GO
IF OBJECT_ID(N'[dbo].[Semi_FinishedGoodsStock]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Semi_FinishedGoodsStock];
GO
IF OBJECT_ID(N'[dbo].[Stocks]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Stocks];
GO
IF OBJECT_ID(N'[dbo].[StockTransfer]', 'U') IS NOT NULL
    DROP TABLE [dbo].[StockTransfer];
GO
IF OBJECT_ID(N'[dbo].[sysdiagrams]', 'U') IS NOT NULL
    DROP TABLE [dbo].[sysdiagrams];
GO
IF OBJECT_ID(N'[dbo].[TabPPCRep]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TabPPCRep];
GO
IF OBJECT_ID(N'[dbo].[Tax]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Tax];
GO
IF OBJECT_ID(N'[dbo].[tempPPC]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tempPPC];
GO
IF OBJECT_ID(N'[dbo].[TonnValues]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TonnValues];
GO
IF OBJECT_ID(N'[dbo].[Trans_Invoice]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Trans_Invoice];
GO
IF OBJECT_ID(N'[dbo].[Trans_Payment]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Trans_Payment];
GO
IF OBJECT_ID(N'[dbo].[TransporterM]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TransporterM];
GO
IF OBJECT_ID(N'[dbo].[Voucher]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Voucher];
GO
IF OBJECT_ID(N'[dbo].[wfStates]', 'U') IS NOT NULL
    DROP TABLE [dbo].[wfStates];
GO
IF OBJECT_ID(N'[dbo].[wfTransactions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[wfTransactions];
GO
IF OBJECT_ID(N'[dbo].[wfWorkflowPaths]', 'U') IS NOT NULL
    DROP TABLE [dbo].[wfWorkflowPaths];
GO
IF OBJECT_ID(N'[dbo].[wfWorkflowRoles]', 'U') IS NOT NULL
    DROP TABLE [dbo].[wfWorkflowRoles];
GO
IF OBJECT_ID(N'[dbo].[wgTenant]', 'U') IS NOT NULL
    DROP TABLE [dbo].[wgTenant];
GO
IF OBJECT_ID(N'[dbo].[WireCertificate]', 'U') IS NOT NULL
    DROP TABLE [dbo].[WireCertificate];
GO
IF OBJECT_ID(N'[dbo].[WireCertificateDetails]', 'U') IS NOT NULL
    DROP TABLE [dbo].[WireCertificateDetails];
GO
IF OBJECT_ID(N'[dbo].[WireChar]', 'U') IS NOT NULL
    DROP TABLE [dbo].[WireChar];
GO
IF OBJECT_ID(N'[dbo].[WO_Documents]', 'U') IS NOT NULL
    DROP TABLE [dbo].[WO_Documents];
GO
IF OBJECT_ID(N'[dbo].[WorkOrder]', 'U') IS NOT NULL
    DROP TABLE [dbo].[WorkOrder];
GO
IF OBJECT_ID(N'[dbo].[WorkOrderDetails]', 'U') IS NOT NULL
    DROP TABLE [dbo].[WorkOrderDetails];
GO
IF OBJECT_ID(N'[CogitoModelStoreContainer].[BarCodeT]', 'U') IS NOT NULL
    DROP TABLE [CogitoModelStoreContainer].[BarCodeT];
GO
IF OBJECT_ID(N'[CogitoModelStoreContainer].[ideShortCut]', 'U') IS NOT NULL
    DROP TABLE [CogitoModelStoreContainer].[ideShortCut];
GO
IF OBJECT_ID(N'[CogitoModelStoreContainer].[wfTriggerParameters]', 'U') IS NOT NULL
    DROP TABLE [CogitoModelStoreContainer].[wfTriggerParameters];
GO
IF OBJECT_ID(N'[CogitoModelStoreContainer].[wfTriggers]', 'U') IS NOT NULL
    DROP TABLE [CogitoModelStoreContainer].[wfTriggers];
GO
IF OBJECT_ID(N'[CogitoModelStoreContainer].[Vw_SalesOrderTracker]', 'U') IS NOT NULL
    DROP TABLE [CogitoModelStoreContainer].[Vw_SalesOrderTracker];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'est_Estimation'
CREATE TABLE [dbo].[est_Estimation] (
    [Pk_Estimate] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Enquiry] decimal(18,0)  NULL,
    [Name] nvarchar(500)  NULL,
    [PlateYes] bit  NULL,
    [WidthPartationYes] bit  NULL,
    [LenghtPartationYes] bit  NULL,
    [OuterShellYes] bit  NULL,
    [CapYes] bit  NULL,
    [OuterShell] int  NULL,
    [Plate] int  NULL,
    [WidthPartation] int  NULL,
    [LenghtPartation] int  NULL,
    [Cap] int  NULL,
    [ConvRate] decimal(18,0)  NULL,
    [ConvValue] decimal(18,0)  NULL,
    [GMarginPercentage] int  NULL,
    [GMarginValue] decimal(18,0)  NULL,
    [TaxesPercntage] int  NULL,
    [TaxesValue] decimal(18,0)  NULL,
    [TransportValue] decimal(18,0)  NULL,
    [WeightHValue] decimal(18,0)  NULL,
    [HandlingChanrgesValue] decimal(18,0)  NULL,
    [PackingChargesValue] decimal(18,0)  NULL,
    [RejectionPercentage] int  NULL,
    [RejectionValue] decimal(18,0)  NULL,
    [TotalWeight] decimal(18,0)  NULL,
    [TotalPrice] decimal(18,0)  NULL,
    [Status] bit  NULL
);
GO

-- Creating table 'est_EstimationPart'
CREATE TABLE [dbo].[est_EstimationPart] (
    [Pk_DetailedEstimationPart] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_DetailedEstimation] decimal(18,0)  NULL,
    [Length] decimal(18,0)  NULL,
    [Width] decimal(18,0)  NULL,
    [Height] decimal(18,0)  NULL,
    [BoardArea] decimal(18,0)  NULL,
    [Deckle] decimal(18,0)  NULL,
    [CuttingSize] decimal(18,0)  NULL,
    [Quantity] decimal(18,0)  NULL,
    [NoBoards] decimal(18,0)  NULL,
    [Weight] decimal(18,0)  NULL,
    [Rate] decimal(18,0)  NULL
);
GO

-- Creating table 'est_Layer'
CREATE TABLE [dbo].[est_Layer] (
    [Pk_Layer] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Estimation] decimal(18,0)  NULL,
    [Gsm] decimal(18,0)  NULL,
    [Bf] decimal(18,0)  NULL,
    [Color] nvarchar(150)  NULL,
    [Rate] decimal(18,0)  NULL
);
GO

-- Creating table 'C_est_Estimation'
CREATE TABLE [dbo].[C_est_Estimation] (
    [PK_DetailedEstimate] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Enquiry] decimal(18,0)  NULL,
    [OuterShellRequired] bit  NULL,
    [CapRequired] bit  NULL,
    [LengthPartitionRequired] bit  NULL,
    [WidthPartitionRequired] bit  NULL,
    [PlateRequired] bit  NULL,
    [NumberOfOuterShells] int  NULL,
    [NumberOfCaps] int  NULL,
    [NumberOfLengthPartition] int  NULL,
    [NumberOfWidthPartition] int  NULL,
    [NumberOfPlates] int  NULL,
    [TotalWeight] decimal(18,3)  NULL,
    [TotalCost] decimal(18,2)  NULL
);
GO

-- Creating table 'C_est_EstimationPart'
CREATE TABLE [dbo].[C_est_EstimationPart] (
    [Pk_DetailedEstimationPart] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [FK_DetailedEstimate] decimal(18,0)  NULL,
    [Length] decimal(18,0)  NULL,
    [Width] decimal(18,0)  NULL,
    [Height] decimal(18,0)  NULL,
    [BoardArea] decimal(18,0)  NULL,
    [Deckle] decimal(18,0)  NULL,
    [CuttingSize] decimal(18,0)  NULL,
    [Quantity] decimal(18,0)  NULL,
    [NoBoards] decimal(18,0)  NULL,
    [Weight] decimal(18,0)  NULL,
    [Rate] decimal(18,0)  NULL
);
GO

-- Creating table 'C_est_Layer'
CREATE TABLE [dbo].[C_est_Layer] (
    [Pk_Layer] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_DetailedEstimationPart] decimal(18,0)  NULL,
    [Gsm] decimal(18,0)  NULL,
    [Bf] decimal(18,0)  NULL,
    [Color] nvarchar(150)  NULL,
    [Rate] decimal(18,0)  NULL
);
GO

-- Creating table 'acc_AccountHeads'
CREATE TABLE [dbo].[acc_AccountHeads] (
    [Pk_AccountHeadId] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [HeadName] nvarchar(50)  NOT NULL,
    [Description] nvarchar(500)  NULL
);
GO

-- Creating table 'AccountType'
CREATE TABLE [dbo].[AccountType] (
    [Pk_AccountTypeID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [AccTypeName] nvarchar(50)  NOT NULL,
    [Description] nvarchar(max)  NULL
);
GO

-- Creating table 'att_RegisterAttendance'
CREATE TABLE [dbo].[att_RegisterAttendance] (
    [Pk_RegisterAttendance] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_RegisterMain] decimal(18,0)  NOT NULL,
    [EmployeeName] nvarchar(50)  NULL,
    [Fk_EmployeeId] decimal(18,0)  NOT NULL,
    [Day_1] bit  NULL,
    [Day_2] bit  NULL,
    [Day_3] bit  NULL,
    [Day_4] bit  NULL,
    [Day_5] bit  NULL,
    [Day_6] bit  NULL,
    [Day_7] bit  NULL,
    [Day_8] bit  NULL,
    [Day_9] bit  NULL,
    [Day_10] bit  NULL,
    [Day_11] bit  NULL,
    [Day_12] bit  NULL,
    [Day_13] bit  NULL,
    [Day_14] bit  NULL,
    [Day_15] bit  NULL,
    [Day_16] bit  NULL,
    [Day_17] bit  NULL,
    [Day_18] bit  NULL,
    [Day_19] bit  NULL,
    [Day_20] bit  NULL,
    [Day_21] bit  NULL,
    [Day_22] bit  NULL,
    [Day_23] bit  NULL,
    [Day_24] bit  NULL,
    [Day_25] bit  NULL,
    [Day_26] bit  NULL,
    [Day_27] bit  NULL,
    [Day_28] bit  NULL,
    [Day_29] bit  NULL,
    [Day_30] bit  NULL,
    [Day_31] bit  NULL
);
GO

-- Creating table 'att_RegisterMain'
CREATE TABLE [dbo].[att_RegisterMain] (
    [Pk_RegisterMain] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [CMonth] int  NOT NULL,
    [CYear] int  NOT NULL
);
GO

-- Creating table 'Bank'
CREATE TABLE [dbo].[Bank] (
    [Pk_BankID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_AccountTypeID] decimal(18,0)  NOT NULL,
    [AccountName] nvarchar(50)  NOT NULL,
    [AccountNumber] nvarchar(50)  NOT NULL,
    [BranchName] nvarchar(50)  NOT NULL,
    [BankName] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'BoxType'
CREATE TABLE [dbo].[BoxType] (
    [Pk_BoxTypeID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'Branch'
CREATE TABLE [dbo].[Branch] (
    [Pk_BranchID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [BranchName] varchar(50)  NULL,
    [Fk_Tanent] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'CustomerComplaints'
CREATE TABLE [dbo].[CustomerComplaints] (
    [Pk_ComplaintID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [ComplaintDate] datetime  NOT NULL,
    [Fk_CustomerID] decimal(18,0)  NULL,
    [Fk_OrderNo] decimal(18,0)  NULL,
    [Fk_EnquiryNo] decimal(18,0)  NULL,
    [Description] nvarchar(500)  NULL,
    [Fk_Status] decimal(18,0)  NULL,
    [Fk_User] decimal(18,0)  NULL,
    [Fk_Tanent] decimal(18,0)  NULL
);
GO

-- Creating table 'CustomerRejections'
CREATE TABLE [dbo].[CustomerRejections] (
    [Pk_RejectionId] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [RejectionDate] datetime  NULL,
    [Fk_OrderNo] decimal(18,0)  NOT NULL,
    [Fk_Customer] decimal(18,0)  NOT NULL,
    [Rejected_Quantity] decimal(18,0)  NOT NULL,
    [Reason] nvarchar(500)  NULL
);
GO

-- Creating table 'emp_Department'
CREATE TABLE [dbo].[emp_Department] (
    [Pk_DepartmentId] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [Description] nvarchar(500)  NULL
);
GO

-- Creating table 'emp_EmployeeDesignation'
CREATE TABLE [dbo].[emp_EmployeeDesignation] (
    [Pk_EmployeeDesignationId] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [DesignationName] nvarchar(50)  NOT NULL,
    [Description] nvarchar(500)  NULL
);
GO

-- Creating table 'emp_EmployeeImage'
CREATE TABLE [dbo].[emp_EmployeeImage] (
    [Pk_EmployeeImageId] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Employee] decimal(18,0)  NULL,
    [ImageLink] nvarchar(50)  NULL,
    [ImageName] nvarchar(50)  NULL,
    [ImageSize] nvarchar(50)  NULL
);
GO

-- Creating table 'est_EstimateAdditionalCost'
CREATE TABLE [dbo].[est_EstimateAdditionalCost] (
    [PK_EstimationAddtionalCosts] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_DetailedEstimation] decimal(18,0)  NULL,
    [ConversionRatePercentage] decimal(18,0)  NULL,
    [ConversionValue] decimal(18,0)  NULL,
    [WestagePercentage] decimal(18,0)  NULL,
    [WestageValue] decimal(18,0)  NULL,
    [TransportCosts] decimal(18,0)  NULL,
    [HandlingCosts] decimal(18,0)  NULL,
    [RejectionPercentage] decimal(18,0)  NULL,
    [RejectionValue] decimal(18,0)  NULL,
    [MarginPercentage] decimal(18,0)  NULL,
    [MarginValue] decimal(18,0)  NULL,
    [PrintingCosts] decimal(18,0)  NULL,
    [PastingCosts] decimal(18,0)  NULL,
    [PinCosts] decimal(18,0)  NULL,
    [ElectricityCosts] decimal(18,0)  NULL,
    [ToolingCosts] decimal(18,0)  NULL,
    [VarnishCosts] decimal(18,0)  NULL
);
GO

-- Creating table 'est_EstimateTax'
CREATE TABLE [dbo].[est_EstimateTax] (
    [PK_EstimationTaxes] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_DetailedEstimation] decimal(18,0)  NULL,
    [VAT_Percentage] decimal(18,2)  NULL,
    [VAT_Value] decimal(18,2)  NULL,
    [Exercise_Percentage] decimal(18,2)  NULL,
    [Exercise_Value] decimal(18,2)  NULL,
    [SalesTax_Percentage] decimal(18,2)  NULL,
    [Sales_Value] decimal(18,2)  NULL
);
GO

-- Creating table 'FinishedGoodsMasterwwws'
CREATE TABLE [dbo].[FinishedGoodsMasterwwws] (
    [Pk_FinishedGoodsId] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [FinishedDate] datetime  NULL,
    [Fk_Customer] decimal(18,0)  NULL,
    [Fk_Order] decimal(18,0)  NULL,
    [Quantity] decimal(18,0)  NULL
);
GO

-- Creating table 'FinishedGoodsShippingDetailswwws'
CREATE TABLE [dbo].[FinishedGoodsShippingDetailswwws] (
    [Pk_ShippmentId] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_FinishedGoodsId] decimal(18,0)  NULL,
    [FirstName] nvarchar(100)  NULL,
    [LastName] nvarchar(100)  NULL,
    [Address] nvarchar(500)  NULL,
    [City] nvarchar(50)  NULL,
    [State] nvarchar(50)  NULL,
    [Pincode] decimal(18,0)  NULL,
    [Country] nvarchar(50)  NULL,
    [Mobile] decimal(18,0)  NULL,
    [Telephone] decimal(18,0)  NULL,
    [Email] nvarchar(50)  NULL,
    [Fax] nvarchar(50)  NULL
);
GO

-- Creating table 'gen_City'
CREATE TABLE [dbo].[gen_City] (
    [Pk_CityID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_StateID] decimal(18,0)  NULL,
    [CityName] varchar(50)  NOT NULL,
    [Country] decimal(18,0)  NULL
);
GO

-- Creating table 'gen_Color'
CREATE TABLE [dbo].[gen_Color] (
    [Pk_Color] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [ColorName] nvarchar(30)  NULL
);
GO

-- Creating table 'gen_Communication'
CREATE TABLE [dbo].[gen_Communication] (
    [Pk_Communication] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Type] nvarchar(200)  NULL
);
GO

-- Creating table 'gen_ContactPerson'
CREATE TABLE [dbo].[gen_ContactPerson] (
    [Pk_ContactPersonId] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [ContactPersonName] nvarchar(100)  NOT NULL,
    [ContactPersonDesignation] nvarchar(100)  NULL,
    [ContactPersonNumber] decimal(18,0)  NOT NULL,
    [ContactPersonEmailId] nvarchar(50)  NOT NULL,
    [Address_No_Street] nvarchar(100)  NOT NULL,
    [Address_State] decimal(18,0)  NOT NULL,
    [Address_City] decimal(18,0)  NOT NULL,
    [Address_PinCode] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'gen_Country'
CREATE TABLE [dbo].[gen_Country] (
    [Pk_Country] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [CountryName] varchar(50)  NOT NULL
);
GO

-- Creating table 'gen_CustomerContacts'
CREATE TABLE [dbo].[gen_CustomerContacts] (
    [Pk_CustomerContacts] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Customer] decimal(18,0)  NULL,
    [Fk_ContactPerson] decimal(18,0)  NULL
);
GO

-- Creating table 'gen_CustomerShippingDetails'
CREATE TABLE [dbo].[gen_CustomerShippingDetails] (
    [Pk_CustomerShippingId] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_CustomerId] decimal(18,0)  NULL,
    [ShippingCustomerName] nvarchar(150)  NULL,
    [ShippingAddress] nvarchar(500)  NULL,
    [ShippingCountry] nvarchar(50)  NULL,
    [ShippingState] nvarchar(50)  NULL,
    [ShippingCity] nvarchar(50)  NULL,
    [ShippingPincode] decimal(18,0)  NULL,
    [ShippingMobile] decimal(18,0)  NULL,
    [ShippingLandLine] decimal(18,0)  NULL,
    [ShippingEmailId] nvarchar(50)  NULL
);
GO

-- Creating table 'gen_Events'
CREATE TABLE [dbo].[gen_Events] (
    [Pk_Events] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [EventSource] decimal(18,0)  NOT NULL,
    [SourceType] nvarchar(50)  NOT NULL,
    [Date] datetime  NOT NULL,
    [StartTime] nvarchar(50)  NULL,
    [EndTime] nvarchar(50)  NULL,
    [EventDescription] nvarchar(500)  NULL,
    [Value] decimal(18,0)  NULL
);
GO

-- Creating table 'gen_Machine'
CREATE TABLE [dbo].[gen_Machine] (
    [Pk_Machine] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Location] nvarchar(255)  NULL,
    [Name] nvarchar(255)  NULL,
    [Capacity_Length] decimal(18,0)  NULL,
    [Capacity_Width] decimal(18,0)  NULL,
    [Capacity_Other] decimal(18,0)  NULL,
    [AdditionalInformation] nvarchar(150)  NULL
);
GO

-- Creating table 'gen_MillContacts'
CREATE TABLE [dbo].[gen_MillContacts] (
    [Pk_MillContacts] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Mill] decimal(18,0)  NULL,
    [Fk_ContactPerson] decimal(18,0)  NULL
);
GO

-- Creating table 'gen_PaperType'
CREATE TABLE [dbo].[gen_PaperType] (
    [Pk_PaperType] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [PaperTypeName] nvarchar(50)  NULL,
    [PaperDescription] nvarchar(500)  NULL
);
GO

-- Creating table 'gen_State'
CREATE TABLE [dbo].[gen_State] (
    [Pk_StateID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [StateName] varchar(50)  NOT NULL,
    [Country] decimal(18,0)  NULL
);
GO

-- Creating table 'gen_VendorContacts'
CREATE TABLE [dbo].[gen_VendorContacts] (
    [Pk_VendorContact] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Vendor] decimal(18,0)  NULL,
    [Fk_ContactPerson] decimal(18,0)  NULL
);
GO

-- Creating table 'GenReports'
CREATE TABLE [dbo].[GenReports] (
    [Pk_ReportID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [ReportName] varchar(50)  NULL
);
GO

-- Creating table 'Inv_MaterialCategory'
CREATE TABLE [dbo].[Inv_MaterialCategory] (
    [Pk_MaterialCategory] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(30)  NULL,
    [Description] nvarchar(100)  NULL
);
GO

-- Creating table 'Inv_MaterialIndentMaster'
CREATE TABLE [dbo].[Inv_MaterialIndentMaster] (
    [Pk_MaterialOrderMasterId] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [MaterialIndentDate] datetime  NULL,
    [Fk_VendorId] decimal(18,0)  NULL,
    [Fk_Status] decimal(18,0)  NULL,
    [Fk_Branch] decimal(18,0)  NULL,
    [Fk_Tanent] decimal(18,0)  NULL,
    [Comments] nvarchar(500)  NULL,
    [Fk_UserID] decimal(18,0)  NULL
);
GO

-- Creating table 'Inv_PaymentDetails'
CREATE TABLE [dbo].[Inv_PaymentDetails] (
    [Pk_PaymtNo] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_InvoiceNo] decimal(18,0)  NOT NULL,
    [CardNo] decimal(18,0)  NULL,
    [CName] varchar(50)  NULL,
    [BankName] varchar(50)  NULL,
    [CardType] varchar(50)  NULL,
    [ChequeNo] varchar(50)  NULL,
    [ChequeDate] datetime  NULL,
    [CBankName] varchar(50)  NULL,
    [TransNo] varchar(50)  NULL,
    [GateWay] varchar(50)  NULL,
    [TBankName] varchar(50)  NULL,
    [TargetedAccount] varchar(50)  NULL,
    [CashDenominations] varchar(500)  NULL
);
GO

-- Creating table 'MachineMaintenances'
CREATE TABLE [dbo].[MachineMaintenances] (
    [Pk_MachineMaintenance] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Machine] decimal(18,0)  NOT NULL,
    [FromDate] datetime  NULL,
    [ToDate] datetime  NULL,
    [DoneBy] varchar(50)  NULL,
    [Comments] varchar(500)  NULL
);
GO

-- Creating table 'MaterialQuotationDetails'
CREATE TABLE [dbo].[MaterialQuotationDetails] (
    [Pk_QuotationMaterial] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_QuotationRequest] decimal(18,0)  NULL,
    [Fk_Material] decimal(18,0)  NOT NULL,
    [Quantity] decimal(18,2)  NULL,
    [SpecialInformation] varchar(500)  NULL
);
GO

-- Creating table 'MaterialQuotationMaster'
CREATE TABLE [dbo].[MaterialQuotationMaster] (
    [Pk_QuotationRequest] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Vendor] decimal(18,0)  NULL,
    [Requests] varchar(250)  NULL,
    [RequestDate] datetime  NULL,
    [CreatedBy] decimal(18,0)  NULL
);
GO

-- Creating table 'Payment'
CREATE TABLE [dbo].[Payment] (
    [Pk_PaymentID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Amount] decimal(18,2)  NOT NULL,
    [Description] varchar(500)  NULL,
    [Fk_Vendor] decimal(18,0)  NOT NULL,
    [ChequeNo] varchar(50)  NULL,
    [ChequeDate] datetime  NULL,
    [Fk_Bank] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'PaymentTracks'
CREATE TABLE [dbo].[PaymentTracks] (
    [Pk_PaymtID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Paymt_Date] datetime  NOT NULL,
    [Fk_Invno] decimal(18,0)  NOT NULL,
    [ChequeNo] varchar(50)  NULL,
    [BankName] varchar(50)  NULL,
    [TransactionID] varchar(50)  NULL,
    [PaidAmt] decimal(18,2)  NULL,
    [BalanceAmt] decimal(18,2)  NULL,
    [BillAmt] decimal(18,2)  NULL
);
GO

-- Creating table 'PendingTracks'
CREATE TABLE [dbo].[PendingTracks] (
    [Pk_IndentTrack] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Indent] decimal(18,0)  NOT NULL,
    [Quantity] decimal(18,2)  NOT NULL,
    [AlreadyInwarded] decimal(18,2)  NOT NULL,
    [Pending] decimal(18,2)  NOT NULL,
    [Fk_Material] decimal(18,0)  NOT NULL,
    [QC_Qty] decimal(18,2)  NULL
);
GO

-- Creating table 'Receipt'
CREATE TABLE [dbo].[Receipt] (
    [Pk_ReceiptID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Customer] decimal(18,0)  NOT NULL,
    [Amount] decimal(18,2)  NOT NULL,
    [ChequeNo] nvarchar(50)  NULL,
    [ChequeDate] datetime  NULL,
    [Description] nvarchar(500)  NULL,
    [BankName] nvarchar(50)  NULL
);
GO

-- Creating table 'StockTransfer'
CREATE TABLE [dbo].[StockTransfer] (
    [Pk_TransferID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [TransferDate] datetime  NULL,
    [Fk_FromBranchID] decimal(18,0)  NOT NULL,
    [Fk_ToBranchID] decimal(18,0)  NOT NULL,
    [TransferQty] decimal(18,0)  NOT NULL,
    [Fk_Material] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'sysdiagrams'
CREATE TABLE [dbo].[sysdiagrams] (
    [name] nvarchar(128)  NOT NULL,
    [principal_id] int  NOT NULL,
    [diagram_id] int IDENTITY(1,1) NOT NULL,
    [version] int  NULL,
    [definition] varbinary(max)  NULL
);
GO

-- Creating table 'TempChecks'
CREATE TABLE [dbo].[TempChecks] (
    [Pk_Vals] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Upd_Val] int  NULL
);
GO

-- Creating table 'Voucher'
CREATE TABLE [dbo].[Voucher] (
    [Pk_ExpensesID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Amount] decimal(18,2)  NOT NULL,
    [Description] varchar(500)  NULL,
    [Fk_AccountHeadId] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'wfStates'
CREATE TABLE [dbo].[wfStates] (
    [Pk_State] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [State] nvarchar(255)  NULL
);
GO

-- Creating table 'wfTransactions'
CREATE TABLE [dbo].[wfTransactions] (
    [Pk_Transaction] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Path] decimal(18,0)  NULL,
    [Comment] nvarchar(500)  NULL,
    [Entity] decimal(18,0)  NULL,
    [Date] datetime  NULL,
    [Fk_User] decimal(18,0)  NULL
);
GO

-- Creating table 'wfTriggerParameters'
CREATE TABLE [dbo].[wfTriggerParameters] (
    [Pk_TriggerParameters] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Triggers] decimal(18,0)  NULL,
    [ParameterName] nvarchar(500)  NULL,
    [Template] nvarchar(max)  NULL
);
GO

-- Creating table 'wfTriggers'
CREATE TABLE [dbo].[wfTriggers] (
    [Pk_Triggers] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Path] decimal(18,0)  NULL,
    [TriggerType] nvarchar(255)  NULL
);
GO

-- Creating table 'wfWorkflowPaths'
CREATE TABLE [dbo].[wfWorkflowPaths] (
    [Pk_Paths] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Tanent] decimal(18,0)  NULL,
    [InitialState] bit  NULL,
    [CurrentState] decimal(18,0)  NULL,
    [NextState] decimal(18,0)  NULL,
    [PathName] nvarchar(255)  NULL,
    [AskComment] bit  NULL,
    [ActionName] nvarchar(100)  NULL,
    [IconClass] nvarchar(50)  NULL,
    [BelongsTo] nvarchar(255)  NULL,
    [Fk_WfRole] decimal(18,0)  NULL
);
GO

-- Creating table 'wfWorkflowRoles'
CREATE TABLE [dbo].[wfWorkflowRoles] (
    [Pk_WfRole] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [RoleName] nvarchar(50)  NULL
);
GO

-- Creating table 'wfWorkflowRoleUsers'
CREATE TABLE [dbo].[wfWorkflowRoleUsers] (
    [Pk_wfRoleUser] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Role] decimal(18,0)  NULL,
    [Fk_User] decimal(18,0)  NULL
);
GO

-- Creating table 'wgTenant'
CREATE TABLE [dbo].[wgTenant] (
    [Pk_Tanent] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [TanentName] nvarchar(500)  NOT NULL,
    [CreationDate] datetime  NOT NULL,
    [CreationChannel] nvarchar(50)  NULL,
    [CreatedBy] decimal(18,0)  NULL,
    [IsActive] bit  NULL
);
GO

-- Creating table 'Vw_Agreed_ActualDelivery'
CREATE TABLE [dbo].[Vw_Agreed_ActualDelivery] (
    [CustomerName] char(100)  NOT NULL,
    [Pk_Order] decimal(18,0)  NOT NULL,
    [DeliveryDate] datetime  NOT NULL,
    [Quantity] decimal(18,0)  NULL,
    [DeliveredDate] datetime  NULL,
    [Product] nvarchar(500)  NULL
);
GO

-- Creating table 'VW_BoardSearch'
CREATE TABLE [dbo].[VW_BoardSearch] (
    [MillName] char(50)  NULL,
    [Name] nvarchar(30)  NULL,
    [GSM] decimal(20,0)  NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Fk_MaterialCategory] decimal(18,0)  NULL,
    [Fk_Mill] decimal(18,0)  NULL,
    [Fk_PaperType] decimal(18,0)  NULL,
    [Fk_Color] decimal(18,0)  NULL,
    [Deckle] decimal(20,0)  NULL,
    [BF] decimal(20,0)  NULL,
    [Length] decimal(20,0)  NULL,
    [Width] decimal(20,0)  NULL,
    [Height] decimal(20,0)  NULL,
    [GSM2] decimal(20,0)  NULL,
    [PPly] decimal(20,0)  NULL,
    [FPly] decimal(20,0)  NULL,
    [Weight] decimal(20,0)  NULL,
    [Size] decimal(20,0)  NULL,
    [VendorIdNumber] decimal(18,0)  NULL,
    [Company] nvarchar(20)  NULL,
    [Brand] nvarchar(20)  NULL,
    [MaterialType] nvarchar(20)  NULL,
    [Description] nvarchar(100)  NULL,
    [Fk_UnitId] decimal(18,0)  NULL,
    [ColorName] nvarchar(30)  NULL,
    [UnitName] nvarchar(50)  NULL,
    [Mat_Cat_Name] nvarchar(30)  NULL
);
GO

-- Creating table 'vw_CustomerDetails'
CREATE TABLE [dbo].[vw_CustomerDetails] (
    [Pk_Customer] decimal(18,0)  NOT NULL,
    [CustomerName] char(100)  NOT NULL,
    [Pk_ContactPersonId] decimal(18,0)  NOT NULL,
    [ContactPersonName] nvarchar(100)  NOT NULL,
    [Pk_CustomerContacts] decimal(18,0)  NOT NULL,
    [CityName] varchar(50)  NOT NULL,
    [StateName] varchar(150)  NOT NULL,
    [CountryName] varchar(250)  NOT NULL
);
GO

-- Creating table 'Vw_CustShip'
CREATE TABLE [dbo].[Vw_CustShip] (
    [Pk_Customer] decimal(18,0)  NOT NULL,
    [Pk_CustomerShippingId] decimal(18,0)  NOT NULL,
    [CustomerName] char(100)  NOT NULL,
    [ShippingCustomerName] nvarchar(150)  NULL,
    [ShippingAddress] nvarchar(500)  NULL,
    [ShippingCountry] nvarchar(50)  NULL,
    [ShippingState] nvarchar(50)  NULL,
    [ShippingPincode] decimal(18,0)  NULL,
    [ShippingCity] nvarchar(50)  NULL,
    [ShippingMobile] decimal(18,0)  NULL,
    [ShippingLandLine] decimal(18,0)  NULL,
    [ShippingEmailId] nvarchar(50)  NULL,
    [Fk_CustomerId] decimal(18,0)  NULL
);
GO

-- Creating table 'Vw_Estimate'
CREATE TABLE [dbo].[Vw_Estimate] (
    [Pk_Enquiry] decimal(18,0)  NOT NULL,
    [CustomerName] char(100)  NOT NULL,
    [Date] datetime  NOT NULL,
    [Length] decimal(18,0)  NULL,
    [Width] decimal(18,0)  NULL,
    [Height] decimal(18,0)  NULL,
    [rn] bigint  NULL,
    [Pk_Estimate] decimal(18,0)  NOT NULL,
    [TotalWeight] decimal(18,0)  NULL,
    [TotalPrice] decimal(18,0)  NULL
);
GO

-- Creating table 'VW_FG_Production'
CREATE TABLE [dbo].[VW_FG_Production] (
    [Pk_Order] decimal(18,0)  NOT NULL,
    [Product] nvarchar(500)  NULL,
    [FinishedDate] datetime  NULL,
    [Quantity] decimal(18,0)  NULL,
    [Fk_Order] decimal(18,0)  NULL,
    [Pk_FinishedGoodsId] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'Vw_Indent'
CREATE TABLE [dbo].[Vw_Indent] (
    [Name] nvarchar(50)  NULL,
    [UnitName] nvarchar(50)  NULL,
    [MaterialIndentDate] datetime  NULL,
    [VendorName] char(50)  NOT NULL,
    [Pk_Vendor] decimal(18,0)  NOT NULL,
    [Pk_MaterialOrderMasterId] decimal(18,0)  NOT NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Quantity] decimal(18,3)  NULL,
    [Fk_CustomerOrder] decimal(18,0)  NULL,
    [Pk_MaterialOrderDetailsId] decimal(18,0)  NOT NULL,
    [Fk_Tanent] decimal(18,0)  NULL
);
GO

-- Creating table 'Vw_InOutSummary'
CREATE TABLE [dbo].[Vw_InOutSummary] (
    [Cat_Name] nvarchar(30)  NULL,
    [Pk_MaterialCategory] decimal(18,0)  NOT NULL,
    [Name] nvarchar(50)  NULL,
    [Inwd_Qty] decimal(18,2)  NOT NULL,
    [Issue_Qty] decimal(18,0)  NOT NULL,
    [UnitName] nvarchar(50)  NULL,
    [Inward_Date] datetime  NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [IssueDate] datetime  NULL
);
GO

-- Creating table 'Vw_Issue'
CREATE TABLE [dbo].[Vw_Issue] (
    [Pk_MaterialIssueID] decimal(18,0)  NOT NULL,
    [IssueDate] datetime  NULL,
    [Pk_Order] decimal(18,0)  NOT NULL,
    [Fk_Material] decimal(18,0)  NOT NULL,
    [Name] nvarchar(50)  NULL,
    [Quantity] decimal(18,0)  NOT NULL,
    [Product] nvarchar(500)  NULL,
    [DCNo] varchar(50)  NULL,
    [Pk_MaterialIssueDetailsID] decimal(18,0)  NOT NULL,
    [UnitName] nvarchar(50)  NULL
);
GO

-- Creating table 'Vw_ItemMaster'
CREATE TABLE [dbo].[Vw_ItemMaster] (
    [Cat_Name] nvarchar(30)  NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Name] nvarchar(50)  NULL,
    [Min_Value] decimal(18,0)  NULL,
    [Max_Value] decimal(18,0)  NULL,
    [UnitName] nvarchar(50)  NULL,
    [Pk_MaterialCategory] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'Vw_Material'
CREATE TABLE [dbo].[Vw_Material] (
    [Fk_Material] decimal(18,0)  NULL,
    [VendorName] char(50)  NOT NULL,
    [Name] nvarchar(50)  NULL,
    [Quantity] decimal(18,3)  NULL,
    [Pk_MaterialOrderMasterId] decimal(18,0)  NOT NULL,
    [Pk_Vendor] decimal(18,0)  NOT NULL,
    [Pk_MaterialOrderDetailsId] decimal(18,0)  NOT NULL,
    [Pending] decimal(19,2)  NULL,
    [Pk_IndentTrack] decimal(18,0)  NULL,
    [QC_Qty] decimal(18,2)  NULL
);
GO

-- Creating table 'VW_MillDuplicate'
CREATE TABLE [dbo].[VW_MillDuplicate] (
    [ContactPersonName] nvarchar(100)  NOT NULL,
    [Fk_Mill] decimal(18,0)  NULL,
    [Pk_MillContacts] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'VW_Min_Level'
CREATE TABLE [dbo].[VW_Min_Level] (
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Name] nvarchar(50)  NULL,
    [Min_Value] decimal(18,0)  NULL,
    [Max_Value] decimal(18,0)  NULL,
    [UnitName] nvarchar(50)  NULL,
    [Quantity] decimal(18,2)  NULL,
    [Cat_Name] nvarchar(30)  NULL,
    [Pk_MaterialCategory] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'Vw_MonthlyConsumption'
CREATE TABLE [dbo].[Vw_MonthlyConsumption] (
    [Cat_Name] nvarchar(30)  NULL,
    [MaterialName] nvarchar(50)  NULL,
    [Quantity] decimal(18,0)  NOT NULL,
    [Pk_MaterialIssueDetailsID] decimal(18,0)  NOT NULL,
    [RetQty] decimal(18,0)  NOT NULL,
    [Consumption] decimal(19,0)  NOT NULL,
    [IssueDate] datetime  NULL,
    [Pk_MaterialIssueID] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'vw_MonthlySalesStatus'
CREATE TABLE [dbo].[vw_MonthlySalesStatus] (
    [Pk_Invoice] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Months] int  NULL,
    [Years] int  NULL,
    [GrandTotal] decimal(18,2)  NULL
);
GO

-- Creating table 'vw_OrderDeliveryDetails'
CREATE TABLE [dbo].[vw_OrderDeliveryDetails] (
    [Pk_DeliverySechedule] decimal(18,0)  NOT NULL,
    [Pk_Order] decimal(18,0)  NOT NULL,
    [DeliveryDate] datetime  NOT NULL,
    [Quantity] decimal(18,0)  NULL,
    [CustomerName] char(100)  NOT NULL,
    [Fk_Tanent] decimal(18,0)  NULL
);
GO

-- Creating table 'Vw_PaperRecpt'
CREATE TABLE [dbo].[Vw_PaperRecpt] (
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Mat_Name] nvarchar(50)  NULL,
    [Pk_MaterialCategory] decimal(18,0)  NOT NULL,
    [Cat_Name] nvarchar(30)  NULL,
    [Quantity] decimal(18,2)  NOT NULL,
    [Inward_Date] datetime  NULL,
    [Pk_Inward] decimal(18,0)  NOT NULL,
    [Price] decimal(18,2)  NULL
);
GO

-- Creating table 'Vw_PoorQuality'
CREATE TABLE [dbo].[Vw_PoorQuality] (
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Name] nvarchar(50)  NULL,
    [UnitName] nvarchar(50)  NULL,
    [Result] bit  NULL,
    [Quantity] decimal(18,0)  NULL,
    [CheckDate] datetime  NULL
);
GO

-- Creating table 'Vw_RawQuality'
CREATE TABLE [dbo].[Vw_RawQuality] (
    [Pk_Material] decimal(18,0)  NULL,
    [Name] nvarchar(30)  NULL,
    [Pk_MaterialCategory] decimal(18,0)  NULL,
    [Mat_Name] nvarchar(50)  NULL,
    [UnitName] nvarchar(50)  NULL,
    [Pk_Vendor] decimal(18,0)  NULL,
    [VendorName] char(50)  NULL,
    [Pk_QualityCheck] decimal(18,0)  NOT NULL,
    [Quantity] decimal(18,0)  NULL,
    [Comments] varchar(500)  NULL,
    [InvoiceNumber] decimal(18,0)  NULL,
    [CheckDate] datetime  NULL,
    [Fk_IndentNumber] decimal(18,0)  NULL
);
GO

-- Creating table 'vw_StockLedger'
CREATE TABLE [dbo].[vw_StockLedger] (
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Name] nvarchar(50)  NULL,
    [Inwd_Qty] decimal(38,2)  NOT NULL,
    [Issue_Qty] decimal(38,0)  NOT NULL,
    [RQty] decimal(38,0)  NOT NULL,
    [StockQty] decimal(18,2)  NULL,
    [Fk_Tanent] decimal(18,0)  NOT NULL,
    [Pk_Stock] decimal(18,0)  NOT NULL,
    [Fk_BranchID] decimal(18,0)  NULL
);
GO

-- Creating table 'vw_StockMaterial'
CREATE TABLE [dbo].[vw_StockMaterial] (
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Name] nvarchar(50)  NULL,
    [Quantity] decimal(18,2)  NULL,
    [Min_Value] decimal(18,0)  NULL,
    [Pk_Stock] decimal(18,0)  NOT NULL,
    [Fk_Tanent] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'Vw_StockSum'
CREATE TABLE [dbo].[Vw_StockSum] (
    [Pk_MaterialCategory] decimal(18,0)  NOT NULL,
    [Cat_Name] nvarchar(30)  NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Mat_Name] nvarchar(50)  NULL,
    [Quantity] decimal(18,2)  NULL
);
GO

-- Creating table 'vw_VendorDetails'
CREATE TABLE [dbo].[vw_VendorDetails] (
    [Pk_Vendor] decimal(18,0)  NOT NULL,
    [VendorName] char(50)  NOT NULL,
    [Pk_ContactPersonId] decimal(18,0)  NOT NULL,
    [ContactPersonName] nvarchar(100)  NOT NULL,
    [Pk_VendorContact] decimal(18,0)  NOT NULL,
    [CityName] varchar(50)  NOT NULL,
    [StateName] varchar(50)  NOT NULL,
    [CountryName] varchar(50)  NOT NULL
);
GO

-- Creating table 'C_est_Estimation1'
CREATE TABLE [dbo].[C_est_Estimation1] (
    [PK_DetailedEstimate] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Enquiry] decimal(18,0)  NULL,
    [OuterShellRequired] bit  NULL,
    [CapRequired] bit  NULL,
    [LengthPartitionRequired] bit  NULL,
    [WidthPartitionRequired] bit  NULL,
    [PlateRequired] bit  NULL,
    [NumberOfOuterShells] int  NULL,
    [NumberOfCaps] int  NULL,
    [NumberOfLengthPartition] int  NULL,
    [NumberOfWidthPartition] int  NULL,
    [NumberOfPlates] int  NULL,
    [TotalWeight] decimal(18,3)  NULL,
    [TotalCost] decimal(18,2)  NULL
);
GO

-- Creating table 'C_est_Estimation3'
CREATE TABLE [dbo].[C_est_Estimation3] (
    [PK_DetailedEstimate] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Enquiry] decimal(18,0)  NULL,
    [OuterShellRequired] bit  NULL,
    [CapRequired] bit  NULL,
    [LengthPartitionRequired] bit  NULL,
    [WidthPartitionRequired] bit  NULL,
    [PlateRequired] bit  NULL,
    [NumberOfOuterShells] int  NULL,
    [NumberOfCaps] int  NULL,
    [NumberOfLengthPartition] int  NULL,
    [NumberOfWidthPartition] int  NULL,
    [NumberOfPlates] int  NULL,
    [TotalWeight] decimal(18,3)  NULL,
    [TotalCost] decimal(18,2)  NULL
);
GO

-- Creating table 'C_est_Estimation4'
CREATE TABLE [dbo].[C_est_Estimation4] (
    [PK_DetailedEstimate] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Enquiry] decimal(18,0)  NULL,
    [OuterShellRequired] bit  NULL,
    [CapRequired] bit  NULL,
    [LengthPartitionRequired] bit  NULL,
    [WidthPartitionRequired] bit  NULL,
    [PlateRequired] bit  NULL,
    [NumberOfOuterShells] int  NULL,
    [NumberOfCaps] int  NULL,
    [NumberOfLengthPartition] int  NULL,
    [NumberOfWidthPartition] int  NULL,
    [NumberOfPlates] int  NULL,
    [TotalWeight] decimal(18,3)  NULL,
    [TotalCost] decimal(18,2)  NULL
);
GO

-- Creating table 'C_est_Estimation5'
CREATE TABLE [dbo].[C_est_Estimation5] (
    [PK_DetailedEstimate] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Enquiry] decimal(18,0)  NULL,
    [OuterShellRequired] bit  NULL,
    [CapRequired] bit  NULL,
    [LengthPartitionRequired] bit  NULL,
    [WidthPartitionRequired] bit  NULL,
    [PlateRequired] bit  NULL,
    [NumberOfOuterShells] int  NULL,
    [NumberOfCaps] int  NULL,
    [NumberOfLengthPartition] int  NULL,
    [NumberOfWidthPartition] int  NULL,
    [NumberOfPlates] int  NULL,
    [TotalWeight] decimal(18,3)  NULL,
    [TotalCost] decimal(18,2)  NULL
);
GO

-- Creating table 'est_BoxEstimation'
CREATE TABLE [dbo].[est_BoxEstimation] (
    [Pk_BoxEstimation] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Date] datetime  NOT NULL,
    [GrandTotal] decimal(18,0)  NULL
);
GO

-- Creating table 'Vw_Qc'
CREATE TABLE [dbo].[Vw_Qc] (
    [Pending] decimal(38,0)  NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [Name] nvarchar(50)  NULL,
    [AccQty] decimal(18,0)  NULL,
    [FkQualityCheck] decimal(18,0)  NULL,
    [InwdQty] decimal(18,0)  NOT NULL,
    [Pk_QualityChild] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'Vw_EstDet1'
CREATE TABLE [dbo].[Vw_EstDet1] (
    [Fk_BoxID] decimal(18,0)  NULL,
    [Fk_EnquiryChild] decimal(18,0)  NULL,
    [Pk_BoxEstimationChild] decimal(18,0)  NOT NULL,
    [Fk_BoxEstimation] decimal(18,0)  NULL,
    [Name] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'BoxChild'
CREATE TABLE [dbo].[BoxChild] (
    [Pk_BoxCID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_BoxID] decimal(18,0)  NULL,
    [Fk_Material] decimal(18,0)  NULL
);
GO

-- Creating table 'PaperStock'
CREATE TABLE [dbo].[PaperStock] (
    [Pk_PaperStock] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [RollNo] varchar(50)  NULL,
    [Quantity] decimal(18,0)  NULL
);
GO

-- Creating table 'Vw_BoxPaperStock'
CREATE TABLE [dbo].[Vw_BoxPaperStock] (
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Name] nvarchar(50)  NULL,
    [Fk_BoxID] decimal(18,0)  NULL,
    [Pk_BoxCID] decimal(18,0)  NOT NULL,
    [Expr1] decimal(18,0)  NOT NULL,
    [Quantity] decimal(18,0)  NULL,
    [RollNo] varchar(50)  NULL
);
GO

-- Creating table 'Tax'
CREATE TABLE [dbo].[Tax] (
    [PkTax] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [TaxName] varchar(50)  NULL,
    [TaxValue] decimal(18,2)  NULL
);
GO

-- Creating table 'Vw_MaterialParts'
CREATE TABLE [dbo].[Vw_MaterialParts] (
    [Name] nvarchar(50)  NULL,
    [Fk_MaterialCategory] decimal(18,0)  NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Fk_Mill] decimal(18,0)  NULL,
    [Fk_PaperType] decimal(18,0)  NULL,
    [Fk_Color] decimal(18,0)  NULL,
    [Deckle] decimal(20,0)  NULL,
    [GSM] decimal(20,0)  NULL,
    [BF] decimal(20,0)  NULL,
    [Length] decimal(20,0)  NULL,
    [Width] decimal(20,0)  NULL,
    [Height] decimal(20,0)  NULL,
    [GSM2] decimal(20,0)  NULL,
    [PPly] decimal(20,0)  NULL,
    [FPly] decimal(20,0)  NULL,
    [Weight] decimal(20,0)  NULL,
    [Size] decimal(20,0)  NULL,
    [VendorIdNumber] decimal(18,0)  NULL,
    [Company] nvarchar(20)  NULL,
    [Brand] nvarchar(20)  NULL,
    [MaterialType] nvarchar(20)  NULL,
    [Description] nvarchar(100)  NULL,
    [Fk_UnitId] decimal(18,0)  NULL,
    [Min_Value] decimal(18,0)  NULL,
    [Moisture] decimal(20,0)  NULL,
    [Max_Value] decimal(18,0)  NULL,
    [GSM3] decimal(18,0)  NULL,
    [PartNo] nvarchar(50)  NULL
);
GO

-- Creating table 'Vw_BoxPaper'
CREATE TABLE [dbo].[Vw_BoxPaper] (
    [RollNo] varchar(50)  NULL,
    [Pk_BoxID] decimal(18,0)  NOT NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Name] nvarchar(50)  NULL,
    [Quantity] decimal(18,0)  NULL
);
GO

-- Creating table 'gen_DeliverySchedule'
CREATE TABLE [dbo].[gen_DeliverySchedule] (
    [Pk_DeliverySechedule] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_BoxID] decimal(18,0)  NULL,
    [DeliveryDate] datetime  NOT NULL,
    [Quantity] decimal(18,0)  NULL,
    [Comments] nvarchar(500)  NULL,
    [DeliveryCompleted] bit  NULL,
    [DeliveredDate] datetime  NULL,
    [DeliveryCancelled] bit  NULL,
    [Fk_Order] decimal(18,0)  NULL,
    [OrdQty] decimal(18,0)  NULL,
    [Fk_PartID] decimal(18,0)  NULL,
    [Fk_Material] decimal(18,0)  NULL
);
GO

-- Creating table 'Vw_BoxPQty'
CREATE TABLE [dbo].[Vw_BoxPQty] (
    [sPaper] decimal(38,0)  NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Pk_Order] decimal(18,0)  NOT NULL,
    [BName] nvarchar(50)  NOT NULL,
    [MName] nvarchar(50)  NULL,
    [PaperStkQty] decimal(18,0)  NULL,
    [Pk_BoxID] decimal(18,0)  NOT NULL,
    [LayerPaperWt] decimal(18,2)  NULL,
    [OrdQty] decimal(18,0)  NULL
);
GO

-- Creating table 'Vw_BoxPReq'
CREATE TABLE [dbo].[Vw_BoxPReq] (
    [BName] nvarchar(50)  NOT NULL,
    [Pk_Order] decimal(18,0)  NOT NULL,
    [MName] nvarchar(50)  NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [sPaper] decimal(38,0)  NULL,
    [OrdQty] decimal(18,0)  NULL,
    [PReq] decimal(37,2)  NULL,
    [Pk_BoxID] decimal(18,0)  NOT NULL,
    [LayerPaperWt] decimal(18,2)  NULL
);
GO

-- Creating table 'Vw_EnqReport'
CREATE TABLE [dbo].[Vw_EnqReport] (
    [Pk_Enquiry] decimal(18,0)  NOT NULL,
    [Date] datetime  NOT NULL,
    [CustomerName] char(100)  NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [Quantity] decimal(18,0)  NULL,
    [Pk_BoxID] decimal(18,0)  NOT NULL,
    [Ddate] datetime  NULL
);
GO

-- Creating table 'eq_EnquiryChild'
CREATE TABLE [dbo].[eq_EnquiryChild] (
    [Pk_EnquiryChild] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Quantity] decimal(18,0)  NULL,
    [Fk_Enquiry] decimal(18,0)  NULL,
    [Fk_BoxID] decimal(18,0)  NULL,
    [Ddate] datetime  NULL,
    [Amount] decimal(18,0)  NULL,
    [ConvValue] decimal(18,0)  NULL,
    [Estimated] bit  NULL
);
GO

-- Creating table 'POReturnMaster'
CREATE TABLE [dbo].[POReturnMaster] (
    [Pk_PoRetID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [PoRetDate] datetime  NULL,
    [Fk_PoNo] decimal(18,0)  NULL
);
GO

-- Creating table 'BarCodeT'
CREATE TABLE [dbo].[BarCodeT] (
    [Id] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_BoxID] decimal(18,0)  NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [Fk_PoNo] decimal(18,0)  NULL,
    [Fk_JobCardNo] decimal(18,0)  NULL,
    [JCDate] datetime  NULL,
    [AssStkQty] decimal(18,0)  NULL,
    [PODate] datetime  NULL,
    [InwdQty] decimal(18,0)  NULL,
    [POQty] decimal(18,0)  NULL,
    [PendingQty] decimal(18,0)  NULL,
    [BName] varchar(50)  NULL,
    [MName] varchar(50)  NULL,
    [ExStk] decimal(18,0)  NULL
);
GO

-- Creating table 'BoardDesc'
CREATE TABLE [dbo].[BoardDesc] (
    [Pk_Value] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_BoardID] decimal(18,0)  NULL,
    [LayerID] decimal(18,0)  NULL,
    [GSM] decimal(18,0)  NULL,
    [BF] decimal(18,0)  NULL,
    [Color] varchar(50)  NULL,
    [Fk_Material] decimal(18,0)  NULL
);
GO

-- Creating table 'Vw_BoardDesc'
CREATE TABLE [dbo].[Vw_BoardDesc] (
    [Pk_Material] decimal(18,0)  NOT NULL,
    [GSM] decimal(18,0)  NULL,
    [BF] decimal(18,0)  NULL,
    [Color] varchar(50)  NULL,
    [Name] nvarchar(50)  NULL,
    [Length] decimal(20,0)  NULL,
    [Width] decimal(20,0)  NULL,
    [Height] decimal(20,0)  NULL
);
GO

-- Creating table 'OD_ID'
CREATE TABLE [dbo].[OD_ID] (
    [Pk_ID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [PlyValue] decimal(18,0)  NULL,
    [Length] decimal(18,0)  NULL,
    [Width] decimal(18,0)  NULL,
    [Height] decimal(18,0)  NULL,
    [Fk_FluteID] decimal(18,0)  NULL
);
GO

-- Creating table 'est_BoxEstimationChild'
CREATE TABLE [dbo].[est_BoxEstimationChild] (
    [Pk_BoxEstimationChild] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_BoxEstimation] decimal(18,0)  NULL,
    [Fk_EnquiryChild] decimal(18,0)  NULL,
    [ConvRate] decimal(18,2)  NULL,
    [ConvValue] decimal(18,2)  NULL,
    [GMarginPercentage] decimal(18,2)  NULL,
    [GMarginValue] decimal(18,2)  NULL,
    [TaxesPercntage] decimal(18,2)  NULL,
    [TaxesValue] decimal(18,2)  NULL,
    [TransportValue] decimal(18,2)  NULL,
    [WeightHValue] decimal(18,2)  NULL,
    [HandlingChanrgesValue] decimal(18,2)  NULL,
    [PackingChargesValue] decimal(18,2)  NULL,
    [RejectionPercentage] decimal(18,2)  NULL,
    [RejectionValue] decimal(18,2)  NULL,
    [TotalWeight] decimal(18,2)  NULL,
    [TotalPrice] decimal(18,2)  NULL,
    [Status] bit  NULL,
    [VATPercentage] decimal(18,2)  NULL,
    [VATValue] decimal(18,2)  NULL,
    [CSTPercentage] decimal(18,2)  NULL,
    [CSTValue] decimal(18,2)  NULL,
    [TransportPercentage] decimal(18,2)  NULL
);
GO

-- Creating table 'eq_Documents'
CREATE TABLE [dbo].[eq_Documents] (
    [Pk_Documents] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Enquiry] decimal(18,0)  NULL,
    [DocumentLink] nvarchar(500)  NULL,
    [FileName] nvarchar(255)  NULL,
    [FileSize] nvarchar(50)  NULL
);
GO

-- Creating table 'eq_Enquiry'
CREATE TABLE [dbo].[eq_Enquiry] (
    [Pk_Enquiry] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [CommunicationType] decimal(18,0)  NULL,
    [Customer] decimal(18,0)  NULL,
    [Description] nvarchar(50)  NULL,
    [Date] datetime  NOT NULL,
    [SpecialRequirements] nvarchar(50)  NULL,
    [EqComments] nvarchar(50)  NULL,
    [Fk_Status] decimal(18,0)  NULL,
    [DocumentLink] varchar(500)  NULL,
    [Fk_Branch] decimal(18,0)  NULL,
    [Fk_Tanent] decimal(18,0)  NULL,
    [RefNo] nchar(50)  NULL,
    [SampleRecd] nchar(10)  NULL
);
GO

-- Creating table 'Vw_Inwd'
CREATE TABLE [dbo].[Vw_Inwd] (
    [Pk_QualityCheck] decimal(18,0)  NOT NULL,
    [CheckDate] varchar(30)  NULL,
    [Fk_PONo] decimal(18,0)  NULL,
    [Expr1] datetime  NULL,
    [Fk_Indent] decimal(18,0)  NULL
);
GO

-- Creating table 'ideActivity'
CREATE TABLE [dbo].[ideActivity] (
    [Pk_Activities] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Tanent] decimal(18,0)  NULL,
    [ActivityName] varchar(250)  NULL,
    [Link] varchar(500)  NULL,
    [NavigationParameters] varchar(500)  NULL,
    [ImageName] varchar(500)  NULL,
    [Fk_ParentActivity] decimal(18,0)  NULL,
    [OrderBy] decimal(18,0)  NULL
);
GO

-- Creating table 'ideActivityPermission'
CREATE TABLE [dbo].[ideActivityPermission] (
    [Pk_ActivityPermissions] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Activities] decimal(18,0)  NULL,
    [Fk_Permissions] decimal(18,0)  NULL,
    [Fk_Roles] decimal(18,0)  NULL
);
GO

-- Creating table 'idePermission'
CREATE TABLE [dbo].[idePermission] (
    [Pk_Permissions] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [PermissionName] varchar(250)  NULL,
    [PermissionCode] varchar(50)  NULL,
    [Description] varchar(500)  NULL
);
GO

-- Creating table 'ideRole'
CREATE TABLE [dbo].[ideRole] (
    [Pk_Roles] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [RoleName] varchar(250)  NULL,
    [Description] varchar(250)  NULL
);
GO

-- Creating table 'ideRoleUser'
CREATE TABLE [dbo].[ideRoleUser] (
    [Pk_RoleUsers] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Roles] decimal(18,0)  NULL,
    [Fk_Users] decimal(18,0)  NULL
);
GO

-- Creating table 'ideShortCut'
CREATE TABLE [dbo].[ideShortCut] (
    [Pk_Shortcuts] decimal(18,0)  NOT NULL,
    [ShortCut] varchar(500)  NULL,
    [Fk_Module] decimal(18,0)  NULL,
    [Fk_User] decimal(18,0)  NULL
);
GO

-- Creating table 'PapInsert'
CREATE TABLE [dbo].[PapInsert] (
    [Pk_ID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [GSM] decimal(18,0)  NULL,
    [BF] decimal(18,0)  NULL,
    [DECKEL] decimal(18,0)  NULL,
    [REEL] decimal(18,0)  NULL,
    [WT] decimal(18,0)  NULL
);
GO

-- Creating table 'gen_Unit'
CREATE TABLE [dbo].[gen_Unit] (
    [Pk_Unit] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [UnitName] nvarchar(50)  NULL,
    [UnitType] nvarchar(50)  NULL,
    [Description] nvarchar(300)  NULL
);
GO

-- Creating table 'Stocks'
CREATE TABLE [dbo].[Stocks] (
    [Pk_Stock] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [Quantity] decimal(18,2)  NULL,
    [SafeValue] decimal(18,2)  NULL,
    [Fk_Tanent] decimal(18,0)  NOT NULL,
    [Fk_BranchID] decimal(18,0)  NULL
);
GO

-- Creating table 'gen_Vendor'
CREATE TABLE [dbo].[gen_Vendor] (
    [Fk_Tanent] decimal(18,0)  NOT NULL,
    [Pk_Vendor] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [VendorName] char(50)  NOT NULL,
    [VendorType] char(30)  NOT NULL,
    [OfficeContactNumber] decimal(30,0)  NOT NULL,
    [Email] nvarchar(50)  NOT NULL,
    [Address_No_Street] nvarchar(100)  NOT NULL,
    [Fk_State] decimal(18,0)  NOT NULL,
    [Fk_Country] decimal(18,0)  NOT NULL,
    [Fk_City] decimal(18,0)  NOT NULL,
    [PinCode] decimal(18,0)  NOT NULL,
    [LandLine] decimal(18,0)  NULL,
    [MobileNumber] decimal(18,0)  NULL,
    [Product] nchar(10)  NULL
);
GO

-- Creating table 'gen_VendorMaterials'
CREATE TABLE [dbo].[gen_VendorMaterials] (
    [Pk_VendorMaterial] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Vendor] decimal(18,0)  NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [Rate] decimal(18,2)  NULL
);
GO

-- Creating table 'ideUser'
CREATE TABLE [dbo].[ideUser] (
    [Pk_Users] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Tanent] decimal(18,0)  NULL,
    [Fk_Branch] decimal(18,0)  NULL,
    [UserName] nvarchar(200)  NULL,
    [Password] nvarchar(200)  NULL,
    [FirstName] nvarchar(500)  NULL,
    [LastName] nvarchar(500)  NULL,
    [Office] nvarchar(50)  NULL,
    [IsActive] bit  NULL,
    [CreatedDate] datetime  NULL
);
GO

-- Creating table 'IssueReturn'
CREATE TABLE [dbo].[IssueReturn] (
    [Pk_IssueReturnMasterId] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [IssueReturnDate] datetime  NULL,
    [Fk_EnteredUserId] decimal(18,0)  NULL,
    [Fk_IssueId] decimal(18,0)  NULL,
    [IssueDate] datetime  NULL,
    [Fk_Tanent] decimal(18,0)  NULL,
    [Fk_Branch] decimal(18,0)  NULL
);
GO

-- Creating table 'IssueReturnDetails'
CREATE TABLE [dbo].[IssueReturnDetails] (
    [Pk_IssueReturnDetailsId] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_IssueReturnMasterId] decimal(18,0)  NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [ReturnQuantity] decimal(18,0)  NULL,
    [Pk_StockID] decimal(18,0)  NULL,
    [RollNo] nvarchar(50)  NULL
);
GO

-- Creating table 'JobCardDetails'
CREATE TABLE [dbo].[JobCardDetails] (
    [Pk_JobCardDet] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_JobCardID] decimal(18,0)  NULL,
    [Fk_PaperStock] decimal(18,0)  NULL,
    [QtyConsumed] decimal(18,0)  NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [RM_Consumed] decimal(18,0)  NULL
);
GO

-- Creating table 'MachineMaintenance1Set'
CREATE TABLE [dbo].[MachineMaintenance1Set] (
    [Pk_MachineMaintenance] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Machine] decimal(18,0)  NOT NULL,
    [FromDate] datetime  NULL,
    [ToDate] datetime  NULL,
    [DoneBy] varchar(50)  NULL,
    [Comments] varchar(500)  NULL
);
GO

-- Creating table 'PaymentTrack1Set'
CREATE TABLE [dbo].[PaymentTrack1Set] (
    [Pk_PaymtID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Paymt_Date] datetime  NOT NULL,
    [Fk_Invno] decimal(18,0)  NOT NULL,
    [ChequeNo] varchar(50)  NULL,
    [BankName] varchar(50)  NULL,
    [TransactionID] varchar(50)  NULL,
    [PaidAmt] decimal(18,2)  NULL,
    [BalanceAmt] decimal(18,2)  NULL,
    [BillAmt] decimal(18,2)  NULL
);
GO

-- Creating table 'POReturnDetails'
CREATE TABLE [dbo].[POReturnDetails] (
    [Pk_PoRetDetID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_PoRetID] decimal(18,0)  NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [Quantity] decimal(18,0)  NULL,
    [Rate] decimal(18,2)  NULL,
    [Amount] decimal(18,2)  NULL
);
GO

-- Creating table 'Tax1Set'
CREATE TABLE [dbo].[Tax1Set] (
    [PkTax] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [TaxName] varchar(50)  NULL,
    [TaxValue] decimal(18,2)  NULL
);
GO

-- Creating table 'wfWorkflowRoleUser1Set'
CREATE TABLE [dbo].[wfWorkflowRoleUser1Set] (
    [Pk_wfRoleUser] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Role] decimal(18,0)  NULL,
    [Fk_User] decimal(18,0)  NULL
);
GO

-- Creating table 'gen_Mill'
CREATE TABLE [dbo].[gen_Mill] (
    [Pk_Mill] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [MillName] nvarchar(150)  NULL,
    [Address] varchar(100)  NULL,
    [ContactNumber] decimal(18,0)  NULL,
    [Email] varchar(50)  NULL,
    [ContactPerson1] varchar(20)  NULL,
    [Website] varchar(50)  NULL,
    [FkTenantID] decimal(18,0)  NOT NULL,
    [LandLine] decimal(18,0)  NULL,
    [SName] nvarchar(50)  NULL
);
GO

-- Creating table 'Inv_Material'
CREATE TABLE [dbo].[Inv_Material] (
    [Pk_Material] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_MaterialCategory] decimal(18,0)  NULL,
    [Name] nvarchar(250)  NULL,
    [Fk_Mill] decimal(18,0)  NULL,
    [Fk_PaperType] decimal(18,0)  NULL,
    [Fk_Color] decimal(18,0)  NULL,
    [Deckle] decimal(20,2)  NULL,
    [GSM] decimal(20,0)  NULL,
    [BF] decimal(20,0)  NULL,
    [Length] decimal(20,0)  NULL,
    [Width] decimal(20,0)  NULL,
    [Height] decimal(20,0)  NULL,
    [GSM2] decimal(20,0)  NULL,
    [PPly] decimal(20,0)  NULL,
    [FPly] decimal(20,0)  NULL,
    [Weight] decimal(20,0)  NULL,
    [Size] decimal(20,0)  NULL,
    [VendorIdNumber] decimal(18,0)  NULL,
    [Company] nvarchar(20)  NULL,
    [Brand] nvarchar(20)  NULL,
    [MaterialType] nvarchar(20)  NULL,
    [Description] nvarchar(100)  NULL,
    [Fk_UnitId] decimal(18,0)  NULL,
    [Moisture] decimal(20,0)  NULL,
    [Min_Value] decimal(18,0)  NULL,
    [Max_Value] decimal(18,0)  NULL,
    [GSM3] decimal(18,0)  NULL,
    [PartNo] nvarchar(50)  NULL,
    [BF2] decimal(18,0)  NULL,
    [BF3] decimal(18,0)  NULL,
    [BF4] decimal(18,0)  NULL,
    [BF5] decimal(18,0)  NULL,
    [GSM4] decimal(18,0)  NULL,
    [GSM5] decimal(18,0)  NULL
);
GO

-- Creating table 'MaterialIssueDetails'
CREATE TABLE [dbo].[MaterialIssueDetails] (
    [Pk_MaterialIssueDetailsID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_IssueID] decimal(18,0)  NOT NULL,
    [Fk_Material] decimal(18,0)  NOT NULL,
    [Quantity] decimal(18,0)  NOT NULL,
    [ReturnQuantity] decimal(18,0)  NULL,
    [Weight] decimal(18,0)  NULL,
    [Pk_StockID] decimal(18,0)  NULL,
    [RollNo] nvarchar(50)  NULL,
    [Fk_Mill] decimal(18,0)  NULL
);
GO

-- Creating table 'Items_Layers'
CREATE TABLE [dbo].[Items_Layers] (
    [Pk_LayerID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_PartId] decimal(18,0)  NULL,
    [GSM] decimal(18,0)  NULL,
    [BF] decimal(18,0)  NULL,
    [Color] nvarchar(50)  NULL,
    [Rate] decimal(18,2)  NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [Weight] decimal(18,3)  NULL,
    [Tk_Factor] decimal(18,2)  NULL
);
GO

-- Creating table 'FluteType'
CREATE TABLE [dbo].[FluteType] (
    [Pk_FluteID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [FluteName] varchar(50)  NULL,
    [TKFactor] decimal(18,2)  NULL,
    [Height] decimal(18,2)  NULL
);
GO

-- Creating table 'Trans_Invoice'
CREATE TABLE [dbo].[Trans_Invoice] (
    [Pk_ID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [VehicleNo] nvarchar(50)  NULL,
    [VehicleName] nvarchar(100)  NULL,
    [DriverName] nvarchar(70)  NULL,
    [MobileNo] nvarchar(50)  NULL,
    [Pickup_Point] nvarchar(100)  NULL,
    [Drop_Point] nvarchar(100)  NULL,
    [Invno] nvarchar(50)  NULL,
    [InvDate] datetime  NULL,
    [Amount] decimal(18,0)  NULL,
    [CustomerName] nvarchar(50)  NULL,
    [Fk_Transporter] decimal(18,0)  NULL,
    [BalAmt] decimal(18,0)  NULL
);
GO

-- Creating table 'Trans_Payment'
CREATE TABLE [dbo].[Trans_Payment] (
    [Pk_ID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Transporter] decimal(18,0)  NULL,
    [AmtPaid] decimal(18,0)  NULL,
    [InvNo] nvarchar(50)  NULL,
    [ChequeNo] nvarchar(50)  NULL,
    [ChequeDate] datetime  NULL,
    [Pay_Date] datetime  NULL
);
GO

-- Creating table 'TransporterM'
CREATE TABLE [dbo].[TransporterM] (
    [Pk_ID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [TransporterName] nvarchar(100)  NULL,
    [Address1] nvarchar(100)  NULL,
    [MobileNo] nvarchar(50)  NULL,
    [LLine] nvarchar(50)  NULL
);
GO

-- Creating table 'QuotationDetails'
CREATE TABLE [dbo].[QuotationDetails] (
    [Pk_QuotationDetID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_QuotationID] decimal(18,0)  NULL,
    [Quantity] decimal(18,0)  NULL,
    [CostPerBox] decimal(18,2)  NULL,
    [BSValue] decimal(18,2)  NULL,
    [Fk_EstimationID] decimal(18,0)  NULL,
    [BoxID] decimal(18,0)  NULL,
    [BName] nvarchar(50)  NULL,
    [Dimension] nvarchar(100)  NULL
);
GO

-- Creating table 'Vw_Quote'
CREATE TABLE [dbo].[Vw_Quote] (
    [Pk_Quotation] decimal(18,0)  NOT NULL,
    [CustomerName] char(100)  NOT NULL,
    [QuotationDate] datetime  NULL,
    [Pk_Enquiry] decimal(18,0)  NOT NULL,
    [Date] datetime  NOT NULL,
    [Quantity] decimal(18,0)  NULL,
    [Pk_BoxID] decimal(18,0)  NULL,
    [Name] nvarchar(50)  NULL,
    [Fk_Enquiry] decimal(18,0)  NULL
);
GO

-- Creating table 'emp_EmployeeMaster'
CREATE TABLE [dbo].[emp_EmployeeMaster] (
    [Pk_EmployeeId] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [EmployeeId] nvarchar(50)  NULL,
    [FirstName] nvarchar(50)  NOT NULL,
    [LastName] nvarchar(50)  NULL,
    [FatherName] nvarchar(50)  NULL,
    [MotherName] nvarchar(50)  NULL,
    [DOB] datetime  NULL,
    [Address] nvarchar(250)  NULL,
    [Fk_City] decimal(18,0)  NOT NULL,
    [Fk_State] decimal(18,0)  NOT NULL,
    [Pincode] decimal(18,0)  NULL,
    [Mobile] nvarchar(50)  NULL,
    [Phone] nvarchar(50)  NULL,
    [Email] nvarchar(50)  NULL,
    [Fk_DepartmentId] decimal(18,0)  NOT NULL,
    [Fk_EmployeeDesignation] decimal(18,0)  NOT NULL,
    [IsActive] bit  NULL,
    [AadharCardNo] nvarchar(100)  NULL,
    [PanNo] nvarchar(100)  NULL,
    [AcNo] nvarchar(50)  NULL
);
GO

-- Creating table 'InkChar'
CREATE TABLE [dbo].[InkChar] (
    [Pk_CharID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(100)  NULL
);
GO

-- Creating table 'WireCertificate'
CREATE TABLE [dbo].[WireCertificate] (
    [Pk_Id] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [Invno] nvarchar(50)  NULL,
    [InvDate] datetime  NULL,
    [Quantity] decimal(18,0)  NULL,
    [Tested] nvarchar(50)  NULL,
    [Approved] nvarchar(50)  NULL,
    [Fk_Vendor] decimal(18,0)  NULL
);
GO

-- Creating table 'WireCertificateDetails'
CREATE TABLE [dbo].[WireCertificateDetails] (
    [Pk_IdDet] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_ID] decimal(18,0)  NULL,
    [Fk_Params] decimal(18,0)  NULL,
    [Observed] nvarchar(150)  NULL,
    [Remarks] nvarchar(150)  NULL,
    [Status] nchar(10)  NULL
);
GO

-- Creating table 'WireChar'
CREATE TABLE [dbo].[WireChar] (
    [Pk_CharID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(100)  NULL,
    [StdVal] nvarchar(50)  NULL
);
GO

-- Creating table 'QuotationMaster'
CREATE TABLE [dbo].[QuotationMaster] (
    [Pk_Quotation] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Grandtotal] decimal(18,2)  NULL,
    [Description] nvarchar(max)  NULL,
    [QuotationDate] datetime  NULL,
    [Fk_Enquiry] decimal(18,0)  NULL,
    [Terms] nvarchar(max)  NULL
);
GO

-- Creating table 'PaperChar'
CREATE TABLE [dbo].[PaperChar] (
    [Pk_CharID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(100)  NULL,
    [UOM] nvarchar(50)  NULL,
    [TestMethod] nvarchar(50)  NULL
);
GO

-- Creating table 'Vw_WireCertificate'
CREATE TABLE [dbo].[Vw_WireCertificate] (
    [Name] nvarchar(250)  NULL,
    [Pk_Id] decimal(18,0)  NOT NULL,
    [Fk_Params] decimal(18,0)  NULL,
    [Observed] nvarchar(150)  NULL,
    [Remarks] nvarchar(150)  NULL,
    [Invno] nvarchar(50)  NULL,
    [InvDate] datetime  NULL,
    [Quantity] decimal(18,0)  NULL,
    [Tested] nvarchar(50)  NULL,
    [Approved] nvarchar(50)  NULL,
    [Expr1] nvarchar(100)  NULL,
    [StdVal] nvarchar(50)  NULL,
    [VendorName] char(50)  NOT NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [Pk_IdDet] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'InkCertificate'
CREATE TABLE [dbo].[InkCertificate] (
    [Pk_Id] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [BatchNo] nvarchar(50)  NULL,
    [Description] nvarchar(150)  NULL,
    [Quantity] decimal(18,0)  NULL,
    [Tested] nvarchar(50)  NULL,
    [Approved] nvarchar(50)  NULL
);
GO

-- Creating table 'Vw_InkCertificate'
CREATE TABLE [dbo].[Vw_InkCertificate] (
    [Pk_IdDet] decimal(18,0)  NOT NULL,
    [Fk_Params] decimal(18,0)  NULL,
    [Spec_Target] nvarchar(150)  NULL,
    [Spec_Tolerance] nvarchar(150)  NULL,
    [Results] nvarchar(50)  NULL,
    [Name] nvarchar(100)  NULL,
    [Expr1] nvarchar(250)  NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [Pk_Id] decimal(18,0)  NOT NULL,
    [BatchNo] nvarchar(50)  NULL,
    [Description] nvarchar(150)  NULL,
    [Quantity] decimal(18,0)  NULL,
    [Tested] nvarchar(50)  NULL,
    [Approved] nvarchar(50)  NULL
);
GO

-- Creating table 'GlueCertificate'
CREATE TABLE [dbo].[GlueCertificate] (
    [Pk_Id] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [Invno] nvarchar(50)  NULL,
    [InvDate] datetime  NULL,
    [Quantity] decimal(18,0)  NULL,
    [Tested] nvarchar(50)  NULL,
    [Approved] nvarchar(50)  NULL,
    [Fk_Vendor] decimal(18,0)  NULL
);
GO

-- Creating table 'GlueCertificateDetails'
CREATE TABLE [dbo].[GlueCertificateDetails] (
    [Pk_IdDet] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_PkID] decimal(18,0)  NULL,
    [PCode] nvarchar(150)  NULL,
    [MixingRatio] nvarchar(150)  NULL,
    [GelTemp] nchar(10)  NULL,
    [Specification1] nvarchar(150)  NULL,
    [Specification2] nvarchar(150)  NULL,
    [Results1] nvarchar(150)  NULL,
    [Results2] nvarchar(150)  NULL,
    [Status] nchar(10)  NULL
);
GO

-- Creating table 'Vw_GlueCertificate'
CREATE TABLE [dbo].[Vw_GlueCertificate] (
    [Pk_IdDet] decimal(18,0)  NOT NULL,
    [PCode] nvarchar(150)  NULL,
    [MixingRatio] nvarchar(150)  NULL,
    [GelTemp] nchar(10)  NULL,
    [Specification1] nchar(10)  NULL,
    [Specification2] nchar(10)  NULL,
    [Results1] nchar(10)  NULL,
    [Results2] nchar(10)  NULL,
    [Name] nvarchar(250)  NULL,
    [VendorName] char(50)  NOT NULL,
    [Invno] nvarchar(50)  NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [InvDate] datetime  NULL,
    [Quantity] decimal(18,0)  NULL,
    [Tested] nvarchar(50)  NULL,
    [Approved] nvarchar(50)  NULL,
    [Fk_Vendor] decimal(18,0)  NULL,
    [Pk_Id] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'Vw_PStock'
CREATE TABLE [dbo].[Vw_PStock] (
    [TQty] decimal(38,0)  NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Name] nvarchar(250)  NULL,
    [JDate] datetime  NULL,
    [BoxName] nvarchar(500)  NOT NULL,
    [Assigned] decimal(18,0)  NULL,
    [Fk_Status] decimal(18,0)  NULL,
    [RM_Consumed] decimal(18,0)  NULL,
    [Pk_JobCardID] decimal(18,0)  NOT NULL,
    [CustomerName] char(100)  NOT NULL
);
GO

-- Creating table 'InkCertificateDetails'
CREATE TABLE [dbo].[InkCertificateDetails] (
    [Pk_IdDet] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_ID] decimal(18,0)  NULL,
    [Fk_Params] decimal(18,0)  NULL,
    [Spec_Target] nvarchar(150)  NULL,
    [Spec_Tolerance] nvarchar(150)  NULL,
    [Results] nvarchar(50)  NULL,
    [Status] nchar(10)  NULL
);
GO

-- Creating table 'Vw_AssStock'
CREATE TABLE [dbo].[Vw_AssStock] (
    [Expr1] decimal(38,3)  NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Pk_BoxID] decimal(18,0)  NOT NULL,
    [MName] nvarchar(250)  NULL,
    [Pk_JobCardID] decimal(18,0)  NOT NULL,
    [JDate] datetime  NULL,
    [Fk_Status] decimal(18,0)  NULL,
    [bName] nvarchar(500)  NOT NULL,
    [RollNo] varchar(50)  NULL,
    [Pk_JobCardDet] decimal(18,0)  NOT NULL,
    [RM_Consumed] decimal(18,0)  NULL
);
GO

-- Creating table 'Vw_TotInwdQty'
CREATE TABLE [dbo].[Vw_TotInwdQty] (
    [Qty] decimal(38,0)  NULL,
    [Pk_Inward] decimal(18,0)  NOT NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Name] nvarchar(250)  NULL,
    [Inward_Date] datetime  NULL
);
GO

-- Creating table 'Vw_OrdQty'
CREATE TABLE [dbo].[Vw_OrdQty] (
    [Name] nvarchar(500)  NOT NULL,
    [BType] nvarchar(50)  NOT NULL,
    [Pk_EnquiryChild] decimal(18,0)  NOT NULL,
    [Fk_Enquiry] decimal(18,0)  NULL,
    [Fk_BoxID] decimal(18,0)  NULL,
    [Quantity] decimal(18,0)  NULL,
    [PName] nvarchar(50)  NULL,
    [PartQty] int  NULL,
    [Pk_PartPropertyID] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'Vw_PartsSchedules'
CREATE TABLE [dbo].[Vw_PartsSchedules] (
    [DeliveryDate] datetime  NOT NULL,
    [SchQty] decimal(18,0)  NULL,
    [Pk_BoxID] decimal(18,0)  NOT NULL,
    [Name] nvarchar(500)  NOT NULL,
    [OrdQty] decimal(18,0)  NULL,
    [Pk_Order] decimal(18,0)  NOT NULL,
    [OrderDate] datetime  NOT NULL,
    [CustomerName] char(100)  NOT NULL,
    [Pk_DeliverySechedule] decimal(18,0)  NOT NULL,
    [Cust_PO] nvarchar(50)  NULL,
    [Fk_Enquiry] decimal(18,0)  NULL,
    [PName] nvarchar(50)  NULL,
    [Pk_PartPropertyID] decimal(18,0)  NOT NULL,
    [Fk_BoxID] decimal(18,0)  NULL
);
GO

-- Creating table 'PaperCertificateDetails'
CREATE TABLE [dbo].[PaperCertificateDetails] (
    [Pk_IdDet] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_PkID] decimal(18,0)  NULL,
    [Fk_Characteristics] decimal(18,0)  NULL,
    [Target] nvarchar(150)  NULL,
    [Acceptance] nvarchar(150)  NULL,
    [Result1] nchar(10)  NULL,
    [Result2] nchar(10)  NULL,
    [Result3] nchar(10)  NULL,
    [Result4] nchar(10)  NULL,
    [Result5] nchar(10)  NULL,
    [Result6] nchar(10)  NULL,
    [MinVal] nchar(10)  NULL,
    [MaxVal] nchar(10)  NULL,
    [AvgVal] nchar(10)  NULL,
    [Remarks] nchar(150)  NULL,
    [TQty] decimal(18,0)  NULL,
    [ReelNo] nchar(10)  NULL,
    [Status] nchar(10)  NULL
);
GO

-- Creating table 'JobCardPartsDetails'
CREATE TABLE [dbo].[JobCardPartsDetails] (
    [Pk_JobCardPartsDet] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_JobCardPartsID] decimal(18,0)  NULL,
    [Fk_PaperStock] decimal(18,0)  NULL,
    [QtyConsumed] decimal(18,0)  NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [RM_Consumed] decimal(18,0)  NULL
);
GO

-- Creating table 'JobCardPartsMaster'
CREATE TABLE [dbo].[JobCardPartsMaster] (
    [Pk_JobCardPartsID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [JDate] datetime  NULL,
    [Fk_Order] decimal(18,0)  NULL,
    [Fk_Status] decimal(18,0)  NULL,
    [Fk_PartID] decimal(18,0)  NULL,
    [Printing] bit  NULL,
    [Calico] bit  NULL,
    [Others] varchar(500)  NULL,
    [Invno] varchar(50)  NULL,
    [Corrugation] varchar(50)  NULL,
    [TopPaperQty] decimal(18,0)  NULL,
    [TwoPlyQty] decimal(18,0)  NULL,
    [TwoPlyWt] decimal(18,0)  NULL,
    [PastingQty] decimal(18,0)  NULL,
    [PastingWstQty] decimal(18,0)  NULL,
    [RotaryQty] decimal(18,0)  NULL,
    [RotaryWstQty] decimal(18,0)  NULL,
    [PunchingQty] decimal(18,0)  NULL,
    [PunchingWstQty] decimal(18,0)  NULL,
    [SlotingQty] decimal(18,0)  NULL,
    [SlotingWstQty] decimal(18,0)  NULL,
    [PinningQty] decimal(18,0)  NULL,
    [PinningWstQty] decimal(18,0)  NULL,
    [FinishingQty] decimal(18,0)  NULL,
    [FinishingWstQty] decimal(18,0)  NULL,
    [TotalQty] decimal(18,0)  NULL,
    [TotalWstQty] decimal(18,0)  NULL,
    [Fk_Schedule] decimal(18,0)  NULL
);
GO

-- Creating table 'Vw_JCpartsRep'
CREATE TABLE [dbo].[Vw_JCpartsRep] (
    [Pk_BoxID] decimal(18,0)  NOT NULL,
    [BName] nvarchar(500)  NOT NULL,
    [Description] nvarchar(500)  NULL,
    [Pk_Order] decimal(18,0)  NOT NULL,
    [OrderDate] datetime  NOT NULL,
    [Fk_Enquiry] decimal(18,0)  NULL,
    [Fk_Customer] decimal(18,0)  NULL,
    [Fk_Branch] decimal(18,0)  NULL,
    [Product] nvarchar(500)  NULL,
    [ShippingAddress] nvarchar(max)  NULL,
    [ShippingInstruction] nvarchar(max)  NULL,
    [SpecialInstructions] nvarchar(max)  NULL,
    [Quantity] int  NULL,
    [Price] decimal(18,2)  NULL,
    [Fk_Tanent] decimal(18,0)  NULL,
    [Fk_ShippingId] decimal(18,0)  NULL,
    [Fk_BoxID] decimal(18,0)  NULL,
    [Cust_PO] nvarchar(50)  NULL,
    [CustomerName] char(100)  NOT NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [CuttingSize] decimal(18,2)  NULL,
    [PartRate] decimal(18,2)  NULL,
    [PartTakeup] decimal(18,2)  NULL,
    [PName] nvarchar(50)  NULL,
    [Length] int  NULL,
    [Width] int  NULL,
    [Height] int  NULL,
    [TotalQty] int  NULL,
    [Deckle] decimal(18,2)  NULL,
    [BoardArea] decimal(18,2)  NULL,
    [BoardGSM] decimal(18,2)  NULL,
    [BoardBS] decimal(18,2)  NULL,
    [PlyVal] decimal(18,0)  NULL,
    [FluteHt] decimal(18,2)  NULL,
    [FluteName] varchar(50)  NULL,
    [GSM] decimal(18,0)  NULL,
    [BF] decimal(18,0)  NULL,
    [Color] nvarchar(50)  NULL,
    [Rate] decimal(18,2)  NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [Tk_Factor] decimal(18,2)  NULL,
    [Weight] decimal(18,3)  NULL,
    [MillName] nvarchar(150)  NULL,
    [ColorName] nvarchar(30)  NULL,
    [Pk_LayerID] decimal(18,0)  NOT NULL,
    [SchQty] decimal(18,0)  NULL,
    [Fk_PartID] decimal(18,0)  NULL,
    [Fk_Order] decimal(18,0)  NULL,
    [JDate] datetime  NULL,
    [Pk_JobCardPartsID] decimal(18,0)  NOT NULL,
    [Printing] bit  NULL,
    [Calico] bit  NULL,
    [Others] varchar(500)  NULL,
    [DeliveryDate] datetime  NOT NULL,
    [Invno] varchar(50)  NULL
);
GO

-- Creating table 'OutsourcingChild'
CREATE TABLE [dbo].[OutsourcingChild] (
    [Pk_SrcDetID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_PkSrcID] decimal(18,0)  NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [IssueWeight] decimal(18,2)  NULL,
    [ReelNo] nvarchar(50)  NULL,
    [Length] decimal(18,0)  NULL,
    [Width] decimal(18,0)  NULL,
    [Height] decimal(18,0)  NULL,
    [Ply] decimal(18,0)  NULL,
    [AppNos] int  NULL,
    [LayerWt] decimal(18,2)  NULL
);
GO

-- Creating table 'PartJobs'
CREATE TABLE [dbo].[PartJobs] (
    [Pk_ID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [RecdDate] datetime  NULL,
    [Description] nvarchar(max)  NULL,
    [Fk_Status] decimal(18,0)  NULL,
    [Fk_Customer] decimal(18,0)  NULL,
    [DCNo] nvarchar(50)  NULL
);
GO

-- Creating table 'FinishedGoodsStock'
CREATE TABLE [dbo].[FinishedGoodsStock] (
    [Pk_StockId] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Stock] decimal(18,0)  NULL,
    [Fk_BoxID] decimal(18,0)  NULL
);
GO

-- Creating table 'Vw_JCPartDet'
CREATE TABLE [dbo].[Vw_JCPartDet] (
    [Pk_JobCardPartsID] decimal(18,0)  NOT NULL,
    [JDate] datetime  NULL,
    [Fk_Order] decimal(18,0)  NULL,
    [Fk_PartID] decimal(18,0)  NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Name] nvarchar(250)  NULL,
    [BoxName] nvarchar(500)  NOT NULL,
    [PName] nvarchar(50)  NULL,
    [Pk_PartPropertyID] decimal(18,0)  NOT NULL,
    [Pk_PaperStock] decimal(18,0)  NOT NULL,
    [RollNo] varchar(50)  NULL,
    [MillName] nvarchar(150)  NULL,
    [RM_Consumed] decimal(18,0)  NULL
);
GO

-- Creating table 'JobWorkRMStock'
CREATE TABLE [dbo].[JobWorkRMStock] (
    [Pk_StockID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [StockValue] decimal(18,0)  NULL,
    [RollNo] varchar(50)  NULL,
    [Fk_Customer] decimal(18,0)  NULL
);
GO

-- Creating table 'Vw_JobIssue'
CREATE TABLE [dbo].[Vw_JobIssue] (
    [Pk_JobCardDet] decimal(18,0)  NOT NULL,
    [RollNo] varchar(50)  NULL,
    [RM_Consumed] decimal(18,0)  NULL,
    [Name] nvarchar(250)  NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Fk_BoxID] decimal(18,0)  NULL,
    [Fk_Status] decimal(18,0)  NULL,
    [Pk_JobCardID] decimal(18,0)  NOT NULL,
    [Expr1] nvarchar(500)  NOT NULL,
    [JDate] datetime  NULL,
    [Expr2] decimal(18,0)  NULL,
    [TotalQty] decimal(18,0)  NOT NULL,
    [CustomerName] char(100)  NOT NULL,
    [Pk_PaperStock] decimal(18,0)  NULL,
    [SName] nvarchar(50)  NULL,
    [Pk_Mill] decimal(18,0)  NULL,
    [Quantity] decimal(18,2)  NULL,
    [ColorName] nvarchar(30)  NULL
);
GO

-- Creating table 'Vw_Quality'
CREATE TABLE [dbo].[Vw_Quality] (
    [Pk_QualityCheck] decimal(18,0)  NOT NULL,
    [CheckDate] datetime  NULL,
    [Fk_PONo] decimal(18,0)  NULL,
    [Name] nvarchar(250)  NULL,
    [POQty] decimal(18,0)  NULL,
    [AccQty] decimal(18,0)  NULL,
    [InvoiceNumber] nvarchar(50)  NULL,
    [ColorName] nvarchar(30)  NULL,
    [MillName] nvarchar(150)  NULL
);
GO

-- Creating table 'Vw_TotPaperStockList'
CREATE TABLE [dbo].[Vw_TotPaperStockList] (
    [TQty] decimal(38,0)  NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Name] nvarchar(250)  NULL,
    [Min_Value] decimal(18,0)  NULL,
    [MillName] nvarchar(150)  NULL,
    [ColorName] nvarchar(30)  NULL
);
GO

-- Creating table 'Vw_Est_Det'
CREATE TABLE [dbo].[Vw_Est_Det] (
    [Pk_BoxEstimation] decimal(18,0)  NOT NULL,
    [CustomerName] char(100)  NULL,
    [EstDATE] datetime  NOT NULL,
    [GrandTotal] decimal(18,0)  NULL,
    [Fk_Enquiry] decimal(18,0)  NULL,
    [Quantity] decimal(18,0)  NULL,
    [Pk_BoxID] decimal(18,0)  NULL,
    [Name] nvarchar(500)  NULL,
    [Length] int  NULL,
    [Width] int  NULL,
    [Height] int  NULL,
    [BoardBS] decimal(18,2)  NULL,
    [PName] nvarchar(50)  NULL
);
GO

-- Creating table 'Vw_Quotation'
CREATE TABLE [dbo].[Vw_Quotation] (
    [Name] nvarchar(500)  NOT NULL,
    [CustomerName] char(100)  NOT NULL,
    [Quantity] decimal(18,0)  NULL,
    [Pk_Quotation] decimal(18,0)  NOT NULL,
    [QuotationDate] datetime  NULL,
    [CustomerType] char(50)  NOT NULL,
    [CustomerAddress] nvarchar(500)  NOT NULL,
    [LandLine] decimal(18,0)  NOT NULL,
    [Email] nvarchar(50)  NOT NULL,
    [StateName] varchar(50)  NOT NULL,
    [CountryName] varchar(50)  NOT NULL,
    [BoxID] decimal(18,0)  NULL,
    [BName] nvarchar(50)  NULL,
    [Dimension] nvarchar(100)  NULL,
    [Fk_EstimationID] decimal(18,0)  NULL,
    [Fk_Enquiry] decimal(18,0)  NULL,
    [ContactPerson] char(50)  NOT NULL,
    [CostPerBox] decimal(18,2)  NULL,
    [BoardBS] decimal(18,2)  NULL,
    [Description] nvarchar(500)  NULL,
    [Terms] nvarchar(max)  NULL,
    [PName] nvarchar(50)  NULL
);
GO

-- Creating table 'Sample_JobCardDetails'
CREATE TABLE [dbo].[Sample_JobCardDetails] (
    [Pk_JobCardDet] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_JobCardID] decimal(18,0)  NULL,
    [Fk_PaperStock] decimal(18,0)  NULL,
    [QtyConsumed] decimal(18,0)  NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [RM_Consumed] decimal(18,0)  NULL
);
GO

-- Creating table 'Sample_JobCardMaster'
CREATE TABLE [dbo].[Sample_JobCardMaster] (
    [Pk_JobCardID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [JDate] datetime  NULL,
    [Fk_Order] decimal(18,0)  NULL,
    [Fk_Status] decimal(18,0)  NULL,
    [Fk_BoxID] decimal(18,0)  NULL,
    [Printing] nvarchar(50)  NULL,
    [Calico] bit  NULL,
    [Others] varchar(500)  NULL,
    [Invno] varchar(50)  NULL,
    [Corrugation] varchar(50)  NULL,
    [TopPaperQty] decimal(18,0)  NULL,
    [TwoPlyQty] decimal(18,0)  NULL,
    [TwoPlyWt] decimal(18,0)  NULL,
    [PastingQty] decimal(18,0)  NULL,
    [PastingWstQty] decimal(18,0)  NULL,
    [RotaryQty] decimal(18,0)  NULL,
    [RotaryWstQty] decimal(18,0)  NULL,
    [PunchingQty] decimal(18,0)  NULL,
    [PunchingWstQty] decimal(18,0)  NULL,
    [SlotingQty] decimal(18,0)  NULL,
    [SlotingWstQty] decimal(18,0)  NULL,
    [PinningQty] decimal(18,0)  NULL,
    [PinningWstQty] decimal(18,0)  NULL,
    [FinishingQty] decimal(18,0)  NULL,
    [FinishingWstQty] decimal(18,0)  NULL,
    [TotalQty] decimal(18,0)  NOT NULL,
    [TotalWstQty] decimal(18,0)  NULL,
    [Machine] decimal(18,0)  NULL,
    [StartTime] nvarchar(50)  NULL,
    [EndTime] nvarchar(50)  NULL,
    [ChkQuality] decimal(18,0)  NULL,
    [PColor] nvarchar(50)  NULL,
    [PDetails] nvarchar(150)  NULL,
    [MFG] nvarchar(50)  NULL,
    [DPRSc] nvarchar(50)  NULL,
    [ECCQty] decimal(18,0)  NULL,
    [EccWstQty] decimal(18,0)  NULL,
    [GummingQty] decimal(18,0)  NULL,
    [GummingWstQty] decimal(18,0)  NULL,
    [Fk_Schedule] decimal(18,0)  NULL,
    [Stitching] bit  NULL,
    [CutLength] decimal(18,2)  NULL,
    [UpsVal] decimal(18,2)  NULL,
    [Corr] nchar(10)  NULL,
    [TopP] nchar(10)  NULL,
    [Pasting] nchar(10)  NULL,
    [Rotary] nchar(10)  NULL,
    [PrintingP] nchar(10)  NULL,
    [Punching] nchar(10)  NULL,
    [Slotting] nchar(10)  NULL,
    [Finishing] nchar(10)  NULL,
    [Pinning] nchar(10)  NULL,
    [Gumming] nchar(10)  NULL,
    [Bundling] nchar(10)  NULL,
    [Bundles] decimal(18,0)  NULL,
    [CS_BS] nchar(10)  NULL
);
GO

-- Creating table 'SampleBoxSpecs'
CREATE TABLE [dbo].[SampleBoxSpecs] (
    [Pk_BoxSpecID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [OuterShellYes] bit  NULL,
    [CapYes] bit  NULL,
    [LenghtPartationYes] bit  NULL,
    [WidthPartationYes] bit  NULL,
    [PlateYes] bit  NULL,
    [OuterShell] int  NULL,
    [Cap] int  NULL,
    [LenghtPartation] int  NULL,
    [WidthPartation] int  NULL,
    [Plate] int  NULL,
    [TotalWeight] decimal(18,3)  NULL,
    [Fk_BoxID] decimal(18,0)  NULL,
    [TopYes] bit  NULL,
    [BottomYes] bit  NULL,
    [TopVal] int  NULL,
    [BottVal] int  NULL,
    [SleeveYes] bit  NULL,
    [Sleeve] int  NULL
);
GO

-- Creating table 'SampleItemPartProperty'
CREATE TABLE [dbo].[SampleItemPartProperty] (
    [Pk_PartPropertyID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_BoxSpecsID] decimal(18,0)  NOT NULL,
    [PName] nvarchar(50)  NULL,
    [Length] int  NULL,
    [Width] int  NULL,
    [Height] int  NULL,
    [Weight] decimal(18,3)  NULL,
    [Quantity] int  NULL,
    [NoBoards] decimal(18,2)  NULL,
    [BoardArea] decimal(18,2)  NULL,
    [Deckle] decimal(18,2)  NULL,
    [CuttingSize] decimal(18,2)  NULL,
    [Rate] decimal(18,2)  NULL,
    [AddBLength] decimal(18,2)  NULL,
    [TakeUpFactor] decimal(18,2)  NULL,
    [PlyVal] decimal(18,0)  NULL,
    [OLength1] int  NULL,
    [OWidth1] int  NULL,
    [OHeight1] int  NULL,
    [BoardGSM] decimal(18,2)  NULL,
    [BoardBS] decimal(18,2)  NULL
);
GO

-- Creating table 'SampleItems_Layers'
CREATE TABLE [dbo].[SampleItems_Layers] (
    [Pk_LayerID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_PartId] decimal(18,0)  NULL,
    [GSM] decimal(18,0)  NULL,
    [BF] decimal(18,0)  NULL,
    [Color] nvarchar(50)  NULL,
    [Rate] decimal(18,2)  NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [Weight] decimal(18,3)  NULL,
    [Tk_Factor] decimal(18,2)  NULL
);
GO

-- Creating table 'SampleBoxMaster'
CREATE TABLE [dbo].[SampleBoxMaster] (
    [Pk_BoxID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(500)  NOT NULL,
    [Customer] decimal(18,0)  NOT NULL,
    [Description] nvarchar(500)  NULL,
    [Fk_BoxType] decimal(18,0)  NULL,
    [PartNo] varchar(50)  NULL,
    [Fk_FluteType] decimal(18,0)  NULL,
    [AsPer] nvarchar(50)  NULL
);
GO

-- Creating table 'Vw_SampleBoxDet'
CREATE TABLE [dbo].[Vw_SampleBoxDet] (
    [Name] nvarchar(500)  NOT NULL,
    [Length] int  NULL,
    [Width] int  NULL,
    [Height] int  NULL,
    [Weight] decimal(18,3)  NULL,
    [GSM] decimal(18,0)  NULL,
    [BF] decimal(18,0)  NULL,
    [Pk_PartPropertyID] decimal(18,0)  NOT NULL,
    [Pk_LayerID] decimal(18,0)  NOT NULL,
    [Pk_BoxID] decimal(18,0)  NOT NULL,
    [CustomerName] char(100)  NOT NULL,
    [OuterShellYes] bit  NULL,
    [CapYes] bit  NULL,
    [LenghtPartationYes] bit  NULL,
    [WidthPartationYes] bit  NULL,
    [PlateYes] bit  NULL,
    [Pk_BoxSpecID] decimal(18,0)  NOT NULL,
    [MaterialName] nvarchar(250)  NULL,
    [BType] nvarchar(50)  NOT NULL,
    [BoardArea] decimal(18,2)  NULL,
    [Deckle] decimal(18,2)  NULL,
    [CuttingSize] decimal(18,2)  NULL,
    [PName] nvarchar(50)  NULL,
    [BoardBS] decimal(18,2)  NULL,
    [BoardGSM] decimal(18,2)  NULL,
    [TakeUpFactor] decimal(18,2)  NULL,
    [FluteName] varchar(50)  NULL,
    [FluteTKF] decimal(18,2)  NULL,
    [LayerTKF] decimal(18,2)  NULL,
    [ColorName] nvarchar(30)  NULL,
    [Quantity] int  NULL,
    [PlyVal] decimal(18,0)  NULL,
    [AsPer] nvarchar(50)  NULL
);
GO

-- Creating table 'BoxStock'
CREATE TABLE [dbo].[BoxStock] (
    [Pk_StockID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_BoxID] decimal(18,0)  NULL,
    [Quantity] decimal(18,0)  NULL,
    [Fk_PartID] decimal(18,0)  NULL
);
GO

-- Creating table 'DispatchChild'
CREATE TABLE [dbo].[DispatchChild] (
    [Pk_DispDet] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_DispMast] decimal(18,0)  NULL,
    [Fk_BoxID] decimal(18,0)  NULL,
    [Quantity] decimal(18,0)  NULL,
    [Weight] decimal(18,2)  NULL,
    [Bundle] nvarchar(50)  NULL,
    [NosInBundle] int  NULL,
    [NoOfRows] int  NULL,
    [NoOfColumns] int  NULL
);
GO

-- Creating table 'DispatchMast'
CREATE TABLE [dbo].[DispatchMast] (
    [Pk_Dispatch] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Dis_Date] datetime  NULL,
    [Fk_Customer] decimal(18,0)  NULL
);
GO

-- Creating table 'PaperCertificate'
CREATE TABLE [dbo].[PaperCertificate] (
    [Pk_Id] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [Invno] nvarchar(50)  NULL,
    [InvDate] datetime  NULL,
    [Quantity] decimal(18,0)  NULL,
    [Tested] nvarchar(50)  NULL,
    [Approved] nvarchar(50)  NULL,
    [Fk_Vendor] decimal(18,0)  NULL,
    [Fk_PoNo] decimal(18,0)  NULL
);
GO

-- Creating table 'Vw_BoxScheduleDet'
CREATE TABLE [dbo].[Vw_BoxScheduleDet] (
    [Pk_DeliverySechedule] decimal(18,0)  NOT NULL,
    [Name] nvarchar(500)  NOT NULL,
    [PName] nvarchar(50)  NULL,
    [Deckle] decimal(18,2)  NULL,
    [CuttingSize] decimal(18,2)  NULL,
    [GSM] decimal(18,0)  NULL,
    [BF] decimal(18,0)  NULL,
    [Weight] decimal(18,3)  NULL,
    [Pk_PartPropertyID] decimal(18,0)  NOT NULL,
    [PlyVal] decimal(18,0)  NULL,
    [Pk_LayerID] decimal(18,0)  NOT NULL,
    [Length] int  NULL,
    [Width] int  NULL,
    [Height] int  NULL
);
GO

-- Creating table 'Vw_PendingPO'
CREATE TABLE [dbo].[Vw_PendingPO] (
    [Pk_PONo] decimal(18,0)  NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [Pk_MaterialOrderMasterId] decimal(18,0)  NULL,
    [Pk_MaterialOrderDetailsId] decimal(18,0)  NOT NULL,
    [Quantity] decimal(18,0)  NULL
);
GO

-- Creating table 'PurchaseOrderD'
CREATE TABLE [dbo].[PurchaseOrderD] (
    [Pk_PODet] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_PONo] decimal(18,0)  NOT NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [Quantity] decimal(18,0)  NULL,
    [Rate] decimal(18,2)  NULL,
    [Amount] decimal(18,2)  NULL,
    [InwdQty] decimal(18,0)  NULL,
    [Fk_IndentVal] decimal(18,0)  NULL,
    [HSNCode] nvarchar(50)  NULL
);
GO

-- Creating table 'Vw_IndentCertificate'
CREATE TABLE [dbo].[Vw_IndentCertificate] (
    [Pk_MaterialOrderMasterId] decimal(18,0)  NOT NULL,
    [MaterialIndentDate] datetime  NULL,
    [RequiredDate] datetime  NULL,
    [Quantity] decimal(18,3)  NULL,
    [Name] nvarchar(250)  NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [Pk_MaterialOrderDetailsId] decimal(18,0)  NOT NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [MillName] nvarchar(150)  NULL,
    [Fk_CustomerOrder] decimal(18,0)  NULL,
    [CustomerName] char(100)  NULL,
    [ColorName] nvarchar(30)  NULL,
    [Cust_PO] nvarchar(50)  NULL,
    [Pk_Mill] decimal(18,0)  NULL
);
GO

-- Creating table 'ProcessMaster'
CREATE TABLE [dbo].[ProcessMaster] (
    [Pk_Process] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [ProcessName] nvarchar(150)  NULL
);
GO

-- Creating table 'JProcess'
CREATE TABLE [dbo].[JProcess] (
    [Pk_JProcessID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_ProcessID] decimal(18,0)  NULL,
    [Fk_JobCardID] decimal(18,0)  NULL
);
GO

-- Creating table 'Vw_ProcessJC'
CREATE TABLE [dbo].[Vw_ProcessJC] (
    [Pk_Process] decimal(18,0)  NOT NULL,
    [ProcessName] nvarchar(150)  NULL,
    [Fk_JobCardID] decimal(18,0)  NULL,
    [Pk_JProcessID] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'JC_Documents'
CREATE TABLE [dbo].[JC_Documents] (
    [Pk_Documents] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_JCNo] decimal(18,0)  NULL,
    [FileName] nvarchar(255)  NULL,
    [FileSize] nvarchar(50)  NULL
);
GO

-- Creating table 'vw_JProcessList'
CREATE TABLE [dbo].[vw_JProcessList] (
    [ProcessName] nvarchar(150)  NULL,
    [Pk_JobCardID] decimal(18,0)  NULL,
    [Pk_Process] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'Vw_Schedules'
CREATE TABLE [dbo].[Vw_Schedules] (
    [DeliveryDate] datetime  NOT NULL,
    [SchQty] decimal(18,0)  NULL,
    [Pk_BoxID] decimal(18,0)  NOT NULL,
    [Name] nvarchar(500)  NOT NULL,
    [OrdQty] decimal(18,0)  NULL,
    [Pk_Order] decimal(18,0)  NOT NULL,
    [OrderDate] datetime  NOT NULL,
    [CustomerName] char(100)  NOT NULL,
    [Pk_DeliverySechedule] decimal(18,0)  NOT NULL,
    [Cust_PO] nvarchar(50)  NULL,
    [Fk_Enquiry] decimal(18,0)  NULL,
    [PName] nvarchar(50)  NULL,
    [Pk_PartPropertyID] decimal(18,0)  NOT NULL,
    [Fk_BoxID] decimal(18,0)  NULL
);
GO

-- Creating table 'Vw_DeliveryScheduleReport'
CREATE TABLE [dbo].[Vw_DeliveryScheduleReport] (
    [Pk_DeliverySechedule] decimal(18,0)  NOT NULL,
    [DeliveryDate] datetime  NOT NULL,
    [Pk_Order] decimal(18,0)  NOT NULL,
    [OrderDate] datetime  NOT NULL,
    [Name] nvarchar(500)  NOT NULL,
    [CustomerName] char(100)  NOT NULL,
    [Quantity] decimal(18,0)  NULL,
    [OrdQty] decimal(18,0)  NULL,
    [PartNo] varchar(50)  NULL,
    [Expr1] nvarchar(50)  NOT NULL,
    [Fk_BoxID] decimal(18,0)  NULL,
    [Fk_PartID] decimal(18,0)  NULL,
    [PName] nvarchar(50)  NULL
);
GO

-- Creating table 'COA_Parameters'
CREATE TABLE [dbo].[COA_Parameters] (
    [Pk_ParaID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [ParaName] nvarchar(50)  NULL
);
GO

-- Creating table 'COA'
CREATE TABLE [dbo].[COA] (
    [Pk_ID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [RDate] datetime  NULL,
    [Fk_Invno] decimal(18,0)  NULL
);
GO

-- Creating table 'Vw_PartPaperLoad'
CREATE TABLE [dbo].[Vw_PartPaperLoad] (
    [RollNo] varchar(50)  NULL,
    [Pk_BoxID] decimal(18,0)  NOT NULL,
    [BName] nvarchar(500)  NOT NULL,
    [PName] nvarchar(50)  NULL,
    [Pk_PartPropertyID] decimal(18,0)  NOT NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Quantity] decimal(18,0)  NULL,
    [Pk_PaperStock] decimal(18,0)  NOT NULL,
    [MaterialName] nvarchar(250)  NULL,
    [MillName] nvarchar(150)  NULL,
    [ColorName] nvarchar(30)  NULL,
    [GSM] decimal(20,0)  NULL,
    [BF] decimal(20,0)  NULL,
    [Deckle] decimal(20,2)  NULL
);
GO

-- Creating table 'COADetails'
CREATE TABLE [dbo].[COADetails] (
    [Pk_IDDet] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_MastID] decimal(18,0)  NULL,
    [Fk_ParaID] decimal(18,0)  NULL,
    [UOM] nvarchar(50)  NULL,
    [Specification] nvarchar(150)  NULL,
    [Result] nvarchar(150)  NULL,
    [Remarks] nvarchar(150)  NULL
);
GO

-- Creating table 'Vw_COARep'
CREATE TABLE [dbo].[Vw_COARep] (
    [Pk_ID] decimal(18,0)  NOT NULL,
    [RDate] datetime  NULL,
    [Fk_Invno] decimal(18,0)  NULL,
    [Name] nvarchar(500)  NULL,
    [Length] int  NULL,
    [Width] int  NULL,
    [Height] int  NULL,
    [PlyVal] decimal(18,0)  NULL,
    [Pk_BoxID] decimal(18,0)  NULL,
    [ParaName] nvarchar(50)  NULL,
    [CustomerName] char(100)  NULL,
    [Quantity] decimal(18,0)  NULL,
    [Fk_ParaID] decimal(18,0)  NULL,
    [UOM] nvarchar(50)  NULL,
    [Specification] nvarchar(150)  NULL,
    [Result] nvarchar(150)  NULL,
    [Remarks] nvarchar(150)  NULL,
    [Pk_IDDet] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'Vw_PaperReqDetIndent'
CREATE TABLE [dbo].[Vw_PaperReqDetIndent] (
    [PaperWeight] decimal(38,3)  NULL,
    [PaperReq] decimal(38,3)  NULL,
    [Name] nvarchar(250)  NULL,
    [Pk_BoxID] decimal(18,0)  NOT NULL,
    [Fk_OrderID] decimal(18,0)  NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [OrdQty] decimal(18,0)  NULL,
    [BName] nvarchar(500)  NOT NULL,
    [ColorName] nvarchar(30)  NULL,
    [MillName] nvarchar(150)  NULL,
    [Pk_Order] decimal(18,0)  NOT NULL,
    [OrderDate] datetime  NOT NULL,
    [CustomerName] char(100)  NOT NULL,
    [Cust_PO] nvarchar(50)  NULL,
    [Ddate] datetime  NULL,
    [Pk_Mill] decimal(18,0)  NOT NULL,
    [Deckle] decimal(20,2)  NULL,
    [GSM] decimal(20,0)  NULL,
    [BF] decimal(20,0)  NULL
);
GO

-- Creating table 'Vw_PaperStock'
CREATE TABLE [dbo].[Vw_PaperStock] (
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Name] nvarchar(250)  NULL,
    [ColorName] nvarchar(30)  NULL,
    [Pk_Mill] decimal(18,0)  NOT NULL,
    [MillName] nvarchar(150)  NULL,
    [GSM] decimal(20,0)  NULL,
    [Deckle] decimal(20,2)  NULL,
    [BF] decimal(20,0)  NULL
);
GO

-- Creating table 'Vw_InvOrdQty'
CREATE TABLE [dbo].[Vw_InvOrdQty] (
    [Fk_OrderNo] decimal(18,0)  NOT NULL,
    [Fk_PartID] decimal(18,0)  NULL,
    [Pk_Invoice] decimal(18,0)  NOT NULL,
    [Quantity] decimal(18,0)  NULL,
    [Fk_BoxID] decimal(18,0)  NULL
);
GO

-- Creating table 'Vw_FinishingQty_Wt'
CREATE TABLE [dbo].[Vw_FinishingQty_Wt] (
    [Pk_ID] decimal(18,0)  NOT NULL,
    [Fk_ProcessID] decimal(18,0)  NULL,
    [WastageQty] decimal(18,2)  NULL,
    [Quantity] decimal(18,0)  NULL,
    [ProcessDate] datetime  NULL,
    [PQuantity] decimal(18,0)  NULL,
    [PStartTime] datetime  NULL,
    [PEndTime] datetime  NULL,
    [ReasonWastage] nvarchar(250)  NULL,
    [RemainingQty] decimal(18,0)  NULL,
    [WQty] decimal(18,2)  NULL,
    [Pk_JobCardID] decimal(18,0)  NOT NULL,
    [ProcessName] nvarchar(150)  NULL,
    [Weight] decimal(18,3)  NULL,
    [PName] nvarchar(50)  NULL,
    [PrdnWt] decimal(37,3)  NULL
);
GO

-- Creating table 'TonnValues'
CREATE TABLE [dbo].[TonnValues] (
    [Pk_ID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [ExStock] decimal(18,2)  NULL,
    [IssStock] decimal(18,2)  NULL,
    [IssRetStock] decimal(18,2)  NULL,
    [WastageVal] decimal(18,2)  NULL,
    [ProductionVal] decimal(18,2)  NULL
);
GO

-- Creating table 'Vw_PoBom'
CREATE TABLE [dbo].[Vw_PoBom] (
    [Name] nvarchar(250)  NULL,
    [Pk_PONo] decimal(18,0)  NOT NULL,
    [PODate] datetime  NULL,
    [VendorName] char(50)  NOT NULL,
    [Fk_Status] decimal(18,0)  NULL,
    [POQty] decimal(18,0)  NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [AccQty] decimal(18,0)  NULL,
    [PendQty] decimal(19,0)  NULL,
    [Fk_Indent] decimal(18,0)  NULL,
    [Inward_Date] datetime  NULL,
    [Pk_InwardDet] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'Vw_GetPaperWtSum'
CREATE TABLE [dbo].[Vw_GetPaperWtSum] (
    [SWt] decimal(38,3)  NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Pk_BoxID] decimal(18,0)  NOT NULL,
    [Name] nvarchar(250)  NULL,
    [MillName] nvarchar(150)  NULL
);
GO

-- Creating table 'Semi_FinishedGoodsStock'
CREATE TABLE [dbo].[Semi_FinishedGoodsStock] (
    [Pk_StkID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_MatCatID] decimal(18,0)  NULL,
    [Stock] decimal(18,0)  NULL,
    [Fk_JobCardID] decimal(18,0)  NULL,
    [Description] nvarchar(150)  NULL
);
GO

-- Creating table 'Vw_PaperSummary'
CREATE TABLE [dbo].[Vw_PaperSummary] (
    [SWt] decimal(38,3)  NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Pk_BoxID] decimal(18,0)  NOT NULL,
    [Name] nvarchar(250)  NULL,
    [MillName] nvarchar(150)  NULL,
    [Fk_Status] decimal(18,0)  NULL,
    [Quantity] decimal(18,0)  NULL,
    [Pk_PONo] decimal(18,0)  NOT NULL,
    [PODate] datetime  NULL,
    [InwdQty] decimal(18,0)  NULL
);
GO

-- Creating table 'Vw_PO'
CREATE TABLE [dbo].[Vw_PO] (
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Name] nvarchar(250)  NULL,
    [Quantity] decimal(18,0)  NULL,
    [Pk_PONo] decimal(18,0)  NOT NULL,
    [Rate] decimal(18,0)  NULL,
    [Amount] decimal(18,2)  NULL,
    [Fk_Status] decimal(18,0)  NULL,
    [InwdQty] decimal(18,0)  NULL,
    [VendorName] char(50)  NOT NULL,
    [GSM] decimal(20,0)  NULL,
    [BF] decimal(20,0)  NULL,
    [Pk_Vendor] decimal(18,0)  NOT NULL,
    [PODate] datetime  NULL,
    [ColorName] nvarchar(30)  NULL,
    [MillName] nvarchar(150)  NULL,
    [Fk_IndentVal] decimal(18,0)  NULL,
    [Pk_Mill] decimal(18,0)  NOT NULL,
    [Deckle] decimal(20,2)  NULL
);
GO

-- Creating table 'Vw_MIndent'
CREATE TABLE [dbo].[Vw_MIndent] (
    [Pk_MaterialOrderMasterId] decimal(18,0)  NOT NULL,
    [MaterialIndentDate] datetime  NULL,
    [RequiredDate] datetime  NULL,
    [Quantity] decimal(18,3)  NULL,
    [Name] nvarchar(250)  NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [Pk_MaterialOrderDetailsId] decimal(18,0)  NOT NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [MillName] nvarchar(150)  NULL,
    [Fk_CustomerOrder] decimal(18,0)  NULL,
    [CustomerName] char(100)  NULL,
    [ColorName] nvarchar(30)  NULL,
    [Cust_PO] nvarchar(50)  NULL,
    [Pk_Mill] decimal(18,0)  NULL,
    [GSM] decimal(20,0)  NULL,
    [Deckle] decimal(20,2)  NULL,
    [BF] decimal(20,0)  NULL
);
GO

-- Creating table 'Vw_WIP_Stock'
CREATE TABLE [dbo].[Vw_WIP_Stock] (
    [Pk_StkID] decimal(18,0)  NOT NULL,
    [Stock] decimal(18,0)  NULL,
    [Name] nvarchar(500)  NOT NULL,
    [Description] nvarchar(150)  NULL,
    [BoxPartName] nvarchar(50)  NULL,
    [Pk_JobCardID] decimal(18,0)  NOT NULL,
    [CatName] nvarchar(30)  NULL
);
GO

-- Creating table 'Vw_InwardRep'
CREATE TABLE [dbo].[Vw_InwardRep] (
    [Pk_Inward] decimal(18,0)  NOT NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Name] nvarchar(250)  NULL,
    [Quantity] decimal(18,0)  NULL,
    [Inward_Date] datetime  NULL,
    [Fk_QC] decimal(18,0)  NULL,
    [Fk_Indent] decimal(18,0)  NULL,
    [PONo] decimal(18,0)  NULL,
    [RollNo] nvarchar(50)  NULL,
    [Pk_InwardDet] decimal(18,0)  NOT NULL,
    [ColorName] nvarchar(30)  NULL,
    [MillName] nvarchar(150)  NULL,
    [Pk_Mill] decimal(18,0)  NULL
);
GO

-- Creating table 'Vw_JcIssues'
CREATE TABLE [dbo].[Vw_JcIssues] (
    [Name] nvarchar(250)  NULL,
    [Pk_Material] decimal(18,0)  NULL,
    [Fk_BoxID] decimal(18,0)  NULL,
    [Fk_Status] decimal(18,0)  NULL,
    [Pk_JobCardID] decimal(18,0)  NOT NULL,
    [Expr1] nvarchar(500)  NOT NULL,
    [JDate] datetime  NULL,
    [TotalQty] decimal(18,0)  NOT NULL,
    [CustomerName] char(100)  NOT NULL,
    [SName] nvarchar(50)  NULL,
    [Pk_Mill] decimal(18,0)  NULL,
    [Pk_MaterialIssueID] decimal(18,0)  NULL,
    [IssueDate] datetime  NULL,
    [IssueQty] decimal(18,0)  NULL,
    [ColorName] nvarchar(30)  NULL,
    [MillName] nvarchar(150)  NULL,
    [RollNo] nvarchar(50)  NULL,
    [Pk_MaterialIssueDetailsID] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'VwOrderRep'
CREATE TABLE [dbo].[VwOrderRep] (
    [Pk_Order] decimal(18,0)  NOT NULL,
    [OrderDate] datetime  NOT NULL,
    [CustomerName] char(100)  NOT NULL,
    [Name] nvarchar(500)  NOT NULL,
    [Ddate] datetime  NULL,
    [OrdQty] decimal(18,0)  NULL,
    [EnqQty] decimal(18,0)  NULL,
    [Pk_BoxID] decimal(18,0)  NOT NULL,
    [PName] nvarchar(50)  NULL,
    [Cust_PO] nvarchar(50)  NULL,
    [Pk_OrderChild] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'Vw_PaperWtBox'
CREATE TABLE [dbo].[Vw_PaperWtBox] (
    [Expr1] decimal(38,3)  NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Pk_BoxID] decimal(18,0)  NOT NULL,
    [Name] nvarchar(250)  NULL,
    [stkqty] decimal(38,0)  NULL,
    [PName] nvarchar(50)  NULL,
    [Quantity] int  NULL,
    [Pk_PartPropertyID] decimal(18,0)  NOT NULL,
    [GSM] decimal(18,0)  NULL,
    [MillName] nvarchar(150)  NULL,
    [ColorName] nvarchar(30)  NULL,
    [PlyVal] decimal(18,0)  NULL
);
GO

-- Creating table 'Vw_JobWorkStock'
CREATE TABLE [dbo].[Vw_JobWorkStock] (
    [CustomerName] char(100)  NOT NULL,
    [StockValue] decimal(18,0)  NULL,
    [RollNo] varchar(50)  NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [MillName] nvarchar(150)  NULL,
    [ColorName] nvarchar(30)  NULL,
    [Name] nvarchar(250)  NULL,
    [GSM] decimal(20,0)  NULL,
    [BF] decimal(20,0)  NULL,
    [Deckle] decimal(20,2)  NULL
);
GO

-- Creating table 'Vw_NetPapReqd'
CREATE TABLE [dbo].[Vw_NetPapReqd] (
    [PaperReq] decimal(38,3)  NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Name] nvarchar(250)  NULL,
    [MillName] nvarchar(150)  NULL,
    [ColorName] nvarchar(30)  NULL,
    [stkqty] decimal(38,0)  NULL,
    [DeliveryDate] datetime  NOT NULL,
    [CustomerName] char(100)  NOT NULL,
    [Box_Name] nvarchar(500)  NOT NULL,
    [PName] nvarchar(50)  NULL,
    [Length] int  NULL,
    [Width] int  NULL,
    [Height] int  NULL,
    [Quantity] decimal(18,0)  NULL,
    [LayerWt] decimal(38,3)  NULL
);
GO

-- Creating table 'Vw_JCIssReport'
CREATE TABLE [dbo].[Vw_JCIssReport] (
    [ColorName] nvarchar(30)  NULL,
    [MillName] nvarchar(150)  NULL,
    [IssQty] decimal(18,0)  NULL,
    [RollNo] nvarchar(50)  NULL,
    [Pk_MaterialIssueDetailsID] decimal(18,0)  NULL,
    [Pk_MaterialIssueID] decimal(18,0)  NULL,
    [IssueDate] datetime  NULL,
    [Pk_JobCardID] decimal(18,0)  NOT NULL,
    [GSM] decimal(20,0)  NULL,
    [BF] decimal(20,0)  NULL,
    [Name] nvarchar(250)  NULL,
    [Deckle] decimal(20,2)  NULL,
    [Weight] decimal(18,0)  NULL,
    [Pk_Order] decimal(18,0)  NULL,
    [BName] nvarchar(500)  NOT NULL,
    [TotalQty] decimal(18,0)  NOT NULL,
    [Fk_BoxID] decimal(18,0)  NULL,
    [Fk_Schedule] decimal(18,0)  NULL,
    [Pk_Material] decimal(18,0)  NULL
);
GO

-- Creating table 'Vw_JobBoxDetails'
CREATE TABLE [dbo].[Vw_JobBoxDetails] (
    [ColorName] nvarchar(30)  NULL,
    [MillName] nvarchar(150)  NULL,
    [IssQty] decimal(18,0)  NULL,
    [RollNo] nvarchar(50)  NULL,
    [Pk_MaterialIssueDetailsID] decimal(18,0)  NOT NULL,
    [Pk_MaterialIssueID] decimal(18,0)  NULL,
    [IssueDate] datetime  NULL,
    [Pk_JobCardID] decimal(18,0)  NOT NULL,
    [GSM] decimal(20,0)  NULL,
    [BF] decimal(20,0)  NULL,
    [Name] nvarchar(250)  NULL,
    [Deckle] decimal(20,2)  NULL,
    [BName] nvarchar(500)  NOT NULL,
    [TotalQty] decimal(18,0)  NOT NULL,
    [LayerWt] decimal(18,3)  NULL,
    [Quantity] int  NULL,
    [Pk_Material] decimal(18,0)  NULL,
    [ReturnQty] decimal(18,0)  NOT NULL,
    [Weight] decimal(18,3)  NULL
);
GO

-- Creating table 'Vw_AssignedStock'
CREATE TABLE [dbo].[Vw_AssignedStock] (
    [Pk_PaperStock] decimal(18,0)  NOT NULL,
    [RollNo] varchar(50)  NULL,
    [Name] nvarchar(250)  NULL,
    [Pk_JobCardID] decimal(18,0)  NOT NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Quantity] decimal(18,0)  NULL,
    [MillName] nvarchar(150)  NULL,
    [ColorName] nvarchar(30)  NULL,
    [Pk_Mill] decimal(18,0)  NOT NULL,
    [Fk_Status] decimal(18,0)  NULL,
    [CustomerName] char(100)  NOT NULL,
    [BoxName] nvarchar(500)  NOT NULL
);
GO

-- Creating table 'vw_PaperRollStock'
CREATE TABLE [dbo].[vw_PaperRollStock] (
    [RollNo] varchar(50)  NULL,
    [Quantity] decimal(18,0)  NULL,
    [Name] nvarchar(250)  NULL,
    [Pk_Material] decimal(18,0)  NULL,
    [Pk_PaperStock] decimal(18,0)  NOT NULL,
    [GSM] decimal(20,0)  NULL,
    [BF] decimal(20,0)  NULL,
    [ColorName] nvarchar(30)  NULL,
    [Min_Value] decimal(18,0)  NULL,
    [Max_Value] decimal(18,0)  NULL,
    [SName] nvarchar(50)  NULL,
    [Deckle] decimal(20,2)  NULL,
    [MillName] nvarchar(150)  NULL,
    [Fk_JobCardID] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'Vw_BalanceOrdQty'
CREATE TABLE [dbo].[Vw_BalanceOrdQty] (
    [DelvQty] decimal(38,0)  NULL,
    [Pk_Order] decimal(18,0)  NOT NULL,
    [OrderDate] datetime  NOT NULL,
    [CustomerName] char(100)  NOT NULL,
    [OrdQty] decimal(18,0)  NULL,
    [PartName] nvarchar(50)  NULL,
    [BoxName] nvarchar(500)  NOT NULL,
    [Ex_Stock] decimal(18,0)  NOT NULL,
    [Pk_Invoice] decimal(18,0)  NOT NULL,
    [InvDate] datetime  NULL,
    [Quantity] decimal(18,0)  NULL,
    [Fk_BoxID] decimal(18,0)  NULL,
    [Fk_PartID] decimal(18,0)  NULL
);
GO

-- Creating table 'PartJobsReturns'
CREATE TABLE [dbo].[PartJobsReturns] (
    [Pk_ID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Ret_Date] datetime  NULL,
    [TypeofPrd] nchar(10)  NULL,
    [Fk_PJID] decimal(18,0)  NULL,
    [RetPrdWeight] decimal(18,0)  NULL,
    [Fk_Status] decimal(18,0)  NULL,
    [PrdName] decimal(18,0)  NULL,
    [RecdJobStatus] nchar(10)  NULL,
    [WtDiff] decimal(18,2)  NULL,
    [DCNo] nvarchar(50)  NULL,
    [ProductName] nvarchar(50)  NULL
);
GO

-- Creating table 'Vw_JWInDelv'
CREATE TABLE [dbo].[Vw_JWInDelv] (
    [Ret_Date] datetime  NULL,
    [ProductName] nvarchar(50)  NULL,
    [DCNo] nvarchar(50)  NULL,
    [RetPrdWeight] decimal(18,0)  NULL,
    [Pk_ID] decimal(18,0)  NOT NULL,
    [CustomerName] char(100)  NOT NULL,
    [Description] nvarchar(max)  NULL,
    [Name] nvarchar(30)  NULL,
    [DelvID] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'JW_JobCardMaster'
CREATE TABLE [dbo].[JW_JobCardMaster] (
    [Pk_JobCardID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [JDate] datetime  NULL,
    [Fk_Order] decimal(18,0)  NULL,
    [Fk_Status] decimal(18,0)  NULL,
    [Fk_BoxID] decimal(18,0)  NULL,
    [Printing] nvarchar(50)  NULL,
    [Calico] bit  NULL,
    [Others] varchar(500)  NULL,
    [Invno] varchar(50)  NULL,
    [TotalQty] decimal(18,0)  NOT NULL,
    [Machine] decimal(18,0)  NULL,
    [PColor] nvarchar(50)  NULL,
    [PDetails] nvarchar(150)  NULL,
    [MFG] nvarchar(50)  NULL,
    [DPRSc] nvarchar(50)  NULL,
    [Stitching] bit  NULL,
    [Fk_Vendor] decimal(18,0)  NULL
);
GO

-- Creating table 'JWStocks'
CREATE TABLE [dbo].[JWStocks] (
    [Pk_JWStock] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [Quantity] decimal(18,2)  NULL,
    [SafeValue] decimal(18,2)  NULL,
    [Fk_Tanent] decimal(18,0)  NOT NULL,
    [Fk_BranchID] decimal(18,0)  NULL
);
GO

-- Creating table 'Outsourcing'
CREATE TABLE [dbo].[Outsourcing] (
    [Pk_SrcID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Vendor] decimal(18,0)  NULL,
    [Description] nvarchar(max)  NULL,
    [IssueDate] datetime  NULL,
    [Fk_Status] decimal(18,0)  NULL,
    [Fk_JobCardID] decimal(18,0)  NULL
);
GO

-- Creating table 'JobWorkReceivables'
CREATE TABLE [dbo].[JobWorkReceivables] (
    [Pk_ID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [RecdDate] datetime  NULL,
    [TypeofPrd] nchar(10)  NULL,
    [Fk_JobCardID] decimal(18,0)  NULL,
    [RecdPrdWeight] decimal(18,0)  NULL,
    [Fk_Status] decimal(18,0)  NULL,
    [PrdName] decimal(18,0)  NULL,
    [OutSourcedTaskStatus] nchar(10)  NULL,
    [WtDiff] decimal(18,2)  NULL,
    [DcNo] nchar(10)  NULL,
    [Name] nchar(50)  NULL
);
GO

-- Creating table 'Vw_PartJobs'
CREATE TABLE [dbo].[Vw_PartJobs] (
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Name] nvarchar(250)  NULL,
    [GSM] decimal(20,0)  NULL,
    [BF] decimal(20,0)  NULL,
    [Deckle] decimal(20,2)  NULL,
    [Length] decimal(18,0)  NULL,
    [Width] decimal(18,0)  NULL,
    [Height] decimal(18,0)  NULL,
    [Ply] decimal(18,0)  NULL,
    [AppNos] int  NULL,
    [LayerWt] decimal(18,2)  NULL,
    [Pk_ID] decimal(18,0)  NOT NULL,
    [RecdDate] datetime  NULL,
    [Description] nvarchar(max)  NULL,
    [CustomerName] char(100)  NOT NULL,
    [Quantity] decimal(18,0)  NULL,
    [RollNo] nvarchar(50)  NULL,
    [PkJobDetID] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'JW_RM_RetDet'
CREATE TABLE [dbo].[JW_RM_RetDet] (
    [Pk_InwardDet] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Inward] decimal(18,0)  NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [Quantity] decimal(18,0)  NULL,
    [RollNo] nvarchar(50)  NULL
);
GO

-- Creating table 'JW_RM_RetMast'
CREATE TABLE [dbo].[JW_RM_RetMast] (
    [Pk_Inward] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Inward_Date] datetime  NULL,
    [Fk_Customer] decimal(18,0)  NULL,
    [JWNo] decimal(18,0)  NULL
);
GO

-- Creating table 'JWMat_InwdD'
CREATE TABLE [dbo].[JWMat_InwdD] (
    [Pk_InwardDet] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Inward] decimal(18,0)  NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [Quantity] decimal(18,0)  NULL,
    [RollNo] nvarchar(50)  NULL
);
GO

-- Creating table 'JWMat_InwdM'
CREATE TABLE [dbo].[JWMat_InwdM] (
    [Pk_Inward] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Inward_Date] datetime  NULL,
    [Fk_Vendor] decimal(18,0)  NULL,
    [JWNo] decimal(18,0)  NULL
);
GO

-- Creating table 'Vw_JWTotPaperStockList'
CREATE TABLE [dbo].[Vw_JWTotPaperStockList] (
    [TQty] decimal(38,0)  NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Name] nvarchar(250)  NULL,
    [Fk_Customer] decimal(18,0)  NULL,
    [GSM] decimal(20,0)  NULL,
    [BF] decimal(20,0)  NULL,
    [Deckle] decimal(20,2)  NULL,
    [CustomerName] char(100)  NOT NULL
);
GO

-- Creating table 'Vw_JW_RetRep'
CREATE TABLE [dbo].[Vw_JW_RetRep] (
    [CustomerName] char(100)  NOT NULL,
    [Name] nvarchar(250)  NULL,
    [Quantity] decimal(18,0)  NULL,
    [RollNo] nvarchar(50)  NULL,
    [Pk_Inward] decimal(18,0)  NOT NULL,
    [Inward_Date] datetime  NULL,
    [JWNo] decimal(18,0)  NULL,
    [Pk_InwardDet] decimal(18,0)  NOT NULL,
    [Pk_Material] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'Vw_JWInwdRep'
CREATE TABLE [dbo].[Vw_JWInwdRep] (
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Name] nvarchar(250)  NULL,
    [Inward_Date] datetime  NULL,
    [Pk_Inward] decimal(18,0)  NOT NULL,
    [Quantity] decimal(18,0)  NULL,
    [RollNo] nvarchar(50)  NULL,
    [Pk_InwardDet] decimal(18,0)  NOT NULL,
    [VendorName] char(50)  NOT NULL
);
GO

-- Creating table 'Vw_OSTasks'
CREATE TABLE [dbo].[Vw_OSTasks] (
    [Pk_Material] decimal(18,0)  NULL,
    [Name] nvarchar(250)  NULL,
    [VendorName] char(50)  NOT NULL,
    [GSM] decimal(20,0)  NULL,
    [BF] decimal(20,0)  NULL,
    [Pk_Vendor] decimal(18,0)  NOT NULL,
    [Pk_SrcID] decimal(18,0)  NOT NULL,
    [Description] nvarchar(max)  NULL,
    [IssueWeight] decimal(18,2)  NULL,
    [ReelNo] nvarchar(50)  NULL,
    [Deckle] decimal(20,2)  NULL,
    [Length] decimal(18,0)  NULL,
    [Width] decimal(18,0)  NULL,
    [LayerWt] decimal(18,2)  NULL,
    [AppNos] int  NULL,
    [IssueDate] datetime  NULL,
    [Ply] decimal(18,0)  NULL,
    [Pk_SrcDetID] decimal(18,0)  NOT NULL,
    [Fk_JobCardID] decimal(18,0)  NULL
);
GO

-- Creating table 'TabPPCRep'
CREATE TABLE [dbo].[TabPPCRep] (
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Name] nvarchar(100)  NULL,
    [DeliveryDate] datetime  NULL,
    [CustomerName] nvarchar(90)  NULL,
    [Box_Name] nvarchar(90)  NULL,
    [LayerWt] decimal(18,3)  NULL,
    [PName] nchar(30)  NULL,
    [Length] decimal(18,0)  NULL,
    [Width] decimal(18,0)  NULL,
    [Height] decimal(18,0)  NULL,
    [Quantity] decimal(18,0)  NULL,
    [Pk_BoxID] decimal(18,0)  NULL,
    [PlyVal] int  NULL,
    [JCDeckle] decimal(18,2)  NULL,
    [Deckle] decimal(18,2)  NULL,
    [Ups] decimal(18,2)  NULL,
    [CL] decimal(18,2)  NULL,
    [Pk_DeliverySechedule] decimal(18,0)  NULL,
    [FluteName] nchar(10)  NULL,
    [Weight] decimal(18,2)  NULL,
    [gsm1] int  NULL,
    [gsm2] int  NULL,
    [gsm3] int  NULL,
    [gsm4] int  NULL,
    [gsm5] int  NULL,
    [gsm6] int  NULL,
    [gsm7] int  NULL,
    [gsm8] int  NULL,
    [gsm9] int  NULL,
    [BF] decimal(18,2)  NULL,
    [Pk_Num] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Pk_LayerID] decimal(18,0)  NULL,
    [BF1] int  NULL,
    [BF2] int  NULL,
    [BF3] int  NULL,
    [BF4] int  NULL,
    [BF5] int  NULL,
    [BF6] int  NULL,
    [BF7] int  NULL,
    [BF8] int  NULL,
    [BF9] int  NULL,
    [WT1] decimal(18,2)  NULL,
    [WT2] decimal(18,2)  NULL,
    [WT3] decimal(18,2)  NULL,
    [WT4] decimal(18,2)  NULL,
    [WT5] decimal(18,2)  NULL,
    [WT6] decimal(18,2)  NULL,
    [WT7] decimal(18,2)  NULL,
    [WT8] decimal(18,2)  NULL,
    [WT9] decimal(18,2)  NULL
);
GO

-- Creating table 'tempPPC'
CREATE TABLE [dbo].[tempPPC] (
    [Pk_Materail] decimal(18,0)  NOT NULL,
    [Name] nvarchar(100)  NULL,
    [DeliveryDate] datetime  NULL,
    [CustomerName] nvarchar(90)  NULL,
    [Box_Name] nvarchar(90)  NULL,
    [LayerWt] decimal(18,3)  NULL,
    [PName] nchar(30)  NULL,
    [Length] decimal(18,0)  NULL,
    [Width] decimal(18,0)  NULL,
    [Height] decimal(18,0)  NULL,
    [Quantity] decimal(18,0)  NULL,
    [Pk_BoxID] decimal(18,0)  NULL,
    [PlyVal] int  NULL,
    [JCDeckle] decimal(18,2)  NULL,
    [Deckle] decimal(18,2)  NULL,
    [Ups] decimal(18,2)  NULL,
    [CL] decimal(18,2)  NULL,
    [Pk_DeliverySechedule] decimal(18,0)  NULL,
    [FluteName] nchar(10)  NULL,
    [Weight] decimal(18,2)  NULL,
    [gsm1] int  NULL,
    [gsm2] int  NULL,
    [gsm3] int  NULL,
    [gsm4] int  NULL,
    [gsm5] int  NULL,
    [gsm6] int  NULL,
    [gsm7] int  NULL,
    [gsm8] int  NULL,
    [gsm9] int  NULL,
    [BF] decimal(18,2)  NULL,
    [Pk_Num] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Pk_LayerID] decimal(18,0)  NULL,
    [BF1] int  NULL,
    [BF2] int  NULL,
    [BF3] int  NULL,
    [BF4] int  NULL,
    [BF5] int  NULL,
    [BF6] int  NULL,
    [BF7] int  NULL,
    [BF8] int  NULL,
    [BF9] int  NULL,
    [WT1] decimal(18,2)  NULL,
    [WT2] decimal(18,2)  NULL,
    [WT3] decimal(18,2)  NULL,
    [WT4] decimal(18,2)  NULL,
    [WT5] decimal(18,2)  NULL,
    [WT6] decimal(18,2)  NULL,
    [WT7] decimal(18,2)  NULL,
    [WT8] decimal(18,2)  NULL,
    [WT9] decimal(18,2)  NULL
);
GO

-- Creating table 'Vw_PPC'
CREATE TABLE [dbo].[Vw_PPC] (
    [Pk_LayerID] decimal(18,0)  NULL,
    [Pk_Material] decimal(18,0)  NULL,
    [Name] nvarchar(250)  NULL,
    [MillName] nvarchar(150)  NULL,
    [ColorName] nvarchar(30)  NULL,
    [stkqty] decimal(38,0)  NULL,
    [DeliveryDate] datetime  NULL,
    [CustomerName] char(100)  NULL,
    [Box_Name] nvarchar(500)  NULL,
    [PName] nvarchar(50)  NULL,
    [Length] int  NULL,
    [Width] int  NULL,
    [Height] int  NULL,
    [Quantity] decimal(18,0)  NULL,
    [Expr1] decimal(38,3)  NULL,
    [Pk_BoxID] decimal(18,0)  NULL,
    [PlyVal] decimal(18,0)  NULL,
    [Deckle] decimal(18,2)  NULL,
    [Ups] decimal(24,0)  NULL,
    [CDeck] decimal(36,0)  NULL,
    [CL] int  NULL,
    [Pk_DeliverySechedule] decimal(18,0)  NULL,
    [Pk_PartPropertyID] decimal(18,0)  NOT NULL,
    [LayerWt] decimal(38,3)  NULL,
    [FluteName] varchar(50)  NULL,
    [Weight] decimal(18,3)  NULL,
    [GSM] decimal(18,0)  NULL,
    [BF] decimal(18,0)  NULL,
    [JCDeckle] decimal(36,0)  NULL
);
GO

-- Creating table 'gen_Customer'
CREATE TABLE [dbo].[gen_Customer] (
    [Pk_Customer] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [CustomerName] char(100)  NOT NULL,
    [CustomerType] char(50)  NOT NULL,
    [CustomerAddress] nvarchar(500)  NOT NULL,
    [Fk_City] decimal(18,0)  NOT NULL,
    [Fk_State] decimal(18,0)  NOT NULL,
    [Fk_Country] decimal(18,0)  NOT NULL,
    [PinCode] decimal(18,0)  NOT NULL,
    [CustomerContact] decimal(18,0)  NOT NULL,
    [LandLine] decimal(18,0)  NOT NULL,
    [Email] nvarchar(50)  NOT NULL,
    [ContactPerson] char(50)  NOT NULL,
    [Fk_Tanent] decimal(18,0)  NOT NULL,
    [CreatedDateOn] datetime  NOT NULL,
    [CustomerShort] nchar(10)  NULL,
    [VAT] nvarchar(50)  NULL,
    [PAN] nvarchar(50)  NULL,
    [Regn] nvarchar(50)  NULL,
    [CreditLimit] nchar(10)  NULL,
    [CreditAmt] decimal(18,2)  NULL,
    [Fk_ParentCust] decimal(18,0)  NULL
);
GO

-- Creating table 'BoxSpecs'
CREATE TABLE [dbo].[BoxSpecs] (
    [Pk_BoxSpecID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [OuterShellYes] bit  NULL,
    [CapYes] bit  NULL,
    [LenghtPartationYes] bit  NULL,
    [WidthPartationYes] bit  NULL,
    [PlateYes] bit  NULL,
    [OuterShell] int  NULL,
    [Cap] int  NULL,
    [LenghtPartation] int  NULL,
    [WidthPartation] int  NULL,
    [Plate] int  NULL,
    [TotalWeight] decimal(18,3)  NULL,
    [Fk_BoxID] decimal(18,0)  NULL,
    [TopYes] bit  NULL,
    [BottomYes] bit  NULL,
    [TopVal] int  NULL,
    [BottVal] int  NULL,
    [SleeveYes] bit  NULL,
    [Sleeve] int  NULL
);
GO

-- Creating table 'Vw_SchOrders'
CREATE TABLE [dbo].[Vw_SchOrders] (
    [CustomerName] char(100)  NOT NULL,
    [Fk_Customer] decimal(18,0)  NULL,
    [Cust_PO] nvarchar(50)  NULL,
    [Pk_Order] decimal(18,0)  NOT NULL,
    [OrderDate] datetime  NOT NULL,
    [Pk_DeliverySechedule] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'BoxMaster'
CREATE TABLE [dbo].[BoxMaster] (
    [Pk_BoxID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(500)  NOT NULL,
    [Customer] decimal(18,0)  NOT NULL,
    [Description] nvarchar(500)  NULL,
    [Fk_BoxType] decimal(18,0)  NULL,
    [PartNo] varchar(50)  NULL,
    [Fk_FluteType] decimal(18,0)  NULL
);
GO

-- Creating table 'Box_Documents'
CREATE TABLE [dbo].[Box_Documents] (
    [BoxPk_Documents] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_BoxID] decimal(18,0)  NULL,
    [FileName] nvarchar(255)  NULL,
    [FileSize] nvarchar(50)  NULL
);
GO

-- Creating table 'Vw_PaperCertificate'
CREATE TABLE [dbo].[Vw_PaperCertificate] (
    [Pk_Id] decimal(18,0)  NOT NULL,
    [Invno] nvarchar(50)  NULL,
    [InvDate] datetime  NULL,
    [Name] nvarchar(250)  NULL,
    [Fk_Mill] decimal(18,0)  NULL,
    [Fk_Characteristics] decimal(18,0)  NULL,
    [Target] nvarchar(150)  NULL,
    [Acceptance] nvarchar(150)  NULL,
    [Result1] nchar(10)  NULL,
    [Result2] nchar(10)  NULL,
    [Result3] nchar(10)  NULL,
    [Result4] nchar(10)  NULL,
    [Result5] nchar(10)  NULL,
    [Result6] nchar(10)  NULL,
    [MinVal] nchar(10)  NULL,
    [MaxVal] nchar(10)  NULL,
    [AvgVal] nchar(10)  NULL,
    [Remarks] nchar(150)  NULL,
    [Quantity] decimal(18,0)  NULL,
    [Tested] nvarchar(50)  NULL,
    [Approved] nvarchar(50)  NULL,
    [VendorName] char(50)  NOT NULL,
    [CName] nvarchar(100)  NULL,
    [UOM] nvarchar(50)  NULL,
    [Pk_CharID] decimal(18,0)  NOT NULL,
    [Pk_IdDet] decimal(18,0)  NOT NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [TestMethod] nvarchar(50)  NULL,
    [TQty] decimal(18,0)  NULL,
    [ReelNo] nchar(10)  NULL
);
GO

-- Creating table 'Vw_JCard'
CREATE TABLE [dbo].[Vw_JCard] (
    [Pk_JobCardID] decimal(18,0)  NULL,
    [JDate] datetime  NULL,
    [Fk_Order] decimal(18,0)  NULL,
    [CustomerName] char(100)  NOT NULL,
    [Invno] varchar(50)  NULL,
    [Corrugation] varchar(50)  NULL,
    [TopPaperQty] decimal(18,0)  NULL,
    [TwoPlyQty] decimal(18,0)  NULL,
    [TwoPlyWt] decimal(18,0)  NULL,
    [PastingQty] decimal(18,0)  NULL,
    [PastingWstQty] decimal(18,0)  NULL,
    [RotaryQty] decimal(18,0)  NULL,
    [RotaryWstQty] decimal(18,0)  NULL,
    [PunchingQty] decimal(18,0)  NULL,
    [PunchingWstQty] decimal(18,0)  NULL,
    [SlotingQty] decimal(18,0)  NULL,
    [SlotingWstQty] decimal(18,0)  NULL,
    [PinningQty] decimal(18,0)  NULL,
    [PinningWstQty] decimal(18,0)  NULL,
    [FinishingQty] decimal(18,0)  NULL,
    [FinishingWstQty] decimal(18,0)  NULL,
    [TotalQty] decimal(18,0)  NULL,
    [TotalWstQty] decimal(18,0)  NULL,
    [BName] nvarchar(500)  NULL,
    [Fk_Status] decimal(18,0)  NULL,
    [Fk_PartID] decimal(18,0)  NULL,
    [PName] nvarchar(50)  NULL,
    [Length] int  NULL,
    [Width] int  NULL,
    [Height] int  NULL,
    [Pk_PartPropertyID] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'Vw_PaperConsumption'
CREATE TABLE [dbo].[Vw_PaperConsumption] (
    [RollNo] nvarchar(50)  NULL,
    [Quantity] decimal(18,0)  NOT NULL,
    [ReturnQuantity] decimal(18,0)  NULL,
    [Pk_JobCardID] decimal(18,0)  NOT NULL,
    [JDate] datetime  NULL,
    [Fk_Order] decimal(18,0)  NULL,
    [OrderDate] datetime  NOT NULL,
    [CustomerName] char(100)  NOT NULL
);
GO

-- Creating table 'Vw_IndentPOSearch'
CREATE TABLE [dbo].[Vw_IndentPOSearch] (
    [Fk_PONo] decimal(18,0)  NULL,
    [Pk_MaterialOrderMasterId] decimal(18,0)  NOT NULL,
    [Name] nvarchar(250)  NULL,
    [IndQty] decimal(18,3)  NULL,
    [POQty] decimal(18,0)  NULL,
    [PendingQty] decimal(22,3)  NULL,
    [MaterialIndentDate] datetime  NULL,
    [ColorName] nvarchar(30)  NULL,
    [Pk_MaterialOrderDetailsId] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'Vw_SampleJC'
CREATE TABLE [dbo].[Vw_SampleJC] (
    [Pk_BoxID] decimal(18,0)  NOT NULL,
    [BName] nvarchar(500)  NOT NULL,
    [Description] nvarchar(500)  NULL,
    [CustomerName] char(100)  NOT NULL,
    [CuttingSize] decimal(18,2)  NULL,
    [PartTakeup] decimal(18,2)  NULL,
    [PName] nvarchar(50)  NULL,
    [Length] int  NULL,
    [Width] int  NULL,
    [Height] int  NULL,
    [TotalQty] int  NULL,
    [Deckle] decimal(18,2)  NULL,
    [BoardGSM] decimal(18,2)  NULL,
    [BoardBS] decimal(18,2)  NULL,
    [PlyVal] decimal(18,0)  NULL,
    [FluteHt] decimal(18,2)  NULL,
    [FluteName] varchar(50)  NULL,
    [GSM] decimal(18,0)  NULL,
    [BF] decimal(18,0)  NULL,
    [ColorName] nvarchar(50)  NULL,
    [Weight] decimal(18,3)  NULL,
    [Tk_Factor] decimal(18,2)  NULL,
    [BoardArea] decimal(18,2)  NULL,
    [Pk_JobCardID] decimal(18,0)  NOT NULL,
    [JDate] datetime  NULL,
    [Printing] nvarchar(50)  NULL,
    [Calico] bit  NULL,
    [Others] varchar(500)  NULL,
    [Invno] varchar(50)  NULL,
    [Corrugation] varchar(50)  NULL,
    [TopPaperQty] decimal(18,0)  NULL,
    [TwoPlyQty] decimal(18,0)  NULL,
    [TwoPlyWt] decimal(18,0)  NULL,
    [PastingQty] decimal(18,0)  NULL,
    [RotaryQty] decimal(18,0)  NULL,
    [PunchingQty] decimal(18,0)  NULL,
    [SlotingQty] decimal(18,0)  NULL,
    [PinningQty] decimal(18,0)  NULL,
    [FinishingQty] decimal(18,0)  NULL,
    [ProdQty] decimal(18,0)  NOT NULL,
    [Machine] decimal(18,0)  NULL,
    [ChkQuality] decimal(18,0)  NULL,
    [PColor] nvarchar(50)  NULL,
    [PDetails] nvarchar(150)  NULL,
    [MFG] nvarchar(50)  NULL,
    [DPRSc] nvarchar(50)  NULL,
    [ECCQty] decimal(18,0)  NULL,
    [GummingQty] decimal(18,0)  NULL,
    [UpsVal] decimal(18,2)  NULL,
    [CutLength] decimal(18,2)  NULL,
    [Stitching] bit  NULL,
    [MillName] nvarchar(150)  NULL,
    [Pk_LayerID] decimal(18,0)  NOT NULL,
    [Rate] decimal(18,2)  NULL,
    [Corr] nchar(10)  NULL,
    [TopP] nchar(10)  NULL,
    [Rotary] nchar(10)  NULL,
    [Pasting] nchar(10)  NULL,
    [PrintingP] nchar(10)  NULL,
    [Punching] nchar(10)  NULL,
    [Slotting] nchar(10)  NULL,
    [Finishing] nchar(10)  NULL,
    [Pinning] nchar(10)  NULL,
    [Gumming] nchar(10)  NULL,
    [Bundling] nchar(10)  NULL
);
GO

-- Creating table 'vw_PoCertList'
CREATE TABLE [dbo].[vw_PoCertList] (
    [Fk_Material] decimal(18,0)  NULL,
    [Name] nvarchar(250)  NULL,
    [GSM] decimal(20,0)  NULL,
    [BF] decimal(20,0)  NULL,
    [ColorName] nvarchar(30)  NULL,
    [VendorName] char(50)  NULL,
    [Pk_Vendor] decimal(18,0)  NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Pk_PONo] decimal(18,0)  NOT NULL,
    [Fk_Status] decimal(18,0)  NULL,
    [PkDisp] varchar(255)  NULL,
    [HSNCode] nvarchar(50)  NULL
);
GO

-- Creating table 'Inv_MaterialIndentDetails'
CREATE TABLE [dbo].[Inv_MaterialIndentDetails] (
    [Pk_MaterialOrderDetailsId] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_MaterialOrderMasterId] decimal(18,0)  NULL,
    [RequiredDate] datetime  NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [Fk_CustomerOrder] decimal(18,0)  NULL,
    [Quantity] decimal(18,3)  NULL,
    [Fk_Mill] decimal(18,0)  NULL,
    [Fk_Vendor] decimal(18,0)  NULL,
    [GSM] decimal(18,0)  NULL,
    [BF] decimal(18,0)  NULL,
    [Deckle] decimal(18,2)  NULL
);
GO

-- Creating table 'Vw_Popup_MaterialIssue'
CREATE TABLE [dbo].[Vw_Popup_MaterialIssue] (
    [Name] nvarchar(30)  NULL,
    [Pk_MaterialCategory] decimal(18,0)  NOT NULL,
    [MaterialName] nvarchar(250)  NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Quantity] decimal(18,2)  NULL,
    [Pk_Stock] decimal(18,0)  NOT NULL,
    [Fk_Tanent] decimal(18,0)  NOT NULL,
    [UnitName] nvarchar(50)  NULL,
    [Fk_BranchID] decimal(18,0)  NULL,
    [ColorName] nvarchar(30)  NULL,
    [MillName] nvarchar(150)  NULL,
    [Length] decimal(20,0)  NULL,
    [Width] decimal(20,0)  NULL,
    [Height] decimal(20,0)  NULL,
    [PPly] decimal(20,0)  NULL,
    [Weight] decimal(20,0)  NULL,
    [Brand] nvarchar(20)  NULL,
    [Description] nvarchar(100)  NULL
);
GO

-- Creating table 'Vw_PODet'
CREATE TABLE [dbo].[Vw_PODet] (
    [PODate] datetime  NULL,
    [Fk_Indent] decimal(18,0)  NULL,
    [Pk_PONo] decimal(18,0)  NOT NULL,
    [VendorName] char(50)  NOT NULL,
    [Pk_Id] decimal(18,0)  NOT NULL,
    [Name] nvarchar(250)  NULL,
    [PkDisp] varchar(255)  NULL
);
GO

-- Creating table 'Vw_BillingBoxStocks'
CREATE TABLE [dbo].[Vw_BillingBoxStocks] (
    [Name] nvarchar(500)  NOT NULL,
    [Pk_BoxID] decimal(18,0)  NOT NULL,
    [Pk_Order] decimal(18,0)  NOT NULL,
    [CustomerName] char(100)  NOT NULL,
    [Quantity] decimal(18,0)  NULL,
    [PName] nvarchar(50)  NULL,
    [Pk_StockID] decimal(18,0)  NOT NULL,
    [Fk_Customer] decimal(18,0)  NULL,
    [Pk_PartPropertyID] decimal(18,0)  NOT NULL,
    [Fk_Status] decimal(18,0)  NULL,
    [OrdQty] decimal(18,0)  NULL,
    [Product] nvarchar(500)  NULL,
    [Weight] decimal(18,3)  NULL
);
GO

-- Creating table 'Vw_PoDetOthers'
CREATE TABLE [dbo].[Vw_PoDetOthers] (
    [PODate] datetime  NULL,
    [Fk_Indent] decimal(18,0)  NULL,
    [Pk_PONo] decimal(18,0)  NOT NULL,
    [VendorName] char(50)  NOT NULL,
    [Name] nvarchar(250)  NULL,
    [PkDisp] varchar(255)  NULL,
    [Fk_MaterialCategory] decimal(18,0)  NULL,
    [MatCatName] nvarchar(30)  NULL,
    [Description] nvarchar(100)  NULL,
    [Pk_PODet] decimal(18,0)  NOT NULL,
    [UnitName] nvarchar(50)  NULL
);
GO

-- Creating table 'Vw_OthersStock'
CREATE TABLE [dbo].[Vw_OthersStock] (
    [MatName] nvarchar(250)  NULL,
    [CatName] nvarchar(30)  NULL,
    [Quantity] decimal(18,2)  NULL,
    [Fk_MaterialCategory] decimal(18,0)  NULL,
    [Pk_Material] decimal(18,0)  NULL,
    [Pk_Stock] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'ConsumableIssueDet'
CREATE TABLE [dbo].[ConsumableIssueDet] (
    [Pk_ID_Det] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_ID] decimal(18,0)  NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [Quantity] decimal(18,0)  NULL,
    [Returned] nchar(10)  NULL,
    [Remarks] nvarchar(500)  NULL
);
GO

-- Creating table 'ConsumablesIssue'
CREATE TABLE [dbo].[ConsumablesIssue] (
    [Pk_ID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [IssuedBy] nvarchar(100)  NULL,
    [IssuedTo] nvarchar(100)  NULL,
    [IssueDate] datetime  NULL
);
GO

-- Creating table 'Vw_ConsIssueList'
CREATE TABLE [dbo].[Vw_ConsIssueList] (
    [CatName] nvarchar(30)  NULL,
    [Pk_Material] decimal(18,0)  NULL,
    [MatName] nvarchar(250)  NULL,
    [Pk_ID] decimal(18,0)  NOT NULL,
    [Pk_ID_Det] decimal(18,0)  NOT NULL,
    [Quantity] decimal(18,0)  NULL,
    [Returned] nchar(10)  NULL,
    [IssuedBy] nvarchar(100)  NULL,
    [IssuedTo] nvarchar(100)  NULL,
    [IssueDate] datetime  NULL
);
GO

-- Creating table 'Vw_ConsStockList'
CREATE TABLE [dbo].[Vw_ConsStockList] (
    [CatName] nvarchar(30)  NULL,
    [Pk_Material] decimal(18,0)  NULL,
    [MatName] nvarchar(250)  NULL,
    [Pk_Stock] decimal(18,0)  NOT NULL,
    [Quantity] decimal(18,2)  NULL,
    [Fk_MaterialCategory] decimal(18,0)  NULL
);
GO

-- Creating table 'Vw_ConsPO'
CREATE TABLE [dbo].[Vw_ConsPO] (
    [Pk_MaterialCategory] decimal(18,0)  NOT NULL,
    [Pk_PONo] decimal(18,0)  NOT NULL,
    [PODate] datetime  NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [Name] nvarchar(30)  NULL,
    [MatName] nvarchar(250)  NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Pk_PODet] decimal(18,0)  NOT NULL,
    [Quantity] decimal(18,0)  NULL,
    [Amount] decimal(18,2)  NULL,
    [VendorName] char(50)  NOT NULL,
    [Fk_Status] decimal(18,0)  NULL,
    [Pk_Vendor] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'Vw_Mat_Inwd'
CREATE TABLE [dbo].[Vw_Mat_Inwd] (
    [SQty] decimal(38,0)  NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [PONo] decimal(18,0)  NULL,
    [Pk_Inward] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'Vw_PO_Pending_Inward'
CREATE TABLE [dbo].[Vw_PO_Pending_Inward] (
    [Name] nvarchar(250)  NULL,
    [Fk_MaterialCategory] decimal(18,0)  NULL,
    [Pk_PONo] decimal(18,0)  NOT NULL,
    [PODate] datetime  NULL,
    [OrdQty] decimal(18,0)  NULL,
    [Pk_PODet] decimal(18,0)  NOT NULL,
    [CatName] nvarchar(30)  NULL,
    [SQty] decimal(38,0)  NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [PONo] decimal(18,0)  NULL,
    [Pk_Inward] decimal(18,0)  NULL
);
GO

-- Creating table 'Vw_MatInwdDetails'
CREATE TABLE [dbo].[Vw_MatInwdDetails] (
    [Sqty] decimal(38,0)  NULL,
    [Pk_MaterialCategory] decimal(18,0)  NOT NULL,
    [CatName] nvarchar(30)  NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [MatName] nvarchar(250)  NULL,
    [Pk_Inward] decimal(18,0)  NOT NULL,
    [Inward_Date] datetime  NULL,
    [PONo] decimal(18,0)  NULL,
    [VendorName] char(50)  NOT NULL
);
GO

-- Creating table 'Vw_IssueDay'
CREATE TABLE [dbo].[Vw_IssueDay] (
    [Pk_JobCardID] decimal(18,0)  NOT NULL,
    [JDate] datetime  NULL,
    [Fk_Material] decimal(18,0)  NOT NULL,
    [IssQty] decimal(18,0)  NOT NULL,
    [RollNo] nvarchar(50)  NULL,
    [Name] nvarchar(250)  NULL,
    [Pk_MaterialIssueID] decimal(18,0)  NOT NULL,
    [IssueDate] datetime  NULL,
    [MillName] nvarchar(150)  NULL,
    [ColorName] nvarchar(30)  NULL,
    [CustomerName] char(100)  NOT NULL,
    [Pk_Order] decimal(18,0)  NOT NULL,
    [OrderDate] datetime  NOT NULL
);
GO

-- Creating table 'WorkOrderDetails'
CREATE TABLE [dbo].[WorkOrderDetails] (
    [Pk_WorkOrderDet] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_WorkOrderID] decimal(18,0)  NULL,
    [Fk_PaperStock] decimal(18,0)  NULL,
    [QtyConsumed] decimal(18,0)  NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [RM_Consumed] decimal(18,0)  NULL
);
GO

-- Creating table 'WO_Documents'
CREATE TABLE [dbo].[WO_Documents] (
    [PK_WODocuments] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_WONo] decimal(18,0)  NULL,
    [FileName] nvarchar(255)  NULL,
    [FileSize] nvarchar(50)  NULL
);
GO

-- Creating table 'JobWorkChild'
CREATE TABLE [dbo].[JobWorkChild] (
    [PkJobDetID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_JobID] decimal(18,0)  NULL,
    [Length] decimal(18,0)  NULL,
    [Width] decimal(18,0)  NULL,
    [Height] decimal(18,0)  NULL,
    [Ply] decimal(18,0)  NULL,
    [AppNos] int  NULL,
    [LayerWt] decimal(18,2)  NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [Quantity] decimal(18,0)  NULL,
    [RollNo] nvarchar(50)  NULL,
    [RecdWeight] decimal(18,0)  NULL
);
GO

-- Creating table 'Vw__OpenJcList'
CREATE TABLE [dbo].[Vw__OpenJcList] (
    [CustomerName] char(100)  NOT NULL,
    [OrderDate] datetime  NOT NULL,
    [Pk_Order] decimal(18,0)  NOT NULL,
    [Pk_JobCardID] decimal(18,0)  NOT NULL,
    [JDate] datetime  NULL,
    [Fk_Status] decimal(18,0)  NULL,
    [Name] nvarchar(500)  NOT NULL,
    [TotalQty] decimal(18,0)  NOT NULL,
    [Pk_BoxID] decimal(18,0)  NOT NULL,
    [PName] nvarchar(50)  NULL
);
GO

-- Creating table 'WorkOrder'
CREATE TABLE [dbo].[WorkOrder] (
    [WorkOrder_ID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [WODate] datetime  NULL,
    [Fk_JobCardID] decimal(18,0)  NULL,
    [Fk_Status] decimal(18,0)  NULL,
    [Fk_BoxID] decimal(18,0)  NULL,
    [Printing] nvarchar(50)  NULL,
    [Calico] bit  NULL,
    [Others] varchar(500)  NULL,
    [Invno] varchar(50)  NULL,
    [Corrugation] varchar(50)  NULL,
    [TopPaperQty] decimal(18,0)  NULL,
    [TwoPlyQty] decimal(18,0)  NULL,
    [TwoPlyWt] decimal(18,0)  NULL,
    [PastingQty] decimal(18,0)  NULL,
    [PastingWstQty] decimal(18,0)  NULL,
    [RotaryQty] decimal(18,0)  NULL,
    [RotaryWstQty] decimal(18,0)  NULL,
    [PunchingQty] decimal(18,0)  NULL,
    [PunchingWstQty] decimal(18,0)  NULL,
    [SlotingQty] decimal(18,0)  NULL,
    [SlotingWstQty] decimal(18,0)  NULL,
    [PinningQty] decimal(18,0)  NULL,
    [PinningWstQty] decimal(18,0)  NULL,
    [FinishingQty] decimal(18,0)  NULL,
    [FinishingWstQty] decimal(18,0)  NULL,
    [TotalQty] decimal(18,0)  NOT NULL,
    [TotalWstQty] decimal(18,0)  NULL,
    [Machine] decimal(18,0)  NULL,
    [StartTime] nvarchar(50)  NULL,
    [EndTime] nvarchar(50)  NULL,
    [ChkQuality] decimal(18,0)  NULL,
    [PColor] nvarchar(50)  NULL,
    [PDetails] nvarchar(150)  NULL,
    [MFG] nvarchar(50)  NULL,
    [DPRSc] nvarchar(50)  NULL,
    [ECCQty] decimal(18,0)  NULL,
    [EccWstQty] decimal(18,0)  NULL,
    [GummingQty] decimal(18,0)  NULL,
    [GummingWstQty] decimal(18,0)  NULL,
    [Fk_Schedule] decimal(18,0)  NULL,
    [Stitching] bit  NULL,
    [CutLength] decimal(18,2)  NULL,
    [UpsVal] decimal(18,2)  NULL,
    [Corr] nchar(10)  NULL,
    [TopP] nchar(10)  NULL,
    [Pasting] nchar(10)  NULL,
    [Rotary] nchar(10)  NULL,
    [PrintingP] nchar(10)  NULL,
    [Punching] nchar(10)  NULL,
    [Slotting] nchar(10)  NULL,
    [Finishing] nchar(10)  NULL,
    [Pinning] nchar(10)  NULL,
    [Gumming] nchar(10)  NULL,
    [Bundling] nchar(10)  NULL,
    [Bundles] decimal(18,0)  NULL,
    [CS_BS] nchar(10)  NULL
);
GO

-- Creating table 'MaterialIssue'
CREATE TABLE [dbo].[MaterialIssue] (
    [Pk_MaterialIssueID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [IssueDate] datetime  NULL,
    [Fk_UserID] decimal(18,0)  NULL,
    [Fk_Tanent] decimal(18,0)  NULL,
    [Fk_OrderNo] decimal(18,0)  NULL,
    [DCNo] varchar(50)  NULL,
    [Fk_Branch] decimal(18,0)  NULL,
    [Fk_JobCardID] decimal(18,0)  NULL,
    [Fk_WOrderNo] decimal(18,0)  NULL
);
GO

-- Creating table 'Vw_IssueScheduleList'
CREATE TABLE [dbo].[Vw_IssueScheduleList] (
    [DeliveryDate] datetime  NOT NULL,
    [SchQty] decimal(18,0)  NULL,
    [Pk_BoxID] decimal(18,0)  NOT NULL,
    [Name] nvarchar(500)  NOT NULL,
    [OrdQty] decimal(18,0)  NULL,
    [Pk_Order] decimal(18,0)  NOT NULL,
    [OrderDate] datetime  NOT NULL,
    [CustomerName] char(100)  NOT NULL,
    [Pk_DeliverySechedule] decimal(18,0)  NOT NULL,
    [Cust_PO] nvarchar(50)  NULL,
    [Fk_Enquiry] decimal(18,0)  NULL,
    [PName] nvarchar(50)  NULL,
    [Pk_PartPropertyID] decimal(18,0)  NOT NULL,
    [Fk_BoxID] decimal(18,0)  NULL,
    [Pk_JobCardID] decimal(18,0)  NOT NULL,
    [Product] nvarchar(500)  NULL,
    [WorkOrder_ID] decimal(18,0)  NOT NULL,
    [WODate] datetime  NULL
);
GO

-- Creating table 'JobCardMaster'
CREATE TABLE [dbo].[JobCardMaster] (
    [Pk_JobCardID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [JDate] datetime  NULL,
    [Fk_Order] decimal(18,0)  NULL,
    [Fk_Status] decimal(18,0)  NULL,
    [Fk_BoxID] decimal(18,0)  NULL,
    [Printing] nvarchar(50)  NULL,
    [Calico] bit  NULL,
    [Others] varchar(500)  NULL,
    [Invno] varchar(50)  NULL,
    [Corrugation] varchar(50)  NULL,
    [TopPaperQty] decimal(18,0)  NULL,
    [TwoPlyQty] decimal(18,0)  NULL,
    [TwoPlyWt] decimal(18,0)  NULL,
    [PastingQty] decimal(18,0)  NULL,
    [PastingWstQty] decimal(18,0)  NULL,
    [RotaryQty] decimal(18,0)  NULL,
    [RotaryWstQty] decimal(18,0)  NULL,
    [PunchingQty] decimal(18,0)  NULL,
    [PunchingWstQty] decimal(18,0)  NULL,
    [SlotingQty] decimal(18,0)  NULL,
    [SlotingWstQty] decimal(18,0)  NULL,
    [PinningQty] decimal(18,0)  NULL,
    [PinningWstQty] decimal(18,0)  NULL,
    [FinishingQty] decimal(18,0)  NULL,
    [FinishingWstQty] decimal(18,0)  NULL,
    [TotalQty] decimal(18,0)  NOT NULL,
    [TotalWstQty] decimal(18,0)  NULL,
    [Machine] decimal(18,0)  NULL,
    [StartTime] nvarchar(50)  NULL,
    [EndTime] nvarchar(50)  NULL,
    [ChkQuality] decimal(18,0)  NULL,
    [PColor] nvarchar(50)  NULL,
    [PDetails] nvarchar(150)  NULL,
    [MFG] nvarchar(50)  NULL,
    [DPRSc] nvarchar(50)  NULL,
    [ECCQty] decimal(18,0)  NULL,
    [EccWstQty] decimal(18,0)  NULL,
    [GummingQty] decimal(18,0)  NULL,
    [GummingWstQty] decimal(18,0)  NULL,
    [Fk_Schedule] decimal(18,0)  NULL,
    [Stitching] bit  NULL,
    [CutLength] decimal(18,2)  NULL,
    [UpsVal] decimal(18,2)  NULL,
    [Corr] nchar(10)  NULL,
    [TopP] nchar(10)  NULL,
    [Pasting] nchar(10)  NULL,
    [Rotary] nchar(10)  NULL,
    [PrintingP] nchar(10)  NULL,
    [Punching] nchar(10)  NULL,
    [Slotting] nchar(10)  NULL,
    [Finishing] nchar(10)  NULL,
    [Pinning] nchar(10)  NULL,
    [Gumming] nchar(10)  NULL,
    [Bundling] nchar(10)  NULL,
    [Bundles] decimal(18,0)  NULL,
    [CS_BS] nchar(10)  NULL,
    [Accepted] nchar(10)  NULL
);
GO

-- Creating table 'gen_Order'
CREATE TABLE [dbo].[gen_Order] (
    [Pk_Order] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [OrderDate] datetime  NOT NULL,
    [Fk_Enquiry] decimal(18,0)  NULL,
    [Fk_Customer] decimal(18,0)  NULL,
    [Fk_Branch] decimal(18,0)  NULL,
    [Product] nvarchar(500)  NULL,
    [ShippingAddress] nvarchar(max)  NULL,
    [ShippingInstruction] nvarchar(max)  NULL,
    [SpecialInstructions] nvarchar(max)  NULL,
    [Quantity] int  NULL,
    [Price] decimal(18,2)  NULL,
    [Fk_Tanent] decimal(18,0)  NULL,
    [Fk_ShippingId] decimal(18,0)  NULL,
    [Fk_BoxID] decimal(18,0)  NULL,
    [Cust_PO] nvarchar(50)  NULL,
    [GrandTotal] decimal(18,2)  NULL,
    [NetValue] decimal(18,2)  NULL,
    [Accepted] nchar(10)  NULL,
    [Fk_StatusVal] decimal(18,0)  NULL,
    [CustOrdDate] datetime  NULL
);
GO

-- Creating table 'Vw_POCertificate'
CREATE TABLE [dbo].[Vw_POCertificate] (
    [SQty] decimal(38,0)  NOT NULL,
    [Pk_PONo] decimal(18,0)  NOT NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [Quantity] decimal(18,0)  NULL,
    [Name] nvarchar(250)  NULL,
    [VendorName] char(50)  NOT NULL,
    [Pk_Vendor] decimal(18,0)  NOT NULL,
    [GSM] decimal(20,0)  NULL,
    [BF] decimal(20,0)  NULL,
    [ColorName] nvarchar(30)  NULL,
    [PkDisp] varchar(255)  NULL,
    [Pk_PODet] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'Vw_ProcessWastage'
CREATE TABLE [dbo].[Vw_ProcessWastage] (
    [Pk_ID] decimal(18,0)  NOT NULL,
    [Fk_ProcessID] decimal(18,0)  NULL,
    [WastageQty] decimal(18,2)  NULL,
    [Quantity] decimal(18,0)  NULL,
    [ProcessDate] datetime  NULL,
    [PQuantity] decimal(18,0)  NULL,
    [ReasonWastage] nvarchar(250)  NULL,
    [RemainingQty] decimal(18,0)  NULL,
    [WQty] decimal(18,2)  NULL,
    [Pk_JobCardID] decimal(18,0)  NOT NULL,
    [ProcessName] nvarchar(150)  NULL,
    [Pk_Process] decimal(18,0)  NOT NULL,
    [PSTime] nchar(10)  NULL,
    [PETime] nchar(10)  NULL
);
GO

-- Creating table 'Vw_PoGetRec'
CREATE TABLE [dbo].[Vw_PoGetRec] (
    [Pk_Material] decimal(18,0)  NULL,
    [Name] nvarchar(250)  NULL,
    [Quantity] decimal(18,0)  NULL,
    [Pk_PONo] decimal(18,0)  NOT NULL,
    [Rate] decimal(18,2)  NULL,
    [Amount] decimal(18,2)  NULL,
    [Fk_Status] decimal(18,0)  NULL,
    [InwdQty] decimal(18,0)  NULL,
    [VendorName] char(50)  NOT NULL,
    [GSM] decimal(20,0)  NOT NULL,
    [BF] decimal(20,0)  NOT NULL,
    [Pk_Vendor] decimal(18,0)  NOT NULL,
    [PODate] datetime  NULL,
    [ColorName] nvarchar(30)  NULL,
    [MillName] nvarchar(150)  NULL,
    [Fk_IndentVal] decimal(18,0)  NULL,
    [Pk_Mill] decimal(18,0)  NOT NULL,
    [Deckle] decimal(20,2)  NOT NULL,
    [UnitName] nvarchar(50)  NULL,
    [CatName] nvarchar(30)  NULL,
    [Pk_PODet] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'Vw_InProcessJC'
CREATE TABLE [dbo].[Vw_InProcessJC] (
    [Pk_JobCardID] decimal(18,0)  NOT NULL,
    [JDate] datetime  NULL,
    [Fk_Order] decimal(18,0)  NULL,
    [Fk_Status] decimal(18,0)  NULL,
    [Fk_BoxID] decimal(18,0)  NULL,
    [Printing] nvarchar(50)  NULL,
    [Calico] bit  NULL,
    [Others] varchar(500)  NULL,
    [Invno] varchar(50)  NULL,
    [Corrugation] varchar(50)  NULL,
    [TopPaperQty] decimal(18,0)  NULL,
    [TwoPlyQty] decimal(18,0)  NULL,
    [TwoPlyWt] decimal(18,0)  NULL,
    [PastingQty] decimal(18,0)  NULL,
    [PastingWstQty] decimal(18,0)  NULL,
    [RotaryQty] decimal(18,0)  NULL,
    [RotaryWstQty] decimal(18,0)  NULL,
    [PunchingQty] decimal(18,0)  NULL,
    [PunchingWstQty] decimal(18,0)  NULL,
    [SlotingQty] decimal(18,0)  NULL,
    [SlotingWstQty] decimal(18,0)  NULL,
    [PinningQty] decimal(18,0)  NULL,
    [PinningWstQty] decimal(18,0)  NULL,
    [FinishingQty] decimal(18,0)  NULL,
    [FinishingWstQty] decimal(18,0)  NULL,
    [TotalQty] decimal(18,0)  NOT NULL,
    [TotalWstQty] decimal(18,0)  NULL,
    [Machine] decimal(18,0)  NULL,
    [StartTime] nvarchar(50)  NULL,
    [EndTime] nvarchar(50)  NULL,
    [ChkQuality] decimal(18,0)  NULL,
    [PColor] nvarchar(50)  NULL,
    [PDetails] nvarchar(150)  NULL,
    [MFG] nvarchar(50)  NULL,
    [DPRSc] nvarchar(50)  NULL,
    [ECCQty] decimal(18,0)  NULL,
    [EccWstQty] decimal(18,0)  NULL,
    [GummingQty] decimal(18,0)  NULL,
    [GummingWstQty] decimal(18,0)  NULL,
    [Fk_Schedule] decimal(18,0)  NULL,
    [Stitching] bit  NULL,
    [CutLength] decimal(18,2)  NULL,
    [UpsVal] decimal(18,2)  NULL,
    [Corr] nchar(10)  NULL,
    [TopP] nchar(10)  NULL,
    [Pasting] nchar(10)  NULL,
    [Rotary] nchar(10)  NULL,
    [PrintingP] nchar(10)  NULL,
    [Punching] nchar(10)  NULL,
    [Slotting] nchar(10)  NULL,
    [Finishing] nchar(10)  NULL,
    [Pinning] nchar(10)  NULL,
    [Gumming] nchar(10)  NULL,
    [Bundling] nchar(10)  NULL,
    [Bundles] decimal(18,0)  NULL,
    [CS_BS] nchar(10)  NULL,
    [Accepted] nchar(10)  NULL,
    [Status] decimal(18,0)  NULL,
    [CustomerName] char(100)  NOT NULL,
    [Pk_Order] decimal(18,0)  NOT NULL,
    [Name] nvarchar(500)  NOT NULL,
    [PName] nvarchar(50)  NULL,
    [BType] nvarchar(50)  NOT NULL,
    [State] nvarchar(255)  NULL
);
GO

-- Creating table 'Vw_Sch_Ord_Pending'
CREATE TABLE [dbo].[Vw_Sch_Ord_Pending] (
    [TotSchQty] decimal(38,0)  NULL,
    [TotPendingQty] decimal(38,0)  NULL,
    [Pk_Order] decimal(18,0)  NOT NULL,
    [OrdQty] decimal(18,0)  NULL
);
GO

-- Creating table 'Vw_BoxStock'
CREATE TABLE [dbo].[Vw_BoxStock] (
    [Pk_BoxID] decimal(18,0)  NOT NULL,
    [Name] nvarchar(500)  NOT NULL,
    [Pk_StockID] decimal(18,0)  NOT NULL,
    [Quantity] decimal(18,0)  NULL,
    [PartNo] varchar(50)  NULL,
    [Expr1] nvarchar(50)  NOT NULL,
    [CustomerName] char(100)  NOT NULL,
    [PName] nvarchar(50)  NULL,
    [Pk_PartPropertyID] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'Vw_TSchQty'
CREATE TABLE [dbo].[Vw_TSchQty] (
    [TQty] decimal(38,0)  NULL,
    [Pk_BoxID] decimal(18,0)  NOT NULL,
    [Pk_Order] decimal(18,0)  NOT NULL,
    [Fk_BoxID] decimal(18,0)  NULL,
    [Fk_PartID] decimal(18,0)  NULL
);
GO

-- Creating table 'MaterialInwardD'
CREATE TABLE [dbo].[MaterialInwardD] (
    [Pk_InwardDet] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Inward] decimal(18,0)  NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [Quantity] decimal(18,0)  NULL,
    [Price] decimal(18,0)  NULL,
    [RollNo] nvarchar(50)  NULL,
    [Fk_Mill] decimal(18,0)  NULL,
    [Remarks] nvarchar(500)  NULL,
    [TaxVal] decimal(18,0)  NULL,
    [HSNCode] nvarchar(50)  NULL
);
GO

-- Creating table 'Vw_UnScheduled_Orders'
CREATE TABLE [dbo].[Vw_UnScheduled_Orders] (
    [Pk_DeliverySechedule] decimal(18,0)  NULL,
    [Pk_Order] decimal(18,0)  NOT NULL,
    [OrderDate] datetime  NOT NULL,
    [CustomerName] char(100)  NOT NULL,
    [OrdQty] decimal(18,0)  NULL,
    [Name] nvarchar(500)  NOT NULL
);
GO

-- Creating table 'Vw_UnSchOrders'
CREATE TABLE [dbo].[Vw_UnSchOrders] (
    [Pk_Order] decimal(18,0)  NOT NULL,
    [OrderDate] datetime  NOT NULL,
    [CustomerName] char(100)  NOT NULL,
    [Fk_Customer] decimal(18,0)  NULL,
    [Cust_PO] nvarchar(50)  NULL
);
GO

-- Creating table 'Vw_SchAndPrd'
CREATE TABLE [dbo].[Vw_SchAndPrd] (
    [Pk_Order] decimal(18,0)  NULL,
    [OrderDate] datetime  NULL,
    [CustomerName] char(100)  NULL,
    [OrdQty] decimal(18,0)  NULL,
    [SchQty] decimal(18,0)  NULL,
    [Pk_DeliverySechedule] decimal(18,0)  NULL,
    [Pk_JobCardID] decimal(18,0)  NOT NULL,
    [PlanQty] decimal(18,0)  NOT NULL,
    [ProcessName] nvarchar(150)  NULL,
    [PrdnQty] decimal(18,0)  NULL,
    [Name] nvarchar(500)  NULL,
    [Pk_BoxID] decimal(18,0)  NULL,
    [Pk_PartPropertyID] decimal(18,0)  NULL,
    [PName] nvarchar(50)  NULL,
    [ActQty] decimal(18,0)  NULL
);
GO

-- Creating table 'Vw_BoxDet'
CREATE TABLE [dbo].[Vw_BoxDet] (
    [Name] nvarchar(500)  NOT NULL,
    [Length] decimal(18,0)  NULL,
    [Width] decimal(18,0)  NULL,
    [Height] decimal(18,0)  NULL,
    [Weight] decimal(18,3)  NULL,
    [GSM] decimal(18,0)  NULL,
    [BF] decimal(18,0)  NULL,
    [Pk_PartPropertyID] decimal(18,0)  NOT NULL,
    [Pk_LayerID] decimal(18,0)  NOT NULL,
    [Pk_BoxID] decimal(18,0)  NOT NULL,
    [CustomerName] char(100)  NOT NULL,
    [OuterShellYes] bit  NULL,
    [CapYes] bit  NULL,
    [LenghtPartationYes] bit  NULL,
    [WidthPartationYes] bit  NULL,
    [PlateYes] bit  NULL,
    [Pk_BoxSpecID] decimal(18,0)  NOT NULL,
    [MaterialName] nvarchar(250)  NULL,
    [BType] nvarchar(50)  NOT NULL,
    [BoardArea] decimal(18,2)  NULL,
    [Deckle] decimal(18,2)  NULL,
    [CuttingSize] decimal(18,2)  NULL,
    [PName] nvarchar(50)  NULL,
    [BoardBS] decimal(18,2)  NULL,
    [BoardGSM] decimal(18,2)  NULL,
    [TakeUpFactor] decimal(18,2)  NULL,
    [FluteName] varchar(50)  NULL,
    [FluteTKF] decimal(18,2)  NULL,
    [LayerTKF] decimal(18,2)  NULL,
    [ColorName] nvarchar(30)  NULL,
    [Quantity] int  NULL,
    [PlyVal] decimal(18,0)  NULL,
    [LayerWt] decimal(18,3)  NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Pk_Customer] decimal(18,0)  NOT NULL,
    [Pk_BoxTypeID] decimal(18,0)  NOT NULL,
    [Description] nvarchar(500)  NULL,
    [PartNo] varchar(50)  NULL,
    [Pk_FluteID] decimal(18,0)  NOT NULL,
    [NoBoards] decimal(18,2)  NULL
);
GO

-- Creating table 'ItemPartProperty'
CREATE TABLE [dbo].[ItemPartProperty] (
    [Pk_PartPropertyID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_BoxSpecsID] decimal(18,0)  NOT NULL,
    [PName] nvarchar(50)  NULL,
    [Length] decimal(18,2)  NULL,
    [Width] decimal(18,2)  NULL,
    [Height] decimal(18,2)  NULL,
    [Weight] decimal(18,3)  NULL,
    [Quantity] int  NULL,
    [NoBoards] decimal(18,2)  NULL,
    [BoardArea] decimal(18,2)  NULL,
    [Deckle] decimal(18,2)  NULL,
    [CuttingSize] decimal(18,2)  NULL,
    [Rate] decimal(18,2)  NULL,
    [AddBLength] decimal(18,2)  NULL,
    [TakeUpFactor] decimal(18,2)  NULL,
    [PlyVal] decimal(18,0)  NULL,
    [OLength1] int  NULL,
    [OWidth1] int  NULL,
    [OHeight1] int  NULL,
    [BoardGSM] decimal(18,2)  NULL,
    [BoardBS] decimal(18,2)  NULL
);
GO

-- Creating table 'Vw_JobProcessSteps'
CREATE TABLE [dbo].[Vw_JobProcessSteps] (
    [Pk_JobCardID] decimal(18,0)  NOT NULL,
    [Pk_ID] decimal(18,0)  NOT NULL,
    [ProcessName] nvarchar(150)  NULL,
    [Quantity] decimal(18,0)  NULL,
    [WastageQty] decimal(18,2)  NULL,
    [ProcessDate] datetime  NULL,
    [PQuantity] decimal(18,0)  NULL,
    [PSTime] nchar(10)  NULL,
    [PETime] nchar(10)  NULL,
    [WQty] decimal(18,2)  NULL,
    [JDate] datetime  NULL,
    [CustomerName] char(100)  NOT NULL,
    [Name] nvarchar(500)  NOT NULL,
    [PName] nvarchar(50)  NULL,
    [Fk_Order] decimal(18,0)  NULL,
    [TotalQty] decimal(18,0)  NOT NULL,
    [Fk_Status] decimal(18,0)  NULL,
    [RemainingQty] decimal(18,0)  NULL
);
GO

-- Creating table 'MatInwD'
CREATE TABLE [dbo].[MatInwD] (
    [Pk_InwDet] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Inwd] decimal(18,0)  NULL,
    [Fk_BoxID] decimal(18,0)  NULL,
    [Quantity] decimal(18,0)  NULL,
    [Price] decimal(18,0)  NULL,
    [Remarks] nvarchar(500)  NULL,
    [TaxVal] decimal(18,0)  NULL
);
GO

-- Creating table 'MatInwM'
CREATE TABLE [dbo].[MatInwM] (
    [Pk_Inwd] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Inward_Date] datetime  NULL,
    [Fk_InwdBy] decimal(18,0)  NOT NULL,
    [PONo] decimal(18,0)  NULL,
    [Pur_InvNo] nvarchar(50)  NULL,
    [Pur_InvDate] datetime  NULL,
    [Fk_Vendor] decimal(18,0)  NULL
);
GO

-- Creating table 'Vw_BoxSpec'
CREATE TABLE [dbo].[Vw_BoxSpec] (
    [Name] nvarchar(500)  NOT NULL,
    [Length] decimal(18,2)  NULL,
    [Width] decimal(18,2)  NULL,
    [Height] decimal(18,2)  NULL,
    [Weight] decimal(18,3)  NULL,
    [Pk_PartPropertyID] decimal(18,0)  NOT NULL,
    [Pk_BoxID] decimal(18,0)  NOT NULL,
    [BType] nvarchar(50)  NOT NULL,
    [PName] nvarchar(50)  NULL,
    [Pk_BoxSpecID] decimal(18,0)  NOT NULL,
    [CustomerName] char(100)  NOT NULL,
    [Quantity] int  NULL,
    [Pk_Customer] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'Vw_Sales_Details'
CREATE TABLE [dbo].[Vw_Sales_Details] (
    [Pk_Invoice] decimal(18,0)  NOT NULL,
    [InvDate] datetime  NULL,
    [CustomerName] char(100)  NOT NULL,
    [Quantity] decimal(18,0)  NULL,
    [Price] decimal(18,2)  NULL,
    [GrandTotal] decimal(18,2)  NULL,
    [NETVALUE] decimal(18,2)  NULL,
    [TaxType] char(10)  NULL,
    [Fk_OrderNo] decimal(18,0)  NOT NULL,
    [PayMode] nvarchar(50)  NULL,
    [Pk_Inv_Details] decimal(18,0)  NOT NULL,
    [Pk_Customer] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'Vw_OrderList'
CREATE TABLE [dbo].[Vw_OrderList] (
    [Pk_Order] decimal(18,0)  NOT NULL,
    [OrderDate] datetime  NOT NULL,
    [Product] nvarchar(500)  NULL,
    [CustomerName] char(100)  NOT NULL,
    [Pk_Customer] decimal(18,0)  NOT NULL,
    [Name] nvarchar(500)  NOT NULL,
    [PName] nvarchar(50)  NULL,
    [OrdQty] decimal(18,0)  NULL,
    [Rate] decimal(18,2)  NULL,
    [Pk_OrderChild] decimal(18,0)  NOT NULL,
    [Cust_PO] nvarchar(50)  NULL
);
GO

-- Creating table 'Gen_OrderChild'
CREATE TABLE [dbo].[Gen_OrderChild] (
    [Pk_OrderChild] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_OrderID] decimal(18,0)  NULL,
    [Fk_BoxID] decimal(18,0)  NULL,
    [OrdQty] decimal(18,0)  NULL,
    [EnqQty] decimal(18,0)  NULL,
    [Ddate] datetime  NULL,
    [Fk_Status] decimal(18,0)  NULL,
    [Fk_PartID] decimal(18,0)  NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [Rate] decimal(18,2)  NULL,
    [Rollno] varchar(50)  NULL
);
GO

-- Creating table 'Vw_Others_OrderList'
CREATE TABLE [dbo].[Vw_Others_OrderList] (
    [MatName] nvarchar(250)  NULL,
    [CatName] nvarchar(30)  NULL,
    [Fk_MaterialCategory] decimal(18,0)  NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [CustomerName] char(100)  NOT NULL,
    [Pk_Order] decimal(18,0)  NOT NULL,
    [Fk_Customer] decimal(18,0)  NULL,
    [OrderDate] datetime  NOT NULL,
    [OrdQty] decimal(18,0)  NULL
);
GO

-- Creating table 'Vw_Others_Order'
CREATE TABLE [dbo].[Vw_Others_Order] (
    [MaterialName] nvarchar(250)  NULL,
    [CatName] nvarchar(30)  NULL,
    [Fk_MaterialCategory] decimal(18,0)  NULL,
    [Pk_Material] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'Vw_OthersBill'
CREATE TABLE [dbo].[Vw_OthersBill] (
    [MaterialName] nvarchar(250)  NULL,
    [CatName] nvarchar(30)  NULL,
    [Quantity] decimal(18,2)  NULL,
    [Fk_MaterialCategory] decimal(18,0)  NULL,
    [Pk_Material] decimal(18,0)  NULL,
    [Pk_Stock] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'Vw_Age'
CREATE TABLE [dbo].[Vw_Age] (
    [RollNo] varchar(50)  NULL,
    [Pk_PaperStock] decimal(18,0)  NOT NULL,
    [Name] nvarchar(250)  NULL,
    [Quantity] decimal(18,0)  NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Age] int  NULL,
    [GSM] decimal(20,0)  NULL,
    [BF] decimal(20,0)  NULL,
    [Deckle] decimal(20,2)  NULL,
    [ColorName] nvarchar(30)  NULL,
    [MillName] nvarchar(150)  NULL
);
GO

-- Creating table 'PurchaseOrderM'
CREATE TABLE [dbo].[PurchaseOrderM] (
    [Pk_PONo] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [PODate] datetime  NULL,
    [Fk_Vendor] decimal(18,0)  NULL,
    [Fk_Status] decimal(18,0)  NULL,
    [TaxType] nchar(10)  NULL,
    [ED] decimal(18,0)  NULL,
    [GrandTotal] decimal(18,0)  NULL,
    [NetValue] decimal(18,0)  NULL,
    [Fk_Indent] decimal(18,0)  NULL,
    [CGST] decimal(18,2)  NULL,
    [SGST] decimal(18,2)  NULL,
    [DeliveryAdd] nvarchar(max)  NULL,
    [PkDisp] varchar(255)  NULL,
    [Two] nvarchar(100)  NULL,
    [Four] nvarchar(100)  NULL,
    [Five] nvarchar(100)  NULL,
    [Six] nvarchar(100)  NULL,
    [Seven] nvarchar(100)  NULL,
    [Comments] nvarchar(500)  NULL
);
GO

-- Creating table 'SampMaterialIssue'
CREATE TABLE [dbo].[SampMaterialIssue] (
    [Pk_MaterialIssueID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [IssueDate] datetime  NULL,
    [Fk_UserID] decimal(18,0)  NULL,
    [Fk_Tanent] decimal(18,0)  NULL,
    [DCNo] varchar(50)  NULL,
    [Fk_Branch] decimal(18,0)  NULL,
    [Fk_JobCardID] decimal(18,0)  NULL
);
GO

-- Creating table 'SampMaterialIssueDetails'
CREATE TABLE [dbo].[SampMaterialIssueDetails] (
    [Pk_MaterialIssueDetailsID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_IssueID] decimal(18,0)  NOT NULL,
    [Fk_Material] decimal(18,0)  NOT NULL,
    [Quantity] decimal(18,0)  NOT NULL,
    [ReturnQuantity] decimal(18,0)  NULL,
    [Weight] decimal(18,0)  NULL,
    [Pk_StockID] decimal(18,0)  NULL,
    [RollNo] nvarchar(50)  NULL,
    [Fk_Mill] decimal(18,0)  NULL
);
GO

-- Creating table 'Vw_Samp_MatIssue'
CREATE TABLE [dbo].[Vw_Samp_MatIssue] (
    [Pk_BoxID] decimal(18,0)  NOT NULL,
    [Name] nvarchar(500)  NOT NULL,
    [Pk_JobCardID] decimal(18,0)  NOT NULL,
    [JDate] datetime  NULL,
    [Pk_MaterialIssueID] decimal(18,0)  NOT NULL,
    [IssueDate] datetime  NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [MatName] nvarchar(250)  NULL,
    [MillName] nvarchar(150)  NULL,
    [ColorName] nvarchar(30)  NULL,
    [Quantity] decimal(18,0)  NOT NULL,
    [RollNo] nvarchar(50)  NULL
);
GO

-- Creating table 'Vw_JCReport'
CREATE TABLE [dbo].[Vw_JCReport] (
    [Pk_BoxID] decimal(18,0)  NOT NULL,
    [BName] nvarchar(500)  NOT NULL,
    [Description] nvarchar(500)  NULL,
    [Pk_Order] decimal(18,0)  NOT NULL,
    [OrderDate] datetime  NOT NULL,
    [Product] nvarchar(500)  NULL,
    [OrdQty] int  NULL,
    [Fk_BoxID] decimal(18,0)  NULL,
    [Cust_PO] nvarchar(50)  NULL,
    [CustomerName] char(100)  NOT NULL,
    [CuttingSize] decimal(18,2)  NULL,
    [PartTakeup] decimal(18,2)  NULL,
    [PName] nvarchar(50)  NULL,
    [Length] decimal(2,0)  NULL,
    [Width] decimal(2,0)  NULL,
    [Height] decimal(2,0)  NULL,
    [TotalQty] int  NULL,
    [Deckle] decimal(18,2)  NULL,
    [BoardGSM] decimal(18,2)  NULL,
    [BoardBS] decimal(18,2)  NULL,
    [PlyVal] decimal(18,0)  NULL,
    [FluteHt] decimal(18,2)  NULL,
    [FluteName] varchar(50)  NULL,
    [GSM] decimal(18,0)  NULL,
    [BF] decimal(18,0)  NULL,
    [ColorName] nvarchar(50)  NULL,
    [Weight] decimal(18,3)  NULL,
    [SchQty] decimal(18,0)  NULL,
    [DeliveryDate] datetime  NOT NULL,
    [Tk_Factor] decimal(18,2)  NULL,
    [BoardArea] decimal(18,2)  NULL,
    [Pk_JobCardID] decimal(18,0)  NOT NULL,
    [JDate] datetime  NULL,
    [Printing] nvarchar(50)  NULL,
    [Calico] bit  NULL,
    [Others] varchar(500)  NULL,
    [Invno] varchar(50)  NULL,
    [Corrugation] varchar(50)  NULL,
    [TopPaperQty] decimal(18,0)  NULL,
    [TwoPlyQty] decimal(18,0)  NULL,
    [TwoPlyWt] decimal(18,0)  NULL,
    [PastingQty] decimal(18,0)  NULL,
    [RotaryQty] decimal(18,0)  NULL,
    [PunchingQty] decimal(18,0)  NULL,
    [SlotingQty] decimal(18,0)  NULL,
    [PinningQty] decimal(18,0)  NULL,
    [FinishingQty] decimal(18,0)  NULL,
    [ProdQty] decimal(18,0)  NOT NULL,
    [Machine] decimal(18,0)  NULL,
    [ChkQuality] decimal(18,0)  NULL,
    [PColor] nvarchar(50)  NULL,
    [PDetails] nvarchar(150)  NULL,
    [MFG] nvarchar(50)  NULL,
    [DPRSc] nvarchar(50)  NULL,
    [ECCQty] decimal(18,0)  NULL,
    [GummingQty] decimal(18,0)  NULL,
    [UpsVal] decimal(18,2)  NULL,
    [CutLength] decimal(18,2)  NULL,
    [Stitching] bit  NULL,
    [MillName] nvarchar(150)  NULL,
    [Pk_LayerID] decimal(18,0)  NOT NULL,
    [Rate] decimal(18,2)  NULL,
    [Corr] nchar(10)  NULL,
    [TopP] nchar(10)  NULL,
    [Rotary] nchar(10)  NULL,
    [Pasting] nchar(10)  NULL,
    [PrintingP] nchar(10)  NULL,
    [Punching] nchar(10)  NULL,
    [Slotting] nchar(10)  NULL,
    [Finishing] nchar(10)  NULL,
    [Pinning] nchar(10)  NULL,
    [Gumming] nchar(10)  NULL,
    [Bundling] nchar(10)  NULL
);
GO

-- Creating table 'Vw_IssuesDev'
CREATE TABLE [dbo].[Vw_IssuesDev] (
    [Pk_BoxID] decimal(18,0)  NOT NULL,
    [Pk_LayerID] decimal(18,0)  NOT NULL,
    [GSM] decimal(18,0)  NULL,
    [BF] decimal(18,0)  NULL,
    [BoxMat] nvarchar(250)  NULL,
    [Pk_PartPropertyID] decimal(18,0)  NOT NULL,
    [PName] nvarchar(50)  NULL,
    [Pk_JobCardID] decimal(18,0)  NOT NULL,
    [Pk_MaterialIssueID] decimal(18,0)  NULL,
    [Pk_MaterialIssueDetailsID] decimal(18,0)  NULL,
    [IssueMat] nvarchar(250)  NULL,
    [IssuePk] decimal(18,0)  NULL,
    [BoxPk] decimal(18,0)  NOT NULL,
    [Name] nvarchar(500)  NOT NULL
);
GO

-- Creating table 'Vw_PapDistIssueList'
CREATE TABLE [dbo].[Vw_PapDistIssueList] (
    [qty] decimal(38,0)  NULL,
    [Pk_MaterialIssueID] decimal(18,0)  NOT NULL,
    [IssueDate] datetime  NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Name] nvarchar(250)  NULL,
    [Pk_JobCardID] decimal(18,0)  NOT NULL,
    [JDate] datetime  NULL,
    [Fk_Order] decimal(18,0)  NULL,
    [CustomerName] char(100)  NOT NULL,
    [Quantity] decimal(18,0)  NOT NULL,
    [RollNo] nvarchar(50)  NULL,
    [MillName] nvarchar(150)  NULL,
    [ColorName] nvarchar(30)  NULL,
    [Pk_BoxID] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'Vw_Upd_Stock_Jobprocess'
CREATE TABLE [dbo].[Vw_Upd_Stock_Jobprocess] (
    [StockUpd] decimal(18,0)  NULL,
    [ProcessName] nvarchar(150)  NULL,
    [Fk_JobCardID] decimal(18,0)  NULL,
    [Pk_Process] decimal(18,0)  NOT NULL,
    [Pk_ID] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'SampJobProcess'
CREATE TABLE [dbo].[SampJobProcess] (
    [Pk_ID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_ProcessID] decimal(18,0)  NULL,
    [Fk_JobCardID] decimal(18,0)  NULL,
    [Quantity] decimal(18,0)  NULL,
    [WastageQty] decimal(18,2)  NULL,
    [ProcessDate] datetime  NULL,
    [PQuantity] decimal(18,0)  NULL,
    [ReasonWastage] nvarchar(250)  NULL,
    [RemainingQty] decimal(18,0)  NULL,
    [WQty] decimal(18,2)  NULL,
    [PSTime] nchar(10)  NULL,
    [PETime] nchar(10)  NULL,
    [R1Wastage] nvarchar(250)  NULL,
    [R2Wastage] nvarchar(250)  NULL,
    [R3Wastage] nvarchar(250)  NULL,
    [R4Wastage] nvarchar(250)  NULL,
    [StockUpd] decimal(18,0)  NULL
);
GO

-- Creating table 'SampJProcess'
CREATE TABLE [dbo].[SampJProcess] (
    [Pk_JProcessID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_ProcessID] decimal(18,0)  NULL,
    [Fk_JobCardID] decimal(18,0)  NULL
);
GO

-- Creating table 'Vw_Iss_Return'
CREATE TABLE [dbo].[Vw_Iss_Return] (
    [Pk_IssueReturnMasterId] decimal(18,0)  NOT NULL,
    [IssueReturnDate] datetime  NULL,
    [Fk_IssueId] decimal(18,0)  NULL,
    [IssueDate] datetime  NULL,
    [Name] nvarchar(250)  NULL,
    [ReturnQuantity] decimal(18,0)  NULL,
    [RollNo] nvarchar(50)  NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [ColorName] nvarchar(30)  NULL,
    [MillName] nvarchar(150)  NULL,
    [Pk_IssueReturnDetailsId] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'Vw_OpenPO'
CREATE TABLE [dbo].[Vw_OpenPO] (
    [Pk_Material] decimal(18,0)  NULL,
    [Name] nvarchar(250)  NULL,
    [Quantity] decimal(18,0)  NULL,
    [Pk_PONo] decimal(18,0)  NOT NULL,
    [Rate] decimal(18,2)  NULL,
    [Amount] decimal(18,2)  NULL,
    [Fk_Status] decimal(18,0)  NULL,
    [InwdQty] decimal(18,0)  NULL,
    [VendorName] char(50)  NOT NULL,
    [GSM] decimal(20,0)  NOT NULL,
    [BF] decimal(20,0)  NOT NULL,
    [Pk_Vendor] decimal(18,0)  NOT NULL,
    [PODate] datetime  NULL,
    [ColorName] nvarchar(30)  NULL,
    [MillName] nvarchar(150)  NULL,
    [Fk_IndentVal] decimal(18,0)  NULL,
    [Pk_Mill] decimal(18,0)  NOT NULL,
    [Deckle] decimal(20,2)  NOT NULL,
    [UnitName] nvarchar(50)  NULL,
    [CatName] nvarchar(30)  NULL,
    [Pk_PODet] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'Vw_JWStock'
CREATE TABLE [dbo].[Vw_JWStock] (
    [StockValue] decimal(18,0)  NULL,
    [RollNo] varchar(50)  NULL,
    [CustomerName] char(100)  NOT NULL,
    [Name] nvarchar(250)  NULL,
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Pk_StockID] decimal(18,0)  NOT NULL,
    [GSM] decimal(20,0)  NULL,
    [BF] decimal(20,0)  NULL,
    [Deckle] decimal(20,2)  NULL,
    [Pk_Customer] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'Inv_Billing'
CREATE TABLE [dbo].[Inv_Billing] (
    [Pk_Invoice] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Invno] varchar(50)  NULL,
    [InvDate] datetime  NULL,
    [Fk_OrderNo] decimal(18,0)  NOT NULL,
    [Fk_Customer] decimal(18,0)  NULL,
    [PayMode] nvarchar(50)  NULL,
    [GrandTotal] decimal(18,2)  NULL,
    [COA] nvarchar(50)  NULL,
    [ESUGAM] nvarchar(50)  NULL,
    [NETVALUE] decimal(18,2)  NULL,
    [FORM_CT3] bit  NULL,
    [CST] bit  NULL,
    [FORM_H] bit  NULL,
    [TaxType] char(10)  NULL,
    [ED] decimal(18,0)  NULL,
    [Buyers_OrderNo] nvarchar(50)  NULL,
    [BOrderDated] datetime  NULL,
    [DNTimeofInv] datetime  NULL,
    [MVehicleNo] nvarchar(50)  NULL,
    [DNTimeofRemoval] datetime  NULL,
    [DeliveryName] nvarchar(150)  NULL,
    [DeliveryAdd1] nvarchar(500)  NULL,
    [DeliveryAdd2] nvarchar(500)  NULL,
    [StateCode] nvarchar(50)  NULL,
    [Cancel] bit  NULL,
    [Despatched] nvarchar(150)  NULL,
    [Destination] nvarchar(150)  NULL
);
GO

-- Creating table 'JobProcess'
CREATE TABLE [dbo].[JobProcess] (
    [Pk_ID] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_ProcessID] decimal(18,0)  NULL,
    [Fk_JobCardID] decimal(18,0)  NULL,
    [Quantity] decimal(18,0)  NULL,
    [WastageQty] decimal(18,2)  NULL,
    [ProcessDate] datetime  NULL,
    [PQuantity] decimal(18,0)  NULL,
    [ReasonWastage] nvarchar(250)  NULL,
    [RemainingQty] decimal(18,0)  NULL,
    [WQty] decimal(18,2)  NULL,
    [PSTime] nchar(10)  NULL,
    [PETime] nchar(10)  NULL,
    [R1Wastage] nvarchar(250)  NULL,
    [R2Wastage] nvarchar(250)  NULL,
    [R3Wastage] nvarchar(250)  NULL,
    [R4Wastage] nvarchar(250)  NULL,
    [StockUpd] decimal(18,0)  NULL,
    [fk_boxid] decimal(18,0)  NULL
);
GO

-- Creating table 'MaterialInwardM'
CREATE TABLE [dbo].[MaterialInwardM] (
    [Pk_Inward] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Inward_Date] datetime  NULL,
    [Fk_InwardBy] decimal(18,0)  NOT NULL,
    [Fk_QC] decimal(18,0)  NULL,
    [Fk_Indent] decimal(18,0)  NULL,
    [PONo] decimal(18,0)  NULL,
    [Pur_InvNo] nvarchar(50)  NULL,
    [Pur_InvDate] datetime  NULL,
    [Remarks] nvarchar(500)  NULL
);
GO

-- Creating table 'Vw_OthersMaterial'
CREATE TABLE [dbo].[Vw_OthersMaterial] (
    [Pk_Material] decimal(18,0)  NOT NULL,
    [Name] nvarchar(250)  NULL,
    [ColorName] nvarchar(30)  NULL,
    [UnitName] nvarchar(50)  NULL,
    [CatName] nvarchar(30)  NULL
);
GO

-- Creating table 'Inv_BillingDetails'
CREATE TABLE [dbo].[Inv_BillingDetails] (
    [Pk_Inv_Details] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Invoice] decimal(18,0)  NULL,
    [Quantity] decimal(18,0)  NULL,
    [Price] decimal(18,2)  NULL,
    [Fk_TaxID] decimal(18,0)  NULL,
    [Discount] decimal(18,2)  NULL,
    [NetAmount] decimal(18,2)  NULL,
    [Description] nvarchar(500)  NULL,
    [TotalAmount] decimal(18,2)  NULL,
    [TaxValue] decimal(18,2)  NULL,
    [Fk_BoxID] decimal(18,0)  NULL,
    [Fk_PartID] decimal(18,0)  NULL,
    [HsnCode] nvarchar(50)  NULL,
    [UnitVal] nchar(10)  NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [Bundles] nvarchar(100)  NULL
);
GO

-- Creating table 'VwInvoice'
CREATE TABLE [dbo].[VwInvoice] (
    [CustomerName] char(100)  NOT NULL,
    [Invno] varchar(50)  NULL,
    [InvDate] datetime  NULL,
    [Fk_OrderNo] decimal(18,0)  NOT NULL,
    [Quantity] decimal(18,0)  NULL,
    [Price] decimal(18,2)  NULL,
    [Discount] decimal(18,2)  NULL,
    [NetAmount] decimal(18,2)  NULL,
    [Description] nvarchar(500)  NULL,
    [Pk_Invoice] decimal(18,0)  NOT NULL,
    [Pk_Inv_Details] decimal(18,0)  NOT NULL,
    [Pk_Customer] decimal(18,0)  NOT NULL,
    [PinCode] decimal(18,0)  NOT NULL,
    [LandLine] decimal(18,0)  NOT NULL,
    [CustomerAddress] nvarchar(500)  NOT NULL,
    [COA] nvarchar(50)  NULL,
    [ESUGAM] nvarchar(50)  NULL,
    [CreditLimit] nchar(10)  NULL,
    [VAT] nvarchar(50)  NULL,
    [PAN] nvarchar(50)  NULL,
    [Regn] nvarchar(50)  NULL,
    [GrandTotal] decimal(18,2)  NULL,
    [NETVALUE] decimal(18,2)  NULL,
    [FORM_CT3] bit  NULL,
    [FORM_H] bit  NULL,
    [TaxType] char(10)  NULL,
    [Buyers_OrderNo] nvarchar(50)  NULL,
    [BOrderDated] datetime  NULL,
    [DNTimeofInv] datetime  NULL,
    [MVehicleNo] nvarchar(50)  NULL,
    [DNTimeofRemoval] datetime  NULL,
    [DeliveryName] nvarchar(150)  NULL,
    [DeliveryAdd1] nvarchar(500)  NULL,
    [DeliveryAdd2] nvarchar(500)  NULL,
    [HsnCode] nvarchar(50)  NULL,
    [StateCode] nvarchar(50)  NULL,
    [StateName] varchar(50)  NOT NULL,
    [CityName] varchar(50)  NOT NULL,
    [CountryName] varchar(50)  NOT NULL,
    [Name] nvarchar(500)  NOT NULL,
    [TaxName] varchar(50)  NULL,
    [Expr1] decimal(18,2)  NULL,
    [Fk_PartID] decimal(18,0)  NULL,
    [TaxValue] decimal(18,2)  NULL,
    [PName] nvarchar(50)  NULL,
    [Length] decimal(18,2)  NULL,
    [Width] decimal(18,2)  NULL,
    [Height] decimal(18,2)  NULL,
    [CreditAmt] decimal(18,2)  NULL,
    [Pk_PartPropertyID] decimal(18,0)  NOT NULL,
    [UnitVal] nchar(10)  NULL,
    [TotalAmount] decimal(18,2)  NULL,
    [Pk_BoxID] decimal(18,0)  NOT NULL,
    [Email] nvarchar(50)  NOT NULL,
    [ContactPerson] char(50)  NOT NULL,
    [CST] bit  NULL,
    [ED] decimal(18,0)  NULL,
    [PayMode] nvarchar(50)  NULL,
    [Weight] decimal(18,3)  NULL,
    [Despatched] nvarchar(150)  NULL,
    [Destination] nvarchar(150)  NULL,
    [Bundles] nvarchar(100)  NULL
);
GO

-- Creating table 'Vw_POReport'
CREATE TABLE [dbo].[Vw_POReport] (
    [Pk_Material] decimal(18,0)  NULL,
    [Name] nvarchar(250)  NULL,
    [Quantity] decimal(18,0)  NULL,
    [Pk_PONo] decimal(18,0)  NOT NULL,
    [PODate] datetime  NULL,
    [Rate] decimal(18,2)  NULL,
    [Amount] decimal(18,2)  NULL,
    [VendorName] char(50)  NOT NULL,
    [GrandTotal] decimal(18,0)  NULL,
    [NetValue] decimal(18,0)  NULL,
    [TaxType] nchar(10)  NULL,
    [Fk_Indent] decimal(18,0)  NULL,
    [MillName] nvarchar(150)  NULL,
    [ColorName] nvarchar(30)  NULL,
    [Fk_IndentVal] decimal(18,0)  NULL,
    [Email] nvarchar(50)  NOT NULL,
    [Address_No_Street] nvarchar(100)  NOT NULL,
    [PinCode] decimal(18,0)  NOT NULL,
    [LandLine] decimal(18,0)  NULL,
    [MobileNumber] decimal(18,0)  NULL,
    [PkDisp] varchar(255)  NULL,
    [OfficeContactNumber] decimal(30,0)  NOT NULL,
    [Two] nvarchar(100)  NULL,
    [Four] nvarchar(100)  NULL,
    [Five] nvarchar(100)  NULL,
    [Six] nvarchar(100)  NULL,
    [Seven] nvarchar(100)  NULL,
    [HSNCode] nvarchar(50)  NULL,
    [UnitName] nvarchar(50)  NULL,
    [Pk_PODet] decimal(18,0)  NOT NULL,
    [CGST] decimal(18,2)  NULL,
    [SGST] decimal(18,2)  NULL,
    [Comments] nvarchar(500)  NULL
);
GO

-- Creating table 'Vw_BoxOrder'
CREATE TABLE [dbo].[Vw_BoxOrder] (
    [SchQty] decimal(38,0)  NULL,
    [Pk_Order] decimal(18,0)  NOT NULL,
    [Pk_BoxID] decimal(18,0)  NOT NULL,
    [Name] nvarchar(500)  NOT NULL,
    [Fk_Enquiry] decimal(18,0)  NULL,
    [Pk_OrderChild] decimal(18,0)  NOT NULL,
    [PartNo] varchar(50)  NULL,
    [OrdQty] decimal(18,0)  NULL,
    [EnqQty] decimal(18,0)  NULL,
    [PName] nvarchar(50)  NULL,
    [Quantity] int  NULL,
    [Pk_PartPropertyID] decimal(18,0)  NOT NULL,
    [Fk_Status] decimal(18,0)  NULL,
    [OrderDate] datetime  NOT NULL,
    [CustomerName] char(100)  NOT NULL,
    [Fk_Customer] decimal(18,0)  NULL,
    [Product] nvarchar(500)  NULL,
    [Weight] decimal(18,3)  NULL,
    [Cust_PO] nvarchar(50)  NULL,
    [CustOrdDate] datetime  NULL
);
GO

-- Creating table 'GRN_Details'
CREATE TABLE [dbo].[GRN_Details] (
    [Pk_GRNDetId] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_GRNID] decimal(18,0)  NOT NULL,
    [Fk_Material] decimal(18,0)  NULL,
    [TotQty] decimal(18,0)  NULL,
    [TaxValue] decimal(18,2)  NULL,
    [Rate] decimal(18,2)  NULL,
    [TaxAmt] decimal(18,2)  NULL,
    [HSNCode] nchar(50)  NULL,
    [TotAmt] decimal(18,2)  NULL
);
GO

-- Creating table 'GRN_Mast'
CREATE TABLE [dbo].[GRN_Mast] (
    [Pk_GRNId] decimal(18,0) IDENTITY(1,1) NOT NULL,
    [Fk_Inward] decimal(18,0)  NULL,
    [GRNDate] datetime  NULL
);
GO

-- Creating table 'Vw_GRNMatData'
CREATE TABLE [dbo].[Vw_GRNMatData] (
    [Fk_Material] decimal(18,0)  NOT NULL,
    [TQty] decimal(38,0)  NULL,
    [Pk_Inward] decimal(18,0)  NOT NULL,
    [Name] nvarchar(250)  NULL
);
GO

-- Creating table 'Vw_GrnInwdDet'
CREATE TABLE [dbo].[Vw_GrnInwdDet] (
    [Pk_GRNId] decimal(18,0)  NOT NULL,
    [GRNDate] datetime  NULL,
    [Pk_Inward] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'Vw_GRNMaterialDet'
CREATE TABLE [dbo].[Vw_GRNMaterialDet] (
    [Pk_GRNId] decimal(18,0)  NOT NULL,
    [GRNDate] datetime  NULL,
    [Pk_Inward] decimal(18,0)  NOT NULL,
    [Pk_GRNDetId] decimal(18,0)  NOT NULL,
    [TotQty] decimal(18,0)  NULL,
    [TaxValue] decimal(18,2)  NULL,
    [Rate] decimal(18,2)  NULL,
    [TaxAmt] decimal(18,2)  NULL,
    [HSNCode] nchar(50)  NULL,
    [TotAmt] decimal(18,2)  NULL,
    [Name] nvarchar(250)  NULL
);
GO

-- Creating table 'Vw_InvRM'
CREATE TABLE [dbo].[Vw_InvRM] (
    [CustomerName] char(100)  NOT NULL,
    [Invno] varchar(50)  NULL,
    [InvDate] datetime  NULL,
    [Fk_OrderNo] decimal(18,0)  NOT NULL,
    [Quantity] decimal(18,0)  NULL,
    [Price] decimal(18,2)  NULL,
    [Discount] decimal(18,2)  NULL,
    [NetAmount] decimal(18,2)  NULL,
    [Description] nvarchar(500)  NULL,
    [Pk_Invoice] decimal(18,0)  NOT NULL,
    [Pk_Inv_Details] decimal(18,0)  NOT NULL,
    [Pk_Customer] decimal(18,0)  NOT NULL,
    [PinCode] decimal(18,0)  NOT NULL,
    [LandLine] decimal(18,0)  NOT NULL,
    [CustomerAddress] nvarchar(500)  NOT NULL,
    [COA] nvarchar(50)  NULL,
    [ESUGAM] nvarchar(50)  NULL,
    [CreditLimit] nchar(10)  NULL,
    [VAT] nvarchar(50)  NULL,
    [PAN] nvarchar(50)  NULL,
    [Regn] nvarchar(50)  NULL,
    [GrandTotal] decimal(18,2)  NULL,
    [NETVALUE] decimal(18,2)  NULL,
    [FORM_CT3] bit  NULL,
    [FORM_H] bit  NULL,
    [TaxType] char(10)  NULL,
    [Buyers_OrderNo] nvarchar(50)  NULL,
    [BOrderDated] datetime  NULL,
    [DNTimeofInv] datetime  NULL,
    [MVehicleNo] nvarchar(50)  NULL,
    [DNTimeofRemoval] datetime  NULL,
    [DeliveryName] nvarchar(150)  NULL,
    [DeliveryAdd1] nvarchar(500)  NULL,
    [DeliveryAdd2] nvarchar(500)  NULL,
    [HsnCode] nvarchar(50)  NULL,
    [StateCode] nvarchar(50)  NULL,
    [StateName] varchar(50)  NOT NULL,
    [CityName] varchar(50)  NOT NULL,
    [CountryName] varchar(50)  NOT NULL,
    [TaxName] varchar(50)  NULL,
    [Expr1] decimal(18,2)  NULL,
    [TaxValue] decimal(18,2)  NULL,
    [CreditAmt] decimal(18,2)  NULL,
    [UnitVal] nchar(10)  NULL,
    [TotalAmount] decimal(18,2)  NULL,
    [Email] nvarchar(50)  NOT NULL,
    [ContactPerson] char(50)  NOT NULL,
    [CST] bit  NULL,
    [ED] decimal(18,0)  NULL,
    [PayMode] nvarchar(50)  NULL,
    [Name] nvarchar(250)  NULL,
    [Despatched] nvarchar(150)  NULL,
    [Destination] nvarchar(150)  NULL,
    [Fk_Material] decimal(18,0)  NULL
);
GO

-- Creating table 'Vw_SOrderTracker'
CREATE TABLE [dbo].[Vw_SOrderTracker] (
    [OrdQty] decimal(18,0)  NULL,
    [Pk_Order] decimal(18,0)  NOT NULL,
    [OrderDate] datetime  NOT NULL,
    [Name] nvarchar(500)  NULL,
    [PName] nvarchar(50)  NULL,
    [Pk_Invoice] decimal(18,0)  NOT NULL,
    [InvDate] datetime  NULL,
    [Quantity] decimal(18,0)  NULL,
    [Pk_BoxID] decimal(18,0)  NULL,
    [Cust_PO] nvarchar(50)  NULL,
    [Pk_Inv_Details] decimal(18,0)  NOT NULL,
    [Pk_PartPropertyID] decimal(18,0)  NULL,
    [Fk_BoxID] decimal(18,0)  NULL,
    [Fk_PartID] decimal(18,0)  NULL,
    [CustOrdDate] datetime  NULL
);
GO

-- Creating table 'Vw_SalesOrderTracker'
CREATE TABLE [dbo].[Vw_SalesOrderTracker] (
    [OrdQty] decimal(18,0)  NULL,
    [Pk_Order] decimal(18,0)  NOT NULL,
    [OrderDate] datetime  NOT NULL,
    [Name] nvarchar(500)  NOT NULL,
    [PName] nvarchar(50)  NULL,
    [Pk_Invoice] decimal(18,0)  NOT NULL,
    [InvDate] datetime  NULL,
    [Quantity] decimal(18,0)  NULL,
    [Pk_BoxID] decimal(18,0)  NOT NULL,
    [Cust_PO] nvarchar(50)  NULL,
    [CustOrdDate] datetime  NULL,
    [CustomerName] char(100)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Pk_Estimate] in table 'est_Estimation'
ALTER TABLE [dbo].[est_Estimation]
ADD CONSTRAINT [PK_est_Estimation]
    PRIMARY KEY CLUSTERED ([Pk_Estimate] ASC);
GO

-- Creating primary key on [Pk_DetailedEstimationPart] in table 'est_EstimationPart'
ALTER TABLE [dbo].[est_EstimationPart]
ADD CONSTRAINT [PK_est_EstimationPart]
    PRIMARY KEY CLUSTERED ([Pk_DetailedEstimationPart] ASC);
GO

-- Creating primary key on [Pk_Layer] in table 'est_Layer'
ALTER TABLE [dbo].[est_Layer]
ADD CONSTRAINT [PK_est_Layer]
    PRIMARY KEY CLUSTERED ([Pk_Layer] ASC);
GO

-- Creating primary key on [PK_DetailedEstimate] in table 'C_est_Estimation'
ALTER TABLE [dbo].[C_est_Estimation]
ADD CONSTRAINT [PK_C_est_Estimation]
    PRIMARY KEY CLUSTERED ([PK_DetailedEstimate] ASC);
GO

-- Creating primary key on [Pk_DetailedEstimationPart] in table 'C_est_EstimationPart'
ALTER TABLE [dbo].[C_est_EstimationPart]
ADD CONSTRAINT [PK_C_est_EstimationPart]
    PRIMARY KEY CLUSTERED ([Pk_DetailedEstimationPart] ASC);
GO

-- Creating primary key on [Pk_Layer] in table 'C_est_Layer'
ALTER TABLE [dbo].[C_est_Layer]
ADD CONSTRAINT [PK_C_est_Layer]
    PRIMARY KEY CLUSTERED ([Pk_Layer] ASC);
GO

-- Creating primary key on [Pk_AccountHeadId] in table 'acc_AccountHeads'
ALTER TABLE [dbo].[acc_AccountHeads]
ADD CONSTRAINT [PK_acc_AccountHeads]
    PRIMARY KEY CLUSTERED ([Pk_AccountHeadId] ASC);
GO

-- Creating primary key on [Pk_AccountTypeID] in table 'AccountType'
ALTER TABLE [dbo].[AccountType]
ADD CONSTRAINT [PK_AccountType]
    PRIMARY KEY CLUSTERED ([Pk_AccountTypeID] ASC);
GO

-- Creating primary key on [Pk_RegisterAttendance] in table 'att_RegisterAttendance'
ALTER TABLE [dbo].[att_RegisterAttendance]
ADD CONSTRAINT [PK_att_RegisterAttendance]
    PRIMARY KEY CLUSTERED ([Pk_RegisterAttendance] ASC);
GO

-- Creating primary key on [Pk_RegisterMain] in table 'att_RegisterMain'
ALTER TABLE [dbo].[att_RegisterMain]
ADD CONSTRAINT [PK_att_RegisterMain]
    PRIMARY KEY CLUSTERED ([Pk_RegisterMain] ASC);
GO

-- Creating primary key on [Pk_BankID] in table 'Bank'
ALTER TABLE [dbo].[Bank]
ADD CONSTRAINT [PK_Bank]
    PRIMARY KEY CLUSTERED ([Pk_BankID] ASC);
GO

-- Creating primary key on [Pk_BoxTypeID] in table 'BoxType'
ALTER TABLE [dbo].[BoxType]
ADD CONSTRAINT [PK_BoxType]
    PRIMARY KEY CLUSTERED ([Pk_BoxTypeID] ASC);
GO

-- Creating primary key on [Pk_BranchID] in table 'Branch'
ALTER TABLE [dbo].[Branch]
ADD CONSTRAINT [PK_Branch]
    PRIMARY KEY CLUSTERED ([Pk_BranchID] ASC);
GO

-- Creating primary key on [Pk_ComplaintID] in table 'CustomerComplaints'
ALTER TABLE [dbo].[CustomerComplaints]
ADD CONSTRAINT [PK_CustomerComplaints]
    PRIMARY KEY CLUSTERED ([Pk_ComplaintID] ASC);
GO

-- Creating primary key on [Pk_RejectionId] in table 'CustomerRejections'
ALTER TABLE [dbo].[CustomerRejections]
ADD CONSTRAINT [PK_CustomerRejections]
    PRIMARY KEY CLUSTERED ([Pk_RejectionId] ASC);
GO

-- Creating primary key on [Pk_DepartmentId] in table 'emp_Department'
ALTER TABLE [dbo].[emp_Department]
ADD CONSTRAINT [PK_emp_Department]
    PRIMARY KEY CLUSTERED ([Pk_DepartmentId] ASC);
GO

-- Creating primary key on [Pk_EmployeeDesignationId] in table 'emp_EmployeeDesignation'
ALTER TABLE [dbo].[emp_EmployeeDesignation]
ADD CONSTRAINT [PK_emp_EmployeeDesignation]
    PRIMARY KEY CLUSTERED ([Pk_EmployeeDesignationId] ASC);
GO

-- Creating primary key on [Pk_EmployeeImageId] in table 'emp_EmployeeImage'
ALTER TABLE [dbo].[emp_EmployeeImage]
ADD CONSTRAINT [PK_emp_EmployeeImage]
    PRIMARY KEY CLUSTERED ([Pk_EmployeeImageId] ASC);
GO

-- Creating primary key on [PK_EstimationAddtionalCosts] in table 'est_EstimateAdditionalCost'
ALTER TABLE [dbo].[est_EstimateAdditionalCost]
ADD CONSTRAINT [PK_est_EstimateAdditionalCost]
    PRIMARY KEY CLUSTERED ([PK_EstimationAddtionalCosts] ASC);
GO

-- Creating primary key on [PK_EstimationTaxes] in table 'est_EstimateTax'
ALTER TABLE [dbo].[est_EstimateTax]
ADD CONSTRAINT [PK_est_EstimateTax]
    PRIMARY KEY CLUSTERED ([PK_EstimationTaxes] ASC);
GO

-- Creating primary key on [Pk_FinishedGoodsId] in table 'FinishedGoodsMasterwwws'
ALTER TABLE [dbo].[FinishedGoodsMasterwwws]
ADD CONSTRAINT [PK_FinishedGoodsMasterwwws]
    PRIMARY KEY CLUSTERED ([Pk_FinishedGoodsId] ASC);
GO

-- Creating primary key on [Pk_ShippmentId] in table 'FinishedGoodsShippingDetailswwws'
ALTER TABLE [dbo].[FinishedGoodsShippingDetailswwws]
ADD CONSTRAINT [PK_FinishedGoodsShippingDetailswwws]
    PRIMARY KEY CLUSTERED ([Pk_ShippmentId] ASC);
GO

-- Creating primary key on [Pk_CityID] in table 'gen_City'
ALTER TABLE [dbo].[gen_City]
ADD CONSTRAINT [PK_gen_City]
    PRIMARY KEY CLUSTERED ([Pk_CityID] ASC);
GO

-- Creating primary key on [Pk_Color] in table 'gen_Color'
ALTER TABLE [dbo].[gen_Color]
ADD CONSTRAINT [PK_gen_Color]
    PRIMARY KEY CLUSTERED ([Pk_Color] ASC);
GO

-- Creating primary key on [Pk_Communication] in table 'gen_Communication'
ALTER TABLE [dbo].[gen_Communication]
ADD CONSTRAINT [PK_gen_Communication]
    PRIMARY KEY CLUSTERED ([Pk_Communication] ASC);
GO

-- Creating primary key on [Pk_ContactPersonId] in table 'gen_ContactPerson'
ALTER TABLE [dbo].[gen_ContactPerson]
ADD CONSTRAINT [PK_gen_ContactPerson]
    PRIMARY KEY CLUSTERED ([Pk_ContactPersonId] ASC);
GO

-- Creating primary key on [Pk_Country] in table 'gen_Country'
ALTER TABLE [dbo].[gen_Country]
ADD CONSTRAINT [PK_gen_Country]
    PRIMARY KEY CLUSTERED ([Pk_Country] ASC);
GO

-- Creating primary key on [Pk_CustomerContacts] in table 'gen_CustomerContacts'
ALTER TABLE [dbo].[gen_CustomerContacts]
ADD CONSTRAINT [PK_gen_CustomerContacts]
    PRIMARY KEY CLUSTERED ([Pk_CustomerContacts] ASC);
GO

-- Creating primary key on [Pk_CustomerShippingId] in table 'gen_CustomerShippingDetails'
ALTER TABLE [dbo].[gen_CustomerShippingDetails]
ADD CONSTRAINT [PK_gen_CustomerShippingDetails]
    PRIMARY KEY CLUSTERED ([Pk_CustomerShippingId] ASC);
GO

-- Creating primary key on [Pk_Events] in table 'gen_Events'
ALTER TABLE [dbo].[gen_Events]
ADD CONSTRAINT [PK_gen_Events]
    PRIMARY KEY CLUSTERED ([Pk_Events] ASC);
GO

-- Creating primary key on [Pk_Machine] in table 'gen_Machine'
ALTER TABLE [dbo].[gen_Machine]
ADD CONSTRAINT [PK_gen_Machine]
    PRIMARY KEY CLUSTERED ([Pk_Machine] ASC);
GO

-- Creating primary key on [Pk_MillContacts] in table 'gen_MillContacts'
ALTER TABLE [dbo].[gen_MillContacts]
ADD CONSTRAINT [PK_gen_MillContacts]
    PRIMARY KEY CLUSTERED ([Pk_MillContacts] ASC);
GO

-- Creating primary key on [Pk_PaperType] in table 'gen_PaperType'
ALTER TABLE [dbo].[gen_PaperType]
ADD CONSTRAINT [PK_gen_PaperType]
    PRIMARY KEY CLUSTERED ([Pk_PaperType] ASC);
GO

-- Creating primary key on [Pk_StateID] in table 'gen_State'
ALTER TABLE [dbo].[gen_State]
ADD CONSTRAINT [PK_gen_State]
    PRIMARY KEY CLUSTERED ([Pk_StateID] ASC);
GO

-- Creating primary key on [Pk_VendorContact] in table 'gen_VendorContacts'
ALTER TABLE [dbo].[gen_VendorContacts]
ADD CONSTRAINT [PK_gen_VendorContacts]
    PRIMARY KEY CLUSTERED ([Pk_VendorContact] ASC);
GO

-- Creating primary key on [Pk_ReportID] in table 'GenReports'
ALTER TABLE [dbo].[GenReports]
ADD CONSTRAINT [PK_GenReports]
    PRIMARY KEY CLUSTERED ([Pk_ReportID] ASC);
GO

-- Creating primary key on [Pk_MaterialCategory] in table 'Inv_MaterialCategory'
ALTER TABLE [dbo].[Inv_MaterialCategory]
ADD CONSTRAINT [PK_Inv_MaterialCategory]
    PRIMARY KEY CLUSTERED ([Pk_MaterialCategory] ASC);
GO

-- Creating primary key on [Pk_MaterialOrderMasterId] in table 'Inv_MaterialIndentMaster'
ALTER TABLE [dbo].[Inv_MaterialIndentMaster]
ADD CONSTRAINT [PK_Inv_MaterialIndentMaster]
    PRIMARY KEY CLUSTERED ([Pk_MaterialOrderMasterId] ASC);
GO

-- Creating primary key on [Pk_PaymtNo] in table 'Inv_PaymentDetails'
ALTER TABLE [dbo].[Inv_PaymentDetails]
ADD CONSTRAINT [PK_Inv_PaymentDetails]
    PRIMARY KEY CLUSTERED ([Pk_PaymtNo] ASC);
GO

-- Creating primary key on [Pk_MachineMaintenance] in table 'MachineMaintenances'
ALTER TABLE [dbo].[MachineMaintenances]
ADD CONSTRAINT [PK_MachineMaintenances]
    PRIMARY KEY CLUSTERED ([Pk_MachineMaintenance] ASC);
GO

-- Creating primary key on [Pk_QuotationMaterial] in table 'MaterialQuotationDetails'
ALTER TABLE [dbo].[MaterialQuotationDetails]
ADD CONSTRAINT [PK_MaterialQuotationDetails]
    PRIMARY KEY CLUSTERED ([Pk_QuotationMaterial] ASC);
GO

-- Creating primary key on [Pk_QuotationRequest] in table 'MaterialQuotationMaster'
ALTER TABLE [dbo].[MaterialQuotationMaster]
ADD CONSTRAINT [PK_MaterialQuotationMaster]
    PRIMARY KEY CLUSTERED ([Pk_QuotationRequest] ASC);
GO

-- Creating primary key on [Pk_PaymentID] in table 'Payment'
ALTER TABLE [dbo].[Payment]
ADD CONSTRAINT [PK_Payment]
    PRIMARY KEY CLUSTERED ([Pk_PaymentID] ASC);
GO

-- Creating primary key on [Pk_PaymtID] in table 'PaymentTracks'
ALTER TABLE [dbo].[PaymentTracks]
ADD CONSTRAINT [PK_PaymentTracks]
    PRIMARY KEY CLUSTERED ([Pk_PaymtID] ASC);
GO

-- Creating primary key on [Pk_IndentTrack] in table 'PendingTracks'
ALTER TABLE [dbo].[PendingTracks]
ADD CONSTRAINT [PK_PendingTracks]
    PRIMARY KEY CLUSTERED ([Pk_IndentTrack] ASC);
GO

-- Creating primary key on [Pk_ReceiptID] in table 'Receipt'
ALTER TABLE [dbo].[Receipt]
ADD CONSTRAINT [PK_Receipt]
    PRIMARY KEY CLUSTERED ([Pk_ReceiptID] ASC);
GO

-- Creating primary key on [Pk_TransferID] in table 'StockTransfer'
ALTER TABLE [dbo].[StockTransfer]
ADD CONSTRAINT [PK_StockTransfer]
    PRIMARY KEY CLUSTERED ([Pk_TransferID] ASC);
GO

-- Creating primary key on [diagram_id] in table 'sysdiagrams'
ALTER TABLE [dbo].[sysdiagrams]
ADD CONSTRAINT [PK_sysdiagrams]
    PRIMARY KEY CLUSTERED ([diagram_id] ASC);
GO

-- Creating primary key on [Pk_Vals] in table 'TempChecks'
ALTER TABLE [dbo].[TempChecks]
ADD CONSTRAINT [PK_TempChecks]
    PRIMARY KEY CLUSTERED ([Pk_Vals] ASC);
GO

-- Creating primary key on [Pk_ExpensesID] in table 'Voucher'
ALTER TABLE [dbo].[Voucher]
ADD CONSTRAINT [PK_Voucher]
    PRIMARY KEY CLUSTERED ([Pk_ExpensesID] ASC);
GO

-- Creating primary key on [Pk_State] in table 'wfStates'
ALTER TABLE [dbo].[wfStates]
ADD CONSTRAINT [PK_wfStates]
    PRIMARY KEY CLUSTERED ([Pk_State] ASC);
GO

-- Creating primary key on [Pk_Transaction] in table 'wfTransactions'
ALTER TABLE [dbo].[wfTransactions]
ADD CONSTRAINT [PK_wfTransactions]
    PRIMARY KEY CLUSTERED ([Pk_Transaction] ASC);
GO

-- Creating primary key on [Pk_TriggerParameters] in table 'wfTriggerParameters'
ALTER TABLE [dbo].[wfTriggerParameters]
ADD CONSTRAINT [PK_wfTriggerParameters]
    PRIMARY KEY CLUSTERED ([Pk_TriggerParameters] ASC);
GO

-- Creating primary key on [Pk_Triggers] in table 'wfTriggers'
ALTER TABLE [dbo].[wfTriggers]
ADD CONSTRAINT [PK_wfTriggers]
    PRIMARY KEY CLUSTERED ([Pk_Triggers] ASC);
GO

-- Creating primary key on [Pk_Paths] in table 'wfWorkflowPaths'
ALTER TABLE [dbo].[wfWorkflowPaths]
ADD CONSTRAINT [PK_wfWorkflowPaths]
    PRIMARY KEY CLUSTERED ([Pk_Paths] ASC);
GO

-- Creating primary key on [Pk_WfRole] in table 'wfWorkflowRoles'
ALTER TABLE [dbo].[wfWorkflowRoles]
ADD CONSTRAINT [PK_wfWorkflowRoles]
    PRIMARY KEY CLUSTERED ([Pk_WfRole] ASC);
GO

-- Creating primary key on [Pk_wfRoleUser] in table 'wfWorkflowRoleUsers'
ALTER TABLE [dbo].[wfWorkflowRoleUsers]
ADD CONSTRAINT [PK_wfWorkflowRoleUsers]
    PRIMARY KEY CLUSTERED ([Pk_wfRoleUser] ASC);
GO

-- Creating primary key on [Pk_Tanent] in table 'wgTenant'
ALTER TABLE [dbo].[wgTenant]
ADD CONSTRAINT [PK_wgTenant]
    PRIMARY KEY CLUSTERED ([Pk_Tanent] ASC);
GO

-- Creating primary key on [CustomerName], [Pk_Order], [DeliveryDate] in table 'Vw_Agreed_ActualDelivery'
ALTER TABLE [dbo].[Vw_Agreed_ActualDelivery]
ADD CONSTRAINT [PK_Vw_Agreed_ActualDelivery]
    PRIMARY KEY CLUSTERED ([CustomerName], [Pk_Order], [DeliveryDate] ASC);
GO

-- Creating primary key on [Pk_Material] in table 'VW_BoardSearch'
ALTER TABLE [dbo].[VW_BoardSearch]
ADD CONSTRAINT [PK_VW_BoardSearch]
    PRIMARY KEY CLUSTERED ([Pk_Material] ASC);
GO

-- Creating primary key on [Pk_Customer], [CustomerName], [Pk_ContactPersonId], [ContactPersonName], [Pk_CustomerContacts], [CityName], [StateName], [CountryName] in table 'vw_CustomerDetails'
ALTER TABLE [dbo].[vw_CustomerDetails]
ADD CONSTRAINT [PK_vw_CustomerDetails]
    PRIMARY KEY CLUSTERED ([Pk_Customer], [CustomerName], [Pk_ContactPersonId], [ContactPersonName], [Pk_CustomerContacts], [CityName], [StateName], [CountryName] ASC);
GO

-- Creating primary key on [Pk_Customer], [Pk_CustomerShippingId], [CustomerName] in table 'Vw_CustShip'
ALTER TABLE [dbo].[Vw_CustShip]
ADD CONSTRAINT [PK_Vw_CustShip]
    PRIMARY KEY CLUSTERED ([Pk_Customer], [Pk_CustomerShippingId], [CustomerName] ASC);
GO

-- Creating primary key on [Pk_Enquiry], [CustomerName], [Date], [Pk_Estimate] in table 'Vw_Estimate'
ALTER TABLE [dbo].[Vw_Estimate]
ADD CONSTRAINT [PK_Vw_Estimate]
    PRIMARY KEY CLUSTERED ([Pk_Enquiry], [CustomerName], [Date], [Pk_Estimate] ASC);
GO

-- Creating primary key on [Pk_Order], [Pk_FinishedGoodsId] in table 'VW_FG_Production'
ALTER TABLE [dbo].[VW_FG_Production]
ADD CONSTRAINT [PK_VW_FG_Production]
    PRIMARY KEY CLUSTERED ([Pk_Order], [Pk_FinishedGoodsId] ASC);
GO

-- Creating primary key on [VendorName], [Pk_Vendor], [Pk_MaterialOrderMasterId], [Pk_Material], [Pk_MaterialOrderDetailsId] in table 'Vw_Indent'
ALTER TABLE [dbo].[Vw_Indent]
ADD CONSTRAINT [PK_Vw_Indent]
    PRIMARY KEY CLUSTERED ([VendorName], [Pk_Vendor], [Pk_MaterialOrderMasterId], [Pk_Material], [Pk_MaterialOrderDetailsId] ASC);
GO

-- Creating primary key on [Pk_MaterialCategory], [Inwd_Qty], [Issue_Qty], [Pk_Material] in table 'Vw_InOutSummary'
ALTER TABLE [dbo].[Vw_InOutSummary]
ADD CONSTRAINT [PK_Vw_InOutSummary]
    PRIMARY KEY CLUSTERED ([Pk_MaterialCategory], [Inwd_Qty], [Issue_Qty], [Pk_Material] ASC);
GO

-- Creating primary key on [Pk_MaterialIssueID], [Pk_Order], [Fk_Material], [Quantity], [Pk_MaterialIssueDetailsID] in table 'Vw_Issue'
ALTER TABLE [dbo].[Vw_Issue]
ADD CONSTRAINT [PK_Vw_Issue]
    PRIMARY KEY CLUSTERED ([Pk_MaterialIssueID], [Pk_Order], [Fk_Material], [Quantity], [Pk_MaterialIssueDetailsID] ASC);
GO

-- Creating primary key on [Pk_Material], [Pk_MaterialCategory] in table 'Vw_ItemMaster'
ALTER TABLE [dbo].[Vw_ItemMaster]
ADD CONSTRAINT [PK_Vw_ItemMaster]
    PRIMARY KEY CLUSTERED ([Pk_Material], [Pk_MaterialCategory] ASC);
GO

-- Creating primary key on [VendorName], [Pk_MaterialOrderMasterId], [Pk_Vendor], [Pk_MaterialOrderDetailsId] in table 'Vw_Material'
ALTER TABLE [dbo].[Vw_Material]
ADD CONSTRAINT [PK_Vw_Material]
    PRIMARY KEY CLUSTERED ([VendorName], [Pk_MaterialOrderMasterId], [Pk_Vendor], [Pk_MaterialOrderDetailsId] ASC);
GO

-- Creating primary key on [ContactPersonName], [Pk_MillContacts] in table 'VW_MillDuplicate'
ALTER TABLE [dbo].[VW_MillDuplicate]
ADD CONSTRAINT [PK_VW_MillDuplicate]
    PRIMARY KEY CLUSTERED ([ContactPersonName], [Pk_MillContacts] ASC);
GO

-- Creating primary key on [Pk_Material], [Pk_MaterialCategory] in table 'VW_Min_Level'
ALTER TABLE [dbo].[VW_Min_Level]
ADD CONSTRAINT [PK_VW_Min_Level]
    PRIMARY KEY CLUSTERED ([Pk_Material], [Pk_MaterialCategory] ASC);
GO

-- Creating primary key on [Quantity], [Pk_MaterialIssueDetailsID], [RetQty], [Consumption], [Pk_MaterialIssueID] in table 'Vw_MonthlyConsumption'
ALTER TABLE [dbo].[Vw_MonthlyConsumption]
ADD CONSTRAINT [PK_Vw_MonthlyConsumption]
    PRIMARY KEY CLUSTERED ([Quantity], [Pk_MaterialIssueDetailsID], [RetQty], [Consumption], [Pk_MaterialIssueID] ASC);
GO

-- Creating primary key on [Pk_Invoice] in table 'vw_MonthlySalesStatus'
ALTER TABLE [dbo].[vw_MonthlySalesStatus]
ADD CONSTRAINT [PK_vw_MonthlySalesStatus]
    PRIMARY KEY CLUSTERED ([Pk_Invoice] ASC);
GO

-- Creating primary key on [Pk_DeliverySechedule], [Pk_Order], [DeliveryDate], [CustomerName] in table 'vw_OrderDeliveryDetails'
ALTER TABLE [dbo].[vw_OrderDeliveryDetails]
ADD CONSTRAINT [PK_vw_OrderDeliveryDetails]
    PRIMARY KEY CLUSTERED ([Pk_DeliverySechedule], [Pk_Order], [DeliveryDate], [CustomerName] ASC);
GO

-- Creating primary key on [Pk_Material], [Pk_MaterialCategory], [Quantity], [Pk_Inward] in table 'Vw_PaperRecpt'
ALTER TABLE [dbo].[Vw_PaperRecpt]
ADD CONSTRAINT [PK_Vw_PaperRecpt]
    PRIMARY KEY CLUSTERED ([Pk_Material], [Pk_MaterialCategory], [Quantity], [Pk_Inward] ASC);
GO

-- Creating primary key on [Pk_Material] in table 'Vw_PoorQuality'
ALTER TABLE [dbo].[Vw_PoorQuality]
ADD CONSTRAINT [PK_Vw_PoorQuality]
    PRIMARY KEY CLUSTERED ([Pk_Material] ASC);
GO

-- Creating primary key on [Pk_QualityCheck] in table 'Vw_RawQuality'
ALTER TABLE [dbo].[Vw_RawQuality]
ADD CONSTRAINT [PK_Vw_RawQuality]
    PRIMARY KEY CLUSTERED ([Pk_QualityCheck] ASC);
GO

-- Creating primary key on [Pk_Material], [Inwd_Qty], [Issue_Qty], [RQty], [Fk_Tanent], [Pk_Stock] in table 'vw_StockLedger'
ALTER TABLE [dbo].[vw_StockLedger]
ADD CONSTRAINT [PK_vw_StockLedger]
    PRIMARY KEY CLUSTERED ([Pk_Material], [Inwd_Qty], [Issue_Qty], [RQty], [Fk_Tanent], [Pk_Stock] ASC);
GO

-- Creating primary key on [Pk_Material], [Pk_Stock], [Fk_Tanent] in table 'vw_StockMaterial'
ALTER TABLE [dbo].[vw_StockMaterial]
ADD CONSTRAINT [PK_vw_StockMaterial]
    PRIMARY KEY CLUSTERED ([Pk_Material], [Pk_Stock], [Fk_Tanent] ASC);
GO

-- Creating primary key on [Pk_MaterialCategory], [Pk_Material] in table 'Vw_StockSum'
ALTER TABLE [dbo].[Vw_StockSum]
ADD CONSTRAINT [PK_Vw_StockSum]
    PRIMARY KEY CLUSTERED ([Pk_MaterialCategory], [Pk_Material] ASC);
GO

-- Creating primary key on [Pk_Vendor], [VendorName], [Pk_ContactPersonId], [ContactPersonName], [Pk_VendorContact], [CityName], [StateName], [CountryName] in table 'vw_VendorDetails'
ALTER TABLE [dbo].[vw_VendorDetails]
ADD CONSTRAINT [PK_vw_VendorDetails]
    PRIMARY KEY CLUSTERED ([Pk_Vendor], [VendorName], [Pk_ContactPersonId], [ContactPersonName], [Pk_VendorContact], [CityName], [StateName], [CountryName] ASC);
GO

-- Creating primary key on [PK_DetailedEstimate] in table 'C_est_Estimation1'
ALTER TABLE [dbo].[C_est_Estimation1]
ADD CONSTRAINT [PK_C_est_Estimation1]
    PRIMARY KEY CLUSTERED ([PK_DetailedEstimate] ASC);
GO

-- Creating primary key on [PK_DetailedEstimate] in table 'C_est_Estimation3'
ALTER TABLE [dbo].[C_est_Estimation3]
ADD CONSTRAINT [PK_C_est_Estimation3]
    PRIMARY KEY CLUSTERED ([PK_DetailedEstimate] ASC);
GO

-- Creating primary key on [PK_DetailedEstimate] in table 'C_est_Estimation4'
ALTER TABLE [dbo].[C_est_Estimation4]
ADD CONSTRAINT [PK_C_est_Estimation4]
    PRIMARY KEY CLUSTERED ([PK_DetailedEstimate] ASC);
GO

-- Creating primary key on [PK_DetailedEstimate] in table 'C_est_Estimation5'
ALTER TABLE [dbo].[C_est_Estimation5]
ADD CONSTRAINT [PK_C_est_Estimation5]
    PRIMARY KEY CLUSTERED ([PK_DetailedEstimate] ASC);
GO

-- Creating primary key on [Pk_BoxEstimation] in table 'est_BoxEstimation'
ALTER TABLE [dbo].[est_BoxEstimation]
ADD CONSTRAINT [PK_est_BoxEstimation]
    PRIMARY KEY CLUSTERED ([Pk_BoxEstimation] ASC);
GO

-- Creating primary key on [InwdQty], [Pk_QualityChild] in table 'Vw_Qc'
ALTER TABLE [dbo].[Vw_Qc]
ADD CONSTRAINT [PK_Vw_Qc]
    PRIMARY KEY CLUSTERED ([InwdQty], [Pk_QualityChild] ASC);
GO

-- Creating primary key on [Pk_BoxEstimationChild], [Name] in table 'Vw_EstDet1'
ALTER TABLE [dbo].[Vw_EstDet1]
ADD CONSTRAINT [PK_Vw_EstDet1]
    PRIMARY KEY CLUSTERED ([Pk_BoxEstimationChild], [Name] ASC);
GO

-- Creating primary key on [Pk_BoxCID] in table 'BoxChild'
ALTER TABLE [dbo].[BoxChild]
ADD CONSTRAINT [PK_BoxChild]
    PRIMARY KEY CLUSTERED ([Pk_BoxCID] ASC);
GO

-- Creating primary key on [Pk_PaperStock] in table 'PaperStock'
ALTER TABLE [dbo].[PaperStock]
ADD CONSTRAINT [PK_PaperStock]
    PRIMARY KEY CLUSTERED ([Pk_PaperStock] ASC);
GO

-- Creating primary key on [Pk_Material], [Pk_BoxCID], [Expr1] in table 'Vw_BoxPaperStock'
ALTER TABLE [dbo].[Vw_BoxPaperStock]
ADD CONSTRAINT [PK_Vw_BoxPaperStock]
    PRIMARY KEY CLUSTERED ([Pk_Material], [Pk_BoxCID], [Expr1] ASC);
GO

-- Creating primary key on [PkTax] in table 'Tax'
ALTER TABLE [dbo].[Tax]
ADD CONSTRAINT [PK_Tax]
    PRIMARY KEY CLUSTERED ([PkTax] ASC);
GO

-- Creating primary key on [Pk_Material] in table 'Vw_MaterialParts'
ALTER TABLE [dbo].[Vw_MaterialParts]
ADD CONSTRAINT [PK_Vw_MaterialParts]
    PRIMARY KEY CLUSTERED ([Pk_Material] ASC);
GO

-- Creating primary key on [Pk_BoxID], [Pk_Material] in table 'Vw_BoxPaper'
ALTER TABLE [dbo].[Vw_BoxPaper]
ADD CONSTRAINT [PK_Vw_BoxPaper]
    PRIMARY KEY CLUSTERED ([Pk_BoxID], [Pk_Material] ASC);
GO

-- Creating primary key on [Pk_DeliverySechedule] in table 'gen_DeliverySchedule'
ALTER TABLE [dbo].[gen_DeliverySchedule]
ADD CONSTRAINT [PK_gen_DeliverySchedule]
    PRIMARY KEY CLUSTERED ([Pk_DeliverySechedule] ASC);
GO

-- Creating primary key on [Pk_Material], [Pk_Order], [BName], [Pk_BoxID] in table 'Vw_BoxPQty'
ALTER TABLE [dbo].[Vw_BoxPQty]
ADD CONSTRAINT [PK_Vw_BoxPQty]
    PRIMARY KEY CLUSTERED ([Pk_Material], [Pk_Order], [BName], [Pk_BoxID] ASC);
GO

-- Creating primary key on [BName], [Pk_Order], [Pk_Material], [Pk_BoxID] in table 'Vw_BoxPReq'
ALTER TABLE [dbo].[Vw_BoxPReq]
ADD CONSTRAINT [PK_Vw_BoxPReq]
    PRIMARY KEY CLUSTERED ([BName], [Pk_Order], [Pk_Material], [Pk_BoxID] ASC);
GO

-- Creating primary key on [Pk_Enquiry], [Date], [CustomerName], [Name], [Pk_BoxID] in table 'Vw_EnqReport'
ALTER TABLE [dbo].[Vw_EnqReport]
ADD CONSTRAINT [PK_Vw_EnqReport]
    PRIMARY KEY CLUSTERED ([Pk_Enquiry], [Date], [CustomerName], [Name], [Pk_BoxID] ASC);
GO

-- Creating primary key on [Pk_EnquiryChild] in table 'eq_EnquiryChild'
ALTER TABLE [dbo].[eq_EnquiryChild]
ADD CONSTRAINT [PK_eq_EnquiryChild]
    PRIMARY KEY CLUSTERED ([Pk_EnquiryChild] ASC);
GO

-- Creating primary key on [Pk_PoRetID] in table 'POReturnMaster'
ALTER TABLE [dbo].[POReturnMaster]
ADD CONSTRAINT [PK_POReturnMaster]
    PRIMARY KEY CLUSTERED ([Pk_PoRetID] ASC);
GO

-- Creating primary key on [Id] in table 'BarCodeT'
ALTER TABLE [dbo].[BarCodeT]
ADD CONSTRAINT [PK_BarCodeT]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Pk_Value] in table 'BoardDesc'
ALTER TABLE [dbo].[BoardDesc]
ADD CONSTRAINT [PK_BoardDesc]
    PRIMARY KEY CLUSTERED ([Pk_Value] ASC);
GO

-- Creating primary key on [Pk_Material] in table 'Vw_BoardDesc'
ALTER TABLE [dbo].[Vw_BoardDesc]
ADD CONSTRAINT [PK_Vw_BoardDesc]
    PRIMARY KEY CLUSTERED ([Pk_Material] ASC);
GO

-- Creating primary key on [Pk_ID] in table 'OD_ID'
ALTER TABLE [dbo].[OD_ID]
ADD CONSTRAINT [PK_OD_ID]
    PRIMARY KEY CLUSTERED ([Pk_ID] ASC);
GO

-- Creating primary key on [Pk_BoxEstimationChild] in table 'est_BoxEstimationChild'
ALTER TABLE [dbo].[est_BoxEstimationChild]
ADD CONSTRAINT [PK_est_BoxEstimationChild]
    PRIMARY KEY CLUSTERED ([Pk_BoxEstimationChild] ASC);
GO

-- Creating primary key on [Pk_Documents] in table 'eq_Documents'
ALTER TABLE [dbo].[eq_Documents]
ADD CONSTRAINT [PK_eq_Documents]
    PRIMARY KEY CLUSTERED ([Pk_Documents] ASC);
GO

-- Creating primary key on [Pk_Enquiry] in table 'eq_Enquiry'
ALTER TABLE [dbo].[eq_Enquiry]
ADD CONSTRAINT [PK_eq_Enquiry]
    PRIMARY KEY CLUSTERED ([Pk_Enquiry] ASC);
GO

-- Creating primary key on [Pk_QualityCheck] in table 'Vw_Inwd'
ALTER TABLE [dbo].[Vw_Inwd]
ADD CONSTRAINT [PK_Vw_Inwd]
    PRIMARY KEY CLUSTERED ([Pk_QualityCheck] ASC);
GO

-- Creating primary key on [Pk_Activities] in table 'ideActivity'
ALTER TABLE [dbo].[ideActivity]
ADD CONSTRAINT [PK_ideActivity]
    PRIMARY KEY CLUSTERED ([Pk_Activities] ASC);
GO

-- Creating primary key on [Pk_ActivityPermissions] in table 'ideActivityPermission'
ALTER TABLE [dbo].[ideActivityPermission]
ADD CONSTRAINT [PK_ideActivityPermission]
    PRIMARY KEY CLUSTERED ([Pk_ActivityPermissions] ASC);
GO

-- Creating primary key on [Pk_Permissions] in table 'idePermission'
ALTER TABLE [dbo].[idePermission]
ADD CONSTRAINT [PK_idePermission]
    PRIMARY KEY CLUSTERED ([Pk_Permissions] ASC);
GO

-- Creating primary key on [Pk_Roles] in table 'ideRole'
ALTER TABLE [dbo].[ideRole]
ADD CONSTRAINT [PK_ideRole]
    PRIMARY KEY CLUSTERED ([Pk_Roles] ASC);
GO

-- Creating primary key on [Pk_RoleUsers] in table 'ideRoleUser'
ALTER TABLE [dbo].[ideRoleUser]
ADD CONSTRAINT [PK_ideRoleUser]
    PRIMARY KEY CLUSTERED ([Pk_RoleUsers] ASC);
GO

-- Creating primary key on [Pk_Shortcuts] in table 'ideShortCut'
ALTER TABLE [dbo].[ideShortCut]
ADD CONSTRAINT [PK_ideShortCut]
    PRIMARY KEY CLUSTERED ([Pk_Shortcuts] ASC);
GO

-- Creating primary key on [Pk_ID] in table 'PapInsert'
ALTER TABLE [dbo].[PapInsert]
ADD CONSTRAINT [PK_PapInsert]
    PRIMARY KEY CLUSTERED ([Pk_ID] ASC);
GO

-- Creating primary key on [Pk_Unit] in table 'gen_Unit'
ALTER TABLE [dbo].[gen_Unit]
ADD CONSTRAINT [PK_gen_Unit]
    PRIMARY KEY CLUSTERED ([Pk_Unit] ASC);
GO

-- Creating primary key on [Pk_Stock] in table 'Stocks'
ALTER TABLE [dbo].[Stocks]
ADD CONSTRAINT [PK_Stocks]
    PRIMARY KEY CLUSTERED ([Pk_Stock] ASC);
GO

-- Creating primary key on [Pk_Vendor] in table 'gen_Vendor'
ALTER TABLE [dbo].[gen_Vendor]
ADD CONSTRAINT [PK_gen_Vendor]
    PRIMARY KEY CLUSTERED ([Pk_Vendor] ASC);
GO

-- Creating primary key on [Pk_VendorMaterial] in table 'gen_VendorMaterials'
ALTER TABLE [dbo].[gen_VendorMaterials]
ADD CONSTRAINT [PK_gen_VendorMaterials]
    PRIMARY KEY CLUSTERED ([Pk_VendorMaterial] ASC);
GO

-- Creating primary key on [Pk_Users] in table 'ideUser'
ALTER TABLE [dbo].[ideUser]
ADD CONSTRAINT [PK_ideUser]
    PRIMARY KEY CLUSTERED ([Pk_Users] ASC);
GO

-- Creating primary key on [Pk_IssueReturnMasterId] in table 'IssueReturn'
ALTER TABLE [dbo].[IssueReturn]
ADD CONSTRAINT [PK_IssueReturn]
    PRIMARY KEY CLUSTERED ([Pk_IssueReturnMasterId] ASC);
GO

-- Creating primary key on [Pk_IssueReturnDetailsId] in table 'IssueReturnDetails'
ALTER TABLE [dbo].[IssueReturnDetails]
ADD CONSTRAINT [PK_IssueReturnDetails]
    PRIMARY KEY CLUSTERED ([Pk_IssueReturnDetailsId] ASC);
GO

-- Creating primary key on [Pk_JobCardDet] in table 'JobCardDetails'
ALTER TABLE [dbo].[JobCardDetails]
ADD CONSTRAINT [PK_JobCardDetails]
    PRIMARY KEY CLUSTERED ([Pk_JobCardDet] ASC);
GO

-- Creating primary key on [Pk_MachineMaintenance] in table 'MachineMaintenance1Set'
ALTER TABLE [dbo].[MachineMaintenance1Set]
ADD CONSTRAINT [PK_MachineMaintenance1Set]
    PRIMARY KEY CLUSTERED ([Pk_MachineMaintenance] ASC);
GO

-- Creating primary key on [Pk_PaymtID] in table 'PaymentTrack1Set'
ALTER TABLE [dbo].[PaymentTrack1Set]
ADD CONSTRAINT [PK_PaymentTrack1Set]
    PRIMARY KEY CLUSTERED ([Pk_PaymtID] ASC);
GO

-- Creating primary key on [Pk_PoRetDetID] in table 'POReturnDetails'
ALTER TABLE [dbo].[POReturnDetails]
ADD CONSTRAINT [PK_POReturnDetails]
    PRIMARY KEY CLUSTERED ([Pk_PoRetDetID] ASC);
GO

-- Creating primary key on [PkTax] in table 'Tax1Set'
ALTER TABLE [dbo].[Tax1Set]
ADD CONSTRAINT [PK_Tax1Set]
    PRIMARY KEY CLUSTERED ([PkTax] ASC);
GO

-- Creating primary key on [Pk_wfRoleUser] in table 'wfWorkflowRoleUser1Set'
ALTER TABLE [dbo].[wfWorkflowRoleUser1Set]
ADD CONSTRAINT [PK_wfWorkflowRoleUser1Set]
    PRIMARY KEY CLUSTERED ([Pk_wfRoleUser] ASC);
GO

-- Creating primary key on [Pk_Mill] in table 'gen_Mill'
ALTER TABLE [dbo].[gen_Mill]
ADD CONSTRAINT [PK_gen_Mill]
    PRIMARY KEY CLUSTERED ([Pk_Mill] ASC);
GO

-- Creating primary key on [Pk_Material] in table 'Inv_Material'
ALTER TABLE [dbo].[Inv_Material]
ADD CONSTRAINT [PK_Inv_Material]
    PRIMARY KEY CLUSTERED ([Pk_Material] ASC);
GO

-- Creating primary key on [Pk_MaterialIssueDetailsID] in table 'MaterialIssueDetails'
ALTER TABLE [dbo].[MaterialIssueDetails]
ADD CONSTRAINT [PK_MaterialIssueDetails]
    PRIMARY KEY CLUSTERED ([Pk_MaterialIssueDetailsID] ASC);
GO

-- Creating primary key on [Pk_LayerID] in table 'Items_Layers'
ALTER TABLE [dbo].[Items_Layers]
ADD CONSTRAINT [PK_Items_Layers]
    PRIMARY KEY CLUSTERED ([Pk_LayerID] ASC);
GO

-- Creating primary key on [Pk_FluteID] in table 'FluteType'
ALTER TABLE [dbo].[FluteType]
ADD CONSTRAINT [PK_FluteType]
    PRIMARY KEY CLUSTERED ([Pk_FluteID] ASC);
GO

-- Creating primary key on [Pk_ID] in table 'Trans_Invoice'
ALTER TABLE [dbo].[Trans_Invoice]
ADD CONSTRAINT [PK_Trans_Invoice]
    PRIMARY KEY CLUSTERED ([Pk_ID] ASC);
GO

-- Creating primary key on [Pk_ID] in table 'Trans_Payment'
ALTER TABLE [dbo].[Trans_Payment]
ADD CONSTRAINT [PK_Trans_Payment]
    PRIMARY KEY CLUSTERED ([Pk_ID] ASC);
GO

-- Creating primary key on [Pk_ID] in table 'TransporterM'
ALTER TABLE [dbo].[TransporterM]
ADD CONSTRAINT [PK_TransporterM]
    PRIMARY KEY CLUSTERED ([Pk_ID] ASC);
GO

-- Creating primary key on [Pk_QuotationDetID] in table 'QuotationDetails'
ALTER TABLE [dbo].[QuotationDetails]
ADD CONSTRAINT [PK_QuotationDetails]
    PRIMARY KEY CLUSTERED ([Pk_QuotationDetID] ASC);
GO

-- Creating primary key on [Pk_Quotation], [CustomerName], [Pk_Enquiry], [Date] in table 'Vw_Quote'
ALTER TABLE [dbo].[Vw_Quote]
ADD CONSTRAINT [PK_Vw_Quote]
    PRIMARY KEY CLUSTERED ([Pk_Quotation], [CustomerName], [Pk_Enquiry], [Date] ASC);
GO

-- Creating primary key on [Pk_EmployeeId] in table 'emp_EmployeeMaster'
ALTER TABLE [dbo].[emp_EmployeeMaster]
ADD CONSTRAINT [PK_emp_EmployeeMaster]
    PRIMARY KEY CLUSTERED ([Pk_EmployeeId] ASC);
GO

-- Creating primary key on [Pk_CharID] in table 'InkChar'
ALTER TABLE [dbo].[InkChar]
ADD CONSTRAINT [PK_InkChar]
    PRIMARY KEY CLUSTERED ([Pk_CharID] ASC);
GO

-- Creating primary key on [Pk_Id] in table 'WireCertificate'
ALTER TABLE [dbo].[WireCertificate]
ADD CONSTRAINT [PK_WireCertificate]
    PRIMARY KEY CLUSTERED ([Pk_Id] ASC);
GO

-- Creating primary key on [Pk_IdDet] in table 'WireCertificateDetails'
ALTER TABLE [dbo].[WireCertificateDetails]
ADD CONSTRAINT [PK_WireCertificateDetails]
    PRIMARY KEY CLUSTERED ([Pk_IdDet] ASC);
GO

-- Creating primary key on [Pk_CharID] in table 'WireChar'
ALTER TABLE [dbo].[WireChar]
ADD CONSTRAINT [PK_WireChar]
    PRIMARY KEY CLUSTERED ([Pk_CharID] ASC);
GO

-- Creating primary key on [Pk_Quotation] in table 'QuotationMaster'
ALTER TABLE [dbo].[QuotationMaster]
ADD CONSTRAINT [PK_QuotationMaster]
    PRIMARY KEY CLUSTERED ([Pk_Quotation] ASC);
GO

-- Creating primary key on [Pk_CharID] in table 'PaperChar'
ALTER TABLE [dbo].[PaperChar]
ADD CONSTRAINT [PK_PaperChar]
    PRIMARY KEY CLUSTERED ([Pk_CharID] ASC);
GO

-- Creating primary key on [Pk_Id], [VendorName], [Pk_IdDet] in table 'Vw_WireCertificate'
ALTER TABLE [dbo].[Vw_WireCertificate]
ADD CONSTRAINT [PK_Vw_WireCertificate]
    PRIMARY KEY CLUSTERED ([Pk_Id], [VendorName], [Pk_IdDet] ASC);
GO

-- Creating primary key on [Pk_Id] in table 'InkCertificate'
ALTER TABLE [dbo].[InkCertificate]
ADD CONSTRAINT [PK_InkCertificate]
    PRIMARY KEY CLUSTERED ([Pk_Id] ASC);
GO

-- Creating primary key on [Pk_IdDet], [Pk_Id] in table 'Vw_InkCertificate'
ALTER TABLE [dbo].[Vw_InkCertificate]
ADD CONSTRAINT [PK_Vw_InkCertificate]
    PRIMARY KEY CLUSTERED ([Pk_IdDet], [Pk_Id] ASC);
GO

-- Creating primary key on [Pk_Id] in table 'GlueCertificate'
ALTER TABLE [dbo].[GlueCertificate]
ADD CONSTRAINT [PK_GlueCertificate]
    PRIMARY KEY CLUSTERED ([Pk_Id] ASC);
GO

-- Creating primary key on [Pk_IdDet] in table 'GlueCertificateDetails'
ALTER TABLE [dbo].[GlueCertificateDetails]
ADD CONSTRAINT [PK_GlueCertificateDetails]
    PRIMARY KEY CLUSTERED ([Pk_IdDet] ASC);
GO

-- Creating primary key on [Pk_IdDet], [VendorName], [Pk_Id] in table 'Vw_GlueCertificate'
ALTER TABLE [dbo].[Vw_GlueCertificate]
ADD CONSTRAINT [PK_Vw_GlueCertificate]
    PRIMARY KEY CLUSTERED ([Pk_IdDet], [VendorName], [Pk_Id] ASC);
GO

-- Creating primary key on [Pk_Material], [BoxName], [Pk_JobCardID], [CustomerName] in table 'Vw_PStock'
ALTER TABLE [dbo].[Vw_PStock]
ADD CONSTRAINT [PK_Vw_PStock]
    PRIMARY KEY CLUSTERED ([Pk_Material], [BoxName], [Pk_JobCardID], [CustomerName] ASC);
GO

-- Creating primary key on [Pk_IdDet] in table 'InkCertificateDetails'
ALTER TABLE [dbo].[InkCertificateDetails]
ADD CONSTRAINT [PK_InkCertificateDetails]
    PRIMARY KEY CLUSTERED ([Pk_IdDet] ASC);
GO

-- Creating primary key on [Pk_Material], [Pk_BoxID], [Pk_JobCardID], [bName], [Pk_JobCardDet] in table 'Vw_AssStock'
ALTER TABLE [dbo].[Vw_AssStock]
ADD CONSTRAINT [PK_Vw_AssStock]
    PRIMARY KEY CLUSTERED ([Pk_Material], [Pk_BoxID], [Pk_JobCardID], [bName], [Pk_JobCardDet] ASC);
GO

-- Creating primary key on [Pk_Inward], [Pk_Material] in table 'Vw_TotInwdQty'
ALTER TABLE [dbo].[Vw_TotInwdQty]
ADD CONSTRAINT [PK_Vw_TotInwdQty]
    PRIMARY KEY CLUSTERED ([Pk_Inward], [Pk_Material] ASC);
GO

-- Creating primary key on [Name], [BType], [Pk_EnquiryChild], [Pk_PartPropertyID] in table 'Vw_OrdQty'
ALTER TABLE [dbo].[Vw_OrdQty]
ADD CONSTRAINT [PK_Vw_OrdQty]
    PRIMARY KEY CLUSTERED ([Name], [BType], [Pk_EnquiryChild], [Pk_PartPropertyID] ASC);
GO

-- Creating primary key on [DeliveryDate], [Pk_BoxID], [Name], [Pk_Order], [OrderDate], [CustomerName], [Pk_DeliverySechedule], [Pk_PartPropertyID] in table 'Vw_PartsSchedules'
ALTER TABLE [dbo].[Vw_PartsSchedules]
ADD CONSTRAINT [PK_Vw_PartsSchedules]
    PRIMARY KEY CLUSTERED ([DeliveryDate], [Pk_BoxID], [Name], [Pk_Order], [OrderDate], [CustomerName], [Pk_DeliverySechedule], [Pk_PartPropertyID] ASC);
GO

-- Creating primary key on [Pk_IdDet] in table 'PaperCertificateDetails'
ALTER TABLE [dbo].[PaperCertificateDetails]
ADD CONSTRAINT [PK_PaperCertificateDetails]
    PRIMARY KEY CLUSTERED ([Pk_IdDet] ASC);
GO

-- Creating primary key on [Pk_JobCardPartsDet] in table 'JobCardPartsDetails'
ALTER TABLE [dbo].[JobCardPartsDetails]
ADD CONSTRAINT [PK_JobCardPartsDetails]
    PRIMARY KEY CLUSTERED ([Pk_JobCardPartsDet] ASC);
GO

-- Creating primary key on [Pk_JobCardPartsID] in table 'JobCardPartsMaster'
ALTER TABLE [dbo].[JobCardPartsMaster]
ADD CONSTRAINT [PK_JobCardPartsMaster]
    PRIMARY KEY CLUSTERED ([Pk_JobCardPartsID] ASC);
GO

-- Creating primary key on [Pk_BoxID], [BName], [Pk_Order], [OrderDate], [CustomerName], [Pk_Material], [Pk_LayerID], [Pk_JobCardPartsID], [DeliveryDate] in table 'Vw_JCpartsRep'
ALTER TABLE [dbo].[Vw_JCpartsRep]
ADD CONSTRAINT [PK_Vw_JCpartsRep]
    PRIMARY KEY CLUSTERED ([Pk_BoxID], [BName], [Pk_Order], [OrderDate], [CustomerName], [Pk_Material], [Pk_LayerID], [Pk_JobCardPartsID], [DeliveryDate] ASC);
GO

-- Creating primary key on [Pk_SrcDetID] in table 'OutsourcingChild'
ALTER TABLE [dbo].[OutsourcingChild]
ADD CONSTRAINT [PK_OutsourcingChild]
    PRIMARY KEY CLUSTERED ([Pk_SrcDetID] ASC);
GO

-- Creating primary key on [Pk_ID] in table 'PartJobs'
ALTER TABLE [dbo].[PartJobs]
ADD CONSTRAINT [PK_PartJobs]
    PRIMARY KEY CLUSTERED ([Pk_ID] ASC);
GO

-- Creating primary key on [Pk_StockId] in table 'FinishedGoodsStock'
ALTER TABLE [dbo].[FinishedGoodsStock]
ADD CONSTRAINT [PK_FinishedGoodsStock]
    PRIMARY KEY CLUSTERED ([Pk_StockId] ASC);
GO

-- Creating primary key on [Pk_JobCardPartsID], [Pk_Material], [BoxName], [Pk_PartPropertyID], [Pk_PaperStock] in table 'Vw_JCPartDet'
ALTER TABLE [dbo].[Vw_JCPartDet]
ADD CONSTRAINT [PK_Vw_JCPartDet]
    PRIMARY KEY CLUSTERED ([Pk_JobCardPartsID], [Pk_Material], [BoxName], [Pk_PartPropertyID], [Pk_PaperStock] ASC);
GO

-- Creating primary key on [Pk_StockID] in table 'JobWorkRMStock'
ALTER TABLE [dbo].[JobWorkRMStock]
ADD CONSTRAINT [PK_JobWorkRMStock]
    PRIMARY KEY CLUSTERED ([Pk_StockID] ASC);
GO

-- Creating primary key on [Pk_JobCardDet], [Pk_Material], [Pk_JobCardID], [Expr1], [TotalQty], [CustomerName] in table 'Vw_JobIssue'
ALTER TABLE [dbo].[Vw_JobIssue]
ADD CONSTRAINT [PK_Vw_JobIssue]
    PRIMARY KEY CLUSTERED ([Pk_JobCardDet], [Pk_Material], [Pk_JobCardID], [Expr1], [TotalQty], [CustomerName] ASC);
GO

-- Creating primary key on [Pk_QualityCheck] in table 'Vw_Quality'
ALTER TABLE [dbo].[Vw_Quality]
ADD CONSTRAINT [PK_Vw_Quality]
    PRIMARY KEY CLUSTERED ([Pk_QualityCheck] ASC);
GO

-- Creating primary key on [Pk_Material] in table 'Vw_TotPaperStockList'
ALTER TABLE [dbo].[Vw_TotPaperStockList]
ADD CONSTRAINT [PK_Vw_TotPaperStockList]
    PRIMARY KEY CLUSTERED ([Pk_Material] ASC);
GO

-- Creating primary key on [Pk_BoxEstimation], [EstDATE] in table 'Vw_Est_Det'
ALTER TABLE [dbo].[Vw_Est_Det]
ADD CONSTRAINT [PK_Vw_Est_Det]
    PRIMARY KEY CLUSTERED ([Pk_BoxEstimation], [EstDATE] ASC);
GO

-- Creating primary key on [Name], [CustomerName], [Pk_Quotation], [CustomerType], [CustomerAddress], [LandLine], [Email], [StateName], [CountryName], [ContactPerson] in table 'Vw_Quotation'
ALTER TABLE [dbo].[Vw_Quotation]
ADD CONSTRAINT [PK_Vw_Quotation]
    PRIMARY KEY CLUSTERED ([Name], [CustomerName], [Pk_Quotation], [CustomerType], [CustomerAddress], [LandLine], [Email], [StateName], [CountryName], [ContactPerson] ASC);
GO

-- Creating primary key on [Pk_JobCardDet] in table 'Sample_JobCardDetails'
ALTER TABLE [dbo].[Sample_JobCardDetails]
ADD CONSTRAINT [PK_Sample_JobCardDetails]
    PRIMARY KEY CLUSTERED ([Pk_JobCardDet] ASC);
GO

-- Creating primary key on [Pk_JobCardID] in table 'Sample_JobCardMaster'
ALTER TABLE [dbo].[Sample_JobCardMaster]
ADD CONSTRAINT [PK_Sample_JobCardMaster]
    PRIMARY KEY CLUSTERED ([Pk_JobCardID] ASC);
GO

-- Creating primary key on [Pk_BoxSpecID] in table 'SampleBoxSpecs'
ALTER TABLE [dbo].[SampleBoxSpecs]
ADD CONSTRAINT [PK_SampleBoxSpecs]
    PRIMARY KEY CLUSTERED ([Pk_BoxSpecID] ASC);
GO

-- Creating primary key on [Pk_PartPropertyID] in table 'SampleItemPartProperty'
ALTER TABLE [dbo].[SampleItemPartProperty]
ADD CONSTRAINT [PK_SampleItemPartProperty]
    PRIMARY KEY CLUSTERED ([Pk_PartPropertyID] ASC);
GO

-- Creating primary key on [Pk_LayerID] in table 'SampleItems_Layers'
ALTER TABLE [dbo].[SampleItems_Layers]
ADD CONSTRAINT [PK_SampleItems_Layers]
    PRIMARY KEY CLUSTERED ([Pk_LayerID] ASC);
GO

-- Creating primary key on [Pk_BoxID] in table 'SampleBoxMaster'
ALTER TABLE [dbo].[SampleBoxMaster]
ADD CONSTRAINT [PK_SampleBoxMaster]
    PRIMARY KEY CLUSTERED ([Pk_BoxID] ASC);
GO

-- Creating primary key on [Name], [Pk_PartPropertyID], [Pk_LayerID], [Pk_BoxID], [CustomerName], [Pk_BoxSpecID], [BType] in table 'Vw_SampleBoxDet'
ALTER TABLE [dbo].[Vw_SampleBoxDet]
ADD CONSTRAINT [PK_Vw_SampleBoxDet]
    PRIMARY KEY CLUSTERED ([Name], [Pk_PartPropertyID], [Pk_LayerID], [Pk_BoxID], [CustomerName], [Pk_BoxSpecID], [BType] ASC);
GO

-- Creating primary key on [Pk_StockID] in table 'BoxStock'
ALTER TABLE [dbo].[BoxStock]
ADD CONSTRAINT [PK_BoxStock]
    PRIMARY KEY CLUSTERED ([Pk_StockID] ASC);
GO

-- Creating primary key on [Pk_DispDet] in table 'DispatchChild'
ALTER TABLE [dbo].[DispatchChild]
ADD CONSTRAINT [PK_DispatchChild]
    PRIMARY KEY CLUSTERED ([Pk_DispDet] ASC);
GO

-- Creating primary key on [Pk_Dispatch] in table 'DispatchMast'
ALTER TABLE [dbo].[DispatchMast]
ADD CONSTRAINT [PK_DispatchMast]
    PRIMARY KEY CLUSTERED ([Pk_Dispatch] ASC);
GO

-- Creating primary key on [Pk_Id] in table 'PaperCertificate'
ALTER TABLE [dbo].[PaperCertificate]
ADD CONSTRAINT [PK_PaperCertificate]
    PRIMARY KEY CLUSTERED ([Pk_Id] ASC);
GO

-- Creating primary key on [Pk_DeliverySechedule], [Name], [Pk_PartPropertyID], [Pk_LayerID] in table 'Vw_BoxScheduleDet'
ALTER TABLE [dbo].[Vw_BoxScheduleDet]
ADD CONSTRAINT [PK_Vw_BoxScheduleDet]
    PRIMARY KEY CLUSTERED ([Pk_DeliverySechedule], [Name], [Pk_PartPropertyID], [Pk_LayerID] ASC);
GO

-- Creating primary key on [Pk_MaterialOrderDetailsId] in table 'Vw_PendingPO'
ALTER TABLE [dbo].[Vw_PendingPO]
ADD CONSTRAINT [PK_Vw_PendingPO]
    PRIMARY KEY CLUSTERED ([Pk_MaterialOrderDetailsId] ASC);
GO

-- Creating primary key on [Pk_PODet] in table 'PurchaseOrderD'
ALTER TABLE [dbo].[PurchaseOrderD]
ADD CONSTRAINT [PK_PurchaseOrderD]
    PRIMARY KEY CLUSTERED ([Pk_PODet] ASC);
GO

-- Creating primary key on [Pk_MaterialOrderMasterId], [Pk_MaterialOrderDetailsId], [Pk_Material] in table 'Vw_IndentCertificate'
ALTER TABLE [dbo].[Vw_IndentCertificate]
ADD CONSTRAINT [PK_Vw_IndentCertificate]
    PRIMARY KEY CLUSTERED ([Pk_MaterialOrderMasterId], [Pk_MaterialOrderDetailsId], [Pk_Material] ASC);
GO

-- Creating primary key on [Pk_Process] in table 'ProcessMaster'
ALTER TABLE [dbo].[ProcessMaster]
ADD CONSTRAINT [PK_ProcessMaster]
    PRIMARY KEY CLUSTERED ([Pk_Process] ASC);
GO

-- Creating primary key on [Pk_JProcessID] in table 'JProcess'
ALTER TABLE [dbo].[JProcess]
ADD CONSTRAINT [PK_JProcess]
    PRIMARY KEY CLUSTERED ([Pk_JProcessID] ASC);
GO

-- Creating primary key on [Pk_Process], [Pk_JProcessID] in table 'Vw_ProcessJC'
ALTER TABLE [dbo].[Vw_ProcessJC]
ADD CONSTRAINT [PK_Vw_ProcessJC]
    PRIMARY KEY CLUSTERED ([Pk_Process], [Pk_JProcessID] ASC);
GO

-- Creating primary key on [Pk_Documents] in table 'JC_Documents'
ALTER TABLE [dbo].[JC_Documents]
ADD CONSTRAINT [PK_JC_Documents]
    PRIMARY KEY CLUSTERED ([Pk_Documents] ASC);
GO

-- Creating primary key on [Pk_Process] in table 'vw_JProcessList'
ALTER TABLE [dbo].[vw_JProcessList]
ADD CONSTRAINT [PK_vw_JProcessList]
    PRIMARY KEY CLUSTERED ([Pk_Process] ASC);
GO

-- Creating primary key on [DeliveryDate], [Pk_BoxID], [Name], [Pk_Order], [OrderDate], [CustomerName], [Pk_DeliverySechedule], [Pk_PartPropertyID] in table 'Vw_Schedules'
ALTER TABLE [dbo].[Vw_Schedules]
ADD CONSTRAINT [PK_Vw_Schedules]
    PRIMARY KEY CLUSTERED ([DeliveryDate], [Pk_BoxID], [Name], [Pk_Order], [OrderDate], [CustomerName], [Pk_DeliverySechedule], [Pk_PartPropertyID] ASC);
GO

-- Creating primary key on [Pk_DeliverySechedule], [DeliveryDate], [Pk_Order], [OrderDate], [Name], [CustomerName], [Expr1] in table 'Vw_DeliveryScheduleReport'
ALTER TABLE [dbo].[Vw_DeliveryScheduleReport]
ADD CONSTRAINT [PK_Vw_DeliveryScheduleReport]
    PRIMARY KEY CLUSTERED ([Pk_DeliverySechedule], [DeliveryDate], [Pk_Order], [OrderDate], [Name], [CustomerName], [Expr1] ASC);
GO

-- Creating primary key on [Pk_ParaID] in table 'COA_Parameters'
ALTER TABLE [dbo].[COA_Parameters]
ADD CONSTRAINT [PK_COA_Parameters]
    PRIMARY KEY CLUSTERED ([Pk_ParaID] ASC);
GO

-- Creating primary key on [Pk_ID] in table 'COA'
ALTER TABLE [dbo].[COA]
ADD CONSTRAINT [PK_COA]
    PRIMARY KEY CLUSTERED ([Pk_ID] ASC);
GO

-- Creating primary key on [Pk_BoxID], [BName], [Pk_PartPropertyID], [Pk_Material], [Pk_PaperStock] in table 'Vw_PartPaperLoad'
ALTER TABLE [dbo].[Vw_PartPaperLoad]
ADD CONSTRAINT [PK_Vw_PartPaperLoad]
    PRIMARY KEY CLUSTERED ([Pk_BoxID], [BName], [Pk_PartPropertyID], [Pk_Material], [Pk_PaperStock] ASC);
GO

-- Creating primary key on [Pk_IDDet] in table 'COADetails'
ALTER TABLE [dbo].[COADetails]
ADD CONSTRAINT [PK_COADetails]
    PRIMARY KEY CLUSTERED ([Pk_IDDet] ASC);
GO

-- Creating primary key on [Pk_ID], [Pk_IDDet] in table 'Vw_COARep'
ALTER TABLE [dbo].[Vw_COARep]
ADD CONSTRAINT [PK_Vw_COARep]
    PRIMARY KEY CLUSTERED ([Pk_ID], [Pk_IDDet] ASC);
GO

-- Creating primary key on [Pk_BoxID], [Pk_Material], [BName], [Pk_Order], [OrderDate], [CustomerName], [Pk_Mill] in table 'Vw_PaperReqDetIndent'
ALTER TABLE [dbo].[Vw_PaperReqDetIndent]
ADD CONSTRAINT [PK_Vw_PaperReqDetIndent]
    PRIMARY KEY CLUSTERED ([Pk_BoxID], [Pk_Material], [BName], [Pk_Order], [OrderDate], [CustomerName], [Pk_Mill] ASC);
GO

-- Creating primary key on [Pk_Material], [Pk_Mill] in table 'Vw_PaperStock'
ALTER TABLE [dbo].[Vw_PaperStock]
ADD CONSTRAINT [PK_Vw_PaperStock]
    PRIMARY KEY CLUSTERED ([Pk_Material], [Pk_Mill] ASC);
GO

-- Creating primary key on [Fk_OrderNo], [Pk_Invoice] in table 'Vw_InvOrdQty'
ALTER TABLE [dbo].[Vw_InvOrdQty]
ADD CONSTRAINT [PK_Vw_InvOrdQty]
    PRIMARY KEY CLUSTERED ([Fk_OrderNo], [Pk_Invoice] ASC);
GO

-- Creating primary key on [Pk_ID], [Pk_JobCardID] in table 'Vw_FinishingQty_Wt'
ALTER TABLE [dbo].[Vw_FinishingQty_Wt]
ADD CONSTRAINT [PK_Vw_FinishingQty_Wt]
    PRIMARY KEY CLUSTERED ([Pk_ID], [Pk_JobCardID] ASC);
GO

-- Creating primary key on [Pk_ID] in table 'TonnValues'
ALTER TABLE [dbo].[TonnValues]
ADD CONSTRAINT [PK_TonnValues]
    PRIMARY KEY CLUSTERED ([Pk_ID] ASC);
GO

-- Creating primary key on [Pk_PONo], [VendorName], [Pk_Material], [Pk_InwardDet] in table 'Vw_PoBom'
ALTER TABLE [dbo].[Vw_PoBom]
ADD CONSTRAINT [PK_Vw_PoBom]
    PRIMARY KEY CLUSTERED ([Pk_PONo], [VendorName], [Pk_Material], [Pk_InwardDet] ASC);
GO

-- Creating primary key on [Pk_Material], [Pk_BoxID] in table 'Vw_GetPaperWtSum'
ALTER TABLE [dbo].[Vw_GetPaperWtSum]
ADD CONSTRAINT [PK_Vw_GetPaperWtSum]
    PRIMARY KEY CLUSTERED ([Pk_Material], [Pk_BoxID] ASC);
GO

-- Creating primary key on [Pk_StkID] in table 'Semi_FinishedGoodsStock'
ALTER TABLE [dbo].[Semi_FinishedGoodsStock]
ADD CONSTRAINT [PK_Semi_FinishedGoodsStock]
    PRIMARY KEY CLUSTERED ([Pk_StkID] ASC);
GO

-- Creating primary key on [Pk_Material], [Pk_BoxID], [Pk_PONo] in table 'Vw_PaperSummary'
ALTER TABLE [dbo].[Vw_PaperSummary]
ADD CONSTRAINT [PK_Vw_PaperSummary]
    PRIMARY KEY CLUSTERED ([Pk_Material], [Pk_BoxID], [Pk_PONo] ASC);
GO

-- Creating primary key on [Pk_Material], [Pk_PONo], [VendorName], [Pk_Vendor], [Pk_Mill] in table 'Vw_PO'
ALTER TABLE [dbo].[Vw_PO]
ADD CONSTRAINT [PK_Vw_PO]
    PRIMARY KEY CLUSTERED ([Pk_Material], [Pk_PONo], [VendorName], [Pk_Vendor], [Pk_Mill] ASC);
GO

-- Creating primary key on [Pk_MaterialOrderMasterId], [Pk_MaterialOrderDetailsId], [Pk_Material] in table 'Vw_MIndent'
ALTER TABLE [dbo].[Vw_MIndent]
ADD CONSTRAINT [PK_Vw_MIndent]
    PRIMARY KEY CLUSTERED ([Pk_MaterialOrderMasterId], [Pk_MaterialOrderDetailsId], [Pk_Material] ASC);
GO

-- Creating primary key on [Pk_StkID], [Name], [Pk_JobCardID] in table 'Vw_WIP_Stock'
ALTER TABLE [dbo].[Vw_WIP_Stock]
ADD CONSTRAINT [PK_Vw_WIP_Stock]
    PRIMARY KEY CLUSTERED ([Pk_StkID], [Name], [Pk_JobCardID] ASC);
GO

-- Creating primary key on [Pk_Inward], [Pk_Material], [Pk_InwardDet] in table 'Vw_InwardRep'
ALTER TABLE [dbo].[Vw_InwardRep]
ADD CONSTRAINT [PK_Vw_InwardRep]
    PRIMARY KEY CLUSTERED ([Pk_Inward], [Pk_Material], [Pk_InwardDet] ASC);
GO

-- Creating primary key on [Pk_JobCardID], [Expr1], [TotalQty], [CustomerName], [Pk_MaterialIssueDetailsID] in table 'Vw_JcIssues'
ALTER TABLE [dbo].[Vw_JcIssues]
ADD CONSTRAINT [PK_Vw_JcIssues]
    PRIMARY KEY CLUSTERED ([Pk_JobCardID], [Expr1], [TotalQty], [CustomerName], [Pk_MaterialIssueDetailsID] ASC);
GO

-- Creating primary key on [Pk_Order], [OrderDate], [CustomerName], [Name], [Pk_BoxID], [Pk_OrderChild] in table 'VwOrderRep'
ALTER TABLE [dbo].[VwOrderRep]
ADD CONSTRAINT [PK_VwOrderRep]
    PRIMARY KEY CLUSTERED ([Pk_Order], [OrderDate], [CustomerName], [Name], [Pk_BoxID], [Pk_OrderChild] ASC);
GO

-- Creating primary key on [Pk_Material], [Pk_BoxID], [Pk_PartPropertyID] in table 'Vw_PaperWtBox'
ALTER TABLE [dbo].[Vw_PaperWtBox]
ADD CONSTRAINT [PK_Vw_PaperWtBox]
    PRIMARY KEY CLUSTERED ([Pk_Material], [Pk_BoxID], [Pk_PartPropertyID] ASC);
GO

-- Creating primary key on [CustomerName], [Pk_Material] in table 'Vw_JobWorkStock'
ALTER TABLE [dbo].[Vw_JobWorkStock]
ADD CONSTRAINT [PK_Vw_JobWorkStock]
    PRIMARY KEY CLUSTERED ([CustomerName], [Pk_Material] ASC);
GO

-- Creating primary key on [Pk_Material], [DeliveryDate], [CustomerName], [Box_Name] in table 'Vw_NetPapReqd'
ALTER TABLE [dbo].[Vw_NetPapReqd]
ADD CONSTRAINT [PK_Vw_NetPapReqd]
    PRIMARY KEY CLUSTERED ([Pk_Material], [DeliveryDate], [CustomerName], [Box_Name] ASC);
GO

-- Creating primary key on [Pk_JobCardID], [BName], [TotalQty] in table 'Vw_JCIssReport'
ALTER TABLE [dbo].[Vw_JCIssReport]
ADD CONSTRAINT [PK_Vw_JCIssReport]
    PRIMARY KEY CLUSTERED ([Pk_JobCardID], [BName], [TotalQty] ASC);
GO

-- Creating primary key on [Pk_JobCardID], [BName], [TotalQty], [ReturnQty], [Pk_MaterialIssueDetailsID] in table 'Vw_JobBoxDetails'
ALTER TABLE [dbo].[Vw_JobBoxDetails]
ADD CONSTRAINT [PK_Vw_JobBoxDetails]
    PRIMARY KEY CLUSTERED ([Pk_JobCardID], [BName], [TotalQty], [ReturnQty], [Pk_MaterialIssueDetailsID] ASC);
GO

-- Creating primary key on [Pk_PaperStock], [Pk_JobCardID], [Pk_Material], [Pk_Mill], [CustomerName], [BoxName] in table 'Vw_AssignedStock'
ALTER TABLE [dbo].[Vw_AssignedStock]
ADD CONSTRAINT [PK_Vw_AssignedStock]
    PRIMARY KEY CLUSTERED ([Pk_PaperStock], [Pk_JobCardID], [Pk_Material], [Pk_Mill], [CustomerName], [BoxName] ASC);
GO

-- Creating primary key on [Pk_PaperStock], [Fk_JobCardID] in table 'vw_PaperRollStock'
ALTER TABLE [dbo].[vw_PaperRollStock]
ADD CONSTRAINT [PK_vw_PaperRollStock]
    PRIMARY KEY CLUSTERED ([Pk_PaperStock], [Fk_JobCardID] ASC);
GO

-- Creating primary key on [Pk_Order], [OrderDate], [CustomerName], [BoxName], [Ex_Stock], [Pk_Invoice] in table 'Vw_BalanceOrdQty'
ALTER TABLE [dbo].[Vw_BalanceOrdQty]
ADD CONSTRAINT [PK_Vw_BalanceOrdQty]
    PRIMARY KEY CLUSTERED ([Pk_Order], [OrderDate], [CustomerName], [BoxName], [Ex_Stock], [Pk_Invoice] ASC);
GO

-- Creating primary key on [Pk_ID] in table 'PartJobsReturns'
ALTER TABLE [dbo].[PartJobsReturns]
ADD CONSTRAINT [PK_PartJobsReturns]
    PRIMARY KEY CLUSTERED ([Pk_ID] ASC);
GO

-- Creating primary key on [Pk_ID], [CustomerName], [DelvID] in table 'Vw_JWInDelv'
ALTER TABLE [dbo].[Vw_JWInDelv]
ADD CONSTRAINT [PK_Vw_JWInDelv]
    PRIMARY KEY CLUSTERED ([Pk_ID], [CustomerName], [DelvID] ASC);
GO

-- Creating primary key on [Pk_JobCardID] in table 'JW_JobCardMaster'
ALTER TABLE [dbo].[JW_JobCardMaster]
ADD CONSTRAINT [PK_JW_JobCardMaster]
    PRIMARY KEY CLUSTERED ([Pk_JobCardID] ASC);
GO

-- Creating primary key on [Pk_JWStock] in table 'JWStocks'
ALTER TABLE [dbo].[JWStocks]
ADD CONSTRAINT [PK_JWStocks]
    PRIMARY KEY CLUSTERED ([Pk_JWStock] ASC);
GO

-- Creating primary key on [Pk_SrcID] in table 'Outsourcing'
ALTER TABLE [dbo].[Outsourcing]
ADD CONSTRAINT [PK_Outsourcing]
    PRIMARY KEY CLUSTERED ([Pk_SrcID] ASC);
GO

-- Creating primary key on [Pk_ID] in table 'JobWorkReceivables'
ALTER TABLE [dbo].[JobWorkReceivables]
ADD CONSTRAINT [PK_JobWorkReceivables]
    PRIMARY KEY CLUSTERED ([Pk_ID] ASC);
GO

-- Creating primary key on [Pk_Material], [Pk_ID], [CustomerName], [PkJobDetID] in table 'Vw_PartJobs'
ALTER TABLE [dbo].[Vw_PartJobs]
ADD CONSTRAINT [PK_Vw_PartJobs]
    PRIMARY KEY CLUSTERED ([Pk_Material], [Pk_ID], [CustomerName], [PkJobDetID] ASC);
GO

-- Creating primary key on [Pk_InwardDet] in table 'JW_RM_RetDet'
ALTER TABLE [dbo].[JW_RM_RetDet]
ADD CONSTRAINT [PK_JW_RM_RetDet]
    PRIMARY KEY CLUSTERED ([Pk_InwardDet] ASC);
GO

-- Creating primary key on [Pk_Inward] in table 'JW_RM_RetMast'
ALTER TABLE [dbo].[JW_RM_RetMast]
ADD CONSTRAINT [PK_JW_RM_RetMast]
    PRIMARY KEY CLUSTERED ([Pk_Inward] ASC);
GO

-- Creating primary key on [Pk_InwardDet] in table 'JWMat_InwdD'
ALTER TABLE [dbo].[JWMat_InwdD]
ADD CONSTRAINT [PK_JWMat_InwdD]
    PRIMARY KEY CLUSTERED ([Pk_InwardDet] ASC);
GO

-- Creating primary key on [Pk_Inward] in table 'JWMat_InwdM'
ALTER TABLE [dbo].[JWMat_InwdM]
ADD CONSTRAINT [PK_JWMat_InwdM]
    PRIMARY KEY CLUSTERED ([Pk_Inward] ASC);
GO

-- Creating primary key on [Pk_Material], [CustomerName] in table 'Vw_JWTotPaperStockList'
ALTER TABLE [dbo].[Vw_JWTotPaperStockList]
ADD CONSTRAINT [PK_Vw_JWTotPaperStockList]
    PRIMARY KEY CLUSTERED ([Pk_Material], [CustomerName] ASC);
GO

-- Creating primary key on [CustomerName], [Pk_Inward], [Pk_InwardDet], [Pk_Material] in table 'Vw_JW_RetRep'
ALTER TABLE [dbo].[Vw_JW_RetRep]
ADD CONSTRAINT [PK_Vw_JW_RetRep]
    PRIMARY KEY CLUSTERED ([CustomerName], [Pk_Inward], [Pk_InwardDet], [Pk_Material] ASC);
GO

-- Creating primary key on [Pk_Material], [Pk_Inward], [Pk_InwardDet], [VendorName] in table 'Vw_JWInwdRep'
ALTER TABLE [dbo].[Vw_JWInwdRep]
ADD CONSTRAINT [PK_Vw_JWInwdRep]
    PRIMARY KEY CLUSTERED ([Pk_Material], [Pk_Inward], [Pk_InwardDet], [VendorName] ASC);
GO

-- Creating primary key on [VendorName], [Pk_Vendor], [Pk_SrcID], [Pk_SrcDetID] in table 'Vw_OSTasks'
ALTER TABLE [dbo].[Vw_OSTasks]
ADD CONSTRAINT [PK_Vw_OSTasks]
    PRIMARY KEY CLUSTERED ([VendorName], [Pk_Vendor], [Pk_SrcID], [Pk_SrcDetID] ASC);
GO

-- Creating primary key on [Pk_Material], [Pk_Num] in table 'TabPPCRep'
ALTER TABLE [dbo].[TabPPCRep]
ADD CONSTRAINT [PK_TabPPCRep]
    PRIMARY KEY CLUSTERED ([Pk_Material], [Pk_Num] ASC);
GO

-- Creating primary key on [Pk_Materail], [Pk_Num] in table 'tempPPC'
ALTER TABLE [dbo].[tempPPC]
ADD CONSTRAINT [PK_tempPPC]
    PRIMARY KEY CLUSTERED ([Pk_Materail], [Pk_Num] ASC);
GO

-- Creating primary key on [Pk_PartPropertyID] in table 'Vw_PPC'
ALTER TABLE [dbo].[Vw_PPC]
ADD CONSTRAINT [PK_Vw_PPC]
    PRIMARY KEY CLUSTERED ([Pk_PartPropertyID] ASC);
GO

-- Creating primary key on [Pk_Customer] in table 'gen_Customer'
ALTER TABLE [dbo].[gen_Customer]
ADD CONSTRAINT [PK_gen_Customer]
    PRIMARY KEY CLUSTERED ([Pk_Customer] ASC);
GO

-- Creating primary key on [Pk_BoxSpecID] in table 'BoxSpecs'
ALTER TABLE [dbo].[BoxSpecs]
ADD CONSTRAINT [PK_BoxSpecs]
    PRIMARY KEY CLUSTERED ([Pk_BoxSpecID] ASC);
GO

-- Creating primary key on [CustomerName], [Pk_Order], [OrderDate], [Pk_DeliverySechedule] in table 'Vw_SchOrders'
ALTER TABLE [dbo].[Vw_SchOrders]
ADD CONSTRAINT [PK_Vw_SchOrders]
    PRIMARY KEY CLUSTERED ([CustomerName], [Pk_Order], [OrderDate], [Pk_DeliverySechedule] ASC);
GO

-- Creating primary key on [Pk_BoxID] in table 'BoxMaster'
ALTER TABLE [dbo].[BoxMaster]
ADD CONSTRAINT [PK_BoxMaster]
    PRIMARY KEY CLUSTERED ([Pk_BoxID] ASC);
GO

-- Creating primary key on [BoxPk_Documents] in table 'Box_Documents'
ALTER TABLE [dbo].[Box_Documents]
ADD CONSTRAINT [PK_Box_Documents]
    PRIMARY KEY CLUSTERED ([BoxPk_Documents] ASC);
GO

-- Creating primary key on [Pk_Id], [VendorName], [Pk_CharID], [Pk_IdDet] in table 'Vw_PaperCertificate'
ALTER TABLE [dbo].[Vw_PaperCertificate]
ADD CONSTRAINT [PK_Vw_PaperCertificate]
    PRIMARY KEY CLUSTERED ([Pk_Id], [VendorName], [Pk_CharID], [Pk_IdDet] ASC);
GO

-- Creating primary key on [CustomerName], [Pk_PartPropertyID] in table 'Vw_JCard'
ALTER TABLE [dbo].[Vw_JCard]
ADD CONSTRAINT [PK_Vw_JCard]
    PRIMARY KEY CLUSTERED ([CustomerName], [Pk_PartPropertyID] ASC);
GO

-- Creating primary key on [Quantity], [Pk_JobCardID], [OrderDate], [CustomerName] in table 'Vw_PaperConsumption'
ALTER TABLE [dbo].[Vw_PaperConsumption]
ADD CONSTRAINT [PK_Vw_PaperConsumption]
    PRIMARY KEY CLUSTERED ([Quantity], [Pk_JobCardID], [OrderDate], [CustomerName] ASC);
GO

-- Creating primary key on [Pk_MaterialOrderMasterId], [Pk_MaterialOrderDetailsId] in table 'Vw_IndentPOSearch'
ALTER TABLE [dbo].[Vw_IndentPOSearch]
ADD CONSTRAINT [PK_Vw_IndentPOSearch]
    PRIMARY KEY CLUSTERED ([Pk_MaterialOrderMasterId], [Pk_MaterialOrderDetailsId] ASC);
GO

-- Creating primary key on [Pk_BoxID], [BName], [CustomerName], [Pk_JobCardID], [ProdQty], [Pk_LayerID] in table 'Vw_SampleJC'
ALTER TABLE [dbo].[Vw_SampleJC]
ADD CONSTRAINT [PK_Vw_SampleJC]
    PRIMARY KEY CLUSTERED ([Pk_BoxID], [BName], [CustomerName], [Pk_JobCardID], [ProdQty], [Pk_LayerID] ASC);
GO

-- Creating primary key on [Pk_Material], [Pk_PONo] in table 'vw_PoCertList'
ALTER TABLE [dbo].[vw_PoCertList]
ADD CONSTRAINT [PK_vw_PoCertList]
    PRIMARY KEY CLUSTERED ([Pk_Material], [Pk_PONo] ASC);
GO

-- Creating primary key on [Pk_MaterialOrderDetailsId] in table 'Inv_MaterialIndentDetails'
ALTER TABLE [dbo].[Inv_MaterialIndentDetails]
ADD CONSTRAINT [PK_Inv_MaterialIndentDetails]
    PRIMARY KEY CLUSTERED ([Pk_MaterialOrderDetailsId] ASC);
GO

-- Creating primary key on [Pk_MaterialCategory], [Pk_Material], [Pk_Stock], [Fk_Tanent] in table 'Vw_Popup_MaterialIssue'
ALTER TABLE [dbo].[Vw_Popup_MaterialIssue]
ADD CONSTRAINT [PK_Vw_Popup_MaterialIssue]
    PRIMARY KEY CLUSTERED ([Pk_MaterialCategory], [Pk_Material], [Pk_Stock], [Fk_Tanent] ASC);
GO

-- Creating primary key on [Pk_PONo], [VendorName], [Pk_Id] in table 'Vw_PODet'
ALTER TABLE [dbo].[Vw_PODet]
ADD CONSTRAINT [PK_Vw_PODet]
    PRIMARY KEY CLUSTERED ([Pk_PONo], [VendorName], [Pk_Id] ASC);
GO

-- Creating primary key on [Name], [Pk_BoxID], [Pk_Order], [CustomerName], [Pk_StockID], [Pk_PartPropertyID] in table 'Vw_BillingBoxStocks'
ALTER TABLE [dbo].[Vw_BillingBoxStocks]
ADD CONSTRAINT [PK_Vw_BillingBoxStocks]
    PRIMARY KEY CLUSTERED ([Name], [Pk_BoxID], [Pk_Order], [CustomerName], [Pk_StockID], [Pk_PartPropertyID] ASC);
GO

-- Creating primary key on [Pk_PONo], [VendorName], [Pk_PODet] in table 'Vw_PoDetOthers'
ALTER TABLE [dbo].[Vw_PoDetOthers]
ADD CONSTRAINT [PK_Vw_PoDetOthers]
    PRIMARY KEY CLUSTERED ([Pk_PONo], [VendorName], [Pk_PODet] ASC);
GO

-- Creating primary key on [Pk_Stock] in table 'Vw_OthersStock'
ALTER TABLE [dbo].[Vw_OthersStock]
ADD CONSTRAINT [PK_Vw_OthersStock]
    PRIMARY KEY CLUSTERED ([Pk_Stock] ASC);
GO

-- Creating primary key on [Pk_ID_Det] in table 'ConsumableIssueDet'
ALTER TABLE [dbo].[ConsumableIssueDet]
ADD CONSTRAINT [PK_ConsumableIssueDet]
    PRIMARY KEY CLUSTERED ([Pk_ID_Det] ASC);
GO

-- Creating primary key on [Pk_ID] in table 'ConsumablesIssue'
ALTER TABLE [dbo].[ConsumablesIssue]
ADD CONSTRAINT [PK_ConsumablesIssue]
    PRIMARY KEY CLUSTERED ([Pk_ID] ASC);
GO

-- Creating primary key on [Pk_ID], [Pk_ID_Det] in table 'Vw_ConsIssueList'
ALTER TABLE [dbo].[Vw_ConsIssueList]
ADD CONSTRAINT [PK_Vw_ConsIssueList]
    PRIMARY KEY CLUSTERED ([Pk_ID], [Pk_ID_Det] ASC);
GO

-- Creating primary key on [Pk_Stock] in table 'Vw_ConsStockList'
ALTER TABLE [dbo].[Vw_ConsStockList]
ADD CONSTRAINT [PK_Vw_ConsStockList]
    PRIMARY KEY CLUSTERED ([Pk_Stock] ASC);
GO

-- Creating primary key on [Pk_MaterialCategory], [Pk_PONo], [Pk_Material], [Pk_PODet], [VendorName], [Pk_Vendor] in table 'Vw_ConsPO'
ALTER TABLE [dbo].[Vw_ConsPO]
ADD CONSTRAINT [PK_Vw_ConsPO]
    PRIMARY KEY CLUSTERED ([Pk_MaterialCategory], [Pk_PONo], [Pk_Material], [Pk_PODet], [VendorName], [Pk_Vendor] ASC);
GO

-- Creating primary key on [Pk_Inward] in table 'Vw_Mat_Inwd'
ALTER TABLE [dbo].[Vw_Mat_Inwd]
ADD CONSTRAINT [PK_Vw_Mat_Inwd]
    PRIMARY KEY CLUSTERED ([Pk_Inward] ASC);
GO

-- Creating primary key on [Pk_PONo], [Pk_PODet] in table 'Vw_PO_Pending_Inward'
ALTER TABLE [dbo].[Vw_PO_Pending_Inward]
ADD CONSTRAINT [PK_Vw_PO_Pending_Inward]
    PRIMARY KEY CLUSTERED ([Pk_PONo], [Pk_PODet] ASC);
GO

-- Creating primary key on [Pk_MaterialCategory], [Pk_Material], [Pk_Inward], [VendorName] in table 'Vw_MatInwdDetails'
ALTER TABLE [dbo].[Vw_MatInwdDetails]
ADD CONSTRAINT [PK_Vw_MatInwdDetails]
    PRIMARY KEY CLUSTERED ([Pk_MaterialCategory], [Pk_Material], [Pk_Inward], [VendorName] ASC);
GO

-- Creating primary key on [Pk_JobCardID], [Fk_Material], [IssQty], [Pk_MaterialIssueID], [CustomerName], [Pk_Order], [OrderDate] in table 'Vw_IssueDay'
ALTER TABLE [dbo].[Vw_IssueDay]
ADD CONSTRAINT [PK_Vw_IssueDay]
    PRIMARY KEY CLUSTERED ([Pk_JobCardID], [Fk_Material], [IssQty], [Pk_MaterialIssueID], [CustomerName], [Pk_Order], [OrderDate] ASC);
GO

-- Creating primary key on [Pk_WorkOrderDet] in table 'WorkOrderDetails'
ALTER TABLE [dbo].[WorkOrderDetails]
ADD CONSTRAINT [PK_WorkOrderDetails]
    PRIMARY KEY CLUSTERED ([Pk_WorkOrderDet] ASC);
GO

-- Creating primary key on [PK_WODocuments] in table 'WO_Documents'
ALTER TABLE [dbo].[WO_Documents]
ADD CONSTRAINT [PK_WO_Documents]
    PRIMARY KEY CLUSTERED ([PK_WODocuments] ASC);
GO

-- Creating primary key on [PkJobDetID] in table 'JobWorkChild'
ALTER TABLE [dbo].[JobWorkChild]
ADD CONSTRAINT [PK_JobWorkChild]
    PRIMARY KEY CLUSTERED ([PkJobDetID] ASC);
GO

-- Creating primary key on [CustomerName], [OrderDate], [Pk_Order], [Pk_JobCardID], [Name], [TotalQty], [Pk_BoxID] in table 'Vw__OpenJcList'
ALTER TABLE [dbo].[Vw__OpenJcList]
ADD CONSTRAINT [PK_Vw__OpenJcList]
    PRIMARY KEY CLUSTERED ([CustomerName], [OrderDate], [Pk_Order], [Pk_JobCardID], [Name], [TotalQty], [Pk_BoxID] ASC);
GO

-- Creating primary key on [WorkOrder_ID] in table 'WorkOrder'
ALTER TABLE [dbo].[WorkOrder]
ADD CONSTRAINT [PK_WorkOrder]
    PRIMARY KEY CLUSTERED ([WorkOrder_ID] ASC);
GO

-- Creating primary key on [Pk_MaterialIssueID] in table 'MaterialIssue'
ALTER TABLE [dbo].[MaterialIssue]
ADD CONSTRAINT [PK_MaterialIssue]
    PRIMARY KEY CLUSTERED ([Pk_MaterialIssueID] ASC);
GO

-- Creating primary key on [DeliveryDate], [Pk_BoxID], [Name], [Pk_Order], [OrderDate], [CustomerName], [Pk_DeliverySechedule], [Pk_PartPropertyID], [Pk_JobCardID], [WorkOrder_ID] in table 'Vw_IssueScheduleList'
ALTER TABLE [dbo].[Vw_IssueScheduleList]
ADD CONSTRAINT [PK_Vw_IssueScheduleList]
    PRIMARY KEY CLUSTERED ([DeliveryDate], [Pk_BoxID], [Name], [Pk_Order], [OrderDate], [CustomerName], [Pk_DeliverySechedule], [Pk_PartPropertyID], [Pk_JobCardID], [WorkOrder_ID] ASC);
GO

-- Creating primary key on [Pk_JobCardID] in table 'JobCardMaster'
ALTER TABLE [dbo].[JobCardMaster]
ADD CONSTRAINT [PK_JobCardMaster]
    PRIMARY KEY CLUSTERED ([Pk_JobCardID] ASC);
GO

-- Creating primary key on [Pk_Order] in table 'gen_Order'
ALTER TABLE [dbo].[gen_Order]
ADD CONSTRAINT [PK_gen_Order]
    PRIMARY KEY CLUSTERED ([Pk_Order] ASC);
GO

-- Creating primary key on [SQty], [Pk_PONo], [VendorName], [Pk_Vendor], [Pk_PODet] in table 'Vw_POCertificate'
ALTER TABLE [dbo].[Vw_POCertificate]
ADD CONSTRAINT [PK_Vw_POCertificate]
    PRIMARY KEY CLUSTERED ([SQty], [Pk_PONo], [VendorName], [Pk_Vendor], [Pk_PODet] ASC);
GO

-- Creating primary key on [Pk_ID], [Pk_JobCardID], [Pk_Process] in table 'Vw_ProcessWastage'
ALTER TABLE [dbo].[Vw_ProcessWastage]
ADD CONSTRAINT [PK_Vw_ProcessWastage]
    PRIMARY KEY CLUSTERED ([Pk_ID], [Pk_JobCardID], [Pk_Process] ASC);
GO

-- Creating primary key on [Pk_PONo], [VendorName], [GSM], [BF], [Pk_Vendor], [Pk_Mill], [Deckle], [Pk_PODet] in table 'Vw_PoGetRec'
ALTER TABLE [dbo].[Vw_PoGetRec]
ADD CONSTRAINT [PK_Vw_PoGetRec]
    PRIMARY KEY CLUSTERED ([Pk_PONo], [VendorName], [GSM], [BF], [Pk_Vendor], [Pk_Mill], [Deckle], [Pk_PODet] ASC);
GO

-- Creating primary key on [Pk_JobCardID], [TotalQty], [CustomerName], [Pk_Order], [Name], [BType] in table 'Vw_InProcessJC'
ALTER TABLE [dbo].[Vw_InProcessJC]
ADD CONSTRAINT [PK_Vw_InProcessJC]
    PRIMARY KEY CLUSTERED ([Pk_JobCardID], [TotalQty], [CustomerName], [Pk_Order], [Name], [BType] ASC);
GO

-- Creating primary key on [Pk_Order] in table 'Vw_Sch_Ord_Pending'
ALTER TABLE [dbo].[Vw_Sch_Ord_Pending]
ADD CONSTRAINT [PK_Vw_Sch_Ord_Pending]
    PRIMARY KEY CLUSTERED ([Pk_Order] ASC);
GO

-- Creating primary key on [Pk_BoxID], [Name], [Pk_StockID], [Expr1], [CustomerName], [Pk_PartPropertyID] in table 'Vw_BoxStock'
ALTER TABLE [dbo].[Vw_BoxStock]
ADD CONSTRAINT [PK_Vw_BoxStock]
    PRIMARY KEY CLUSTERED ([Pk_BoxID], [Name], [Pk_StockID], [Expr1], [CustomerName], [Pk_PartPropertyID] ASC);
GO

-- Creating primary key on [Pk_BoxID], [Pk_Order] in table 'Vw_TSchQty'
ALTER TABLE [dbo].[Vw_TSchQty]
ADD CONSTRAINT [PK_Vw_TSchQty]
    PRIMARY KEY CLUSTERED ([Pk_BoxID], [Pk_Order] ASC);
GO

-- Creating primary key on [Pk_InwardDet] in table 'MaterialInwardD'
ALTER TABLE [dbo].[MaterialInwardD]
ADD CONSTRAINT [PK_MaterialInwardD]
    PRIMARY KEY CLUSTERED ([Pk_InwardDet] ASC);
GO

-- Creating primary key on [Pk_Order], [OrderDate], [CustomerName], [Name] in table 'Vw_UnScheduled_Orders'
ALTER TABLE [dbo].[Vw_UnScheduled_Orders]
ADD CONSTRAINT [PK_Vw_UnScheduled_Orders]
    PRIMARY KEY CLUSTERED ([Pk_Order], [OrderDate], [CustomerName], [Name] ASC);
GO

-- Creating primary key on [Pk_Order], [OrderDate], [CustomerName] in table 'Vw_UnSchOrders'
ALTER TABLE [dbo].[Vw_UnSchOrders]
ADD CONSTRAINT [PK_Vw_UnSchOrders]
    PRIMARY KEY CLUSTERED ([Pk_Order], [OrderDate], [CustomerName] ASC);
GO

-- Creating primary key on [Pk_JobCardID], [PlanQty] in table 'Vw_SchAndPrd'
ALTER TABLE [dbo].[Vw_SchAndPrd]
ADD CONSTRAINT [PK_Vw_SchAndPrd]
    PRIMARY KEY CLUSTERED ([Pk_JobCardID], [PlanQty] ASC);
GO

-- Creating primary key on [Name], [Pk_PartPropertyID], [Pk_LayerID], [Pk_BoxID], [CustomerName], [Pk_BoxSpecID], [BType], [Pk_Material], [Pk_Customer], [Pk_BoxTypeID], [Pk_FluteID] in table 'Vw_BoxDet'
ALTER TABLE [dbo].[Vw_BoxDet]
ADD CONSTRAINT [PK_Vw_BoxDet]
    PRIMARY KEY CLUSTERED ([Name], [Pk_PartPropertyID], [Pk_LayerID], [Pk_BoxID], [CustomerName], [Pk_BoxSpecID], [BType], [Pk_Material], [Pk_Customer], [Pk_BoxTypeID], [Pk_FluteID] ASC);
GO

-- Creating primary key on [Pk_PartPropertyID] in table 'ItemPartProperty'
ALTER TABLE [dbo].[ItemPartProperty]
ADD CONSTRAINT [PK_ItemPartProperty]
    PRIMARY KEY CLUSTERED ([Pk_PartPropertyID] ASC);
GO

-- Creating primary key on [Pk_JobCardID], [Pk_ID], [CustomerName], [Name], [TotalQty] in table 'Vw_JobProcessSteps'
ALTER TABLE [dbo].[Vw_JobProcessSteps]
ADD CONSTRAINT [PK_Vw_JobProcessSteps]
    PRIMARY KEY CLUSTERED ([Pk_JobCardID], [Pk_ID], [CustomerName], [Name], [TotalQty] ASC);
GO

-- Creating primary key on [Pk_InwDet] in table 'MatInwD'
ALTER TABLE [dbo].[MatInwD]
ADD CONSTRAINT [PK_MatInwD]
    PRIMARY KEY CLUSTERED ([Pk_InwDet] ASC);
GO

-- Creating primary key on [Pk_Inwd] in table 'MatInwM'
ALTER TABLE [dbo].[MatInwM]
ADD CONSTRAINT [PK_MatInwM]
    PRIMARY KEY CLUSTERED ([Pk_Inwd] ASC);
GO

-- Creating primary key on [Name], [Pk_PartPropertyID], [Pk_BoxID], [BType], [Pk_BoxSpecID], [CustomerName], [Pk_Customer] in table 'Vw_BoxSpec'
ALTER TABLE [dbo].[Vw_BoxSpec]
ADD CONSTRAINT [PK_Vw_BoxSpec]
    PRIMARY KEY CLUSTERED ([Name], [Pk_PartPropertyID], [Pk_BoxID], [BType], [Pk_BoxSpecID], [CustomerName], [Pk_Customer] ASC);
GO

-- Creating primary key on [Pk_Invoice], [CustomerName], [Fk_OrderNo], [Pk_Inv_Details], [Pk_Customer] in table 'Vw_Sales_Details'
ALTER TABLE [dbo].[Vw_Sales_Details]
ADD CONSTRAINT [PK_Vw_Sales_Details]
    PRIMARY KEY CLUSTERED ([Pk_Invoice], [CustomerName], [Fk_OrderNo], [Pk_Inv_Details], [Pk_Customer] ASC);
GO

-- Creating primary key on [Pk_Order], [OrderDate], [CustomerName], [Pk_Customer], [Name], [Pk_OrderChild] in table 'Vw_OrderList'
ALTER TABLE [dbo].[Vw_OrderList]
ADD CONSTRAINT [PK_Vw_OrderList]
    PRIMARY KEY CLUSTERED ([Pk_Order], [OrderDate], [CustomerName], [Pk_Customer], [Name], [Pk_OrderChild] ASC);
GO

-- Creating primary key on [Pk_OrderChild] in table 'Gen_OrderChild'
ALTER TABLE [dbo].[Gen_OrderChild]
ADD CONSTRAINT [PK_Gen_OrderChild]
    PRIMARY KEY CLUSTERED ([Pk_OrderChild] ASC);
GO

-- Creating primary key on [Pk_Material], [CustomerName], [Pk_Order], [OrderDate] in table 'Vw_Others_OrderList'
ALTER TABLE [dbo].[Vw_Others_OrderList]
ADD CONSTRAINT [PK_Vw_Others_OrderList]
    PRIMARY KEY CLUSTERED ([Pk_Material], [CustomerName], [Pk_Order], [OrderDate] ASC);
GO

-- Creating primary key on [Pk_Material] in table 'Vw_Others_Order'
ALTER TABLE [dbo].[Vw_Others_Order]
ADD CONSTRAINT [PK_Vw_Others_Order]
    PRIMARY KEY CLUSTERED ([Pk_Material] ASC);
GO

-- Creating primary key on [Pk_Stock] in table 'Vw_OthersBill'
ALTER TABLE [dbo].[Vw_OthersBill]
ADD CONSTRAINT [PK_Vw_OthersBill]
    PRIMARY KEY CLUSTERED ([Pk_Stock] ASC);
GO

-- Creating primary key on [Pk_PaperStock], [Pk_Material] in table 'Vw_Age'
ALTER TABLE [dbo].[Vw_Age]
ADD CONSTRAINT [PK_Vw_Age]
    PRIMARY KEY CLUSTERED ([Pk_PaperStock], [Pk_Material] ASC);
GO

-- Creating primary key on [Pk_PONo] in table 'PurchaseOrderM'
ALTER TABLE [dbo].[PurchaseOrderM]
ADD CONSTRAINT [PK_PurchaseOrderM]
    PRIMARY KEY CLUSTERED ([Pk_PONo] ASC);
GO

-- Creating primary key on [Pk_MaterialIssueID] in table 'SampMaterialIssue'
ALTER TABLE [dbo].[SampMaterialIssue]
ADD CONSTRAINT [PK_SampMaterialIssue]
    PRIMARY KEY CLUSTERED ([Pk_MaterialIssueID] ASC);
GO

-- Creating primary key on [Pk_MaterialIssueDetailsID] in table 'SampMaterialIssueDetails'
ALTER TABLE [dbo].[SampMaterialIssueDetails]
ADD CONSTRAINT [PK_SampMaterialIssueDetails]
    PRIMARY KEY CLUSTERED ([Pk_MaterialIssueDetailsID] ASC);
GO

-- Creating primary key on [Pk_BoxID], [Name], [Pk_JobCardID], [Pk_MaterialIssueID], [Pk_Material], [Quantity] in table 'Vw_Samp_MatIssue'
ALTER TABLE [dbo].[Vw_Samp_MatIssue]
ADD CONSTRAINT [PK_Vw_Samp_MatIssue]
    PRIMARY KEY CLUSTERED ([Pk_BoxID], [Name], [Pk_JobCardID], [Pk_MaterialIssueID], [Pk_Material], [Quantity] ASC);
GO

-- Creating primary key on [Pk_BoxID], [BName], [Pk_Order], [OrderDate], [CustomerName], [DeliveryDate], [Pk_JobCardID], [ProdQty], [Pk_LayerID] in table 'Vw_JCReport'
ALTER TABLE [dbo].[Vw_JCReport]
ADD CONSTRAINT [PK_Vw_JCReport]
    PRIMARY KEY CLUSTERED ([Pk_BoxID], [BName], [Pk_Order], [OrderDate], [CustomerName], [DeliveryDate], [Pk_JobCardID], [ProdQty], [Pk_LayerID] ASC);
GO

-- Creating primary key on [Pk_BoxID], [Pk_LayerID], [Pk_PartPropertyID], [Pk_JobCardID], [BoxPk], [Name] in table 'Vw_IssuesDev'
ALTER TABLE [dbo].[Vw_IssuesDev]
ADD CONSTRAINT [PK_Vw_IssuesDev]
    PRIMARY KEY CLUSTERED ([Pk_BoxID], [Pk_LayerID], [Pk_PartPropertyID], [Pk_JobCardID], [BoxPk], [Name] ASC);
GO

-- Creating primary key on [Pk_MaterialIssueID], [Pk_Material], [Pk_JobCardID], [CustomerName], [Quantity], [Pk_BoxID] in table 'Vw_PapDistIssueList'
ALTER TABLE [dbo].[Vw_PapDistIssueList]
ADD CONSTRAINT [PK_Vw_PapDistIssueList]
    PRIMARY KEY CLUSTERED ([Pk_MaterialIssueID], [Pk_Material], [Pk_JobCardID], [CustomerName], [Quantity], [Pk_BoxID] ASC);
GO

-- Creating primary key on [Pk_Process], [Pk_ID] in table 'Vw_Upd_Stock_Jobprocess'
ALTER TABLE [dbo].[Vw_Upd_Stock_Jobprocess]
ADD CONSTRAINT [PK_Vw_Upd_Stock_Jobprocess]
    PRIMARY KEY CLUSTERED ([Pk_Process], [Pk_ID] ASC);
GO

-- Creating primary key on [Pk_ID] in table 'SampJobProcess'
ALTER TABLE [dbo].[SampJobProcess]
ADD CONSTRAINT [PK_SampJobProcess]
    PRIMARY KEY CLUSTERED ([Pk_ID] ASC);
GO

-- Creating primary key on [Pk_JProcessID] in table 'SampJProcess'
ALTER TABLE [dbo].[SampJProcess]
ADD CONSTRAINT [PK_SampJProcess]
    PRIMARY KEY CLUSTERED ([Pk_JProcessID] ASC);
GO

-- Creating primary key on [Pk_IssueReturnMasterId], [Pk_Material], [Pk_IssueReturnDetailsId] in table 'Vw_Iss_Return'
ALTER TABLE [dbo].[Vw_Iss_Return]
ADD CONSTRAINT [PK_Vw_Iss_Return]
    PRIMARY KEY CLUSTERED ([Pk_IssueReturnMasterId], [Pk_Material], [Pk_IssueReturnDetailsId] ASC);
GO

-- Creating primary key on [Pk_PONo], [VendorName], [GSM], [BF], [Pk_Vendor], [Pk_Mill], [Deckle], [Pk_PODet] in table 'Vw_OpenPO'
ALTER TABLE [dbo].[Vw_OpenPO]
ADD CONSTRAINT [PK_Vw_OpenPO]
    PRIMARY KEY CLUSTERED ([Pk_PONo], [VendorName], [GSM], [BF], [Pk_Vendor], [Pk_Mill], [Deckle], [Pk_PODet] ASC);
GO

-- Creating primary key on [CustomerName], [Pk_Material], [Pk_StockID], [Pk_Customer] in table 'Vw_JWStock'
ALTER TABLE [dbo].[Vw_JWStock]
ADD CONSTRAINT [PK_Vw_JWStock]
    PRIMARY KEY CLUSTERED ([CustomerName], [Pk_Material], [Pk_StockID], [Pk_Customer] ASC);
GO

-- Creating primary key on [Pk_Invoice] in table 'Inv_Billing'
ALTER TABLE [dbo].[Inv_Billing]
ADD CONSTRAINT [PK_Inv_Billing]
    PRIMARY KEY CLUSTERED ([Pk_Invoice] ASC);
GO

-- Creating primary key on [Pk_ID] in table 'JobProcess'
ALTER TABLE [dbo].[JobProcess]
ADD CONSTRAINT [PK_JobProcess]
    PRIMARY KEY CLUSTERED ([Pk_ID] ASC);
GO

-- Creating primary key on [Pk_Inward] in table 'MaterialInwardM'
ALTER TABLE [dbo].[MaterialInwardM]
ADD CONSTRAINT [PK_MaterialInwardM]
    PRIMARY KEY CLUSTERED ([Pk_Inward] ASC);
GO

-- Creating primary key on [Pk_Material] in table 'Vw_OthersMaterial'
ALTER TABLE [dbo].[Vw_OthersMaterial]
ADD CONSTRAINT [PK_Vw_OthersMaterial]
    PRIMARY KEY CLUSTERED ([Pk_Material] ASC);
GO

-- Creating primary key on [Pk_Inv_Details] in table 'Inv_BillingDetails'
ALTER TABLE [dbo].[Inv_BillingDetails]
ADD CONSTRAINT [PK_Inv_BillingDetails]
    PRIMARY KEY CLUSTERED ([Pk_Inv_Details] ASC);
GO

-- Creating primary key on [CustomerName], [Fk_OrderNo], [Pk_Invoice], [Pk_Inv_Details], [Pk_Customer], [PinCode], [LandLine], [CustomerAddress], [StateName], [CityName], [CountryName], [Name], [Pk_PartPropertyID], [Pk_BoxID], [Email], [ContactPerson] in table 'VwInvoice'
ALTER TABLE [dbo].[VwInvoice]
ADD CONSTRAINT [PK_VwInvoice]
    PRIMARY KEY CLUSTERED ([CustomerName], [Fk_OrderNo], [Pk_Invoice], [Pk_Inv_Details], [Pk_Customer], [PinCode], [LandLine], [CustomerAddress], [StateName], [CityName], [CountryName], [Name], [Pk_PartPropertyID], [Pk_BoxID], [Email], [ContactPerson] ASC);
GO

-- Creating primary key on [Pk_PONo], [VendorName], [Email], [Address_No_Street], [PinCode], [OfficeContactNumber], [Pk_PODet] in table 'Vw_POReport'
ALTER TABLE [dbo].[Vw_POReport]
ADD CONSTRAINT [PK_Vw_POReport]
    PRIMARY KEY CLUSTERED ([Pk_PONo], [VendorName], [Email], [Address_No_Street], [PinCode], [OfficeContactNumber], [Pk_PODet] ASC);
GO

-- Creating primary key on [Pk_Order], [Pk_BoxID], [Name], [Pk_OrderChild], [Pk_PartPropertyID], [OrderDate], [CustomerName] in table 'Vw_BoxOrder'
ALTER TABLE [dbo].[Vw_BoxOrder]
ADD CONSTRAINT [PK_Vw_BoxOrder]
    PRIMARY KEY CLUSTERED ([Pk_Order], [Pk_BoxID], [Name], [Pk_OrderChild], [Pk_PartPropertyID], [OrderDate], [CustomerName] ASC);
GO

-- Creating primary key on [Pk_GRNDetId] in table 'GRN_Details'
ALTER TABLE [dbo].[GRN_Details]
ADD CONSTRAINT [PK_GRN_Details]
    PRIMARY KEY CLUSTERED ([Pk_GRNDetId] ASC);
GO

-- Creating primary key on [Pk_GRNId] in table 'GRN_Mast'
ALTER TABLE [dbo].[GRN_Mast]
ADD CONSTRAINT [PK_GRN_Mast]
    PRIMARY KEY CLUSTERED ([Pk_GRNId] ASC);
GO

-- Creating primary key on [Pk_Inward], [Fk_Material] in table 'Vw_GRNMatData'
ALTER TABLE [dbo].[Vw_GRNMatData]
ADD CONSTRAINT [PK_Vw_GRNMatData]
    PRIMARY KEY CLUSTERED ([Pk_Inward], [Fk_Material] ASC);
GO

-- Creating primary key on [Pk_GRNId], [Pk_Inward] in table 'Vw_GrnInwdDet'
ALTER TABLE [dbo].[Vw_GrnInwdDet]
ADD CONSTRAINT [PK_Vw_GrnInwdDet]
    PRIMARY KEY CLUSTERED ([Pk_GRNId], [Pk_Inward] ASC);
GO

-- Creating primary key on [Pk_GRNId], [Pk_Inward], [Pk_GRNDetId] in table 'Vw_GRNMaterialDet'
ALTER TABLE [dbo].[Vw_GRNMaterialDet]
ADD CONSTRAINT [PK_Vw_GRNMaterialDet]
    PRIMARY KEY CLUSTERED ([Pk_GRNId], [Pk_Inward], [Pk_GRNDetId] ASC);
GO

-- Creating primary key on [CustomerName], [Fk_OrderNo], [Pk_Invoice], [Pk_Inv_Details], [Pk_Customer], [PinCode], [LandLine], [CustomerAddress], [StateName], [CityName], [CountryName], [Email], [ContactPerson] in table 'Vw_InvRM'
ALTER TABLE [dbo].[Vw_InvRM]
ADD CONSTRAINT [PK_Vw_InvRM]
    PRIMARY KEY CLUSTERED ([CustomerName], [Fk_OrderNo], [Pk_Invoice], [Pk_Inv_Details], [Pk_Customer], [PinCode], [LandLine], [CustomerAddress], [StateName], [CityName], [CountryName], [Email], [ContactPerson] ASC);
GO

-- Creating primary key on [Pk_Order], [OrderDate], [Pk_Invoice], [Pk_Inv_Details] in table 'Vw_SOrderTracker'
ALTER TABLE [dbo].[Vw_SOrderTracker]
ADD CONSTRAINT [PK_Vw_SOrderTracker]
    PRIMARY KEY CLUSTERED ([Pk_Order], [OrderDate], [Pk_Invoice], [Pk_Inv_Details] ASC);
GO

-- Creating primary key on [Pk_Order], [OrderDate], [Name], [Pk_Invoice], [Pk_BoxID], [CustomerName] in table 'Vw_SalesOrderTracker'
ALTER TABLE [dbo].[Vw_SalesOrderTracker]
ADD CONSTRAINT [PK_Vw_SalesOrderTracker]
    PRIMARY KEY CLUSTERED ([Pk_Order], [OrderDate], [Name], [Pk_Invoice], [Pk_BoxID], [CustomerName] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Fk_DetailedEstimation] in table 'est_EstimationPart'
ALTER TABLE [dbo].[est_EstimationPart]
ADD CONSTRAINT [FK_est_EstimationPart_est_Estimation1]
    FOREIGN KEY ([Fk_DetailedEstimation])
    REFERENCES [dbo].[est_Estimation]
        ([Pk_Estimate])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_est_EstimationPart_est_Estimation1'
CREATE INDEX [IX_FK_est_EstimationPart_est_Estimation1]
ON [dbo].[est_EstimationPart]
    ([Fk_DetailedEstimation]);
GO

-- Creating foreign key on [Fk_Estimation] in table 'est_Layer'
ALTER TABLE [dbo].[est_Layer]
ADD CONSTRAINT [FK_est_Layer_est_EstimationPart1]
    FOREIGN KEY ([Fk_Estimation])
    REFERENCES [dbo].[est_EstimationPart]
        ([Pk_DetailedEstimationPart])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_est_Layer_est_EstimationPart1'
CREATE INDEX [IX_FK_est_Layer_est_EstimationPart1]
ON [dbo].[est_Layer]
    ([Fk_Estimation]);
GO

-- Creating foreign key on [Fk_DetailedEstimation] in table 'est_EstimateAdditionalCost'
ALTER TABLE [dbo].[est_EstimateAdditionalCost]
ADD CONSTRAINT [FK_est_EstimateAdditionalCost_est_Estimation]
    FOREIGN KEY ([Fk_DetailedEstimation])
    REFERENCES [dbo].[C_est_Estimation]
        ([PK_DetailedEstimate])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_est_EstimateAdditionalCost_est_Estimation'
CREATE INDEX [IX_FK_est_EstimateAdditionalCost_est_Estimation]
ON [dbo].[est_EstimateAdditionalCost]
    ([Fk_DetailedEstimation]);
GO

-- Creating foreign key on [Fk_DetailedEstimation] in table 'est_EstimateTax'
ALTER TABLE [dbo].[est_EstimateTax]
ADD CONSTRAINT [FK_est_EstimateTax_est_Estimation]
    FOREIGN KEY ([Fk_DetailedEstimation])
    REFERENCES [dbo].[C_est_Estimation]
        ([PK_DetailedEstimate])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_est_EstimateTax_est_Estimation'
CREATE INDEX [IX_FK_est_EstimateTax_est_Estimation]
ON [dbo].[est_EstimateTax]
    ([Fk_DetailedEstimation]);
GO

-- Creating foreign key on [FK_DetailedEstimate] in table 'C_est_EstimationPart'
ALTER TABLE [dbo].[C_est_EstimationPart]
ADD CONSTRAINT [FK_est_EstimationPart_est_Estimation]
    FOREIGN KEY ([FK_DetailedEstimate])
    REFERENCES [dbo].[C_est_Estimation]
        ([PK_DetailedEstimate])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_est_EstimationPart_est_Estimation'
CREATE INDEX [IX_FK_est_EstimationPart_est_Estimation]
ON [dbo].[C_est_EstimationPart]
    ([FK_DetailedEstimate]);
GO

-- Creating foreign key on [Fk_DetailedEstimationPart] in table 'C_est_Layer'
ALTER TABLE [dbo].[C_est_Layer]
ADD CONSTRAINT [FK_est_Layer_est_EstimationPart]
    FOREIGN KEY ([Fk_DetailedEstimationPart])
    REFERENCES [dbo].[C_est_EstimationPart]
        ([Pk_DetailedEstimationPart])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_est_Layer_est_EstimationPart'
CREATE INDEX [IX_FK_est_Layer_est_EstimationPart]
ON [dbo].[C_est_Layer]
    ([Fk_DetailedEstimationPart]);
GO

-- Creating foreign key on [Fk_AccountHeadId] in table 'Voucher'
ALTER TABLE [dbo].[Voucher]
ADD CONSTRAINT [FK_Voucher_acc_AccountHeads]
    FOREIGN KEY ([Fk_AccountHeadId])
    REFERENCES [dbo].[acc_AccountHeads]
        ([Pk_AccountHeadId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Voucher_acc_AccountHeads'
CREATE INDEX [IX_FK_Voucher_acc_AccountHeads]
ON [dbo].[Voucher]
    ([Fk_AccountHeadId]);
GO

-- Creating foreign key on [Fk_AccountTypeID] in table 'Bank'
ALTER TABLE [dbo].[Bank]
ADD CONSTRAINT [FK_Bank_AccountType]
    FOREIGN KEY ([Fk_AccountTypeID])
    REFERENCES [dbo].[AccountType]
        ([Pk_AccountTypeID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Bank_AccountType'
CREATE INDEX [IX_FK_Bank_AccountType]
ON [dbo].[Bank]
    ([Fk_AccountTypeID]);
GO

-- Creating foreign key on [Fk_RegisterMain] in table 'att_RegisterAttendance'
ALTER TABLE [dbo].[att_RegisterAttendance]
ADD CONSTRAINT [FK_att_RegisterAttendance_att_RegisterMain]
    FOREIGN KEY ([Fk_RegisterMain])
    REFERENCES [dbo].[att_RegisterMain]
        ([Pk_RegisterMain])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_att_RegisterAttendance_att_RegisterMain'
CREATE INDEX [IX_FK_att_RegisterAttendance_att_RegisterMain]
ON [dbo].[att_RegisterAttendance]
    ([Fk_RegisterMain]);
GO

-- Creating foreign key on [Fk_Bank] in table 'Payment'
ALTER TABLE [dbo].[Payment]
ADD CONSTRAINT [FK_Payment_Bank]
    FOREIGN KEY ([Fk_Bank])
    REFERENCES [dbo].[Bank]
        ([Pk_BankID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Payment_Bank'
CREATE INDEX [IX_FK_Payment_Bank]
ON [dbo].[Payment]
    ([Fk_Bank]);
GO

-- Creating foreign key on [Fk_Tanent] in table 'Branch'
ALTER TABLE [dbo].[Branch]
ADD CONSTRAINT [FK_Branch_wgTenant]
    FOREIGN KEY ([Fk_Tanent])
    REFERENCES [dbo].[wgTenant]
        ([Pk_Tanent])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Branch_wgTenant'
CREATE INDEX [IX_FK_Branch_wgTenant]
ON [dbo].[Branch]
    ([Fk_Tanent]);
GO

-- Creating foreign key on [Fk_Branch] in table 'Inv_MaterialIndentMaster'
ALTER TABLE [dbo].[Inv_MaterialIndentMaster]
ADD CONSTRAINT [FK_Inv_MaterialIndentMaster_Branch]
    FOREIGN KEY ([Fk_Branch])
    REFERENCES [dbo].[Branch]
        ([Pk_BranchID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Inv_MaterialIndentMaster_Branch'
CREATE INDEX [IX_FK_Inv_MaterialIndentMaster_Branch]
ON [dbo].[Inv_MaterialIndentMaster]
    ([Fk_Branch]);
GO

-- Creating foreign key on [Fk_FromBranchID] in table 'StockTransfer'
ALTER TABLE [dbo].[StockTransfer]
ADD CONSTRAINT [FK_StockTransfer_Branch]
    FOREIGN KEY ([Fk_FromBranchID])
    REFERENCES [dbo].[Branch]
        ([Pk_BranchID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_StockTransfer_Branch'
CREATE INDEX [IX_FK_StockTransfer_Branch]
ON [dbo].[StockTransfer]
    ([Fk_FromBranchID]);
GO

-- Creating foreign key on [Fk_ToBranchID] in table 'StockTransfer'
ALTER TABLE [dbo].[StockTransfer]
ADD CONSTRAINT [FK_StockTransfer_Branch1]
    FOREIGN KEY ([Fk_ToBranchID])
    REFERENCES [dbo].[Branch]
        ([Pk_BranchID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_StockTransfer_Branch1'
CREATE INDEX [IX_FK_StockTransfer_Branch1]
ON [dbo].[StockTransfer]
    ([Fk_ToBranchID]);
GO

-- Creating foreign key on [Fk_Status] in table 'CustomerComplaints'
ALTER TABLE [dbo].[CustomerComplaints]
ADD CONSTRAINT [FK_CustomerComplaints_wfStates]
    FOREIGN KEY ([Fk_Status])
    REFERENCES [dbo].[wfStates]
        ([Pk_State])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CustomerComplaints_wfStates'
CREATE INDEX [IX_FK_CustomerComplaints_wfStates]
ON [dbo].[CustomerComplaints]
    ([Fk_Status]);
GO

-- Creating foreign key on [Fk_Tanent] in table 'CustomerComplaints'
ALTER TABLE [dbo].[CustomerComplaints]
ADD CONSTRAINT [FK_CustomerComplaints_wgTenant]
    FOREIGN KEY ([Fk_Tanent])
    REFERENCES [dbo].[wgTenant]
        ([Pk_Tanent])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CustomerComplaints_wgTenant'
CREATE INDEX [IX_FK_CustomerComplaints_wgTenant]
ON [dbo].[CustomerComplaints]
    ([Fk_Tanent]);
GO

-- Creating foreign key on [Fk_FinishedGoodsId] in table 'FinishedGoodsShippingDetailswwws'
ALTER TABLE [dbo].[FinishedGoodsShippingDetailswwws]
ADD CONSTRAINT [FK_FinishedGoodsShippingDetails_FinishedGoodsMaster]
    FOREIGN KEY ([Fk_FinishedGoodsId])
    REFERENCES [dbo].[FinishedGoodsMasterwwws]
        ([Pk_FinishedGoodsId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_FinishedGoodsShippingDetails_FinishedGoodsMaster'
CREATE INDEX [IX_FK_FinishedGoodsShippingDetails_FinishedGoodsMaster]
ON [dbo].[FinishedGoodsShippingDetailswwws]
    ([Fk_FinishedGoodsId]);
GO

-- Creating foreign key on [Fk_StateID] in table 'gen_City'
ALTER TABLE [dbo].[gen_City]
ADD CONSTRAINT [FK_gen_City_gen_City]
    FOREIGN KEY ([Fk_StateID])
    REFERENCES [dbo].[gen_State]
        ([Pk_StateID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_gen_City_gen_City'
CREATE INDEX [IX_FK_gen_City_gen_City]
ON [dbo].[gen_City]
    ([Fk_StateID]);
GO

-- Creating foreign key on [Country] in table 'gen_City'
ALTER TABLE [dbo].[gen_City]
ADD CONSTRAINT [FK_gen_City_gen_Country]
    FOREIGN KEY ([Country])
    REFERENCES [dbo].[gen_Country]
        ([Pk_Country])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_gen_City_gen_Country'
CREATE INDEX [IX_FK_gen_City_gen_Country]
ON [dbo].[gen_City]
    ([Country]);
GO

-- Creating foreign key on [Address_City] in table 'gen_ContactPerson'
ALTER TABLE [dbo].[gen_ContactPerson]
ADD CONSTRAINT [FK_gen_ContactPerson_gen_City]
    FOREIGN KEY ([Address_City])
    REFERENCES [dbo].[gen_City]
        ([Pk_CityID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_gen_ContactPerson_gen_City'
CREATE INDEX [IX_FK_gen_ContactPerson_gen_City]
ON [dbo].[gen_ContactPerson]
    ([Address_City]);
GO

-- Creating foreign key on [Address_State] in table 'gen_ContactPerson'
ALTER TABLE [dbo].[gen_ContactPerson]
ADD CONSTRAINT [FK_gen_ContactPerson_gen_State]
    FOREIGN KEY ([Address_State])
    REFERENCES [dbo].[gen_State]
        ([Pk_StateID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_gen_ContactPerson_gen_State'
CREATE INDEX [IX_FK_gen_ContactPerson_gen_State]
ON [dbo].[gen_ContactPerson]
    ([Address_State]);
GO

-- Creating foreign key on [Fk_ContactPerson] in table 'gen_CustomerContacts'
ALTER TABLE [dbo].[gen_CustomerContacts]
ADD CONSTRAINT [FK_gen_CustomerContacts_gen_ContactPerson]
    FOREIGN KEY ([Fk_ContactPerson])
    REFERENCES [dbo].[gen_ContactPerson]
        ([Pk_ContactPersonId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_gen_CustomerContacts_gen_ContactPerson'
CREATE INDEX [IX_FK_gen_CustomerContacts_gen_ContactPerson]
ON [dbo].[gen_CustomerContacts]
    ([Fk_ContactPerson]);
GO

-- Creating foreign key on [Fk_ContactPerson] in table 'gen_MillContacts'
ALTER TABLE [dbo].[gen_MillContacts]
ADD CONSTRAINT [FK_gen_MillContacts_gen_ContactPerson]
    FOREIGN KEY ([Fk_ContactPerson])
    REFERENCES [dbo].[gen_ContactPerson]
        ([Pk_ContactPersonId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_gen_MillContacts_gen_ContactPerson'
CREATE INDEX [IX_FK_gen_MillContacts_gen_ContactPerson]
ON [dbo].[gen_MillContacts]
    ([Fk_ContactPerson]);
GO

-- Creating foreign key on [Fk_ContactPerson] in table 'gen_VendorContacts'
ALTER TABLE [dbo].[gen_VendorContacts]
ADD CONSTRAINT [FK_gen_VendorContacts_gen_ContactPerson]
    FOREIGN KEY ([Fk_ContactPerson])
    REFERENCES [dbo].[gen_ContactPerson]
        ([Pk_ContactPersonId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_gen_VendorContacts_gen_ContactPerson'
CREATE INDEX [IX_FK_gen_VendorContacts_gen_ContactPerson]
ON [dbo].[gen_VendorContacts]
    ([Fk_ContactPerson]);
GO

-- Creating foreign key on [Country] in table 'gen_State'
ALTER TABLE [dbo].[gen_State]
ADD CONSTRAINT [FK_gen_State_gen_Country]
    FOREIGN KEY ([Country])
    REFERENCES [dbo].[gen_Country]
        ([Pk_Country])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_gen_State_gen_Country'
CREATE INDEX [IX_FK_gen_State_gen_Country]
ON [dbo].[gen_State]
    ([Country]);
GO

-- Creating foreign key on [Fk_Machine] in table 'MachineMaintenances'
ALTER TABLE [dbo].[MachineMaintenances]
ADD CONSTRAINT [FK_MachineMaintenance_gen_Machine]
    FOREIGN KEY ([Fk_Machine])
    REFERENCES [dbo].[gen_Machine]
        ([Pk_Machine])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MachineMaintenance_gen_Machine'
CREATE INDEX [IX_FK_MachineMaintenance_gen_Machine]
ON [dbo].[MachineMaintenances]
    ([Fk_Machine]);
GO

-- Creating foreign key on [Fk_Status] in table 'Inv_MaterialIndentMaster'
ALTER TABLE [dbo].[Inv_MaterialIndentMaster]
ADD CONSTRAINT [FK_MaterialIndent_wfStates]
    FOREIGN KEY ([Fk_Status])
    REFERENCES [dbo].[wfStates]
        ([Pk_State])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MaterialIndent_wfStates'
CREATE INDEX [IX_FK_MaterialIndent_wfStates]
ON [dbo].[Inv_MaterialIndentMaster]
    ([Fk_Status]);
GO

-- Creating foreign key on [Fk_Tanent] in table 'Inv_MaterialIndentMaster'
ALTER TABLE [dbo].[Inv_MaterialIndentMaster]
ADD CONSTRAINT [FK_MaterialIndent_wgTenant]
    FOREIGN KEY ([Fk_Tanent])
    REFERENCES [dbo].[wgTenant]
        ([Pk_Tanent])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MaterialIndent_wgTenant'
CREATE INDEX [IX_FK_MaterialIndent_wgTenant]
ON [dbo].[Inv_MaterialIndentMaster]
    ([Fk_Tanent]);
GO

-- Creating foreign key on [Fk_Indent] in table 'PendingTracks'
ALTER TABLE [dbo].[PendingTracks]
ADD CONSTRAINT [FK_PendingTrack_Inv_MaterialIndentMaster]
    FOREIGN KEY ([Fk_Indent])
    REFERENCES [dbo].[Inv_MaterialIndentMaster]
        ([Pk_MaterialOrderMasterId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PendingTrack_Inv_MaterialIndentMaster'
CREATE INDEX [IX_FK_PendingTrack_Inv_MaterialIndentMaster]
ON [dbo].[PendingTracks]
    ([Fk_Indent]);
GO

-- Creating foreign key on [Pk_QuotationMaterial] in table 'MaterialQuotationDetails'
ALTER TABLE [dbo].[MaterialQuotationDetails]
ADD CONSTRAINT [FK_MaterialQuotationDetails_MaterialQuotationDetails]
    FOREIGN KEY ([Pk_QuotationMaterial])
    REFERENCES [dbo].[MaterialQuotationDetails]
        ([Pk_QuotationMaterial])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Fk_QuotationRequest] in table 'MaterialQuotationDetails'
ALTER TABLE [dbo].[MaterialQuotationDetails]
ADD CONSTRAINT [FK_MaterialQuotationDetails_MaterialQuotationMaster]
    FOREIGN KEY ([Fk_QuotationRequest])
    REFERENCES [dbo].[MaterialQuotationMaster]
        ([Pk_QuotationRequest])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MaterialQuotationDetails_MaterialQuotationMaster'
CREATE INDEX [IX_FK_MaterialQuotationDetails_MaterialQuotationMaster]
ON [dbo].[MaterialQuotationDetails]
    ([Fk_QuotationRequest]);
GO

-- Creating foreign key on [NextState] in table 'wfWorkflowPaths'
ALTER TABLE [dbo].[wfWorkflowPaths]
ADD CONSTRAINT [FK_wfWorkflowPaths_wfStates]
    FOREIGN KEY ([NextState])
    REFERENCES [dbo].[wfStates]
        ([Pk_State])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_wfWorkflowPaths_wfStates'
CREATE INDEX [IX_FK_wfWorkflowPaths_wfStates]
ON [dbo].[wfWorkflowPaths]
    ([NextState]);
GO

-- Creating foreign key on [CurrentState] in table 'wfWorkflowPaths'
ALTER TABLE [dbo].[wfWorkflowPaths]
ADD CONSTRAINT [FK_wfWorkflowPaths_wfStates1]
    FOREIGN KEY ([CurrentState])
    REFERENCES [dbo].[wfStates]
        ([Pk_State])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_wfWorkflowPaths_wfStates1'
CREATE INDEX [IX_FK_wfWorkflowPaths_wfStates1]
ON [dbo].[wfWorkflowPaths]
    ([CurrentState]);
GO

-- Creating foreign key on [Fk_Path] in table 'wfTransactions'
ALTER TABLE [dbo].[wfTransactions]
ADD CONSTRAINT [FK_wfTransactions_wfWorkflowPaths]
    FOREIGN KEY ([Fk_Path])
    REFERENCES [dbo].[wfWorkflowPaths]
        ([Pk_Paths])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_wfTransactions_wfWorkflowPaths'
CREATE INDEX [IX_FK_wfTransactions_wfWorkflowPaths]
ON [dbo].[wfTransactions]
    ([Fk_Path]);
GO

-- Creating foreign key on [Fk_Triggers] in table 'wfTriggerParameters'
ALTER TABLE [dbo].[wfTriggerParameters]
ADD CONSTRAINT [FK_wfTriggerParameters_wfTriggers]
    FOREIGN KEY ([Fk_Triggers])
    REFERENCES [dbo].[wfTriggers]
        ([Pk_Triggers])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_wfTriggerParameters_wfTriggers'
CREATE INDEX [IX_FK_wfTriggerParameters_wfTriggers]
ON [dbo].[wfTriggerParameters]
    ([Fk_Triggers]);
GO

-- Creating foreign key on [Fk_Path] in table 'wfTriggers'
ALTER TABLE [dbo].[wfTriggers]
ADD CONSTRAINT [FK_wfTriggers_wfWorkflowPaths]
    FOREIGN KEY ([Fk_Path])
    REFERENCES [dbo].[wfWorkflowPaths]
        ([Pk_Paths])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_wfTriggers_wfWorkflowPaths'
CREATE INDEX [IX_FK_wfTriggers_wfWorkflowPaths]
ON [dbo].[wfTriggers]
    ([Fk_Path]);
GO

-- Creating foreign key on [Fk_WfRole] in table 'wfWorkflowPaths'
ALTER TABLE [dbo].[wfWorkflowPaths]
ADD CONSTRAINT [FK_wfWorkflowPaths_wfWorkflowRoles]
    FOREIGN KEY ([Fk_WfRole])
    REFERENCES [dbo].[wfWorkflowRoles]
        ([Pk_WfRole])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_wfWorkflowPaths_wfWorkflowRoles'
CREATE INDEX [IX_FK_wfWorkflowPaths_wfWorkflowRoles]
ON [dbo].[wfWorkflowPaths]
    ([Fk_WfRole]);
GO

-- Creating foreign key on [Fk_Tanent] in table 'wfWorkflowPaths'
ALTER TABLE [dbo].[wfWorkflowPaths]
ADD CONSTRAINT [FK_wfWorkflowPaths_wgTanent]
    FOREIGN KEY ([Fk_Tanent])
    REFERENCES [dbo].[wgTenant]
        ([Pk_Tanent])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_wfWorkflowPaths_wgTanent'
CREATE INDEX [IX_FK_wfWorkflowPaths_wgTanent]
ON [dbo].[wfWorkflowPaths]
    ([Fk_Tanent]);
GO

-- Creating foreign key on [Fk_Role] in table 'wfWorkflowRoleUsers'
ALTER TABLE [dbo].[wfWorkflowRoleUsers]
ADD CONSTRAINT [FK_wfWorkflowRoleUser_wfWorkflowRoles]
    FOREIGN KEY ([Fk_Role])
    REFERENCES [dbo].[wfWorkflowRoles]
        ([Pk_WfRole])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_wfWorkflowRoleUser_wfWorkflowRoles'
CREATE INDEX [IX_FK_wfWorkflowRoleUser_wfWorkflowRoles]
ON [dbo].[wfWorkflowRoleUsers]
    ([Fk_Role]);
GO

-- Creating foreign key on [Fk_DetailedEstimation] in table 'est_EstimateAdditionalCost'
ALTER TABLE [dbo].[est_EstimateAdditionalCost]
ADD CONSTRAINT [FK_est_EstimateAdditionalCost_est_Estimation1]
    FOREIGN KEY ([Fk_DetailedEstimation])
    REFERENCES [dbo].[C_est_Estimation1]
        ([PK_DetailedEstimate])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_est_EstimateAdditionalCost_est_Estimation1'
CREATE INDEX [IX_FK_est_EstimateAdditionalCost_est_Estimation1]
ON [dbo].[est_EstimateAdditionalCost]
    ([Fk_DetailedEstimation]);
GO

-- Creating foreign key on [Fk_DetailedEstimation] in table 'est_EstimateTax'
ALTER TABLE [dbo].[est_EstimateTax]
ADD CONSTRAINT [FK_est_EstimateTax_est_Estimation1]
    FOREIGN KEY ([Fk_DetailedEstimation])
    REFERENCES [dbo].[C_est_Estimation1]
        ([PK_DetailedEstimate])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_est_EstimateTax_est_Estimation1'
CREATE INDEX [IX_FK_est_EstimateTax_est_Estimation1]
ON [dbo].[est_EstimateTax]
    ([Fk_DetailedEstimation]);
GO

-- Creating foreign key on [Fk_DetailedEstimation] in table 'est_EstimateAdditionalCost'
ALTER TABLE [dbo].[est_EstimateAdditionalCost]
ADD CONSTRAINT [FK_est_EstimateAdditionalCost_est_Estimation3]
    FOREIGN KEY ([Fk_DetailedEstimation])
    REFERENCES [dbo].[C_est_Estimation3]
        ([PK_DetailedEstimate])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_est_EstimateAdditionalCost_est_Estimation3'
CREATE INDEX [IX_FK_est_EstimateAdditionalCost_est_Estimation3]
ON [dbo].[est_EstimateAdditionalCost]
    ([Fk_DetailedEstimation]);
GO

-- Creating foreign key on [Fk_DetailedEstimation] in table 'est_EstimateTax'
ALTER TABLE [dbo].[est_EstimateTax]
ADD CONSTRAINT [FK_est_EstimateTax_est_Estimation3]
    FOREIGN KEY ([Fk_DetailedEstimation])
    REFERENCES [dbo].[C_est_Estimation3]
        ([PK_DetailedEstimate])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_est_EstimateTax_est_Estimation3'
CREATE INDEX [IX_FK_est_EstimateTax_est_Estimation3]
ON [dbo].[est_EstimateTax]
    ([Fk_DetailedEstimation]);
GO

-- Creating foreign key on [Fk_DetailedEstimation] in table 'est_EstimateAdditionalCost'
ALTER TABLE [dbo].[est_EstimateAdditionalCost]
ADD CONSTRAINT [FK_est_EstimateAdditionalCost_est_Estimation4]
    FOREIGN KEY ([Fk_DetailedEstimation])
    REFERENCES [dbo].[C_est_Estimation4]
        ([PK_DetailedEstimate])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_est_EstimateAdditionalCost_est_Estimation4'
CREATE INDEX [IX_FK_est_EstimateAdditionalCost_est_Estimation4]
ON [dbo].[est_EstimateAdditionalCost]
    ([Fk_DetailedEstimation]);
GO

-- Creating foreign key on [Fk_DetailedEstimation] in table 'est_EstimateTax'
ALTER TABLE [dbo].[est_EstimateTax]
ADD CONSTRAINT [FK_est_EstimateTax_est_Estimation4]
    FOREIGN KEY ([Fk_DetailedEstimation])
    REFERENCES [dbo].[C_est_Estimation4]
        ([PK_DetailedEstimate])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_est_EstimateTax_est_Estimation4'
CREATE INDEX [IX_FK_est_EstimateTax_est_Estimation4]
ON [dbo].[est_EstimateTax]
    ([Fk_DetailedEstimation]);
GO

-- Creating foreign key on [Fk_DetailedEstimation] in table 'est_EstimateAdditionalCost'
ALTER TABLE [dbo].[est_EstimateAdditionalCost]
ADD CONSTRAINT [FK_est_EstimateAdditionalCost_est_Estimation5]
    FOREIGN KEY ([Fk_DetailedEstimation])
    REFERENCES [dbo].[C_est_Estimation5]
        ([PK_DetailedEstimate])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_est_EstimateAdditionalCost_est_Estimation5'
CREATE INDEX [IX_FK_est_EstimateAdditionalCost_est_Estimation5]
ON [dbo].[est_EstimateAdditionalCost]
    ([Fk_DetailedEstimation]);
GO

-- Creating foreign key on [Fk_DetailedEstimation] in table 'est_EstimateTax'
ALTER TABLE [dbo].[est_EstimateTax]
ADD CONSTRAINT [FK_est_EstimateTax_est_Estimation5]
    FOREIGN KEY ([Fk_DetailedEstimation])
    REFERENCES [dbo].[C_est_Estimation5]
        ([PK_DetailedEstimate])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_est_EstimateTax_est_Estimation5'
CREATE INDEX [IX_FK_est_EstimateTax_est_Estimation5]
ON [dbo].[est_EstimateTax]
    ([Fk_DetailedEstimation]);
GO

-- Creating foreign key on [EventSource] in table 'gen_Events'
ALTER TABLE [dbo].[gen_Events]
ADD CONSTRAINT [FK_gen_Events_gen_DeliverySchedule]
    FOREIGN KEY ([EventSource])
    REFERENCES [dbo].[gen_DeliverySchedule]
        ([Pk_DeliverySechedule])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_gen_Events_gen_DeliverySchedule'
CREATE INDEX [IX_FK_gen_Events_gen_DeliverySchedule]
ON [dbo].[gen_Events]
    ([EventSource]);
GO

-- Creating foreign key on [Fk_EnquiryChild] in table 'est_BoxEstimationChild'
ALTER TABLE [dbo].[est_BoxEstimationChild]
ADD CONSTRAINT [FK_est_BoxEstimationChild_eq_EnquiryChild]
    FOREIGN KEY ([Fk_EnquiryChild])
    REFERENCES [dbo].[eq_EnquiryChild]
        ([Pk_EnquiryChild])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_est_BoxEstimationChild_eq_EnquiryChild'
CREATE INDEX [IX_FK_est_BoxEstimationChild_eq_EnquiryChild]
ON [dbo].[est_BoxEstimationChild]
    ([Fk_EnquiryChild]);
GO

-- Creating foreign key on [Fk_BoxEstimation] in table 'est_BoxEstimationChild'
ALTER TABLE [dbo].[est_BoxEstimationChild]
ADD CONSTRAINT [FK_est_BoxEstimationChild_est_BoxEstimation]
    FOREIGN KEY ([Fk_BoxEstimation])
    REFERENCES [dbo].[est_BoxEstimation]
        ([Pk_BoxEstimation])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_est_BoxEstimationChild_est_BoxEstimation'
CREATE INDEX [IX_FK_est_BoxEstimationChild_est_BoxEstimation]
ON [dbo].[est_BoxEstimationChild]
    ([Fk_BoxEstimation]);
GO

-- Creating foreign key on [Pk_State] in table 'wfStates'
ALTER TABLE [dbo].[wfStates]
ADD CONSTRAINT [FK_wfStates_wfStates]
    FOREIGN KEY ([Pk_State])
    REFERENCES [dbo].[wfStates]
        ([Pk_State])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Fk_Enquiry] in table 'eq_Documents'
ALTER TABLE [dbo].[eq_Documents]
ADD CONSTRAINT [FK_eq_Documents_eq_Enquiry]
    FOREIGN KEY ([Fk_Enquiry])
    REFERENCES [dbo].[eq_Enquiry]
        ([Pk_Enquiry])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_eq_Documents_eq_Enquiry'
CREATE INDEX [IX_FK_eq_Documents_eq_Enquiry]
ON [dbo].[eq_Documents]
    ([Fk_Enquiry]);
GO

-- Creating foreign key on [CommunicationType] in table 'eq_Enquiry'
ALTER TABLE [dbo].[eq_Enquiry]
ADD CONSTRAINT [FK_eq_Enquiry_gen_Communication]
    FOREIGN KEY ([CommunicationType])
    REFERENCES [dbo].[gen_Communication]
        ([Pk_Communication])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_eq_Enquiry_gen_Communication'
CREATE INDEX [IX_FK_eq_Enquiry_gen_Communication]
ON [dbo].[eq_Enquiry]
    ([CommunicationType]);
GO

-- Creating foreign key on [Fk_Status] in table 'eq_Enquiry'
ALTER TABLE [dbo].[eq_Enquiry]
ADD CONSTRAINT [FK_eq_Enquiry_wfStates]
    FOREIGN KEY ([Fk_Status])
    REFERENCES [dbo].[wfStates]
        ([Pk_State])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_eq_Enquiry_wfStates'
CREATE INDEX [IX_FK_eq_Enquiry_wfStates]
ON [dbo].[eq_Enquiry]
    ([Fk_Status]);
GO

-- Creating foreign key on [Fk_Tanent] in table 'eq_Enquiry'
ALTER TABLE [dbo].[eq_Enquiry]
ADD CONSTRAINT [FK_eq_Enquiry_wgTenant]
    FOREIGN KEY ([Fk_Tanent])
    REFERENCES [dbo].[wgTenant]
        ([Pk_Tanent])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_eq_Enquiry_wgTenant'
CREATE INDEX [IX_FK_eq_Enquiry_wgTenant]
ON [dbo].[eq_Enquiry]
    ([Fk_Tanent]);
GO

-- Creating foreign key on [Fk_Enquiry] in table 'eq_EnquiryChild'
ALTER TABLE [dbo].[eq_EnquiryChild]
ADD CONSTRAINT [FK_eq_EnquiryChild_eq_Enquiry1]
    FOREIGN KEY ([Fk_Enquiry])
    REFERENCES [dbo].[eq_Enquiry]
        ([Pk_Enquiry])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_eq_EnquiryChild_eq_Enquiry1'
CREATE INDEX [IX_FK_eq_EnquiryChild_eq_Enquiry1]
ON [dbo].[eq_EnquiryChild]
    ([Fk_Enquiry]);
GO

-- Creating foreign key on [Fk_Enquiry] in table 'est_Estimation'
ALTER TABLE [dbo].[est_Estimation]
ADD CONSTRAINT [FK_est_Estimation_eq_Enquiry]
    FOREIGN KEY ([Fk_Enquiry])
    REFERENCES [dbo].[eq_Enquiry]
        ([Pk_Enquiry])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_est_Estimation_eq_Enquiry'
CREATE INDEX [IX_FK_est_Estimation_eq_Enquiry]
ON [dbo].[est_Estimation]
    ([Fk_Enquiry]);
GO

-- Creating foreign key on [Pk_Color] in table 'gen_Color'
ALTER TABLE [dbo].[gen_Color]
ADD CONSTRAINT [FK_gen_Color_gen_Color]
    FOREIGN KEY ([Pk_Color])
    REFERENCES [dbo].[gen_Color]
        ([Pk_Color])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Fk_Branch] in table 'eq_Enquiry'
ALTER TABLE [dbo].[eq_Enquiry]
ADD CONSTRAINT [FK_eq_Enquiry_Branch]
    FOREIGN KEY ([Fk_Branch])
    REFERENCES [dbo].[Branch]
        ([Pk_BranchID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_eq_Enquiry_Branch'
CREATE INDEX [IX_FK_eq_Enquiry_Branch]
ON [dbo].[eq_Enquiry]
    ([Fk_Branch]);
GO

-- Creating foreign key on [Fk_Tanent] in table 'ideActivity'
ALTER TABLE [dbo].[ideActivity]
ADD CONSTRAINT [FK_ideActivities_wgTenant]
    FOREIGN KEY ([Fk_Tanent])
    REFERENCES [dbo].[wgTenant]
        ([Pk_Tanent])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ideActivities_wgTenant'
CREATE INDEX [IX_FK_ideActivities_wgTenant]
ON [dbo].[ideActivity]
    ([Fk_Tanent]);
GO

-- Creating foreign key on [Fk_Activities] in table 'ideActivityPermission'
ALTER TABLE [dbo].[ideActivityPermission]
ADD CONSTRAINT [FK_ideActivityPermissions_ideActivities]
    FOREIGN KEY ([Fk_Activities])
    REFERENCES [dbo].[ideActivity]
        ([Pk_Activities])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ideActivityPermissions_ideActivities'
CREATE INDEX [IX_FK_ideActivityPermissions_ideActivities]
ON [dbo].[ideActivityPermission]
    ([Fk_Activities]);
GO

-- Creating foreign key on [Fk_Permissions] in table 'ideActivityPermission'
ALTER TABLE [dbo].[ideActivityPermission]
ADD CONSTRAINT [FK_ideActivityPermissions_idePermissions]
    FOREIGN KEY ([Fk_Permissions])
    REFERENCES [dbo].[idePermission]
        ([Pk_Permissions])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ideActivityPermissions_idePermissions'
CREATE INDEX [IX_FK_ideActivityPermissions_idePermissions]
ON [dbo].[ideActivityPermission]
    ([Fk_Permissions]);
GO

-- Creating foreign key on [Fk_Roles] in table 'ideActivityPermission'
ALTER TABLE [dbo].[ideActivityPermission]
ADD CONSTRAINT [FK_ideActivityPermissions_ideRoles]
    FOREIGN KEY ([Fk_Roles])
    REFERENCES [dbo].[ideRole]
        ([Pk_Roles])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ideActivityPermissions_ideRoles'
CREATE INDEX [IX_FK_ideActivityPermissions_ideRoles]
ON [dbo].[ideActivityPermission]
    ([Fk_Roles]);
GO

-- Creating foreign key on [Fk_Roles] in table 'ideRoleUser'
ALTER TABLE [dbo].[ideRoleUser]
ADD CONSTRAINT [FK_ideRoleUsers_ideRoles]
    FOREIGN KEY ([Fk_Roles])
    REFERENCES [dbo].[ideRole]
        ([Pk_Roles])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ideRoleUsers_ideRoles'
CREATE INDEX [IX_FK_ideRoleUsers_ideRoles]
ON [dbo].[ideRoleUser]
    ([Fk_Roles]);
GO

-- Creating foreign key on [Fk_Branch] in table 'ideUser'
ALTER TABLE [dbo].[ideUser]
ADD CONSTRAINT [FK_ideUser_Branch]
    FOREIGN KEY ([Fk_Branch])
    REFERENCES [dbo].[Branch]
        ([Pk_BranchID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ideUser_Branch'
CREATE INDEX [IX_FK_ideUser_Branch]
ON [dbo].[ideUser]
    ([Fk_Branch]);
GO

-- Creating foreign key on [Fk_City] in table 'gen_Vendor'
ALTER TABLE [dbo].[gen_Vendor]
ADD CONSTRAINT [FK_gen_Vendor_gen_City]
    FOREIGN KEY ([Fk_City])
    REFERENCES [dbo].[gen_City]
        ([Pk_CityID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_gen_Vendor_gen_City'
CREATE INDEX [IX_FK_gen_Vendor_gen_City]
ON [dbo].[gen_Vendor]
    ([Fk_City]);
GO

-- Creating foreign key on [Fk_Country] in table 'gen_Vendor'
ALTER TABLE [dbo].[gen_Vendor]
ADD CONSTRAINT [FK_gen_Vendor_gen_Country]
    FOREIGN KEY ([Fk_Country])
    REFERENCES [dbo].[gen_Country]
        ([Pk_Country])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_gen_Vendor_gen_Country'
CREATE INDEX [IX_FK_gen_Vendor_gen_Country]
ON [dbo].[gen_Vendor]
    ([Fk_Country]);
GO

-- Creating foreign key on [Fk_Machine] in table 'MachineMaintenance1Set'
ALTER TABLE [dbo].[MachineMaintenance1Set]
ADD CONSTRAINT [FK_MachineMaintenance_gen_Machine1]
    FOREIGN KEY ([Fk_Machine])
    REFERENCES [dbo].[gen_Machine]
        ([Pk_Machine])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MachineMaintenance_gen_Machine1'
CREATE INDEX [IX_FK_MachineMaintenance_gen_Machine1]
ON [dbo].[MachineMaintenance1Set]
    ([Fk_Machine]);
GO

-- Creating foreign key on [Fk_State] in table 'gen_Vendor'
ALTER TABLE [dbo].[gen_Vendor]
ADD CONSTRAINT [FK_gen_Vendor_gen_State]
    FOREIGN KEY ([Fk_State])
    REFERENCES [dbo].[gen_State]
        ([Pk_StateID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_gen_Vendor_gen_State'
CREATE INDEX [IX_FK_gen_Vendor_gen_State]
ON [dbo].[gen_Vendor]
    ([Fk_State]);
GO

-- Creating foreign key on [Fk_Vendor] in table 'gen_VendorContacts'
ALTER TABLE [dbo].[gen_VendorContacts]
ADD CONSTRAINT [FK_gen_VendorContacts_gen_Vendor]
    FOREIGN KEY ([Fk_Vendor])
    REFERENCES [dbo].[gen_Vendor]
        ([Pk_Vendor])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_gen_VendorContacts_gen_Vendor'
CREATE INDEX [IX_FK_gen_VendorContacts_gen_Vendor]
ON [dbo].[gen_VendorContacts]
    ([Fk_Vendor]);
GO

-- Creating foreign key on [Fk_Vendor] in table 'gen_VendorMaterials'
ALTER TABLE [dbo].[gen_VendorMaterials]
ADD CONSTRAINT [FK_gen_VendorMaterials_gen_Vendor]
    FOREIGN KEY ([Fk_Vendor])
    REFERENCES [dbo].[gen_Vendor]
        ([Pk_Vendor])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_gen_VendorMaterials_gen_Vendor'
CREATE INDEX [IX_FK_gen_VendorMaterials_gen_Vendor]
ON [dbo].[gen_VendorMaterials]
    ([Fk_Vendor]);
GO

-- Creating foreign key on [Fk_VendorId] in table 'Inv_MaterialIndentMaster'
ALTER TABLE [dbo].[Inv_MaterialIndentMaster]
ADD CONSTRAINT [FK_MaterialIndent_gen_Vendor]
    FOREIGN KEY ([Fk_VendorId])
    REFERENCES [dbo].[gen_Vendor]
        ([Pk_Vendor])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MaterialIndent_gen_Vendor'
CREATE INDEX [IX_FK_MaterialIndent_gen_Vendor]
ON [dbo].[Inv_MaterialIndentMaster]
    ([Fk_VendorId]);
GO

-- Creating foreign key on [Fk_Vendor] in table 'MaterialQuotationMaster'
ALTER TABLE [dbo].[MaterialQuotationMaster]
ADD CONSTRAINT [FK_MaterialQuotationMaster_MaterialQuotationMaster]
    FOREIGN KEY ([Fk_Vendor])
    REFERENCES [dbo].[gen_Vendor]
        ([Pk_Vendor])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MaterialQuotationMaster_MaterialQuotationMaster'
CREATE INDEX [IX_FK_MaterialQuotationMaster_MaterialQuotationMaster]
ON [dbo].[MaterialQuotationMaster]
    ([Fk_Vendor]);
GO

-- Creating foreign key on [Fk_Vendor] in table 'Payment'
ALTER TABLE [dbo].[Payment]
ADD CONSTRAINT [FK_Payment_gen_Vendor]
    FOREIGN KEY ([Fk_Vendor])
    REFERENCES [dbo].[gen_Vendor]
        ([Pk_Vendor])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Payment_gen_Vendor'
CREATE INDEX [IX_FK_Payment_gen_Vendor]
ON [dbo].[Payment]
    ([Fk_Vendor]);
GO

-- Creating foreign key on [Fk_Users] in table 'ideRoleUser'
ALTER TABLE [dbo].[ideRoleUser]
ADD CONSTRAINT [FK_ideRoleUser_ideUser]
    FOREIGN KEY ([Fk_Users])
    REFERENCES [dbo].[ideUser]
        ([Pk_Users])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ideRoleUser_ideUser'
CREATE INDEX [IX_FK_ideRoleUser_ideUser]
ON [dbo].[ideRoleUser]
    ([Fk_Users]);
GO

-- Creating foreign key on [Fk_User] in table 'ideShortCut'
ALTER TABLE [dbo].[ideShortCut]
ADD CONSTRAINT [FK_ideShortCut_ideUser]
    FOREIGN KEY ([Fk_User])
    REFERENCES [dbo].[ideUser]
        ([Pk_Users])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ideShortCut_ideUser'
CREATE INDEX [IX_FK_ideShortCut_ideUser]
ON [dbo].[ideShortCut]
    ([Fk_User]);
GO

-- Creating foreign key on [Fk_Tanent] in table 'ideUser'
ALTER TABLE [dbo].[ideUser]
ADD CONSTRAINT [FK_ideUser_wgTenant]
    FOREIGN KEY ([Fk_Tanent])
    REFERENCES [dbo].[wgTenant]
        ([Pk_Tanent])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ideUser_wgTenant'
CREATE INDEX [IX_FK_ideUser_wgTenant]
ON [dbo].[ideUser]
    ([Fk_Tanent]);
GO

-- Creating foreign key on [Fk_UserID] in table 'Inv_MaterialIndentMaster'
ALTER TABLE [dbo].[Inv_MaterialIndentMaster]
ADD CONSTRAINT [FK_Inv_MaterialIndentMaster_ideUser]
    FOREIGN KEY ([Fk_UserID])
    REFERENCES [dbo].[ideUser]
        ([Pk_Users])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Inv_MaterialIndentMaster_ideUser'
CREATE INDEX [IX_FK_Inv_MaterialIndentMaster_ideUser]
ON [dbo].[Inv_MaterialIndentMaster]
    ([Fk_UserID]);
GO

-- Creating foreign key on [Fk_EnteredUserId] in table 'IssueReturn'
ALTER TABLE [dbo].[IssueReturn]
ADD CONSTRAINT [FK_IssueReturn_ideUser]
    FOREIGN KEY ([Fk_EnteredUserId])
    REFERENCES [dbo].[ideUser]
        ([Pk_Users])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_IssueReturn_ideUser'
CREATE INDEX [IX_FK_IssueReturn_ideUser]
ON [dbo].[IssueReturn]
    ([Fk_EnteredUserId]);
GO

-- Creating foreign key on [Fk_IssueReturnMasterId] in table 'IssueReturnDetails'
ALTER TABLE [dbo].[IssueReturnDetails]
ADD CONSTRAINT [FK_IssueReturnDetails_IssueReturnMaster]
    FOREIGN KEY ([Fk_IssueReturnMasterId])
    REFERENCES [dbo].[IssueReturn]
        ([Pk_IssueReturnMasterId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_IssueReturnDetails_IssueReturnMaster'
CREATE INDEX [IX_FK_IssueReturnDetails_IssueReturnMaster]
ON [dbo].[IssueReturnDetails]
    ([Fk_IssueReturnMasterId]);
GO

-- Creating foreign key on [Fk_PaperStock] in table 'JobCardDetails'
ALTER TABLE [dbo].[JobCardDetails]
ADD CONSTRAINT [FK_JobCardDetails_PaperStock]
    FOREIGN KEY ([Fk_PaperStock])
    REFERENCES [dbo].[PaperStock]
        ([Pk_PaperStock])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JobCardDetails_PaperStock'
CREATE INDEX [IX_FK_JobCardDetails_PaperStock]
ON [dbo].[JobCardDetails]
    ([Fk_PaperStock]);
GO

-- Creating foreign key on [Fk_PoRetID] in table 'POReturnDetails'
ALTER TABLE [dbo].[POReturnDetails]
ADD CONSTRAINT [FK_POReturnDetails_POReturnMaster]
    FOREIGN KEY ([Fk_PoRetID])
    REFERENCES [dbo].[POReturnMaster]
        ([Pk_PoRetID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_POReturnDetails_POReturnMaster'
CREATE INDEX [IX_FK_POReturnDetails_POReturnMaster]
ON [dbo].[POReturnDetails]
    ([Fk_PoRetID]);
GO

-- Creating foreign key on [Fk_Material] in table 'BoardDesc'
ALTER TABLE [dbo].[BoardDesc]
ADD CONSTRAINT [FK_BoardDesc_Inv_Material]
    FOREIGN KEY ([Fk_Material])
    REFERENCES [dbo].[Inv_Material]
        ([Pk_Material])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_BoardDesc_Inv_Material'
CREATE INDEX [IX_FK_BoardDesc_Inv_Material]
ON [dbo].[BoardDesc]
    ([Fk_Material]);
GO

-- Creating foreign key on [Fk_Color] in table 'Inv_Material'
ALTER TABLE [dbo].[Inv_Material]
ADD CONSTRAINT [FK_Inv_Material_gen_Color]
    FOREIGN KEY ([Fk_Color])
    REFERENCES [dbo].[gen_Color]
        ([Pk_Color])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Inv_Material_gen_Color'
CREATE INDEX [IX_FK_Inv_Material_gen_Color]
ON [dbo].[Inv_Material]
    ([Fk_Color]);
GO

-- Creating foreign key on [Fk_Mill] in table 'gen_MillContacts'
ALTER TABLE [dbo].[gen_MillContacts]
ADD CONSTRAINT [FK_gen_MillContacts_gen_Mill]
    FOREIGN KEY ([Fk_Mill])
    REFERENCES [dbo].[gen_Mill]
        ([Pk_Mill])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_gen_MillContacts_gen_Mill'
CREATE INDEX [IX_FK_gen_MillContacts_gen_Mill]
ON [dbo].[gen_MillContacts]
    ([Fk_Mill]);
GO

-- Creating foreign key on [Fk_Mill] in table 'Inv_Material'
ALTER TABLE [dbo].[Inv_Material]
ADD CONSTRAINT [FK_Inv_Material_gen_Mill]
    FOREIGN KEY ([Fk_Mill])
    REFERENCES [dbo].[gen_Mill]
        ([Pk_Mill])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Inv_Material_gen_Mill'
CREATE INDEX [IX_FK_Inv_Material_gen_Mill]
ON [dbo].[Inv_Material]
    ([Fk_Mill]);
GO

-- Creating foreign key on [Fk_PaperType] in table 'Inv_Material'
ALTER TABLE [dbo].[Inv_Material]
ADD CONSTRAINT [FK_Inv_Material_gen_PaperType]
    FOREIGN KEY ([Fk_PaperType])
    REFERENCES [dbo].[gen_PaperType]
        ([Pk_PaperType])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Inv_Material_gen_PaperType'
CREATE INDEX [IX_FK_Inv_Material_gen_PaperType]
ON [dbo].[Inv_Material]
    ([Fk_PaperType]);
GO

-- Creating foreign key on [Fk_UnitId] in table 'Inv_Material'
ALTER TABLE [dbo].[Inv_Material]
ADD CONSTRAINT [FK_Inv_Material_gen_Unit]
    FOREIGN KEY ([Fk_UnitId])
    REFERENCES [dbo].[gen_Unit]
        ([Pk_Unit])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Inv_Material_gen_Unit'
CREATE INDEX [IX_FK_Inv_Material_gen_Unit]
ON [dbo].[Inv_Material]
    ([Fk_UnitId]);
GO

-- Creating foreign key on [VendorIdNumber] in table 'Inv_Material'
ALTER TABLE [dbo].[Inv_Material]
ADD CONSTRAINT [FK_Inv_Material_gen_Vendor]
    FOREIGN KEY ([VendorIdNumber])
    REFERENCES [dbo].[gen_Vendor]
        ([Pk_Vendor])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Inv_Material_gen_Vendor'
CREATE INDEX [IX_FK_Inv_Material_gen_Vendor]
ON [dbo].[Inv_Material]
    ([VendorIdNumber]);
GO

-- Creating foreign key on [Fk_Material] in table 'gen_VendorMaterials'
ALTER TABLE [dbo].[gen_VendorMaterials]
ADD CONSTRAINT [FK_gen_VendorMaterials_Inv_Material]
    FOREIGN KEY ([Fk_Material])
    REFERENCES [dbo].[Inv_Material]
        ([Pk_Material])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_gen_VendorMaterials_Inv_Material'
CREATE INDEX [IX_FK_gen_VendorMaterials_Inv_Material]
ON [dbo].[gen_VendorMaterials]
    ([Fk_Material]);
GO

-- Creating foreign key on [Fk_MaterialCategory] in table 'Inv_Material'
ALTER TABLE [dbo].[Inv_Material]
ADD CONSTRAINT [FK_Inv_Material_Inv_MaterialCategory]
    FOREIGN KEY ([Fk_MaterialCategory])
    REFERENCES [dbo].[Inv_MaterialCategory]
        ([Pk_MaterialCategory])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Inv_Material_Inv_MaterialCategory'
CREATE INDEX [IX_FK_Inv_Material_Inv_MaterialCategory]
ON [dbo].[Inv_Material]
    ([Fk_MaterialCategory]);
GO

-- Creating foreign key on [Fk_Material] in table 'IssueReturnDetails'
ALTER TABLE [dbo].[IssueReturnDetails]
ADD CONSTRAINT [FK_IssueReturnDetails_Inv_Material]
    FOREIGN KEY ([Fk_Material])
    REFERENCES [dbo].[Inv_Material]
        ([Pk_Material])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_IssueReturnDetails_Inv_Material'
CREATE INDEX [IX_FK_IssueReturnDetails_Inv_Material]
ON [dbo].[IssueReturnDetails]
    ([Fk_Material]);
GO

-- Creating foreign key on [Fk_Material] in table 'JobCardDetails'
ALTER TABLE [dbo].[JobCardDetails]
ADD CONSTRAINT [FK_JobCardDetails_Inv_Material]
    FOREIGN KEY ([Fk_Material])
    REFERENCES [dbo].[Inv_Material]
        ([Pk_Material])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JobCardDetails_Inv_Material'
CREATE INDEX [IX_FK_JobCardDetails_Inv_Material]
ON [dbo].[JobCardDetails]
    ([Fk_Material]);
GO

-- Creating foreign key on [Fk_Material] in table 'MaterialQuotationDetails'
ALTER TABLE [dbo].[MaterialQuotationDetails]
ADD CONSTRAINT [FK_MaterialQuotationDetails_Inv_Material]
    FOREIGN KEY ([Fk_Material])
    REFERENCES [dbo].[Inv_Material]
        ([Pk_Material])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MaterialQuotationDetails_Inv_Material'
CREATE INDEX [IX_FK_MaterialQuotationDetails_Inv_Material]
ON [dbo].[MaterialQuotationDetails]
    ([Fk_Material]);
GO

-- Creating foreign key on [Fk_Material] in table 'PaperStock'
ALTER TABLE [dbo].[PaperStock]
ADD CONSTRAINT [FK_PaperStock_PaperStock]
    FOREIGN KEY ([Fk_Material])
    REFERENCES [dbo].[Inv_Material]
        ([Pk_Material])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PaperStock_PaperStock'
CREATE INDEX [IX_FK_PaperStock_PaperStock]
ON [dbo].[PaperStock]
    ([Fk_Material]);
GO

-- Creating foreign key on [Fk_Material] in table 'POReturnDetails'
ALTER TABLE [dbo].[POReturnDetails]
ADD CONSTRAINT [FK_POReturnDetails_Inv_Material]
    FOREIGN KEY ([Fk_Material])
    REFERENCES [dbo].[Inv_Material]
        ([Pk_Material])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_POReturnDetails_Inv_Material'
CREATE INDEX [IX_FK_POReturnDetails_Inv_Material]
ON [dbo].[POReturnDetails]
    ([Fk_Material]);
GO

-- Creating foreign key on [Fk_Material] in table 'Stocks'
ALTER TABLE [dbo].[Stocks]
ADD CONSTRAINT [FK_Stocks_Inv_Material]
    FOREIGN KEY ([Fk_Material])
    REFERENCES [dbo].[Inv_Material]
        ([Pk_Material])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Stocks_Inv_Material'
CREATE INDEX [IX_FK_Stocks_Inv_Material]
ON [dbo].[Stocks]
    ([Fk_Material]);
GO

-- Creating foreign key on [Fk_Material] in table 'StockTransfer'
ALTER TABLE [dbo].[StockTransfer]
ADD CONSTRAINT [FK_StockTransfer_Inv_Material]
    FOREIGN KEY ([Fk_Material])
    REFERENCES [dbo].[Inv_Material]
        ([Pk_Material])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_StockTransfer_Inv_Material'
CREATE INDEX [IX_FK_StockTransfer_Inv_Material]
ON [dbo].[StockTransfer]
    ([Fk_Material]);
GO

-- Creating foreign key on [Fk_Mill] in table 'MaterialIssueDetails'
ALTER TABLE [dbo].[MaterialIssueDetails]
ADD CONSTRAINT [FK_MaterialIssueDetails_gen_Mill]
    FOREIGN KEY ([Fk_Mill])
    REFERENCES [dbo].[gen_Mill]
        ([Pk_Mill])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MaterialIssueDetails_gen_Mill'
CREATE INDEX [IX_FK_MaterialIssueDetails_gen_Mill]
ON [dbo].[MaterialIssueDetails]
    ([Fk_Mill]);
GO

-- Creating foreign key on [Fk_Material] in table 'MaterialIssueDetails'
ALTER TABLE [dbo].[MaterialIssueDetails]
ADD CONSTRAINT [FK_MaterialIssueDetails_Inv_Material]
    FOREIGN KEY ([Fk_Material])
    REFERENCES [dbo].[Inv_Material]
        ([Pk_Material])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MaterialIssueDetails_Inv_Material'
CREATE INDEX [IX_FK_MaterialIssueDetails_Inv_Material]
ON [dbo].[MaterialIssueDetails]
    ([Fk_Material]);
GO

-- Creating foreign key on [Fk_FluteID] in table 'OD_ID'
ALTER TABLE [dbo].[OD_ID]
ADD CONSTRAINT [FK_OD_ID_FluteType]
    FOREIGN KEY ([Fk_FluteID])
    REFERENCES [dbo].[FluteType]
        ([Pk_FluteID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_OD_ID_FluteType'
CREATE INDEX [IX_FK_OD_ID_FluteType]
ON [dbo].[OD_ID]
    ([Fk_FluteID]);
GO

-- Creating foreign key on [Fk_Transporter] in table 'Trans_Invoice'
ALTER TABLE [dbo].[Trans_Invoice]
ADD CONSTRAINT [FK_Trans_Invoice_TransporterM]
    FOREIGN KEY ([Fk_Transporter])
    REFERENCES [dbo].[TransporterM]
        ([Pk_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Trans_Invoice_TransporterM'
CREATE INDEX [IX_FK_Trans_Invoice_TransporterM]
ON [dbo].[Trans_Invoice]
    ([Fk_Transporter]);
GO

-- Creating foreign key on [Fk_Transporter] in table 'Trans_Payment'
ALTER TABLE [dbo].[Trans_Payment]
ADD CONSTRAINT [FK_Trans_Payment_TransporterM]
    FOREIGN KEY ([Fk_Transporter])
    REFERENCES [dbo].[TransporterM]
        ([Pk_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Trans_Payment_TransporterM'
CREATE INDEX [IX_FK_Trans_Payment_TransporterM]
ON [dbo].[Trans_Payment]
    ([Fk_Transporter]);
GO

-- Creating foreign key on [Fk_EstimationID] in table 'QuotationDetails'
ALTER TABLE [dbo].[QuotationDetails]
ADD CONSTRAINT [FK_QuotationDetails_est_BoxEstimation]
    FOREIGN KEY ([Fk_EstimationID])
    REFERENCES [dbo].[est_BoxEstimation]
        ([Pk_BoxEstimation])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_QuotationDetails_est_BoxEstimation'
CREATE INDEX [IX_FK_QuotationDetails_est_BoxEstimation]
ON [dbo].[QuotationDetails]
    ([Fk_EstimationID]);
GO

-- Creating foreign key on [Fk_EmployeeId] in table 'att_RegisterAttendance'
ALTER TABLE [dbo].[att_RegisterAttendance]
ADD CONSTRAINT [FK_att_RegisterAttendance_emp_EmployeeMaster]
    FOREIGN KEY ([Fk_EmployeeId])
    REFERENCES [dbo].[emp_EmployeeMaster]
        ([Pk_EmployeeId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_att_RegisterAttendance_emp_EmployeeMaster'
CREATE INDEX [IX_FK_att_RegisterAttendance_emp_EmployeeMaster]
ON [dbo].[att_RegisterAttendance]
    ([Fk_EmployeeId]);
GO

-- Creating foreign key on [Fk_DepartmentId] in table 'emp_EmployeeMaster'
ALTER TABLE [dbo].[emp_EmployeeMaster]
ADD CONSTRAINT [FK_emp_EmployeeMaster_emp_Department]
    FOREIGN KEY ([Fk_DepartmentId])
    REFERENCES [dbo].[emp_Department]
        ([Pk_DepartmentId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_emp_EmployeeMaster_emp_Department'
CREATE INDEX [IX_FK_emp_EmployeeMaster_emp_Department]
ON [dbo].[emp_EmployeeMaster]
    ([Fk_DepartmentId]);
GO

-- Creating foreign key on [Fk_EmployeeDesignation] in table 'emp_EmployeeMaster'
ALTER TABLE [dbo].[emp_EmployeeMaster]
ADD CONSTRAINT [FK_emp_EmployeeMaster_emp_EmployeeDesignation]
    FOREIGN KEY ([Fk_EmployeeDesignation])
    REFERENCES [dbo].[emp_EmployeeDesignation]
        ([Pk_EmployeeDesignationId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_emp_EmployeeMaster_emp_EmployeeDesignation'
CREATE INDEX [IX_FK_emp_EmployeeMaster_emp_EmployeeDesignation]
ON [dbo].[emp_EmployeeMaster]
    ([Fk_EmployeeDesignation]);
GO

-- Creating foreign key on [Fk_Employee] in table 'emp_EmployeeImage'
ALTER TABLE [dbo].[emp_EmployeeImage]
ADD CONSTRAINT [FK_emp_EmployeeImage_emp_EmployeeMaster]
    FOREIGN KEY ([Fk_Employee])
    REFERENCES [dbo].[emp_EmployeeMaster]
        ([Pk_EmployeeId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_emp_EmployeeImage_emp_EmployeeMaster'
CREATE INDEX [IX_FK_emp_EmployeeImage_emp_EmployeeMaster]
ON [dbo].[emp_EmployeeImage]
    ([Fk_Employee]);
GO

-- Creating foreign key on [Fk_City] in table 'emp_EmployeeMaster'
ALTER TABLE [dbo].[emp_EmployeeMaster]
ADD CONSTRAINT [FK_emp_EmployeeMaster_gen_City]
    FOREIGN KEY ([Fk_City])
    REFERENCES [dbo].[gen_City]
        ([Pk_CityID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_emp_EmployeeMaster_gen_City'
CREATE INDEX [IX_FK_emp_EmployeeMaster_gen_City]
ON [dbo].[emp_EmployeeMaster]
    ([Fk_City]);
GO

-- Creating foreign key on [Fk_State] in table 'emp_EmployeeMaster'
ALTER TABLE [dbo].[emp_EmployeeMaster]
ADD CONSTRAINT [FK_emp_EmployeeMaster_gen_State]
    FOREIGN KEY ([Fk_State])
    REFERENCES [dbo].[gen_State]
        ([Pk_StateID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_emp_EmployeeMaster_gen_State'
CREATE INDEX [IX_FK_emp_EmployeeMaster_gen_State]
ON [dbo].[emp_EmployeeMaster]
    ([Fk_State]);
GO

-- Creating foreign key on [Fk_Vendor] in table 'WireCertificate'
ALTER TABLE [dbo].[WireCertificate]
ADD CONSTRAINT [FK_WireCertificate_gen_Vendor]
    FOREIGN KEY ([Fk_Vendor])
    REFERENCES [dbo].[gen_Vendor]
        ([Pk_Vendor])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_WireCertificate_gen_Vendor'
CREATE INDEX [IX_FK_WireCertificate_gen_Vendor]
ON [dbo].[WireCertificate]
    ([Fk_Vendor]);
GO

-- Creating foreign key on [Fk_Material] in table 'WireCertificate'
ALTER TABLE [dbo].[WireCertificate]
ADD CONSTRAINT [FK_WireCertificate_Inv_Material]
    FOREIGN KEY ([Fk_Material])
    REFERENCES [dbo].[Inv_Material]
        ([Pk_Material])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_WireCertificate_Inv_Material'
CREATE INDEX [IX_FK_WireCertificate_Inv_Material]
ON [dbo].[WireCertificate]
    ([Fk_Material]);
GO

-- Creating foreign key on [Fk_ID] in table 'WireCertificateDetails'
ALTER TABLE [dbo].[WireCertificateDetails]
ADD CONSTRAINT [FK_WireCertificateDetails_WireCertificate]
    FOREIGN KEY ([Fk_ID])
    REFERENCES [dbo].[WireCertificate]
        ([Pk_Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_WireCertificateDetails_WireCertificate'
CREATE INDEX [IX_FK_WireCertificateDetails_WireCertificate]
ON [dbo].[WireCertificateDetails]
    ([Fk_ID]);
GO

-- Creating foreign key on [Fk_Params] in table 'WireCertificateDetails'
ALTER TABLE [dbo].[WireCertificateDetails]
ADD CONSTRAINT [FK_WireCertificateDetails_WireChar]
    FOREIGN KEY ([Fk_Params])
    REFERENCES [dbo].[WireChar]
        ([Pk_CharID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_WireCertificateDetails_WireChar'
CREATE INDEX [IX_FK_WireCertificateDetails_WireChar]
ON [dbo].[WireCertificateDetails]
    ([Fk_Params]);
GO

-- Creating foreign key on [Fk_Enquiry] in table 'QuotationMaster'
ALTER TABLE [dbo].[QuotationMaster]
ADD CONSTRAINT [FK_QuotationMaster_eq_Enquiry]
    FOREIGN KEY ([Fk_Enquiry])
    REFERENCES [dbo].[eq_Enquiry]
        ([Pk_Enquiry])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_QuotationMaster_eq_Enquiry'
CREATE INDEX [IX_FK_QuotationMaster_eq_Enquiry]
ON [dbo].[QuotationMaster]
    ([Fk_Enquiry]);
GO

-- Creating foreign key on [Fk_QuotationID] in table 'QuotationDetails'
ALTER TABLE [dbo].[QuotationDetails]
ADD CONSTRAINT [FK_QuotationDetails_QuotationMaster]
    FOREIGN KEY ([Fk_QuotationID])
    REFERENCES [dbo].[QuotationMaster]
        ([Pk_Quotation])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_QuotationDetails_QuotationMaster'
CREATE INDEX [IX_FK_QuotationDetails_QuotationMaster]
ON [dbo].[QuotationDetails]
    ([Fk_QuotationID]);
GO

-- Creating foreign key on [Fk_Material] in table 'InkCertificate'
ALTER TABLE [dbo].[InkCertificate]
ADD CONSTRAINT [FK_InkCertificate_Inv_Material]
    FOREIGN KEY ([Fk_Material])
    REFERENCES [dbo].[Inv_Material]
        ([Pk_Material])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_InkCertificate_Inv_Material'
CREATE INDEX [IX_FK_InkCertificate_Inv_Material]
ON [dbo].[InkCertificate]
    ([Fk_Material]);
GO

-- Creating foreign key on [Fk_Vendor] in table 'GlueCertificate'
ALTER TABLE [dbo].[GlueCertificate]
ADD CONSTRAINT [FK_GlueCertificate_gen_Vendor]
    FOREIGN KEY ([Fk_Vendor])
    REFERENCES [dbo].[gen_Vendor]
        ([Pk_Vendor])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GlueCertificate_gen_Vendor'
CREATE INDEX [IX_FK_GlueCertificate_gen_Vendor]
ON [dbo].[GlueCertificate]
    ([Fk_Vendor]);
GO

-- Creating foreign key on [Fk_Material] in table 'GlueCertificate'
ALTER TABLE [dbo].[GlueCertificate]
ADD CONSTRAINT [FK_GlueCertificate_Inv_Material]
    FOREIGN KEY ([Fk_Material])
    REFERENCES [dbo].[Inv_Material]
        ([Pk_Material])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GlueCertificate_Inv_Material'
CREATE INDEX [IX_FK_GlueCertificate_Inv_Material]
ON [dbo].[GlueCertificate]
    ([Fk_Material]);
GO

-- Creating foreign key on [Fk_PkID] in table 'GlueCertificateDetails'
ALTER TABLE [dbo].[GlueCertificateDetails]
ADD CONSTRAINT [FK_GlueCertificateDetails_GlueCertificate]
    FOREIGN KEY ([Fk_PkID])
    REFERENCES [dbo].[GlueCertificate]
        ([Pk_Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GlueCertificateDetails_GlueCertificate'
CREATE INDEX [IX_FK_GlueCertificateDetails_GlueCertificate]
ON [dbo].[GlueCertificateDetails]
    ([Fk_PkID]);
GO

-- Creating foreign key on [Fk_ID] in table 'InkCertificateDetails'
ALTER TABLE [dbo].[InkCertificateDetails]
ADD CONSTRAINT [FK_InkCertificateDetails_InkCertificate]
    FOREIGN KEY ([Fk_ID])
    REFERENCES [dbo].[InkCertificate]
        ([Pk_Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_InkCertificateDetails_InkCertificate'
CREATE INDEX [IX_FK_InkCertificateDetails_InkCertificate]
ON [dbo].[InkCertificateDetails]
    ([Fk_ID]);
GO

-- Creating foreign key on [Fk_Params] in table 'InkCertificateDetails'
ALTER TABLE [dbo].[InkCertificateDetails]
ADD CONSTRAINT [FK_InkCertificateDetails_InkChar]
    FOREIGN KEY ([Fk_Params])
    REFERENCES [dbo].[InkChar]
        ([Pk_CharID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_InkCertificateDetails_InkChar'
CREATE INDEX [IX_FK_InkCertificateDetails_InkChar]
ON [dbo].[InkCertificateDetails]
    ([Fk_Params]);
GO

-- Creating foreign key on [Fk_Characteristics] in table 'PaperCertificateDetails'
ALTER TABLE [dbo].[PaperCertificateDetails]
ADD CONSTRAINT [FK_PaperCertificateDetails_PaperChar]
    FOREIGN KEY ([Fk_Characteristics])
    REFERENCES [dbo].[PaperChar]
        ([Pk_CharID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PaperCertificateDetails_PaperChar'
CREATE INDEX [IX_FK_PaperCertificateDetails_PaperChar]
ON [dbo].[PaperCertificateDetails]
    ([Fk_Characteristics]);
GO

-- Creating foreign key on [Fk_Material] in table 'JobCardPartsDetails'
ALTER TABLE [dbo].[JobCardPartsDetails]
ADD CONSTRAINT [FK_JobCardPartsDetails_Inv_Material]
    FOREIGN KEY ([Fk_Material])
    REFERENCES [dbo].[Inv_Material]
        ([Pk_Material])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JobCardPartsDetails_Inv_Material'
CREATE INDEX [IX_FK_JobCardPartsDetails_Inv_Material]
ON [dbo].[JobCardPartsDetails]
    ([Fk_Material]);
GO

-- Creating foreign key on [Fk_PaperStock] in table 'JobCardPartsDetails'
ALTER TABLE [dbo].[JobCardPartsDetails]
ADD CONSTRAINT [FK_JobCardPartsDetails_PaperStock]
    FOREIGN KEY ([Fk_PaperStock])
    REFERENCES [dbo].[PaperStock]
        ([Pk_PaperStock])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JobCardPartsDetails_PaperStock'
CREATE INDEX [IX_FK_JobCardPartsDetails_PaperStock]
ON [dbo].[JobCardPartsDetails]
    ([Fk_PaperStock]);
GO

-- Creating foreign key on [Fk_Schedule] in table 'JobCardPartsMaster'
ALTER TABLE [dbo].[JobCardPartsMaster]
ADD CONSTRAINT [FK_JobCardPartsMaster_gen_DeliverySchedule]
    FOREIGN KEY ([Fk_Schedule])
    REFERENCES [dbo].[gen_DeliverySchedule]
        ([Pk_DeliverySechedule])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JobCardPartsMaster_gen_DeliverySchedule'
CREATE INDEX [IX_FK_JobCardPartsMaster_gen_DeliverySchedule]
ON [dbo].[JobCardPartsMaster]
    ([Fk_Schedule]);
GO

-- Creating foreign key on [Fk_JobCardPartsID] in table 'JobCardPartsDetails'
ALTER TABLE [dbo].[JobCardPartsDetails]
ADD CONSTRAINT [FK_JobCardPartsDetails_JobCardPartsMaster]
    FOREIGN KEY ([Fk_JobCardPartsID])
    REFERENCES [dbo].[JobCardPartsMaster]
        ([Pk_JobCardPartsID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JobCardPartsDetails_JobCardPartsMaster'
CREATE INDEX [IX_FK_JobCardPartsDetails_JobCardPartsMaster]
ON [dbo].[JobCardPartsDetails]
    ([Fk_JobCardPartsID]);
GO

-- Creating foreign key on [Fk_Status] in table 'JobCardPartsMaster'
ALTER TABLE [dbo].[JobCardPartsMaster]
ADD CONSTRAINT [FK_JobCardPartsMaster_wfStates]
    FOREIGN KEY ([Fk_Status])
    REFERENCES [dbo].[wfStates]
        ([Pk_State])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JobCardPartsMaster_wfStates'
CREATE INDEX [IX_FK_JobCardPartsMaster_wfStates]
ON [dbo].[JobCardPartsMaster]
    ([Fk_Status]);
GO

-- Creating foreign key on [Fk_Material] in table 'OutsourcingChild'
ALTER TABLE [dbo].[OutsourcingChild]
ADD CONSTRAINT [FK_OursourcingChild_Inv_Material]
    FOREIGN KEY ([Fk_Material])
    REFERENCES [dbo].[Inv_Material]
        ([Pk_Material])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_OursourcingChild_Inv_Material'
CREATE INDEX [IX_FK_OursourcingChild_Inv_Material]
ON [dbo].[OutsourcingChild]
    ([Fk_Material]);
GO

-- Creating foreign key on [Fk_Status] in table 'PartJobs'
ALTER TABLE [dbo].[PartJobs]
ADD CONSTRAINT [FK_PartJobs_wfStates]
    FOREIGN KEY ([Fk_Status])
    REFERENCES [dbo].[wfStates]
        ([Pk_State])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PartJobs_wfStates'
CREATE INDEX [IX_FK_PartJobs_wfStates]
ON [dbo].[PartJobs]
    ([Fk_Status]);
GO

-- Creating foreign key on [Fk_Material] in table 'JobWorkRMStock'
ALTER TABLE [dbo].[JobWorkRMStock]
ADD CONSTRAINT [FK_JobWorkRMStock_Inv_Material]
    FOREIGN KEY ([Fk_Material])
    REFERENCES [dbo].[Inv_Material]
        ([Pk_Material])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JobWorkRMStock_Inv_Material'
CREATE INDEX [IX_FK_JobWorkRMStock_Inv_Material]
ON [dbo].[JobWorkRMStock]
    ([Fk_Material]);
GO

-- Creating foreign key on [Fk_Material] in table 'Sample_JobCardDetails'
ALTER TABLE [dbo].[Sample_JobCardDetails]
ADD CONSTRAINT [FK_Sample_JobCardDetails_Inv_Material]
    FOREIGN KEY ([Fk_Material])
    REFERENCES [dbo].[Inv_Material]
        ([Pk_Material])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Sample_JobCardDetails_Inv_Material'
CREATE INDEX [IX_FK_Sample_JobCardDetails_Inv_Material]
ON [dbo].[Sample_JobCardDetails]
    ([Fk_Material]);
GO

-- Creating foreign key on [Fk_Material] in table 'SampleItems_Layers'
ALTER TABLE [dbo].[SampleItems_Layers]
ADD CONSTRAINT [FK_SampleItems_Layers_Inv_Material]
    FOREIGN KEY ([Fk_Material])
    REFERENCES [dbo].[Inv_Material]
        ([Pk_Material])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SampleItems_Layers_Inv_Material'
CREATE INDEX [IX_FK_SampleItems_Layers_Inv_Material]
ON [dbo].[SampleItems_Layers]
    ([Fk_Material]);
GO

-- Creating foreign key on [Fk_JobCardID] in table 'Sample_JobCardDetails'
ALTER TABLE [dbo].[Sample_JobCardDetails]
ADD CONSTRAINT [FK_Sample_JobCardDetails_Sample_JobCardMaster]
    FOREIGN KEY ([Fk_JobCardID])
    REFERENCES [dbo].[Sample_JobCardMaster]
        ([Pk_JobCardID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Sample_JobCardDetails_Sample_JobCardMaster'
CREATE INDEX [IX_FK_Sample_JobCardDetails_Sample_JobCardMaster]
ON [dbo].[Sample_JobCardDetails]
    ([Fk_JobCardID]);
GO

-- Creating foreign key on [Fk_BoxSpecsID] in table 'SampleItemPartProperty'
ALTER TABLE [dbo].[SampleItemPartProperty]
ADD CONSTRAINT [FK_SampleItemPartProperty_SampleBoxSpecs]
    FOREIGN KEY ([Fk_BoxSpecsID])
    REFERENCES [dbo].[SampleBoxSpecs]
        ([Pk_BoxSpecID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SampleItemPartProperty_SampleBoxSpecs'
CREATE INDEX [IX_FK_SampleItemPartProperty_SampleBoxSpecs]
ON [dbo].[SampleItemPartProperty]
    ([Fk_BoxSpecsID]);
GO

-- Creating foreign key on [Fk_PartId] in table 'SampleItems_Layers'
ALTER TABLE [dbo].[SampleItems_Layers]
ADD CONSTRAINT [FK_SampleItems_Layers_SampleItemPartProperty]
    FOREIGN KEY ([Fk_PartId])
    REFERENCES [dbo].[SampleItemPartProperty]
        ([Pk_PartPropertyID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SampleItems_Layers_SampleItemPartProperty'
CREATE INDEX [IX_FK_SampleItems_Layers_SampleItemPartProperty]
ON [dbo].[SampleItems_Layers]
    ([Fk_PartId]);
GO

-- Creating foreign key on [Fk_BoxType] in table 'SampleBoxMaster'
ALTER TABLE [dbo].[SampleBoxMaster]
ADD CONSTRAINT [FK_SampleBoxMaster_BoxType]
    FOREIGN KEY ([Fk_BoxType])
    REFERENCES [dbo].[BoxType]
        ([Pk_BoxTypeID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SampleBoxMaster_BoxType'
CREATE INDEX [IX_FK_SampleBoxMaster_BoxType]
ON [dbo].[SampleBoxMaster]
    ([Fk_BoxType]);
GO

-- Creating foreign key on [Fk_FluteType] in table 'SampleBoxMaster'
ALTER TABLE [dbo].[SampleBoxMaster]
ADD CONSTRAINT [FK_SampleBoxMaster_FluteType]
    FOREIGN KEY ([Fk_FluteType])
    REFERENCES [dbo].[FluteType]
        ([Pk_FluteID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SampleBoxMaster_FluteType'
CREATE INDEX [IX_FK_SampleBoxMaster_FluteType]
ON [dbo].[SampleBoxMaster]
    ([Fk_FluteType]);
GO

-- Creating foreign key on [Fk_BoxID] in table 'Sample_JobCardMaster'
ALTER TABLE [dbo].[Sample_JobCardMaster]
ADD CONSTRAINT [FK_Sample_JobCardMaster_SampleBoxMaster]
    FOREIGN KEY ([Fk_BoxID])
    REFERENCES [dbo].[SampleBoxMaster]
        ([Pk_BoxID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Sample_JobCardMaster_SampleBoxMaster'
CREATE INDEX [IX_FK_Sample_JobCardMaster_SampleBoxMaster]
ON [dbo].[Sample_JobCardMaster]
    ([Fk_BoxID]);
GO

-- Creating foreign key on [Fk_BoxID] in table 'SampleBoxSpecs'
ALTER TABLE [dbo].[SampleBoxSpecs]
ADD CONSTRAINT [FK_SampleBoxSpecs_SampleBoxMaster]
    FOREIGN KEY ([Fk_BoxID])
    REFERENCES [dbo].[SampleBoxMaster]
        ([Pk_BoxID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SampleBoxSpecs_SampleBoxMaster'
CREATE INDEX [IX_FK_SampleBoxSpecs_SampleBoxMaster]
ON [dbo].[SampleBoxSpecs]
    ([Fk_BoxID]);
GO

-- Creating foreign key on [Fk_DispMast] in table 'DispatchChild'
ALTER TABLE [dbo].[DispatchChild]
ADD CONSTRAINT [FK_DispatchChild_DispatchMast]
    FOREIGN KEY ([Fk_DispMast])
    REFERENCES [dbo].[DispatchMast]
        ([Pk_Dispatch])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DispatchChild_DispatchMast'
CREATE INDEX [IX_FK_DispatchChild_DispatchMast]
ON [dbo].[DispatchChild]
    ([Fk_DispMast]);
GO

-- Creating foreign key on [Fk_Vendor] in table 'PaperCertificate'
ALTER TABLE [dbo].[PaperCertificate]
ADD CONSTRAINT [FK_PaperCertificate_gen_Vendor]
    FOREIGN KEY ([Fk_Vendor])
    REFERENCES [dbo].[gen_Vendor]
        ([Pk_Vendor])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PaperCertificate_gen_Vendor'
CREATE INDEX [IX_FK_PaperCertificate_gen_Vendor]
ON [dbo].[PaperCertificate]
    ([Fk_Vendor]);
GO

-- Creating foreign key on [Fk_Material] in table 'PaperCertificate'
ALTER TABLE [dbo].[PaperCertificate]
ADD CONSTRAINT [FK_PaperCertificate_Inv_Material]
    FOREIGN KEY ([Fk_Material])
    REFERENCES [dbo].[Inv_Material]
        ([Pk_Material])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PaperCertificate_Inv_Material'
CREATE INDEX [IX_FK_PaperCertificate_Inv_Material]
ON [dbo].[PaperCertificate]
    ([Fk_Material]);
GO

-- Creating foreign key on [Fk_PkID] in table 'PaperCertificateDetails'
ALTER TABLE [dbo].[PaperCertificateDetails]
ADD CONSTRAINT [FK_PaperCertificateDetails_PaperCertificate]
    FOREIGN KEY ([Fk_PkID])
    REFERENCES [dbo].[PaperCertificate]
        ([Pk_Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PaperCertificateDetails_PaperCertificate'
CREATE INDEX [IX_FK_PaperCertificateDetails_PaperCertificate]
ON [dbo].[PaperCertificateDetails]
    ([Fk_PkID]);
GO

-- Creating foreign key on [Fk_Material] in table 'PurchaseOrderD'
ALTER TABLE [dbo].[PurchaseOrderD]
ADD CONSTRAINT [FK_PurchaseOrderD_Inv_Material]
    FOREIGN KEY ([Fk_Material])
    REFERENCES [dbo].[Inv_Material]
        ([Pk_Material])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PurchaseOrderD_Inv_Material'
CREATE INDEX [IX_FK_PurchaseOrderD_Inv_Material]
ON [dbo].[PurchaseOrderD]
    ([Fk_Material]);
GO

-- Creating foreign key on [Fk_ProcessID] in table 'JProcess'
ALTER TABLE [dbo].[JProcess]
ADD CONSTRAINT [FK_JProcess_ProcessMaster]
    FOREIGN KEY ([Fk_ProcessID])
    REFERENCES [dbo].[ProcessMaster]
        ([Pk_Process])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JProcess_ProcessMaster'
CREATE INDEX [IX_FK_JProcess_ProcessMaster]
ON [dbo].[JProcess]
    ([Fk_ProcessID]);
GO

-- Creating foreign key on [Fk_MastID] in table 'COADetails'
ALTER TABLE [dbo].[COADetails]
ADD CONSTRAINT [FK_COADetails_COA]
    FOREIGN KEY ([Fk_MastID])
    REFERENCES [dbo].[COA]
        ([Pk_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_COADetails_COA'
CREATE INDEX [IX_FK_COADetails_COA]
ON [dbo].[COADetails]
    ([Fk_MastID]);
GO

-- Creating foreign key on [Fk_ParaID] in table 'COADetails'
ALTER TABLE [dbo].[COADetails]
ADD CONSTRAINT [FK_COADetails_COA_Parameters]
    FOREIGN KEY ([Fk_ParaID])
    REFERENCES [dbo].[COA_Parameters]
        ([Pk_ParaID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_COADetails_COA_Parameters'
CREATE INDEX [IX_FK_COADetails_COA_Parameters]
ON [dbo].[COADetails]
    ([Fk_ParaID]);
GO

-- Creating foreign key on [Fk_MatCatID] in table 'Semi_FinishedGoodsStock'
ALTER TABLE [dbo].[Semi_FinishedGoodsStock]
ADD CONSTRAINT [FK_Semi_FinishedGoodsStock_Inv_MaterialCategory]
    FOREIGN KEY ([Fk_MatCatID])
    REFERENCES [dbo].[Inv_MaterialCategory]
        ([Pk_MaterialCategory])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Semi_FinishedGoodsStock_Inv_MaterialCategory'
CREATE INDEX [IX_FK_Semi_FinishedGoodsStock_Inv_MaterialCategory]
ON [dbo].[Semi_FinishedGoodsStock]
    ([Fk_MatCatID]);
GO

-- Creating foreign key on [PrdName] in table 'PartJobsReturns'
ALTER TABLE [dbo].[PartJobsReturns]
ADD CONSTRAINT [FK_PartJobsReturns_Inv_MaterialCategory]
    FOREIGN KEY ([PrdName])
    REFERENCES [dbo].[Inv_MaterialCategory]
        ([Pk_MaterialCategory])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PartJobsReturns_Inv_MaterialCategory'
CREATE INDEX [IX_FK_PartJobsReturns_Inv_MaterialCategory]
ON [dbo].[PartJobsReturns]
    ([PrdName]);
GO

-- Creating foreign key on [Fk_PJID] in table 'PartJobsReturns'
ALTER TABLE [dbo].[PartJobsReturns]
ADD CONSTRAINT [FK_PartJobsReturns_PartJobs]
    FOREIGN KEY ([Fk_PJID])
    REFERENCES [dbo].[PartJobs]
        ([Pk_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PartJobsReturns_PartJobs'
CREATE INDEX [IX_FK_PartJobsReturns_PartJobs]
ON [dbo].[PartJobsReturns]
    ([Fk_PJID]);
GO

-- Creating foreign key on [Fk_Status] in table 'PartJobsReturns'
ALTER TABLE [dbo].[PartJobsReturns]
ADD CONSTRAINT [FK_PartJobsReturns_wfStates]
    FOREIGN KEY ([Fk_Status])
    REFERENCES [dbo].[wfStates]
        ([Pk_State])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PartJobsReturns_wfStates'
CREATE INDEX [IX_FK_PartJobsReturns_wfStates]
ON [dbo].[PartJobsReturns]
    ([Fk_Status]);
GO

-- Creating foreign key on [Fk_BranchID] in table 'JWStocks'
ALTER TABLE [dbo].[JWStocks]
ADD CONSTRAINT [FK_JWStocks_Branch]
    FOREIGN KEY ([Fk_BranchID])
    REFERENCES [dbo].[Branch]
        ([Pk_BranchID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JWStocks_Branch'
CREATE INDEX [IX_FK_JWStocks_Branch]
ON [dbo].[JWStocks]
    ([Fk_BranchID]);
GO

-- Creating foreign key on [Fk_Vendor] in table 'JW_JobCardMaster'
ALTER TABLE [dbo].[JW_JobCardMaster]
ADD CONSTRAINT [FK_JW_JobCardMaster_gen_Vendor]
    FOREIGN KEY ([Fk_Vendor])
    REFERENCES [dbo].[gen_Vendor]
        ([Pk_Vendor])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JW_JobCardMaster_gen_Vendor'
CREATE INDEX [IX_FK_JW_JobCardMaster_gen_Vendor]
ON [dbo].[JW_JobCardMaster]
    ([Fk_Vendor]);
GO

-- Creating foreign key on [Fk_Material] in table 'JWStocks'
ALTER TABLE [dbo].[JWStocks]
ADD CONSTRAINT [FK_JWStocks_Inv_Material]
    FOREIGN KEY ([Fk_Material])
    REFERENCES [dbo].[Inv_Material]
        ([Pk_Material])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JWStocks_Inv_Material'
CREATE INDEX [IX_FK_JWStocks_Inv_Material]
ON [dbo].[JWStocks]
    ([Fk_Material]);
GO

-- Creating foreign key on [Fk_Status] in table 'JW_JobCardMaster'
ALTER TABLE [dbo].[JW_JobCardMaster]
ADD CONSTRAINT [FK_JW_JobCardMaster_wfStates]
    FOREIGN KEY ([Fk_Status])
    REFERENCES [dbo].[wfStates]
        ([Pk_State])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JW_JobCardMaster_wfStates'
CREATE INDEX [IX_FK_JW_JobCardMaster_wfStates]
ON [dbo].[JW_JobCardMaster]
    ([Fk_Status]);
GO

-- Creating foreign key on [Fk_Tanent] in table 'JWStocks'
ALTER TABLE [dbo].[JWStocks]
ADD CONSTRAINT [FK_JWStocks_wgTenant]
    FOREIGN KEY ([Fk_Tanent])
    REFERENCES [dbo].[wgTenant]
        ([Pk_Tanent])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JWStocks_wgTenant'
CREATE INDEX [IX_FK_JWStocks_wgTenant]
ON [dbo].[JWStocks]
    ([Fk_Tanent]);
GO

-- Creating foreign key on [Fk_Vendor] in table 'Outsourcing'
ALTER TABLE [dbo].[Outsourcing]
ADD CONSTRAINT [FK_Outsource_gen_Vendor]
    FOREIGN KEY ([Fk_Vendor])
    REFERENCES [dbo].[gen_Vendor]
        ([Pk_Vendor])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Outsource_gen_Vendor'
CREATE INDEX [IX_FK_Outsource_gen_Vendor]
ON [dbo].[Outsourcing]
    ([Fk_Vendor]);
GO

-- Creating foreign key on [Fk_JobCardID] in table 'Outsourcing'
ALTER TABLE [dbo].[Outsourcing]
ADD CONSTRAINT [FK_Outsourcing_JW_JobCardMaster]
    FOREIGN KEY ([Fk_JobCardID])
    REFERENCES [dbo].[JW_JobCardMaster]
        ([Pk_JobCardID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Outsourcing_JW_JobCardMaster'
CREATE INDEX [IX_FK_Outsourcing_JW_JobCardMaster]
ON [dbo].[Outsourcing]
    ([Fk_JobCardID]);
GO

-- Creating foreign key on [Fk_Status] in table 'Outsourcing'
ALTER TABLE [dbo].[Outsourcing]
ADD CONSTRAINT [FK_Outsourcing_wfStates]
    FOREIGN KEY ([Fk_Status])
    REFERENCES [dbo].[wfStates]
        ([Pk_State])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Outsourcing_wfStates'
CREATE INDEX [IX_FK_Outsourcing_wfStates]
ON [dbo].[Outsourcing]
    ([Fk_Status]);
GO

-- Creating foreign key on [Fk_PkSrcID] in table 'OutsourcingChild'
ALTER TABLE [dbo].[OutsourcingChild]
ADD CONSTRAINT [FK_OutsourcingChild_Outsourcing]
    FOREIGN KEY ([Fk_PkSrcID])
    REFERENCES [dbo].[Outsourcing]
        ([Pk_SrcID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_OutsourcingChild_Outsourcing'
CREATE INDEX [IX_FK_OutsourcingChild_Outsourcing]
ON [dbo].[OutsourcingChild]
    ([Fk_PkSrcID]);
GO

-- Creating foreign key on [Fk_JobCardID] in table 'JobWorkReceivables'
ALTER TABLE [dbo].[JobWorkReceivables]
ADD CONSTRAINT [FK_JobWorkReceivables_JW_JobCardMaster]
    FOREIGN KEY ([Fk_JobCardID])
    REFERENCES [dbo].[JW_JobCardMaster]
        ([Pk_JobCardID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JobWorkReceivables_JW_JobCardMaster'
CREATE INDEX [IX_FK_JobWorkReceivables_JW_JobCardMaster]
ON [dbo].[JobWorkReceivables]
    ([Fk_JobCardID]);
GO

-- Creating foreign key on [Fk_Status] in table 'JobWorkReceivables'
ALTER TABLE [dbo].[JobWorkReceivables]
ADD CONSTRAINT [FK_JobWorkReceivables_wfStates]
    FOREIGN KEY ([Fk_Status])
    REFERENCES [dbo].[wfStates]
        ([Pk_State])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JobWorkReceivables_wfStates'
CREATE INDEX [IX_FK_JobWorkReceivables_wfStates]
ON [dbo].[JobWorkReceivables]
    ([Fk_Status]);
GO

-- Creating foreign key on [Fk_Material] in table 'JW_RM_RetDet'
ALTER TABLE [dbo].[JW_RM_RetDet]
ADD CONSTRAINT [FK_JW_RM_RetDet_Inv_Material]
    FOREIGN KEY ([Fk_Material])
    REFERENCES [dbo].[Inv_Material]
        ([Pk_Material])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JW_RM_RetDet_Inv_Material'
CREATE INDEX [IX_FK_JW_RM_RetDet_Inv_Material]
ON [dbo].[JW_RM_RetDet]
    ([Fk_Material]);
GO

-- Creating foreign key on [Fk_Inward] in table 'JW_RM_RetDet'
ALTER TABLE [dbo].[JW_RM_RetDet]
ADD CONSTRAINT [FK_JW_RM_RetDet_JW_RM_RetMast]
    FOREIGN KEY ([Fk_Inward])
    REFERENCES [dbo].[JW_RM_RetMast]
        ([Pk_Inward])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JW_RM_RetDet_JW_RM_RetMast'
CREATE INDEX [IX_FK_JW_RM_RetDet_JW_RM_RetMast]
ON [dbo].[JW_RM_RetDet]
    ([Fk_Inward]);
GO

-- Creating foreign key on [JWNo] in table 'JW_RM_RetMast'
ALTER TABLE [dbo].[JW_RM_RetMast]
ADD CONSTRAINT [FK_JW_RM_RetMast_PartJobs]
    FOREIGN KEY ([JWNo])
    REFERENCES [dbo].[PartJobs]
        ([Pk_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JW_RM_RetMast_PartJobs'
CREATE INDEX [IX_FK_JW_RM_RetMast_PartJobs]
ON [dbo].[JW_RM_RetMast]
    ([JWNo]);
GO

-- Creating foreign key on [Fk_Vendor] in table 'JWMat_InwdM'
ALTER TABLE [dbo].[JWMat_InwdM]
ADD CONSTRAINT [FK_JWMat_InwdM_gen_Vendor]
    FOREIGN KEY ([Fk_Vendor])
    REFERENCES [dbo].[gen_Vendor]
        ([Pk_Vendor])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JWMat_InwdM_gen_Vendor'
CREATE INDEX [IX_FK_JWMat_InwdM_gen_Vendor]
ON [dbo].[JWMat_InwdM]
    ([Fk_Vendor]);
GO

-- Creating foreign key on [Fk_Material] in table 'JWMat_InwdD'
ALTER TABLE [dbo].[JWMat_InwdD]
ADD CONSTRAINT [FK_JWMat_InwdD_Inv_Material]
    FOREIGN KEY ([Fk_Material])
    REFERENCES [dbo].[Inv_Material]
        ([Pk_Material])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JWMat_InwdD_Inv_Material'
CREATE INDEX [IX_FK_JWMat_InwdD_Inv_Material]
ON [dbo].[JWMat_InwdD]
    ([Fk_Material]);
GO

-- Creating foreign key on [Fk_Inward] in table 'JWMat_InwdD'
ALTER TABLE [dbo].[JWMat_InwdD]
ADD CONSTRAINT [FK_JWMat_InwdD_JWMat_InwdM]
    FOREIGN KEY ([Fk_Inward])
    REFERENCES [dbo].[JWMat_InwdM]
        ([Pk_Inward])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JWMat_InwdD_JWMat_InwdM'
CREATE INDEX [IX_FK_JWMat_InwdD_JWMat_InwdM]
ON [dbo].[JWMat_InwdD]
    ([Fk_Inward]);
GO

-- Creating foreign key on [Fk_CustomerID] in table 'CustomerComplaints'
ALTER TABLE [dbo].[CustomerComplaints]
ADD CONSTRAINT [FK_CustomerComplaints_gen_Customer]
    FOREIGN KEY ([Fk_CustomerID])
    REFERENCES [dbo].[gen_Customer]
        ([Pk_Customer])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CustomerComplaints_gen_Customer'
CREATE INDEX [IX_FK_CustomerComplaints_gen_Customer]
ON [dbo].[CustomerComplaints]
    ([Fk_CustomerID]);
GO

-- Creating foreign key on [Fk_Customer] in table 'CustomerRejections'
ALTER TABLE [dbo].[CustomerRejections]
ADD CONSTRAINT [FK_CustomerRejections_gen_Customer]
    FOREIGN KEY ([Fk_Customer])
    REFERENCES [dbo].[gen_Customer]
        ([Pk_Customer])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CustomerRejections_gen_Customer'
CREATE INDEX [IX_FK_CustomerRejections_gen_Customer]
ON [dbo].[CustomerRejections]
    ([Fk_Customer]);
GO

-- Creating foreign key on [Fk_Customer] in table 'DispatchMast'
ALTER TABLE [dbo].[DispatchMast]
ADD CONSTRAINT [FK_DispatchMast_gen_Customer]
    FOREIGN KEY ([Fk_Customer])
    REFERENCES [dbo].[gen_Customer]
        ([Pk_Customer])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DispatchMast_gen_Customer'
CREATE INDEX [IX_FK_DispatchMast_gen_Customer]
ON [dbo].[DispatchMast]
    ([Fk_Customer]);
GO

-- Creating foreign key on [Customer] in table 'eq_Enquiry'
ALTER TABLE [dbo].[eq_Enquiry]
ADD CONSTRAINT [FK_eq_Enquiry_gen_Customer]
    FOREIGN KEY ([Customer])
    REFERENCES [dbo].[gen_Customer]
        ([Pk_Customer])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_eq_Enquiry_gen_Customer'
CREATE INDEX [IX_FK_eq_Enquiry_gen_Customer]
ON [dbo].[eq_Enquiry]
    ([Customer]);
GO

-- Creating foreign key on [Fk_City] in table 'gen_Customer'
ALTER TABLE [dbo].[gen_Customer]
ADD CONSTRAINT [FK_gen_Customer_gen_City]
    FOREIGN KEY ([Fk_City])
    REFERENCES [dbo].[gen_City]
        ([Pk_CityID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_gen_Customer_gen_City'
CREATE INDEX [IX_FK_gen_Customer_gen_City]
ON [dbo].[gen_Customer]
    ([Fk_City]);
GO

-- Creating foreign key on [Fk_Country] in table 'gen_Customer'
ALTER TABLE [dbo].[gen_Customer]
ADD CONSTRAINT [FK_gen_Customer_gen_Country]
    FOREIGN KEY ([Fk_Country])
    REFERENCES [dbo].[gen_Country]
        ([Pk_Country])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_gen_Customer_gen_Country'
CREATE INDEX [IX_FK_gen_Customer_gen_Country]
ON [dbo].[gen_Customer]
    ([Fk_Country]);
GO

-- Creating foreign key on [Fk_State] in table 'gen_Customer'
ALTER TABLE [dbo].[gen_Customer]
ADD CONSTRAINT [FK_gen_Customer_gen_State]
    FOREIGN KEY ([Fk_State])
    REFERENCES [dbo].[gen_State]
        ([Pk_StateID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_gen_Customer_gen_State'
CREATE INDEX [IX_FK_gen_Customer_gen_State]
ON [dbo].[gen_Customer]
    ([Fk_State]);
GO

-- Creating foreign key on [Fk_Customer] in table 'gen_CustomerContacts'
ALTER TABLE [dbo].[gen_CustomerContacts]
ADD CONSTRAINT [FK_gen_CustomerContacts_gen_Customer]
    FOREIGN KEY ([Fk_Customer])
    REFERENCES [dbo].[gen_Customer]
        ([Pk_Customer])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_gen_CustomerContacts_gen_Customer'
CREATE INDEX [IX_FK_gen_CustomerContacts_gen_Customer]
ON [dbo].[gen_CustomerContacts]
    ([Fk_Customer]);
GO

-- Creating foreign key on [Fk_CustomerId] in table 'gen_CustomerShippingDetails'
ALTER TABLE [dbo].[gen_CustomerShippingDetails]
ADD CONSTRAINT [FK_gen_CustomerShippingDetails_gen_Customer]
    FOREIGN KEY ([Fk_CustomerId])
    REFERENCES [dbo].[gen_Customer]
        ([Pk_Customer])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_gen_CustomerShippingDetails_gen_Customer'
CREATE INDEX [IX_FK_gen_CustomerShippingDetails_gen_Customer]
ON [dbo].[gen_CustomerShippingDetails]
    ([Fk_CustomerId]);
GO

-- Creating foreign key on [Fk_Customer] in table 'JobWorkRMStock'
ALTER TABLE [dbo].[JobWorkRMStock]
ADD CONSTRAINT [FK_JobWorkRMStock_gen_Customer]
    FOREIGN KEY ([Fk_Customer])
    REFERENCES [dbo].[gen_Customer]
        ([Pk_Customer])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JobWorkRMStock_gen_Customer'
CREATE INDEX [IX_FK_JobWorkRMStock_gen_Customer]
ON [dbo].[JobWorkRMStock]
    ([Fk_Customer]);
GO

-- Creating foreign key on [Fk_Customer] in table 'JW_RM_RetMast'
ALTER TABLE [dbo].[JW_RM_RetMast]
ADD CONSTRAINT [FK_JW_RM_RetMast_gen_Customer]
    FOREIGN KEY ([Fk_Customer])
    REFERENCES [dbo].[gen_Customer]
        ([Pk_Customer])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JW_RM_RetMast_gen_Customer'
CREATE INDEX [IX_FK_JW_RM_RetMast_gen_Customer]
ON [dbo].[JW_RM_RetMast]
    ([Fk_Customer]);
GO

-- Creating foreign key on [Fk_Vendor] in table 'JWMat_InwdM'
ALTER TABLE [dbo].[JWMat_InwdM]
ADD CONSTRAINT [FK_JWMat_InwdM_gen_Customer]
    FOREIGN KEY ([Fk_Vendor])
    REFERENCES [dbo].[gen_Customer]
        ([Pk_Customer])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JWMat_InwdM_gen_Customer'
CREATE INDEX [IX_FK_JWMat_InwdM_gen_Customer]
ON [dbo].[JWMat_InwdM]
    ([Fk_Vendor]);
GO

-- Creating foreign key on [Fk_Customer] in table 'PartJobs'
ALTER TABLE [dbo].[PartJobs]
ADD CONSTRAINT [FK_PartJobs_gen_Customer]
    FOREIGN KEY ([Fk_Customer])
    REFERENCES [dbo].[gen_Customer]
        ([Pk_Customer])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PartJobs_gen_Customer'
CREATE INDEX [IX_FK_PartJobs_gen_Customer]
ON [dbo].[PartJobs]
    ([Fk_Customer]);
GO

-- Creating foreign key on [Fk_Customer] in table 'Receipt'
ALTER TABLE [dbo].[Receipt]
ADD CONSTRAINT [FK_Receipt_gen_Customer]
    FOREIGN KEY ([Fk_Customer])
    REFERENCES [dbo].[gen_Customer]
        ([Pk_Customer])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Receipt_gen_Customer'
CREATE INDEX [IX_FK_Receipt_gen_Customer]
ON [dbo].[Receipt]
    ([Fk_Customer]);
GO

-- Creating foreign key on [Customer] in table 'SampleBoxMaster'
ALTER TABLE [dbo].[SampleBoxMaster]
ADD CONSTRAINT [FK_SampleBoxMaster_gen_Customer]
    FOREIGN KEY ([Customer])
    REFERENCES [dbo].[gen_Customer]
        ([Pk_Customer])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SampleBoxMaster_gen_Customer'
CREATE INDEX [IX_FK_SampleBoxMaster_gen_Customer]
ON [dbo].[SampleBoxMaster]
    ([Customer]);
GO

-- Creating foreign key on [Fk_BoxType] in table 'BoxMaster'
ALTER TABLE [dbo].[BoxMaster]
ADD CONSTRAINT [FK_BoxMaster_BoxType]
    FOREIGN KEY ([Fk_BoxType])
    REFERENCES [dbo].[BoxType]
        ([Pk_BoxTypeID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_BoxMaster_BoxType'
CREATE INDEX [IX_FK_BoxMaster_BoxType]
ON [dbo].[BoxMaster]
    ([Fk_BoxType]);
GO

-- Creating foreign key on [Fk_FluteType] in table 'BoxMaster'
ALTER TABLE [dbo].[BoxMaster]
ADD CONSTRAINT [FK_BoxMaster_FluteType]
    FOREIGN KEY ([Fk_FluteType])
    REFERENCES [dbo].[FluteType]
        ([Pk_FluteID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_BoxMaster_FluteType'
CREATE INDEX [IX_FK_BoxMaster_FluteType]
ON [dbo].[BoxMaster]
    ([Fk_FluteType]);
GO

-- Creating foreign key on [Customer] in table 'BoxMaster'
ALTER TABLE [dbo].[BoxMaster]
ADD CONSTRAINT [FK_BoxMaster_gen_Customer]
    FOREIGN KEY ([Customer])
    REFERENCES [dbo].[gen_Customer]
        ([Pk_Customer])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_BoxMaster_gen_Customer'
CREATE INDEX [IX_FK_BoxMaster_gen_Customer]
ON [dbo].[BoxMaster]
    ([Customer]);
GO

-- Creating foreign key on [Fk_BoxID] in table 'BoxSpecs'
ALTER TABLE [dbo].[BoxSpecs]
ADD CONSTRAINT [FK_BoxSpecs_BoxMast]
    FOREIGN KEY ([Fk_BoxID])
    REFERENCES [dbo].[BoxMaster]
        ([Pk_BoxID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_BoxSpecs_BoxMast'
CREATE INDEX [IX_FK_BoxSpecs_BoxMast]
ON [dbo].[BoxSpecs]
    ([Fk_BoxID]);
GO

-- Creating foreign key on [Fk_BoxID] in table 'BoxStock'
ALTER TABLE [dbo].[BoxStock]
ADD CONSTRAINT [FK_BoxStock_BoxStock]
    FOREIGN KEY ([Fk_BoxID])
    REFERENCES [dbo].[BoxMaster]
        ([Pk_BoxID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_BoxStock_BoxStock'
CREATE INDEX [IX_FK_BoxStock_BoxStock]
ON [dbo].[BoxStock]
    ([Fk_BoxID]);
GO

-- Creating foreign key on [Fk_BoxID] in table 'DispatchChild'
ALTER TABLE [dbo].[DispatchChild]
ADD CONSTRAINT [FK_DispatchChild_BoxMaster]
    FOREIGN KEY ([Fk_BoxID])
    REFERENCES [dbo].[BoxMaster]
        ([Pk_BoxID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DispatchChild_BoxMaster'
CREATE INDEX [IX_FK_DispatchChild_BoxMaster]
ON [dbo].[DispatchChild]
    ([Fk_BoxID]);
GO

-- Creating foreign key on [Fk_BoxID] in table 'eq_EnquiryChild'
ALTER TABLE [dbo].[eq_EnquiryChild]
ADD CONSTRAINT [FK_eq_EnquiryChild_BoxMaster1]
    FOREIGN KEY ([Fk_BoxID])
    REFERENCES [dbo].[BoxMaster]
        ([Pk_BoxID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_eq_EnquiryChild_BoxMaster1'
CREATE INDEX [IX_FK_eq_EnquiryChild_BoxMaster1]
ON [dbo].[eq_EnquiryChild]
    ([Fk_BoxID]);
GO

-- Creating foreign key on [Fk_BoxID] in table 'FinishedGoodsStock'
ALTER TABLE [dbo].[FinishedGoodsStock]
ADD CONSTRAINT [FK_FinishedGoodsStock_BoxMast]
    FOREIGN KEY ([Fk_BoxID])
    REFERENCES [dbo].[BoxMaster]
        ([Pk_BoxID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_FinishedGoodsStock_BoxMast'
CREATE INDEX [IX_FK_FinishedGoodsStock_BoxMast]
ON [dbo].[FinishedGoodsStock]
    ([Fk_BoxID]);
GO

-- Creating foreign key on [Fk_BoxID] in table 'gen_DeliverySchedule'
ALTER TABLE [dbo].[gen_DeliverySchedule]
ADD CONSTRAINT [FK_gen_DeliverySchedule_BoxMaster]
    FOREIGN KEY ([Fk_BoxID])
    REFERENCES [dbo].[BoxMaster]
        ([Pk_BoxID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_gen_DeliverySchedule_BoxMaster'
CREATE INDEX [IX_FK_gen_DeliverySchedule_BoxMaster]
ON [dbo].[gen_DeliverySchedule]
    ([Fk_BoxID]);
GO

-- Creating foreign key on [Fk_BoxID] in table 'JW_JobCardMaster'
ALTER TABLE [dbo].[JW_JobCardMaster]
ADD CONSTRAINT [FK_JW_JobCardMaster_BoxMaster]
    FOREIGN KEY ([Fk_BoxID])
    REFERENCES [dbo].[BoxMaster]
        ([Pk_BoxID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JW_JobCardMaster_BoxMaster'
CREATE INDEX [IX_FK_JW_JobCardMaster_BoxMaster]
ON [dbo].[JW_JobCardMaster]
    ([Fk_BoxID]);
GO

-- Creating foreign key on [BoxID] in table 'QuotationDetails'
ALTER TABLE [dbo].[QuotationDetails]
ADD CONSTRAINT [FK_QuotationDetails_BoxMaster]
    FOREIGN KEY ([BoxID])
    REFERENCES [dbo].[BoxMaster]
        ([Pk_BoxID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_QuotationDetails_BoxMaster'
CREATE INDEX [IX_FK_QuotationDetails_BoxMaster]
ON [dbo].[QuotationDetails]
    ([BoxID]);
GO

-- Creating foreign key on [Fk_BoxID] in table 'Box_Documents'
ALTER TABLE [dbo].[Box_Documents]
ADD CONSTRAINT [FK_Box_Documents_BoxMaster]
    FOREIGN KEY ([Fk_BoxID])
    REFERENCES [dbo].[BoxMaster]
        ([Pk_BoxID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Box_Documents_BoxMaster'
CREATE INDEX [IX_FK_Box_Documents_BoxMaster]
ON [dbo].[Box_Documents]
    ([Fk_BoxID]);
GO

-- Creating foreign key on [Fk_Mill] in table 'Inv_MaterialIndentDetails'
ALTER TABLE [dbo].[Inv_MaterialIndentDetails]
ADD CONSTRAINT [FK_Inv_MaterialIndentDetails_gen_Mill]
    FOREIGN KEY ([Fk_Mill])
    REFERENCES [dbo].[gen_Mill]
        ([Pk_Mill])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Inv_MaterialIndentDetails_gen_Mill'
CREATE INDEX [IX_FK_Inv_MaterialIndentDetails_gen_Mill]
ON [dbo].[Inv_MaterialIndentDetails]
    ([Fk_Mill]);
GO

-- Creating foreign key on [Fk_Vendor] in table 'Inv_MaterialIndentDetails'
ALTER TABLE [dbo].[Inv_MaterialIndentDetails]
ADD CONSTRAINT [FK_Inv_MaterialIndentDetails_gen_Vendor]
    FOREIGN KEY ([Fk_Vendor])
    REFERENCES [dbo].[gen_Vendor]
        ([Pk_Vendor])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Inv_MaterialIndentDetails_gen_Vendor'
CREATE INDEX [IX_FK_Inv_MaterialIndentDetails_gen_Vendor]
ON [dbo].[Inv_MaterialIndentDetails]
    ([Fk_Vendor]);
GO

-- Creating foreign key on [Fk_Material] in table 'Inv_MaterialIndentDetails'
ALTER TABLE [dbo].[Inv_MaterialIndentDetails]
ADD CONSTRAINT [FK_Inv_MaterialIndentDetails_Inv_Material]
    FOREIGN KEY ([Fk_Material])
    REFERENCES [dbo].[Inv_Material]
        ([Pk_Material])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Inv_MaterialIndentDetails_Inv_Material'
CREATE INDEX [IX_FK_Inv_MaterialIndentDetails_Inv_Material]
ON [dbo].[Inv_MaterialIndentDetails]
    ([Fk_Material]);
GO

-- Creating foreign key on [Fk_MaterialOrderMasterId] in table 'Inv_MaterialIndentDetails'
ALTER TABLE [dbo].[Inv_MaterialIndentDetails]
ADD CONSTRAINT [FK_MaterialIndentDetails_MaterialIndent]
    FOREIGN KEY ([Fk_MaterialOrderMasterId])
    REFERENCES [dbo].[Inv_MaterialIndentMaster]
        ([Pk_MaterialOrderMasterId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MaterialIndentDetails_MaterialIndent'
CREATE INDEX [IX_FK_MaterialIndentDetails_MaterialIndent]
ON [dbo].[Inv_MaterialIndentDetails]
    ([Fk_MaterialOrderMasterId]);
GO

-- Creating foreign key on [Fk_ID] in table 'ConsumableIssueDet'
ALTER TABLE [dbo].[ConsumableIssueDet]
ADD CONSTRAINT [FK_ConsumableIssueDet_ConsumablesIssue]
    FOREIGN KEY ([Fk_ID])
    REFERENCES [dbo].[ConsumablesIssue]
        ([Pk_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ConsumableIssueDet_ConsumablesIssue'
CREATE INDEX [IX_FK_ConsumableIssueDet_ConsumablesIssue]
ON [dbo].[ConsumableIssueDet]
    ([Fk_ID]);
GO

-- Creating foreign key on [Fk_Material] in table 'ConsumableIssueDet'
ALTER TABLE [dbo].[ConsumableIssueDet]
ADD CONSTRAINT [FK_ConsumableIssueDet_Inv_Material]
    FOREIGN KEY ([Fk_Material])
    REFERENCES [dbo].[Inv_Material]
        ([Pk_Material])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ConsumableIssueDet_Inv_Material'
CREATE INDEX [IX_FK_ConsumableIssueDet_Inv_Material]
ON [dbo].[ConsumableIssueDet]
    ([Fk_Material]);
GO

-- Creating foreign key on [Fk_Material] in table 'WorkOrderDetails'
ALTER TABLE [dbo].[WorkOrderDetails]
ADD CONSTRAINT [FK_WorkOrderDetails_Inv_Material]
    FOREIGN KEY ([Fk_Material])
    REFERENCES [dbo].[Inv_Material]
        ([Pk_Material])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_WorkOrderDetails_Inv_Material'
CREATE INDEX [IX_FK_WorkOrderDetails_Inv_Material]
ON [dbo].[WorkOrderDetails]
    ([Fk_Material]);
GO

-- Creating foreign key on [Fk_PaperStock] in table 'WorkOrderDetails'
ALTER TABLE [dbo].[WorkOrderDetails]
ADD CONSTRAINT [FK_WorkOrderDetails_PaperStock]
    FOREIGN KEY ([Fk_PaperStock])
    REFERENCES [dbo].[PaperStock]
        ([Pk_PaperStock])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_WorkOrderDetails_PaperStock'
CREATE INDEX [IX_FK_WorkOrderDetails_PaperStock]
ON [dbo].[WorkOrderDetails]
    ([Fk_PaperStock]);
GO

-- Creating foreign key on [Fk_Material] in table 'JobWorkChild'
ALTER TABLE [dbo].[JobWorkChild]
ADD CONSTRAINT [FK_JobWorkChild_Inv_Material]
    FOREIGN KEY ([Fk_Material])
    REFERENCES [dbo].[Inv_Material]
        ([Pk_Material])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JobWorkChild_Inv_Material'
CREATE INDEX [IX_FK_JobWorkChild_Inv_Material]
ON [dbo].[JobWorkChild]
    ([Fk_Material]);
GO

-- Creating foreign key on [Fk_JobID] in table 'JobWorkChild'
ALTER TABLE [dbo].[JobWorkChild]
ADD CONSTRAINT [FK_JobWorkChild_JobWorkChild]
    FOREIGN KEY ([Fk_JobID])
    REFERENCES [dbo].[PartJobs]
        ([Pk_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JobWorkChild_JobWorkChild'
CREATE INDEX [IX_FK_JobWorkChild_JobWorkChild]
ON [dbo].[JobWorkChild]
    ([Fk_JobID]);
GO

-- Creating foreign key on [Fk_BoxID] in table 'WorkOrder'
ALTER TABLE [dbo].[WorkOrder]
ADD CONSTRAINT [FK_WorkOrder_BoxMaster]
    FOREIGN KEY ([Fk_BoxID])
    REFERENCES [dbo].[BoxMaster]
        ([Pk_BoxID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_WorkOrder_BoxMaster'
CREATE INDEX [IX_FK_WorkOrder_BoxMaster]
ON [dbo].[WorkOrder]
    ([Fk_BoxID]);
GO

-- Creating foreign key on [Fk_Material] in table 'gen_DeliverySchedule'
ALTER TABLE [dbo].[gen_DeliverySchedule]
ADD CONSTRAINT [FK_gen_DeliverySchedule_Inv_Material]
    FOREIGN KEY ([Fk_Material])
    REFERENCES [dbo].[Inv_Material]
        ([Pk_Material])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_gen_DeliverySchedule_Inv_Material'
CREATE INDEX [IX_FK_gen_DeliverySchedule_Inv_Material]
ON [dbo].[gen_DeliverySchedule]
    ([Fk_Material]);
GO

-- Creating foreign key on [Fk_Schedule] in table 'WorkOrder'
ALTER TABLE [dbo].[WorkOrder]
ADD CONSTRAINT [FK_WorkOrder_gen_DeliverySchedule]
    FOREIGN KEY ([Fk_Schedule])
    REFERENCES [dbo].[gen_DeliverySchedule]
        ([Pk_DeliverySechedule])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_WorkOrder_gen_DeliverySchedule'
CREATE INDEX [IX_FK_WorkOrder_gen_DeliverySchedule]
ON [dbo].[WorkOrder]
    ([Fk_Schedule]);
GO

-- Creating foreign key on [Fk_Status] in table 'WorkOrder'
ALTER TABLE [dbo].[WorkOrder]
ADD CONSTRAINT [FK_WorkOrder_wfStates]
    FOREIGN KEY ([Fk_Status])
    REFERENCES [dbo].[wfStates]
        ([Pk_State])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_WorkOrder_wfStates'
CREATE INDEX [IX_FK_WorkOrder_wfStates]
ON [dbo].[WorkOrder]
    ([Fk_Status]);
GO

-- Creating foreign key on [Fk_WONo] in table 'WO_Documents'
ALTER TABLE [dbo].[WO_Documents]
ADD CONSTRAINT [FK_WO_Documents_WorkOrder]
    FOREIGN KEY ([Fk_WONo])
    REFERENCES [dbo].[WorkOrder]
        ([WorkOrder_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_WO_Documents_WorkOrder'
CREATE INDEX [IX_FK_WO_Documents_WorkOrder]
ON [dbo].[WO_Documents]
    ([Fk_WONo]);
GO

-- Creating foreign key on [Fk_WorkOrderID] in table 'WorkOrderDetails'
ALTER TABLE [dbo].[WorkOrderDetails]
ADD CONSTRAINT [FK_WorkOrderDetails_WorkOrder]
    FOREIGN KEY ([Fk_WorkOrderID])
    REFERENCES [dbo].[WorkOrder]
        ([WorkOrder_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_WorkOrderDetails_WorkOrder'
CREATE INDEX [IX_FK_WorkOrderDetails_WorkOrder]
ON [dbo].[WorkOrderDetails]
    ([Fk_WorkOrderID]);
GO

-- Creating foreign key on [Fk_Branch] in table 'MaterialIssue'
ALTER TABLE [dbo].[MaterialIssue]
ADD CONSTRAINT [FK_MaterialIssue_Branch]
    FOREIGN KEY ([Fk_Branch])
    REFERENCES [dbo].[Branch]
        ([Pk_BranchID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MaterialIssue_Branch'
CREATE INDEX [IX_FK_MaterialIssue_Branch]
ON [dbo].[MaterialIssue]
    ([Fk_Branch]);
GO

-- Creating foreign key on [Fk_UserID] in table 'MaterialIssue'
ALTER TABLE [dbo].[MaterialIssue]
ADD CONSTRAINT [FK_MaterialIssue_ideUser]
    FOREIGN KEY ([Fk_UserID])
    REFERENCES [dbo].[ideUser]
        ([Pk_Users])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MaterialIssue_ideUser'
CREATE INDEX [IX_FK_MaterialIssue_ideUser]
ON [dbo].[MaterialIssue]
    ([Fk_UserID]);
GO

-- Creating foreign key on [Fk_IssueId] in table 'IssueReturn'
ALTER TABLE [dbo].[IssueReturn]
ADD CONSTRAINT [FK_IssueReturnMaster_MaterialIssue]
    FOREIGN KEY ([Fk_IssueId])
    REFERENCES [dbo].[MaterialIssue]
        ([Pk_MaterialIssueID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_IssueReturnMaster_MaterialIssue'
CREATE INDEX [IX_FK_IssueReturnMaster_MaterialIssue]
ON [dbo].[IssueReturn]
    ([Fk_IssueId]);
GO

-- Creating foreign key on [Fk_Tanent] in table 'MaterialIssue'
ALTER TABLE [dbo].[MaterialIssue]
ADD CONSTRAINT [FK_MaterialIssue_wgTenant]
    FOREIGN KEY ([Fk_Tanent])
    REFERENCES [dbo].[wgTenant]
        ([Pk_Tanent])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MaterialIssue_wgTenant'
CREATE INDEX [IX_FK_MaterialIssue_wgTenant]
ON [dbo].[MaterialIssue]
    ([Fk_Tanent]);
GO

-- Creating foreign key on [Fk_WOrderNo] in table 'MaterialIssue'
ALTER TABLE [dbo].[MaterialIssue]
ADD CONSTRAINT [FK_MaterialIssue_WorkOrder]
    FOREIGN KEY ([Fk_WOrderNo])
    REFERENCES [dbo].[WorkOrder]
        ([WorkOrder_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MaterialIssue_WorkOrder'
CREATE INDEX [IX_FK_MaterialIssue_WorkOrder]
ON [dbo].[MaterialIssue]
    ([Fk_WOrderNo]);
GO

-- Creating foreign key on [Fk_IssueID] in table 'MaterialIssueDetails'
ALTER TABLE [dbo].[MaterialIssueDetails]
ADD CONSTRAINT [FK_MaterialIssueDetails_MaterialIssue]
    FOREIGN KEY ([Fk_IssueID])
    REFERENCES [dbo].[MaterialIssue]
        ([Pk_MaterialIssueID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MaterialIssueDetails_MaterialIssue'
CREATE INDEX [IX_FK_MaterialIssueDetails_MaterialIssue]
ON [dbo].[MaterialIssueDetails]
    ([Fk_IssueID]);
GO

-- Creating foreign key on [Fk_BoxID] in table 'JobCardMaster'
ALTER TABLE [dbo].[JobCardMaster]
ADD CONSTRAINT [FK_JobCardMaster_BoxMaster]
    FOREIGN KEY ([Fk_BoxID])
    REFERENCES [dbo].[BoxMaster]
        ([Pk_BoxID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JobCardMaster_BoxMaster'
CREATE INDEX [IX_FK_JobCardMaster_BoxMaster]
ON [dbo].[JobCardMaster]
    ([Fk_BoxID]);
GO

-- Creating foreign key on [Fk_Schedule] in table 'JobCardMaster'
ALTER TABLE [dbo].[JobCardMaster]
ADD CONSTRAINT [FK_JobCardMaster_gen_DeliverySchedule]
    FOREIGN KEY ([Fk_Schedule])
    REFERENCES [dbo].[gen_DeliverySchedule]
        ([Pk_DeliverySechedule])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JobCardMaster_gen_DeliverySchedule'
CREATE INDEX [IX_FK_JobCardMaster_gen_DeliverySchedule]
ON [dbo].[JobCardMaster]
    ([Fk_Schedule]);
GO

-- Creating foreign key on [Fk_JCNo] in table 'JC_Documents'
ALTER TABLE [dbo].[JC_Documents]
ADD CONSTRAINT [FK_JC_Documents_JobCardMaster]
    FOREIGN KEY ([Fk_JCNo])
    REFERENCES [dbo].[JobCardMaster]
        ([Pk_JobCardID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JC_Documents_JobCardMaster'
CREATE INDEX [IX_FK_JC_Documents_JobCardMaster]
ON [dbo].[JC_Documents]
    ([Fk_JCNo]);
GO

-- Creating foreign key on [Fk_JobCardID] in table 'JobCardDetails'
ALTER TABLE [dbo].[JobCardDetails]
ADD CONSTRAINT [FK_JobCardDetails_JobCardMaster]
    FOREIGN KEY ([Fk_JobCardID])
    REFERENCES [dbo].[JobCardMaster]
        ([Pk_JobCardID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JobCardDetails_JobCardMaster'
CREATE INDEX [IX_FK_JobCardDetails_JobCardMaster]
ON [dbo].[JobCardDetails]
    ([Fk_JobCardID]);
GO

-- Creating foreign key on [Fk_Status] in table 'JobCardMaster'
ALTER TABLE [dbo].[JobCardMaster]
ADD CONSTRAINT [FK_JobCardMaster_wfStates]
    FOREIGN KEY ([Fk_Status])
    REFERENCES [dbo].[wfStates]
        ([Pk_State])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JobCardMaster_wfStates'
CREATE INDEX [IX_FK_JobCardMaster_wfStates]
ON [dbo].[JobCardMaster]
    ([Fk_Status]);
GO

-- Creating foreign key on [Fk_JobCardID] in table 'JProcess'
ALTER TABLE [dbo].[JProcess]
ADD CONSTRAINT [FK_JProcess_JobCardMaster]
    FOREIGN KEY ([Fk_JobCardID])
    REFERENCES [dbo].[JobCardMaster]
        ([Pk_JobCardID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JProcess_JobCardMaster'
CREATE INDEX [IX_FK_JProcess_JobCardMaster]
ON [dbo].[JProcess]
    ([Fk_JobCardID]);
GO

-- Creating foreign key on [Fk_JobCardID] in table 'MaterialIssue'
ALTER TABLE [dbo].[MaterialIssue]
ADD CONSTRAINT [FK_MaterialIssue_JobCardMaster]
    FOREIGN KEY ([Fk_JobCardID])
    REFERENCES [dbo].[JobCardMaster]
        ([Pk_JobCardID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MaterialIssue_JobCardMaster'
CREATE INDEX [IX_FK_MaterialIssue_JobCardMaster]
ON [dbo].[MaterialIssue]
    ([Fk_JobCardID]);
GO

-- Creating foreign key on [Fk_JobCardID] in table 'WorkOrder'
ALTER TABLE [dbo].[WorkOrder]
ADD CONSTRAINT [FK_WorkOrder_gen_Order1]
    FOREIGN KEY ([Fk_JobCardID])
    REFERENCES [dbo].[JobCardMaster]
        ([Pk_JobCardID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_WorkOrder_gen_Order1'
CREATE INDEX [IX_FK_WorkOrder_gen_Order1]
ON [dbo].[WorkOrder]
    ([Fk_JobCardID]);
GO

-- Creating foreign key on [Fk_OrderNo] in table 'CustomerComplaints'
ALTER TABLE [dbo].[CustomerComplaints]
ADD CONSTRAINT [FK_CustomerComplaints_gen_Order]
    FOREIGN KEY ([Fk_OrderNo])
    REFERENCES [dbo].[gen_Order]
        ([Pk_Order])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CustomerComplaints_gen_Order'
CREATE INDEX [IX_FK_CustomerComplaints_gen_Order]
ON [dbo].[CustomerComplaints]
    ([Fk_OrderNo]);
GO

-- Creating foreign key on [Fk_OrderNo] in table 'CustomerRejections'
ALTER TABLE [dbo].[CustomerRejections]
ADD CONSTRAINT [FK_CustomerRejections_gen_Order]
    FOREIGN KEY ([Fk_OrderNo])
    REFERENCES [dbo].[gen_Order]
        ([Pk_Order])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CustomerRejections_gen_Order'
CREATE INDEX [IX_FK_CustomerRejections_gen_Order]
ON [dbo].[CustomerRejections]
    ([Fk_OrderNo]);
GO

-- Creating foreign key on [Fk_Enquiry] in table 'gen_Order'
ALTER TABLE [dbo].[gen_Order]
ADD CONSTRAINT [FK_gen_Order_eq_Enquiry1]
    FOREIGN KEY ([Fk_Enquiry])
    REFERENCES [dbo].[eq_Enquiry]
        ([Pk_Enquiry])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_gen_Order_eq_Enquiry1'
CREATE INDEX [IX_FK_gen_Order_eq_Enquiry1]
ON [dbo].[gen_Order]
    ([Fk_Enquiry]);
GO

-- Creating foreign key on [Fk_Customer] in table 'gen_Order'
ALTER TABLE [dbo].[gen_Order]
ADD CONSTRAINT [FK_gen_Order_gen_Customer1]
    FOREIGN KEY ([Fk_Customer])
    REFERENCES [dbo].[gen_Customer]
        ([Pk_Customer])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_gen_Order_gen_Customer1'
CREATE INDEX [IX_FK_gen_Order_gen_Customer1]
ON [dbo].[gen_Order]
    ([Fk_Customer]);
GO

-- Creating foreign key on [Fk_ShippingId] in table 'gen_Order'
ALTER TABLE [dbo].[gen_Order]
ADD CONSTRAINT [FK_gen_Order_gen_CustomerShippingDetails]
    FOREIGN KEY ([Fk_ShippingId])
    REFERENCES [dbo].[gen_CustomerShippingDetails]
        ([Pk_CustomerShippingId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_gen_Order_gen_CustomerShippingDetails'
CREATE INDEX [IX_FK_gen_Order_gen_CustomerShippingDetails]
ON [dbo].[gen_Order]
    ([Fk_ShippingId]);
GO

-- Creating foreign key on [Fk_Order] in table 'gen_DeliverySchedule'
ALTER TABLE [dbo].[gen_DeliverySchedule]
ADD CONSTRAINT [FK_gen_DeliverySchedule_gen_Order]
    FOREIGN KEY ([Fk_Order])
    REFERENCES [dbo].[gen_Order]
        ([Pk_Order])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_gen_DeliverySchedule_gen_Order'
CREATE INDEX [IX_FK_gen_DeliverySchedule_gen_Order]
ON [dbo].[gen_DeliverySchedule]
    ([Fk_Order]);
GO

-- Creating foreign key on [Fk_StatusVal] in table 'gen_Order'
ALTER TABLE [dbo].[gen_Order]
ADD CONSTRAINT [FK_gen_Order_wfStates]
    FOREIGN KEY ([Fk_StatusVal])
    REFERENCES [dbo].[wfStates]
        ([Pk_State])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_gen_Order_wfStates'
CREATE INDEX [IX_FK_gen_Order_wfStates]
ON [dbo].[gen_Order]
    ([Fk_StatusVal]);
GO

-- Creating foreign key on [Fk_Order] in table 'JobCardMaster'
ALTER TABLE [dbo].[JobCardMaster]
ADD CONSTRAINT [FK_JobCardMaster_gen_Order1]
    FOREIGN KEY ([Fk_Order])
    REFERENCES [dbo].[gen_Order]
        ([Pk_Order])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JobCardMaster_gen_Order1'
CREATE INDEX [IX_FK_JobCardMaster_gen_Order1]
ON [dbo].[JobCardMaster]
    ([Fk_Order]);
GO

-- Creating foreign key on [Fk_Order] in table 'JobCardPartsMaster'
ALTER TABLE [dbo].[JobCardPartsMaster]
ADD CONSTRAINT [FK_JobCardPartsMaster_gen_Order1]
    FOREIGN KEY ([Fk_Order])
    REFERENCES [dbo].[gen_Order]
        ([Pk_Order])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JobCardPartsMaster_gen_Order1'
CREATE INDEX [IX_FK_JobCardPartsMaster_gen_Order1]
ON [dbo].[JobCardPartsMaster]
    ([Fk_Order]);
GO

-- Creating foreign key on [Fk_Order] in table 'JW_JobCardMaster'
ALTER TABLE [dbo].[JW_JobCardMaster]
ADD CONSTRAINT [FK_JW_JobCardMaster_gen_Order]
    FOREIGN KEY ([Fk_Order])
    REFERENCES [dbo].[gen_Order]
        ([Pk_Order])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JW_JobCardMaster_gen_Order'
CREATE INDEX [IX_FK_JW_JobCardMaster_gen_Order]
ON [dbo].[JW_JobCardMaster]
    ([Fk_Order]);
GO

-- Creating foreign key on [Fk_CustomerOrder] in table 'Inv_MaterialIndentDetails'
ALTER TABLE [dbo].[Inv_MaterialIndentDetails]
ADD CONSTRAINT [FK_MaterialIndentDetails_gen_Order]
    FOREIGN KEY ([Fk_CustomerOrder])
    REFERENCES [dbo].[gen_Order]
        ([Pk_Order])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MaterialIndentDetails_gen_Order'
CREATE INDEX [IX_FK_MaterialIndentDetails_gen_Order]
ON [dbo].[Inv_MaterialIndentDetails]
    ([Fk_CustomerOrder]);
GO

-- Creating foreign key on [Fk_OrderNo] in table 'MaterialIssue'
ALTER TABLE [dbo].[MaterialIssue]
ADD CONSTRAINT [FK_MaterialIssue_gen_Order]
    FOREIGN KEY ([Fk_OrderNo])
    REFERENCES [dbo].[gen_Order]
        ([Pk_Order])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MaterialIssue_gen_Order'
CREATE INDEX [IX_FK_MaterialIssue_gen_Order]
ON [dbo].[MaterialIssue]
    ([Fk_OrderNo]);
GO

-- Creating foreign key on [Fk_Mill] in table 'MaterialInwardD'
ALTER TABLE [dbo].[MaterialInwardD]
ADD CONSTRAINT [FK_MaterialInwardD_gen_Mill]
    FOREIGN KEY ([Fk_Mill])
    REFERENCES [dbo].[gen_Mill]
        ([Pk_Mill])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MaterialInwardD_gen_Mill'
CREATE INDEX [IX_FK_MaterialInwardD_gen_Mill]
ON [dbo].[MaterialInwardD]
    ([Fk_Mill]);
GO

-- Creating foreign key on [Fk_Material] in table 'MaterialInwardD'
ALTER TABLE [dbo].[MaterialInwardD]
ADD CONSTRAINT [FK_MaterialInwardD_Inv_Material]
    FOREIGN KEY ([Fk_Material])
    REFERENCES [dbo].[Inv_Material]
        ([Pk_Material])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MaterialInwardD_Inv_Material'
CREATE INDEX [IX_FK_MaterialInwardD_Inv_Material]
ON [dbo].[MaterialInwardD]
    ([Fk_Material]);
GO

-- Creating foreign key on [TaxVal] in table 'MaterialInwardD'
ALTER TABLE [dbo].[MaterialInwardD]
ADD CONSTRAINT [FK_MaterialInwardD_Tax]
    FOREIGN KEY ([TaxVal])
    REFERENCES [dbo].[Tax]
        ([PkTax])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MaterialInwardD_Tax'
CREATE INDEX [IX_FK_MaterialInwardD_Tax]
ON [dbo].[MaterialInwardD]
    ([TaxVal]);
GO

-- Creating foreign key on [Fk_BoxSpecsID] in table 'ItemPartProperty'
ALTER TABLE [dbo].[ItemPartProperty]
ADD CONSTRAINT [FK_ItemPartProperty_BoxSpecs]
    FOREIGN KEY ([Fk_BoxSpecsID])
    REFERENCES [dbo].[BoxSpecs]
        ([Pk_BoxSpecID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ItemPartProperty_BoxSpecs'
CREATE INDEX [IX_FK_ItemPartProperty_BoxSpecs]
ON [dbo].[ItemPartProperty]
    ([Fk_BoxSpecsID]);
GO

-- Creating foreign key on [Fk_PartID] in table 'BoxStock'
ALTER TABLE [dbo].[BoxStock]
ADD CONSTRAINT [FK_BoxStock_ItemPartProperty]
    FOREIGN KEY ([Fk_PartID])
    REFERENCES [dbo].[ItemPartProperty]
        ([Pk_PartPropertyID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_BoxStock_ItemPartProperty'
CREATE INDEX [IX_FK_BoxStock_ItemPartProperty]
ON [dbo].[BoxStock]
    ([Fk_PartID]);
GO

-- Creating foreign key on [Fk_PartID] in table 'gen_DeliverySchedule'
ALTER TABLE [dbo].[gen_DeliverySchedule]
ADD CONSTRAINT [FK_gen_DeliverySchedule_ItemPartProperty]
    FOREIGN KEY ([Fk_PartID])
    REFERENCES [dbo].[ItemPartProperty]
        ([Pk_PartPropertyID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_gen_DeliverySchedule_ItemPartProperty'
CREATE INDEX [IX_FK_gen_DeliverySchedule_ItemPartProperty]
ON [dbo].[gen_DeliverySchedule]
    ([Fk_PartID]);
GO

-- Creating foreign key on [Fk_PartId] in table 'Items_Layers'
ALTER TABLE [dbo].[Items_Layers]
ADD CONSTRAINT [FK_Items_Layers]
    FOREIGN KEY ([Fk_PartId])
    REFERENCES [dbo].[ItemPartProperty]
        ([Pk_PartPropertyID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Items_Layers'
CREATE INDEX [IX_FK_Items_Layers]
ON [dbo].[Items_Layers]
    ([Fk_PartId]);
GO

-- Creating foreign key on [Fk_BoxID] in table 'MatInwD'
ALTER TABLE [dbo].[MatInwD]
ADD CONSTRAINT [FK_MatInwD_BoxMaster]
    FOREIGN KEY ([Fk_BoxID])
    REFERENCES [dbo].[BoxMaster]
        ([Pk_BoxID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MatInwD_BoxMaster'
CREATE INDEX [IX_FK_MatInwD_BoxMaster]
ON [dbo].[MatInwD]
    ([Fk_BoxID]);
GO

-- Creating foreign key on [TaxVal] in table 'MatInwD'
ALTER TABLE [dbo].[MatInwD]
ADD CONSTRAINT [FK_MatInwD_Tax]
    FOREIGN KEY ([TaxVal])
    REFERENCES [dbo].[Tax]
        ([PkTax])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MatInwD_Tax'
CREATE INDEX [IX_FK_MatInwD_Tax]
ON [dbo].[MatInwD]
    ([TaxVal]);
GO

-- Creating foreign key on [Fk_Vendor] in table 'MatInwM'
ALTER TABLE [dbo].[MatInwM]
ADD CONSTRAINT [FK_MatInwM_gen_Vendor]
    FOREIGN KEY ([Fk_Vendor])
    REFERENCES [dbo].[gen_Vendor]
        ([Pk_Vendor])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MatInwM_gen_Vendor'
CREATE INDEX [IX_FK_MatInwM_gen_Vendor]
ON [dbo].[MatInwM]
    ([Fk_Vendor]);
GO

-- Creating foreign key on [Fk_InwdBy] in table 'MatInwM'
ALTER TABLE [dbo].[MatInwM]
ADD CONSTRAINT [FK_MatInwd_ideUser]
    FOREIGN KEY ([Fk_InwdBy])
    REFERENCES [dbo].[ideUser]
        ([Pk_Users])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MatInwd_ideUser'
CREATE INDEX [IX_FK_MatInwd_ideUser]
ON [dbo].[MatInwM]
    ([Fk_InwdBy]);
GO

-- Creating foreign key on [Fk_Inwd] in table 'MatInwD'
ALTER TABLE [dbo].[MatInwD]
ADD CONSTRAINT [FK_MatInwD_MatInwM]
    FOREIGN KEY ([Fk_Inwd])
    REFERENCES [dbo].[MatInwM]
        ([Pk_Inwd])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MatInwD_MatInwM'
CREATE INDEX [IX_FK_MatInwD_MatInwM]
ON [dbo].[MatInwD]
    ([Fk_Inwd]);
GO

-- Creating foreign key on [Fk_BoxID] in table 'Gen_OrderChild'
ALTER TABLE [dbo].[Gen_OrderChild]
ADD CONSTRAINT [FK_Gen_OrderChild_BoxMaster]
    FOREIGN KEY ([Fk_BoxID])
    REFERENCES [dbo].[BoxMaster]
        ([Pk_BoxID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Gen_OrderChild_BoxMaster'
CREATE INDEX [IX_FK_Gen_OrderChild_BoxMaster]
ON [dbo].[Gen_OrderChild]
    ([Fk_BoxID]);
GO

-- Creating foreign key on [Fk_OrderID] in table 'Gen_OrderChild'
ALTER TABLE [dbo].[Gen_OrderChild]
ADD CONSTRAINT [FK_Gen_OrderChild_gen_Order1]
    FOREIGN KEY ([Fk_OrderID])
    REFERENCES [dbo].[gen_Order]
        ([Pk_Order])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Gen_OrderChild_gen_Order1'
CREATE INDEX [IX_FK_Gen_OrderChild_gen_Order1]
ON [dbo].[Gen_OrderChild]
    ([Fk_OrderID]);
GO

-- Creating foreign key on [Fk_Material] in table 'Gen_OrderChild'
ALTER TABLE [dbo].[Gen_OrderChild]
ADD CONSTRAINT [FK_Gen_OrderChild_Inv_Material]
    FOREIGN KEY ([Fk_Material])
    REFERENCES [dbo].[Inv_Material]
        ([Pk_Material])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Gen_OrderChild_Inv_Material'
CREATE INDEX [IX_FK_Gen_OrderChild_Inv_Material]
ON [dbo].[Gen_OrderChild]
    ([Fk_Material]);
GO

-- Creating foreign key on [Fk_PartID] in table 'Gen_OrderChild'
ALTER TABLE [dbo].[Gen_OrderChild]
ADD CONSTRAINT [FK_Gen_OrderChild_ItemPartProperty]
    FOREIGN KEY ([Fk_PartID])
    REFERENCES [dbo].[ItemPartProperty]
        ([Pk_PartPropertyID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Gen_OrderChild_ItemPartProperty'
CREATE INDEX [IX_FK_Gen_OrderChild_ItemPartProperty]
ON [dbo].[Gen_OrderChild]
    ([Fk_PartID]);
GO

-- Creating foreign key on [Fk_Vendor] in table 'PurchaseOrderM'
ALTER TABLE [dbo].[PurchaseOrderM]
ADD CONSTRAINT [FK_PurchaseOrderM_gen_Vendor]
    FOREIGN KEY ([Fk_Vendor])
    REFERENCES [dbo].[gen_Vendor]
        ([Pk_Vendor])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PurchaseOrderM_gen_Vendor'
CREATE INDEX [IX_FK_PurchaseOrderM_gen_Vendor]
ON [dbo].[PurchaseOrderM]
    ([Fk_Vendor]);
GO

-- Creating foreign key on [Fk_Indent] in table 'PurchaseOrderM'
ALTER TABLE [dbo].[PurchaseOrderM]
ADD CONSTRAINT [FK_PurchaseOrderM_Inv_MaterialIndentMaster]
    FOREIGN KEY ([Fk_Indent])
    REFERENCES [dbo].[Inv_MaterialIndentMaster]
        ([Pk_MaterialOrderMasterId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PurchaseOrderM_Inv_MaterialIndentMaster'
CREATE INDEX [IX_FK_PurchaseOrderM_Inv_MaterialIndentMaster]
ON [dbo].[PurchaseOrderM]
    ([Fk_Indent]);
GO

-- Creating foreign key on [Fk_PoNo] in table 'PaperCertificate'
ALTER TABLE [dbo].[PaperCertificate]
ADD CONSTRAINT [FK_PaperCertificate_PurchaseOrderM]
    FOREIGN KEY ([Fk_PoNo])
    REFERENCES [dbo].[PurchaseOrderM]
        ([Pk_PONo])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PaperCertificate_PurchaseOrderM'
CREATE INDEX [IX_FK_PaperCertificate_PurchaseOrderM]
ON [dbo].[PaperCertificate]
    ([Fk_PoNo]);
GO

-- Creating foreign key on [Fk_PoNo] in table 'POReturnMaster'
ALTER TABLE [dbo].[POReturnMaster]
ADD CONSTRAINT [FK_POReturnMaster_PurchaseOrderM]
    FOREIGN KEY ([Fk_PoNo])
    REFERENCES [dbo].[PurchaseOrderM]
        ([Pk_PONo])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_POReturnMaster_PurchaseOrderM'
CREATE INDEX [IX_FK_POReturnMaster_PurchaseOrderM]
ON [dbo].[POReturnMaster]
    ([Fk_PoNo]);
GO

-- Creating foreign key on [Fk_PONo] in table 'PurchaseOrderD'
ALTER TABLE [dbo].[PurchaseOrderD]
ADD CONSTRAINT [FK_PurchaseOrderD_PurchaseOrderM1]
    FOREIGN KEY ([Fk_PONo])
    REFERENCES [dbo].[PurchaseOrderM]
        ([Pk_PONo])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PurchaseOrderD_PurchaseOrderM1'
CREATE INDEX [IX_FK_PurchaseOrderD_PurchaseOrderM1]
ON [dbo].[PurchaseOrderD]
    ([Fk_PONo]);
GO

-- Creating foreign key on [Fk_Status] in table 'PurchaseOrderM'
ALTER TABLE [dbo].[PurchaseOrderM]
ADD CONSTRAINT [FK_PurchaseOrderM_wfStates]
    FOREIGN KEY ([Fk_Status])
    REFERENCES [dbo].[wfStates]
        ([Pk_State])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PurchaseOrderM_wfStates'
CREATE INDEX [IX_FK_PurchaseOrderM_wfStates]
ON [dbo].[PurchaseOrderM]
    ([Fk_Status]);
GO

-- Creating foreign key on [Fk_Branch] in table 'SampMaterialIssue'
ALTER TABLE [dbo].[SampMaterialIssue]
ADD CONSTRAINT [FK_SampMaterialIssue_Branch]
    FOREIGN KEY ([Fk_Branch])
    REFERENCES [dbo].[Branch]
        ([Pk_BranchID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SampMaterialIssue_Branch'
CREATE INDEX [IX_FK_SampMaterialIssue_Branch]
ON [dbo].[SampMaterialIssue]
    ([Fk_Branch]);
GO

-- Creating foreign key on [Fk_Mill] in table 'SampMaterialIssueDetails'
ALTER TABLE [dbo].[SampMaterialIssueDetails]
ADD CONSTRAINT [FK_SampMaterialIssueDetails_gen_Mill]
    FOREIGN KEY ([Fk_Mill])
    REFERENCES [dbo].[gen_Mill]
        ([Pk_Mill])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SampMaterialIssueDetails_gen_Mill'
CREATE INDEX [IX_FK_SampMaterialIssueDetails_gen_Mill]
ON [dbo].[SampMaterialIssueDetails]
    ([Fk_Mill]);
GO

-- Creating foreign key on [Fk_UserID] in table 'SampMaterialIssue'
ALTER TABLE [dbo].[SampMaterialIssue]
ADD CONSTRAINT [FK_SampMaterialIssue_ideUser]
    FOREIGN KEY ([Fk_UserID])
    REFERENCES [dbo].[ideUser]
        ([Pk_Users])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SampMaterialIssue_ideUser'
CREATE INDEX [IX_FK_SampMaterialIssue_ideUser]
ON [dbo].[SampMaterialIssue]
    ([Fk_UserID]);
GO

-- Creating foreign key on [Fk_Material] in table 'SampMaterialIssueDetails'
ALTER TABLE [dbo].[SampMaterialIssueDetails]
ADD CONSTRAINT [FK_SampMaterialIssueDetails_Inv_Material]
    FOREIGN KEY ([Fk_Material])
    REFERENCES [dbo].[Inv_Material]
        ([Pk_Material])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SampMaterialIssueDetails_Inv_Material'
CREATE INDEX [IX_FK_SampMaterialIssueDetails_Inv_Material]
ON [dbo].[SampMaterialIssueDetails]
    ([Fk_Material]);
GO

-- Creating foreign key on [Fk_JobCardID] in table 'SampMaterialIssue'
ALTER TABLE [dbo].[SampMaterialIssue]
ADD CONSTRAINT [FK_SampMaterialIssue_Sample_JobCardMaster]
    FOREIGN KEY ([Fk_JobCardID])
    REFERENCES [dbo].[Sample_JobCardMaster]
        ([Pk_JobCardID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SampMaterialIssue_Sample_JobCardMaster'
CREATE INDEX [IX_FK_SampMaterialIssue_Sample_JobCardMaster]
ON [dbo].[SampMaterialIssue]
    ([Fk_JobCardID]);
GO

-- Creating foreign key on [Fk_Tanent] in table 'SampMaterialIssue'
ALTER TABLE [dbo].[SampMaterialIssue]
ADD CONSTRAINT [FK_SampMaterialIssue_wgTenant]
    FOREIGN KEY ([Fk_Tanent])
    REFERENCES [dbo].[wgTenant]
        ([Pk_Tanent])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SampMaterialIssue_wgTenant'
CREATE INDEX [IX_FK_SampMaterialIssue_wgTenant]
ON [dbo].[SampMaterialIssue]
    ([Fk_Tanent]);
GO

-- Creating foreign key on [Fk_IssueID] in table 'SampMaterialIssueDetails'
ALTER TABLE [dbo].[SampMaterialIssueDetails]
ADD CONSTRAINT [FK_SampMaterialIssueDetails_SampMaterialIssue]
    FOREIGN KEY ([Fk_IssueID])
    REFERENCES [dbo].[SampMaterialIssue]
        ([Pk_MaterialIssueID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SampMaterialIssueDetails_SampMaterialIssue'
CREATE INDEX [IX_FK_SampMaterialIssueDetails_SampMaterialIssue]
ON [dbo].[SampMaterialIssueDetails]
    ([Fk_IssueID]);
GO

-- Creating foreign key on [Fk_ProcessID] in table 'SampJobProcess'
ALTER TABLE [dbo].[SampJobProcess]
ADD CONSTRAINT [FK_JobProcess_ProcessMaster_Samp]
    FOREIGN KEY ([Fk_ProcessID])
    REFERENCES [dbo].[ProcessMaster]
        ([Pk_Process])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JobProcess_ProcessMaster_Samp'
CREATE INDEX [IX_FK_JobProcess_ProcessMaster_Samp]
ON [dbo].[SampJobProcess]
    ([Fk_ProcessID]);
GO

-- Creating foreign key on [Fk_ProcessID] in table 'SampJProcess'
ALTER TABLE [dbo].[SampJProcess]
ADD CONSTRAINT [FK_JProcess_ProcessMaster_Samp]
    FOREIGN KEY ([Fk_ProcessID])
    REFERENCES [dbo].[ProcessMaster]
        ([Pk_Process])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JProcess_ProcessMaster_Samp'
CREATE INDEX [IX_FK_JProcess_ProcessMaster_Samp]
ON [dbo].[SampJProcess]
    ([Fk_ProcessID]);
GO

-- Creating foreign key on [Fk_JobCardID] in table 'SampJobProcess'
ALTER TABLE [dbo].[SampJobProcess]
ADD CONSTRAINT [FK_JobProcess_SampJobCardMaster]
    FOREIGN KEY ([Fk_JobCardID])
    REFERENCES [dbo].[Sample_JobCardMaster]
        ([Pk_JobCardID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JobProcess_SampJobCardMaster'
CREATE INDEX [IX_FK_JobProcess_SampJobCardMaster]
ON [dbo].[SampJobProcess]
    ([Fk_JobCardID]);
GO

-- Creating foreign key on [Fk_JobCardID] in table 'SampJProcess'
ALTER TABLE [dbo].[SampJProcess]
ADD CONSTRAINT [FK_JProcess_Sample_JobCardMaster]
    FOREIGN KEY ([Fk_JobCardID])
    REFERENCES [dbo].[Sample_JobCardMaster]
        ([Pk_JobCardID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JProcess_Sample_JobCardMaster'
CREATE INDEX [IX_FK_JProcess_Sample_JobCardMaster]
ON [dbo].[SampJProcess]
    ([Fk_JobCardID]);
GO

-- Creating foreign key on [Fk_Invno] in table 'COA'
ALTER TABLE [dbo].[COA]
ADD CONSTRAINT [FK_COA_Inv_Billing]
    FOREIGN KEY ([Fk_Invno])
    REFERENCES [dbo].[Inv_Billing]
        ([Pk_Invoice])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_COA_Inv_Billing'
CREATE INDEX [IX_FK_COA_Inv_Billing]
ON [dbo].[COA]
    ([Fk_Invno]);
GO

-- Creating foreign key on [Fk_Customer] in table 'Inv_Billing'
ALTER TABLE [dbo].[Inv_Billing]
ADD CONSTRAINT [FK_Inv_Billing_gen_Customer]
    FOREIGN KEY ([Fk_Customer])
    REFERENCES [dbo].[gen_Customer]
        ([Pk_Customer])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Inv_Billing_gen_Customer'
CREATE INDEX [IX_FK_Inv_Billing_gen_Customer]
ON [dbo].[Inv_Billing]
    ([Fk_Customer]);
GO

-- Creating foreign key on [Fk_OrderNo] in table 'Inv_Billing'
ALTER TABLE [dbo].[Inv_Billing]
ADD CONSTRAINT [FK_Inv_Billing_gen_Order]
    FOREIGN KEY ([Fk_OrderNo])
    REFERENCES [dbo].[gen_Order]
        ([Pk_Order])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Inv_Billing_gen_Order'
CREATE INDEX [IX_FK_Inv_Billing_gen_Order]
ON [dbo].[Inv_Billing]
    ([Fk_OrderNo]);
GO

-- Creating foreign key on [Fk_InvoiceNo] in table 'Inv_PaymentDetails'
ALTER TABLE [dbo].[Inv_PaymentDetails]
ADD CONSTRAINT [FK_Inv_PaymentDetails_Inv_Billing]
    FOREIGN KEY ([Fk_InvoiceNo])
    REFERENCES [dbo].[Inv_Billing]
        ([Pk_Invoice])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Inv_PaymentDetails_Inv_Billing'
CREATE INDEX [IX_FK_Inv_PaymentDetails_Inv_Billing]
ON [dbo].[Inv_PaymentDetails]
    ([Fk_InvoiceNo]);
GO

-- Creating foreign key on [Fk_JobCardID] in table 'JobProcess'
ALTER TABLE [dbo].[JobProcess]
ADD CONSTRAINT [FK_JobProcess_JobCardMaster]
    FOREIGN KEY ([Fk_JobCardID])
    REFERENCES [dbo].[JobCardMaster]
        ([Pk_JobCardID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JobProcess_JobCardMaster'
CREATE INDEX [IX_FK_JobProcess_JobCardMaster]
ON [dbo].[JobProcess]
    ([Fk_JobCardID]);
GO

-- Creating foreign key on [Fk_ProcessID] in table 'JobProcess'
ALTER TABLE [dbo].[JobProcess]
ADD CONSTRAINT [FK_JobProcess_ProcessMaster]
    FOREIGN KEY ([Fk_ProcessID])
    REFERENCES [dbo].[ProcessMaster]
        ([Pk_Process])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JobProcess_ProcessMaster'
CREATE INDEX [IX_FK_JobProcess_ProcessMaster]
ON [dbo].[JobProcess]
    ([Fk_ProcessID]);
GO

-- Creating foreign key on [Fk_InwardBy] in table 'MaterialInwardM'
ALTER TABLE [dbo].[MaterialInwardM]
ADD CONSTRAINT [FK_MaterialInwardM_ideUser]
    FOREIGN KEY ([Fk_InwardBy])
    REFERENCES [dbo].[ideUser]
        ([Pk_Users])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MaterialInwardM_ideUser'
CREATE INDEX [IX_FK_MaterialInwardM_ideUser]
ON [dbo].[MaterialInwardM]
    ([Fk_InwardBy]);
GO

-- Creating foreign key on [Fk_Indent] in table 'MaterialInwardM'
ALTER TABLE [dbo].[MaterialInwardM]
ADD CONSTRAINT [FK_MaterialInwardM_Inv_MaterialIndentMaster]
    FOREIGN KEY ([Fk_Indent])
    REFERENCES [dbo].[Inv_MaterialIndentMaster]
        ([Pk_MaterialOrderMasterId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MaterialInwardM_Inv_MaterialIndentMaster'
CREATE INDEX [IX_FK_MaterialInwardM_Inv_MaterialIndentMaster]
ON [dbo].[MaterialInwardM]
    ([Fk_Indent]);
GO

-- Creating foreign key on [Fk_Inward] in table 'MaterialInwardD'
ALTER TABLE [dbo].[MaterialInwardD]
ADD CONSTRAINT [FK_MaterialInwardD_MaterialInwardM]
    FOREIGN KEY ([Fk_Inward])
    REFERENCES [dbo].[MaterialInwardM]
        ([Pk_Inward])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MaterialInwardD_MaterialInwardM'
CREATE INDEX [IX_FK_MaterialInwardD_MaterialInwardM]
ON [dbo].[MaterialInwardD]
    ([Fk_Inward]);
GO

-- Creating foreign key on [PONo] in table 'MaterialInwardM'
ALTER TABLE [dbo].[MaterialInwardM]
ADD CONSTRAINT [FK_MaterialInwardM_PurchaseOrderM]
    FOREIGN KEY ([PONo])
    REFERENCES [dbo].[PurchaseOrderM]
        ([Pk_PONo])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MaterialInwardM_PurchaseOrderM'
CREATE INDEX [IX_FK_MaterialInwardM_PurchaseOrderM]
ON [dbo].[MaterialInwardM]
    ([PONo]);
GO

-- Creating foreign key on [Fk_BoxID] in table 'Inv_BillingDetails'
ALTER TABLE [dbo].[Inv_BillingDetails]
ADD CONSTRAINT [FK_Inv_BillingDetails_BoxMaster]
    FOREIGN KEY ([Fk_BoxID])
    REFERENCES [dbo].[BoxMaster]
        ([Pk_BoxID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Inv_BillingDetails_BoxMaster'
CREATE INDEX [IX_FK_Inv_BillingDetails_BoxMaster]
ON [dbo].[Inv_BillingDetails]
    ([Fk_BoxID]);
GO

-- Creating foreign key on [Fk_Invoice] in table 'Inv_BillingDetails'
ALTER TABLE [dbo].[Inv_BillingDetails]
ADD CONSTRAINT [FK_Inv_BillingDetails_Inv_Billing]
    FOREIGN KEY ([Fk_Invoice])
    REFERENCES [dbo].[Inv_Billing]
        ([Pk_Invoice])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Inv_BillingDetails_Inv_Billing'
CREATE INDEX [IX_FK_Inv_BillingDetails_Inv_Billing]
ON [dbo].[Inv_BillingDetails]
    ([Fk_Invoice]);
GO

-- Creating foreign key on [Fk_Material] in table 'Inv_BillingDetails'
ALTER TABLE [dbo].[Inv_BillingDetails]
ADD CONSTRAINT [FK_Inv_BillingDetails_Inv_Material]
    FOREIGN KEY ([Fk_Material])
    REFERENCES [dbo].[Inv_Material]
        ([Pk_Material])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Inv_BillingDetails_Inv_Material'
CREATE INDEX [IX_FK_Inv_BillingDetails_Inv_Material]
ON [dbo].[Inv_BillingDetails]
    ([Fk_Material]);
GO

-- Creating foreign key on [Fk_PartID] in table 'Inv_BillingDetails'
ALTER TABLE [dbo].[Inv_BillingDetails]
ADD CONSTRAINT [FK_Inv_BillingDetails_ItemPartProperty]
    FOREIGN KEY ([Fk_PartID])
    REFERENCES [dbo].[ItemPartProperty]
        ([Pk_PartPropertyID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Inv_BillingDetails_ItemPartProperty'
CREATE INDEX [IX_FK_Inv_BillingDetails_ItemPartProperty]
ON [dbo].[Inv_BillingDetails]
    ([Fk_PartID]);
GO

-- Creating foreign key on [Fk_GRNID] in table 'GRN_Details'
ALTER TABLE [dbo].[GRN_Details]
ADD CONSTRAINT [FK_GRN_Details_GRN_Mast]
    FOREIGN KEY ([Fk_GRNID])
    REFERENCES [dbo].[GRN_Mast]
        ([Pk_GRNId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GRN_Details_GRN_Mast'
CREATE INDEX [IX_FK_GRN_Details_GRN_Mast]
ON [dbo].[GRN_Details]
    ([Fk_GRNID]);
GO

-- Creating foreign key on [Fk_Material] in table 'GRN_Details'
ALTER TABLE [dbo].[GRN_Details]
ADD CONSTRAINT [FK_GRN_Details_Inv_Material]
    FOREIGN KEY ([Fk_Material])
    REFERENCES [dbo].[Inv_Material]
        ([Pk_Material])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GRN_Details_Inv_Material'
CREATE INDEX [IX_FK_GRN_Details_Inv_Material]
ON [dbo].[GRN_Details]
    ([Fk_Material]);
GO

-- Creating foreign key on [Fk_Inward] in table 'GRN_Mast'
ALTER TABLE [dbo].[GRN_Mast]
ADD CONSTRAINT [FK_GRN_Mast_MaterialInwardM]
    FOREIGN KEY ([Fk_Inward])
    REFERENCES [dbo].[MaterialInwardM]
        ([Pk_Inward])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GRN_Mast_MaterialInwardM'
CREATE INDEX [IX_FK_GRN_Mast_MaterialInwardM]
ON [dbo].[GRN_Mast]
    ([Fk_Inward]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------