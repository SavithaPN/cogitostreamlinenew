﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//namespace CogitoStreamLineModel.DomainModel
//{
//    class ProcessM
//    {
//    }
//}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using System.Web;
using Newtonsoft.Json;


namespace CogitoStreamLineModel.DomainModel
{
    public class ProcessM : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private ProcessMaster oColor = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_Color = null;
            string sColorName = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<ProcessMaster> oColors = null;

            try { sPk_Color = p_Params.Single(p => p.ParameterName == "Pk_Process").ParameterValue; }
            catch { }
            try { sColorName = p_Params.Single(p => p.ParameterName == "ProcessName").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oColors = _oEntites.ProcessMaster;

            try
            {
                if (!string.IsNullOrEmpty(sPk_Color))
                {
                    oColors = oColors.Where(p => p.Pk_Process == decimal.Parse(sPk_Color));
                }
                if (!string.IsNullOrEmpty(sColorName))
                {
                    oColors = oColors.Where(p => p.ProcessName.IndexOf(sColorName, StringComparison.OrdinalIgnoreCase) >= 0);

                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oColors.Count();


            oColors = oColors.OrderBy(p => p.Pk_Process);

            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oColors = oColors.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oColors.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }


        public SearchResult SearchJobCard(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_Color = null;
            string sPk_JC = null;
            string sColorName = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_ProcessJC> oColors = null;

            try { sPk_Color = p_Params.Single(p => p.ParameterName == "Pk_Process").ParameterValue; }
            catch { }
            try { sPk_JC = p_Params.Single(p => p.ParameterName == "Pk_JobCardID").ParameterValue; }
            catch { }
            try { sColorName = p_Params.Single(p => p.ParameterName == "ProcessName").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oColors = _oEntites.Vw_ProcessJC;

            try
            {
                if (!string.IsNullOrEmpty(sPk_Color))
                {
                    oColors = oColors.Where(p => p.Pk_Process == decimal.Parse(sPk_Color));
                }
                if (!string.IsNullOrEmpty(sPk_JC))
                {
                    oColors = oColors.Where(p => p.Fk_JobCardID == decimal.Parse(sPk_JC));
                }
                if (!string.IsNullOrEmpty(sColorName))
                {
                    oColors = oColors.Where(p => p.ProcessName.IndexOf(sColorName, StringComparison.OrdinalIgnoreCase) >= 0);

                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oColors.Count();


            oColors = oColors.OrderBy(p => p.Pk_JProcessID);

            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oColors = oColors.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oColors.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }



        public SearchResult SearchColorName(List<SearchParameter> p_Params)
        {

            SearchResult oSearchResult = new SearchResult();
            IEnumerable<ProcessMaster> oColorName = null;
            oColorName = _oEntites.ProcessMaster;

            oSearchResult.RecordCount = oColorName.Count();
            oColorName.OrderBy(p => p.Pk_Process);

            List<EntityObject> oFilteredItem = oColorName.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItem;

            return oSearchResult;
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                ProcessMaster oNewColor = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(ProcessMaster), omValues) as ProcessMaster;

                _oEntites.AddToProcessMaster(oNewColor);

                _oEntites.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                ProcessMaster oColor = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(ProcessMaster), omValues) as ProcessMaster;
                ProcessMaster oColorFromShelf = _oEntites.ProcessMaster.Where(p => p.Pk_Process == oColor.Pk_Process).Single();
                object orefItems = oColorFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(ProcessMaster), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;

            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                ProcessMaster oColor = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(ProcessMaster), omValues) as ProcessMaster;
                ProcessMaster oColorFromShelf = _oEntites.ProcessMaster.Where(p => p.Pk_Process == oColor.Pk_Process).Single();
                object orefItems = oColorFromShelf;
                //_oEntites.DeleteObject(oEmployeesFromShelf);

                _oEntites.DeleteObject(oColorFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oColor;

            }
            set
            {
                oColor = value as ProcessMaster;
            }
        }

        public decimal ID
        {
            get
            {
                return oColor.Pk_Process;
            }
            set
            {
                oColor = _oEntites.ProcessMaster.Where(p => p.Pk_Process == value).Single();
            }
        }

        public object GetRaw()
        {
            return oColor;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}


