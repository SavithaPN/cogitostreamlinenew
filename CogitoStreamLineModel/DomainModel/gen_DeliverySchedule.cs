﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using WebGareCore;
using Newtonsoft.Json;
using System.Web;
using System.Globalization;

namespace CogitoStreamLineModel.DomainModel
{
    public class Cgen_DeliverySchedule : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private gen_DeliverySchedule oDelivery = null;

        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();


            string sPk_DeliverySechedule = null;
            string sBName = null;
            string sFromDate = null;
            string sToDate = null;
            string sOrder = null;
            string sCName = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<gen_DeliverySchedule> oGDel = null;

            try { sPk_DeliverySechedule = p_Params.Single(p => p.ParameterName == "Pk_DeliverySechedule").ParameterValue; }
            catch { }
            try { sBName = p_Params.Single(p => p.ParameterName == "BoxName").ParameterValue; }
            catch { }
            try { sCName = p_Params.Single(p => p.ParameterName == "CustName").ParameterValue; }
            catch { }
            try { sOrder = p_Params.Single(p => p.ParameterName == "Fk_Order").ParameterValue; }
            catch { }
            try { sFromDate = p_Params.Single(p => p.ParameterName == "FromDate").ParameterValue; }
            catch { }
            try { sToDate = p_Params.Single(p => p.ParameterName == "ToDate").ParameterValue; }
            catch { }

            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oGDel = _oEntites.gen_DeliverySchedule;

            try
            {
                if (!string.IsNullOrEmpty(sPk_DeliverySechedule))
                {
                    oGDel = oGDel.Where(p => p.Pk_DeliverySechedule == Convert.ToDecimal(sPk_DeliverySechedule));
                }
                if (!string.IsNullOrEmpty(sBName))
                {
                    oGDel = oGDel.Where(p => p.BoxMaster.Name.IndexOf(sBName, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sCName))
                {
                    oGDel = oGDel.Where(p => p.gen_Order.gen_Customer.CustomerName.IndexOf(sCName, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sOrder))
                {
                    oGDel = oGDel.Where(p => p.Fk_Order == Convert.ToDecimal(sOrder));
                }
                if (!string.IsNullOrEmpty(sFromDate))
                {

                    DateTime FrmDate = Convert.ToDateTime(sFromDate.ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                    oGDel = oGDel.Where(p => (p.DeliveryDate >= Convert.ToDateTime(FrmDate)));
                }
                if (!string.IsNullOrEmpty(sToDate))
                {
                    DateTime ToDate = Convert.ToDateTime(sToDate.ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                    oGDel = oGDel.Where(p => (p.DeliveryDate <= Convert.ToDateTime(ToDate)));
                }
              


            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oGDel.Count();
            oGDel = oGDel.OrderByDescending(p => p.Pk_DeliverySechedule);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oGDel = oGDel.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredOrders = oGDel.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredOrders;

            return oSearchResult;

        }
        public SearchResult SearchDet(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();


            string sPk_DeliverySechedule = null;
            string sBName = null;
            string sFromDate = null;
            string sToDate = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<gen_DeliverySchedule> oGDel = null;

            try { sPk_DeliverySechedule = p_Params.Single(p => p.ParameterName == "Pk_DeliverySechedule").ParameterValue; }
            catch { }
            try { sBName = p_Params.Single(p => p.ParameterName == "BoxName").ParameterValue; }
            catch { }
            try { sFromDate = p_Params.Single(p => p.ParameterName == "FromDate").ParameterValue; }
            catch { }
            try { sToDate = p_Params.Single(p => p.ParameterName == "ToDate").ParameterValue; }
            catch { }

            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oGDel = _oEntites.gen_DeliverySchedule;

            try
            {
                if (!string.IsNullOrEmpty(sPk_DeliverySechedule))
                {
                    oGDel = oGDel.Where(p => p.Pk_DeliverySechedule == Convert.ToDecimal(sPk_DeliverySechedule));
                }
                //if (!string.IsNullOrEmpty(sBName))
                //{
                //    oGDel = oGDel.Where(p => p.BoxMaster.Name.IndexOf(sBName, StringComparison.OrdinalIgnoreCase) >= 0);
                //}
               
                if (!string.IsNullOrEmpty(sFromDate))
                {
                    oGDel = oGDel.Where(p => (p.DeliveryDate >= Convert.ToDateTime(sFromDate)));
                }
                if (!string.IsNullOrEmpty(sToDate))
                {
                    oGDel = oGDel.Where(p => (p.DeliveryDate <= Convert.ToDateTime(sToDate)));
                }
            

            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oGDel.Count();
            oGDel = oGDel.OrderByDescending(p => p.Pk_DeliverySechedule);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oGDel = oGDel.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredOrders = oGDel.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredOrders;

            return oSearchResult;

        }


        public SearchResult SearchSch(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();


            string sPk_DeliverySechedule = null;
            string sBName = null;
            string sFromDate = null;
            string sToDate = null;
            string sSchNo = null;
            string sPONo = null;
            string sPkOrder = null;
            string sCustomerName = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_Schedules> oGDel = null;

            try { sPk_DeliverySechedule = p_Params.Single(p => p.ParameterName == "Pk_DeliverySechedule").ParameterValue; }
            catch { }
            try { sPkOrder = p_Params.Single(p => p.ParameterName == "Pk_Order").ParameterValue; }
            catch { }
            try { sBName = p_Params.Single(p => p.ParameterName == "BoxName").ParameterValue; }
            catch { }
            try { sPONo = p_Params.Single(p => p.ParameterName == "PONo").ParameterValue; }
            catch { }
            try { sCustomerName = p_Params.Single(p => p.ParameterName == "CustomerName").ParameterValue; }
            catch { }
            try { sSchNo = p_Params.Single(p => p.ParameterName == "SchNo").ParameterValue; }
            catch { }
            try { sFromDate = p_Params.Single(p => p.ParameterName == "FromDate").ParameterValue; }
            catch { }
            try { sToDate = p_Params.Single(p => p.ParameterName == "ToDate").ParameterValue; }
            catch { }

            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oGDel = _oEntites.Vw_Schedules;

            try
            {
                if (!string.IsNullOrEmpty(sPk_DeliverySechedule))
                {
                    oGDel = oGDel.Where(p => p.Pk_DeliverySechedule == Convert.ToDecimal(sPk_DeliverySechedule));
                }
                if (!string.IsNullOrEmpty(sSchNo))
                {
                    oGDel = oGDel.Where(p => p.Pk_DeliverySechedule == Convert.ToDecimal(sSchNo));
                }
                if (!string.IsNullOrEmpty(sPkOrder))
                {
                    oGDel = oGDel.Where(p => p.Pk_Order == Convert.ToDecimal(sPkOrder));
                }
                if (!string.IsNullOrEmpty(sBName))
                {
                    oGDel = oGDel.Where(p => p.Name.IndexOf(sBName, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sCustomerName))
                {
                    oGDel = oGDel.Where(p => p.CustomerName.IndexOf(sCustomerName, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sPONo))
                {
                    oGDel = oGDel.Where(p => p.Cust_PO.IndexOf(sPONo, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sFromDate))
                {
                    oGDel = oGDel.Where(p => (p.DeliveryDate >= Convert.ToDateTime(sFromDate)));
                }
                if (!string.IsNullOrEmpty(sToDate))
                {
                    oGDel = oGDel.Where(p => (p.DeliveryDate <= Convert.ToDateTime(sToDate)));
                }


            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oGDel.Count();
            oGDel = oGDel.OrderByDescending(p => p.Pk_DeliverySechedule);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oGDel = oGDel.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredOrders = oGDel.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredOrders;

            return oSearchResult;

        }

        public SearchResult SearchSchPendingQty(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();


            string sPk_DeliverySechedule = null;
            string sBName = null;
            string sFromDate = null;
            string sToDate = null;
            string sSchNo = null;
            string sPONo = null;
            string sPkOrder = null;
            string sCustomerName = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_Sch_Ord_Pending> oGDel = null;

            try { sPk_DeliverySechedule = p_Params.Single(p => p.ParameterName == "Pk_DeliverySechedule").ParameterValue; }
            catch { }
            try { sPkOrder = p_Params.Single(p => p.ParameterName == "OrderNo").ParameterValue; }
            catch { }
            try { sBName = p_Params.Single(p => p.ParameterName == "BoxName").ParameterValue; }
            catch { }
            try { sPONo = p_Params.Single(p => p.ParameterName == "PONo").ParameterValue; }
            catch { }
            try { sCustomerName = p_Params.Single(p => p.ParameterName == "CustomerName").ParameterValue; }
            catch { }
            try { sSchNo = p_Params.Single(p => p.ParameterName == "SchNo").ParameterValue; }
            catch { }
            try { sFromDate = p_Params.Single(p => p.ParameterName == "FromDate").ParameterValue; }
            catch { }
            try { sToDate = p_Params.Single(p => p.ParameterName == "ToDate").ParameterValue; }
            catch { }

            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oGDel = _oEntites.Vw_Sch_Ord_Pending;

            try
            {
                //if (!string.IsNullOrEmpty(sPk_DeliverySechedule))
                //{
                //    oGDel = oGDel.Where(p => p.Pk_DeliverySechedule == Convert.ToDecimal(sPk_DeliverySechedule));
                //}
                //if (!string.IsNullOrEmpty(sSchNo))
                //{
                //    oGDel = oGDel.Where(p => p.Pk_DeliverySechedule == Convert.ToDecimal(sSchNo));
                //}
                if (!string.IsNullOrEmpty(sPkOrder))
                {
                    oGDel = oGDel.Where(p => p.Pk_Order == Convert.ToDecimal(sPkOrder));
                }
                //if (!string.IsNullOrEmpty(sBName))
                //{
                //    oGDel = oGDel.Where(p => p.Name.IndexOf(sBName, StringComparison.OrdinalIgnoreCase) >= 0);
                //}
                //if (!string.IsNullOrEmpty(sCustomerName))
                //{
                //    oGDel = oGDel.Where(p => p.CustomerName.IndexOf(sCustomerName, StringComparison.OrdinalIgnoreCase) >= 0);
                //}
                //if (!string.IsNullOrEmpty(sPONo))
                //{
                //    oGDel = oGDel.Where(p => p.Cust_PO.IndexOf(sPONo, StringComparison.OrdinalIgnoreCase) >= 0);
                //}
                //if (!string.IsNullOrEmpty(sFromDate))
                //{
                //    oGDel = oGDel.Where(p => (p.DeliveryDate >= Convert.ToDateTime(sFromDate)));
                //}
                //if (!string.IsNullOrEmpty(sToDate))
                //{
                //    oGDel = oGDel.Where(p => (p.DeliveryDate <= Convert.ToDateTime(sToDate)));
                //}


            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oGDel.Count();
            oGDel = oGDel.OrderByDescending(p => p.Pk_Order);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {
                var Nnval = int.Parse(sPageSize);
                if (Nnval == 0)
                {

                    //var page = (int.Parse(sStartIndex) / 1) ; // set current page number, must be >= 1
                    //var skip = int.Parse(sPageSize) * (page - 1);

                    //oPOrd = oPOrd.Select(p => p)
                    //                    .Skip(skip)
                    //                    .Take(int.Parse(sPageSize));
                }
                else
                {
                    var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                    var skip = int.Parse(sPageSize) * (page - 1);

                    oGDel = oGDel.Select(p => p)
                                        .Skip(skip)
                                        .Take(int.Parse(sPageSize));
                }
            }

            List<EntityObject> oFilteredOrders = oGDel.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredOrders;

            return oSearchResult;

        }


        public SearchResult SearchSchIssue(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();


            string sPk_DeliverySechedule = null;
            string sBName = null;
            string sPk_JobID = null;
            string sFromDate = null;
            string sToDate = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_IssueScheduleList> oGDel = null;

            try { sPk_DeliverySechedule = p_Params.Single(p => p.ParameterName == "Pk_DeliverySechedule").ParameterValue; }
            catch { }
            try { sBName = p_Params.Single(p => p.ParameterName == "BoxName").ParameterValue; }
            catch { }
            try { sPk_JobID = p_Params.Single(p => p.ParameterName == "PK_JobCardID").ParameterValue; }
            catch { }
            try { sFromDate = p_Params.Single(p => p.ParameterName == "FromDate").ParameterValue; }
            catch { }
            try { sToDate = p_Params.Single(p => p.ParameterName == "ToDate").ParameterValue; }
            catch { }

            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oGDel = _oEntites.Vw_IssueScheduleList;

            try
            {
                if (!string.IsNullOrEmpty(sPk_DeliverySechedule))
                {
                    oGDel = oGDel.Where(p => p.Pk_DeliverySechedule == Convert.ToDecimal(sPk_DeliverySechedule));
                }
                if (!string.IsNullOrEmpty(sPk_JobID))
                {
                    oGDel = oGDel.Where(p => p.Pk_JobCardID == decimal.Parse(sPk_JobID));
                }

                if (!string.IsNullOrEmpty(sFromDate))
                {
                    oGDel = oGDel.Where(p => (p.DeliveryDate >= Convert.ToDateTime(sFromDate)));
                }
                if (!string.IsNullOrEmpty(sToDate))
                {
                    oGDel = oGDel.Where(p => (p.DeliveryDate <= Convert.ToDateTime(sToDate)));
                }


            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oGDel.Count();
            oGDel = oGDel.OrderByDescending(p => p.Pk_DeliverySechedule);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oGDel = oGDel.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredOrders = oGDel.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredOrders;

            return oSearchResult;

        }

        //


        public SearchResult SearchSchParts(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();


            string sPk_DeliverySechedule = null;
            string sBName = null;
            string sFromDate = null;
            string sToDate = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_PartsSchedules> oGDel = null;

            try { sPk_DeliverySechedule = p_Params.Single(p => p.ParameterName == "Pk_DeliverySechedule").ParameterValue; }
            catch { }
            try { sBName = p_Params.Single(p => p.ParameterName == "BoxName").ParameterValue; }
            catch { }
            try { sFromDate = p_Params.Single(p => p.ParameterName == "FromDate").ParameterValue; }
            catch { }
            try { sToDate = p_Params.Single(p => p.ParameterName == "ToDate").ParameterValue; }
            catch { }

            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oGDel = _oEntites.Vw_PartsSchedules;

            try
            {
                if (!string.IsNullOrEmpty(sPk_DeliverySechedule))
                {
                    oGDel = oGDel.Where(p => p.Pk_DeliverySechedule == Convert.ToDecimal(sPk_DeliverySechedule));
                }
                //if (!string.IsNullOrEmpty(sBName))
                //{
                //    oGDel = oGDel.Where(p => p.BoxMaster.Name.IndexOf(sBName, StringComparison.OrdinalIgnoreCase) >= 0);
                //}

                if (!string.IsNullOrEmpty(sFromDate))
                {
                    oGDel = oGDel.Where(p => (p.DeliveryDate >= Convert.ToDateTime(sFromDate)));
                }
                if (!string.IsNullOrEmpty(sToDate))
                {
                    oGDel = oGDel.Where(p => (p.DeliveryDate <= Convert.ToDateTime(sToDate)));
                }


            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oGDel.Count();
            oGDel = oGDel.OrderByDescending(p => p.Pk_DeliverySechedule);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oGDel = oGDel.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredOrders = oGDel.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredOrders;

            return oSearchResult;

        }

        public SearchResult SearchSchDet(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();


            string sPk_DeliverySechedule = null;
            string sBName = null;
            string sFromDate = null;
            string sPartID = null;
            string sToDate = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;

            IEnumerable<Vw_BoxScheduleDet> oGDel = null;

            try { sPk_DeliverySechedule = p_Params.Single(p => p.ParameterName == "Pk_DeliverySechedule").ParameterValue; }
            catch { }
            try { sPartID = p_Params.Single(p => p.ParameterName == "PartID").ParameterValue; }
            catch { }
            
            try { sBName = p_Params.Single(p => p.ParameterName == "BoxName").ParameterValue; }
            catch { }
            try { sFromDate = p_Params.Single(p => p.ParameterName == "FromDate").ParameterValue; }
            catch { }
            try { sToDate = p_Params.Single(p => p.ParameterName == "ToDate").ParameterValue; }
            catch { }

            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oGDel = _oEntites.Vw_BoxScheduleDet;

            try
            {
                if (!string.IsNullOrEmpty(sPk_DeliverySechedule))
                {
                    oGDel = oGDel.Where(p => p.Pk_DeliverySechedule == Convert.ToDecimal(sPk_DeliverySechedule));
                }
                if (!string.IsNullOrEmpty(sPartID))
                {
                    oGDel = oGDel.Where(p => p.Pk_PartPropertyID == Convert.ToDecimal(sPartID));
                }

                //if (!string.IsNullOrEmpty(sFromDate))
                //{
                //    oGDel = oGDel.Where(p => (p.DeliveryDate >= Convert.ToDateTime(sFromDate)));
                //}
                //if (!string.IsNullOrEmpty(sToDate))
                //{
                //    oGDel = oGDel.Where(p => (p.DeliveryDate <= Convert.ToDateTime(sToDate)));
                //}


            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oGDel.Count();
            oGDel = oGDel.OrderByDescending(p => p.Pk_DeliverySechedule);

            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {
                var Nnval = int.Parse(sPageSize);
                if (Nnval == 0)
                {

                    //var page = (int.Parse(sStartIndex) / 1) ; // set current page number, must be >= 1
                    //var skip = int.Parse(sPageSize) * (page - 1);

                    //oPOrd = oPOrd.Select(p => p)
                    //                    .Skip(skip)
                    //                    .Take(int.Parse(sPageSize));
                }
                else
                {
                    var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                    var skip = int.Parse(sPageSize) * (page - 1);

                    oGDel = oGDel.Select(p => p)
                                        .Skip(skip)
                                        .Take(int.Parse(sPageSize));
                }
            }
            List<EntityObject> oFilteredOrders = oGDel.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredOrders;

            return oSearchResult;

        }



        public SearchResult SearchStk(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();


            string sPk_DeliverySechedule = null;
            string sBName = null;
            string sFromDate = null;
            string sToDate = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_WIP_Stock> oGDel = null;

            try { sPk_DeliverySechedule = p_Params.Single(p => p.ParameterName == "Pk_StkID").ParameterValue; }
            catch { }
            //try { sBName = p_Params.Single(p => p.ParameterName == "BoxName").ParameterValue; }
            //catch { }
            //try { sFromDate = p_Params.Single(p => p.ParameterName == "FromDate").ParameterValue; }
            //catch { }
            //try { sToDate = p_Params.Single(p => p.ParameterName == "ToDate").ParameterValue; }
            //catch { }

            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oGDel = _oEntites.Vw_WIP_Stock;

            try
            {
                if (!string.IsNullOrEmpty(sPk_DeliverySechedule))
                {
                    oGDel = oGDel.Where(p => p.Pk_StkID == Convert.ToDecimal(sPk_DeliverySechedule));
                }
                //if (!string.IsNullOrEmpty(sBName))
                //{
                //    oGDel = oGDel.Where(p => p.BoxMaster.Name.IndexOf(sBName, StringComparison.OrdinalIgnoreCase) >= 0);
                //}

                //if (!string.IsNullOrEmpty(sFromDate))
                //{
                //    oGDel = oGDel.Where(p => (p.DeliveryDate >= Convert.ToDateTime(sFromDate)));
                //}
                //if (!string.IsNullOrEmpty(sToDate))
                //{
                //    oGDel = oGDel.Where(p => (p.DeliveryDate <= Convert.ToDateTime(sToDate)));
                //}



            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oGDel.Count();
            oGDel = oGDel.OrderByDescending(p => p.Pk_StkID);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oGDel = oGDel.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredOrders = oGDel.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredOrders;

            return oSearchResult;

        }
    


        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                gen_DeliverySchedule oNewVSch = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_DeliverySchedule), omValues) as gen_DeliverySchedule;

                if (omValues.ContainsKey("deliverySchedules"))
                {
                    string sDeliverySchedules = omValues["deliverySchedules"].ToString();
                    object[] DeliverySchedules = JsonConvert.DeserializeObject<object[]>(sDeliverySchedules);

                    foreach (object oSchedule in DeliverySchedules)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oSchedule.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());
                        gen_DeliverySchedule oDeliverySchedule = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_DeliverySchedule), oDataProperties) as gen_DeliverySchedule;


                        oDeliverySchedule.DeliveryDate = Convert.ToDateTime(oDataProperties["DeliveryDate"].ToString(),
    System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);


                        oDeliverySchedule.Fk_Order = Convert.ToDecimal(omValues["Fk_Order"]);
                        decimal? FkOrder=Convert.ToDecimal(omValues["Fk_Order"]);

                     //   Gen_OrderChild oGen_Child1 = _oEntites.Gen_OrderChild.Where(p => p.Fk_OrderID == FkOrder).Single();


                          decimal? FkPartID=Convert.ToDecimal(oDeliverySchedule.Fk_PartID);
                          var SchQty = Convert.ToInt32(oDeliverySchedule.Quantity);
                        oDeliverySchedule.DeliveredDate = null;

                        ////int RCount = _oEntites.Gen_OrderChild.Where(p => p.Fk_OrderID == FkOrder && p.Fk_PartID == FkPartID).Count();
                        ////if (RCount > 0)
                        ////{


                        ////    Gen_OrderChild oGen_Child = _oEntites.Gen_OrderChild.Where(p => p.Fk_OrderID == FkOrder && p.Fk_PartID == FkPartID).SingleOrDefault();
                        ////    if (SchQty >= oGen_Child.OrdQty)
                        ////    {
                        ////        oGen_Child.Fk_Status = 4;
                        ////        _oEntites.SaveChanges();
                        ////    }
                        ////}shantha

                        
                        _oEntites.AddTogen_DeliverySchedule(oDeliverySchedule);
                    }
                }
                //_oEntites.AddTogen_DeliverySchedule(oNewVSch);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {

                gen_DeliverySchedule oPaperTypes = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_DeliverySchedule), omValues) as gen_DeliverySchedule;
                oPaperTypes.Pk_DeliverySechedule = Convert.ToDecimal(omValues["Pk_DeliverySchedule"]);
                gen_DeliverySchedule oPaperTypesFromShelf = _oEntites.gen_DeliverySchedule.Where(p => p.Pk_DeliverySechedule == oPaperTypes.Pk_DeliverySechedule).Single();
                object orefItems = oPaperTypesFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(gen_DeliverySchedule), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;
                }
                //_oEntites.SaveChanges();

                //oResult.Success = true;
          
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                gen_DeliverySchedule oOrder = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_DeliverySchedule), omValues) as gen_DeliverySchedule;
                gen_DeliverySchedule oOrderFromShelf = _oEntites.gen_DeliverySchedule.Where(p => p.Pk_DeliverySechedule == oOrder.Pk_DeliverySechedule).Single();
                _oEntites.DeleteObject(oOrderFromShelf);
                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oDelivery;
            }
            set
            {
                oDelivery = value as gen_DeliverySchedule;
            }
        }

        public decimal ID
        {
            get
            {
                return oDelivery.Pk_DeliverySechedule;
            }
            set
            {
                oDelivery = _oEntites.gen_DeliverySchedule.Where(p => p.Pk_DeliverySechedule == value).Single();
            }
        }

        public object GetRaw()
        {
            return oDelivery;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}

