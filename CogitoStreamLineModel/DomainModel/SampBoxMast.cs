﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//namespace CogitoStreamLineModel.DomainModel
//{
//    class SampBoxMast
//    {
//    }
//}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using System.Web;
using Newtonsoft.Json;


namespace CogitoStreamLineModel.DomainModel
{
    public class SampBoxMast : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private SampleBoxMaster oBoxMaster = null;
        public Decimal fkbox = 0;

        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sCustName = null;
            string sName = null;
            string sPk_BoxID = null;
            string sCustID = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<SampleBoxMaster> oBoxMasters = null;

            try { sCustName = p_Params.Single(p => p.ParameterName == "CustName").ParameterValue; }
            catch { }
            try { sName = p_Params.Single(p => p.ParameterName == "Name").ParameterValue; }
            catch { }
            try { sPk_BoxID = p_Params.Single(p => p.ParameterName == "Pk_BoxID").ParameterValue; }
            catch { }
            try { sCustID = p_Params.Single(p => p.ParameterName == "CustID").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oBoxMasters = _oEntites.SampleBoxMaster;

            try
            {
                if (!string.IsNullOrEmpty(sPk_BoxID))
                {
                    oBoxMasters = oBoxMasters.Where(p => p.Pk_BoxID == decimal.Parse(sPk_BoxID));
                }
                if (!string.IsNullOrEmpty(sCustID))
                {
                    oBoxMasters = oBoxMasters.Where(p => p.Customer == decimal.Parse(sCustID));
                }
                if (!string.IsNullOrEmpty(sCustName))
                {
                    oBoxMasters = oBoxMasters.Where(p => p.gen_Customer.CustomerName.IndexOf(sCustName, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sName))
                {
                    oBoxMasters = oBoxMasters.Where(p => p.Name.IndexOf(sName, StringComparison.OrdinalIgnoreCase) >= 0);

                }

            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oBoxMasters.Count();

            //oBoxMasters.OrderByDescending(p => p.Pk_BoxID);
            oBoxMasters = oBoxMasters.OrderByDescending(p => p.Pk_BoxID);

            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oBoxMasters = oBoxMasters.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oBoxMasters.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }


        public SearchResult SearchMachine(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_Machine = null;

            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<gen_Machine> oMachnines = null;

            try { sPk_Machine = p_Params.Single(p => p.ParameterName == "Pk_Machine").ParameterValue; }
            catch { }

            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oMachnines = _oEntites.gen_Machine;
            try
            {
                if (!string.IsNullOrEmpty(sPk_Machine))
                {
                    oMachnines = oMachnines.Where(p => p.Pk_Machine == decimal.Parse(sPk_Machine));
                }


            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oMachnines.Count();
            oMachnines.OrderBy(p => p.Pk_Machine);
            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oMachnines = oMachnines.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oMachnines.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;
            return oSearchResult;
        }

        //public SearchResult SearchJobBox(List<SearchParameter> p_Params)
        //{
        //    SearchResult oSearchResult = new SearchResult();

        //    //We make a call that Search can only be done for User based on 
        //    //First name, Last name and username
        //    string sPk_BoxID = null;
        //    string sName = null;

        //    string sStartIndex = null;
        //    string sPageSize = null;
        //    string sSorting = null;
        //    IEnumerable<Vw_JobBoxDetails> oJobBox = null;

        //    try { sPk_BoxID = p_Params.Single(p => p.ParameterName == "Pk_BoxID").ParameterValue; }
        //    catch { }
        //    try { sName = p_Params.Single(p => p.ParameterName == "Name").ParameterValue; }
        //    catch { }

        //    try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
        //    catch { }
        //    try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
        //    catch { }
        //    try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
        //    catch { }


        //    oJobBox = _oEntites.Vw_JobBoxDetails;

        //    try
        //    {
        //        if (!string.IsNullOrEmpty(sPk_BoxID))
        //        {
        //            oJobBox = oJobBox.Where(p => p.Fk_BoxID == decimal.Parse(sPk_BoxID));
        //        }
        //        if (!string.IsNullOrEmpty(sName))
        //        {
        //            oJobBox = oJobBox.Where(p => p.Name.IndexOf(sName, StringComparison.OrdinalIgnoreCase) >= 0);

        //        }

        //    }
        //    catch (System.NullReferenceException)
        //    {
        //        //possible that some items may not have the required fields set just ignore them
        //    }

        //    oSearchResult.RecordCount = oJobBox.Count();

        //    oJobBox.OrderBy(p => p.Fk_BoxID);


        //    if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
        //    {

        //        var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
        //        var skip = int.Parse(sPageSize) * (page - 1);

        //        oJobBox = oJobBox.Select(p => p)
        //                            .Skip(skip)
        //                            .Take(int.Parse(sPageSize));
        //    }

        //    List<EntityObject> oFilteredItems = oJobBox.Select(p => p).OfType<EntityObject>().ToList();
        //    oSearchResult.ListOfRecords = oFilteredItems;

        //    return oSearchResult;

        //}

        public SearchResult SearchP(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_BoxID = null;
            string sPartID = null;

            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_SampleBoxDet> oJobBox = null;

            try { sPk_BoxID = p_Params.Single(p => p.ParameterName == "Pk_BoxID").ParameterValue; }
            catch { }
            //try { sName = p_Params.Single(p => p.ParameterName == "Name").ParameterValue; }
            //catch { }
            try { sPartID = p_Params.Single(p => p.ParameterName == "PartID").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oJobBox = _oEntites.Vw_SampleBoxDet;

            try
            {
                if (!string.IsNullOrEmpty(sPk_BoxID))
                {
                    oJobBox = oJobBox.Where(p => p.Pk_BoxID == decimal.Parse(sPk_BoxID));
                }
                if (!string.IsNullOrEmpty(sPartID))
                {
                    oJobBox = oJobBox.Where(p => p.Pk_PartPropertyID == decimal.Parse(sPartID));

                }

            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oJobBox.Count();


            oJobBox.OrderBy(p => p.Pk_BoxID);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oJobBox = oJobBox.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oJobBox.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }


        public SearchResult SearchBoxSp(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_BoxID = null;
            string sName = null;
            string sBName = null;
            string sCustIDVal = null;
            string sPartName = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;

            IEnumerable<Vw_BoxSpec> oJobBox = null;

            try { sPk_BoxID = p_Params.Single(p => p.ParameterName == "Pk_BoxID").ParameterValue; }
            catch { }
            try { sName = p_Params.Single(p => p.ParameterName == "CName").ParameterValue; }
            catch { }
            try { sCustIDVal = p_Params.Single(p => p.ParameterName == "CustIDVal").ParameterValue; }
            catch { }
            try { sBName = p_Params.Single(p => p.ParameterName == "BName").ParameterValue; }
            catch { }
            try { sPartName = p_Params.Single(p => p.ParameterName == "PartName").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oJobBox = _oEntites.Vw_BoxSpec;

            try
            {
                if (!string.IsNullOrEmpty(sPk_BoxID))
                {
                    oJobBox = oJobBox.Where(p => p.Pk_BoxID == decimal.Parse(sPk_BoxID));
                }
                if (!string.IsNullOrEmpty(sName))
                {
                    oJobBox = oJobBox.Where(p => p.CustomerName.IndexOf(sName.Trim(), StringComparison.OrdinalIgnoreCase) >= 0);

                }
                if (!string.IsNullOrEmpty(sCustIDVal))
                {
                    oJobBox = oJobBox.Where(p => p.Pk_Customer == decimal.Parse(sCustIDVal));

                }
                if (!string.IsNullOrEmpty(sBName))
                {
                    oJobBox = oJobBox.Where(p => p.Name.IndexOf(sBName.Trim(), StringComparison.OrdinalIgnoreCase) >= 0);

                }
                if (!string.IsNullOrEmpty(sPartName))
                {
                    oJobBox = oJobBox.Where(p => p.PName.IndexOf(sPartName.Trim(), StringComparison.OrdinalIgnoreCase) >= 0);

                }

            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oJobBox.Count();


            oJobBox.OrderBy(p => p.Pk_BoxID);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oJobBox = oJobBox.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oJobBox.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }


        public SearchResult SearchPSummary(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_BoxID = null;
            //string sName = null;

            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_PaperSummary> oJobBox = null;

            try { sPk_BoxID = p_Params.Single(p => p.ParameterName == "Pk_BoxID").ParameterValue; }
            catch { }
            //try { sName = p_Params.Single(p => p.ParameterName == "Name").ParameterValue; }
            //catch { }

            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oJobBox = _oEntites.Vw_PaperSummary;

            try
            {
                if (!string.IsNullOrEmpty(sPk_BoxID))
                {
                    oJobBox = oJobBox.Where(p => p.Pk_BoxID == decimal.Parse(sPk_BoxID));
                }
                //if (!string.IsNullOrEmpty(sName))
                //{
                //    oJobBox = oJobBox.Where(p => p.Name.IndexOf(sName, StringComparison.OrdinalIgnoreCase) >= 0);

                //}

            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oJobBox.Count();


            oJobBox.OrderBy(p => p.Pk_BoxID);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oJobBox = oJobBox.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oJobBox.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }

        public SearchResult SearchAssignedStock(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            string sBox = null;
            string sPk_Material = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_AssStock> oPaperStock = null;

            //try { sPk_PaperStock = p_Params.Single(p => p.ParameterName == "Pk_PaperStock").ParameterValue; }
            //catch { }
            try { sPk_Material = p_Params.Single(p => p.ParameterName == "Pk_Material").ParameterValue; }
            catch { }
            try { sBox = p_Params.Single(p => p.ParameterName == "Fk_BoxID").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }

            oPaperStock = _oEntites.Vw_AssStock;

            try
            {

                if (!string.IsNullOrEmpty(sPk_Material))
                {
                    oPaperStock = oPaperStock.Where(p => p.Pk_Material == decimal.Parse(sPk_Material));
                }
                if (!string.IsNullOrEmpty(sBox))
                {
                    oPaperStock = oPaperStock.Where(p => p.Pk_BoxID == decimal.Parse(sBox));
                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oPaperStock.Count();
            oPaperStock = oPaperStock.OrderByDescending(p => p.Pk_Material);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oPaperStock = oPaperStock.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oPaperStock.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;
        }

        public SearchResult SearchPOD(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //string sPk_PaperStock = null;
            string sPk_Material = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_PoBom> oPaperStock = null;

            //try { sPk_PaperStock = p_Params.Single(p => p.ParameterName == "Pk_PaperStock").ParameterValue; }
            //catch { }
            try { sPk_Material = p_Params.Single(p => p.ParameterName == "Pk_Material").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }

            oPaperStock = _oEntites.Vw_PoBom;

            try
            {

                if (!string.IsNullOrEmpty(sPk_Material))
                {
                    oPaperStock = oPaperStock.Where(p => p.Pk_Material == decimal.Parse(sPk_Material));
                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oPaperStock.Count();
            oPaperStock = oPaperStock.OrderByDescending(p => p.Pk_Material);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oPaperStock = oPaperStock.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oPaperStock.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;
        }


        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }


        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {

                SampleBoxMaster oNewBox = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(SampleBoxMaster), omValues) as SampleBoxMaster;
                //FType oType = new FType();

                FluteType oType = _oEntites.FluteType.Where(p => p.Pk_FluteID == oNewBox.Fk_FluteType).Single();


                if (omValues.ContainsKey("BoxDetails"))
                {
                    string sBoxDetails = omValues["BoxDetails"].ToString();
                    object[] sOBoxDetails = JsonConvert.DeserializeObject<object[]>(sBoxDetails);

                    //foreach (object oDtetails in sOBoxDetails)
                    //{
                    //    Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oDtetails.ToString());
                    //    Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());
                    //    BoxChild oBoxDetail = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(BoxChild), oDataProperties) as BoxChild;
                    //    oNewBox.BoxChilds.Add(oBoxDetail);


                    //}
                }
                if (omValues.ContainsKey("AsPer"))
                {
                    var AsPerVal = omValues["AsPer"].ToString();

                    if (AsPerVal == "1")
                    {
                        oNewBox.AsPer = "Specs";
                    }
                    else if (AsPerVal == "2")
                    {
                        oNewBox.AsPer = "Sample";
                    }
                    else if (AsPerVal == "3")
                    {
                        oNewBox.AsPer = "Others";
                    }
                }
                _oEntites.AddToSampleBoxMaster(oNewBox);

                _oEntites.SaveChanges();

                oResult.Success = true;
                oResult.Message = oNewBox.Pk_BoxID.ToString() + "-" + oType.TKFactor;
                //fkbox = oNewBox.Pk_BoxID;
                //oResult.Message
            }

            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }
            return oResult;


        }

 
        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                SampleBoxMaster oPOrd = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(SampleBoxMaster), omValues) as SampleBoxMaster;
                SampleBoxMaster OPOrdFromShelf = _oEntites.SampleBoxMaster.Where(p => p.Pk_BoxID == oPOrd.Pk_BoxID).Single();
                object orefPO = OPOrdFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(SampleBoxMaster), omValues, ref orefPO);


                string sMaterials = omValues["BoxDetails"].ToString();
                List<object> Materials = JsonConvert.DeserializeObject<List<object>>(sMaterials);

                List<decimal> UpdatedPk = new List<decimal>();
                List<BoxChild> NewMaterialsList = new List<BoxChild>();
                foreach (object oItem in Materials)
                {
                    Dictionary<string, object> oDataProperties1 = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                    Dictionary<string, object> oDataProperties2 = JsonConvert.DeserializeObject<Dictionary<string, object>>(oDataProperties1["data"].ToString());

                    if (oDataProperties2["Pk_BoxCID"] == null || oDataProperties2["Pk_BoxCID"].ToString() == "")
                    {
                        BoxChild oNewPOrderD
                            = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(BoxChild), oDataProperties2) as BoxChild;

                        NewMaterialsList.Add(oNewPOrderD);
                    }
                    else
                    {
                        //Handel Update here
                        decimal dPkPOrdDet = Convert.ToDecimal(oDataProperties2["Pk_BoxCID"]);
                        decimal dFkMaterial = Convert.ToDecimal(oDataProperties2["Fk_Material"]);
                        //decimal dQuantity = Convert.ToDecimal(oDataProperties2["Quantity"]);

                        //     gtot = Convert.ToDecimal(gtot) + Convert.ToDecimal(oDataProperties2["Amount"]);


                        BoxChild oPOrderDetFromShelf = _oEntites.BoxChild.Where(p => p.Pk_BoxCID == dPkPOrdDet && p.Fk_Material == dFkMaterial).Single();

                        object oPOrderDet
                                            = _oEntites.BoxChild.Where(p => p.Pk_BoxCID == dPkPOrdDet).First();

                        _oEntites.DeleteObject(oPOrderDet);


                        BoxChild oIndentDetails = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(BoxChild), oDataProperties2) as BoxChild;

                        NewMaterialsList.Add(oIndentDetails);
                        UpdatedPk.Add(dPkPOrdDet);

                    }


                }
                // Handeling Deleted Records

                //List<BoxChild> oDeletedRecords = OPOrdFromShelf.BoxChilds.Where(p => !UpdatedPk.Contains(p.Pk_BoxCID)).ToList();



                ////Add new elements
                //foreach (BoxChild oNewMaterialDetail in NewMaterialsList)
                //{
                //    OPOrdFromShelf.BoxChilds.Add(oNewMaterialDetail);
                //}

                _oEntites.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                SampleBoxMaster oBox = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(SampleBoxMaster), omValues) as SampleBoxMaster;
                SampleBoxMaster oBoxFromShelf = _oEntites.SampleBoxMaster.Where(p => p.Pk_BoxID == oBox.Pk_BoxID).Single();
                object orefItems = oBoxFromShelf;
                //_oEntites.DeleteObject(oEmployeesFromShelf);

                _oEntites.DeleteObject(oBoxFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oBoxMaster;

            }
            set
            {
                oBoxMaster = value as SampleBoxMaster;
            }
        }

        public decimal ID
        {
            get
            {
                return oBoxMaster.Pk_BoxID;
            }
            set
            {
                oBoxMaster = _oEntites.SampleBoxMaster.Where(p => p.Pk_BoxID == value).Single();
            }
        }

        public object GetRaw()
        {
            return oBoxMaster;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}


