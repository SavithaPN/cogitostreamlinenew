﻿//Name          : FinishedGoods Class
//Description   : Contains the Country class definition, every page in the application will be instance of this class with following
//                 1. Communication 2. Data binding 3. Loading preferences
//Author        : CH.V.N.Ravi Kumar
//Date 	        : 08/09/2015
//Crh Number    : SL00060
//Modifications : 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using System.Web;
using Newtonsoft.Json;

namespace CogitoStreamLineModel.DomainModel
{
    public class FinishedGoods : IDomainObject
    {
        #region Variables
        readonly CogitoStreamLineEntities _oEntities = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private FinishedGoodsMaster oFinishedGoodsMaster = null;
        private bool disposed = false;
        #endregion Variables

        #region Properties
        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oFinishedGoodsMaster;
            }
            set
            {
                oFinishedGoodsMaster = value as FinishedGoodsMaster;
            }
        }
        public decimal ID
        {
            get
            {
                return oFinishedGoodsMaster.Pk_FinishedGoodsId;
            }
            set
            {
                oFinishedGoodsMaster = _oEntities.FinishedGoodsMasters.Where(p => p.Pk_FinishedGoodsId == value).Single();
            }
        }
        #endregion Properties

        #region Methods
        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            string sPk_FinishedGoodsId = null;
            string sCustomer = null;
            string sFinishedDate = null;
            string sFk_Order = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<FinishedGoodsMaster> oFinishedGoods = null;

            try { sPk_FinishedGoodsId = p_Params.Single(p => p.ParameterName == "Pk_FinishedGoodsId").ParameterValue; }
            catch { }
            try { sCustomer = p_Params.Single(p => p.ParameterName == "Customer").ParameterValue; }
            catch { }
            try
            { sFinishedDate = p_Params.Single(p => p.ParameterName == "FinishedDate").ParameterValue; }
            catch { }
            try { sFk_Order = p_Params.Single(p => p.ParameterName == "Fk_Order").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }

            oFinishedGoods = _oEntities.FinishedGoodsMasters;

            try
            {
                if (!string.IsNullOrEmpty(sPk_FinishedGoodsId))
                {
                    oFinishedGoods = oFinishedGoods.Where(p => p.Pk_FinishedGoodsId == decimal.Parse(sPk_FinishedGoodsId));
                }
                if (!string.IsNullOrEmpty(sFinishedDate))
                {
                    oFinishedGoods = oFinishedGoods.Where(p => p.FinishedDate == Convert.ToDateTime(sFinishedDate));
                }
                if (!string.IsNullOrEmpty(sCustomer))
                {
                    oFinishedGoods = oFinishedGoods.Where(p => p.gen_Customer.CustomerName.IndexOf(sCustomer, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sFk_Order))
                {
                    oFinishedGoods = oFinishedGoods.Where(p => p.Fk_Order == Convert.ToDecimal(sFk_Order));
                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oFinishedGoods.Count();
            oFinishedGoods = oFinishedGoods.OrderByDescending(p => p.Pk_FinishedGoodsId);

            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {
                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oFinishedGoods = oFinishedGoods.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredFinishedGoods = oFinishedGoods.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredFinishedGoods;

            return oSearchResult;
        }
        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }
        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                FinishedGoodsMaster oNewFinishedGoodsMaster = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(FinishedGoodsMaster), omValues) as FinishedGoodsMaster;

                FinishedGoodsShippingDetail oFinishedGoodsShippingDetail = new FinishedGoodsShippingDetail();
                oFinishedGoodsShippingDetail.FirstName = omValues["FirstName"].ToString();
                oFinishedGoodsShippingDetail.LastName = omValues["LastName"].ToString();
                oFinishedGoodsShippingDetail.Address = omValues["Address"].ToString();
                oFinishedGoodsShippingDetail.City = omValues["City"].ToString();
                oFinishedGoodsShippingDetail.State = omValues["State"].ToString();
                oFinishedGoodsShippingDetail.Pincode = Convert.ToDecimal(omValues["Pincode"].ToString());
                oFinishedGoodsShippingDetail.Country = omValues["Country"].ToString();
                oFinishedGoodsShippingDetail.Mobile = Convert.ToDecimal(omValues["Mobile"].ToString());
                oFinishedGoodsShippingDetail.Telephone = Convert.ToDecimal(omValues["Telephone"].ToString());
                oFinishedGoodsShippingDetail.Email = omValues["Email"].ToString();
                if (omValues.ContainsKey("Fax"))
                    oFinishedGoodsShippingDetail.Fax = omValues["Fax"].ToString();




                oNewFinishedGoodsMaster.FinishedGoodsShippingDetails.Add(oFinishedGoodsShippingDetail);
                decimal fkorder = Convert.ToDecimal(omValues["Fk_Order"].ToString());
                var strprd = omValues["Product"].ToString();
                int stckcnt = _oEntities.FinishedGoodsStocks.Where(s => s.Fk_Order == fkorder && s.Product == strprd).Count();

                if (stckcnt > 0)
                {
                    FinishedGoodsStock ofinishedgoodsstock = _oEntities.FinishedGoodsStocks.Where(s => s.Fk_Order == fkorder && s.Product == strprd).Single();
                    ofinishedgoodsstock.Stock = Convert.ToDecimal(ofinishedgoodsstock.Stock) + Convert.ToDecimal(omValues["Quantity"].ToString());
                }
                else
                {
                    FinishedGoodsStock ofinishedgoodsstock = new FinishedGoodsStock();
                    ofinishedgoodsstock.Fk_Order = fkorder;
                    ofinishedgoodsstock.Product = strprd;
                    ofinishedgoodsstock.Stock = Convert.ToDecimal(omValues["Quantity"].ToString());
                    _oEntities.AddToFinishedGoodsStocks(ofinishedgoodsstock);
                }


                _oEntities.AddToFinishedGoodsMasters(oNewFinishedGoodsMaster);
                _oEntities.SaveChanges();
                oResult.Success = true;
                oResult.Message = oNewFinishedGoodsMaster.Pk_FinishedGoodsId.ToString();
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }
            return oResult;
        }
        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                FinishedGoodsMaster oFinishedGoods = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(FinishedGoodsMaster), omValues) as FinishedGoodsMaster;
                FinishedGoodsMaster oFinishedGoodsFromShelf = _oEntities.FinishedGoodsMasters.Where(p => p.Pk_FinishedGoodsId == oFinishedGoods.Pk_FinishedGoodsId).Single();


                FinishedGoodsShippingDetail oFinishedGoodsShippingDetail = new FinishedGoodsShippingDetail();
                if (omValues.ContainsKey("shippingDetails"))
                {
                    string sShippingDetails = omValues["shippingDetails"].ToString();
                    object[] ShippingDetails = JsonConvert.DeserializeObject<object[]>(sShippingDetails);
                    foreach (var oItem in ShippingDetails)
                    {
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());

                        oFinishedGoodsShippingDetail.FirstName = oDataProperties["FirstName"].ToString();
                        oFinishedGoodsShippingDetail.LastName = oDataProperties["LastName"].ToString();
                        oFinishedGoodsShippingDetail.Address = oDataProperties["Address"].ToString();
                        oFinishedGoodsShippingDetail.City = oDataProperties["City"].ToString();
                        oFinishedGoodsShippingDetail.State = oDataProperties["State"].ToString();
                        oFinishedGoodsShippingDetail.Pincode = Convert.ToDecimal(oDataProperties["Pincode"].ToString());
                        oFinishedGoodsShippingDetail.Country = oDataProperties["Country"].ToString();
                        oFinishedGoodsShippingDetail.Mobile = Convert.ToDecimal(oDataProperties["Mobile"].ToString());
                        oFinishedGoodsShippingDetail.Telephone = Convert.ToDecimal(oDataProperties["Telephone"].ToString());
                        oFinishedGoodsShippingDetail.Email = oDataProperties["Email"].ToString();
                        if (oDataProperties.ContainsKey("Fax"))
                            oFinishedGoodsShippingDetail.Fax = oDataProperties["Fax"].ToString();
                    }

                }

                decimal fkorder = Convert.ToDecimal(omValues["Fk_Order"].ToString());
                var strprd = omValues["Product"].ToString();
                int stckcnt = _oEntities.FinishedGoodsStocks.Where(s => s.Fk_Order == fkorder && s.Product == strprd).Count();

                if (stckcnt > 0)
                {

                    FinishedGoodsStock ofinishedgoodsstock = _oEntities.FinishedGoodsStocks.Where(s => s.Fk_Order == fkorder && s.Product == strprd).Single();
                    ofinishedgoodsstock.Stock = Convert.ToDecimal(ofinishedgoodsstock.Stock) - Convert.ToDecimal(oFinishedGoodsFromShelf.Quantity);
                    ofinishedgoodsstock.Stock = Convert.ToDecimal(ofinishedgoodsstock.Stock) + Convert.ToDecimal(omValues["Quantity"].ToString());
                }
                else
                {
                    FinishedGoodsStock ofinishedgoodsstock = new FinishedGoodsStock();
                    ofinishedgoodsstock.Fk_Order = fkorder;
                    ofinishedgoodsstock.Product = strprd;
                    ofinishedgoodsstock.Stock = Convert.ToDecimal(omValues["Quantity"].ToString());
                    _oEntities.AddToFinishedGoodsStocks(ofinishedgoodsstock);
                }


                object orefFinishedGoods = oFinishedGoodsFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(FinishedGoodsMaster), omValues, ref orefFinishedGoods);
                _oEntities.SaveChanges();
                oResult.Success = true;
                oResult.Message = oFinishedGoods.Pk_FinishedGoodsId.ToString();
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }
            return oResult;
        }
        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                FinishedGoodsMaster oFinishedGoods = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(FinishedGoodsMaster), omValues) as FinishedGoodsMaster;
                FinishedGoodsMaster oFinishedGoodsFromShelf = _oEntities.FinishedGoodsMasters.Where(p => p.Pk_FinishedGoodsId == oFinishedGoods.Pk_FinishedGoodsId).Single();

                var strprd = omValues["Product"].ToString();
                decimal fkorder = Convert.ToDecimal(omValues["Fk_Order"].ToString());
                FinishedGoodsStock ofinishedgoodsstock = _oEntities.FinishedGoodsStocks.Where(s => s.Fk_Order == fkorder && s.Product == strprd).Single();
                ofinishedgoodsstock.Stock = Convert.ToDecimal(ofinishedgoodsstock.Stock) - Convert.ToDecimal(oFinishedGoodsFromShelf.Quantity);

                object orefFinishedGoods = oFinishedGoodsFromShelf;
                _oEntities.DeleteObject(oFinishedGoodsFromShelf);
                _oEntities.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }
            return oResult;
        }
        public object GetRaw()
        {
            return oFinishedGoodsMaster;
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntities.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion Methods
    }
}

