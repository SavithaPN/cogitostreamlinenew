﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using System.Web;
using Newtonsoft.Json;


namespace CogitoStreamLineModel.DomainModel
{
    public class ODID : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private OD_ID oColor = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_Color = null;
            string sLength = null;
            string sWidth = null;
            string sHeight = null;
    //FTypeVal
            string sPPly = null;
            string sFTVal = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<OD_ID> oColors = null;

            try { sPk_Color = p_Params.Single(p => p.ParameterName == "ID").ParameterValue; }
            catch { }
            try { sLength = p_Params.Single(p => p.ParameterName == "Length").ParameterValue; }
            catch { }
            try { sPPly = p_Params.Single(p => p.ParameterName == "PlyV").ParameterValue; }
            catch { }
            try { sWidth = p_Params.Single(p => p.ParameterName == "Width").ParameterValue; }
            catch { }
            try { sHeight = p_Params.Single(p => p.ParameterName == "Height").ParameterValue; }
            catch { }
            try { sFTVal = p_Params.Single(p => p.ParameterName == "FTypeVal").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oColors = _oEntites.OD_ID;

            try
            {
                if (!string.IsNullOrEmpty(sPk_Color))
                {
                    oColors = oColors.Where(p => p.Pk_ID == decimal.Parse(sPk_Color));
                }
                if (!string.IsNullOrEmpty(sLength))
                {
                    oColors = oColors.Where(p => p.Length == decimal.Parse(sLength));
                }
                if (!string.IsNullOrEmpty(sPPly))
                {
                    oColors = oColors.Where(p => p.PlyValue == decimal.Parse(sPPly));
                }
                if (!string.IsNullOrEmpty(sFTVal))
                {
                    oColors = oColors.Where(p => p.Fk_FluteID == decimal.Parse(sFTVal));
                }
                if (!string.IsNullOrEmpty(sWidth))
                {
                    oColors = oColors.Where(p => p.Width == decimal.Parse(sWidth));
                }
                if (!string.IsNullOrEmpty(sHeight))
                {
                    oColors = oColors.Where(p => p.Height == decimal.Parse(sHeight));
                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oColors.Count();


            oColors = oColors.OrderByDescending(p => p.Pk_ID);

            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oColors = oColors.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oColors.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }

        public SearchResult SearchValueName(List<SearchParameter> p_Params)
        {

            SearchResult oSearchResult = new SearchResult();
            string sPk_DepartmentId = null;
            string sName = null;
            //string sTakeUpFactor = null;
            //string sStartIndex = null;

            //string sPageSize = null;

            //string sSorting = null;
            IEnumerable<OD_ID> oDepts = null;

            try { sPk_DepartmentId = p_Params.Single(p => p.ParameterName == "FluteType").ParameterValue; }
            catch { }
            try { sName = p_Params.Single(p => p.ParameterName == "Ply").ParameterValue; }
            catch { }


            //try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            //catch { }
            //try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            //catch { }
            //try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            //catch { }


            oDepts = _oEntites.OD_ID;
            try
            {
                if (!string.IsNullOrEmpty(sPk_DepartmentId))
                {
                    oDepts = oDepts.Where(p => p.Fk_FluteID == decimal.Parse(sPk_DepartmentId));
                }
                if (!string.IsNullOrEmpty(sName))
                {
                    oDepts = oDepts.Where(p => p.PlyValue == decimal.Parse(sName));

                }

            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oDepts.Count();

            oDepts.OrderBy(p => p.Fk_FluteID);


            //if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            //{

            //    var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
            //    var skip = int.Parse(sPageSize) * (page - 1);

            //    oDepts = oDepts.Select(p => p)
            //                        .Skip(skip)
            //                        .Take(int.Parse(sPageSize));
            //}

            List<EntityObject> oFilteredItems = oDepts.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }


        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                OD_ID oNewColor = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(OD_ID), omValues) as OD_ID;

                _oEntites.AddToOD_ID(oNewColor);

                _oEntites.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                OD_ID oColor = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(OD_ID), omValues) as OD_ID;
                OD_ID oColorFromShelf = _oEntites.OD_ID.Where(p => p.Pk_ID == oColor.Pk_ID).Single();
                object orefItems = oColorFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(OD_ID), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;

            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                OD_ID oColor = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(OD_ID), omValues) as OD_ID;
                OD_ID oColorFromShelf = _oEntites.OD_ID.Where(p => p.Pk_ID == oColor.Pk_ID).Single();
                object orefItems = oColorFromShelf;
                //_oEntites.DeleteObject(oEmployeesFromShelf);

                _oEntites.DeleteObject(oColorFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oColor;

            }
            set
            {
                oColor = value as OD_ID;
            }
        }

        public decimal ID
        {
            get
            {
                return oColor.Pk_ID;
            }
            set
            {
                oColor = _oEntites.OD_ID.Where(p => p.Pk_ID == value).Single();
            }
        }

        public object GetRaw()
        {
            return oColor;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}


