﻿
//Name          : Paymt
//Description   : Contains the payment class definition, every page in the application will be instance of this class with following
//                 1. Communication 2. Data binding 3. Loading preferences
//Author        : Shantha
//Date 	        : 26/10/2015
//Crh Number    : SL0002
//Modifications : 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using System.Web;
using Newtonsoft.Json;


namespace CogitoStreamLineModel.DomainModel
{
    public class Paymt : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private Payment oPaymt = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_PaymentID = null;
            string sDescription = null;
            string sChequeNo = null;
            string sChequeDate = null;
            string sFk_Vendor = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Payment> oPaymts = null;

            try { sPk_PaymentID = p_Params.Single(p => p.ParameterName == "Pk_PaymentID").ParameterValue; }
            catch { }
            try { sDescription = p_Params.Single(p => p.ParameterName == "Description").ParameterValue; }
            catch { }
            try { sChequeNo = p_Params.Single(p => p.ParameterName == "ChequeNo").ParameterValue; }
            catch { }
            try { sChequeDate = p_Params.Single(p => p.ParameterName == "ChequeDate").ParameterValue; }
            catch { }
            try { sFk_Vendor = p_Params.Single(p => p.ParameterName == "Fk_Vendor").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oPaymts = _oEntites.Payment;

            try
            {
                if (!string.IsNullOrEmpty(sPk_PaymentID))
                {
                    oPaymts = oPaymts.Where(p => p.Pk_PaymentID == decimal.Parse(sPk_PaymentID));
                }
                if (!string.IsNullOrEmpty(sDescription))
                {
                    oPaymts = oPaymts.Where(p => p.Description.IndexOf(sDescription, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sChequeDate))
                {
                    oPaymts = oPaymts.Where(p => p.ChequeDate == Convert.ToDateTime(sChequeDate));
                }
                if (!string.IsNullOrEmpty(sChequeNo))
                {
                    oPaymts = oPaymts.Where(p => p.ChequeNo.IndexOf(sChequeNo, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sFk_Vendor))
                {
                    oPaymts = oPaymts.Where(p => p.Fk_Vendor != null && p.gen_Vendor.VendorName.IndexOf(sFk_Vendor, StringComparison.OrdinalIgnoreCase) >= 0);
                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oPaymts.Count();

            oPaymts.OrderBy(p => p.Pk_PaymentID);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oPaymts = oPaymts.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oPaymts.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }

        //public SearchResult SearchColorName(List<SearchParameter> p_Params)
        //{

        //    SearchResult oSearchResult = new SearchResult();
        //    IEnumerable<gen_Color> oColorName = null;
        //    oColorName = _oEntites.gen_Color;

        //    oSearchResult.RecordCount = oColorName.Count();
        //    oColorName.OrderBy(p => p.Pk_Color);

        //    List<EntityObject> oFilteredItem = oColorName.Select(p => p).OfType<EntityObject>().ToList();
        //    oSearchResult.ListOfRecords = oFilteredItem;

        //    return oSearchResult;
        //}

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Payment oNewPaymt = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Payment), omValues) as Payment;

                _oEntites.AddToPayment(oNewPaymt);

                _oEntites.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Payment oPaymt = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Payment), omValues) as Payment;
                Payment oPaymtFromShelf = _oEntites.Payment.Where(p => p.Pk_PaymentID == oPaymt.Pk_PaymentID).Single();
                object orefItems = oPaymtFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(Payment), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;

            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Payment oPaymt = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Payment), omValues) as Payment;
                Payment oPaymtFromShelf = _oEntites.Payment.Where(p => p.Pk_PaymentID == oPaymt.Pk_PaymentID).Single();
                object orefItems = oPaymtFromShelf;
                _oEntites.DeleteObject(oPaymtFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oPaymt;

            }
            set
            {
                oPaymt = value as Payment;
            }
        }

        public decimal ID
        {
            get
            {
                return oPaymt.Pk_PaymentID;
            }
            set
            {
                oPaymt = _oEntites.Payment.Where(p => p.Pk_PaymentID == value).Single();
            }
        }

        public object GetRaw()
        {
            return oPaymt;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}


