﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using System.Web;
using Newtonsoft.Json;


namespace CogitoStreamLineModel.DomainModel
{
    public class SampItemProperties : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private SampleItemPartProperty oItemPartProperty = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_PartPropertyID = null;
            //string sName = null;

            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<SampleItemPartProperty> oItemPartPropertys = null;

            try { sPk_PartPropertyID = p_Params.Single(p => p.ParameterName == "Pk_PartPropertyID").ParameterValue; }
            catch { }
            //try { sName = p_Params.Single(p => p.ParameterName == "Name").ParameterValue; }
            //catch { }

            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oItemPartPropertys = _oEntites.SampleItemPartProperty;

            try
            {
                if (!string.IsNullOrEmpty(sPk_PartPropertyID))
                {
                    oItemPartPropertys = oItemPartPropertys.Where(p => p.Pk_PartPropertyID == decimal.Parse(sPk_PartPropertyID));
                }
                //if (!string.IsNullOrEmpty(sName))
                //{
                //    oItemPartPropertys = oItemPartPropertys.Where(p => p.Name.IndexOf(sName, StringComparison.OrdinalIgnoreCase) >= 0);

                //}

            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oItemPartPropertys.Count();

            oItemPartPropertys.OrderBy(p => p.Pk_PartPropertyID);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oItemPartPropertys = oItemPartPropertys.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oItemPartPropertys.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }


        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            //try
            //{
            //    SampleItemPartProperty oBoxM = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(SampleItemPartProperty), omValues) as SampleItemPartProperty;

            //    //string sParts = omValues["parts"].ToString();
            //    //object[] Parts = JsonConvert.DeserializeObject<object[]>(sParts);

            //    //foreach (object oItem in Parts)
            //    //{
            //    //    Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
            //    //    SampleItemPartProperty oPart = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(SampleItemPartProperty), oProperties) as SampleItemPartProperty;

            //    //    string sLayers = oProperties["layers"].ToString();
            //    //    object[] Layers = JsonConvert.DeserializeObject<object[]>(sLayers);

            //    //    foreach (object oLayerItem in Layers)
            //    //    {
            //    //        Dictionary<string, object> oLayerProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oLayerItem.ToString());
            //    //        Items_Layers oLayer = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Items_Layers), oLayerProperties) as Items_Layers;

            //    //        oPart.Items_Layers.Add(oLayer);
            //    //    }

            //    //    oBoxM.SampleItemPartProperty.Add(oPart);

            //    //}

            //    _oEntites.AddToItemPartPropertys(oBoxM);
            //    _oEntites.SaveChanges();
            //    oResult.Message = oBoxM.Pk_PartPropertyID.ToString();
            //    oResult.Success = true;
            //ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                SampleItemPartProperty oNewBox = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(SampleItemPartProperty), omValues) as SampleItemPartProperty;

                _oEntites.AddToSampleItemPartProperty(oNewBox);

                _oEntites.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                SampleItemPartProperty oBox = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(SampleItemPartProperty), omValues) as SampleItemPartProperty;
                SampleItemPartProperty oBoxFromShelf = _oEntites.SampleItemPartProperty.Where(p => p.Pk_PartPropertyID == oBox.Pk_PartPropertyID).Single();
                object orefItems = oBoxFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(SampleItemPartProperty), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;

            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                SampleItemPartProperty oBox = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(SampleItemPartProperty), omValues) as SampleItemPartProperty;
                SampleItemPartProperty oBoxFromShelf = _oEntites.SampleItemPartProperty.Where(p => p.Pk_PartPropertyID == oBox.Pk_PartPropertyID).Single();
                object orefItems = oBoxFromShelf;
                //_oEntites.DeleteObject(oEmployeesFromShelf);

                _oEntites.DeleteObject(oBoxFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oItemPartProperty;

            }
            set
            {
                oItemPartProperty = value as SampleItemPartProperty;
            }
        }

        public decimal ID
        {
            get
            {
                return oItemPartProperty.Pk_PartPropertyID;
            }
            set
            {
                oItemPartProperty = _oEntites.SampleItemPartProperty.Where(p => p.Pk_PartPropertyID == value).Single();
            }
        }

        public object GetRaw()
        {
            return oItemPartProperty;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}


