﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Objects.DataClasses;
using WebGareCore;
using WebGareCore.CommonObjects;
using System.Data;
using CogitoStreamLineModel;

namespace CogitoStreamLineModel.DomainModel
{
    public class JWRMStock : IDomainObject
    {
        //readonly WebGareCoreEntities2  _oEntites = new WebGareCoreEntities2();
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();

        private Dictionary<string, object> omValues = null;
        private JobWorkRMStock oStock = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_Stock = null;
            string sMName = null;
            string sMaterialCategory = null;
            //string sStateId = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<JobWorkRMStock> oStocks = null;

            try { sPk_Stock = p_Params.Single(p => p.ParameterName == "Pk_StockID").ParameterValue; }
            catch { }
            try { sMName = p_Params.Single(p => p.ParameterName == "MaterialName").ParameterValue; }
            catch { }
            try { sMaterialCategory = p_Params.Single(p => p.ParameterName == "MaterialCategory").ParameterValue; }
            catch { }
            //try { sStateId = p_Params.Single(p => p.ParameterName == "StateId").ParameterValue; }
            //catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oStocks = _oEntites.JobWorkRMStock;
            try
            {
                if (!string.IsNullOrEmpty(sPk_Stock))
                {
                    oStocks = oStocks.Where(p => p.Pk_StockID == decimal.Parse(sPk_Stock));
                }
                if (!string.IsNullOrEmpty(sMName))
                {
                    //oStocks = oStocks.Where(p => p.CityName.IndexOf(sCityName, StringComparison.OrdinalIgnoreCase) >= 0);
                    oStocks = oStocks.Where(p => p.Inv_Material.Name.ToLower() == (sMName.ToLower().ToString().Trim()));

                }
                if (!string.IsNullOrEmpty(sMaterialCategory))
                {
                    oStocks = oStocks.Where(p => p.Inv_Material.Inv_MaterialCategory.Name == sMaterialCategory);
                }
                //if (!string.IsNullOrEmpty(sStateId))
                //{
                //    oStocks = oStocks.Where(p => p.Fk_StateID == decimal.Parse(sStateId));
                //}
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oStocks.Count();
            oStocks.OrderBy(p => p.Pk_StockID);
            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oStocks = oStocks.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oStocks.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;
            return oSearchResult;
        }


        public SearchResult SearchpStk(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_Material = null;
            string sMName = null;
            string sMjcName = null;
            string sGSM = null;
            string sMill = null;
            string sBF = null;
            string sRollNo = null;
            string sDeckel = null;
            string sColor = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_JWStock> oStocks = null;

            try { sPk_Material = p_Params.Single(p => p.ParameterName == "Pk_Material").ParameterValue; }
            catch { }
            try { sMName = p_Params.Single(p => p.ParameterName == "MaterialName").ParameterValue; }
            catch { }
            try { sMjcName = p_Params.Single(p => p.ParameterName == "MaterialJCName").ParameterValue; }
            catch { }
            try { sGSM = p_Params.Single(p => p.ParameterName == "GSM").ParameterValue; }
            catch { }
            try { sBF = p_Params.Single(p => p.ParameterName == "BF").ParameterValue; }
            catch { }
            try { sRollNo = p_Params.Single(p => p.ParameterName == "RollNo").ParameterValue; }
            catch { }
            try { sMill = p_Params.Single(p => p.ParameterName == "Mill").ParameterValue; }
            catch { }
            try { sDeckel = p_Params.Single(p => p.ParameterName == "Deckle").ParameterValue; }
            catch { }
            try { sColor = p_Params.Single(p => p.ParameterName == "Color").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oStocks = _oEntites.Vw_JWStock;
            try
            {
                if (!string.IsNullOrEmpty(sPk_Material))
                {
                    oStocks = oStocks.Where(p => p.Pk_Material == decimal.Parse(sPk_Material));
                }
                if (!string.IsNullOrEmpty(sMName))
                {
                    // oStocks = oStocks.Where(p => p.Name.ToLower() == (sMName.ToLower().ToString().Trim()));
                    oStocks = oStocks.Where(p => p.Name.IndexOf(sMName, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                //if (!string.IsNullOrEmpty(sMjcName))
                //{
                //    oStocks = oStocks.Where(p => p.Name.IndexOf(sMjcName, StringComparison.OrdinalIgnoreCase) >= 0);
                //}
                if (!string.IsNullOrEmpty(sGSM))
                {
                    oStocks = oStocks.Where(p => p.GSM == decimal.Parse(sGSM));
                }
                if (!string.IsNullOrEmpty(sBF))
                {
                    oStocks = oStocks.Where(p => p.BF == decimal.Parse(sBF));
                }
                if (!string.IsNullOrEmpty(sDeckel))
                {
                    oStocks = oStocks.Where(p => p.Deckle == decimal.Parse(sDeckel));
                }
                //if (!string.IsNullOrEmpty(sColor))
                //{
                //    oStocks = oStocks.Where(p => p.ColorName != null && p.ColorName.IndexOf(sColor, StringComparison.OrdinalIgnoreCase) >= 0);
                //}

                //if (!string.IsNullOrEmpty(sMill))
                //{
                //    oStocks = oStocks.Where(p => p.SName.IndexOf(sMill, StringComparison.OrdinalIgnoreCase) >= 0);
                //}
                if (!string.IsNullOrEmpty(sRollNo))
                {
                    oStocks = oStocks.Where(p => p.RollNo.IndexOf(sRollNo, StringComparison.OrdinalIgnoreCase) >= 0);
                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oStocks.Count();
            oStocks = oStocks.OrderBy(p => p.Pk_StockID);
            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oStocks = oStocks.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oStocks.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;
            return oSearchResult;
        }

        //public SearchResult SearchpLoadStk(List<SearchParameter> p_Params)
        //{
        //    SearchResult oSearchResult = new SearchResult();

        //    //We make a call that Search can only be done for User based on 
        //    //First name, Last name and username
        //    string sPk_Material = null;
        //    string sMName = null;
        //    string sMjcName = null;
        //    string sGSM = null;
        //    string sMill = null;
        //    string sBF = null;
        //    string sDeckel = null;
        //    string sFk_BoxID = null;
        //    string sPartID = null;
        //    string sColor = null;
        //    string sStartIndex = null;
        //    string sPageSize = null;
        //    string sSorting = null;
        //    IEnumerable<Vw_PartPaperLoad> oStocks = null;


        //    try { sFk_BoxID = p_Params.Single(p => p.ParameterName == "Fk_BoxID").ParameterValue; }
        //    catch { }
        //    try { sPartID = p_Params.Single(p => p.ParameterName == "PartID").ParameterValue; }
        //    catch { }
        //    try { sPk_Material = p_Params.Single(p => p.ParameterName == "Pk_Material").ParameterValue; }
        //    catch { }
        //    try { sMName = p_Params.Single(p => p.ParameterName == "MaterialName").ParameterValue; }
        //    catch { }
        //    try { sMjcName = p_Params.Single(p => p.ParameterName == "MaterialJCName").ParameterValue; }
        //    catch { }
        //    try { sGSM = p_Params.Single(p => p.ParameterName == "GSM").ParameterValue; }
        //    catch { }
        //    try { sBF = p_Params.Single(p => p.ParameterName == "BF").ParameterValue; }
        //    catch { }
        //    try { sMill = p_Params.Single(p => p.ParameterName == "Mill").ParameterValue; }
        //    catch { }
        //    try { sDeckel = p_Params.Single(p => p.ParameterName == "Deckle").ParameterValue; }
        //    catch { }
        //    try { sColor = p_Params.Single(p => p.ParameterName == "Color").ParameterValue; }
        //    catch { }
        //    try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
        //    catch { }
        //    try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
        //    catch { }
        //    try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
        //    catch { }


        //    oStocks = _oEntites.Vw_PartPaperLoad;
        //    try
        //    {
        //        if (!string.IsNullOrEmpty(sFk_BoxID))
        //        {
        //            oStocks = oStocks.Where(p => p.Pk_BoxID == decimal.Parse(sFk_BoxID));
        //        }
        //        if (!string.IsNullOrEmpty(sPartID))
        //        {
        //            oStocks = oStocks.Where(p => p.Pk_PartPropertyID == decimal.Parse(sPartID));
        //        }
        //        if (!string.IsNullOrEmpty(sPk_Material))
        //        {
        //            oStocks = oStocks.Where(p => p.Pk_Material == decimal.Parse(sPk_Material));
        //        }
        //        if (!string.IsNullOrEmpty(sMName))
        //        {
        //            oStocks = oStocks.Where(p => p.MaterialName.IndexOf(sMName, StringComparison.OrdinalIgnoreCase) >= 0);
        //        }
        //        //if (!string.IsNullOrEmpty(sMjcName))
        //        //{
        //        //    oStocks = oStocks.Where(p => p.Name.IndexOf(sMjcName, StringComparison.OrdinalIgnoreCase) >= 0);
        //        //}
        //        if (!string.IsNullOrEmpty(sGSM))
        //        {
        //            oStocks = oStocks.Where(p => p.GSM == decimal.Parse(sGSM));
        //        }
        //        if (!string.IsNullOrEmpty(sBF))
        //        {
        //            oStocks = oStocks.Where(p => p.BF == decimal.Parse(sBF));
        //        }
        //        //if (!string.IsNullOrEmpty(sDeckel))
        //        //{
        //        //    oStocks = oStocks.Where(p => p.Deckle == decimal.Parse(sDeckel));
        //        //}
        //        if (!string.IsNullOrEmpty(sColor))
        //        {
        //            oStocks = oStocks.Where(p => p.ColorName != null && p.ColorName.IndexOf(sColor, StringComparison.OrdinalIgnoreCase) >= 0);
        //        }

        //        //if (!string.IsNullOrEmpty(sMill))
        //        //{
        //        //    oStocks = oStocks.Where(p => p.SName.IndexOf(sMill, StringComparison.OrdinalIgnoreCase) >= 0);
        //        //}
        //    }
        //    catch (System.NullReferenceException)
        //    {
        //        //possible that some items may not have the required fields set just ignore them
        //    }

        //    oSearchResult.RecordCount = oStocks.Count();
        //    oStocks = oStocks.OrderBy(p => p.Pk_PaperStock);
        //    if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
        //    {

        //        var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
        //        var skip = int.Parse(sPageSize) * (page - 1);

        //        oStocks = oStocks.Select(p => p)
        //                            .Skip(skip)
        //                            .Take(int.Parse(sPageSize));
        //    }

        //    List<EntityObject> oFilteredItems = oStocks.Select(p => p).OfType<EntityObject>().ToList();
        //    oSearchResult.ListOfRecords = oFilteredItems;
        //    return oSearchResult;
        //}



        public SearchResult SearchpSummStk(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_Material = null;
            string sMName = null;
            string sGSM = null;
            string sBF = null;
            string sDeckel = null;
            string sColor = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_JWTotPaperStockList> oStocks = null;

            try { sPk_Material = p_Params.Single(p => p.ParameterName == "Pk_Material").ParameterValue; }
            catch { }
            try { sMName = p_Params.Single(p => p.ParameterName == "MaterialName").ParameterValue; }
            catch { }
            try { sGSM = p_Params.Single(p => p.ParameterName == "GSM").ParameterValue; }
            catch { }
            try { sBF = p_Params.Single(p => p.ParameterName == "BF").ParameterValue; }
            catch { }
            try { sDeckel = p_Params.Single(p => p.ParameterName == "Deckle").ParameterValue; }
            catch { }
            try { sColor = p_Params.Single(p => p.ParameterName == "Color").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oStocks = _oEntites.Vw_JWTotPaperStockList;
            try
            {
                if (!string.IsNullOrEmpty(sPk_Material))
                {
                    oStocks = oStocks.Where(p => p.Pk_Material == decimal.Parse(sPk_Material));
                }
                if (!string.IsNullOrEmpty(sMName))
                {
                    oStocks = oStocks.Where(p => p.Name.ToLower() == (sMName.ToLower().ToString().Trim()));
                }


                //if (!string.IsNullOrEmpty(sColor))
                //{
                //    oStocks = oStocks.Where(p => p..ToLower() == (sColor.ToLower().ToString().Trim()));
                //}
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oStocks.Count();
            oStocks = oStocks.OrderBy(p => p.Pk_Material);
            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {
                var Nnval = int.Parse(sPageSize);
                if (Nnval == 0)
                {

                    //var page = (int.Parse(sStartIndex) / 1) ; // set current page number, must be >= 1
                    //var skip = int.Parse(sPageSize) * (page - 1);

                    //oPOrd = oPOrd.Select(p => p)
                    //                    .Skip(skip)
                    //                    .Take(int.Parse(sPageSize));
                }
                else
                {
                    var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                    var skip = int.Parse(sPageSize) * (page - 1);

                    oStocks = oStocks.Select(p => p)
                                        .Skip(skip)
                                        .Take(int.Parse(sPageSize));
                }
            }

            List<EntityObject> oFilteredItems = oStocks.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;
            return oSearchResult;
        }



        //public SearchResult SearchBoxStk(List<SearchParameter> p_Params)
        //{
        //    SearchResult oSearchResult = new SearchResult();

        //    //We make a call that Search can only be done for User based on 
        //    //First name, Last name and username
        //    string sPk_Material = null;
        //    string sMName = null;
        //    string sCustName = null;


        //    string sStartIndex = null;
        //    string sPageSize = null;
        //    string sSorting = null;
        //    IEnumerable<Vw_BoxStock> oStocks = null;

        //    try { sPk_Material = p_Params.Single(p => p.ParameterName == "Pk_BoxID").ParameterValue; }
        //    catch { }
        //    try { sMName = p_Params.Single(p => p.ParameterName == "Name").ParameterValue; }
        //    catch { }
        //    try { sCustName = p_Params.Single(p => p.ParameterName == "CustName").ParameterValue; }
        //    catch { }
        //    try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
        //    catch { }
        //    try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
        //    catch { }
        //    try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
        //    catch { }


        //    oStocks = _oEntites.Vw_BoxStock;
        //    try
        //    {
        //        if (!string.IsNullOrEmpty(sPk_Material))
        //        {
        //            oStocks = oStocks.Where(p => p.Pk_BoxID == decimal.Parse(sPk_Material));
        //        }
        //        if (!string.IsNullOrEmpty(sMName))
        //        {
        //            oStocks = oStocks.Where(p => p.Name.ToLower() == (sMName.ToLower().ToString().Trim()));
        //        }
        //        if (!string.IsNullOrEmpty(sCustName))
        //        {
        //            oStocks = oStocks.Where(p => p.CustomerName != null && p.CustomerName.IndexOf(sCustName, StringComparison.OrdinalIgnoreCase) >= 0);
        //        }

        //    }
        //    catch (System.NullReferenceException)
        //    {
        //        //possible that some items may not have the required fields set just ignore them
        //    }

        //    oSearchResult.RecordCount = oStocks.Count();
        //    oStocks.OrderBy(p => p.Pk_BoxID);
        //    if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
        //    {

        //        var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
        //        var skip = int.Parse(sPageSize) * (page - 1);

        //        oStocks = oStocks.Select(p => p)
        //                            .Skip(skip)
        //                            .Take(int.Parse(sPageSize));
        //    }

        //    List<EntityObject> oFilteredItems = oStocks.Select(p => p).OfType<EntityObject>().ToList();
        //    oSearchResult.ListOfRecords = oFilteredItems;
        //    return oSearchResult;
        //}

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                //Stocks oNewCitys = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Stocks), omValues) as Stocks;

                //_oEntites.AddToStocks(oNewCitys);

                //_oEntites.SaveChanges();
                //oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                //JobWorkRMStock oStocks = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(JobWorkRMStock), omValues) as JobWorkRMStock;
                //Stocks oStocksFromShelf = _oEntites.Stocks.Where(p => p.Pk_StockID == oStocks.Pk_StockID).Single();
                //object orefItems = oStocksFromShelf;
                //WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(JobWorkRMStock), omValues, ref orefItems);
                //_oEntites.SaveChanges();
                //oResult.Success = true;

                Inv_Material oMaterial = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Inv_Material), omValues) as Inv_Material;
                Inv_Material oMaterialFromShelf = _oEntites.Inv_Material.Where(p => p.Pk_Material == oMaterial.Pk_Material).Single();
                object orefItems = oMaterialFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(Inv_Material), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                //Stocks oStocks = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Stocks), omValues) as Stocks;
                //Stocks oStocksFromShelf = _oEntites.Stocks.Where(p => p.Pk_StockID == oStocks.Pk_StockID).Single();
                //object orefItems = oStocksFromShelf;


                //IEnumerable<gen_ContactPerson> ogenCont = _oEntites.gen_ContactPerson.Where(p => p.Address_City == oStocks.Pk_StockID);
                //if (ogenCont.Count() > 0)
                //{
                //    oResult.Success = false;
                //    oResult.Message = "Cannot Delete as this City is Referenced else where";
                //}

                //else
                //{
                //    IEnumerable<gen_VendorContacts> ogenVendorCont = _oEntites.gen_VendorContacts.Where(p => p.Fk_ContactPerson == oStocks.Pk_StockID);
                //    if (ogenVendorCont.Count() > 0)
                //    {
                //        oResult.Success = false;
                //        oResult.Message = "Cannot Delete as this City is Referenced else where";
                //    }


                //    else
                //    {
                //        IEnumerable<gen_CustomerContacts> ogenCustomerCont = _oEntites.gen_CustomerContacts.Where(p => p.Fk_ContactPerson == oStocks.Pk_StockID);
                //        if (ogenCustomerCont.Count() > 0)
                //        {
                //            oResult.Success = false;
                //            oResult.Message = "Cannot Delete as this City is Referenced else where";
                //        }
                //    }


                //    _oEntites.DeleteObject(oStocksFromShelf);

                //    _oEntites.SaveChanges();
                //    oResult.Success = true;
                //}



            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oStock;

            }
            set
            {
                oStock = value as JobWorkRMStock;
            }
        }

        public decimal ID
        {
            get
            {
                return oStock.Pk_StockID;
            }
            set
            {
                //oStock = _oEntites.Stocks.Where(p => p.Pk_StockID == value).Single();
            }
        }

        public object GetRaw()
        {
            return oStock;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}




