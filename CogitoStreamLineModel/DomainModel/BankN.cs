﻿
//Name          : Bank Class
//Description   : Contains the Bank class definition, every page in the application will be instance of this class with following
//                 1. Communication 2. Data binding 3. Loading preferences
//Author        : Shantha
//Date 	        : 27/10/2015
//Crh Number    : SL0002
//Modifications : 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using System.Web;
using Newtonsoft.Json;


namespace CogitoStreamLineModel.DomainModel
{
    public class BankN : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private Bank oBank = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_BankID = null;
            string sBankName = null;
            string sAccName = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Bank> oBanks = null;

            try { sPk_BankID = p_Params.Single(p => p.ParameterName == "Pk_BankID").ParameterValue; }
            catch { }
            try { sBankName = p_Params.Single(p => p.ParameterName == "BankName").ParameterValue; }
            catch { }
            try { sAccName = p_Params.Single(p => p.ParameterName == "AccountName").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oBanks = _oEntites.Bank;

            try
            {
                if (!string.IsNullOrEmpty(sPk_BankID))
                {
                    oBanks = oBanks.Where(p => p.Pk_BankID == decimal.Parse(sPk_BankID));
                }
                if (!string.IsNullOrEmpty(sBankName))
                {
                    oBanks = oBanks.Where(p => p.BankName.IndexOf(sBankName, StringComparison.OrdinalIgnoreCase) >= 0);

                }
                if (!string.IsNullOrEmpty(sAccName))
                {
                    oBanks = oBanks.Where(p => p.AccountName.IndexOf(sAccName, StringComparison.OrdinalIgnoreCase) >= 0);

                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oBanks.Count();

            oBanks.OrderBy(p => p.Pk_BankID);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oBanks = oBanks.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oBanks.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }

        //public SearchResult SearchBankName(List<SearchParameter> p_Params)
        //{

        //    SearchResult oSearchResult = new SearchResult();
        //    IEnumerable<Bank> oBankTypeName = null;
        //    oBankTypeName = _oEntites.Bank;

        //    oSearchResult.RecordCount = oBankTypeName.Count();
        //    oBankTypeName.OrderBy(p => p.Pk_BankID);

        //    List<EntityObject> oFilteredItem = oBankTypeName.Select(p => p).OfType<EntityObject>().ToList();
        //    oSearchResult.ListOfRecords = oFilteredItem;

        //    return oSearchResult;
        //}

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Bank oNewBank = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Bank), omValues) as Bank;

                _oEntites.AddToBank(oNewBank);

                _oEntites.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Bank oBank = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Bank), omValues) as Bank;
                Bank oBankFromShelf = _oEntites.Bank.Where(p => p.Pk_BankID == oBank.Pk_BankID).Single();
                object orefItems = oBankFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(Bank), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;

            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Bank oBank = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Bank), omValues) as Bank;
                Bank oBankFromShelf = _oEntites.Bank.Where(p => p.Pk_BankID == oBank.Pk_BankID).Single();
                object orefItems = oBankFromShelf;
                //_oEntites.DeleteObject(oEmployeesFromShelf);

                _oEntites.DeleteObject(oBankFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oBank;

            }
            set
            {
                oBank = value as Bank;
            }
        }

        public decimal ID
        {
            get
            {
                return oBank.Pk_BankID;
            }
            set
            {
                oBank = _oEntites.Bank.Where(p => p.Pk_BankID == value).Single();
            }
        }

        public object GetRaw()
        {
            return oBank;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}


