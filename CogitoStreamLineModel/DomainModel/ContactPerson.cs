﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using System.Web;
using Newtonsoft.Json;

namespace CogitoStreamLineModel.DomainModel
{
   public  class ContactPerson : IDomainObject
    {
       readonly CogitoStreamLineEntities _oEntities = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private gen_ContactPerson ogen_ContactPerson = null;
        

        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for Contact Person based on 
            //Contact Person name
            string sName = null;

            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<gen_ContactPerson> oContactPersons = null;

            try { sName = p_Params.Single(p => p.ParameterName == "ContactPersonName").ParameterValue; }
            catch { }


            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oContactPersons = _oEntities.gen_ContactPerson;

            try
            {
                if (!string.IsNullOrEmpty(sName))
                {
                    oContactPersons = oContactPersons.Where(p => p.ContactPersonName.IndexOf(sName, StringComparison.OrdinalIgnoreCase) >= 0);
                }




            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oContactPersons.Count();
            oContactPersons.OrderBy(p => p.ContactPersonName);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oContactPersons = oContactPersons.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredContactPersons = oContactPersons.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredContactPersons;

            return oSearchResult;

        }

        public SearchResult SearchCustomerContact(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for Contact Person based on 
            //Contact Person name
            string sName = null;

            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<vw_CustomerDetails> oContactPersons = null;

            try { sName = p_Params.Single(p => p.ParameterName == "ContactPersonName").ParameterValue; }
            catch { }


            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oContactPersons = _oEntities.vw_CustomerDetails;

            try
            {
                if (!string.IsNullOrEmpty(sName))
                {
                    oContactPersons = oContactPersons.Where(p => p.ContactPersonName.IndexOf(sName, StringComparison.OrdinalIgnoreCase) >= 0);
                }




            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oContactPersons.Count();
            oContactPersons.OrderBy(p => p.ContactPersonName);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oContactPersons = oContactPersons.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredContactPersons = oContactPersons.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredContactPersons;

            return oSearchResult;

        }


        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                gen_ContactPerson oNewContactPerson = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_ContactPerson), omValues) as gen_ContactPerson;

                _oEntities.AddTogen_ContactPerson(oNewContactPerson);

                _oEntities.SaveChanges();
                oResult.Message = oNewContactPerson.Pk_ContactPersonId.ToString();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                gen_ContactPerson oContactPerson = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_ContactPerson), omValues) as gen_ContactPerson;
                gen_ContactPerson oContactPersonFromShelf = _oEntities.gen_ContactPerson.Where(p => p.Pk_ContactPersonId == oContactPerson.Pk_ContactPersonId).Single();
                object orefContactPerson = oContactPersonFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(gen_ContactPerson), omValues, ref orefContactPerson);
                _oEntities.SaveChanges();
                oResult.Message = oContactPerson.Pk_ContactPersonId.ToString();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                gen_ContactPerson oContactPerson = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_ContactPerson), omValues) as gen_ContactPerson;
                gen_ContactPerson oContactPersonFromShelf = _oEntities.gen_ContactPerson.Where(p => p.Pk_ContactPersonId == oContactPerson.Pk_ContactPersonId).Single();


                List<gen_CustomerContacts> oContactsLink = oContactPersonFromShelf.gen_CustomerContacts.ToList();

                foreach (gen_CustomerContacts oContact in oContactsLink)
                {
                    _oEntities.DeleteObject(oContact);
                }
                oContactPersonFromShelf.gen_CustomerContacts.Clear();

                List<gen_VendorContacts> oVendorContactsLink = oContactPersonFromShelf.gen_VendorContacts.ToList();

                foreach (gen_VendorContacts oContact in oVendorContactsLink)
                {
                    _oEntities.DeleteObject(oContact);
                }
                oContactPersonFromShelf.gen_VendorContacts.Clear();

                List<gen_MillContacts> oMillContactsLink = oContactPersonFromShelf.gen_MillContacts.ToList();
                foreach (gen_MillContacts oContact in oMillContactsLink)
                {
                    _oEntities.DeleteObject(oContact);
                }
                oContactPersonFromShelf.gen_MillContacts.Clear();

                _oEntities.DeleteObject(oContactPersonFromShelf);

                _oEntities.SaveChanges();
                oResult.Message = oContactPerson.Pk_ContactPersonId.ToString();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return ogen_ContactPerson;
            }
            set
            {
                ogen_ContactPerson = value as gen_ContactPerson;
            }
        }

        public decimal ID
        {
            get
            {
                return ogen_ContactPerson.Pk_ContactPersonId;
            }
            set
            {
                ogen_ContactPerson = _oEntities.gen_ContactPerson.Where(p => p.Pk_ContactPersonId == value).Single();
            }
        }

        public object GetRaw()
        {
            return ogen_ContactPerson;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntities.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
