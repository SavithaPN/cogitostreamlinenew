﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using WebGareCore;
using Newtonsoft.Json;
using System.Web;

namespace CogitoStreamLineModel.DomainModel
{
    public class JobCardParts : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private JobCardPartsMaster oJob = null;

        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for Order based on 
            //First name, Last name and OrderName
            string sJobCardNo = null;
            string sFromJDate = null;
            string sToJDate = null;
            string sCustomerOrder = null;
            string sInvNo = null;
            string sOnlyPending = null;
            string sFk_Order = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<JobCardPartsMaster> oJobCard = null;

            try { sJobCardNo = p_Params.Single(p => p.ParameterName == "Pk_JobCardPartsID").ParameterValue; }
            catch { }
            try { sFromJDate = p_Params.Single(p => p.ParameterName == "FromJDate").ParameterValue; }
            catch { }
            try { sToJDate = p_Params.Single(p => p.ParameterName == "ToJDate").ParameterValue; }
            catch { }
            try { sCustomerOrder = p_Params.Single(p => p.ParameterName == "CustomerOrder").ParameterValue; }
            catch { }
            try { sInvNo = p_Params.Single(p => p.ParameterName == "InvNo").ParameterValue; }
            catch { }
            try { sFk_Order = p_Params.Single(p => p.ParameterName == "Fk_Order").ParameterValue; }
            catch { }
            try { sOnlyPending = p_Params.Single(p => p.ParameterName == "OnlyPending").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oJobCard = _oEntites.JobCardPartsMaster;

            try
            {
                if (!string.IsNullOrEmpty(sJobCardNo))
                {
                    oJobCard = oJobCard.Where(p => p.Pk_JobCardPartsID == Convert.ToDecimal(sJobCardNo));
                }
                if (!string.IsNullOrEmpty(sCustomerOrder))
                {
                    oJobCard = oJobCard.Where(p => p.gen_Order.Pk_Order == Convert.ToDecimal(sCustomerOrder));
                }
                if (!string.IsNullOrEmpty(sFk_Order))
                {
                    oJobCard = oJobCard.Where(p => p.gen_Order.Pk_Order == Convert.ToDecimal(sFk_Order));
                }
                if (!string.IsNullOrEmpty(sInvNo))
                {
                    oJobCard = oJobCard.Where(p => p.Invno.Trim().ToUpper() == sInvNo.Trim().ToUpper());
                }
                if (!string.IsNullOrEmpty(sFromJDate))
                {
                    oJobCard = oJobCard.Where(p => (p.JDate >= Convert.ToDateTime(sFromJDate)));
                }
                if (!string.IsNullOrEmpty(sToJDate))
                {
                    oJobCard = oJobCard.Where(p => (p.JDate <= Convert.ToDateTime(sToJDate)));
                }
                if (!string.IsNullOrEmpty(sOnlyPending))
                {

                    oJobCard = oJobCard.Where(p => p.wfStates.State.IndexOf(sOnlyPending, StringComparison.OrdinalIgnoreCase) >= 0);

                }

            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oJobCard.Count();
            oJobCard = oJobCard.OrderByDescending(p => p.Pk_JobCardPartsID);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oJobCard = oJobCard.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredOrders = oJobCard.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredOrders;

            return oSearchResult;

        }

        public SearchResult SearchJCPart(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for Order based on 
            //First name, Last name and OrderName
            string sJobCardNo = null;
            string sFromJDate = null;
            string sName = null;
            string sCustomerOrder = null;
            string sInvNo = null;
            //string sOnlyPending = null;
            string sFk_Order = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_JCPartDet> oJobCard = null;

            try { sJobCardNo = p_Params.Single(p => p.ParameterName == "Pk_JobCardPartsID").ParameterValue; }
            catch { }
            try { sName = p_Params.Single(p => p.ParameterName == "Name").ParameterValue; }
            catch { }
           
            try { sFk_Order = p_Params.Single(p => p.ParameterName == "Fk_Order").ParameterValue; }
            catch { }
          
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oJobCard = _oEntites.Vw_JCPartDet;

            try
            {
               
                if (!string.IsNullOrEmpty(sFk_Order))
                {
                    oJobCard = oJobCard.Where(p => p.Fk_Order == Convert.ToDecimal(sFk_Order));
                }
                if (!string.IsNullOrEmpty(sName))
                {

                    oJobCard = oJobCard.Where(p => p.Name.IndexOf(sName, StringComparison.OrdinalIgnoreCase) >= 0);
        
                }
               

            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oJobCard.Count();
            oJobCard = oJobCard.OrderByDescending(p => p.Pk_JobCardPartsID);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oJobCard = oJobCard.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredOrders = oJobCard.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredOrders;

            return oSearchResult;

        }
    
        
        public SearchResult SearchPaperStock(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            string sFk_BoxID = null;
            string sFk_Enquiry = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_BoxPaperStock> oPaperStock = null;

            try { sFk_BoxID = p_Params.Single(p => p.ParameterName == "Fk_BoxID").ParameterValue; }
            catch { }
            try { sFk_Enquiry = p_Params.Single(p => p.ParameterName == "Fk_Enquiry").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }

            oPaperStock = _oEntites.Vw_BoxPaperStock;

            try
            {
                if (!string.IsNullOrEmpty(sFk_BoxID))
                {
                    oPaperStock = oPaperStock.Where(p => p.Fk_BoxID == decimal.Parse(sFk_BoxID));
                }
                //if (!string.IsNullOrEmpty(sFk_Enquiry))
                //{
                //    oPaperStock = oPaperStock.Where(p => p.Fk_Enquiry == decimal.Parse(sFk_Enquiry));
                //}
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oPaperStock.Count();
            oPaperStock = oPaperStock.OrderByDescending(p => p.Pk_Material);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oPaperStock = oPaperStock.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oPaperStock.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;
        }

        public SearchResult SearchJCPaperStock(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            string sPk_PaperStock = null;
            string sFk_Material = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<PaperStock> oPaperStock = null;

            try { sPk_PaperStock = p_Params.Single(p => p.ParameterName == "Pk_PaperStock").ParameterValue; }
            catch { }
            try { sFk_Material = p_Params.Single(p => p.ParameterName == "Fk_Material").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }

            oPaperStock = _oEntites.PaperStock;

            try
            {
                if (!string.IsNullOrEmpty(sPk_PaperStock))
                {
                    oPaperStock = oPaperStock.Where(p => p.Pk_PaperStock == decimal.Parse(sPk_PaperStock));
                }
                if (!string.IsNullOrEmpty(sFk_Material))
                {
                    oPaperStock = oPaperStock.Where(p => p.Fk_Material == decimal.Parse(sFk_Material));
                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oPaperStock.Count();
            oPaperStock = oPaperStock.OrderByDescending(p => p.Pk_PaperStock);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oPaperStock = oPaperStock.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oPaperStock.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;
        }

        public SearchResult SearchParts(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            //string sFk_Mill = null;
            //string sName = null;

            //string sBrand = null;
            //string sMaterialType = null;
            //decimal sLength = 0;
            //decimal sWidth = 0;
            //decimal sGSM = 0;
            //decimal sBF = 0;
            //string sColor = null;
            //string sCategory = null;
            //string sPaperType = null;

            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_MaterialParts> oMaterials = null;

            //try { sFk_Mill = p_Params.Single(p => p.ParameterName == "Mill").ParameterValue; }
            //catch { }

            //try { sName = p_Params.Single(p => p.ParameterName == "Name").ParameterValue; }
            //catch { }

            //try { sBrand = p_Params.Single(p => p.ParameterName == "Brand").ParameterValue; }
            //catch { }

            //try { sMaterialType = p_Params.Single(p => p.ParameterName == "MaterialType").ParameterValue; }
            //catch { }

            //try { sColor = p_Params.Single(p => p.ParameterName == "Fk_Color").ParameterValue; }
            //catch { }

            //try { sCategory = p_Params.Single(p => p.ParameterName == "Category").ParameterValue; }
            //catch { }

            //try { sPaperType = p_Params.Single(p => p.ParameterName == "PaperType").ParameterValue; }
            //catch { }

            //try { sLength = Convert.ToDecimal(p_Params.Single(p => p.ParameterName == "Length").ParameterValue); }
            //catch { }

            //try { sWidth = Convert.ToDecimal(p_Params.Single(p => p.ParameterName == "Width").ParameterValue); }
            //catch { }

            //try { sGSM = Convert.ToDecimal(p_Params.Single(p => p.ParameterName == "GSM").ParameterValue); }
            //catch { }

            //try { sBF = Convert.ToDecimal(p_Params.Single(p => p.ParameterName == "BF").ParameterValue); }
            //catch { }


            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oMaterials = _oEntites.Vw_MaterialParts;

            try
            {
                //if (!string.IsNullOrEmpty(sFk_Mill))
                //{
                //    oMaterials = oMaterials.Where(p => p.Fk_Mill != null && p.gen_Mill.MillName.IndexOf(sFk_Mill, StringComparison.OrdinalIgnoreCase) >= 0);
                //}
                //if (!string.IsNullOrEmpty(sName))
                //{
                //    oMaterials = oMaterials.Where(p => p.Name != null && p.Name.IndexOf(sName, StringComparison.OrdinalIgnoreCase) >= 0);
                //}
                //if (!string.IsNullOrEmpty(sBrand))
                //{
                //    oMaterials = oMaterials.Where(p => p.Brand != null && p.Brand.IndexOf(sBrand, StringComparison.OrdinalIgnoreCase) >= 0);
                //}
                //if (!string.IsNullOrEmpty(sMaterialType))
                //{
                //    oMaterials = oMaterials.Where(p => p.MaterialType != null && p.MaterialType.IndexOf(sMaterialType, StringComparison.OrdinalIgnoreCase) >= 0);
                //}
                //if (!string.IsNullOrEmpty(sColor))
                //{
                //    oMaterials = oMaterials.Where(p => p.Fk_Color != null && p.gen_Color.ColorName.IndexOf(sColor, StringComparison.OrdinalIgnoreCase) >= 0);
                //}
                //if (!string.IsNullOrEmpty(sCategory))
                //{
                //oMaterials = oMaterials.Where(p => p.Fk_MaterialCategory != null && p.Inv_MaterialCategory.Name.IndexOf(sCategory, StringComparison.OrdinalIgnoreCase) >= 0);
                //}
                //if (!string.IsNullOrEmpty(sPaperType))
                //{
                //    oMaterials = oMaterials.Where(p => p.Fk_PaperType != null && p.gen_PaperType.PaperTypeName.IndexOf(sPaperType, StringComparison.OrdinalIgnoreCase) >= 0);
                //}
                //if (sLength != 0)
                //{
                //    oMaterials = oMaterials.Where(p => p.Length != null && p.Length == sLength);
                //}
                //if (sWidth != 0)
                //{
                //    oMaterials = oMaterials.Where(p => p.Width != null && p.Width == sWidth);
                //}
                //if (sGSM != 0)
                //{
                //    oMaterials = oMaterials.Where(p => p.GSM != null && p.GSM == sGSM);
                //}
                //if (sBF != 0)
                //{
                //    oMaterials = oMaterials.Where(p => p.BF != null && p.BF == sBF);
                //}
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oMaterials.Count();

            oMaterials.OrderBy(p => p.Pk_Material);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oMaterials = oMaterials.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oMaterials.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            // decimal GTotal = 0;

            ModelManuplationResult oResult = new ModelManuplationResult();
            try
            {
                if (!omValues.ContainsKey("JDate"))
                    omValues.Add("JDate", DateTime.Now);
                omValues.Add("Fk_Status", 1);

                JobCardPartsMaster oNewJob = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(JobCardPartsMaster), omValues) as JobCardPartsMaster;

                if (omValues.ContainsKey("JCPartsDetails"))
                {
                    string sJCPartsDetails = omValues["JCPartsDetails"].ToString();
                    object[] JCPartsDetails = JsonConvert.DeserializeObject<object[]>(sJCPartsDetails);


                    foreach (object oDtetails in JCPartsDetails)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oDtetails.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());
                        JobCardPartsDetails oJCPartsDetails = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(JobCardPartsDetails), oDataProperties) as JobCardPartsDetails;
                        oJCPartsDetails.RM_Consumed = Convert.ToDecimal(oDataProperties["Quantity"]);            ///Assigned Stock
                        oNewJob.JobCardPartsDetails.Add(oJCPartsDetails);

                    }
                }

                _oEntites.AddToJobCardPartsMaster(oNewJob);

                Stock oStocks = _oEntites.Stocks.Where(p => p.Fk_Material == oNewJob.Fk_PartID).SingleOrDefault();
                if (oStocks == null)
                {
                    oStocks = new Stock();
                    oStocks.Fk_Material = oNewJob.Fk_PartID;
                    oStocks.Quantity = oNewJob.TotalQty;
                    _oEntites.AddToStocks(oStocks);

                }
                else
                {
                    oStocks.Quantity = Convert.ToDecimal(oStocks.Quantity + oNewJob.TotalQty);
                }

                _oEntites.SaveChanges();

                oResult.Success = true;
                oResult.Message = oNewJob.Pk_JobCardPartsID.ToString();
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                decimal dFk_PartID = decimal.Parse(omValues["Fk_PartID"].ToString());
                Stock oStock = _oEntites.Stocks.Where(p => p.Fk_Material == dFk_PartID).SingleOrDefault();
                if (oStock == null)
                {
                    oStock = new Stock();
                    oStock.Fk_Material = dFk_PartID;
                    oStock.Quantity = decimal.Parse(omValues["TotalQty"].ToString());
                    _oEntites.AddToStocks(oStock);

                }
                else
                {
                    decimal oPk_JobCardPartsID = decimal.Parse(omValues["Pk_JobCardPartsID"].ToString());
                    JobCardPartsMaster oJobFromShelf = _oEntites.JobCardPartsMaster.Where(p => p.Pk_JobCardPartsID == oPk_JobCardPartsID).Single();
                    //subtract the old quantity
                    oStock.Quantity = Convert.ToDecimal(oStock.Quantity - oJobFromShelf.TotalQty);
                    //Adding the updated quantity
                    oStock.Quantity = Convert.ToDecimal(oStock.Quantity + decimal.Parse(omValues["TotalQty"].ToString()));
                }


                JobCardPartsMaster oNewJob = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(JobCardPartsMaster), omValues) as JobCardPartsMaster;
                if (omValues["Fk_Status"].ToString() == "1")
                    omValues["Fk_Status"] = 4;//Closed status
                else
                    omValues["Fk_Status"] = 1;//New Status.

                JobCardPartsMaster oNewJobFromShelf = _oEntites.JobCardPartsMaster.Where(p => p.Pk_JobCardPartsID == oNewJob.Pk_JobCardPartsID).Single();
                object orefJobFromShelf = oNewJobFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(JobCardPartsMaster), omValues, ref orefJobFromShelf);

                List<decimal> oUpdateList = new List<decimal>();
                if (omValues.ContainsKey("JCPartsDetails"))
                {
                    string sJobDetail = omValues["JCPartsDetails"].ToString();
                    object[] sJCPartsDetails = JsonConvert.DeserializeObject<object[]>(sJobDetail);
                    List<decimal> UpdatedPk = new List<decimal>();
                    List<JobCardPartsDetails> oJobDetList = new List<JobCardPartsDetails>();

                    foreach (object oIndent in sJCPartsDetails)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oIndent.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());

                        if (oDataProperties.ContainsKey("Pk_JobCardPartsDet") && oDataProperties["Pk_JobCardPartsDet"] != null && oDataProperties["Pk_JobCardPartsDet"].ToString() != "")
                        {
                            decimal Pk_JobCardPartsDet = decimal.Parse(oDataProperties["Pk_JobCardPartsDet"].ToString());

                            JobCardPartsDetails oInvDet = _oEntites.JobCardPartsDetails.Where(p => p.Pk_JobCardPartsDet == Pk_JobCardPartsDet).Single();

                            object orefSchedule = oInvDet;
                            WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(JobCardPartsDetails), oDataProperties, ref orefSchedule);
                            UpdatedPk.Add(Pk_JobCardPartsDet);

                        }
                        else
                        {
                            JobCardPartsDetails oInvDets = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(JobCardPartsDetails), oDataProperties) as JobCardPartsDetails;
                            // oInvDets.RequiredDate = null;
                            oJobDetList.Add(oInvDets);
                        }
                    }

                    var oDeletedRecords = oNewJobFromShelf.JobCardPartsDetails.Where(p => !UpdatedPk.Contains(p.Pk_JobCardPartsDet)).ToList();

                    foreach (JobCardPartsDetails oDeletedDetail in oDeletedRecords)
                    {
                        oNewJobFromShelf.JobCardPartsDetails.Remove(oDeletedDetail);
                    }

                    //Add new elements
                    foreach (JobCardPartsDetails oNewIndentDetail in oJobDetList)
                    {
                        oNewJobFromShelf.JobCardPartsDetails.Add(oNewIndentDetail);
                    }

                }




                _oEntites.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        //public ModelManuplationResult Delete()
        //{
        //    ModelManuplationResult oResult = new ModelManuplationResult();

        //    try
        //    {
        //        JobCardMaster oOrder = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(JobCardMaster), omValues) as JobCardMaster;
        //        JobCardMaster oOrderFromShelf = _oEntites.JobCardMaster.Where(p => p.CustomerPO == oOrder.CustomerPO).Single();
        //        _oEntites.DeleteObject(oOrderFromShelf);
        //        _oEntites.SaveChanges();
        //        oResult.Success = true;
        //    }
        //    catch (Exception e)
        //    {
        //        oResult.Success = false;
        //        oResult.Message = e.Message;
        //        oResult.Exception = e;
        //    }

        //    return oResult;
        //}

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oJob;
            }
            set
            {
                oJob = value as JobCardPartsMaster;
            }
        }

        public decimal ID
        {
            get
            {
                return oJob.Pk_JobCardPartsID;
            }
            set
            {
                oJob = _oEntites.JobCardPartsMaster.Where(p => p.Pk_JobCardPartsID == value).Single();
            }
        }

        public object GetRaw()
        {
            return oJob;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        public ModelManuplationResult Delete()
        {
            throw new NotImplementedException();
        }

        //ModelManuplationResult IDomainObject.CreateNew()
        //{
        //    throw new NotImplementedException();
        //}

        EntityObject IDomainObject.DAO
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        ModelManuplationResult IDomainObject.Delete()
        {
            throw new NotImplementedException();
        }

        void IDomainObject.Dispose()
        {
            Dispose(true);
            //GC.SuppressFinalize(this);
        }



    }
}
