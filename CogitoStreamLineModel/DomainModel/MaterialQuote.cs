﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//namespace CogitoStreamLineModel.DomainModel
//{
//    class MaterialQuote
//    {
//    }
//}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using Newtonsoft.Json;
using System.Web;
using System.Net.Mail;

namespace CogitoStreamLineModel.DomainModel
{
    public class MaterialQuote : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntities = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private MaterialQuotationMaster oMaterialQuotationMaster = null;

        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 

            /* Creating a query object with operator , field Ex Quantity >30 to be used for query later*/
            string sPk_QuotationRequest = null;
            //string sMaterialIndentDate = null;
            string sFk_Vendor = null;
            //string sRequests = null;
            string sFromRequestDate = null;
            string sToRequestDate = null;
            //string sStatus = null;
            //string sOrderNumber = null;
            //string sMaterial = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<MaterialQuotationMaster> oMaterialQuotationMasters = null;

            try { sPk_QuotationRequest = p_Params.Single(p => p.ParameterName == "Pk_QuotationRequest").ParameterValue; }
            catch { }
            try { sFromRequestDate = p_Params.Single(p => p.ParameterName == "FromRequestDate").ParameterValue; }
            catch { }
            try { sToRequestDate = p_Params.Single(p => p.ParameterName == "ToRequestDate").ParameterValue; }
            catch { }
            try { sFk_Vendor = p_Params.Single(p => p.ParameterName == "Fk_Vendor").ParameterValue; }
            catch { }
            //try { sVendor = p_Params.Single(p => p.ParameterName == "Vendor").ParameterValue; }
            //catch { }
            //try { sStatus = p_Params.Single(p => p.ParameterName == "Status").ParameterValue; }
            //catch { }
            //try { sOrderNumber = p_Params.Single(p => p.ParameterName == "OrderNumber").ParameterValue; }
            //catch { }
            //try { sMaterial = p_Params.Single(p => p.ParameterName == "Material").ParameterValue; }
            //catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oMaterialQuotationMasters = _oEntities.MaterialQuotationMasters;
            //DateTime oDateTime;
            try
            {
                if (!string.IsNullOrEmpty(sPk_QuotationRequest))
                {
                    oMaterialQuotationMasters = oMaterialQuotationMasters.Where(p => p.Pk_QuotationRequest == decimal.Parse(sPk_QuotationRequest));
                }
                if (!string.IsNullOrEmpty(sFromRequestDate))
                {
                    oMaterialQuotationMasters = oMaterialQuotationMasters.Where(p => (p.RequestDate >= Convert.ToDateTime(sFromRequestDate)));
                }
                if (!string.IsNullOrEmpty(sToRequestDate))
                {
                    oMaterialQuotationMasters = oMaterialQuotationMasters.Where(p => (p.RequestDate <= Convert.ToDateTime(sToRequestDate)));
                }
                //if (!string.IsNullOrEmpty(sBranch))
                //{
                //    oInv_MaterialIndentMasters = oInv_MaterialIndentMasters.Where(p => p.Fk_Tanent == decimal.Parse(sBranch));
                //}
                if (!string.IsNullOrEmpty(sFk_Vendor))
                {
                    //    oInv_MaterialIndentMasters = oInv_MaterialIndentMasters.Where(p => p.Fk_VendorId == decimal.Parse(sVendor));
                    oMaterialQuotationMasters = oMaterialQuotationMasters.Where(p => p.Fk_Vendor != null && p.gen_Vendor.VendorName.IndexOf(sFk_Vendor, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                //if (!string.IsNullOrEmpty(sStatus))
                //{
                //    oInv_MaterialIndentMasters = oInv_MaterialIndentMasters.Where(p => p.Fk_Status == decimal.Parse(sStatus));
                //}
                //if (!string.IsNullOrEmpty(sOrderNumber))
                //{
                //    List<decimal> oMaterialDetails = _oEntities.Inv_MaterialIndentDetails.Where(x => x.Fk_CustomerOrder == decimal.Parse(sOrderNumber))
                //                                                                         .Select(x => x.Fk_MaterialOrderMasterId).OfType<decimal>().ToList();

                //    oInv_MaterialIndentMasters = oInv_MaterialIndentMasters.Where(p => oMaterialDetails.Contains(p.Pk_MaterialOrderMasterId));
                //}
                //if (!string.IsNullOrEmpty(sMaterial))
                //{
                //    List<decimal> oMaterialDetails = _oEntities.Inv_MaterialIndentDetails.Where(x => x.Fk_Material == decimal.Parse(sMaterial))
                //                                                                         .Select(x => x.Fk_MaterialOrderMasterId).OfType<decimal>().ToList();

                //    oInv_MaterialIndentMasters = oInv_MaterialIndentMasters.Where(p => oMaterialDetails.Contains(p.Pk_MaterialOrderMasterId));
                //}
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oMaterialQuotationMasters.Count();
            oMaterialQuotationMasters = oMaterialQuotationMasters.OrderByDescending(p => p.Pk_QuotationRequest);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oMaterialQuotationMasters = oMaterialQuotationMasters.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oMaterialQuotationMasters.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;
        }

        public List<EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();
            try
            {
                MaterialQuotationMaster oNewMaterialQuote = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(MaterialQuotationMaster), omValues) as MaterialQuotationMaster;
                oNewMaterialQuote.CreatedBy = Convert.ToInt32(HttpContext.Current.Session["UserID"]);
                if (omValues.ContainsKey("QuoteDetails"))
                {
                    string sQuoteDetails = omValues["QuoteDetails"].ToString();
                    object[] QuoteDetails = JsonConvert.DeserializeObject<object[]>(sQuoteDetails);

                    foreach (object oDtetails in QuoteDetails)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oDtetails.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());
                        MaterialQuotationDetail oQuoteDetail = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(MaterialQuotationDetail), oDataProperties) as MaterialQuotationDetail;
                        oNewMaterialQuote.MaterialQuotationDetails.Add(oQuoteDetail);


                    }
                }
                //oNewMaterialQuote.Fk_Status = _oEntities.wfStates.Where(p => p.State == "New").Single().Pk_State;
                //oNewMaterialIndent.Fk_VendorId =oIn

                //oNewOrder.OrderDate = DateTime.Today.ToString();
                _oEntities.AddToMaterialQuotationMasters(oNewMaterialQuote);
                _oEntities.SaveChanges();

                string senderID = "ravich.cogito@gmail.com";
                string senderPassword = "R@viku@123";
                //string result = "Email Sent Successfully";

                string body = " has sent an email from ";
                body += "Phone : ";
                try
                {
                    System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                    mail.To.Add("ravi190585@gmail.com");
                    mail.CC.Add("ravi@cogitotechsolutions.com");
                    Attachment attach = new Attachment("F:\\REJECETED FR N SG LIST.xlsx");
                    mail.Attachments.Add(attach);
                    mail.From = new MailAddress(senderID);
                    mail.Subject = "My Test Email!";
                    mail.Body = body;
                    mail.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "smtp.gmail.com"; //Or Your SMTP Server Address
                    smtp.Credentials = new System.Net.NetworkCredential(senderID, senderPassword);
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.Send(mail);
                }
                catch (Exception )
                {
                    // result = "problem occurred";
                    // Response.Write("Exception in sendEmail:" + ex.Message);
                }

                oResult.Success = true;
                oResult.Message = oNewMaterialQuote.Pk_QuotationRequest.ToString();
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                MaterialQuotationMaster oMaterialQuote = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(MaterialQuotationMaster), omValues) as MaterialQuotationMaster;
                MaterialQuotationMaster oMaterialQuoteFromShelf = _oEntities.MaterialQuotationMasters.Where(p => p.Pk_QuotationRequest == oMaterialQuote.Pk_QuotationRequest).Single();
                object orefMaterialQuoteFromShelf = oMaterialQuoteFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(MaterialQuotationMaster), omValues, ref orefMaterialQuoteFromShelf);

                List<decimal> oUpdateList = new List<decimal>();
                if (omValues.ContainsKey("QuoteDetails"))
                {
                    string sMaterialQuote = omValues["QuoteDetails"].ToString();
                    object[] sMaterialQuotes = JsonConvert.DeserializeObject<object[]>(sMaterialQuote);
                    List<decimal> UpdatedPk = new List<decimal>();
                    List<MaterialQuotationDetail> NewQuoteList = new List<MaterialQuotationDetail>();

                    foreach (object oQuote in sMaterialQuotes)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oQuote.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());

                        if (oDataProperties.ContainsKey("Pk_QuotationMaterial") && oDataProperties["Pk_QuotationMaterial"] != null && oDataProperties["Pk_QuotationMaterial"].ToString() != "")
                        {
                            decimal Pk_QuotationMaterial = decimal.Parse(oDataProperties["Pk_QuotationMaterial"].ToString());

                            MaterialQuotationDetail oQuoteFromShelf = _oEntities.MaterialQuotationDetails.Where(p => p.Pk_QuotationMaterial == Pk_QuotationMaterial).Single();

                            object orefSchedule = oQuoteFromShelf;
                            WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(MaterialQuotationDetail), oDataProperties, ref orefSchedule);
                            UpdatedPk.Add(Pk_QuotationMaterial);
                        }
                        else
                        {
                            MaterialQuotationDetail oQuotes = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(MaterialQuotationDetail), oDataProperties) as MaterialQuotationDetail;
                            //oQuotes.RequestDate = null;
                            NewQuoteList.Add(oQuotes);
                        }
                    }

                    List<MaterialQuotationDetail> oDeletedRecords = oMaterialQuoteFromShelf.MaterialQuotationDetails.Where(p => !UpdatedPk.Contains(p.Pk_QuotationMaterial)).ToList();

                    foreach (MaterialQuotationDetail oDeletedDetail in oDeletedRecords)
                    {
                        oMaterialQuoteFromShelf.MaterialQuotationDetails.Remove(oDeletedDetail);
                    }

                    //Add new elements
                    foreach (MaterialQuotationDetail oNewQuoteDetail in NewQuoteList)
                    {
                        oMaterialQuoteFromShelf.MaterialQuotationDetails.Add(oNewQuoteDetail);
                    }

                }
                _oEntities.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                MaterialQuotationMaster oMaterialQuote = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(MaterialQuotationMaster), omValues) as MaterialQuotationMaster;
                MaterialQuotationMaster oMaterialQuoteFromShelf = _oEntities.MaterialQuotationMasters.Where(p => p.Pk_QuotationRequest == oMaterialQuote.Pk_QuotationRequest).Single();
                oMaterialQuoteFromShelf.MaterialQuotationDetails.Clear();
                _oEntities.DeleteObject(oMaterialQuoteFromShelf);
                _oEntities.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public EntityObject DAO
        {
            get
            {
                return oMaterialQuotationMaster;
            }
            set
            {
                oMaterialQuotationMaster = value as MaterialQuotationMaster;
            }
        }



        public decimal ID
        {
            get
            {
                return oMaterialQuotationMaster.Pk_QuotationRequest;
            }
            //set
            //{
            //    oInv_MaterialIndentMaster.Pk_MaterialOrderMasterId = value;
            //}
            set
            {
                oMaterialQuotationMaster = _oEntities.MaterialQuotationMasters.Where(p => p.Pk_QuotationRequest == value).Single();
            }
        }

        public object GetRaw()
        {
            return oMaterialQuotationMaster;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntities.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}
