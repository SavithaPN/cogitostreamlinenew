﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Objects.DataClasses;
using WebGareCore;
using WebGareCore.CommonObjects;
using System.Data;
using CogitoStreamLineModel;
using System.Data.Entity;

namespace CogitoStreamLineModel.DomainModel
{
    public class Stud : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();

        private Dictionary<string, object> omValues = null;
        private Inv_Material oDept = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();
            string sPk_DepartmentId = null;
            string sName = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Inv_Material> oDepts = null;

            try { sPk_DepartmentId = p_Params.Single(p => p.ParameterName == "Pk_Material").ParameterValue; }
            catch { }
            try { sName = p_Params.Single(p => p.ParameterName == "studentname").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oDepts = _oEntites.Inv_Material;
            try
            {
                if (!string.IsNullOrEmpty(sPk_DepartmentId))
                {
                    oDepts = oDepts.Where(p => p.Pk_Material == decimal.Parse(sPk_DepartmentId));
                }
                //if (!string.IsNullOrEmpty(sName))
                //{
                //    oDepts = oDepts.Where(p => p.studentname.IndexOf(sName.ToLower(), StringComparison.OrdinalIgnoreCase) >= 0);
                //    //oDeptNames = oDeptNames.Where(p => p.PaperTypeName.ToLower() == (sPaperTypeName.ToLower().ToString().Trim()));
                //}
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oDepts.Count();

            oDepts = oDepts.OrderByDescending(p => p.Pk_Material);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oDepts = oDepts.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oDepts.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }

        public SearchResult SearchDepartmentName(List<SearchParameter> p_Params)
        {

            SearchResult oSearchResult = new SearchResult();
            IEnumerable<Inv_Material> oDeptName = null;
            oDeptName = _oEntites.Inv_Material;

            oSearchResult.RecordCount = oDeptName.Count();
            oDeptName.OrderBy(p => p.Pk_Material);

            List<EntityObject> oFilteredItem = oDeptName.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItem;

            return oSearchResult;
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Inv_Material oNewDeptNames = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Inv_Material), omValues) as Inv_Material;

                _oEntites.AddToInv_Material(oNewDeptNames);
                //string sCategory = "CaseBox";
                //decimal dCatId = _oEntites.CategoryM.Where(p => p.CategoryName.ToLower() == sCategory.ToLower()).Single().Pk_Category;

                //ProductM oPrdVal = new ProductM();
                //oPrdVal.Fk_Category = dCatId;

                _oEntites.SaveChanges();


                oResult.Success = true;


                //oPrdVal.Fk_Gen = oNewDeptNames.Pk_Material;
                //_oEntites.AddToProductM(oPrdVal);

                //_oEntites.SaveChanges();
                //oPrdVal.Barcode = oPrdVal.Pk_PrdID.ToString();
                //_oEntites.SaveChanges();
                //oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Inv_Material oDeptNames = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Inv_Material), omValues) as Inv_Material;
                Inv_Material oDeptNamesFromShelf = _oEntites.Inv_Material.Where(p => p.Pk_Material == oDeptNames.Pk_Material).Single();
                object orefItems = oDeptNamesFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(Inv_Material), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Inv_Material oDeptNames = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Inv_Material), omValues) as Inv_Material;
                Inv_Material oDeptNamesFromShelf = _oEntites.Inv_Material.Where(p => p.Pk_Material == oDeptNames.Pk_Material).Single();
                object orefItems = oDeptNamesFromShelf;
                //_oEntites.DeleteObject(oEmployeesFromShelf);

                _oEntites.DeleteObject(oDeptNamesFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oDept;

            }
            set
            {
                oDept = value as Inv_Material;
            }
        }

        public decimal ID
        {
            get
            {
                return oDept.Pk_Material;
            }
            set
            {
                oDept = _oEntites.Inv_Material.Where(p => p.Pk_Material == value).Single();
            }
        }

        public object GetRaw()
        {
            return oDept;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}

