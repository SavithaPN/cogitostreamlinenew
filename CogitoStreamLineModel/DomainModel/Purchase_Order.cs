﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using System.Web;
using Newtonsoft.Json;

namespace CogitoStreamLineModel.DomainModel
{
    public class Purchase_Order : IDomainObject
    {
        #region Variables
        readonly CogitoStreamLineEntities _oEntities = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private PurchaseOrderM oPOrder = null;
        #endregion Variables

        #region Methods
        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            string sPk_PONo = null;
            string sFkIndent = null;
            string sPODate = null;
            string sFromDate = null;
            string sToDate = null;

            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<PurchaseOrderM> oPOrd = null;

            try { sPk_PONo = p_Params.Single(p => p.ParameterName == "Pk_PONo").ParameterValue; }
            catch { }
            try { sFkIndent = p_Params.Single(p => p.ParameterName == "FkIndent").ParameterValue; }
            catch { }
            try { sPODate = p_Params.Single(p => p.ParameterName == "PODate").ParameterValue; }
            catch { }
            try { sFromDate = p_Params.Single(p => p.ParameterName == "TxtFromDate").ParameterValue; }
            catch { }
            try { sToDate = p_Params.Single(p => p.ParameterName == "TxtToDate").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oPOrd = _oEntities.PurchaseOrderM;

            try
            {
                if (!string.IsNullOrEmpty(sPk_PONo))
                {
                    oPOrd = oPOrd.Where(p => p.Pk_PONo == decimal.Parse(sPk_PONo));
                }
                if (!string.IsNullOrEmpty(sFkIndent))
                {
                    oPOrd = oPOrd.Where(p => p.Fk_Indent == decimal.Parse(sFkIndent));
                }
                if (!string.IsNullOrEmpty(sPODate))
                {
                    oPOrd = oPOrd.Where(p => DateTime.Parse(p.PODate.ToString()).ToString("dd/MM/yyyy") == sPODate);
                }
                if (!string.IsNullOrEmpty(sFromDate))
                {

                    DateTime SFrom = Convert.ToDateTime(sFromDate.ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                    oPOrd = oPOrd.Where(p => (p.PODate >= Convert.ToDateTime(SFrom)));
                }
                if (!string.IsNullOrEmpty(sToDate))
                {


                    DateTime STo = Convert.ToDateTime(sToDate.ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                    oPOrd = oPOrd.Where(p => (p.PODate <= Convert.ToDateTime(STo)));
                }
              
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oPOrd.Count();
            oPOrd = oPOrd.OrderByDescending(p => p.Pk_PONo);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oPOrd = oPOrd.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oPOrd.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }


        public SearchResult SearchIssueDetails(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_MaterialID = null;
            string sPk_MaterialName = null;
        //    string sFk_Material = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<vw_StockMaterial> oIssueIssueMasters = null;

            try { sPk_MaterialID = p_Params.Single(p => p.ParameterName == "Pk_MaterialID").ParameterValue; }
            catch { }
            try { sPk_MaterialName = p_Params.Single(p => p.ParameterName == "MaterialName").ParameterValue; }
            catch { }
            //try { sFk_Material = p_Params.Single(p => p.ParameterName == "Fk_Material").ParameterValue; }
            //catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oIssueIssueMasters = _oEntities.vw_StockMaterial;

            try
            {
                if (!string.IsNullOrEmpty(sPk_MaterialID))
                {
                    oIssueIssueMasters = oIssueIssueMasters.Where(p => p.Pk_Material == decimal.Parse(sPk_MaterialID));
                }
                if (!string.IsNullOrEmpty(sPk_MaterialName))
                {
                    oIssueIssueMasters = oIssueIssueMasters.Where(p => p.Name.IndexOf(sPk_MaterialName, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                //if (!string.IsNullOrEmpty(sFk_Material))
                //{
                //    oIssueIssueMasters = oIssueIssueMasters.Where(p => p.Fk_Material == decimal.Parse(sFk_Material));
                //}


            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oIssueIssueMasters.Count();
            oIssueIssueMasters = oIssueIssueMasters.OrderByDescending(p => p.Pk_Material);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oIssueIssueMasters = oIssueIssueMasters.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oIssueIssueMasters.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }

        public SearchResult SearchPODet(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            string sPk_PONo = null;
            string sGSM = null;
            string sBF = null;
            string sDec = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_PoGetRec> oPOrd = null;

            try { sPk_PONo = p_Params.Single(p => p.ParameterName == "Pk_PONo").ParameterValue; }
            catch { }
            try { sGSM = p_Params.Single(p => p.ParameterName == "GSM").ParameterValue; }
            catch { }
            try { sBF = p_Params.Single(p => p.ParameterName == "BF").ParameterValue; }
            catch { }
            try { sDec = p_Params.Single(p => p.ParameterName == "Dec").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oPOrd = _oEntities.Vw_PoGetRec;

            try
            {
                if (!string.IsNullOrEmpty(sPk_PONo))
                {
                    oPOrd = oPOrd.Where(p => p.Pk_PONo == decimal.Parse(sPk_PONo));
                }
                if (!string.IsNullOrEmpty(sBF))
                {
                    oPOrd = oPOrd.Where(p => p.BF == decimal.Parse(sBF));
                }
                if (!string.IsNullOrEmpty(sGSM))
                {
                    oPOrd = oPOrd.Where(p => p.GSM == decimal.Parse(sGSM));
                }
                if (!string.IsNullOrEmpty(sDec))
                {
                    oPOrd = oPOrd.Where(p => p.Deckle == decimal.Parse(sDec));
                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oPOrd.Count();
            oPOrd = oPOrd.OrderByDescending(p => p.Pk_PONo);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {
                var Nnval = int.Parse(sPageSize);
                if (Nnval == 0)
                {

                    //var page = (int.Parse(sStartIndex) / 1) ; // set current page number, must be >= 1
                    //var skip = int.Parse(sPageSize) * (page - 1);

                    //oPOrd = oPOrd.Select(p => p)
                    //                    .Skip(skip)
                    //                    .Take(int.Parse(sPageSize));
                }
                else
                {
                    var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                    var skip = int.Parse(sPageSize) * (page - 1);

                    oPOrd = oPOrd.Select(p => p)
                                        .Skip(skip)
                                        .Take(int.Parse(sPageSize));
                }
            }

            List<EntityObject> oFilteredItems = oPOrd.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }



        public SearchResult SearchOpenPODet(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            string sPk_PONo = null;
            string sGSM = null;
            string sBF = null;
            string sDec = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_OthersMaterial> oPOrd = null;

            try { sPk_PONo = p_Params.Single(p => p.ParameterName == "Pk_PONo").ParameterValue; }
            catch { }
            try { sGSM = p_Params.Single(p => p.ParameterName == "GSM").ParameterValue; }
            catch { }
            try { sBF = p_Params.Single(p => p.ParameterName == "BF").ParameterValue; }
            catch { }
            try { sDec = p_Params.Single(p => p.ParameterName == "Dec").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oPOrd = _oEntities.Vw_OthersMaterial;

            try
            {
                //if (!string.IsNullOrEmpty(sPk_PONo))
                //{
                //    oPOrd = oPOrd.Where(p => p.Pk_PONo == decimal.Parse(sPk_PONo));
                //}
                //if (!string.IsNullOrEmpty(sBF))
                //{
                //    oPOrd = oPOrd.Where(p => p.BF == decimal.Parse(sBF));
                //}
                if (!string.IsNullOrEmpty(sGSM))
                {
                    //oPOrd = oPOrd.Where(p => p.GSM == decimal.Parse(sGSM));
                    oPOrd = oPOrd.Where(p => p.Name.IndexOf(sGSM, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                //if (!string.IsNullOrEmpty(sDec))
                //{
                //    oPOrd = oPOrd.Where(p => p.Deckle == decimal.Parse(sDec));
                //}
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oPOrd.Count();
            oPOrd = oPOrd.OrderByDescending(p => p.Pk_Material);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {
                var Nnval = int.Parse(sPageSize);
                if (Nnval == 0)
                {

                    //var page = (int.Parse(sStartIndex) / 1) ; // set current page number, must be >= 1
                    //var skip = int.Parse(sPageSize) * (page - 1);

                    //oPOrd = oPOrd.Select(p => p)
                    //                    .Skip(skip)
                    //                    .Take(int.Parse(sPageSize));
                }
                else
                {
                    var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                    var skip = int.Parse(sPageSize) * (page - 1);

                    oPOrd = oPOrd.Select(p => p)
                                        .Skip(skip)
                                        .Take(int.Parse(sPageSize));
                }
            }

            List<EntityObject> oFilteredItems = oPOrd.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }
      
     
        
        
        public SearchResult SearchPO(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 

            //string CheckDate = "", string Vendor = "", string InvoiceNo = "", string MaterialInward = "", 

            /* Creating a query object with operator , field Ex Quantity >30 to be used for query later*/
            string sPk_PONo = null;
            //string sMaterialIndentDate = null;
            string sVendorName = null;
            string sGSM = null;
            string sBF = null;
            string sMaterialName = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_POCertificate> oQualityCheck = null;

            try { sPk_PONo = p_Params.Single(p => p.ParameterName == "PoNoDisp").ParameterValue; }
            catch { }
            try { sMaterialName = p_Params.Single(p => p.ParameterName == "MaterialName").ParameterValue; }
            catch { }
            try { sGSM = p_Params.Single(p => p.ParameterName == "GSM").ParameterValue; }
            catch { }
              try { sBF = p_Params.Single(p => p.ParameterName == "BF").ParameterValue; }
            catch { }
            try { sVendorName = p_Params.Single(p => p.ParameterName == "VendorName").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oQualityCheck = _oEntities.Vw_POCertificate;

            try
            {

                if (!string.IsNullOrEmpty(sPk_PONo))
                {
                    // oQualityCheck = oQualityCheck.Where(p => p.Pk_PONo == decimal.Parse(sPk_PONo));PoNoDisp
                    oQualityCheck = oQualityCheck.Where(p => p.PkDisp != null && p.PkDisp.IndexOf(sPk_PONo, StringComparison.OrdinalIgnoreCase) >= 0);


                }
                if (!string.IsNullOrEmpty(sGSM))
                {
                    oQualityCheck = oQualityCheck.Where(p => p.GSM == decimal.Parse(sGSM));
                }
                if (!string.IsNullOrEmpty(sBF))
                {
                    oQualityCheck = oQualityCheck.Where(p => p.BF == decimal.Parse(sBF));
                }
                if (!string.IsNullOrEmpty(sVendorName))
                {
                    oQualityCheck = oQualityCheck.Where(p => p.VendorName != null && p.VendorName.IndexOf(sVendorName, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sMaterialName))
                {
                    oQualityCheck = oQualityCheck.Where(p => p.Name.IndexOf(sMaterialName, StringComparison.OrdinalIgnoreCase) >= 0);
                }

            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oQualityCheck.Count();
            oQualityCheck = oQualityCheck.OrderByDescending(p => p.Pk_PONo);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oQualityCheck = oQualityCheck.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oQualityCheck.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;
        }
          public SearchResult SearchPOCert(List<SearchParameter> p_Params)
          {
              SearchResult oSearchResult = new SearchResult();

              //We make a call that Search can only be done for User based on 

              //string CheckDate = "", string Vendor = "", string InvoiceNo = "", string MaterialInward = "", 

              /* Creating a query object with operator , field Ex Quantity >30 to be used for query later*/
              string sPk_PONo = null;
              //string sMaterialIndentDate = null;
              string sVendorName = null;
              string sMaterialName = null;
              string sStartIndex = null;
              string sPageSize = null;
              string sSorting = null;
              IEnumerable<Vw_PODet> oQualityCheck = null;

              try { sPk_PONo = p_Params.Single(p => p.ParameterName == "PkDisp").ParameterValue; }
              catch { }
              try { sMaterialName = p_Params.Single(p => p.ParameterName == "MaterialName").ParameterValue; }
              catch { }

              try { sVendorName = p_Params.Single(p => p.ParameterName == "VendorName").ParameterValue; }
              catch { }
              try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
              catch { }
              try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
              catch { }
              try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
              catch { }


              oQualityCheck = _oEntities.Vw_PODet;

              try
              {

                  if (!string.IsNullOrEmpty(sPk_PONo))
                  {
                    //  oQualityCheck = oQualityCheck.Where(p => p.Pk_PONo == decimal.Parse(sPk_PONo));
                      oQualityCheck = oQualityCheck.Where(p => p.PkDisp.IndexOf(sPk_PONo, StringComparison.OrdinalIgnoreCase) >= 0);
                  }

                  if (!string.IsNullOrEmpty(sVendorName))
                  {
                      oQualityCheck = oQualityCheck.Where(p => p.VendorName != null && p.VendorName.IndexOf(sVendorName, StringComparison.OrdinalIgnoreCase) >= 0);
                  }
                  //if (!string.IsNullOrEmpty(sMaterialName))
                  //{
                  //    oQualityCheck = oQualityCheck.Where(p => p.Name.IndexOf(sMaterialName, StringComparison.OrdinalIgnoreCase) >= 0);
                  //}

              }
              catch (System.NullReferenceException)
              {
                  //possible that some items may not have the required fields set just ignore them
              }
              oSearchResult.RecordCount = oQualityCheck.Count();
              oQualityCheck = oQualityCheck.OrderByDescending(p => p.Pk_PONo);


              if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
              {

                  var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                  var skip = int.Parse(sPageSize) * (page - 1);

                  oQualityCheck = oQualityCheck.Select(p => p)
                                      .Skip(skip)
                                      .Take(int.Parse(sPageSize));
              }

              List<EntityObject> oFilteredItems = oQualityCheck.Select(p => p).OfType<EntityObject>().ToList();
              oSearchResult.ListOfRecords = oFilteredItems;

              return oSearchResult;
          }

          public SearchResult SearchPOConsumables(List<SearchParameter> p_Params)
          {
              SearchResult oSearchResult = new SearchResult();

              //We make a call that Search can only be done for User based on 

              //string CheckDate = "", string Vendor = "", string InvoiceNo = "", string MaterialInward = "", 

              /* Creating a query object with operator , field Ex Quantity >30 to be used for query later*/
              string sPk_PONo = null;
              //string sMaterialIndentDate = null;
              string sVendorName = null;
              string sMaterialName = null;
              string sStartIndex = null;
              string sPageSize = null;
              string sSorting = null;
              IEnumerable<Vw_PoDetOthers> oQualityCheck = null;

              try { sPk_PONo = p_Params.Single(p => p.ParameterName == "PkDisp").ParameterValue; }
              catch { }
              try { sMaterialName = p_Params.Single(p => p.ParameterName == "MaterialName").ParameterValue; }
              catch { }

              try { sVendorName = p_Params.Single(p => p.ParameterName == "VendorName").ParameterValue; }
              catch { }
              try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
              catch { }
              try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
              catch { }
              try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
              catch { }


              oQualityCheck = _oEntities.Vw_PoDetOthers;

              try
              {

                  if (!string.IsNullOrEmpty(sPk_PONo))
                  {
                     // oQualityCheck = oQualityCheck.Where(p => p.Pk_PONo == decimal.Parse(sPk_PONo));
                      oQualityCheck = oQualityCheck.Where(p => p.PkDisp.IndexOf(sPk_PONo, StringComparison.OrdinalIgnoreCase) >= 0);
                  }

                  if (!string.IsNullOrEmpty(sVendorName))
                  {
                      oQualityCheck = oQualityCheck.Where(p => p.VendorName != null && p.VendorName.IndexOf(sVendorName, StringComparison.OrdinalIgnoreCase) >= 0);
                  }
                  if (!string.IsNullOrEmpty(sMaterialName))
                  {
                      oQualityCheck = oQualityCheck.Where(p => p.Name.IndexOf(sMaterialName, StringComparison.OrdinalIgnoreCase) >= 0);
                  }

              }
              catch (System.NullReferenceException)
              {
                  //possible that some items may not have the required fields set just ignore them
              }
              oSearchResult.RecordCount = oQualityCheck.Count();
              oQualityCheck = oQualityCheck.OrderByDescending(p => p.Pk_PONo);


              if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
              {

                  var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                  var skip = int.Parse(sPageSize) * (page - 1);

                  oQualityCheck = oQualityCheck.Select(p => p)
                                      .Skip(skip)
                                      .Take(int.Parse(sPageSize));
              }

              List<EntityObject> oFilteredItems = oQualityCheck.Select(p => p).OfType<EntityObject>().ToList();
              oSearchResult.ListOfRecords = oFilteredItems;

              return oSearchResult;
          }
    
        public SearchResult SearchIndentDetails(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sIndentNo = null;
            string sPk_MaterialID = null;
            string sPk_MaterialName = null;
            string sGSM = null;
            string sBF = null;
            string sDec = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_MIndent> oIssueIssueMasters = null;

            try { sIndentNo = p_Params.Single(p => p.ParameterName == "IndentNo").ParameterValue; }
            catch { }
            try { sPk_MaterialID = p_Params.Single(p => p.ParameterName == "Pk_MaterialID").ParameterValue; }
            catch { }
            try { sPk_MaterialName = p_Params.Single(p => p.ParameterName == "MaterialName").ParameterValue; }
            catch { }
            try { sGSM = p_Params.Single(p => p.ParameterName == "GSM").ParameterValue; }
            catch { }
            try { sBF = p_Params.Single(p => p.ParameterName == "BF").ParameterValue; }
            catch { }
            try { sDec = p_Params.Single(p => p.ParameterName == "Dec").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oIssueIssueMasters = _oEntities.Vw_MIndent;

            try
            {
                if (!string.IsNullOrEmpty(sIndentNo))
                {
                    oIssueIssueMasters = oIssueIssueMasters.Where(p => p.Pk_MaterialOrderMasterId == decimal.Parse(sIndentNo));
                }
                if (!string.IsNullOrEmpty(sPk_MaterialID))
                {
                    oIssueIssueMasters = oIssueIssueMasters.Where(p => p.Pk_Material == decimal.Parse(sPk_MaterialID));
                }
                if (!string.IsNullOrEmpty(sPk_MaterialName))
                {
                    oIssueIssueMasters = oIssueIssueMasters.Where(p => p.Name.IndexOf(sPk_MaterialName, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sBF))
                {
                    oIssueIssueMasters = oIssueIssueMasters.Where(p => p.BF == decimal.Parse(sBF));
                }
                if (!string.IsNullOrEmpty(sGSM))
                {
                    oIssueIssueMasters = oIssueIssueMasters.Where(p => p.GSM == decimal.Parse(sGSM));
                }
                if (!string.IsNullOrEmpty(sDec))
                {
                    oIssueIssueMasters = oIssueIssueMasters.Where(p => p.Deckle == decimal.Parse(sDec));
                }


            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oIssueIssueMasters.Count();
            oIssueIssueMasters = oIssueIssueMasters.OrderByDescending(p => p.Pk_Material);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oIssueIssueMasters = oIssueIssueMasters.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oIssueIssueMasters.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }
        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {

                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();
                PurchaseOrderM oNewPOrder = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(PurchaseOrderM), omValues) as PurchaseOrderM;
              
                if (omValues.ContainsKey("Materials"))
                {
                    string sMaterials = omValues["Materials"].ToString();
                    object[] Materials = JsonConvert.DeserializeObject<object[]>(sMaterials);

                    foreach (object oItem in Materials)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());
                        PurchaseOrderD oNewPOrderD = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(PurchaseOrderD), oDataProperties) as PurchaseOrderD;

                        oNewPOrderD.InwdQty = 0;
                        oNewPOrderD.Amount = (oNewPOrderD.Quantity * oNewPOrderD.Rate);

                        oNewPOrder.PurchaseOrderD.Add(oNewPOrderD);


                    }

                
                }
                if (omValues.ContainsKey("Fk_Vendor"))
                {
                }
                else
                {
                    oNewPOrder.Fk_Vendor = 1;
                }

                if (omValues.ContainsKey("TaxType"))
                {
                    string TaxT = omValues["TaxType"].ToString();
                    if (TaxT == "IGST")
                    {
                        oNewPOrder.TaxType = "12%";
                    }
                    else
                    {
                        oNewPOrder.TaxType = TaxT;
                    }
                }


                oNewPOrder.Fk_Status = 1;

                oNewPOrder.PODate = Convert.ToDateTime(omValues["PODate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);


                //update purchaseorderm set pkdisp = cast(pk_pono as varchar(255)) + '/' + '2018' where pk_pono<15
                var POMax =
               (from PurchaseOrderM in dc.PurchaseOrderM

                orderby PurchaseOrderM.Pk_PONo descending

                select PurchaseOrderM.Pk_PONo).Take(1);


                decimal PoVal = POMax.First();
                decimal FixVal = 66;

                var insVal = Convert.ToDecimal(PoVal - FixVal);
                var YrNext = (DateTime.Now.AddYears(1).Year);
                oNewPOrder.PkDisp = insVal + "/" +(DateTime.Now.Year).ToString()+ "-" + YrNext;
               
                    
                    _oEntities.AddToPurchaseOrderM(oNewPOrder);
                _oEntities.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }
        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();
            decimal gtot = 0;
            decimal edval = 0;
            decimal taxval = 0;


            try
            {

                PurchaseOrderM oPOrd = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(PurchaseOrderM), omValues) as PurchaseOrderM;
                PurchaseOrderM OPOrdFromShelf = _oEntities.PurchaseOrderM.Where(p => p.Pk_PONo == oPOrd.Pk_PONo).Single();
                object orefPO = OPOrdFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(PurchaseOrderM), omValues, ref orefPO);
             
         
                string sMaterials = omValues["Materials"].ToString();
                List<object> Materials = JsonConvert.DeserializeObject<List<object>>(sMaterials);

                List<decimal> UpdatedPk = new List<decimal>();
                List<PurchaseOrderD> NewMaterialsList = new List<PurchaseOrderD>();
                foreach (object oItem in Materials)
                {
                    Dictionary<string, object> oDataProperties1 = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                    Dictionary<string, object> oDataProperties2 = JsonConvert.DeserializeObject<Dictionary<string, object>>(oDataProperties1["data"].ToString());

                    if (oDataProperties2["Pk_PODet"] == null || oDataProperties2["Pk_PODet"].ToString() == "")
                    {
                        PurchaseOrderD oNewPOrderD
                            = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(PurchaseOrderD), oDataProperties2) as PurchaseOrderD;

                        NewMaterialsList.Add(oNewPOrderD);

                   
                    }
                    else
                    {
                        //Handel Update here
                        decimal dPkPOrdDet = Convert.ToDecimal(oDataProperties2["Pk_PODet"]);
                        decimal dFkMaterial = Convert.ToDecimal(oDataProperties2["Fk_Material"]);
                        //decimal dQuantity = Convert.ToDecimal(oDataProperties2["Quantity"]);

                        //gtot = Convert.ToDecimal(gtot) + Convert.ToDecimal(oDataProperties2["Amount"]);


                        PurchaseOrderD oPOrderDetFromShelf = _oEntities.PurchaseOrderD.Where(p => p.Pk_PODet == dPkPOrdDet && p.Fk_Material == dFkMaterial).Single();

                        object oPOrderDet
                                            = _oEntities.PurchaseOrderD.Where(p => p.Pk_PODet == dPkPOrdDet).First();

                        _oEntities.DeleteObject(oPOrderDet);
                          

                        PurchaseOrderD oIndentDetails = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(PurchaseOrderD), oDataProperties2) as PurchaseOrderD;
                        
                        NewMaterialsList.Add(oIndentDetails);


                        
                        UpdatedPk.Add(dPkPOrdDet);

                    }
                }

            
                // Handeling Deleted Records

                List<PurchaseOrderD> oDeletedRecords = OPOrdFromShelf.PurchaseOrderD.Where(p => !UpdatedPk.Contains(p.Pk_PODet)).ToList();

           

                //Add new elements
                foreach (PurchaseOrderD oNewMaterialDetail in NewMaterialsList)
                {

                    gtot = Convert.ToDecimal(gtot) + Convert.ToDecimal(oNewMaterialDetail.Amount);
                    OPOrdFromShelf.PurchaseOrderD.Add(oNewMaterialDetail);
                }

                //object orefPurchaseM = OPOrdFromShelf;
                //WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(PurchaseOrderM), omValues, ref orefPurchaseM);
                   //EDVAL = Convert.ToDecimal(GTotal) + Convert.ToDecimal(GTotal) * Convert.ToDecimal(t1);
                OPOrdFromShelf.GrandTotal = gtot;

                //oPOrd.GrandTotal = Convert.ToDecimal( gtot);
                edval = Convert.ToDecimal(gtot) + Convert.ToDecimal(gtot) * Convert.ToDecimal(0.06);
                OPOrdFromShelf.ED = edval;

                if (OPOrdFromShelf.TaxType=="VAT")
                {
                 taxval=Convert.ToDecimal(edval)+Convert.ToDecimal(edval)* Convert.ToDecimal(0.055);}
            
            else if (OPOrdFromShelf.TaxType=="CST")
                {
                
                taxval=Convert.ToDecimal(edval)+Convert.ToDecimal(edval)* Convert.ToDecimal(0.02);}

                OPOrdFromShelf.NetValue = Convert.ToDecimal(taxval);

                OPOrdFromShelf.PODate = Convert.ToDateTime(omValues["PODate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                OPOrdFromShelf.Fk_Indent = Convert.ToDecimal(omValues["Fk_Indent"]);
                OPOrdFromShelf.GrandTotal = Convert.ToDecimal(gtot);


     // update grandtotal and fk_indent here
         
                PurchaseOrderM oNewPO = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(PurchaseOrderM), omValues) as PurchaseOrderM;
                //var ss = OPOrdFromShelf.Fk_Status.ToString();
                //var newss = oPOrd.Fk_Status.ToString();
                //string ss1 = omValues["Fk_Status"].ToString();
                //if (ss1 == "True")
                //{
                //    OPOrdFromShelf.Fk_Status = 4;
                //    //omValues["Fk_Status"] = 4;//Closed status
                //}

                //else
                //    OPOrdFromShelf.Fk_Status = 1;//New Status
                
                _oEntities.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }
   
        
        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();


            try
            {
                PurchaseOrderM oPOrd = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(PurchaseOrderM), omValues) as PurchaseOrderM;
                PurchaseOrderM OInvIndentMasterFromShelf = _oEntities.PurchaseOrderM.Where(p => p.Pk_PONo == oPOrd.Pk_PONo).Single();
              //  var branchid = Convert.ToInt32(HttpContext.Current.Session["Branch"]);
                if (omValues.ContainsKey("MaterialData"))
                {
                    string sMaterials = omValues["MaterialData"].ToString();
                    object[] Materials = JsonConvert.DeserializeObject<object[]>(sMaterials);

                    foreach (object oItem in Materials)
                    {
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                        decimal FkMaterial = Convert.ToInt32(oDataProperties["Fk_Material"]);

                        //PurchaseOrderD oIssueReturnDetailsFromShelf = _oEntities.PurchaseOrderD.Where(p => p.Fk_IssueReturnMasterId == OInvIndentMasterFromShelf.Pk_IssueReturnMasterId && p.Fk_Material == FkMaterial).Single();
                        //int sFkMaterial = Convert.ToInt32(oDataProperties["Fk_Material"]);

                        //Stock oInv_Stock = _oEntities.Stocks.Where(p => p.Fk_Material == sFkMaterial && p.Fk_Tanent == OInvIndentMasterFromShelf.MaterialIssue.Fk_Tanent && p.Fk_BranchID == branchid).Single();

                        //if ((oInv_Stock.Quantity - oIssueReturnDetailsFromShelf.ReturnQuantity) >= 0)
                        //{
                        //    MaterialIssueDetail oIssuedetails = _oEntities.MaterialIssueDetails.Where(p => p.Fk_Material == oIssueReturnDetailsFromShelf.Fk_Material && p.Fk_IssueID == oPOrd.Fk_IssueId).Single();
                        //    oIssuedetails.ReturnQuantity = (oIssuedetails.ReturnQuantity - oIssueReturnDetailsFromShelf.ReturnQuantity);
                        //    oInv_Stock.Quantity = Convert.ToDecimal(oInv_Stock.Quantity - oIssueReturnDetailsFromShelf.ReturnQuantity);


                        //    _oEntities.DeleteObject(oIssueReturnDetailsFromShelf);

                        //    _oEntities.SaveChanges();
                        //}
                    }
                }
                int oIssueReturnDet = _oEntities.PurchaseOrderD.Where(p => p.Fk_PONo == oPOrd.Pk_PONo).Count();
                if (oIssueReturnDet == 0)
                {
                    OInvIndentMasterFromShelf = _oEntities.PurchaseOrderM.Where(p => p.Pk_PONo == oPOrd.Pk_PONo).Single();
                    _oEntities.DeleteObject(OInvIndentMasterFromShelf);
                    _oEntities.SaveChanges();
                }
                oResult.Success = true;

            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }


            return oResult;
        }
        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oPOrder;
            }
            set
            {
                oPOrder = value as PurchaseOrderM;
            }
        }
        public decimal ID
        {
            get
            {
                return oPOrder.Pk_PONo;
            }
            set
            {
                oPOrder = _oEntities.PurchaseOrderM.Where(p => p.Pk_PONo == value).Single();
            }
        }
        public object GetRaw()
        {
            return oPOrder;
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntities.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


  

        
        
        
        
        #endregion Methods

    }
}
