﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using System.Web;
using Newtonsoft.Json;


namespace CogitoStreamLineModel.DomainModel
{
    public class Mat_Category : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private Inv_MaterialCategory oMCategory = null;
        private BoardDesc oBoxSp = new BoardDesc();

        public bool SetSpecId(decimal p_dSpec)
        {
            if (_oEntites.Inv_MaterialCategory.Where(p => p.Pk_MaterialCategory == p_dSpec).Count() > 0)
            {
                oMCategory = _oEntites.Inv_MaterialCategory.Where(p => p.Pk_MaterialCategory == p_dSpec).First();

                return true;
            }

            return false;
        }

        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_MaterialCategory = null;
            string sName = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Inv_MaterialCategory> oMCategorys = null;

            try { sPk_MaterialCategory = p_Params.Single(p => p.ParameterName == "Pk_MaterialCategory").ParameterValue; }
            catch { }
            try { sName = p_Params.Single(p => p.ParameterName == "Name").ParameterValue; }
            catch { }
        
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oMCategorys = _oEntites.Inv_MaterialCategory;

            try
            {
                if (!string.IsNullOrEmpty(sPk_MaterialCategory))
                {
                    oMCategorys = oMCategorys.Where(p => p.Pk_MaterialCategory == decimal.Parse(sPk_MaterialCategory));
                }
                if (!string.IsNullOrEmpty(sName))
                {
                    oMCategorys = oMCategorys.Where(p => p.Name.IndexOf(sName, StringComparison.OrdinalIgnoreCase) >= 0);

                }
              
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oMCategorys.Count();

            oMCategorys.OrderBy(p => p.Pk_MaterialCategory);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oMCategorys = oMCategorys.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oMCategorys.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }
        

        public SearchResult SearchIssue(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sMaterialCategory = null;
            string sName = null;
            string sMatName = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_Popup_MaterialIssue> oMIssueCategorys = null;

            try { sMaterialCategory = p_Params.Single(p => p.ParameterName == "MaterialCategory").ParameterValue; }
            catch { }
            try { sName = p_Params.Single(p => p.ParameterName == "Name").ParameterValue; }
            catch { }
            try { sMatName = p_Params.Single(p => p.ParameterName == "MaterialName").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oMIssueCategorys = _oEntites.Vw_Popup_MaterialIssue;

            try
            {

                //oMIssueCategorys = oMIssueCategorys.Where(p => p.Fk_BranchID == Convert.ToInt32(HttpContext.Current.Session["Branch"]));
                
                if (!string.IsNullOrEmpty(sMaterialCategory))
                {
                    if (sMaterialCategory == "Others")
                    {
                        oMIssueCategorys = oMIssueCategorys.Where(p => p.Name !="Paper" );
                    }
                    else
                    {
                        oMIssueCategorys = oMIssueCategorys.Where(p => p.Name.IndexOf(sMaterialCategory, StringComparison.OrdinalIgnoreCase) >= 0);
                    }
                }
                
                if (!string.IsNullOrEmpty(sName))
                {
                    oMIssueCategorys = oMIssueCategorys.Where(p => p.MaterialName.IndexOf(sName, StringComparison.OrdinalIgnoreCase) >= 0);

                }
                if (!string.IsNullOrEmpty(sMatName))
                {
                    oMIssueCategorys = oMIssueCategorys.Where(p => p.MaterialName.IndexOf(sMatName, StringComparison.OrdinalIgnoreCase) >= 0);

                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oMIssueCategorys.Count();

            oMIssueCategorys.OrderBy(p => p.Pk_MaterialCategory);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oMIssueCategorys = oMIssueCategorys.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oMIssueCategorys.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }

        public SearchResult SearchPaperIssue(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sMaterialCategory = null;
            string sName = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<vw_PaperRollStock> oMIssueCategorys = null;

            try { sMaterialCategory = p_Params.Single(p => p.ParameterName == "MaterialCategory").ParameterValue; }
            catch { }
            try { sName = p_Params.Single(p => p.ParameterName == "Name").ParameterValue; }
            catch { }

            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oMIssueCategorys = _oEntites.vw_PaperRollStock;

            try
            {

                //oMIssueCategorys = oMIssueCategorys.Where(p => p.Fk_BranchID == Convert.ToInt32(HttpContext.Current.Session["Branch"]));

                //if (!string.IsNullOrEmpty(sMaterialCategory))
                //{
                //    oMIssueCategorys = oMIssueCategorys.Where(p => p.Name.IndexOf(sMaterialCategory, StringComparison.OrdinalIgnoreCase) >= 0);

                //}
                if (!string.IsNullOrEmpty(sName))
                {
                    oMIssueCategorys = oMIssueCategorys.Where(p => p.Name.IndexOf(sName, StringComparison.OrdinalIgnoreCase) >= 0);

                }

            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oMIssueCategorys.Count();

            oMIssueCategorys.OrderBy(p => p.Pk_Material);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oMIssueCategorys = oMIssueCategorys.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oMIssueCategorys.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }
        public SearchResult SearchJobCard(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sMaterialCategory = null;
            string sJobCardID = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_JobIssue> oMIssueCategorys = null;

            try { sMaterialCategory = p_Params.Single(p => p.ParameterName == "MaterialCategory").ParameterValue; }
            catch { }
            try { sJobCardID = p_Params.Single(p => p.ParameterName == "JobCardID").ParameterValue; }
            catch { }

            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oMIssueCategorys = _oEntites.Vw_JobIssue;

            try
            {

                //oMIssueCategorys = oMIssueCategorys.Where(p => p.Fk_BranchID == Convert.ToInt32(HttpContext.Current.Session["Branch"]));

                //if (!string.IsNullOrEmpty(sMaterialCategory))
                //{
                //    oMIssueCategorys = oMIssueCategorys.Where(p => p.Name.IndexOf(sMaterialCategory, StringComparison.OrdinalIgnoreCase) >= 0);

                //}
                if (!string.IsNullOrEmpty(sJobCardID))
                {
                    oMIssueCategorys = oMIssueCategorys.Where(p => p.Pk_JobCardID == Convert.ToDecimal(sJobCardID));

                }

            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oMIssueCategorys.Count();

            oMIssueCategorys.OrderBy(p => p.Pk_JobCardDet);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oMIssueCategorys = oMIssueCategorys.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oMIssueCategorys.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }
        public SearchResult SearchName(List<SearchParameter> p_Params)
        {

            SearchResult oSearchResult = new SearchResult();
            IEnumerable<Inv_MaterialCategory> oMCategoryName = null;
            oMCategoryName = _oEntites.Inv_MaterialCategory;

            oSearchResult.RecordCount = oMCategoryName.Count();
            oMCategoryName.OrderBy(p => p.Pk_MaterialCategory);

            List<EntityObject> oFilteredItem = oMCategoryName.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItem;

            return oSearchResult;
        }
        public SearchResult SearchPaperIndentIssue(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sMaterialCategory = null;
            string sName = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            string sGSM = null;
            string sBF = null;
            string sDeckle = null;
            string sMill = null;
            IEnumerable<Vw_PaperStock> oMIssueCategorys = null;

            try { sMaterialCategory = p_Params.Single(p => p.ParameterName == "MaterialCategory").ParameterValue; }
            catch { }
            try { sName = p_Params.Single(p => p.ParameterName == "Name").ParameterValue; }
            catch { }
            try { sGSM = p_Params.Single(p => p.ParameterName == "GSM").ParameterValue; }
            catch { }
            try { sBF = p_Params.Single(p => p.ParameterName == "BF").ParameterValue; }
            catch { }
            try { sDeckle = p_Params.Single(p => p.ParameterName == "Deckle").ParameterValue; }
            catch { }
            try { sMill = p_Params.Single(p => p.ParameterName == "Mill").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oMIssueCategorys = _oEntites.Vw_PaperStock;

            try
            {

                //oMIssueCategorys = oMIssueCategorys.Where(p => p.Fk_BranchID == Convert.ToInt32(HttpContext.Current.Session["Branch"]));

                //if (!string.IsNullOrEmpty(sMaterialCategory))
                //{
                //    oMIssueCategorys = oMIssueCategorys.Where(p => p.Name.IndexOf(sMaterialCategory, StringComparison.OrdinalIgnoreCase) >= 0);

                //}
                if (!string.IsNullOrEmpty(sName))
                {
                    oMIssueCategorys = oMIssueCategorys.Where(p => p.Name.IndexOf(sName, StringComparison.OrdinalIgnoreCase) >= 0);

                }
                if (!string.IsNullOrEmpty(sGSM))
                {
                    oMIssueCategorys = oMIssueCategorys.Where(p => p.GSM == Convert.ToDecimal(sGSM));
                }
                if (!string.IsNullOrEmpty(sBF))
                {
                    oMIssueCategorys = oMIssueCategorys.Where(p => p.BF == Convert.ToDecimal(sBF));
                }
                if (!string.IsNullOrEmpty(sDeckle))
                {
                    oMIssueCategorys = oMIssueCategorys.Where(p => p.Deckle == Convert.ToDecimal(sDeckle));
                }
                if (!string.IsNullOrEmpty(sMill))
                {
                    oMIssueCategorys = oMIssueCategorys.Where(p => p.MillName.IndexOf(sMill, StringComparison.OrdinalIgnoreCase) >= 0);
                    //   oMIssueCategorys = oMIssueCategorys.Where(p => p.MillName == Convert.ToDecimal(sMill));
                }

            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oMIssueCategorys.Count();

            oMIssueCategorys.OrderBy(p => p.Pk_Material);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oMIssueCategorys = oMIssueCategorys.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oMIssueCategorys.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }
        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Inv_MaterialCategory oNewMaterialCategory = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Inv_MaterialCategory), omValues) as Inv_MaterialCategory;

                _oEntites.AddToInv_MaterialCategory(oNewMaterialCategory);

                _oEntites.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Inv_MaterialCategory oMCategory = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Inv_MaterialCategory), omValues) as Inv_MaterialCategory;
                Inv_MaterialCategory oMCategoryFromShelf = _oEntites.Inv_MaterialCategory.Where(p => p.Pk_MaterialCategory == oMCategory.Pk_MaterialCategory).Single();
                object orefItems = oMCategoryFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(Inv_MaterialCategory), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;

            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Inv_MaterialCategory oMCategory = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Inv_MaterialCategory), omValues) as Inv_MaterialCategory;
                Inv_MaterialCategory oMCategoryFromShelf = _oEntites.Inv_MaterialCategory.Where(p => p.Pk_MaterialCategory == oMCategory.Pk_MaterialCategory).Single();
                object orefItems = oMCategoryFromShelf;
               _oEntites.DeleteObject(oMCategoryFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oMCategory;

            }
            set
            {
                oMCategory = value as Inv_MaterialCategory;
            }
        }

        public decimal ID
        {
            get
            {
                return oMCategory.Pk_MaterialCategory;
            }
            set
            {
                oMCategory = _oEntites.Inv_MaterialCategory.Where(p => p.Pk_MaterialCategory == value).Single();
            }
        }

        public object GetRaw()
        {
            return oMCategory;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}


