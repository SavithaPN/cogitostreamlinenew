﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using Newtonsoft.Json;
using System.Web;

namespace CogitoStreamLineModel.DomainModel
{
    public class QuotationM : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntities = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private QuotationMaster oQuotation = null;

        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 

            //string CheckDate = "", string Vendor = "", string InvoiceNo = "", string MaterialInwardM = "", 

            /* Creating a query object with operator , field Ex Quantity >30 to be used for query later*/
            string sPk_Quotation = null;
            //string sMaterialIndentDate = null;
            string sDate = null;
            string sVendor = null;
            string sFromDate = null;
            string sToDate = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_Quote> oQuotations = null;

            try { sPk_Quotation = p_Params.Single(p => p.ParameterName == "Pk_Quotation").ParameterValue; }
            catch { }
            try { sDate = p_Params.Single(p => p.ParameterName == "QuotationDate").ParameterValue; }
            catch { }
            try { sVendor = p_Params.Single(p => p.ParameterName == "Customer").ParameterValue; }
            catch { }
            try { sFromDate = p_Params.Single(p => p.ParameterName == "FromDate").ParameterValue; }
            catch { }
            try { sToDate = p_Params.Single(p => p.ParameterName == "ToDate").ParameterValue; }
            catch { }

            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oQuotations = _oEntities.Vw_Quote;

            try
            {
                if (!string.IsNullOrEmpty(sPk_Quotation))
                {
                    oQuotations = oQuotations.Where(p => p.Pk_Quotation == decimal.Parse(sPk_Quotation));
                }
                if (!string.IsNullOrEmpty(sDate))
                {
                    oQuotations = oQuotations.Where(p => (p.QuotationDate == Convert.ToDateTime(sDate)));
                }
                if (!string.IsNullOrEmpty(sVendor))
                {
                  
                    oQuotations = oQuotations.Where(p => (p.CustomerName.IndexOf(sVendor, StringComparison.OrdinalIgnoreCase) >= 0));
                }


                if (!string.IsNullOrEmpty(sFromDate))
                {
                    oQuotations = oQuotations.Where(p => (p.QuotationDate >= Convert.ToDateTime(sFromDate)));
                }
                if (!string.IsNullOrEmpty(sToDate))
                {
                    oQuotations = oQuotations.Where(p => (p.QuotationDate <= Convert.ToDateTime(sToDate)));
                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oQuotations.Count();
            oQuotations = oQuotations.OrderByDescending(p => p.Pk_Quotation);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oQuotations = oQuotations.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oQuotations.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;
        }

        public List<EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();
            try
            {
                //if 
                //omValues = new Dictionary<string, object>();
                QuotationMaster oNewQuote = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(QuotationMaster), omValues) as QuotationMaster;
                if (omValues.ContainsKey("deliverySchedules"))
                {
                    string sQuote = omValues["deliverySchedules"].ToString();
                    object[] QuotationS = JsonConvert.DeserializeObject<object[]>(sQuote);

                    foreach (object oSchedule in QuotationS)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oSchedule.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());
                        QuotationDetails oDeliverySchedule = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(QuotationDetails), oDataProperties) as QuotationDetails;
                        //oDeliverySchedule. = Convert.ToDateTime(oDataProperties["Ddate"]);
                        //oDeliverySchedule.Fk_Status = _oEntities.wfStates.Where(p => p.State == "New").Single().Pk_State;

                        oNewQuote.QuotationDetails.Add(oDeliverySchedule);

                    }
                }

                //oNewQuote1.Fk_BoxEstimation = fkEst;
                var dateval = DateTime.Now;
                //oNewQuote.QuotationDate = dateval;

                oNewQuote.QuotationDate = Convert.ToDateTime(dateval.ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                _oEntities.AddToQuotationMaster(oNewQuote);
                _oEntities.SaveChanges();

                oResult.Success = true;
                oResult.Message = oNewQuote.Pk_Quotation.ToString();
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();
            try
            {
                QuotationMaster oNewQuote = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(QuotationMaster), omValues) as QuotationMaster;

                ///////////////////////////////////
                var dateval = DateTime.Now;
                Dictionary<string, object> omValuesChild = new Dictionary<string, object>();

                est_BoxEstimation oEstimateFromShelf = _oEntities.est_BoxEstimation.Where(p => p.Pk_BoxEstimation == 26).Single();

                omValues.Add("Fk_Customer", oEstimateFromShelf.est_BoxEstimationChild.FirstOrDefault().eq_EnquiryChild.eq_Enquiry.Customer);
                omValues.Add("Description", "");
                omValues.Add("QuotationDATE", dateval);
                omValues.Add("GrandTotal", oEstimateFromShelf.GrandTotal);
                omValues.Add("Fk_BoxEstimation", oEstimateFromShelf.Pk_BoxEstimation);

                //QuotationMaster oNewQuote = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(QuotationMaster), omValues) as QuotationMaster;

                foreach (var boxEstChild in oEstimateFromShelf.est_BoxEstimationChild)
                {
                    omValuesChild.Add("Fk_BoxID", boxEstChild.eq_EnquiryChild.Fk_BoxID);
                    omValuesChild.Add("Quantity", boxEstChild.eq_EnquiryChild.Quantity);
                    omValuesChild.Add("Price", boxEstChild.TotalPrice);
                    omValuesChild.Add("Amount", boxEstChild.eq_EnquiryChild.Quantity * boxEstChild.TotalPrice);
                    QuotationDetails oNewDetail = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(QuotationDetails), omValuesChild) as QuotationDetails;
                    oNewQuote.QuotationDetails.Add(oNewDetail);
                    omValuesChild.Clear();
                }

                /////////////////////////////////////////////////////////////
                _oEntities.AddToQuotationMaster(oNewQuote);
                _oEntities.SaveChanges();

                oResult.Success = true;
                oResult.Message = oNewQuote.Pk_Quotation.ToString();
                //QuotationMaster oPOrd = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(QuotationMaster), omValues) as QuotationMaster;
                //QuotationMaster OPOrdFromShelf = _oEntities.QuotationMaster.Where(p => p.Pk_QualityCheck == oPOrd.Pk_QualityCheck).Single();
                //object orefPO = OPOrdFromShelf;
                //WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(QuotationMaster), omValues, ref orefPO);


                //string sMaterials = omValues["Materials"].ToString();
                //List<object> Materials = JsonConvert.DeserializeObject<List<object>>(sMaterials);

                //List<decimal> UpdatedPk = new List<decimal>();
                //List<QualityChild> NewMaterialsList = new List<QualityChild>();
                //foreach (object oItem in Materials)
                //{
                //    Dictionary<string, object> oDataProperties1 = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                //    Dictionary<string, object> oDataProperties2 = JsonConvert.DeserializeObject<Dictionary<string, object>>(oDataProperties1["data"].ToString());

                //    if (oDataProperties2["Pk_QualityChild"] == null || oDataProperties2["Pk_QualityChild"].ToString() == "")
                //    {
                //        QualityChild oNewPOrderD
                //            = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(QualityChild), oDataProperties2) as QualityChild;

                //        NewMaterialsList.Add(oNewPOrderD);


                //    }
                //    else
                //    {
                //        //Handel Update here
                //        decimal dPkPOrdDet = Convert.ToDecimal(oDataProperties2["Pk_QualityChild"]);
                //        decimal dFkMaterial = Convert.ToDecimal(oDataProperties2["Fk_Material"]);
                //        //decimal dQuantity = Convert.ToDecimal(oDataProperties2["Quantity"]);

                //        // gtot = Convert.ToDecimal(gtot) + Convert.ToDecimal(oDataProperties2["Amount"]);


                //        QualityChild oPOrderDetFromShelf = _oEntities.QualityChild.Where(p => p.Pk_QualityChild == dPkPOrdDet && p.Fk_Material == dFkMaterial).Single();

                //        object oPOrderDet
                //                            = _oEntities.QualityChild.Where(p => p.Pk_QualityChild == dPkPOrdDet).First();

                //        _oEntities.DeleteObject(oPOrderDet);


                //        QualityChild oIndentDetails = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(QualityChild), oDataProperties2) as QualityChild;

                //        NewMaterialsList.Add(oIndentDetails);



                //        UpdatedPk.Add(dPkPOrdDet);

                //    }
                //}
                //// Handeling Deleted Records

                //List<QualityChild> oDeletedRecords = OPOrdFromShelf.QualityChild.Where(p => !UpdatedPk.Contains(p.Pk_QualityChild)).ToList();



                ////Add new elements
                //foreach (QualityChild oNewMaterialDetail in NewMaterialsList)
                //{
                //    OPOrdFromShelf.QualityChild.Add(oNewMaterialDetail);
                //}
                //_oEntities.SaveChanges();

                //oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }
        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                //QuotationMaster oQuotation = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(QuotationMaster), omValues) as QuotationMaster;
                //QuotationMaster oQualityCheckFromShelf = _oEntities.QuotationMaster.Where(p => p.Pk_QualityCheck == oQuotation.Pk_QualityCheck).Single();
                //oQualityCheckFromShelf.QualityDocuments.Clear();



                ////int oPendingcnt = _oEntities.PendingTracks.Where(p => p.Fk_Indent == oQualityCheckFromShelf.Fk_IndentNumber && p.Fk_Material == oQualityCheckFromShelf.Fk_Material).Count();
                //PendingTrack oPendingNew = _oEntities.PendingTracks.Where(p => p.Fk_Indent == oQualityCheckFromShelf.Fk_IndentNumber && p.Fk_Material == oQualityCheckFromShelf.Fk_Material).Single();
                //if (oPendingcnt > 0)
                //{
                //    //PendingTrack oPending = new PendingTrack();

                //    oPendingNew.Pending = (Convert.ToDecimal(oPendingNew.Quantity));
                //    oPendingNew.QC_Qty = 0;
                //}


                //_oEntities.DeleteObject(oQualityCheckFromShelf);

                //_oEntities.SaveChanges();
                //oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public EntityObject DAO
        {
            get
            {
                return oQuotation;
            }
            set
            {
                oQuotation = value as QuotationMaster;
            }
        }



        public decimal ID
        {
            get
            {
                return oQuotation.Pk_Quotation;
            }
            //set
            //{
            //    oQuotation.Pk_QualityCheck = value;
            //}
            set
            {
                oQuotation = _oEntities.QuotationMaster.Where(p => p.Pk_Quotation == value).Single();
            }
        }
        //public decimal PendingValue(decimal Fk_Indent, decimal Fk_Material)
        //{
        //    decimal dPendingValue = _oEntities.PendingTracks.Where(p => p.Fk_Indent == Fk_Indent && p.Fk_Material == Fk_Material).First().Pending;

        //    return dPendingValue;
        //}
        public object GetRaw()
        {
            return oQuotation;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntities.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}
