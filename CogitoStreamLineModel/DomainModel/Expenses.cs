﻿//Name          : Expenses Class
//Description   : Contains the Expenses class definition, every page in the application will be instance of this class with following
//                 1. Communication 2. Data binding 3. Loading preferences
//Author        : Nagesh V Rao
//Date 	        : 07/08/2015
//Crh Number    : SL0012
//Modifications : 





using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Objects.DataClasses;
using WebGareCore;
using WebGareCore.CommonObjects;
using System.Data;
using CogitoStreamLineModel;

namespace CogitoStreamLineModel.DomainModel
{
    public class Expenses : IDomainObject
    {
        //readonly WebGareCoreEntities2  _oEntites = new WebGareCoreEntities2();
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();

        private Dictionary<string, object> omValues = null;
        private Voucher oVoucher = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_ExpensesID = null;
            string sAmount = null;
            string sDescription = null;

            string sAccountHeadId = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Voucher> oVouchers = null;

            try { sPk_ExpensesID = p_Params.Single(p => p.ParameterName == "Pk_ExpensesID").ParameterValue; }
            catch { }
            try { sAmount = p_Params.Single(p => p.ParameterName == "Amount").ParameterValue; }
            catch { }
            try { sDescription = p_Params.Single(p => p.ParameterName == "Description").ParameterValue; }
            catch { }
           
            try { sAccountHeadId = p_Params.Single(p => p.ParameterName == "AccountHeadId").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oVouchers = _oEntites.Voucher;
            try
            {
                if (!string.IsNullOrEmpty(sPk_ExpensesID))
                {
                    oVouchers = oVouchers.Where(p => p.Pk_ExpensesID == decimal.Parse(sPk_ExpensesID));
                }
               
                if (!string.IsNullOrEmpty(sAmount))
                {
                    oVouchers = oVouchers.Where(p => p.Amount == decimal.Parse(sAmount));
                }
                if (!string.IsNullOrEmpty(sDescription))
                {
                    //oCitys = oCitys.Where(p => p.CityName.IndexOf(sCityName, StringComparison.OrdinalIgnoreCase) >= 0);
                    oVouchers = oVouchers.Where(p => p.Description.ToLower() == (sDescription.ToLower().ToString().Trim()));

                }

                if (!string.IsNullOrEmpty(sAccountHeadId))
                {
                    oVouchers = oVouchers.Where(p => p.Fk_AccountHeadId == decimal.Parse(sAccountHeadId));
                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oVouchers.Count();
            oVouchers.OrderBy(p => p.Pk_ExpensesID);
            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oVouchers = oVouchers.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oVouchers.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;
            return oSearchResult;
        }

        public SearchResult SearchVoucherName(List<SearchParameter> p_Params)
        {

            SearchResult oSearchResult = new SearchResult();
            IEnumerable<Voucher> oVoucherName = null;
            oVoucherName = _oEntites.Voucher;

            oSearchResult.RecordCount = oVoucherName.Count();
            oVoucherName.OrderBy(p => p.Pk_ExpensesID);

            List<EntityObject> oFilteredItem = oVoucherName.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItem;

            return oSearchResult;
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Voucher oNewVouchers = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Voucher), omValues) as Voucher;

                _oEntites.AddToVoucher(oNewVouchers);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Voucher oVouchers = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Voucher), omValues) as Voucher;
                Voucher oVouchersFromShelf = _oEntites.Voucher.Where(p => p.Pk_ExpensesID == oVouchers.Pk_ExpensesID).Single();
                object orefItems = oVouchersFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(Voucher), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Voucher oVouchers = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Voucher), omValues) as Voucher;
                Voucher oVouchersFromShelf = _oEntites.Voucher.Where(p => p.Pk_ExpensesID == oVouchers.Pk_ExpensesID).Single();
                object orefItems = oVouchersFromShelf;
                _oEntites.DeleteObject(oVouchersFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oVoucher;

            }
            set
            {
                oVoucher = value as Voucher;
            }
        }

        public decimal ID
        {
            get
            {
                return oVoucher.Pk_ExpensesID;
            }
            set
            {
                oVoucher = _oEntites.Voucher.Where(p => p.Pk_ExpensesID == value).Single();
            }
        }

        public object GetRaw()
        {
            return oVoucher;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}




