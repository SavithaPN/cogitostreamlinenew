﻿//Name          : City Class
//Description   : Contains the Country class definition, every page in the application will be instance of this class with following
//                 1. Communication 2. Data binding 3. Loading preferences
//Author        : Nagesh V Rao
//Date 	        : 07/08/2015
//Crh Number    : SL0012
//Modifications : 





using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Objects.DataClasses;
using WebGareCore;
using WebGareCore.CommonObjects;
using System.Data;
using CogitoStreamLineModel;

namespace CogitoStreamLineModel.DomainModel
{
    public class PayTrack : IDomainObject
    {
        //readonly WebGareCoreEntities2  _oEntites = new WebGareCoreEntities2();
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();

        private Dictionary<string, object> omValues = null;
        private PaymentTrack oPayment = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_PaymtID = null;
            string sPaymt_Date = null;
            string sFk_Invno = null;
            string sChequeNo = null;
            string sBankName = null;
            string sTransactionID = null;
            string sPaidAmt = null;
            string sBalanceAmt = null;
            string sBillAmt = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<PaymentTrack> oPayments = null;

            try { sPk_PaymtID = p_Params.Single(p => p.ParameterName == "Pk_PaymtID").ParameterValue; }
            catch { }
            try { sPaymt_Date = p_Params.Single(p => p.ParameterName == "Paymt_Date").ParameterValue; }
            catch { }
            try { sFk_Invno = p_Params.Single(p => p.ParameterName == "Fk_Invno").ParameterValue; }
            catch { }
            try { sChequeNo = p_Params.Single(p => p.ParameterName == "ChequeNo").ParameterValue; }
            catch { }
            try { sBankName = p_Params.Single(p => p.ParameterName == "BankName").ParameterValue; }
            catch { }
            try { sTransactionID = p_Params.Single(p => p.ParameterName == "TransactionID").ParameterValue; }
            catch { }
            try { sPaidAmt = p_Params.Single(p => p.ParameterName == "PaidAmt").ParameterValue; }
            catch { }
            try { sBalanceAmt = p_Params.Single(p => p.ParameterName == "BalanceAmt").ParameterValue; }
            catch { }
            try { sBillAmt = p_Params.Single(p => p.ParameterName == "BillAmt").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }

            oPayments = _oEntites.PaymentTracks;
            try
            {
                if (!string.IsNullOrEmpty(sPk_PaymtID))
                {
                    oPayments = oPayments.Where(p => p.Pk_PaymtID == decimal.Parse(sPk_PaymtID));
                }
                if (!string.IsNullOrEmpty(sPaymt_Date))
                {
                    oPayments = oPayments.Where(p => (p.Paymt_Date == Convert.ToDateTime(sPaymt_Date)));
                }
                if (!string.IsNullOrEmpty(sFk_Invno))
                {
                    oPayments = oPayments.Where(p => p.Fk_Invno == decimal.Parse(sFk_Invno));
                }
                if (!string.IsNullOrEmpty(sChequeNo))
                {

                    oPayments = oPayments.Where(p => p.ChequeNo.ToLower() == (sChequeNo.ToLower().ToString().Trim()));

                }
                if (!string.IsNullOrEmpty(sBankName))
                {
                    //oCitys = oCitys.Where(p => p.CityName.IndexOf(sCityName, StringComparison.OrdinalIgnoreCase) >= 0);
                    oPayments = oPayments.Where(p => p.BankName.ToLower() == (sBankName.ToLower().ToString().Trim()));

                }
                if (!string.IsNullOrEmpty(sTransactionID))
                {
                    //oCitys = oCitys.Where(p => p.CityName.IndexOf(sCityName, StringComparison.OrdinalIgnoreCase) >= 0);
                    oPayments = oPayments.Where(p => p.TransactionID.ToLower() == (sTransactionID.ToLower().ToString().Trim()));

                }
                if (!string.IsNullOrEmpty(sPaidAmt))
                {
                    oPayments = oPayments.Where(p => p.PaidAmt == decimal.Parse(sPaidAmt));
                }
                if (!string.IsNullOrEmpty(sBalanceAmt))
                {
                    oPayments = oPayments.Where(p => p.BalanceAmt == decimal.Parse(sBalanceAmt));
                }
                if (!string.IsNullOrEmpty(sBillAmt))
                {
                    oPayments = oPayments.Where(p => p.BillAmt == decimal.Parse(sBillAmt));
                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oPayments.Count();
            oPayments = oPayments.OrderByDescending(p => p.Pk_PaymtID);
            
           // oPayments.OrderBy(p => p.Pk_PaymtID);
            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oPayments = oPayments.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oPayments.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;
            return oSearchResult;
        }

        public SearchResult SearchPaymentName(List<SearchParameter> p_Params)
        {

            SearchResult oSearchResult = new SearchResult();
            IEnumerable<PaymentTrack> oPayName = null;
            oPayName = _oEntites.PaymentTracks;

            oSearchResult.RecordCount = oPayName.Count();
            oPayName.OrderBy(p => p.Pk_PaymtID);

            List<EntityObject> oFilteredItem = oPayName.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItem;

            return oSearchResult;
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                PaymentTrack oNewPays = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(PaymentTrack), omValues) as PaymentTrack;

                _oEntites.AddToPaymentTracks(oNewPays);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                PaymentTrack oPayments = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(PaymentTrack), omValues) as PaymentTrack;
                PaymentTrack oPaysFromShelf = _oEntites.PaymentTracks.Where(p => p.Pk_PaymtID == oPayments.Pk_PaymtID).Single();
                object orefItems = oPaysFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(PaymentTrack), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                PaymentTrack oPayments = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(PaymentTrack), omValues) as PaymentTrack;
                PaymentTrack oPaysFromShelf = _oEntites.PaymentTracks.Where(p => p.Pk_PaymtID == oPayments.Pk_PaymtID).Single();
                object orefItems = oPaysFromShelf;
                _oEntites.DeleteObject(oPaysFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oPayment;

            }
            set
            {
                oPayment = value as PaymentTrack;
            }
        }

        public decimal ID
        {
            get
            {
                return oPayment.Pk_PaymtID;
            }
            set
            {
                oPayment = _oEntites.PaymentTracks.Where(p => p.Pk_PaymtID == value).Single();
            }
        }

        public object GetRaw()
        {
            return oPayment;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}




