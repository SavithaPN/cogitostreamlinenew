﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//namespace CogitoStreamLineModel.DomainModel
//{
//    class Work_Order
//    {
//    }
//}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using WebGareCore;
using Newtonsoft.Json;
using System.Web;

namespace CogitoStreamLineModel.DomainModel
{
    public class Work_Order : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private WorkOrder oJob = null;

        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for Order based on 
            //First name, Last name and OrderName
            string sCustomer = null;
            string sBoxName = null;
            string sFromJDate = null;
            string sToJDate = null;
            string sCustomerOrder = null;
            string sFk_Order = null;
            string sInvNo = null;
            string sOnlyPending = null;
            string sBoxID = null;
            string sPkJobcardID = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;

            IEnumerable<WorkOrder> oJobCard = null;

            try { sCustomer = p_Params.Single(p => p.ParameterName == "Customer").ParameterValue; }
            catch { }
            try { sBoxID = p_Params.Single(p => p.ParameterName == "BoxID").ParameterValue; }
            catch { }
            try { sPkJobcardID = p_Params.Single(p => p.ParameterName == "WorkOrder_ID").ParameterValue; }
            catch { }

            try { sBoxName = p_Params.Single(p => p.ParameterName == "BoxName").ParameterValue; }
            catch { }
            try { sFk_Order = p_Params.Single(p => p.ParameterName == "Fk_Order").ParameterValue; }
            catch { }
            try { sFromJDate = p_Params.Single(p => p.ParameterName == "FromJDate").ParameterValue; }
            catch { }
            try { sToJDate = p_Params.Single(p => p.ParameterName == "ToJDate").ParameterValue; }
            catch { }

            try { sCustomerOrder = p_Params.Single(p => p.ParameterName == "CustomerOrder").ParameterValue; }
            catch { }
            try { sInvNo = p_Params.Single(p => p.ParameterName == "InvNo").ParameterValue; }
            catch { }
            try { sOnlyPending = p_Params.Single(p => p.ParameterName == "OnlyPending").ParameterValue; }
            catch { }

            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oJobCard = _oEntites.WorkOrder;

            try
            {
                //if (!string.IsNullOrEmpty(sCustomer))
                //{

                //    //oJobCard = oJobCard.Where(p => p.BoxMaster.gen_Customer.CustomerName.IndexOf(sCustomer, StringComparison.OrdinalIgnoreCase) >= 0);
                //    oJobCard = oJobCard.Where(p => p.gen_Order.gen_Customer.CustomerName.IndexOf(sCustomer, StringComparison.OrdinalIgnoreCase) >= 0);
                //}
                if (!string.IsNullOrEmpty(sBoxID))
                {

                    oJobCard = oJobCard.Where(p => p.Fk_BoxID == Convert.ToDecimal(sBoxID));
                }
                if (!string.IsNullOrEmpty(sBoxName))
                {

                    oJobCard = oJobCard.Where(p => p.BoxMaster.Name.IndexOf(sBoxName, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                //if (!string.IsNullOrEmpty(sCustomerOrder))
                //{
                //    oJobCard = oJobCard.Where(p => p.gen_Order.Pk_Order == Convert.ToDecimal(sCustomerOrder));
                //}
                if (!string.IsNullOrEmpty(sPkJobcardID))
                {
                    oJobCard = oJobCard.Where(p => p.WorkOrder_ID == decimal.Parse(sPkJobcardID));
                }
                //if (!string.IsNullOrEmpty(sFk_Order))
                //{
                //    oJobCard = oJobCard.Where(p => p.gen_Order.Pk_Order == Convert.ToDecimal(sFk_Order));
                //}
                if (!string.IsNullOrEmpty(sInvNo))
                {
                    oJobCard = oJobCard.Where(p => p.Invno.Trim().ToUpper() == sInvNo.Trim().ToUpper());
                }

                if (!string.IsNullOrEmpty(sFromJDate))
                {
                    DateTime SFrom = Convert.ToDateTime(sFromJDate.ToString(),
                        System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                    oJobCard = oJobCard.Where(p => (p.WODate >= Convert.ToDateTime(SFrom)));
                }
                if (!string.IsNullOrEmpty(sToJDate))
                {
                    DateTime STo = Convert.ToDateTime(sToJDate.ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                    oJobCard = oJobCard.Where(p => (p.WODate<= Convert.ToDateTime(STo)));
                }
                if (!string.IsNullOrEmpty(sOnlyPending))
                {

                    if (sOnlyPending == "InProgress")
                    {
                        var RCount = _oEntites.WorkOrder.Where(p => p.Fk_Status != 1 && p.Fk_Status != 11 && p.Fk_Status != 4).Count();

                        if (RCount > 0)
                        {

                            oJobCard = _oEntites.WorkOrder.Where(p => p.Fk_Status != 1 && p.Fk_Status != 11 && p.Fk_Status != 4);
                        }
                    }
                    else
                    { oJobCard = oJobCard.Where(p => p.wfStates.State.IndexOf(sOnlyPending, StringComparison.OrdinalIgnoreCase) >= 0); }

                }



            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oJobCard.Count();
            oJobCard = oJobCard.OrderByDescending(p => p.WorkOrder_ID);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oJobCard = oJobCard.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredOrders = oJobCard.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredOrders;

            return oSearchResult;

        }
        public SearchResult SearchPaperStock(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            string sFk_BoxID = null;
            string sFk_Enquiry = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_BoxPaperStock> oPaperStock = null;

            try { sFk_BoxID = p_Params.Single(p => p.ParameterName == "Fk_BoxID").ParameterValue; }
            catch { }
            try { sFk_Enquiry = p_Params.Single(p => p.ParameterName == "Fk_Enquiry").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }

            oPaperStock = _oEntites.Vw_BoxPaperStock;

            try
            {
                if (!string.IsNullOrEmpty(sFk_BoxID))
                {
                    oPaperStock = oPaperStock.Where(p => p.Fk_BoxID == decimal.Parse(sFk_BoxID));
                }
                //if (!string.IsNullOrEmpty(sFk_Enquiry))
                //{
                //    oPaperStock = oPaperStock.Where(p => p.Fk_Enquiry == decimal.Parse(sFk_Enquiry));
                //}
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oPaperStock.Count();
            oPaperStock = oPaperStock.OrderByDescending(p => p.Pk_Material);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oPaperStock = oPaperStock.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oPaperStock.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;
        }
        public SearchResult SearchBoxPaper(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            string sFk_BoxID = null;
            string sFk_Enquiry = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_BoxPaper> oPaperStock = null;

            try { sFk_BoxID = p_Params.Single(p => p.ParameterName == "Fk_BoxID").ParameterValue; }
            catch { }
            try { sFk_Enquiry = p_Params.Single(p => p.ParameterName == "Fk_Enquiry").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }

            oPaperStock = _oEntites.Vw_BoxPaper;

            try
            {
                if (!string.IsNullOrEmpty(sFk_BoxID))
                {
                    oPaperStock = oPaperStock.Where(p => p.Pk_BoxID == decimal.Parse(sFk_BoxID));
                }
                //if (!string.IsNullOrEmpty(sFk_Enquiry))
                //{
                //    oPaperStock = oPaperStock.Where(p => p.Fk_Enquiry == decimal.Parse(sFk_Enquiry));
                //}
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oPaperStock.Count();
            oPaperStock = oPaperStock.OrderByDescending(p => p.Pk_Material);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oPaperStock = oPaperStock.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oPaperStock.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;
        }

        public SearchResult SearchJCPaperStock(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            string sPk_PaperStock = null;
            string sFk_Material = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<PaperStock> oPaperStock = null;

            try { sPk_PaperStock = p_Params.Single(p => p.ParameterName == "Pk_PaperStock").ParameterValue; }
            catch { }
            try { sFk_Material = p_Params.Single(p => p.ParameterName == "Fk_Material").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }

            oPaperStock = _oEntites.PaperStock;

            try
            {
                if (!string.IsNullOrEmpty(sPk_PaperStock))
                {
                    oPaperStock = oPaperStock.Where(p => p.Pk_PaperStock == decimal.Parse(sPk_PaperStock));
                }
                if (!string.IsNullOrEmpty(sFk_Material))
                {
                    oPaperStock = oPaperStock.Where(p => p.Fk_Material == decimal.Parse(sFk_Material));
                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oPaperStock.Count();
            oPaperStock = oPaperStock.OrderByDescending(p => p.Pk_PaperStock);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oPaperStock = oPaperStock.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oPaperStock.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;
        }
        public SearchResult SearchAssignedStock(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //string sPk_PaperStock = null;
            string sPk_Material = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_PStock> oPaperStock = null;

            //try { sPk_PaperStock = p_Params.Single(p => p.ParameterName == "Pk_PaperStock").ParameterValue; }
            //catch { }
            try { sPk_Material = p_Params.Single(p => p.ParameterName == "Pk_Material").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }

            oPaperStock = _oEntites.Vw_PStock;

            try
            {

                if (!string.IsNullOrEmpty(sPk_Material))
                {
                    oPaperStock = oPaperStock.Where(p => p.Pk_Material == decimal.Parse(sPk_Material));
                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oPaperStock.Count();
            oPaperStock = oPaperStock.OrderByDescending(p => p.Pk_Material);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oPaperStock = oPaperStock.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oPaperStock.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;
        }
        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            // decimal GTotal = 0;

            ModelManuplationResult oResult = new ModelManuplationResult();
            try
            {
                //if (!omValues.ContainsKey("WODate"))
                //    omValues.Add("WODate", DateTime.Now);

                omValues.Add("Fk_Status", 1);
                //omValues.Add("ChkQuality", 0);


                WorkOrder oNewJob = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(WorkOrder), omValues) as WorkOrder;
                Dictionary<string, object> omValuesJProcess = new Dictionary<string, object>();
                decimal? CPly = 0;
                var BoxID = Convert.ToDecimal(omValues["Fk_BoxID"]);
                int RCount1 = _oEntites.Vw_BoxDet.Where(p => p.Pk_BoxID == BoxID).Count();
                if (RCount1 > 0)
                {
                    CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                    Vw_BoxDet PlyVal = _oEntites.Vw_BoxDet.Where(p => p.Pk_BoxID == BoxID).First();

                    CPly = PlyVal.PlyVal;
                    if (CPly == 3)
                    {
                        CPly = 1;
                    }
                    if (CPly == 5)
                    {
                        CPly = 2;
                    }
                    if (CPly == 7)
                    {
                        CPly = 3;
                    }
                    if (CPly == 9)
                    {
                        CPly = 4;
                    }
                    if (CPly == 11)
                    {
                        CPly = 5;
                    }
                }


                string ProcessID = "";
                if (omValues.ContainsKey("Documents"))
                {
                    string sDocs = omValues["Documents"].ToString();
                    if (sDocs != "")
                    {
                        object[] Docs = JsonConvert.DeserializeObject<object[]>(sDocs);

                        foreach (object oItem in Docs)
                        {
                            WO_Documents oDocument = new WO_Documents();
                            Dictionary<string, string> oProperties = JsonConvert.DeserializeObject<Dictionary<string, string>>(oItem.ToString());

                            oDocument.FileName = oProperties["FileName"];
                            oDocument.FileSize = oProperties["FileSize"];

                            oNewJob.WO_Documents.Add(oDocument);
                        }
                    }
                }

                if (omValues.ContainsKey("Corr"))
                {
                    oNewJob.Corr = "Yes";
                    string pName = "Corrugation";

                    ProcessMaster oPMast = _oEntites.ProcessMaster.Where(p => p.ProcessName == pName).Single();
                    // omValuesJProcess.Add("Fk_ProcessID", oPMast.Pk_Process);
                    // omValuesJProcess.Clear(); 

                    ProcessID = ProcessID + "`" + oPMast.Pk_Process;
                }
                else
                {
                    oNewJob.Corr = "No";
                }


                if (omValues.ContainsKey("TopP"))
                {
                    oNewJob.TopP = "Yes";
                    string pName = "TopPaper";
                    ProcessMaster oPMast = _oEntites.ProcessMaster.Where(p => p.ProcessName == pName).Single();
                    //omValuesJProcess.Add("Fk_ProcessID", oPMast.Pk_Process);

                    //omValuesJProcess.Clear(); 
                    ProcessID = ProcessID + "`" + oPMast.Pk_Process;
                }
                else
                {
                    oNewJob.TopP = "No";
                }


                if (omValues.ContainsKey("Pasting"))
                {

                    oNewJob.Pasting = "Yes";
                    string pName = "Pasting";
                    ProcessMaster oPMast = _oEntites.ProcessMaster.Where(p => p.ProcessName == pName).Single();
                    // omValuesJProcess.Add("Fk_ProcessID", oPMast.Pk_Process);
                    ProcessID = ProcessID + "`" + oPMast.Pk_Process;
                }

                else
                {
                    oNewJob.Pasting = "No";
                }

                if (omValues.ContainsKey("Rotary"))
                {
                    oNewJob.Rotary = "Yes";
                    string pName = "Rotary";
                    ProcessMaster oPMast = _oEntites.ProcessMaster.Where(p => p.ProcessName == pName).Single();
                    ProcessID = ProcessID + "`" + oPMast.Pk_Process;
                }
                else
                {
                    oNewJob.Rotary = "No";
                }


                if (omValues.ContainsKey("PrintingP"))
                {
                    oNewJob.PrintingP = "Yes";
                    string pName = "Printing";
                    ProcessMaster oPMast = _oEntites.ProcessMaster.Where(p => p.ProcessName == pName).Single();
                    ProcessID = ProcessID + "`" + oPMast.Pk_Process;
                }

                else
                {
                    oNewJob.PrintingP = "No";
                }

                if (omValues.ContainsKey("Punching"))
                {
                    oNewJob.Punching = "Yes";
                    string pName = "Punching";
                    ProcessMaster oPMast = _oEntites.ProcessMaster.Where(p => p.ProcessName == pName).Single();
                    ProcessID = ProcessID + "`" + oPMast.Pk_Process;
                }
                else
                {
                    oNewJob.Punching = "No";
                }



                if (omValues.ContainsKey("Slotting"))
                {
                    oNewJob.Slotting = "Yes";
                    string pName = "Slotting";
                    ProcessMaster oPMast = _oEntites.ProcessMaster.Where(p => p.ProcessName == pName).Single();
                    ProcessID = ProcessID + "`" + oPMast.Pk_Process;
                }
                else
                {
                    oNewJob.Slotting = "No";
                }


                if (omValues.ContainsKey("Pinning"))
                {
                    oNewJob.Pinning = "Yes";
                    string pName = "Pinning";
                    ProcessMaster oPMast = _oEntites.ProcessMaster.Where(p => p.ProcessName == pName).Single();
                    ProcessID = ProcessID + "`" + oPMast.Pk_Process;
                }
                else
                { oNewJob.Pinning = "No"; }

                if (omValues.ContainsKey("Gumming"))
                {
                    oNewJob.Gumming = "Yes";
                    string pName = "Gumming";
                    ProcessMaster oPMast = _oEntites.ProcessMaster.Where(p => p.ProcessName == pName).Single();
                    ProcessID = ProcessID + "`" + oPMast.Pk_Process;
                }
                else
                { oNewJob.Gumming = "No"; }


                if (omValues.ContainsKey("Bundling"))
                {
                    oNewJob.Bundling = "Yes";
                    string pName = "Bundling";
                    ProcessMaster oPMast = _oEntites.ProcessMaster.Where(p => p.ProcessName == pName).Single();
                    ProcessID = ProcessID + "`" + oPMast.Pk_Process;
                }
                else
                { oNewJob.Bundling = "No"; }

                if (omValues.ContainsKey("Finishing"))
                {
                    oNewJob.Finishing = "Yes";
                    string pName = "Finishing";
                    ProcessMaster oPMast = _oEntites.ProcessMaster.Where(p => p.ProcessName == pName).Single();
                    ProcessID = ProcessID + "`" + oPMast.Pk_Process;
                }
                else
                {
                    oNewJob.Finishing = "Yes";
                    string pName = "Finishing";
                    ProcessMaster oPMast = _oEntites.ProcessMaster.Where(p => p.ProcessName == pName).Single();
                    ProcessID = ProcessID + "`" + oPMast.Pk_Process;
                }

                object[] SelVal = ProcessID.Split('`');

                int i = 0;


                foreach (object oItem in SelVal)
                {

                    //JProcess oJobProcess = new JProcess();
                    //if (i > 0)
                    //{
                    //    decimal SVal = Convert.ToDecimal(SelVal[i]);
                    //    if (SVal > 0)
                    //    {
                    //        int j = 0;
                    //        var CtrVal = 0;
                    //        if (SVal == 1)
                    //        {
                    //            for (j = 13; j <= 17; j++)
                    //            {
                    //                if (CtrVal < CPly)
                    //                {
                    //                    JProcess oJobProcess1 = new JProcess();
                    //                    oJobProcess1.Fk_ProcessID = j;
                    //                    oNewJob.JProcess.Add(oJobProcess1);
                    //                }
                    //                CtrVal = CtrVal + 1;

                    //            }
                    //            i = i + 1;
                    //        }
                    //        ///////////////////////////////////////////////////  

                    //        else if (SVal == 3)
                    //        {
                    //            oJobProcess.Fk_ProcessID = Convert.ToDecimal(SelVal[i]);
                    //            int k = 0;
                    //            var CtrVal1 = 0;
                    //            for (k = 18; k <= 24; k++)
                    //            {
                    //                if (CtrVal1 < CPly)
                    //                {
                    //                    JProcess oJobProcess1 = new JProcess();
                    //                    oJobProcess1.Fk_ProcessID = k;
                    //                    oNewJob.JProcess.Add(oJobProcess1);
                    //                }
                    //                CtrVal1 = CtrVal1 + 1;
                    //            }
                    //            i = i + 1;
                    //        }

                    //        else
                    //        {
                    //            oJobProcess.Fk_ProcessID = Convert.ToDecimal(SelVal[i]);

                    //            oNewJob.JProcess.Add(oJobProcess);
                    //            i = i + 1;
                    //        }

                    //    }
                    //}
                    //else
                    //{ i = i + 1; }
                }

                if (omValues.ContainsKey("JobDetails"))
                {
                    string sJobDetails = omValues["JobDetails"].ToString();
                    object[] JobDetails = JsonConvert.DeserializeObject<object[]>(sJobDetails);


                    foreach (object oDtetails in JobDetails)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oDtetails.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());
                        WorkOrderDetails oJobDetails = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(WorkOrderDetails), oDataProperties) as WorkOrderDetails;
                        //if (oDataProperties["Name"].ToString().Length > 0)
                        //{
                           oJobDetails.RM_Consumed = 0;
                        //}      ///Assigned Stock
                        oNewJob.WorkOrderDetails.Add(oJobDetails);
                    }
                }

             
                //if (omValues.ContainsKey("Process"))
                //{
                //    string sEnquiryChilds = omValues["Process"].ToString();
                //    object[] EnquiryChilds = JsonConvert.DeserializeObject<object[]>(sEnquiryChilds);

                //    foreach (object oEqChild in EnquiryChilds)
                //    {
                //        JobProcess oJCProcess = new JobProcess();
                //        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oEqChild.ToString());
                //        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());
                //        JobProcess oEnquiryChild = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(JobProcess), oDataProperties) as JobProcess;
                //        //var BCode = oDataProperties[""].ToString();
                //        //oEnquiryChild.TagNumber = oDataProperties["Barcode"].ToString();

                //        //PriceList oPC = _oEntites.PriceList.Where(p => p.PLBarcode == BCode).Single();
                //        //var PID = oPC.Pk_ID;
                //        //ProductM oJC = _oEntites.ProductM.Where(p => p.Fk_Gen == PID).Single();
                //        //oEnquiryChild.Fk_FrameID = oJC.Pk_PrdID;
                //        // oJCProcess.Fk_JobCardID
                //        oNewJob.JobProcess.Add(oEnquiryChild);
                //    }
                //}

                oNewJob.StartTime = "";
                oNewJob.EndTime = "";


                // var dateVal = DateTime.Now;
                string sJCard = omValues["JobCard"].ToString();
                oNewJob.Fk_JobCardID = Convert.ToInt64(sJCard);
              
                oNewJob.WODate = Convert.ToDateTime(omValues["WODate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);


                _oEntites.AddToWorkOrder(oNewJob);


                _oEntites.SaveChanges();

                oResult.Success = true;
                oResult.Message = " JC No.-" + oNewJob.WorkOrder_ID.ToString() + " -ItemName- " + oNewJob.BoxMaster.Name + " -Prdn Qty- " + oNewJob.TotalQty ;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();
            CogitoStreamLineEntities dc = new CogitoStreamLineEntities();
            try
            {
                decimal dFk_BoxID = decimal.Parse(omValues["Fk_BoxID"].ToString());
                decimal dFk_Order = decimal.Parse(omValues["Fk_Order"].ToString());
                decimal dSchID = decimal.Parse(omValues["Fk_Schedule"].ToString());





                //decimal oPk_JobCard =  decimal.Parse(omValues["WorkOrder_ID"].ToString());

                gen_DeliverySchedule oDelSch = _oEntites.gen_DeliverySchedule.Where(p => p.Pk_DeliverySechedule == dSchID).SingleOrDefault();

                decimal dFk_PartID = Convert.ToDecimal(oDelSch.Fk_PartID);

                //BoxStock oBoxStock = _oEntites.BoxStock.Where(p => p.Fk_BoxID == dFk_BoxID && p.Fk_PartID == dFk_PartID).SingleOrDefault();

                Gen_OrderChild oGenChild = _oEntites.Gen_OrderChild.Where(p => p.Fk_BoxID == dFk_BoxID && p.Fk_OrderID == dFk_Order && p.Fk_PartID == dFk_PartID).SingleOrDefault();


                WorkOrder oNewJob = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(WorkOrder), omValues) as WorkOrder;
                WorkOrder oNewJobFromShelf = _oEntites.WorkOrder.Where(p => p.WorkOrder_ID == oNewJob.WorkOrder_ID).Single();


                var JCID = oNewJob.WorkOrder_ID;
                oNewJobFromShelf.WorkOrder_ID = JCID;
                oNewJobFromShelf.WODate = Convert.ToDateTime(omValues["WODate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                oNewJobFromShelf.WO_Documents.Clear();
                if (omValues.ContainsKey("Documents"))
                {
                    string sDocs = omValues["Documents"].ToString();
                    object[] Docs = JsonConvert.DeserializeObject<object[]>(sDocs);

                    foreach (object oItem in Docs)
                    {
                        WO_Documents oDocument = new WO_Documents();
                        Dictionary<string, string> oProperties = JsonConvert.DeserializeObject<Dictionary<string, string>>(oItem.ToString());
                        //oDocument.Img1 = oProperties["FileName"];
                        oDocument.FileName = oProperties["FileName"];
                        oDocument.FileSize = oProperties["FileSize"];
                        //string FN = oProperties["FileName"];
                        //var FN = oProperties["FileName"];
                        //oDocument.ImgUrl1 = HttpContext.Current.Server.MapPath(FN);
                        oNewJobFromShelf.WO_Documents.Add(oDocument);
                    }
                }


                object orefJobFromShelf = oNewJobFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(WorkOrder), omValues, ref orefJobFromShelf);
                List<decimal> oUpdateList = new List<decimal>();

                var JCStat = omValues["Fk_Status"].ToString();



                if (omValues.ContainsKey("JobDetails"))
                {
                    string sJobDetail = omValues["JobDetails"].ToString();
                    object[] sJobDetails = JsonConvert.DeserializeObject<object[]>(sJobDetail);
                    List<decimal> UpdatedPk = new List<decimal>();
                    List<WorkOrderDetails> oJobDetList = new List<WorkOrderDetails>();

                    foreach (object oIndent in sJobDetails)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oIndent.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());

                        if (oDataProperties.ContainsKey("Pk_WorkOrderDet") && oDataProperties["Pk_WorkOrderDet"] != null && oDataProperties["Pk_WorkOrderDet"].ToString() != "")
                        {
                            decimal Pk_WorkOrderDet = decimal.Parse(oDataProperties["Pk_WorkOrderDet"].ToString());

                            WorkOrderDetails oInvDet = _oEntites.WorkOrderDetails.Where(p => p.Pk_WorkOrderDet == Pk_WorkOrderDet).Single();

                            object orefSchedule = oInvDet;
                            WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(WorkOrderDetails), oDataProperties, ref orefSchedule);
                            UpdatedPk.Add(Pk_WorkOrderDet);

                        }
                        else
                        {
                            WorkOrderDetails oInvDets = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(WorkOrderDetails), oDataProperties) as WorkOrderDetails;
                            // oInvDets.RequiredDate = null;
                            oJobDetList.Add(oInvDets);
                        }
                    }
                    if (JCStat == "11")
                    {
                        List<WorkOrderDetails> oDeletedRecords = oNewJobFromShelf.WorkOrderDetails.Where(p => UpdatedPk.Contains(p.Pk_WorkOrderDet)).ToList();

                        foreach (WorkOrderDetails oDeletedDetail in oDeletedRecords)
                        {
                            oNewJobFromShelf.WorkOrderDetails.Remove(oDeletedDetail);
                        }
                    }

                    List<WorkOrderDetails> oDeletedRecords1 = oNewJobFromShelf.WorkOrderDetails.Where(p => !UpdatedPk.Contains(p.Pk_WorkOrderDet)).ToList();

                    foreach (WorkOrderDetails oDeletedDetail in oDeletedRecords1)
                    {
                        oNewJobFromShelf.WorkOrderDetails.Remove(oDeletedDetail);
                    }

                    //Add new elements
                    foreach (WorkOrderDetails oNewIndentDetail in oJobDetList)
                    {
                        oNewJobFromShelf.WorkOrderDetails.Add(oNewIndentDetail);
                    }

                }
                ////////////////////job processess
                if (omValues.ContainsKey("Process"))
                {
                    string sJobDetail = omValues["Process"].ToString();
                    object[] sJobDetails = JsonConvert.DeserializeObject<object[]>(sJobDetail);
                    List<decimal> UpdatedPk = new List<decimal>();
                    List<JobProcess> oJobDetList = new List<JobProcess>();

                    foreach (object oIndent in sJobDetails)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oIndent.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());

                        if (oDataProperties.ContainsKey("Pk_ID") && oDataProperties["Pk_ID"] != null && oDataProperties["Pk_ID"].ToString() != "")
                        {
                            decimal Pk_ID = decimal.Parse(oDataProperties["Pk_ID"].ToString());

                            JobProcess oInvDet = _oEntites.JobProcess.Where(p => p.Pk_ID == Pk_ID).Single();

                            oInvDet.ProcessDate = Convert.ToDateTime(oDataProperties["ProcessDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);


                            oInvDet.Fk_JobCardID = JCID;

                            Semi_FinishedGoodsStock oSFG = new Semi_FinishedGoodsStock();
                            if (JCStat == "4")
                            {
                                int PCount = _oEntites.JobProcess.Where(p => p.Fk_ProcessID == oInvDet.Fk_ProcessID && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Count();


                                if (PCount == 1)
                                {
                                    if (oInvDet.Fk_ProcessID == 2)   //top paper
                                    {
                                        int RCount1 = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_MatCatID == 12 && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Count();
                                        if (RCount1 > 0)
                                        {
                                            Semi_FinishedGoodsStock ExStock = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_MatCatID == 12 && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Single();

                                            ExStock.Stock = oInvDet.RemainingQty + ExStock.Stock;

                                        }

                                        else
                                        {
                                            oSFG.Fk_MatCatID = 12;
                                            oSFG.Stock = oInvDet.RemainingQty;
                                            oSFG.Fk_JobCardID = oNewJob.WorkOrder_ID;
                                            _oEntites.AddToSemi_FinishedGoodsStock(oSFG);
                                        }
                                    }

                                    ////////////////////////////
                                    if (oInvDet.Fk_ProcessID == 4)   //Rotary
                                    {
                                        int RCount1 = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_MatCatID == 19 && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Count();
                                        if (RCount1 > 0)
                                        {
                                            Semi_FinishedGoodsStock ExStock = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_MatCatID == 19 && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Single();

                                            ExStock.Stock = oInvDet.RemainingQty + ExStock.Stock;

                                        }

                                        else
                                        {
                                            oSFG.Fk_MatCatID = 19;
                                            oSFG.Stock = oInvDet.RemainingQty;
                                            oSFG.Fk_JobCardID = oNewJob.WorkOrder_ID;
                                            _oEntites.AddToSemi_FinishedGoodsStock(oSFG);
                                        }
                                    }

                                    ////////////////////////////
                                    if (oInvDet.Fk_ProcessID == 5)   //Printing
                                    {
                                        int RCount1 = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_MatCatID == 21 && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Count();
                                        if (RCount1 > 0)
                                        {
                                            Semi_FinishedGoodsStock ExStock = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_MatCatID == 21 && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Single();

                                            ExStock.Stock = oInvDet.RemainingQty + ExStock.Stock;

                                        }

                                        else
                                        {
                                            oSFG.Fk_MatCatID = 21;
                                            oSFG.Stock = oInvDet.RemainingQty;
                                            oSFG.Fk_JobCardID = oNewJob.WorkOrder_ID;
                                            _oEntites.AddToSemi_FinishedGoodsStock(oSFG);
                                        }
                                    }

                                    ////////////////////////////
                                    if (oInvDet.Fk_ProcessID == 7)   //Slotting
                                    {
                                        int RCount1 = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_MatCatID == 22 && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Count();
                                        if (RCount1 > 0)
                                        {
                                            Semi_FinishedGoodsStock ExStock = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_MatCatID == 22 && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Single();

                                            ExStock.Stock = oInvDet.RemainingQty + ExStock.Stock;

                                        }

                                        else
                                        {
                                            oSFG.Fk_MatCatID = 22;
                                            oSFG.Stock = oInvDet.RemainingQty;
                                            oSFG.Fk_JobCardID = oNewJob.WorkOrder_ID;
                                            _oEntites.AddToSemi_FinishedGoodsStock(oSFG);
                                        }
                                    }


                                    ////////////////////////////
                                    if (oInvDet.Fk_ProcessID == 8)   //Punching
                                    {
                                        int RCount1 = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_MatCatID == 23 && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Count();
                                        if (RCount1 > 0)
                                        {
                                            Semi_FinishedGoodsStock ExStock = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_MatCatID == 23 && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Single();

                                            ExStock.Stock = oInvDet.RemainingQty + ExStock.Stock;

                                        }

                                        else
                                        {
                                            oSFG.Fk_MatCatID = 23;
                                            oSFG.Stock = oInvDet.RemainingQty;
                                            oSFG.Fk_JobCardID = oNewJob.WorkOrder_ID;
                                            _oEntites.AddToSemi_FinishedGoodsStock(oSFG);
                                        }
                                    }
                                    ////////////////////////////
                                    if (oInvDet.Fk_ProcessID == 9)   //Pinning
                                    {
                                        int RCount1 = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_MatCatID == 24 && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Count();
                                        if (RCount1 > 0)
                                        {
                                            Semi_FinishedGoodsStock ExStock = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_MatCatID == 24 && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Single();

                                            ExStock.Stock = oInvDet.RemainingQty + ExStock.Stock;

                                        }

                                        else
                                        {
                                            oSFG.Fk_MatCatID = 24;
                                            oSFG.Stock = oInvDet.RemainingQty;
                                            oSFG.Fk_JobCardID = oNewJob.WorkOrder_ID;
                                            _oEntites.AddToSemi_FinishedGoodsStock(oSFG);
                                        }
                                    }


                                    ////////////////////////////
                                    if (oInvDet.Fk_ProcessID == 10)   //Gumming
                                    {
                                        int RCount1 = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_MatCatID == 25 && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Count();
                                        if (RCount1 > 0)
                                        {
                                            Semi_FinishedGoodsStock ExStock = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_MatCatID == 25 && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Single();

                                            ExStock.Stock = oInvDet.RemainingQty + ExStock.Stock;

                                        }

                                        else
                                        {
                                            oSFG.Fk_MatCatID = 25;
                                            oSFG.Stock = oInvDet.RemainingQty;
                                            oSFG.Fk_JobCardID = oNewJob.WorkOrder_ID;
                                            _oEntites.AddToSemi_FinishedGoodsStock(oSFG);
                                        }
                                    }


                                    ////////////////////////////
                                    if (oInvDet.Fk_ProcessID > 12 && oInvDet.Fk_ProcessID < 25)   //Corr and pasting both rem.qty will be updated to 2 ply stock
                                    {
                                        int RCount1 = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_MatCatID == 18 && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Count();
                                        if (RCount1 > 0)
                                        {
                                            Semi_FinishedGoodsStock ExStock = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_MatCatID == 18 && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Single();

                                            ExStock.Stock = oInvDet.RemainingQty + ExStock.Stock;

                                            ExStock.Description = oInvDet.ProcessMaster.ProcessName;
                                        }

                                        else
                                        {
                                            oSFG.Fk_MatCatID = 18;
                                            oSFG.Stock = oInvDet.RemainingQty;
                                            oSFG.Fk_JobCardID = oNewJob.WorkOrder_ID;
                                            oSFG.Description = oInvDet.ProcessMaster.ProcessName;
                                            _oEntites.AddToSemi_FinishedGoodsStock(oSFG);
                                        }
                                    }

                                    _oEntites.SaveChanges();
                                }




                                else/////////////////////////////////////////IF A PROCESS IS REPEATED
                                {

                                    if (oInvDet.Fk_ProcessID == 2)   //top paper
                                    {
                                        decimal? PrsID = _oEntites.JobProcess.Where(p => p.Fk_ProcessID == oInvDet.Fk_ProcessID && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Max(p => p.Fk_ProcessID);

                                        //  int PCount1 = _oEntites.JobProcess.Where(p => p.Fk_ProcessID ==PrCnt && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Count();
                                        JobProcess oInvDet1 = _oEntites.JobProcess.Where(p => p.Fk_ProcessID == PrsID).Single();

                                        var RemQty = oInvDet1.RemainingQty;


                                        int RCount1 = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_MatCatID == 12 && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Count();
                                        if (RCount1 > 0)
                                        {
                                            //Semi_FinishedGoodsStock ExStock = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_MatCatID == 12 && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Single();

                                            //ExStock.Stock = RemQty + ExStock.Stock;

                                        }

                                        else
                                        {
                                            oSFG.Fk_MatCatID = 12;
                                            oSFG.Stock = RemQty;
                                            oSFG.Fk_JobCardID = oNewJob.WorkOrder_ID;
                                            _oEntites.AddToSemi_FinishedGoodsStock(oSFG);
                                        }
                                    }
                                    //////////////////////
                                    if (oInvDet.Fk_ProcessID == 4)   //Rotary
                                    {
                                        decimal? PrsID = _oEntites.JobProcess.Where(p => p.Fk_ProcessID == oInvDet.Fk_ProcessID && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Max(p => p.Pk_ID);

                                        //  int PCount1 = _oEntites.JobProcess.Where(p => p.Fk_ProcessID ==PrCnt && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Count();
                                        JobProcess oInvDet1 = _oEntites.JobProcess.Where(p => p.Pk_ID == PrsID).Single();

                                        var RemQty = oInvDet1.RemainingQty;


                                        int RCount1 = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_MatCatID == 19 && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Count();
                                        if (RCount1 > 0)
                                        {
                                            //Semi_FinishedGoodsStock ExStock = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_MatCatID == 19 && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Single();

                                            //ExStock.Stock = RemQty + ExStock.Stock;

                                        }

                                        else
                                        {
                                            oSFG.Fk_MatCatID = 19;
                                            oSFG.Stock = RemQty;
                                            oSFG.Fk_JobCardID = oNewJob.WorkOrder_ID;
                                            _oEntites.AddToSemi_FinishedGoodsStock(oSFG);
                                        }
                                    }


                                    //////////////////////
                                    if (oInvDet.Fk_ProcessID == 5)   //Printing
                                    {
                                        decimal? PrsID = _oEntites.JobProcess.Where(p => p.Fk_ProcessID == oInvDet.Fk_ProcessID && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Max(p => p.Fk_ProcessID);

                                        //  int PCount1 = _oEntites.JobProcess.Where(p => p.Fk_ProcessID ==PrCnt && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Count();
                                        JobProcess oInvDet1 = _oEntites.JobProcess.Where(p => p.Fk_ProcessID == PrsID).Single();

                                        var RemQty = oInvDet1.RemainingQty;


                                        int RCount1 = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_MatCatID == 21 && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Count();
                                        if (RCount1 > 0)
                                        {
                                            //Semi_FinishedGoodsStock ExStock = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_MatCatID == 21 && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Single();

                                            //ExStock.Stock = RemQty + ExStock.Stock;

                                        }

                                        else
                                        {
                                            oSFG.Fk_MatCatID = 21;
                                            oSFG.Stock = RemQty;
                                            oSFG.Fk_JobCardID = oNewJob.WorkOrder_ID;
                                            _oEntites.AddToSemi_FinishedGoodsStock(oSFG);
                                        }
                                    }

                                    //////////////////////
                                    if (oInvDet.Fk_ProcessID == 7)   //Slotting
                                    {
                                        decimal? PrsID = _oEntites.JobProcess.Where(p => p.Fk_ProcessID == oInvDet.Fk_ProcessID && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Max(p => p.Fk_ProcessID);

                                        //  int PCount1 = _oEntites.JobProcess.Where(p => p.Fk_ProcessID ==PrCnt && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Count();
                                        JobProcess oInvDet1 = _oEntites.JobProcess.Where(p => p.Fk_ProcessID == PrsID).Single();

                                        var RemQty = oInvDet1.RemainingQty;


                                        int RCount1 = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_MatCatID == 22 && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Count();
                                        if (RCount1 > 0)
                                        {
                                            //Semi_FinishedGoodsStock ExStock = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_MatCatID == 22 && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Single();

                                            //ExStock.Stock = RemQty + ExStock.Stock;

                                        }

                                        else
                                        {
                                            oSFG.Fk_MatCatID = 22;
                                            oSFG.Stock = RemQty;
                                            oSFG.Fk_JobCardID = oNewJob.WorkOrder_ID;
                                            _oEntites.AddToSemi_FinishedGoodsStock(oSFG);
                                        }
                                    }


                                    //////////////////////
                                    if (oInvDet.Fk_ProcessID == 8)   //Punching
                                    {
                                        decimal? PrsID = _oEntites.JobProcess.Where(p => p.Fk_ProcessID == oInvDet.Fk_ProcessID && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Max(p => p.Fk_ProcessID);

                                        //  int PCount1 = _oEntites.JobProcess.Where(p => p.Fk_ProcessID ==PrCnt && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Count();
                                        JobProcess oInvDet1 = _oEntites.JobProcess.Where(p => p.Fk_ProcessID == PrsID).Single();

                                        var RemQty = oInvDet1.RemainingQty;


                                        int RCount1 = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_MatCatID == 23 && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Count();
                                        if (RCount1 > 0)
                                        {
                                            //Semi_FinishedGoodsStock ExStock = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_MatCatID == 23 && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Single();

                                            //ExStock.Stock = RemQty + ExStock.Stock;

                                        }

                                        else
                                        {
                                            oSFG.Fk_MatCatID = 23;
                                            oSFG.Stock = RemQty;
                                            oSFG.Fk_JobCardID = oNewJob.WorkOrder_ID;
                                            _oEntites.AddToSemi_FinishedGoodsStock(oSFG);
                                        }
                                    }


                                    //////////////////////
                                    if (oInvDet.Fk_ProcessID == 9)   //Pinning
                                    {
                                        decimal? PrsID = _oEntites.JobProcess.Where(p => p.Fk_ProcessID == oInvDet.Fk_ProcessID && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Max(p => p.Fk_ProcessID);

                                        //  int PCount1 = _oEntites.JobProcess.Where(p => p.Fk_ProcessID ==PrCnt && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Count();
                                        JobProcess oInvDet1 = _oEntites.JobProcess.Where(p => p.Fk_ProcessID == PrsID).Single();

                                        var RemQty = oInvDet1.RemainingQty;


                                        int RCount1 = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_MatCatID == 24 && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Count();
                                        if (RCount1 > 0)
                                        {
                                            //Semi_FinishedGoodsStock ExStock = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_MatCatID == 24 && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Single();

                                            //ExStock.Stock = RemQty + ExStock.Stock;

                                        }

                                        else
                                        {
                                            oSFG.Fk_MatCatID = 24;
                                            oSFG.Stock = RemQty;
                                            oSFG.Fk_JobCardID = oNewJob.WorkOrder_ID;
                                            _oEntites.AddToSemi_FinishedGoodsStock(oSFG);
                                        }
                                    }



                                    //////////////////////
                                    if (oInvDet.Fk_ProcessID == 10)   //Gumming
                                    {
                                        decimal? PrsID = _oEntites.JobProcess.Where(p => p.Fk_ProcessID == oInvDet.Fk_ProcessID && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Max(p => p.Fk_ProcessID);

                                        //  int PCount1 = _oEntites.JobProcess.Where(p => p.Fk_ProcessID ==PrCnt && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Count();
                                        JobProcess oInvDet1 = _oEntites.JobProcess.Where(p => p.Fk_ProcessID == PrsID).Single();

                                        var RemQty = oInvDet1.RemainingQty;


                                        int RCount1 = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_MatCatID == 25 && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Count();
                                        if (RCount1 > 0)
                                        {
                                            //Semi_FinishedGoodsStock ExStock = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_MatCatID == 25 && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Single();

                                            //ExStock.Stock = RemQty + ExStock.Stock;

                                        }

                                        else
                                        {
                                            oSFG.Fk_MatCatID = 25;
                                            oSFG.Stock = RemQty;
                                            oSFG.Fk_JobCardID = oNewJob.WorkOrder_ID;
                                            _oEntites.AddToSemi_FinishedGoodsStock(oSFG);
                                        }
                                    }


                                    //////////////////////
                                    if (oInvDet.Fk_ProcessID > 12 && oInvDet.Fk_ProcessID < 25)   //Corr and pasting both rem.qty will be updated to 2 ply stock
                                    {
                                        decimal? PrsID = _oEntites.JobProcess.Where(p => p.Fk_ProcessID == oInvDet.Fk_ProcessID && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Max(p => p.Fk_ProcessID);

                                        //  int PCount1 = _oEntites.JobProcess.Where(p => p.Fk_ProcessID ==PrCnt && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Count();
                                        JobProcess oInvDet1 = _oEntites.JobProcess.Where(p => p.Fk_ProcessID == PrsID).Single();

                                        var RemQty = oInvDet1.RemainingQty;


                                        int RCount1 = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_MatCatID == 18 && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Count();
                                        if (RCount1 > 0)
                                        {
                                            //Semi_FinishedGoodsStock ExStock = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_MatCatID ==18 && p.Fk_JobCardID == oNewJob.WorkOrder_ID).Single();

                                            //ExStock.Stock = RemQty + ExStock.Stock;

                                            //ExStock.Description = oInvDet.ProcessMaster.ProcessName;
                                        }

                                        else
                                        {
                                            oSFG.Fk_MatCatID = 18;
                                            oSFG.Stock = RemQty;
                                            oSFG.Fk_JobCardID = oNewJob.WorkOrder_ID;
                                            oSFG.Description = oInvDet.ProcessMaster.ProcessName;
                                            _oEntites.AddToSemi_FinishedGoodsStock(oSFG);
                                        }
                                    }
                                    _oEntites.SaveChanges();


                                }


                                object orefSchedule = oInvDet;
                                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(JobProcess), oDataProperties, ref orefSchedule);
                                UpdatedPk.Add(Pk_ID);


                            }
                        }

                        else
                        {
                            JobProcess oInvDets = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(JobProcess), oDataProperties) as JobProcess;
                            // oInvDets.RequiredDate = null;

                            oInvDets.ProcessDate = Convert.ToDateTime(oDataProperties["ProcessDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                            if (oDataProperties.ContainsKey("PSTime"))
                            {
                                oInvDets.PSTime = oDataProperties["PSTime"].ToString();
                            }
                            if (oDataProperties.ContainsKey("PETime"))
                            { oInvDets.PETime = oDataProperties["PETime"].ToString(); }
                            oJobDetList.Add(oInvDets);

                            decimal FQty = decimal.Parse(oDataProperties["PQuantity"].ToString());
                            decimal PrID = Convert.ToDecimal(oInvDets.Fk_ProcessID);
                            ProcessMaster oPrDet = _oEntites.ProcessMaster.Where(p => p.Pk_Process == PrID).Single();

                            string ProName = oPrDet.ProcessName;
                            int RCount1 = _oEntites.wfStates.Where(p => p.State == ProName).Count();
                            if (RCount1 > 0)
                            {
                                wfState oStateName = _oEntites.wfStates.Where(p => p.State == ProName).Single();


                                decimal UpdStatVal = oStateName.Pk_State;
                                //oPk_JobCard
                                oNewJobFromShelf.Fk_Status = UpdStatVal;
                            }
                            else
                            {
                                decimal UpdStatVal = 13;
                                //oPk_JobCard
                                oNewJobFromShelf.Fk_Status = UpdStatVal;
                            }

                            if (PrID == 11)
                            {
                                //if (oBoxStock == null)
                                //{
                                //    oBoxStock = new BoxStock();
                                //    oBoxStock.Fk_BoxID = dFk_BoxID;
                                //    oBoxStock.Fk_PartID = dFk_PartID;
                                //    oBoxStock.Quantity = FQty;
                                //    _oEntites.AddToBoxStock(oBoxStock);

                                //}
                                //else
                                //{
                                //    oBoxStock.Quantity = Convert.ToDecimal(oBoxStock.Quantity + FQty);


                                //}
                            }
                            oNewJobFromShelf.WODate = Convert.ToDateTime(omValues["WODate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                            _oEntites.SaveChanges();
                            ////////////////////////////////////////////////////////update date here


                            Semi_FinishedGoodsStock oSFG1 = new Semi_FinishedGoodsStock();
                            if (JCStat == "4")
                            {
                                int RCount2 = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_MatCatID == 12).Count();
                                if (RCount2 > 0)
                                {
                                    Semi_FinishedGoodsStock ExStock = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_MatCatID == 12).Single();

                                    ExStock.Stock = oInvDets.RemainingQty + ExStock.Stock;

                                }

                                else
                                {
                                    oSFG1.Fk_MatCatID = 12;
                                    oSFG1.Stock = oInvDets.RemainingQty;
                                    _oEntites.AddToSemi_FinishedGoodsStock(oSFG1);
                                }

                                _oEntites.SaveChanges();
                            }

                        }
                    }
                    _oEntites.SaveChanges();
                    //List<JobProcess> oDeletedRecords = oNewJobFromShelf.JobProcess.Where(p => !UpdatedPk.Contains(p.Pk_ID)).ToList();

                    //foreach (JobProcess oDeletedDetail in oDeletedRecords)
                    //{
                    //    oNewJobFromShelf.JobProcess.Remove(oDeletedDetail);
                    //}

                    //Add new elements
                    //foreach (JobProcess oNewIndentDetail in oJobDetList)
                    //{
                    //    oNewJobFromShelf.JobProcess.Add(oNewIndentDetail);
                    //}

                }

                //////////////////////


                _oEntites.SaveChanges();

                oResult.Success = true;
                oResult.Message = " JC No.-" + oNewJob.WorkOrder_ID.ToString() + " Processes Updated ";

            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        //public ModelManuplationResult Delete()
        //{
        //    ModelManuplationResult oResult = new ModelManuplationResult();

        //    try
        //    {
        //        WorkOrder oOrder = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(WorkOrder), omValues) as WorkOrder;
        //        WorkOrder oOrderFromShelf = _oEntites.WorkOrder.Where(p => p.CustomerPO == oOrder.CustomerPO).Single();
        //        _oEntites.DeleteObject(oOrderFromShelf);
        //        _oEntites.SaveChanges();
        //        oResult.Success = true;
        //    }
        //    catch (Exception e)
        //    {
        //        oResult.Success = false;
        //        oResult.Message = e.Message;
        //        oResult.Exception = e;
        //    }

        //    return oResult;
        //}

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oJob;
            }
            set
            {
                oJob = value as WorkOrder;
            }
        }

        public decimal ID
        {
            get
            {
                return oJob.WorkOrder_ID;
            }
            set
            {
                oJob = _oEntites.WorkOrder.Where(p => p.WorkOrder_ID == value).Single();
            }
        }

        public object GetRaw()
        {
            return oJob;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }




        //public Dictionary<int, double> GetMonthWiseOrderCount()
        //{

        //    Dictionary<int, double> oResult = new Dictionary<int, double>();

        //    for (int i = 1; i <= 12; i++)
        //    {
        //        int Count = _oEntites.WorkOrder.Where(p => p.OrderDate.Month == i).Count();

        //        oResult.Add(i, Count);
        //    }

        //    return oResult;
        //}

        //public Dictionary<string, int> GetOrderSatusCount()
        //{

        //    Dictionary<string, int> oResult = new Dictionary<string, int>();

        //    IEnumerable<wfState> oStates = _oEntites.wfStates;

        //    foreach (wfState oState in oStates)
        //    {
        //        int Count = _oEntites.WorkOrder.Where(p => p.wfState.State == oState.State).Count();

        //        if (Count > 0)
        //        {
        //            oResult.Add(oState.State, Count);
        //        }
        //    }

        //    return oResult;
        //}

        //public Dictionary<string, int> GetDeliveryScheduleStatus()
        //{

        //    Dictionary<string, int> oResult = new Dictionary<string, int>();

        //    int MissedCount = _oEntites.gen_DeliverySchedule.Where(p => (p.DeliveryDate <= DateTime.Today && (p.DeliveryCompleted == null || p.DeliveryCompleted == false))).Count();

        //    int CompletedCount = _oEntites.gen_DeliverySchedule.Where(p => p.DeliveryCompleted == true).Count();

        //    int PendingCount = _oEntites.gen_DeliverySchedule.Where(p => (p.DeliveryDate > DateTime.Today && (p.DeliveryCompleted == null || p.DeliveryCompleted == false))).Count();

        //    oResult.Add("Pending", PendingCount);
        //    oResult.Add("Completed", CompletedCount);
        //    oResult.Add("Missed", MissedCount);

        //    return oResult;
        //}

        //public double OnTimeDeliveryPercentage()
        //{

        //    int TotalCount = _oEntites.gen_DeliverySchedule.Where(p => p.DeliveryDate < DateTime.Today).Count();

        //    int CompletedCount = _oEntites.gen_DeliverySchedule.Where(p => p.DeliveryCompleted == true && (p.DeliveryDate >= p.DeliveredDate)).Count();

        //    double oResult = TotalCount != 0 ? ((CompletedCount * 100) / TotalCount) : 0;

        //    return oResult;
        //}

        //public Dictionary<string, string> OrderDeliveries()
        //{
        //    Dictionary<string, string> oResult = new Dictionary<string, string>();
        //    //int Count = 0;
        //    //IEnumerable<Inv_Material> oMaterials = _oEntites.Inv_Material;
        //    DateTime todate = Convert.ToDateTime(DateTime.Today.AddDays(1));
        //    int tannent = Convert.ToInt32(HttpContext.Current.Session["Tanent"]);
        //    IEnumerable<vw_OrderDeliveryDetails> oOrder = _oEntites.vw_OrderDeliveryDetails.Where(p => p.DeliveryDate == todate && p.Fk_Tanent == tannent);
        //    //  IEnumerable<gen_Vendor> ovendor = _oEntites.gen_Vendor;
        //    int ss = 0;
        //    foreach (vw_OrderDeliveryDetails oOrd in oOrder)
        //    {
        //        oResult.Add(ss.ToString(), oOrd.CustomerName + "-" + oOrd.Quantity);
        //        ss++;
        //    }
        //    return oResult;
        //}


        public ModelManuplationResult Delete()
        {
            throw new NotImplementedException();
        }

        //ModelManuplationResult IDomainObject.CreateNew()
        //{
        //    throw new NotImplementedException();
        //}

        EntityObject IDomainObject.DAO
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        ModelManuplationResult IDomainObject.Delete()
        {
            throw new NotImplementedException();
        }

        void IDomainObject.Dispose()
        {
            Dispose(true);
            //GC.SuppressFinalize(this);
        }



    }
}
