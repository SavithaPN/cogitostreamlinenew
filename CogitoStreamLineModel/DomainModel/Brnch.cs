﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Objects.DataClasses;
using WebGareCore;
using WebGareCore.CommonObjects;
using System.Data;
using CogitoStreamLineModel;


namespace CogitoStreamLineModel.DomainModel
{
    public class Brnch : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private Branch oBranch = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();
            string sPk_BranchID = null;
            string sBranchName = null;
            string sFk_Tanent = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Branch> oBranches = null;

            try { sPk_BranchID = p_Params.Single(p => p.ParameterName == "Pk_BranchID").ParameterValue; }
            catch { }
            try { sBranchName = p_Params.Single(p => p.ParameterName == "BranchName").ParameterValue; }
            catch { }
            try { sFk_Tanent = p_Params.Single(p => p.ParameterName == "Fk_Tanent").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oBranches = _oEntites.Branch;
            try
            {
                if (!string.IsNullOrEmpty(sPk_BranchID))
                {
                    oBranches = oBranches.Where(p => p.Pk_BranchID == decimal.Parse(sPk_BranchID));
                }
                if (!string.IsNullOrEmpty(sBranchName))
                {
                    //oBranches = oBranches.Where(p => p.CountryName.IndexOf(sCountryName, StringComparison.OrdinalIgnoreCase) >= 0);
                    oBranches = oBranches.Where(p => p.BranchName.ToLower() == (sBranchName.ToLower().ToString().Trim()));
                }
                if (!string.IsNullOrEmpty(sFk_Tanent))
                {
                    oBranches = oBranches.Where(p => p.Fk_Tanent == decimal.Parse(sFk_Tanent));
                }
            }
            catch (System.NullReferenceException)
            {

            }

           
            oSearchResult.RecordCount = oBranches.Count();

           oBranches= oBranches.OrderBy(p => p.BranchName);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oBranches = oBranches.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oBranches.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }

        public SearchResult SearchBranchName(List<SearchParameter> p_Params)
        {

            SearchResult oSearchResult = new SearchResult();
            IEnumerable<Branch> oBranchName = null;
            oBranchName = _oEntites.Branch;

            oSearchResult.RecordCount = oBranchName.Count();
            oBranchName.OrderBy(p => p.Pk_BranchID);

            List<EntityObject> oFilteredItem = oBranchName.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItem;

            return oSearchResult;
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Branch oNewBranches = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Branch), omValues) as Branch;
                oNewBranches.Fk_Tanent = Convert.ToInt32(System.Web.HttpContext.Current.Session["Tanent"]);


                _oEntites.AddToBranch(oNewBranches);

                _oEntites.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Branch oBranches = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Branch), omValues) as Branch;
                Branch oBranchesFromShelf = _oEntites.Branch.Where(p => p.Pk_BranchID == oBranches.Pk_BranchID).Single();
                object orefItems = oBranchesFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(Branch), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Branch oBranches = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Branch), omValues) as Branch;
                Branch oBranchesFromShelf = _oEntites.Branch.Where(p => p.Pk_BranchID == oBranches.Pk_BranchID).Single();
                object orefItems = oBranchesFromShelf;
                //_oEntites.DeleteObject(oBranchesFromShelf);

                _oEntites.DeleteObject(oBranchesFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oBranch;

            }
            set
            {
                oBranch = value as Branch;
            }
        }

        public decimal ID
        {
            get
            {
                return oBranch.Pk_BranchID;
            }
            set
            {
                oBranch = _oEntites.Branch.Where(p => p.Pk_BranchID == value).Single();
            }
        }

        public object GetRaw()
        {
            return oBranch;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}


