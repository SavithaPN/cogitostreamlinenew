﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using Newtonsoft.Json;
using WebGareCore;


namespace CogitoStreamLineModel.DomainModel
{
    public class Users : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private ideUser oideUser = null;
        public const string SHORTCUTS = "ShortCuts";

        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();
            
            oSearchResult.RecordCount = 0;

                //We make a call that Search can only be done for User based on 
                //First name, Last name and username
                string sUserName = null;
                string sFirstName = null;
                string sLastName = null;
                string sStartIndex = null;
                string sPageSize = null;
                string sSorting = null;
                string sPassword = null;
                IEnumerable<ideUser> oUsers = null;

                try { sUserName = p_Params.Single(p => p.ParameterName == "UserName").ParameterValue; }
                catch { }
                try { sPassword = p_Params.Single(p => p.ParameterName == "Password").ParameterValue; }
                catch { }
                try { sFirstName = p_Params.Single(p => p.ParameterName == "FirstName").ParameterValue; }
                catch { }
                try { sLastName = p_Params.Single(p => p.ParameterName == "LastName").ParameterValue; }
                catch { }
                try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
                catch { }
                try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
                catch { }
                try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
                catch { }


                oUsers = _oEntites.ideUser;

                try
                {
                    if (!string.IsNullOrEmpty(sUserName))
                    {
                        oUsers = oUsers.Where(p => p.UserName.IndexOf(sUserName, StringComparison.OrdinalIgnoreCase) >= 0);
                    }
                    if (!string.IsNullOrEmpty(sPassword))
                    {
                        oUsers = oUsers.Where(p => p.Password.IndexOf(sPassword, StringComparison.OrdinalIgnoreCase) >= 0);
                    }
                    if (!string.IsNullOrEmpty(sFirstName))
                    {
                        oUsers = oUsers.Where(p => p.FirstName.StartsWith(sFirstName));

                    }
                    if (!string.IsNullOrEmpty(sLastName))
                    {
                        oUsers = oUsers.Where(p => (!string.IsNullOrEmpty(p.LastName) && p.LastName.IndexOf(sLastName, StringComparison.OrdinalIgnoreCase) >= 0));
                    }
                }
                catch (System.NullReferenceException)
                {
                    //possible that some items may not have the required fields set just ignore them
                }

                oUsers = oUsers.Where(p => p.IsActive == true);
                oSearchResult.RecordCount = oUsers.Count();
                oUsers.OrderBy(p => p.UserName);


                if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
                {

                    var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                    var skip = int.Parse(sPageSize) * (page - 1);

                    oUsers = oUsers.Select(p => p)
                                        .Skip(skip)
                                        .Take(int.Parse(sPageSize));
                }

                List<EntityObject> oFilteredUsers = oUsers.Select(p => p).OfType<EntityObject>().ToList();
                oSearchResult.ListOfRecords = oFilteredUsers;
           

            return oSearchResult;
            
          }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                ideUser oNewuser = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(ideUser), omValues) as ideUser;

                if (omValues.ContainsKey("Shortcuts"))
                {
                    string sShortCuts = omValues["Shortcuts"].ToString();
                    object[] ShortContacts = JsonConvert.DeserializeObject<object[]>(sShortCuts);

                    foreach (object oItem in ShortContacts)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());
                        ideShortCut oShortCut = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(ideShortCut), oDataProperties) as ideShortCut;
                        oNewuser.ideShortCut.Add(oShortCut);
                    }

                }

                if (_oEntites.ideUser.Where(p => (p.FirstName == oNewuser.FirstName && p.LastName == oNewuser.LastName)).Count() > 0)
                {
                    oResult.Success = false;
                    oResult.Message = "User " + oNewuser.FirstName + " - " + oNewuser.LastName + " already exists";
                    return oResult;
                }

                //Set password same as usename
                oNewuser.Password = oNewuser.UserName;
                oNewuser.IsActive = true;
                
                //Created date is today
                oNewuser.CreatedDate = DateTime.Today;

                _oEntites.AddToideUser(oNewuser);
                _oEntites.SaveChanges();

                //add the role
                ideRoleUser oRoleUser = new ideRoleUser();
                oRoleUser.Fk_Roles = decimal.Parse(omValues["Role"].ToString());
                oRoleUser.Fk_Users = oNewuser.Pk_Users;

                _oEntites.AddToideRoleUser(oRoleUser);
                _oEntites.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                ideUser oUser = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(ideUser), omValues) as ideUser;
                ideUser oUserFromShelf = _oEntites.ideUser.Where(p => p.Pk_Users == oUser.Pk_Users).Single();
                object orefUser = oUserFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(ideUser), omValues, ref orefUser);

                oUserFromShelf.ideShortCut.Clear();
               
                if (omValues.ContainsKey("Shortcuts"))
                {
                    string sShortCuts = omValues["Shortcuts"].ToString();
                    object[] ShortContacts = JsonConvert.DeserializeObject<object[]>(sShortCuts);

                    foreach (object oItem in ShortContacts)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());
                        ideShortCut oShortCut = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(ideShortCut), oDataProperties) as ideShortCut;
                        oUserFromShelf.ideShortCut.Add(oShortCut);
                    }

                }

                //add the role
                ideRoleUser oRoleUser = new ideRoleUser();
                oRoleUser.Fk_Roles = decimal.Parse(omValues["Role"].ToString());
                oUserFromShelf.ideRoleUser.Clear();
                oUserFromShelf.ideRoleUser.Add(oRoleUser);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                ideUser oUser = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(ideUser), omValues) as ideUser;
                ideUser oUserFromShelf = _oEntites.ideUser.Where(p => p.Pk_Users == oUser.Pk_Users).Single();
                _oEntites.DeleteObject(oUserFromShelf);
                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult UpdatePassword(string sUserName, string sCurrentPassword, string sNewPassword)
        {
            ModelManuplationResult oResult = new ModelManuplationResult();
            ideUser oUser = _oEntites.ideUser.Where(p => p.UserName == sUserName).First();
            oResult.Success = true;

            try
            {

                if (!oUser.Password.Equals(sCurrentPassword, StringComparison.InvariantCulture))
                {
                    oResult.Message = "Current Password is wrong";
                    oResult.Success = false;
                    return oResult;
                }

                oUser.Password = sNewPassword;
                _oEntites.SaveChanges();
            }
            catch (Exception e)
            {
                oResult.Message = e.Message;
                oResult.Success = false;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oideUser;
            }
            set
            {
                oideUser = value as ideUser;
            }
        }

        public decimal ID
        {
            get
            {
                return oideUser.Pk_Users;
            }
            set
            {
                oideUser = _oEntites.ideUser.Where(p => p.Pk_Users == value).Single();
            }
        }

        public object GetRaw()
        {
            return oideUser;
        }

        public List<ideActivity> GetHomePageModules(string sUserName)
        {
            List<ideActivity> oModule = new List<ideActivity>();
            //Now need to get the modules to display for the first screen
            ideRole oRole = _oEntites.ideUser.
                                First(p => p.UserName.Equals(sUserName, StringComparison.InvariantCultureIgnoreCase))
                                .ideRoleUser.First().ideRole;
            IEnumerable<ideActivityPermission> oPermittedModiles = oRole.ideActivityPermission;

            IEnumerable<ideActivity> oAllModules = oPermittedModiles.Select(p => p.ideActivity);


            foreach (ideActivity otempModule in oAllModules)
            {
                if (otempModule.Fk_ParentActivity == null)
                {
                    oModule.Add(otempModule);
                }

            }

            return oModule;
        }

        public bool HasRole(string sUserName, string sRoleName)
        {
            ideRole oRole = _oEntites.ideUser.
                                 First(p => p.UserName.Equals(sUserName, StringComparison.InvariantCultureIgnoreCase))
                                 .ideRoleUser.First().ideRole;
            bool bHasRole = oRole.RoleName.Equals(sRoleName) ? true : false;
            return bHasRole;
        }

        public ideActivity GetModule(decimal id)
        {
            ideActivity oModule = _oEntites.ideActivity.Single(p => p.Pk_Activities == id);
            return oModule;
        }

        public List<ideActivity> GetSubModules(decimal id, string sUserName)
        {
            List<ideActivity> oModule = new List<ideActivity>();
            //Now need to get the modules to display for the first screen
            ideRole oRole = _oEntites.ideUser.
                                First(p => p.UserName.Equals(sUserName, StringComparison.InvariantCultureIgnoreCase))
                                .ideRoleUser.First().ideRole;

            IEnumerable<ideActivityPermission> oPermittedModiles = oRole.ideActivityPermission;

            IEnumerable<ideActivity> oAllModules = oPermittedModiles.Select(p => p.ideActivity);


            foreach (ideActivity otempModule in oAllModules)
            {
                if (otempModule.Fk_ParentActivity == id)
                {
                    oModule.Add(otempModule);
                }

            }

            return oModule;
        }


        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        

        public EntityObject AddDetail(string sBasketName, EntityObject oItem)
        {
            if (sBasketName == SHORTCUTS)
            {
                ideShortCut oShortCut = oItem as ideShortCut;
                oideUser.ideShortCut.Add(oShortCut);
                return oShortCut;
            }

            return null;
        }

        public EntityObject RemoveDetail(string sBasketName, decimal dPk)
        {
            if (sBasketName == SHORTCUTS)
            {
                ideShortCut oShortCut = oideUser.ideShortCut.Where(p => p.Pk_Shortcuts == dPk).Single();
                oideUser.ideShortCut.Remove(oShortCut);
                _oEntites.SaveChanges();
                return null;
            }

            return null;
        }
                    
        public List<EntityObject> GetDetails(string sBasketName)
        {
            if (sBasketName == SHORTCUTS)
            {

                List<EntityObject> oShortCuts = oideUser.ideShortCut.Select(p => p).OfType<EntityObject>().ToList();
                return oShortCuts;
            }

            return null;
        }


        public EntityObject UpdateDetail(string sBasketName, decimal dPk, Dictionary<string, object> oValues)
        {
            if (sBasketName == SHORTCUTS)
            {
                ideShortCut oShortCut = oideUser.ideShortCut.Where(p => p.Pk_Shortcuts == dPk).Single();
                object orefShortCut = oShortCut;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(ideShortCut), oValues, ref orefShortCut);
                _oEntites.SaveChanges();
                return oShortCut;
            }

            return null;
        }

        public AuthenticationResult Authenticate(string sUserName, string sPassword)
        {
            AuthenticationResult oResult = new AuthenticationResult();

            try
            {
                oResult.LicenseFailure = false;

                string sFilePath = System.Web.HttpContext.Current.Server.MapPath("~/License.txt");
                string sKey = System.IO.File.ReadAllText(sFilePath);

                string sActualKey = Utilities.Decrypt(sKey);

                string sYear = sActualKey.Substring(5, 2);
                string sMonth = sActualKey.Substring(8, 2);
                string sDay = sActualKey.Substring(11, 2);

                DateTime oValidDate = DateTime.Parse(sDay + "/" + sMonth + "/" + sYear);

                if (oValidDate > DateTime.Today)
                {
                    oResult.Success = false;
                    oResult.LicenseFailure = true;
                    oResult.Message = "License not valid contact administrator";

                    return oResult;
                }

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("UserName", sUserName));
                oSearchParams.Add(new SearchParameter("Password", sPassword));

                SearchResult oSearchResult = this.Search(oSearchParams);

                if (oSearchResult.RecordCount > 0)
                {
                    oResult.ListOfRecords = oSearchResult.ListOfRecords;
                    oResult.Success = true;
                }
                else
                {
                    oResult.Success = false;
                    oResult.Message = "Username or Password was incorrect";
                }
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
            }

            return oResult;

        }
    }
}
