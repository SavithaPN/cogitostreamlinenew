﻿//Name          : Unit Class
//Description   : Contains the Country class definition, every page in the application will be instance of this class with following
//                 1. Communication 2. Data binding 3. Loading preferences
//Author        : Shantha
//Date 	        : 07/08/2015
//Crh Number    : SL0003
//Modifications : 


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using System.Web;
using Newtonsoft.Json;


namespace CogitoStreamLineModel.DomainModel
{
    public class Unit : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private gen_Unit oUnit = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_Unit = null;
            string sUnitName = null;
            string sUnitType = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<gen_Unit> oUnits = null;

            try { sPk_Unit = p_Params.Single(p => p.ParameterName == "Pk_Unit").ParameterValue; }
            catch { }
            try { sUnitName = p_Params.Single(p => p.ParameterName == "UnitName").ParameterValue; }
            catch { }
            try { sUnitType = p_Params.Single(p => p.ParameterName == "UnitType").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oUnits = _oEntites.gen_Unit;

            try
            {
                if (!string.IsNullOrEmpty(sPk_Unit))
                {
                    oUnits = oUnits.Where(p => p.Pk_Unit == decimal.Parse(sPk_Unit));
                }
                if (!string.IsNullOrEmpty(sUnitName))
                {
                    oUnits = oUnits.Where(p => p.UnitName.IndexOf(sUnitName, StringComparison.OrdinalIgnoreCase) >= 0);

                }
                if (!string.IsNullOrEmpty(sUnitType))
                {
                    oUnits = oUnits.Where(p => p.UnitType.IndexOf(sUnitType, StringComparison.OrdinalIgnoreCase) >= 0);

                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oUnits.Count();

            oUnits.OrderBy(p => p.Pk_Unit);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oUnits = oUnits.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oUnits.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }

        public SearchResult SearchUnitName(List<SearchParameter> p_Params)
        {

            SearchResult oSearchResult = new SearchResult();
            IEnumerable<gen_Unit> oUnitName = null;
            oUnitName = _oEntites.gen_Unit;

            oSearchResult.RecordCount = oUnitName.Count();
            oUnitName.OrderBy(p => p.Pk_Unit);

            List<EntityObject> oFilteredItem = oUnitName.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItem;

            return oSearchResult;
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                gen_Unit oNewUnit = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_Unit), omValues) as gen_Unit;

                _oEntites.AddTogen_Unit(oNewUnit);

                _oEntites.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                gen_Unit oUnit = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_Unit), omValues) as gen_Unit;
                gen_Unit oUnitFromShelf = _oEntites.gen_Unit.Where(p => p.Pk_Unit == oUnit.Pk_Unit).Single();
                object orefItems = oUnitFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(gen_Unit), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;

            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                gen_Unit oUnit = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_Unit), omValues) as gen_Unit;
                gen_Unit oUnitFromShelf = _oEntites.gen_Unit.Where(p => p.Pk_Unit == oUnit.Pk_Unit).Single();
                object orefItems = oUnitFromShelf;
                //_oEntites.DeleteObject(oEmployeesFromShelf);

                _oEntites.DeleteObject(oUnitFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oUnit;

            }
            set
            {
                oUnit = value as gen_Unit;
            }
        }

        public decimal ID
        {
            get
            {
                return oUnit.Pk_Unit;
            }
            set
            {
                oUnit = _oEntites.gen_Unit.Where(p => p.Pk_Unit == value).Single();
            }
        }

        public object GetRaw()
        {
            return oUnit;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}


