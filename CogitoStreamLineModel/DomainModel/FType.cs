﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Objects.DataClasses;
using WebGareCore;
using WebGareCore.CommonObjects;
using System.Data;
using CogitoStreamLineModel;
namespace CogitoStreamLineModel.DomainModel
{
    public class FType : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();

        private Dictionary<string, object> omValues = null;
        private FluteType oDept = null;




        public bool SetSpecId(decimal p_dSpec)
        {
            if (_oEntites.FluteType.Where(p => p.Pk_FluteID == p_dSpec).Count() > 0)
            {
                oDept = _oEntites.FluteType.Where(p => p.Pk_FluteID == p_dSpec).First();

                return true;
            }

            return false;
        }




        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();
            string sPk_DepartmentId = null;
            string sName = null;
            string sTakeUpFactor = null;
            string sStartIndex = null;
            
            string sPageSize = null;

            string sSorting = null;
            IEnumerable<FluteType> oDepts = null;

            try { sPk_DepartmentId = p_Params.Single(p => p.ParameterName == "FluteID").ParameterValue; }
            catch { }
            try { sName = p_Params.Single(p => p.ParameterName == "Name").ParameterValue; }
            catch { }
            try { sTakeUpFactor = p_Params.Single(p => p.ParameterName == "TakeUpFactor").ParameterValue; }
            catch { }

            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oDepts = _oEntites.FluteType;
            try
            {
                if (!string.IsNullOrEmpty(sPk_DepartmentId))
                {
                    oDepts = oDepts.Where(p => p.Pk_FluteID == decimal.Parse(sPk_DepartmentId));
                }
                if (!string.IsNullOrEmpty(sName))
                {
                    oDepts = oDepts.Where(p => p.FluteName.IndexOf(sName.ToLower(), StringComparison.OrdinalIgnoreCase) >= 0);
                   
                }
                if (!string.IsNullOrEmpty(sTakeUpFactor))
                {
                    oDepts = oDepts.Where(p => p.TKFactor == decimal.Parse(sTakeUpFactor));
                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oDepts.Count();

            oDepts.OrderBy(p => p.Pk_FluteID);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oDepts = oDepts.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oDepts.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }

    
        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                FluteType oNewDeptNames = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(FluteType), omValues) as FluteType;

                _oEntites.AddToFluteType(oNewDeptNames);

                _oEntites.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                FluteType oDeptNames = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(FluteType), omValues) as FluteType;
                FluteType oDeptNamesFromShelf = _oEntites.FluteType.Where(p => p.Pk_FluteID == oDeptNames.Pk_FluteID).Single();
                object orefItems = oDeptNamesFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(FluteType), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                FluteType oDeptNames = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(FluteType), omValues) as FluteType;
                FluteType oDeptNamesFromShelf = _oEntites.FluteType.Where(p => p.Pk_FluteID == oDeptNames.Pk_FluteID).Single();
                object orefItems = oDeptNamesFromShelf;
                //_oEntites.DeleteObject(oEmployeesFromShelf);

                _oEntites.DeleteObject(oDeptNamesFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oDept;

            }
            set
            {
                oDept = value as FluteType;
            }
        }

        public decimal ID
        {
            get
            {
                return oDept.Pk_FluteID;
            }
            set
            {
                oDept = _oEntites.FluteType.Where(p => p.Pk_FluteID == value).Single();
            }
        }

        public object GetRaw()
        {
            return oDept;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
