﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using System.Web;
using Newtonsoft.Json;


namespace CogitoStreamLineModel.DomainModel
{
    public class Material : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private Inv_Material oMaterial = null;
       // private Vw_Material oIndent = null;
        private BoardDesc oBoxSp = new BoardDesc();
        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sFk_Mill = null;
            string sName = null;
            string sfkMat = null;
            string sBrand = null;
            decimal sDeckle = 0;
            string sMaterialType = null;
            decimal sLength = 0;
            decimal sWidth = 0;
            decimal sGSM = 0;
            decimal sBF = 0;
            string sColor = null;
            string sCategory = null;
            string sPaperType = null;
            decimal sPk_Material = 0;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Inv_Material> oMaterials = null;

            try { sFk_Mill = p_Params.Single(p => p.ParameterName == "Mill").ParameterValue; }
            catch { }

            try { sName = p_Params.Single(p => p.ParameterName == "Name").ParameterValue; }
            catch { }
            try { sDeckle = Convert.ToDecimal(p_Params.Single(p => p.ParameterName == "Deckle").ParameterValue); }
            catch { }
            try { sfkMat = p_Params.Single(p => p.ParameterName == "Pk_Mat").ParameterValue; }
            catch { }

            try { sBrand = p_Params.Single(p => p.ParameterName == "Brand").ParameterValue; }
            catch { }

            try { sMaterialType = p_Params.Single(p => p.ParameterName == "MaterialType").ParameterValue; }
            catch { }

            try { sColor = p_Params.Single(p => p.ParameterName == "Fk_Color").ParameterValue; }
            catch { }

            try { sCategory = p_Params.Single(p => p.ParameterName == "Category").ParameterValue; }
            catch { }

            try { sPaperType = p_Params.Single(p => p.ParameterName == "PaperType").ParameterValue; }
            catch { }

            try { sLength = Convert.ToDecimal(p_Params.Single(p => p.ParameterName == "Length").ParameterValue); }
            catch { }

            try { sWidth = Convert.ToDecimal(p_Params.Single(p => p.ParameterName == "Width").ParameterValue); }
            catch { }

            try { sGSM = Convert.ToDecimal(p_Params.Single(p => p.ParameterName == "GSM").ParameterValue); }
            catch { }

            try { sBF = Convert.ToDecimal(p_Params.Single(p => p.ParameterName == "BF").ParameterValue); }
            catch { }
            try { sPk_Material = Convert.ToDecimal(p_Params.Single(p => p.ParameterName == "Pk_Material").ParameterValue); }
            catch { }

            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oMaterials = _oEntites.Inv_Material;

            try
            {

                if (!string.IsNullOrEmpty(sfkMat))
                {
                    oMaterials = oMaterials.Where(p => p.Pk_Material == decimal.Parse(sfkMat));
                }
                if (sPk_Material!=0)
                {
                    oMaterials =  oMaterials.Where(p => p.Pk_Material != null && p.Pk_Material == sPk_Material);
                }
                if (!string.IsNullOrEmpty(sFk_Mill))
                {
                    oMaterials = oMaterials.Where(p => p.Fk_Mill != null && p.gen_Mill.MillName.IndexOf(sFk_Mill, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sName))
                {
                    oMaterials = oMaterials.Where(p => p.Name != null && p.Name.IndexOf(sName, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sBrand))
                {
                    oMaterials = oMaterials.Where(p => p.Brand != null && p.Brand.IndexOf(sBrand, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sMaterialType))
                {
                    oMaterials = oMaterials.Where(p => p.MaterialType != null && p.MaterialType.IndexOf(sMaterialType, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sColor))
                {
                    oMaterials = oMaterials.Where(p => p.Fk_Color != null && p.gen_Color.ColorName.IndexOf(sColor, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sCategory))
                {
                    oMaterials = oMaterials.Where(p => p.Fk_MaterialCategory != null && p.Inv_MaterialCategory.Name.IndexOf(sCategory, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sPaperType))
                {
                    oMaterials = oMaterials.Where(p => p.Fk_PaperType != null && p.gen_PaperType.PaperTypeName.IndexOf(sPaperType, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (sLength != 0)
                {
                    oMaterials = oMaterials.Where(p => p.Length != null && p.Length == sLength);
                }
                if (sWidth != 0)
                {
                    oMaterials = oMaterials.Where(p => p.Width != null && p.Width == sWidth);
                }
                if (sGSM != 0)
                {
                    oMaterials = oMaterials.Where(p => p.GSM != null && p.GSM == sGSM);
                }
                if (sDeckle != 0)
                {
                    oMaterials = oMaterials.Where(p =>p.Deckle == sDeckle);
                }
                
                if (sBF != 0)
                {
                    oMaterials = oMaterials.Where(p => p.BF != null && p.BF == sBF);
                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oMaterials.Count();

         oMaterials= oMaterials.OrderByDescending(p => p.Pk_Material);


         if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
         {
             var Nnval = int.Parse(sPageSize);
             if (Nnval == 0)
             {

                 //var page = (int.Parse(sStartIndex) / 1) ; // set current page number, must be >= 1
                 //var skip = int.Parse(sPageSize) * (page - 1);

                 //oPOrd = oPOrd.Select(p => p)
                 //                    .Skip(skip)
                 //                    .Take(int.Parse(sPageSize));
             }
             else
             {
                 var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                 var skip = int.Parse(sPageSize) * (page - 1);

                 oMaterials = oMaterials.Select(p => p)
                                     .Skip(skip)
                                     .Take(int.Parse(sPageSize));
             }
         }
            List<EntityObject> oFilteredItems = oMaterials.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;
        }



        public SearchResult SearchMat(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sFk_Mill = null;
            string sName = null;
            string sfkMat = null;
            string sBrand = null;
            string sMaterialType = null;
            decimal sLength = 0;
            decimal sWidth = 0;
            decimal sGSM = 0;
            decimal sBF = 0;
            string sColor = null;
            string sCategory = null;
            string sPaperType = null;

            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Inv_Material> oMaterials = null;

            try { sFk_Mill = p_Params.Single(p => p.ParameterName == "Fk_Mill").ParameterValue; }
            catch { }

            try { sName = p_Params.Single(p => p.ParameterName == "Name").ParameterValue; }
            catch { }

            try { sfkMat = p_Params.Single(p => p.ParameterName == "fkMat").ParameterValue; }
            catch { }

            try { sBrand = p_Params.Single(p => p.ParameterName == "Brand").ParameterValue; }
            catch { }

            try { sMaterialType = p_Params.Single(p => p.ParameterName == "MaterialType").ParameterValue; }
            catch { }

            try { sColor = p_Params.Single(p => p.ParameterName == "Fk_Color").ParameterValue; }
            catch { }

            try { sCategory = p_Params.Single(p => p.ParameterName == "Category").ParameterValue; }
            catch { }

            try { sPaperType = p_Params.Single(p => p.ParameterName == "PaperType").ParameterValue; }
            catch { }

            try { sLength = Convert.ToDecimal(p_Params.Single(p => p.ParameterName == "Length").ParameterValue); }
            catch { }

            try { sWidth = Convert.ToDecimal(p_Params.Single(p => p.ParameterName == "Width").ParameterValue); }
            catch { }

            try { sGSM = Convert.ToDecimal(p_Params.Single(p => p.ParameterName == "GSMval").ParameterValue); }
            catch { }

            try { sBF = Convert.ToDecimal(p_Params.Single(p => p.ParameterName == "BFval").ParameterValue); }
            catch { }


            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oMaterials = _oEntites.Inv_Material;

            try
            {

                if (!string.IsNullOrEmpty(sfkMat))
                {
                    oMaterials = oMaterials.Where(p => p.Pk_Material == decimal.Parse(sfkMat));
                }
                if (!string.IsNullOrEmpty(sFk_Mill))
                {
                    oMaterials = oMaterials.Where(p => p.Fk_Mill != null && p.gen_Mill.MillName.IndexOf(sFk_Mill, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sName))
                {
                    oMaterials = oMaterials.Where(p => p.Name != null && p.Name.IndexOf(sName, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sBrand))
                {
                    oMaterials = oMaterials.Where(p => p.Brand != null && p.Brand.IndexOf(sBrand, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sMaterialType))
                {
                    oMaterials = oMaterials.Where(p => p.MaterialType != null && p.MaterialType.IndexOf(sMaterialType, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sColor))
                {
                    oMaterials = oMaterials.Where(p => p.Fk_Color != null && p.gen_Color.ColorName.IndexOf(sColor, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sCategory))
                {
                    oMaterials = oMaterials.Where(p => p.Fk_MaterialCategory != null && p.Inv_MaterialCategory.Name.IndexOf(sCategory, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sPaperType))
                {
                    oMaterials = oMaterials.Where(p => p.Fk_PaperType != null && p.gen_PaperType.PaperTypeName.IndexOf(sPaperType, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (sLength != 0)
                {
                    oMaterials = oMaterials.Where(p => p.Length != null && p.Length == sLength);
                }
                if (sWidth != 0)
                {
                    oMaterials = oMaterials.Where(p => p.Width != null && p.Width == sWidth);
                }
                if (sGSM != 0)
                {
                    oMaterials = oMaterials.Where(p => p.GSM != null && p.GSM == sGSM);
                }
                if (sBF != 0)
                {
                    oMaterials = oMaterials.Where(p => p.BF != null && p.BF == sBF);
                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oMaterials.Count();

            oMaterials = oMaterials.OrderByDescending(p => p.Pk_Material);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oMaterials = oMaterials.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oMaterials.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;
        }



        public SearchResult SearchIndent(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sIndentNo = null;
            string sVendorName = null;
            string sMaterialName = null;
            string sName = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            //IEnumerable<Inv_Material> oMaterials = null;
            IEnumerable<Vw_Material> oIndents = null;


            try { sIndentNo = p_Params.Single(p => p.ParameterName == "IndentNo").ParameterValue; }
            catch { }

            try { sVendorName = p_Params.Single(p => p.ParameterName == "VendorName").ParameterValue; }
            catch { }
            try { sMaterialName = p_Params.Single(p => p.ParameterName == "MaterialName").ParameterValue; }
            catch { }
            try { sName = p_Params.Single(p => p.ParameterName == "Name").ParameterValue; }
            catch { }


            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oIndents = _oEntites.Vw_Material;

            try
            {
                if (!string.IsNullOrEmpty(sIndentNo))
                {
                    oIndents = oIndents.Where(p => p.Pk_MaterialOrderMasterId == decimal.Parse(sIndentNo));
                }
                if (!string.IsNullOrEmpty(sVendorName))
                {
                    oIndents = oIndents.Where(p => p.VendorName != null && p.VendorName.IndexOf(sVendorName, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sName))
                {
                    oIndents = oIndents.Where(p => p.Name != null && p.Name.IndexOf(sName, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                //if (!string.IsNullOrEmpty(sMaterialName))
                //{
                //    oIndents = oIndents.Where(p => p.Name != null && p.Name.IndexOf(sMaterialName, StringComparison.OrdinalIgnoreCase) >= 0);
                //}

            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oIndents.Count();

            oIndents.OrderBy(p => p.Pk_MaterialOrderMasterId);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oIndents = oIndents.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oIndents.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;
        }


        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }
        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();
            try
            {
                Inv_Material oNewMaterialCategory = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Inv_Material), omValues) as Inv_Material;
                 

              if(omValues.Count<15)
              {



                string sCategory = omValues["Category"].ToString();
                decimal dCatId = _oEntites.Inv_MaterialCategory.Where(p => p.Name.ToLower() == sCategory.ToLower()).Single().Pk_MaterialCategory;
                
             
                //

                IEnumerable<Inv_Material> oMCount = _oEntites.Inv_Material.Where(p => p.GSM == oNewMaterialCategory.GSM && p.BF == oNewMaterialCategory.BF && p.Fk_Mill == oNewMaterialCategory.Fk_Mill && p.Fk_Color == oNewMaterialCategory.Fk_Color && p.Fk_MaterialCategory == dCatId && p.Deckle == oNewMaterialCategory.Deckle);
                if (oMCount.Count() > 0)
                {
                    oResult.Success = false;
                    oResult.Message = "Paper with this GSM, BF, Mill and Colour combination already exists in the database.";
                }

                else
                {

                    //oNewMaterialCategory.Fk_UnitId = 1;
                    if (dCatId == 4)
                    {
                        oNewMaterialCategory.Fk_UnitId = 1;
                    }
                    else
                    {
                        //oNewMaterialCategory.Fk_UnitId = 1;
                    }

                    oNewMaterialCategory.Fk_MaterialCategory = dCatId;


                    _oEntites.AddToInv_Material(oNewMaterialCategory);



                    int tannent = Convert.ToInt32(HttpContext.Current.Session["Tanent"]);
                    int branchVal = Convert.ToInt32(HttpContext.Current.Session["Branch"]);
         

                    Stock oStockMaster = new Stock();
                    oStockMaster.Fk_BranchID = Convert.ToInt32(branchVal);
                    oStockMaster.Fk_Tanent = Convert.ToInt32(tannent);
                    oStockMaster.Quantity = 0;
                    oNewMaterialCategory.Stocks.Add(oStockMaster);



                    if (oNewMaterialCategory.Fk_MaterialCategory == 4)
                    {
                        PaperStock oStockMaster1 = new PaperStock();
                        oStockMaster1.RollNo = "";

                        oStockMaster1.Quantity = 0;
                        oNewMaterialCategory.PaperStock.Add(oStockMaster1);

                    }
                    _oEntites.SaveChanges();
                    oResult.Success = true;
                    oResult.Message = oNewMaterialCategory.Pk_Material.ToString() +"-"+ oNewMaterialCategory.PPly.ToString();
                    
                }

              }
                  else  
                  if(omValues.Count>15)
                  {
                      decimal id = decimal.Parse(omValues["Fk_BoxID"].ToString());
                       string sParts = omValues["parts"].ToString();
                    object[] Parts = JsonConvert.DeserializeObject<object[]>(sParts);

                    foreach (object oItem in Parts)
                    {

                        Inv_Material oNewInv = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Inv_Material), omValues) as Inv_Material;
                        Inv_Material oNewInvFromShelf = _oEntites.Inv_Material.Where(p => p.Pk_Material == id).Single();


                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                        Inv_Material  oPart = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Inv_Material ), oProperties) as Inv_Material ;

                        oNewInvFromShelf.Length = Convert.ToDecimal(oProperties["length"].ToString());
                        oNewInvFromShelf.Width = Convert.ToDecimal(oProperties["width"].ToString());
                        oNewInvFromShelf.Height = Convert.ToDecimal(oProperties["height"].ToString());
                            
                            

                              object orefInvFromShelf = oNewInvFromShelf;
                              WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(Inv_Billing), omValues, ref orefInvFromShelf);


                        string sLayers = oProperties["layers"].ToString();
                        object[] Layers = JsonConvert.DeserializeObject<object[]>(sLayers);
                            int i = 0;
                                int a = 1;
                                foreach (object oLayerItem in Layers)
                                {
                                    //decimal deckel;
                                    Dictionary<string, object> oLayerProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oLayerItem.ToString());

                                    oLayerProperties["Fk_BoardID"] = id;
                                    oLayerProperties["LayerID"] = a;


                                    //oLayerProperties["Weight"] = layerWeight;
                                    BoardDesc oLayer = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(BoardDesc), oLayerProperties) as BoardDesc;

                                //    oPart.BoardDesc.Add(oLayer);
                                    i = i + 1;
                                    a = a + 1; 
                                    
                                    _oEntites.AddToBoardDesc(oLayer);
                                    //_oEntites.SaveChanges();
                               
                                }

                                _oEntites.SaveChanges();
                                oResult.Success = true;
                             

                            } // try
                  }

                }
            
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }
            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Inv_Material oMaterial = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Inv_Material), omValues) as Inv_Material;
                Inv_Material oMaterialFromShelf = _oEntites.Inv_Material.Where(p => p.Pk_Material == oMaterial.Pk_Material).Single();
                object orefItems = oMaterialFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(Inv_Material), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }
            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Inv_Material oMaterial = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Inv_Material), omValues) as Inv_Material;
                Inv_Material oMaterialFromShelf = _oEntites.Inv_Material.Where(p => p.Pk_Material == oMaterial.Pk_Material).Single();
                object orefItems = oMaterialFromShelf;
                _oEntites.DeleteObject(oMaterialFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oMaterial;

            }
            set
            {
                oMaterial = value as Inv_Material;
            }
        }

        public decimal ID
        {
            get
            {
                return oMaterial.Pk_Material;
            }
            set
            {
                oMaterial = _oEntites.Inv_Material.Where(p => p.Pk_Material == value).Single();
            }
        }

        public object GetRaw()
        {
            return oMaterial;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }



        public Dictionary<string, string> GetMinLevelStocks()
        {
            Dictionary<string, string> oResult = new Dictionary<string, string>();
            int tannent = Convert.ToInt32(HttpContext.Current.Session["Tanent"]);
            IEnumerable<vw_StockMaterial> oMaterial = _oEntites.vw_StockMaterial.Where(p => p.Quantity <= p.Min_Value && p.Fk_Tanent==tannent);
            int s = 0;
            foreach (vw_StockMaterial oMaten in oMaterial)
            {
                oResult.Add(s.ToString(), oMaten.Name);
                s++;
            }
            return oResult;
        }
    }
}



