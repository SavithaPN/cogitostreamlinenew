﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//namespace CogitoStreamLineModel.DomainModel
//{
//    class Tax
//    {
//    }
//}


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Objects.DataClasses;
using WebGareCore;
using WebGareCore.CommonObjects;
using System.Data;
using CogitoStreamLineModel;


namespace CogitoStreamLineModel.DomainModel
{
    public class Taxx : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private Tax oTax = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();
            string sPkTax = null;
            string sTaxName = null;
            string sTaxValue = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Tax> oTaxs = null;

            try { sPkTax = p_Params.Single(p => p.ParameterName == "PkTax").ParameterValue; }
            catch { }
            try { sTaxName = p_Params.Single(p => p.ParameterName == "TaxName").ParameterValue; }
            catch { }
            try { sTaxValue = p_Params.Single(p => p.ParameterName == "TaxValue").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oTaxs = _oEntites.Tax;
            try
            {
                if (!string.IsNullOrEmpty(sPkTax))
                {
                    oTaxs = oTaxs.Where(p => p.PkTax == decimal.Parse(sPkTax));
                }
                if (!string.IsNullOrEmpty(sTaxName))
                {
                    //oCountrys = oCountrys.Where(p => p.CountryName.IndexOf(sCountryName, StringComparison.OrdinalIgnoreCase) >= 0);
                    oTaxs = oTaxs.Where(p => p.TaxName.ToLower() == (sTaxName.ToLower().ToString().Trim()));
                }
                if (!string.IsNullOrEmpty(sTaxValue))
                {
                    oTaxs = oTaxs.Where(p => p.TaxValue == decimal.Parse(sTaxValue));
                }
            }
            catch (System.NullReferenceException)
            {

            }

            oSearchResult.RecordCount = oTaxs.Count();

            oTaxs.OrderBy(p => p.PkTax);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oTaxs = oTaxs.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oTaxs.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }

        public SearchResult SearchTaxName(List<SearchParameter> p_Params)
        {

            SearchResult oSearchResult = new SearchResult();
            IEnumerable<Tax> oTaxName = null;
            oTaxName = _oEntites.Tax;

            oSearchResult.RecordCount = oTaxName.Count();
            oTaxName.OrderBy(p => p.PkTax);

            List<EntityObject> oFilteredItem = oTaxName.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItem;

            return oSearchResult;
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Tax oNewTaxs = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Tax), omValues) as Tax;

                _oEntites.AddToTax(oNewTaxs);

                _oEntites.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Tax oTaxs = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Tax), omValues) as Tax;
                Tax oTaxsFromShelf = _oEntites.Tax.Where(p => p.PkTax == oTaxs.PkTax).Single();
                object orefItems = oTaxsFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(Tax), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Tax oTaxs = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Tax), omValues) as Tax;
                Tax oTaxsFromShelf = _oEntites.Tax.Where(p => p.PkTax == oTaxs.PkTax).Single();
                object orefItems = oTaxsFromShelf;
                //_oEntites.DeleteObject(oCountrysFromShelf);

                _oEntites.DeleteObject(oTaxsFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oTax;

            }
            set
            {
                oTax = value as Tax;
            }
        }

        public decimal ID
        {
            get
            {
                return oTax.PkTax;
            }
            set
            {
                oTax = _oEntites.Tax.Where(p => p.PkTax == value).Single();
            }
        }

        public object GetRaw()
        {
            return oTax;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
