﻿
//Name          : Paper Type Class
//Description   : Contains the Paper class definition, every page in the application will be instance of this class with following
//                 1. Communication 2. Data binding 3. Loading preferences
//Author        : Nagesh V Rao
//Date 	        : 08/08/2015
//Crh Number    : SL0009
//Modifications : 
 


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Objects.DataClasses;
using WebGareCore;
using WebGareCore.CommonObjects;
using System.Data;
using CogitoStreamLineModel;
namespace CogitoStreamLineModel.DomainModel
{
    public class PaperType : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();

        private Dictionary<string, object> omValues = null;
        private gen_PaperType oPaperType = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();
            string sPk_PaperType = null;
            string sPaperTypeName = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<gen_PaperType> oPaperTypes = null;

            try { sPk_PaperType = p_Params.Single(p => p.ParameterName == "Pk_PaperType").ParameterValue; }
            catch { }
            try { sPaperTypeName = p_Params.Single(p => p.ParameterName == "PaperTypeName").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oPaperTypes = _oEntites.gen_PaperType;
            try
            {
                if (!string.IsNullOrEmpty(sPk_PaperType))
                {
                    oPaperTypes = oPaperTypes.Where(p => p.Pk_PaperType == decimal.Parse(sPk_PaperType));
                }
                if (!string.IsNullOrEmpty(sPaperTypeName))
                {
                    oPaperTypes = oPaperTypes.Where(p => p.PaperTypeName.IndexOf(sPaperTypeName.ToLower(), StringComparison.OrdinalIgnoreCase) >= 0);
                    //oPaperTypes = oPaperTypes.Where(p => p.PaperTypeName.ToLower() == (sPaperTypeName.ToLower().ToString().Trim()));
                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oPaperTypes.Count();

            oPaperTypes.OrderBy(p => p.Pk_PaperType);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oPaperTypes = oPaperTypes.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oPaperTypes.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }

        public SearchResult SearchPaperTypeName(List<SearchParameter> p_Params)
        {

            SearchResult oSearchResult = new SearchResult();
            IEnumerable<gen_PaperType> oPaperName = null;
            oPaperName = _oEntites.gen_PaperType;

            oSearchResult.RecordCount = oPaperName.Count();
            oPaperName.OrderBy(p => p.Pk_PaperType);

            List<EntityObject> oFilteredItem = oPaperName.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItem;

            return oSearchResult;
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                gen_PaperType oNewPaperTypes = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_PaperType), omValues) as gen_PaperType;

                _oEntites.AddTogen_PaperType(oNewPaperTypes);

                _oEntites.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                gen_PaperType oPaperTypes = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_PaperType), omValues) as gen_PaperType;
                gen_PaperType oPaperTypesFromShelf = _oEntites.gen_PaperType.Where(p => p.Pk_PaperType == oPaperTypes.Pk_PaperType).Single();
                object orefItems = oPaperTypesFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(gen_PaperType), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                gen_PaperType oPaperTypes = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_PaperType), omValues) as gen_PaperType;
                gen_PaperType oPaperTypesFromShelf = _oEntites.gen_PaperType.Where(p => p.Pk_PaperType == oPaperTypes.Pk_PaperType).Single();
                object orefItems = oPaperTypesFromShelf;
                //_oEntites.DeleteObject(oEmployeesFromShelf);

                _oEntites.DeleteObject(oPaperTypesFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oPaperType;

            }
            set
            {
                oPaperType = value as gen_PaperType;
            }
        }

        public decimal ID
        {
            get
            {
                return oPaperType.Pk_PaperType;
            }
            set
            {
                oPaperType = _oEntites.gen_PaperType.Where(p => p.Pk_PaperType == value).Single();
            }
        }

        public object GetRaw()
        {
            return oPaperType;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}




