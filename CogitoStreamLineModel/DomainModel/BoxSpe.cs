﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using WebGareCore;
using Newtonsoft.Json;
using CogitoStreamLineModel;
using System.Data.SqlClient;
using System.Data;

namespace CogitoStreamLineModel.DomainModel
{
    public class BoxSpe : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private BoxSpec oBoxSp = new BoxSpec();
        private est_BoxEstimation oEstBox = new est_BoxEstimation();
        public decimal fkbox = 0;
        public decimal fkboxest = 0;
        public decimal TKF = 0;
        public decimal ply = 0;

        public SearchResult Search(List<SearchParameter> p_Params)
        {
            //We make a call that Search can only be done for Order based on 
            //First name, Last name and OrderName
            decimal? sSpec = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<BoxSpec> oBoxSps = null;

            SearchResult oSearchResult = new SearchResult();

            try { sSpec = Convert.ToDecimal(p_Params.Single(p => p.ParameterName == "Spec").ParameterValue); }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oBoxSps = _oEntites.BoxSpecs;

            try
            {
                if (sSpec != null)
                {
                    oBoxSps = oBoxSps.Where(p => p.Pk_BoxSpecID == sSpec);
                }

            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oBoxSps.Count();
            oBoxSps = oBoxSps.OrderByDescending(p => p.Pk_BoxSpecID);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oBoxSps = oBoxSps.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredOrders = oBoxSps.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredOrders;

            return oSearchResult;

        }

        public bool SetSpecId(decimal p_dSpec)
        {
            if (_oEntites.BoxSpecs.Where(p => p.Fk_BoxID == p_dSpec).Count() > 0)
            {
                oBoxSp = _oEntites.BoxSpecs.Where(p => p.Fk_BoxID == p_dSpec).First();

                return true;
            }

            return false;
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }

        public SearchResult SearchPaper(List<SearchParameter> p_Params)
        {
            //We make a call that Search can only be done for Order based on 
            //First name, Last name and OrderName
            decimal? sGSM = null;
            decimal? sBF = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Inv_Material> oBoxSps = null;

            SearchResult oSearchResult = new SearchResult();

            try { sGSM = Convert.ToDecimal(p_Params.Single(p => p.ParameterName == "GSM").ParameterValue); }
            catch { }
            try { sBF = Convert.ToDecimal(p_Params.Single(p => p.ParameterName == "BF").ParameterValue); }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oBoxSps = _oEntites.Inv_Material;

            try
            {
                if (sGSM != null)
                {
                    oBoxSps = oBoxSps.Where(p => p.GSM == sGSM);
                }
                if (sBF != null)
                {
                    oBoxSps = oBoxSps.Where(p => p.BF == sBF);
                }

            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oBoxSps.Count();
            //oBoxSps = oBoxSps.OrderByDescending(p => p.Pk_BoxSpecID);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oBoxSps = oBoxSps.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredOrders = oBoxSps.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredOrders;

            return oSearchResult;

        }


        public ModelManuplationResult CreateNew()
           
        {
            ModelManuplationResult oResult = new ModelManuplationResult();
            //SqlConnection cn;
            int BType = 0;
            //decimal T1 = 2.0M;
            decimal boardArea, layerWeight, TLength, TWeight, cutLength;
            decimal cuttingsize = 0;
            decimal deckel=0;
            boardArea = 0;
            decimal FluTypeVal = 0;
            decimal TF = 0;
            decimal BoardGSM = 0;
            decimal BGSM = 0;
            decimal BBSVal=0;
            decimal? Ups = 0;
            //bool OutYes = false;
            //bool LPYes = false;  ///small partition -length
            //bool WPYes = false; ////big partition -width
            //bool SYes = false; ////big partition -width
            try
            {
                string PartString="";
                string myString = omValues["Fk_BoxID"].ToString();

                string[] parts = myString.Split(',');
                if (parts.Length != 1)
                {
                    string pStr = parts[1];
                    if (pStr.Length > 0)
                    {
                        int pos = (pStr.IndexOf("=")) + 1;
                        BType = Convert.ToInt16(pStr.Substring(pos));
                    }
                    else
                    { pStr = parts[0]; }
                    
                }

                if (parts.Length > 1)
                {
                    string FPos = parts[2];
                    string[] MPos = FPos.Split('~');

                    if (MPos.Length ==0)
                    {
                        int length = FPos.Length;
                        int StartIndex = (FPos.IndexOf("=")) ;
                        //(FPos.Substring(7-(7-5-1)))
                     //   TKF = Convert.ToDecimal(FPos.Substring(0, (Fpos - 1)));
                        FluTypeVal = Convert.ToDecimal(FPos.Substring(length-(length-(StartIndex+1))));
                    }
                    else
                    {
                        string FStr = MPos[1];
                        if (FStr.Length > 0)
                        {
                            int Fpos = (FStr.IndexOf(";")) + 1;
                            TKF = Convert.ToDecimal(FStr.Substring(0, (Fpos - 1)));
                            int Fpos1 = (FStr.IndexOf("=")) + 1;
                            FluTypeVal = Convert.ToDecimal(FStr.Substring(Fpos1, 1));
                        }
                    }
                }
                int SearchStringPos = myString.IndexOf("Type");
                bool bIsBoxSpecData = false;

                //If from boxspecification  only box data to be updated
                if (SearchStringPos != -1)
                {
                    bIsBoxSpecData = true;

                    string firstval = omValues["Fk_BoxID"].ToString();
                    int index = firstval.IndexOf(",");

                    string fval = firstval.Substring(0, index);
                    omValues["Fk_BoxID"] = decimal.Parse(fval);
                }
                else
                {
                    //omValues["Fk_BoxID"] is used to pass other parameters also
                    //Hence resetting it with only FK_BoxID
                    omValues["Fk_BoxID"] = decimal.Parse(parts[0]);
                }

                //BoxMaster oNewBox = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(BoxMaster), omValues) as BoxMaster;
                BoxSpec oBoxSp = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(BoxSpec), omValues) as BoxSpec;
             
                if (bIsBoxSpecData)
                {
                    string sParts = omValues["parts"].ToString();
                    object[] Parts = JsonConvert.DeserializeObject<object[]>(sParts);
                    int j = 0;
                    foreach (object oItem in Parts)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                        ItemPartProperty oPart = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(ItemPartProperty), oProperties) as ItemPartProperty;


                       
                        string sLayers = oProperties["layers"].ToString();
                        object[] Layers = JsonConvert.DeserializeObject<object[]>(sLayers);

                        if (j == 0)
                        {
                             PartString = oProperties["PName"].ToString();
                        }
                        object[] SelVal = PartString.Split('`');
             
                        
                        //string[] MPos = Layers["rate"].ToString();
                         int i = 0;
                         TWeight = 0;
                            // if (BType == 1)
                            //{
                            //    TLength = (Convert.ToDecimal(oPart.Length) * 2 + Convert.ToDecimal(oPart.Width) * 2 + 50);
                            //    deckel = (Convert.ToDecimal(oPart.Width) + Convert.ToDecimal(oPart.Height) + 20);
                            //    boardArea = (TLength * deckel);
                            //}
                          
                        foreach (object oLayerItem in Layers)
                        {

                            var LayerCtr = Layers.Count();
                            oPart.PName = SelVal[j].ToString();

                            //decimal deckel;
                            Dictionary<string, object> oLayerProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oLayerItem.ToString());
                            if (oPart.PName == "Box")
                            {
                                if (BType == 1)
                                {
                                    TLength = (Convert.ToDecimal(oPart.Length) * 2 + Convert.ToDecimal(oPart.Width) * 2 + 50)/10;
                                   var UpsVal=Math.Floor(Convert.ToDecimal(1820/(oPart.Height+oPart.Width))) ;
                                   Ups = Math.Truncate(UpsVal);
                                    if (Ups > 4)
                                    {
                                        Ups = 4;
                                    }
                                    deckel =(((Convert.ToDecimal(oPart.Width) + Convert.ToDecimal(oPart.Height) )*Convert.ToDecimal(Ups)) + 20)/10;
                                    boardArea = (TLength * deckel);
                                    cuttingsize = TLength;

                                 //   cuttingsize = Math.Round(Convert.ToDecimal(oPart.Length) * 2 + Convert.ToDecimal(oPart.Width) * 2 + Convert.ToDecimal(50));
                                   // var addl = Math.Round(Convert.ToDecimal(boardArea / 1000000), 2);

                                    if (oLayerProperties["rate"].ToString().Length == 0)
                                    {
                                        //var TF = Convert.ToDecimal(oLayerProperties["rate"]);
                                        TF = 1;
                                    }
                                    else
                                    {
                                        TF = Convert.ToDecimal(oLayerProperties["rate"]);
                                    }
                                    //  TF = Convert.ToDecimal(oLayerProperties["rate"]);
                                    decimal GsmVal = Convert.ToDecimal(oLayerProperties["gsm"]);
                                    if (i % 2 != 0)
                                    {
                                        //fluted
                                      
                                        layerWeight =(GsmVal/1000) * (deckel / 100) * (TLength / 100) * TF;    //////////////////rate is takeup factor value
                                        //weight = weight + layerWeight;
                                       // layerWeight = Convert.ToDecimal(((Convert.ToDecimal(oLayerProperties["gsm"]) / 1000) * (Convert.ToDecimal(deckel / 100)) * (Convert.ToDecimal(TLength / 100)) * TF)/Ups);    //////////////////rate is takeup factor value
                                        BGSM = BGSM + TF * Convert.ToDecimal(oLayerProperties["gsm"]);
                                        BBSVal = BBSVal + (Convert.ToDecimal(oLayerProperties["gsm"]) * Convert.ToDecimal(oLayerProperties["bf"]) / 1000) / 2;

                                    }
                                    else
                                    {
                                        layerWeight = (GsmVal / 1000) * (deckel / 100) * (TLength / 100);   //////////////////rate is takeup factor value
                                      //  layerWeight = Convert.ToDecimal(((Convert.ToDecimal(oLayerProperties["gsm"]) / 1000) * (Convert.ToDecimal(deckel / 100)) * (Convert.ToDecimal(TLength / 100)) * TF) / Ups);    //////////////////rate is takeup factor value

                                        //weight = weight + layerWeight;
                                        BGSM = BGSM + Convert.ToDecimal(oLayerProperties["gsm"]);
                                        BBSVal = BBSVal + (Convert.ToDecimal(oLayerProperties["gsm"]) * Convert.ToDecimal(oLayerProperties["bf"]) / 1000);

                                        TF = 1;
                                    }

                                    oLayerProperties["Tk_Factor"] = TF;
                                    oLayerProperties["Rate"] = oLayerProperties["deckel"];
                                    oLayerProperties["Weight"] = (layerWeight) ;
                                    TWeight = TWeight + (layerWeight);


                                    //oLayerProperties["Weight"] = layerWeight;
                                    Items_Layers oLayer = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Items_Layers), oLayerProperties) as Items_Layers;

                                    oPart.Items_Layers.Add(oLayer);

                                    i = i + 1;

                                    ply = i;

                                }
                            }

                            if (oPart.PName == "Length Partition")
                            {

                                deckel = (Convert.ToDecimal(oPart.Width) * 3 + 20);
                                //boardArea = (Convert.ToDecimal(cutLength / 100 * deckel / 100));
                                cutLength = (Convert.ToDecimal((oPart.Length * 2) + 20));
                                //  cuttingsize = Math.Round(Convert.ToDecimal(oPart.Length) + Convert.ToDecimal(oPart.Width) + Convert.ToDecimal(50));
                                var addl = Math.Round(Convert.ToDecimal(oPart.Length)/1000 * Convert.ToDecimal(oPart.Width)/1000, 2);

                                oPart.PName = "Length Partition";
                                // cuttingsize = Math.Round(Convert.ToDecimal(oPart.Length) * 2 + Convert.ToDecimal(oPart.Width) * 2 + Convert.ToDecimal(50));
                                Ups = 1;
                                if (oLayerProperties["rate"].ToString().Length == 0)
                                {

                                    TF = 1;
                                }
                                else
                                {
                                    TF = Convert.ToDecimal(oLayerProperties["rate"]);
                                }
                               // TF = Convert.ToDecimal(oPart.TakeUpFactor);
                                    if (i % 2 != 0)
                                    {
                                        //fluted
                                        layerWeight = Convert.ToDecimal((addl * Convert.ToDecimal(oLayerProperties["gsm"]) / 1000) * TF);     //////////////////rate is takeup factor value
                                        BBSVal = BBSVal + (Convert.ToDecimal(oLayerProperties["gsm"]) * Convert.ToDecimal(oLayerProperties["bf"]) / 1000) / 2;
                                        //Weight = flutedParameter + layerWeight;
                                        BGSM = BGSM + TF * Convert.ToDecimal(oLayerProperties["gsm"]);
                                    }
                                    else
                                    {
                                        //layerWeight = addl * Convert.ToDecimal(oLayerProperties["gsm"]);
                                        //weight = weight + layerWeight;
                                        layerWeight = Convert.ToDecimal((addl * Convert.ToDecimal(oLayerProperties["gsm"]) / 1000));
                                        BBSVal = BBSVal + (Convert.ToDecimal(oLayerProperties["gsm"]) * Convert.ToDecimal(oLayerProperties["bf"]) / 1000) / 2;
                                        //Weight = flutedParameter + layerWeight;
                                        BGSM = BGSM + TF * Convert.ToDecimal(oLayerProperties["gsm"]);
                                        TF = 1;
                                        //nonFlutedParameter = nonFlutedParameter + layerWeight;
                                    }

                                    //flutedWeight = Convert.ToDecimal(cutLength * deckel * flutedParameter * oPart.Quantity);
                                    //nonflutedWeight = Convert.ToDecimal(cutLength * deckel * nonFlutedParameter * oPart.Quantity);


                                    oLayerProperties["Tk_Factor"] = TF;
                                    oLayerProperties["Rate"] = oLayerProperties["deckel"];
                                    oLayerProperties["Weight"] = (layerWeight)/1000 ;

                                    //TotWeight = Convert.ToDecimal((flutedParameter + nonFlutedParameter) / 1000);
                                    TWeight = TWeight + (layerWeight)/1000 ;
                                    //oLayerProperties["Weight"] = layerWeight;
                                    Items_Layers oLayer = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Items_Layers), oLayerProperties) as Items_Layers;

                                    oPart.Items_Layers.Add(oLayer);

                                    i = i + 1;

                                    ply = i;
                                
                            }
                            if (oPart.PName == "Width Partition")
                                {
                                    deckel = (Convert.ToDecimal(oPart.Width) * 3 + 20);
                                    //boardArea = (Convert.ToDecimal(cutLength / 100 * deckel / 100));
                                    cutLength = (Convert.ToDecimal((oPart.Length * 2) + 20));
                                    //  cuttingsize = Math.Round(Convert.ToDecimal(oPart.Length) + Convert.ToDecimal(oPart.Width) + Convert.ToDecimal(50));
                                    var addl = Math.Round(Convert.ToDecimal(oPart.Length)/1000 * Convert.ToDecimal(oPart.Width)/1000, 2);

                                oPart.PName = "Width Partition";
                                // cuttingsize = Math.Round(Convert.ToDecimal(oPart.Length) * 2 + Convert.ToDecimal(oPart.Width) * 2 + Convert.ToDecimal(50));
                               // var addl = Math.Round(Convert.ToDecimal(boardArea * deckel / 1000000), 2);

                                Ups = 1;
                                if (oLayerProperties["rate"].ToString().Length == 0)
                                {
                                    //var TF = Convert.ToDecimal(oLayerProperties["rate"]);
                                    TF = 1;
                                }
                                else
                                {
                                    TF = Convert.ToDecimal(oLayerProperties["rate"]);
                                }
                               // TF = Convert.ToDecimal(oPart.TakeUpFactor);
                                    if (i % 2 != 0)
                                    {
                                        layerWeight = Convert.ToDecimal((addl * Convert.ToDecimal(oLayerProperties["gsm"]) / 1000) * TF);    //////////////////rate is takeup factor value
                                        BBSVal = BBSVal + (Convert.ToDecimal(oLayerProperties["gsm"]) * Convert.ToDecimal(oLayerProperties["bf"]) / 1000) / 2;
                                        //Weight = flutedParameter + layerWeight;
                                        BGSM = BGSM + TF * Convert.ToDecimal(oLayerProperties["gsm"]);
                                    }
                                    else
                                    {
                                        //layerWeight = addl * Convert.ToDecimal(oLayerProperties["gsm"]);
                                        //weight = weight + layerWeight;
                                        layerWeight = Convert.ToDecimal((addl * Convert.ToDecimal(oLayerProperties["gsm"])) / 1000);
                                        BBSVal = BBSVal + (Convert.ToDecimal(oLayerProperties["gsm"]) * Convert.ToDecimal(oLayerProperties["bf"]) / 1000);
                                        //nonFlutedParameter = nonFlutedParameter + layerWeight;
                                        BGSM = BGSM + Convert.ToDecimal(oLayerProperties["gsm"]);
                                        TF = 1;
                                    }

                                    oLayerProperties["Tk_Factor"] = TF;
                                    oLayerProperties["Rate"] = oLayerProperties["deckel"];
                                    oLayerProperties["Weight"] = (layerWeight)/1000;

                                    //TotWeight = Convert.ToDecimal((flutedParameter + nonFlutedParameter) / 1000);
                                    TWeight = TWeight + (layerWeight)/1000 ;
                                    //oLayerProperties["Weight"] = layerWeight;
                                    Items_Layers oLayer = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Items_Layers), oLayerProperties) as Items_Layers;

                                    oPart.Items_Layers.Add(oLayer);

                                    i = i + 1;

                                    ply = i;
                                }
                            if (oPart.PName == "Sleeve")
                                    {
                                        cuttingsize = Math.Round(Convert.ToDecimal(oPart.Length) * 2 + Convert.ToDecimal(oPart.Width) * 2 + Convert.ToDecimal(50));
                                deckel = (Convert.ToDecimal(oPart.Height) * 3 + 20);
                              //  boardArea = (Convert.ToDecimal(cutLength / 100 * deckel / 100));

                                // cuttingsize = Math.Round(Convert.ToDecimal(oPart.Length) * 2 + Convert.ToDecimal(oPart.Width) * 2 + Convert.ToDecimal(50));
                                var addl = Math.Round(Convert.ToDecimal(boardArea * deckel / 1000000), 2);



                                if (oLayerProperties["rate"].ToString().Length == 0)
                                {
                                    //var TF = Convert.ToDecimal(oLayerProperties["rate"]);
                                    TF = 1;
                                }
                                else
                                {
                                    TF = Convert.ToDecimal(oLayerProperties["rate"]);
                                }


                                    if (i % 2 != 0)
                                    {
                                        //fluted
                                        layerWeight = addl * TF * Convert.ToDecimal(oLayerProperties["gsm"]);
                                        //weight = weight + layerWeight;
                                        BGSM = BGSM + TF * Convert.ToDecimal(oLayerProperties["gsm"]);
                                        BBSVal = BBSVal + (Convert.ToDecimal(oLayerProperties["gsm"]) * Convert.ToDecimal(oLayerProperties["bf"]) / 1000) / 2;
                                    }
                                    else
                                    {
                                        layerWeight = addl * Convert.ToDecimal(oLayerProperties["gsm"]);
                                        //weight = weight + layerWeight;
                                        BGSM = BGSM + Convert.ToDecimal(oLayerProperties["gsm"]);
                                        BBSVal = BBSVal + (Convert.ToDecimal(oLayerProperties["gsm"]) * Convert.ToDecimal(oLayerProperties["bf"]) / 1000);

                                        TF = 1;
                                    }
                                    oLayerProperties["Tk_Factor"] = TF;
                                    oLayerProperties["Rate"] = oLayerProperties["deckel"];
                                    oLayerProperties["Weight"] = (layerWeight) / 1000;
                                    TWeight = TWeight + (layerWeight) / 1000;
                                    //oLayerProperties["Weight"] = layerWeight;
                                    Items_Layers oLayer = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Items_Layers), oLayerProperties) as Items_Layers;

                                    oPart.Items_Layers.Add(oLayer);

                                    i = i + 1;

                                    ply = i;
                                    }
                            if (oPart.PName == "Top")
                            {
                                cutLength = Math.Round(Convert.ToDecimal(oPart.Length) * 2 + Convert.ToDecimal(oPart.Width) * 2 + Convert.ToDecimal(50));
                                deckel = (Convert.ToDecimal(oPart.Height) * 3 + 20);
                                boardArea = (Convert.ToDecimal(cutLength / 100 * deckel / 100));

                                // cuttingsize = Math.Round(Convert.ToDecimal(oPart.Length) * 2 + Convert.ToDecimal(oPart.Width) * 2 + Convert.ToDecimal(50));
                                var addl = Math.Round(Convert.ToDecimal(boardArea * deckel / 1000000), 2);

                                Ups = 1;

                                if (oLayerProperties["rate"].ToString().Length == 0)
                                {
                                    //var TF = Convert.ToDecimal(oLayerProperties["rate"]);
                                    TF = 1;
                                }
                                else
                                {
                                    TF = Convert.ToDecimal(oLayerProperties["rate"]);
                                }


                                if (i % 2 != 0)
                                {
                                    //fluted
                                    layerWeight = Convert.ToDecimal((addl * Convert.ToDecimal(oLayerProperties["gsm"]) / 100) * Convert.ToDecimal(oLayerProperties["rate"]));    //////////////////rate is takeup factor value
                                    //Weight = flutedParameter + layerWeight;
                                }
                                else
                                {
                                    //layerWeight = addl * Convert.ToDecimal(oLayerProperties["gsm"]);
                                    //weight = weight + layerWeight;
                                    layerWeight = Convert.ToDecimal((addl * Convert.ToDecimal(oLayerProperties["gsm"])) / 100);
                                    //nonFlutedParameter = nonFlutedParameter + layerWeight;
                                    TF = 1;
                                }

                                oLayerProperties["Tk_Factor"] = TF;
                                oLayerProperties["Rate"] = oLayerProperties["deckel"];
                                oLayerProperties["Weight"] = (layerWeight)/1000;

                                //TotWeight = Convert.ToDecimal((flutedParameter + nonFlutedParameter) / 1000);
                                TWeight = TWeight + (layerWeight) / 1000;
                                //oLayerProperties["Weight"] = layerWeight;
                                Items_Layers oLayer = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Items_Layers), oLayerProperties) as Items_Layers;

                                oPart.Items_Layers.Add(oLayer);

                                i = i + 1;

                                ply = i;
                            }

                            if (oPart.PName == "Bottom")
                            {
                                cutLength = Math.Round(Convert.ToDecimal(oPart.Length) * 2 + Convert.ToDecimal(oPart.Width) * 2 + Convert.ToDecimal(50));
                                deckel = (Convert.ToDecimal(oPart.Height) * 3 + 20);
                                boardArea = (Convert.ToDecimal(cutLength / 100 * deckel / 100));

                                // cuttingsize = Math.Round(Convert.ToDecimal(oPart.Length) * 2 + Convert.ToDecimal(oPart.Width) * 2 + Convert.ToDecimal(50));
                                var addl = Math.Round(Convert.ToDecimal(boardArea * deckel / 1000000), 2);

                                Ups = 1;

                                if (oLayerProperties["rate"].ToString().Length == 0)
                                {
                                    //var TF = Convert.ToDecimal(oLayerProperties["rate"]);
                                    TF = 1;
                                }
                                else
                                {
                                    TF = Convert.ToDecimal(oLayerProperties["rate"]);
                                }


                                if (i % 2 != 0)
                                {
                                    //fluted
                                    layerWeight = Convert.ToDecimal((addl * Convert.ToDecimal(oLayerProperties["gsm"]) / 100) * Convert.ToDecimal(oLayerProperties["rate"]));    //////////////////rate is takeup factor value
                                    //Weight = flutedParameter + layerWeight;
                                }
                                else
                                {
                                    //layerWeight = addl * Convert.ToDecimal(oLayerProperties["gsm"]);
                                    //weight = weight + layerWeight;
                                    layerWeight = Convert.ToDecimal((addl * Convert.ToDecimal(oLayerProperties["gsm"])) / 100);
                                    //nonFlutedParameter = nonFlutedParameter + layerWeight;
                                    TF = 1;
                                }

                                oLayerProperties["Tk_Factor"] = TF;
                                oLayerProperties["Rate"] = oLayerProperties["deckel"];
                                oLayerProperties["Weight"] = (layerWeight) / 1000;

                                //TotWeight = Convert.ToDecimal((flutedParameter + nonFlutedParameter) / 1000);
                                TWeight = TWeight + (layerWeight) / 1000;
                                //oLayerProperties["Weight"] = layerWeight;
                                Items_Layers oLayer = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Items_Layers), oLayerProperties) as Items_Layers;

                                oPart.Items_Layers.Add(oLayer);

                                i = i + 1;

                                ply = i;
                            }
                            if (oPart.PName == "Plate")
                            {

                                oPart.PName = "Plate";

                                if (BType == 5)
                                {
                                    TLength = (Convert.ToDecimal(oPart.Length)) ;
                                    deckel = (Convert.ToDecimal(oPart.Width));
                                    //  Ups = (Convert.ToDecimal(oPart.Width) / 182);
                                    boardArea = (TLength * deckel);
                                    Ups = 1;


                                    //boardArea = (Convert.ToDecimal(cutLength / 100 * deckel / 100));
                                    cuttingsize = (Convert.ToDecimal((oPart.Length)));
                                    //  cuttingsize = Math.Round(Convert.ToDecimal(oPart.Length) + Convert.ToDecimal(oPart.Width) + Convert.ToDecimal(50));
                                    // var addl = Math.Round(Convert.ToDecimal(oPart.Length)*Convert.ToDecimal(oPart.Width) , 2);

                                    //BBSVal = BBSVal + model.layers[i].gsm() * model.layers[i].bf() / 1000 / 2

                                    if (oLayerProperties["rate"].ToString().Length == 0)
                                    {
                                        //var TF = Convert.ToDecimal(oLayerProperties["rate"]);
                                        TF = 1;
                                    }
                                    else
                                    {
                                        TF = Convert.ToDecimal(oLayerProperties["rate"]);
                                    }

                                    //TF = Convert.ToDecimal(oPart.TakeUpFactor);
                                    if (i % 2 != 0)
                                    {
                                        //fluted
                                        layerWeight = Convert.ToDecimal((Convert.ToDecimal(oLayerProperties["gsm"]) / 1000) * (Convert.ToDecimal(deckel / 100)) * (Convert.ToDecimal(TLength / 100)) * TF);    //////////////////rate is takeup factor value
                                        BBSVal = BBSVal + (Convert.ToDecimal(oLayerProperties["gsm"]) * Convert.ToDecimal(oLayerProperties["bf"]) / 1000) / 2;
                                        //Weight = flutedParameter + layerWeight;
                                        BGSM = BGSM + TF * Convert.ToDecimal(oLayerProperties["gsm"]);
                                    }
                                    else
                                    {
                                        //layerWeight = addl * Convert.ToDecimal(oLayerProperties["gsm"]);
                                        //weight = weight + layerWeight;
                                        layerWeight = Convert.ToDecimal((Convert.ToDecimal(oLayerProperties["gsm"])) / 1000) * (Convert.ToDecimal(deckel /100)) * (Convert.ToDecimal(TLength /100)) ;
                                        BBSVal = BBSVal + (Convert.ToDecimal(oLayerProperties["gsm"]) * Convert.ToDecimal(oLayerProperties["bf"]) / 1000);
                                        //nonFlutedParameter = nonFlutedParameter + layerWeight;
                                        BGSM = BGSM + Convert.ToDecimal(oLayerProperties["gsm"]);
                                        TF = 1;
                                    }

                                    oLayerProperties["Tk_Factor"] = TF;
                                    oLayerProperties["Rate"] = oLayerProperties["deckel"];
                                    oLayerProperties["Weight"] = (layerWeight);

                                    //TotWeight = Convert.ToDecimal((flutedParameter + nonFlutedParameter) / 1000);
                                    TWeight = TWeight + (layerWeight);
                                    //oLayerProperties["Weight"] = layerWeight;
                                    Items_Layers oLayer = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Items_Layers), oLayerProperties) as Items_Layers;

                                    oPart.Items_Layers.Add(oLayer);

                                    i = i + 1;

                                    ply = i;
                                }
                                else if (BType == 1)///rsc 
                                {
                                   
                                     Ups = Math.Round(1820/(Convert.ToDecimal(oPart.Width)));
                                     TLength = (Math.Round(Convert.ToDecimal(Ups))) * (Convert.ToDecimal(oPart.Length)) + 20;
                                    deckel = Math.Round(1820/Convert.ToDecimal(oPart.Width)+20);
                                    
                                    boardArea = (TLength * deckel);

                               
                                    if (oLayerProperties["rate"].ToString().Length == 0)
                                    {
                                        //var TF = Convert.ToDecimal(oLayerProperties["rate"]);
                                        TF = 1;
                                    }
                                    else
                                    {
                                        TF = Convert.ToDecimal(oLayerProperties["rate"]);
                                    }
                                   
                                    //TF = Convert.ToDecimal(oPart.TakeUpFactor);
                                    if (i % 2 != 0)
                                    {
                                        //fluted
                                        layerWeight = Convert.ToDecimal((Convert.ToDecimal(oLayerProperties["gsm"])) * (Convert.ToDecimal(oLayerProperties["deckel"])) * (Convert.ToDecimal(TLength))/1000 * TF);    //////////////////rate is takeup factor value
                                        BBSVal = BBSVal + (Convert.ToDecimal(oLayerProperties["gsm"]) * Convert.ToDecimal(oLayerProperties["bf"]) / 1000) / 2;
                                        //Weight = flutedParameter + layerWeight;
                                        BGSM = BGSM + TF * Convert.ToDecimal(oLayerProperties["gsm"]);
                                    }
                                    else
                                    {
                                        //layerWeight = addl * Convert.ToDecimal(oLayerProperties["gsm"]);
                                        //weight = weight + layerWeight;
                                        layerWeight = Convert.ToDecimal((Convert.ToDecimal(oLayerProperties["gsm"]))  * (Convert.ToDecimal(oLayerProperties["deckel"]))) * (Convert.ToDecimal(TLength)/1000);
                                        BBSVal = BBSVal + (Convert.ToDecimal(oLayerProperties["gsm"]) * Convert.ToDecimal(oLayerProperties["bf"]) / 1000);
                                        //nonFlutedParameter = nonFlutedParameter + layerWeight;
                                        BGSM = BGSM + Convert.ToDecimal(oLayerProperties["gsm"]);
                                        TF = 1;
                                    }

                                    oLayerProperties["Tk_Factor"] = TF;
                                    oLayerProperties["Rate"] = oLayerProperties["deckel"];
                                    oLayerProperties["Weight"] =  (layerWeight/100000);

                                    //TotWeight = Convert.ToDecimal((flutedParameter + nonFlutedParameter) / 1000);
                                    TWeight = TWeight + (layerWeight/100000);
                                    //oLayerProperties["Weight"] = layerWeight;
                                    Items_Layers oLayer = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Items_Layers), oLayerProperties) as Items_Layers;

                                    oPart.Items_Layers.Add(oLayer);

                                    i = i + 1;

                                    ply = i;
                                }
                            }

                            if (oPart.PName == "Cap")
                            {
                                cutLength = Math.Round(Convert.ToDecimal(oPart.Length) * 2 + Convert.ToDecimal(oPart.Width) * 2 + Convert.ToDecimal(50));
                                deckel = (Convert.ToDecimal(oPart.Height) * 3 + 20);
                                boardArea = (Convert.ToDecimal(cutLength / 100 * deckel / 100));

                                // cuttingsize = Math.Round(Convert.ToDecimal(oPart.Length) * 2 + Convert.ToDecimal(oPart.Width) * 2 + Convert.ToDecimal(50));
                                var addl = Math.Round(Convert.ToDecimal(boardArea * deckel / 1000000), 2);
                                Ups = 1;


                                if (oLayerProperties["rate"].ToString().Length == 0)
                                {
                                    //var TF = Convert.ToDecimal(oLayerProperties["rate"]);
                                    TF = 1;
                                }
                                else
                                {
                                    TF = Convert.ToDecimal(oLayerProperties["rate"]);
                                }


                                if (i % 2 != 0)
                                {
                                    //fluted
                                    layerWeight = Convert.ToDecimal((addl * Convert.ToDecimal(oLayerProperties["gsm"]) / 100) *TF);    //////////////////rate is takeup factor value
                                    //Weight = flutedParameter + layerWeight;
                                }
                                else
                                {
                                    //layerWeight = addl * Convert.ToDecimal(oLayerProperties["gsm"]);
                                    //weight = weight + layerWeight;
                                    layerWeight = Convert.ToDecimal((addl * Convert.ToDecimal(oLayerProperties["gsm"])) / 100);
                                    //nonFlutedParameter = nonFlutedParameter + layerWeight;
                                    TF = 1;
                                }

                                oLayerProperties["Tk_Factor"] = TF;
                                oLayerProperties["Rate"] = oLayerProperties["deckel"];
                                oLayerProperties["Weight"] = (layerWeight) / 1000;

                                //TotWeight = Convert.ToDecimal((flutedParameter + nonFlutedParameter) / 1000);
                                TWeight = TWeight + (layerWeight) / 1000;
                                //oLayerProperties["Weight"] = layerWeight;
                                Items_Layers oLayer = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Items_Layers), oLayerProperties) as Items_Layers;

                                oPart.Items_Layers.Add(oLayer);

                                i = i + 1;

                                ply = i;
                            }


                        }

                        if (Ups != 0 || Ups != null)
                        {
                            oPart.NoBoards = Ups;
                        }
                        else
                        {
                            oPart.NoBoards = 1;
                        }
                        oPart.PlyVal = (ply);
                        var QtyVal = oPart.Quantity;
                        oPart.Weight = (TWeight)*QtyVal;
                        oPart.BoardGSM = BGSM;
                        oPart.BoardArea = (boardArea/1000000);
                     
                            oPart.Deckle = (deckel);
                            oPart.CuttingSize = (cuttingsize);
                       
                        oPart.BoardBS = BBSVal ;
                        //OD_ID oMaster = _oEntites.OD_ID.Where(p => p.Fk_FluteID == FluTypeVal && p.PlyValue == ply).Single();
                        ////ODID IDVal = new ODID();
                        //oPart.OLength1 = Convert.ToInt32(oMaster.Length) + oPart.Length;
                        //oPart.OWidth1 = Convert.ToInt32(oMaster.Width) + oPart.Width;
                        //oPart.OHeight1 = Convert.ToInt32(oMaster.Height) + oPart.Height;


                        oBoxSp.ItemPartProperty.Add(oPart);
                        j = j + 1;
                    }
                    _oEntites.AddToBoxSpecs(oBoxSp);

                }
                //From estimation
                else
                {
                    object entry;
                    decimal Specid;
                    if (omValues.TryGetValue("ID", out entry))
                    {
                        //Box specification data exists
                        if (entry != null)
                        {
                            //As per discussion BoxSpecdata should not be updated while creating Estimation data

                            Specid = decimal.Parse(omValues["ID"].ToString());

                            BoxSpec oBoxSpFromShelf = _oEntites.BoxSpecs.Where(p => p.Pk_BoxSpecID == Specid).Single();

                            object orefBSpec = oBoxSpFromShelf;
                            WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(BoxSpec), omValues, ref orefBSpec);

                            string sParts = omValues["parts"].ToString();
                            object[] Parts = JsonConvert.DeserializeObject<object[]>(sParts);

                            foreach (object oItem in Parts)
                            {
                                Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                                decimal id = decimal.Parse(oProperties["id"].ToString());
                                ItemPartProperty oItemPartFromShelf = _oEntites.ItemPartProperty.Where(p => p.Pk_PartPropertyID == id).Single();


                                object orefItemPart = oItemPartFromShelf;
                                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(ItemPartProperty), oProperties, ref orefItemPart);
                                //oItemPartFromShelf.Weight = int.Parse(oProperties["weight"].ToString());

                                string sLayers = oProperties["layers"].ToString();
                                object[] Layers = JsonConvert.DeserializeObject<object[]>(sLayers);

                                foreach (object oLayerItem in Layers)
                                {

                                    Dictionary<string, object> oLayerProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oLayerItem.ToString());
                                    decimal layertid = decimal.Parse(oLayerProperties["id"].ToString());
                                    Items_Layers oItemLayerFromShelf = _oEntites.Items_Layers.Where(p => p.Pk_LayerID == layertid).Single();

                                    object orefItemLayer = oItemLayerFromShelf;
                                    WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(Items_Layers), oLayerProperties, ref orefItemLayer);

                                }


                            }

                        }
                    }
                    //From estimation but box specification data to be newly created
                    else
                    {
                        string sParts = omValues["parts"].ToString();
                        object[] Parts = JsonConvert.DeserializeObject<object[]>(sParts);

                        foreach (object oItem in Parts)
                        {
                            Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                            ItemPartProperty oPart = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(ItemPartProperty), oProperties) as ItemPartProperty;
                            oPart.Weight = (int?)Convert.ToDecimal(oProperties["weight"].ToString());
                            oPart.NoBoards = 1;
                           // oPart.TakeUpFactor = (int?)Convert.ToDecimal(oProperties["TakeUpFactor"].ToString());
                            oPart.TakeUpFactor =Convert.ToDecimal(oProperties["TakeUpFactor"]);


                            string sLayers = oProperties["layers"].ToString();
                            object[] Layers = JsonConvert.DeserializeObject<object[]>(sLayers);

                            foreach (object oLayerItem in Layers)
                            {
                                Dictionary<string, object> oLayerProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oLayerItem.ToString());
                                Items_Layers oLayer = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Items_Layers), oLayerProperties) as Items_Layers;

                                oPart.Items_Layers.Add(oLayer);
                            }

                            oBoxSp.ItemPartProperty.Add(oPart);

                        }
                        _oEntites.AddToBoxSpecs(oBoxSp);

                    }

                    //string[] ss = myString.Split('=');
                   
                    //    string[] eqchild = ss[1].Split(',');
                    decimal Fk_EnqChild = Convert.ToDecimal(omValues["EnqChild"]);
                    decimal VATVal = Convert.ToDecimal(omValues["VATPercentage"]);
                    
                    est_BoxEstimation oEstBox = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(est_BoxEstimation), omValues) as est_BoxEstimation;
                    est_BoxEstimationChild oBoxEstChild = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(est_BoxEstimationChild), omValues) as est_BoxEstimationChild;

                    IEnumerable<est_BoxEstimationChild> oMCount = _oEntites.est_BoxEstimationChild.Where(p => p.Fk_EnquiryChild == Fk_EnqChild).ToList();
                    if (oMCount.Count() > 0)
                    {

                        //oBoxEstChild.Fk_BoxEstimation = oEstBox.Pk_BoxEstimation;

                        //oBoxEstChild.Fk_EnquiryChild = Convert.ToDecimal(eqchild[1]);

                        //oEstBox.est_BoxEstimationChild.Add(oBoxEstChild);

                    }

                    else if (oMCount.Count() == 0)
                    {

                        eq_EnquiryChild oEqChildCount = _oEntites.eq_EnquiryChild.Where(p => p.Pk_EnquiryChild == Fk_EnqChild).Single();
                        var FkEnq = oEqChildCount.Fk_Enquiry;

                        List<eq_EnquiryChild> oEqobj = _oEntites.eq_EnquiryChild.Where(p => p.Fk_Enquiry == FkEnq).ToList();

                        var enqChildIds = _oEntites.eq_EnquiryChild.Where(p => p.Fk_Enquiry == FkEnq).Select(p => p.Pk_EnquiryChild).ToArray();

                        var selected = _oEntites.est_BoxEstimationChild.Where(p => enqChildIds.Any(s => s == p.Fk_EnquiryChild)).ToList();

                        if (selected.Count > 0)
                        {
                            var BoxEst = selected.FirstOrDefault().Fk_BoxEstimation;
                          
                            oBoxEstChild.Fk_BoxEstimation = BoxEst;
                            oBoxEstChild.Fk_EnquiryChild = Convert.ToDecimal(Fk_EnqChild);
                            oBoxEstChild.VATPercentage = Convert.ToDecimal(VATVal);

                            _oEntites.AddToest_BoxEstimationChild(oBoxEstChild);

                            _oEntites.SaveChanges();

                        }
                        else
                        {
                            oEstBox.Date = DateTime.Now;
                            _oEntites.AddToest_BoxEstimation(oEstBox);

                            _oEntites.SaveChanges();

                            oResult.Success = true;


                            oBoxEstChild.Fk_BoxEstimation = oEstBox.Pk_BoxEstimation;
                            oBoxEstChild.Fk_EnquiryChild = Convert.ToDecimal(Fk_EnqChild);
                            oBoxEstChild.VATPercentage = Convert.ToDecimal(VATVal);

                            oEstBox.est_BoxEstimationChild.Add(oBoxEstChild);

                        }

                        oEqChildCount.Estimated = true;

                        est_BoxEstimation oEstBoxFromShelf = _oEntites.est_BoxEstimation.Where(p => p.Pk_BoxEstimation == oBoxEstChild.Fk_BoxEstimation).Single();

                        //decimal TotPrice = decimal.Parse(omValues["totalPrice"].ToString());
                        oEstBoxFromShelf.GrandTotal = ((oEstBoxFromShelf.GrandTotal == null ? 0 : oEstBoxFromShelf.GrandTotal) + (oBoxEstChild.TotalPrice == null ? 0 : oBoxEstChild.TotalPrice))* (oBoxEstChild.eq_EnquiryChild.Quantity == null ? 0 : oBoxEstChild.eq_EnquiryChild.Quantity);

                        _oEntites.SaveChanges();
                        oResult.Success = true;

                        //////////////////////////////////

                    }
                }

                _oEntites.SaveChanges();
                oResult.Message = oBoxSp.Pk_BoxSpecID.ToString();
                oResult.Success = true;

            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            //int BType1 = 0;
            //decimal T1u = 1.5M;
            //decimal cuttingsize1, boardArea1, layerWeight1;
            //decimal deckel1 = 0;


            try
            {
                //omValues["Fk_BoxID"] is used to pass other parameters also
                string myString = omValues["Fk_BoxID"].ToString();
                string[] parts = myString.Split(',');
                decimal Pk_BoxEstChild = 0;
                decimal Bid = decimal.Parse(parts[0]);
                bool bIsBoxSpecData = false;
                //If from boxspecification  only box data to be updated
                if (parts.Length != 1)
                {
                    bIsBoxSpecData = true;
                }
                //if from estimation both box and estimation data to be updated
                else
                {
                    //omValues["Fk_BoxID"] is used to pass other parameters also
                    //Hence resetting it with only FK_BoxID
                    omValues["Fk_BoxID"] = decimal.Parse(parts[0]);

                    string boxEstChildId = omValues["BoxEstimationChild"].ToString();
                    Pk_BoxEstChild = decimal.Parse(boxEstChildId);
                }

                //eId = parts[0];

                BoxSpec oBoxSp = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(BoxSpec), omValues) as BoxSpec;
                //if (bIsBoxSpecData)
                //{
                decimal Specid = decimal.Parse(omValues["ID"].ToString());

                BoxSpec oBoxSpFromShelf = _oEntites.BoxSpecs.Where(p => p.Pk_BoxSpecID == Specid).Single();

                object orefBSpec = oBoxSpFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(BoxSpec), omValues, ref orefBSpec);

                string sParts = omValues["parts"].ToString();
                object[] Parts = JsonConvert.DeserializeObject<object[]>(sParts);

                foreach (object oItem in Parts)
                {
                    Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                    decimal id = decimal.Parse(oProperties["id"].ToString());
                    ItemPartProperty oItemPartFromShelf = _oEntites.ItemPartProperty.Where(p => p.Pk_PartPropertyID == id).Single();
                    oItemPartFromShelf.Weight = (int?)Convert.ToDecimal(oProperties["weight"].ToString());

                    object orefItemPart = oItemPartFromShelf;
                    WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(ItemPartProperty), oProperties, ref orefItemPart);
                    //oItemPartFromShelf.Weight = int.Parse(oProperties["weight"].ToString());

                    string sLayers = oProperties["layers"].ToString();
                    var boardArea = Convert.ToDecimal(oProperties["boardArea"]);
                    var PName = oProperties["PName"];
                    var Length= Convert.ToDecimal(oProperties["length"]);
                    var Width= Convert.ToDecimal(oProperties["width"]);
                    object[] Layers = JsonConvert.DeserializeObject<object[]>(sLayers);
                    int i = 0;
                    decimal addl=0;
                    foreach (object oLayerItem in Layers)
                    {

                        if (PName == "undefinedBox`" || PName == "Box")
                        {
                             addl = Math.Round(Convert.ToDecimal(boardArea), 2);
                        }
                        else if (PName == "Length Partition" || PName == "Width Partition")
                        {
                            addl = Math.Round(Convert.ToDecimal((Length)/1000 ) * Convert.ToDecimal((Width)/1000), 2);
                        }
                        else if (PName == "Plate")
                            {
                                addl = Math.Round(Convert.ToDecimal((Length)) * Convert.ToDecimal((Width)), 2);
                            }
                        else
                        {
                            addl = Math.Round(Convert.ToDecimal(boardArea), 2);
                        }
                        Dictionary<string, object> oLayerProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oLayerItem.ToString());
                        decimal layertid = decimal.Parse(oLayerProperties["id"].ToString());
                        Items_Layers oItemLayerFromShelf = _oEntites.Items_Layers.Where(p => p.Pk_LayerID == layertid).Single();

                        if (oLayerProperties["rate"] != "")
                        { oItemLayerFromShelf.Tk_Factor = decimal.Parse(oLayerProperties["rate"].ToString()); }
                        //else if (oLayerProperties["Tk_Factor"] != "")
                        //{ oItemLayerFromShelf.Tk_Factor = decimal.Parse(oLayerProperties["Tk_Factor"].ToString()); }

                        else
                        {
                            //decimal Bid=  decimal.Parse(omValues["Fk_BoxID"].ToString());
                            BoxMaster oBox= _oEntites.BoxMaster.Where(p => p.Pk_BoxID == Bid).Single();
                            if (i % 2 != 0)
                            {
                                var tkFact = oBox.FluteType.TKFactor;
                                oItemLayerFromShelf.Tk_Factor = tkFact;
                            }
                            else
                            { oItemLayerFromShelf.Tk_Factor = 1; }
                        }


                        var GSMVAL=Convert.ToInt32(oLayerProperties["gsm"]);
                       oItemLayerFromShelf.Weight =Convert.ToDecimal(addl * Convert.ToDecimal(oItemLayerFromShelf.Tk_Factor) * GSMVAL)/10000;
                       
                        object orefItemLayer = oItemLayerFromShelf;
                        WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(Items_Layers), oLayerProperties, ref orefItemLayer);
                        i = i + 1;
                    }


                }

                //}

                if (!bIsBoxSpecData)
                {

                    //string myString = omValues["Fk_BoxID"].ToString();
                    //string[] parts = myString.Split(',');
                    ////string eqchild = parts[2];
                    ////string boxEstId = parts[4];
                    //string boxEstChildId = parts[6];
                    ////eId = parts[0];

                    //decimal Pk_BoxEstChild = decimal.Parse(boxEstChildId);

                    est_BoxEstimationChild BoxEstChildFromShelf = _oEntites.est_BoxEstimationChild.Where(p => p.Pk_BoxEstimationChild == Pk_BoxEstChild).Single();

                    est_BoxEstimation oEstBoxFromShelf = BoxEstChildFromShelf.est_BoxEstimation;
                    //Subtrcting the old total price from grand total
                    oEstBoxFromShelf.GrandTotal = (oEstBoxFromShelf.GrandTotal == null ? 0 : oEstBoxFromShelf.GrandTotal) - (BoxEstChildFromShelf.TotalPrice == null ? 0 : BoxEstChildFromShelf.TotalPrice);

                    object orefBEstChild = BoxEstChildFromShelf;
                    WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(est_BoxEstimationChild), omValues, ref orefBEstChild);

                    decimal TotPrice = decimal.Parse(omValues["totalPrice"].ToString());
                    //Adding the updated total price to the grand total
                    oEstBoxFromShelf.GrandTotal = (oEstBoxFromShelf.GrandTotal == null ? 0 : oEstBoxFromShelf.GrandTotal) + TotPrice;
                }
                _oEntites.SaveChanges();
                oResult.Message = oBoxSp.Pk_BoxSpecID.ToString();
                oResult.Success = true;

            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            throw new NotImplementedException();
        }

        public EntityObject DAO
        {
            get
            {
                return oBoxSp;
            }
            set
            {
                oBoxSp = value as BoxSpec;
            }
        }

        public decimal ID
        {
            get
            {
                return oBoxSp.Pk_BoxSpecID;
            }
            set
            {
                oBoxSp = _oEntites.BoxSpecs.Where(p => p.Pk_BoxSpecID == value).Single();
            }
        }

        public object GetRaw()
        {
            return oBoxSp;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public List<EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public EntityObject AddDetail(string sBasketName, EntityObject oDetail)
        {
            throw new NotImplementedException();
        }

        public EntityObject RemoveDetail(string sBasketName, decimal dPk)
        {
            throw new NotImplementedException();
        }

        public EntityObject UpdateDetail(string sBasketName, decimal dPk, Dictionary<string, object> oValues)
        {
            throw new NotImplementedException();
        }

        public List<EntityObject> GetDetails(string sBasketName)
        {
            throw new NotImplementedException();
        }
    }
}
